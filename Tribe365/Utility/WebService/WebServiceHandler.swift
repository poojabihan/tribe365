//
//  WebServiceHandler.swift
//  Tribe365
//
//  Created by kdstudio on 29/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class WebServiceHandler: NSObject {
    
    class func postWebService(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        if withHeader == true && defaults.value(forKey: "token") == nil {
            return
        }
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                if json["code"].stringValue != "401" {
                    completionHandler(json,json["message"].stringValue)
                }
                else {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let alertView = UIAlertController(title: "", message: "You are no longer the authorized user of this application.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
                        logoutFromApp()
                    }))
                    
                    var topVC = UIApplication.shared.keyWindow?.rootViewController
                    while((topVC!.presentedViewController) != nil){
                        topVC = topVC!.presentedViewController
                    }
                    if !(topVC?.isKind(of: UIAlertController.self))! {
                        topVC?.present(alertView, animated: true, completion: nil)
                    }
                }
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
    }

    class func postWebServiceForHtml(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
       
        let headers =  ["Content-Type":"text/html"]
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
//                completionHandler(json,json["message"].stringValue)
                if json["code"].stringValue != "401" {
                    completionHandler(json,json["message"].stringValue)
                }
                else {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                    let alertView = UIAlertController(title: "", message: "You are no longer the authorized user of this application.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
                        logoutFromApp()
                    }))
                    var topVC = UIApplication.shared.keyWindow?.rootViewController
                    while((topVC!.presentedViewController) != nil){
                        topVC = topVC!.presentedViewController
                    }
                    
                    if !(topVC?.isKind(of: UIAlertController.self))! {
                        topVC?.present(alertView, animated: true, completion: nil)
                    }
                }
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    class func getWebServiceForMultiplePart(url:String, param:[String:Any]?, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void  {
        
        Alamofire.request(url, method : .get, parameters: param).responseJSON { response in
           //  let headers = ["Content-Type":"application/json"]
        
           //  Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
//                completionHandler(json,nil)
                if json["code"].stringValue != "401" {
                    completionHandler(json,nil)
                }
                else {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                    let alertView = UIAlertController(title: "", message: "You are no longer the authorized user of this application.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
                        logoutFromApp()
                    }))
                    var topVC = UIApplication.shared.keyWindow?.rootViewController
                    while((topVC!.presentedViewController) != nil){
                        topVC = topVC!.presentedViewController
                    }
                    
                    
                    if !(topVC?.isKind(of: UIAlertController.self))! {
                        topVC?.present(alertView, animated: true, completion: nil)
                    }
                }

                break
            case .failure(let error):
                print("Error: " + error.localizedDescription)
                completionHandler(nil,error.localizedDescription)

                break
            }
        }
    }
    
    class func getWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
//                completionHandler(json,json["message"].stringValue)
                if json["code"].stringValue != "401" {
                    completionHandler(json,json["message"].stringValue)
                }
                else {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                    let alertView = UIAlertController(title: "", message: "You are no longer the authorized user of this application.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
                        logoutFromApp()
                    }))
                    var topVC = UIApplication.shared.keyWindow?.rootViewController
                    while((topVC!.presentedViewController) != nil){
                        topVC = topVC!.presentedViewController
                    }
                                        
                    if !(topVC?.isKind(of: UIAlertController.self))! {
                        topVC?.present(alertView, animated: true, completion: nil)
                    }
                }

                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    
    
    class func delelteWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
      
        
        let defaults = UserDefaults.standard
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        let headers = ["Authorization":token,"Accept":"application/json"]
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
    
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    //MARK: - UnAuthorized Case Handling
    class func logoutFromApp() {
//        let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
//        let url = kBaseURLForIOT + NetworkConstant.Logout.logout// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
//        self.requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
        
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
        
        print(strFcmToken)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()
    }
    
    class func requestToLogoutFromtellSid(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
                    
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
//                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                    
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        onError?(err)
//                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }

    
}
