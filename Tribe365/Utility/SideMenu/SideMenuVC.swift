
//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import UserNotifications

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    @IBOutlet var imgOrg: UIImageView!

    //MARK: - Variables
    var textField: UITextField?
    let panel = JKNotificationPanel()
    
    var aData = ["", "Home","Know ","Know Members","Feedback","Logout"]
    var aDataImage = [#imageLiteral(resourceName: "Side-home-Black"), #imageLiteral(resourceName: "Side-home-Black"),#imageLiteral(resourceName: "Side-Know-Chetaru-Black"),#imageLiteral(resourceName: "Side-Know-Members-Black"),#imageLiteral(resourceName: "Side-Feedback-Black"),#imageLiteral(resourceName: "Side-Logout-Black")]
    var aDataImageSelected = [#imageLiteral(resourceName: "Side-home-red"), #imageLiteral(resourceName: "Side-home-red"),#imageLiteral(resourceName: "Side-Know-Chetaru-red"),#imageLiteral(resourceName: "Side-Know-Members-red"),#imageLiteral(resourceName: "Side-Feedback-Red"),#imageLiteral(resourceName: "Side-Logout-red")]

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()

        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.reloadTableView(_:)), name: NSNotification.Name(rawValue: "RefreshSideBar"), object: nil)
        
        imgOrg.kf.setImage(with: URL(string: AuthModel.sharedInstance.organisation_logo), placeholder:nil)
    }
    
    //MARK: - Custom Methods
    @objc func reloadTableView(_ notification:Notification) {
        
        self.tableView.reloadData()
    }
    
    //MARK: WebService Methods
    func callWebServiceLogout() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Logout.tribeLogout, param: [:], withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
                
            else{
                if response!["status"].boolValue == true {
                    
                    /*{
                     "code": 200,
                     "status": true,
                     "service_name": "user-logout",
                     "message": "successfully logout"
                     }*/
                    print("SUCCESS Add")
                    AuthModel.sharedInstance.selectedSideBarIndex = 1

                    //Call tellsid logout web service
                    //                    let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
                    //
                    //                    let url = kBaseURLForIOT + NetworkConstant.Logout.logout// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
                    //
                    //                    self.requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)
                    
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
//                    let badgeCount: Int = 0
//                    let application = UIApplication.shared
//                    let center = UNUserNotificationCenter.current()
//                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
//                        // Enable or disable features based on authorization.
//                    }
//                    application.registerForRemoteNotifications()
//                    application.applicationIconBadgeNumber = badgeCount
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login")// as! ViewController
                    
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForDiagnostic")
                    UserDefaults.standard.removeObject(forKey: "isSaved")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForTribeometer")
                    UserDefaults.standard.removeObject(forKey: "isSavedTribeometer")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForFC")
                    UserDefaults.standard.removeObject(forKey:
                        "isSavedFC")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForCuturalStructure")
                    UserDefaults.standard.removeObject(forKey: "isSavedForCuturalStructure")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForMotivation")
                    UserDefaults.standard.removeObject(forKey: "isSavedMotivation")
                    
                    
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    //MARK: - UITableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let aCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            aCell.selectionStyle = .none

            aCell.imgProfile.kf.setImage(with: URL(string: AuthModel.sharedInstance.profileImageUrl), placeholder: #imageLiteral(resourceName: "user_default"))
            aCell.lblName.text = AuthModel.sharedInstance.name + " " + AuthModel.sharedInstance.lastName
            aCell.lblEmail.text = AuthModel.sharedInstance.email
            
            return aCell

        }
        else {
            
            let aCell = tableView.dequeueReusableCell(withIdentifier: "kCell", for: indexPath) as! ProfileTableViewCell
            aCell.selectionStyle = .none

            
            if aData[indexPath.row] == "Know " {
                aCell.lblTitle.text = "Know " + AuthModel.sharedInstance.orgName
            }
            else {
                aCell.lblTitle.text = aData[indexPath.row]
            }
            
            if AuthModel.sharedInstance.selectedSideBarIndex == indexPath.row {
                aCell.lblTitle.textColor = ColorCodeConstant.themeRedColor
                aCell.imgOption.image = aDataImageSelected[indexPath.row]
            }
            else {
                aCell.lblTitle.textColor = ColorCodeConstant.darkTextcolor
                aCell.imgOption.image = aDataImage[indexPath.row]
            }
            
            return aCell

        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: false)
        AuthModel.sharedInstance.selectedSideBarIndex = indexPath.row
        //    var aData = ["", "Home","Know Chetaru","Know Members","Feedback","Logout"]

        switch indexPath.row {
        case 0:
            let _ = kConstantObj.SetIntialMainViewController("ProfileView")
        case 1:
            let _ = kConstantObj.SetIntialMainViewController("CategoryDefinedViewForUser")
        case 2:
            let _ = kConstantObj.SetIntialMainViewController("KnowOrganisationViewController")
        case 3:
            let _ = kConstantObj.SetIntialMainViewController("KnowMembersViewController")
        case 4:
            let _ = kConstantObj.SetIntialMainViewController("IOTFeedBackView")
        case 5:
            let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.callWebServiceLogout()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            }))
            
            self.present(alert, animated: true, completion: nil)
        default:
            print("Default")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 120
        }
        else {
            return 60
        }
    }
}

extension UIAlertController {
    
    func isValidEmail(_ email: String) -> Bool {
        return email.count > 0 && NSPredicate(format: "self matches %@", "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,64}").evaluate(with: email)
    }
    
    @objc func textDidChangeInLoginAlert() {
        if let email = textFields?[0].text,
            let action = actions.last {
            action.isEnabled = isValidEmail(email)
        }
    }
}
