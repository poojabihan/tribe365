//
//  ColorCodeConstant.swift
//  Tribe365
//
//  Created by Apple on 11/11/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class ColorCodeConstant: NSObject {

    static let selectedOptionColor = UIColor(hexString: "f77776")
    static let barValueListGrayColor = UIColor(hexString: "a1a2a3")
    static let borderLightGrayColor = UIColor(hexString: "9a9a9a")
    static let borderRedColor = UIColor(hexString: "C91219")
    static let graphBlueColor = UIColor(hexString: "336FB3")
    
    static let darkTextcolor = UIColor(hexString: "333333")
    static let themeRedColor = UIColor(hexString: "eb1c24")
    static let happyIndexGreenColor = UIColor(hexString: "368e4e")

}

