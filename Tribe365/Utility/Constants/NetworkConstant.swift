//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/*LIVE*/
let kBaseURL = "https://tribe365.chetaru.co.uk/api/"
//let kBaseURLForIOT =  "http://tellsid.softintelligence.co.uk/index.php/apitellsid/"
//let KBaseChatImageUrl =  "http://tellsid.softintelligence.co.uk/assets/upload/chatimg/"
//let kImageURL =  "http://tellsid.softintelligence.co.uk/assets/upload/tellsid/"
let kSupportURL = "http://tribe365.chetaru.co.uk/resources/views/support/"

/*PRODUCTION*/
//let kBaseURL = "https://production.chetaru.co.uk/tribe365/api/"
////let kBaseURLForIOT = "http://demo.thechangeconsultancy.co/tellsid/index.php/apitellsid/"
////let KBaseChatImageUrl = "http://demo.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"
////let kImageURL = "http://demo.thechangeconsultancy.co/tellsid/assets/upload/tellsid/"
//let kSupportURL = "http://production.chetaru.co.uk/tribe365/public/support/"

let kAppNameAPI  = "tribe365"
let kAppNameAlert  = "Tribe365"

let kSuperAdmin  = "1"
let kUser  = "3"
    
class NetworkConstant: NSObject {
    
    struct NotificationType {
        static let thumbdUp = "thumbsup" 
        static let chat = "chat"
        static let checklist = "checklist"
        static let tribeValue = "tribeValue"
        static let functionalLens = "functionalLens"
        static let teamRoleAnswers = "teamRoleAnswers"
        static let cultureStructure = "cultureStructure"
        static let motivation = "motivation"
        static let tribeometer = "tribeometer"
        static let diagnostic = "diagnostic"
        static let bubbleRatings = "bubbleRatings"
        static let kudos = "kudoschamp"

    }
    
    struct Auth {
        static let login = "userLogin"
        static let forgotPassword = "forgotPassword"
        static let updateUserProfile = "updateUserProfile"
        static let changePassword = "updatePasswordWithCurrentPassword"
        static let userProfile = "userProfile"
        static let checklist = "getAllAnswersStatus"
        static let getPerformance = "getPerformance"
    }
    
    struct Organisation {
        static let orgList = "getOrganisationList"
        static let addOrganisation = "addOrganisation"
        static let updateOrganisationLogo = "updateLogo"
        static let updateOrganisation = "updateOrganisation"
        static let getBubbleRatingNotificationList = "getBubbleRatingNotificationList"
        static let getBubbleRatingUnReadNotificationList = "getBubbleRatingUnReadNotificationList"
        static let changeNotificationStatus = "changeNotificationStatus"
        static let deleteOrganisation = "deleteOrganisation"
        static let readAllNotification = "readAllNotification"

    }
    
    struct Department {
        static let departmentList = "getDepartmentList"
        
    }
    struct Office {
        static let officeUser = "addOfficeUser"
        static let addOffice = "addOffice"
        static let getCountryList = "getCountryList"
    }
    
    struct User {
        
        static let userDetail = "getUserList"
        static let addUser = "addOfficeUser"
        static let deleteStaff = "deleteStaff"
        static let updateStaffDetail = "updateStaffDetail"
        static let updateUser = "updateStaffDetail"

    }
    struct AdminReports {
         static let getDOTreportGraph = "getDOTreportGraph"
         static let getCOTteamRoleMapReport = "getCOTteamRoleMapReport"
        static let getSOTcultureStructureReport = "getSOTcultureStructureReport"
        static let getDiagnosticReportForGraph = "getDiagnosticReportForGraph"
        static let getTribeometerReportForGraph  = "getTribeometerReportForGraph"
        static let getSOTmotivationReport = "getSOTmotivationReport"
        static let getReportPdfUrl = "getReportPdfUrl"
        static let getDiagnsticReportSubGraph = "getDiagnsticReportSubGraph"
        static let getHappyIndexMonthCount = "getHappyIndexMonthCount"
        static let getHappyIndexWeeksCount = "getHappyIndexWeeksCount"
        static let getHappyIndexDaysCount = "getHappyIndexDaysCount"
        static let getPersonalitytypeReportSubGraph = "getPersonalityTypeReportSubGraph"
        static let getPersonalitytypeReportSubGraphUser = "getPersonalityTypeReportUserSubGraph"
        static let getPersonalitytypeReport = "getPersonalityTypeReport"

    }
    
    struct DOT {
        static let getBeliefValuesList = "getBeliefValuesList"
        static let addDOT = "addDot"
        static let DOTDetail = "dotDetail"
        static let DOTValuesRatings = "ratingsToDotValues"
        static let DOTAddEvidence = "addDotEvidence"
        static let updateDOT = "updateDot"
        static let deleteEvidance = "deleteDotEvidence/"
        static let updateDotEvidence = "updateDotEvidence"
        static let addValue = "addValue"
        static let deleteDotValue = "deleteDotValue"
        static let addBelief = "addBelief"
        static let updateBelief = "updateBelief"
        static let getEvidenceList = "getEvidenceList"
        static let deleteBelief = "deleteBelief"
        static let getDOTValuesList = "getDOTValuesList"
        static let getDashboardDetails = "getDashboardDetail"
        static let addHappyIndex = "addHappyIndex"

    }
    
    struct  Action {
        
        static let updateAction = "updateAction"
        static let addAction = "addAction"
        static let actionTierList = "actionTierList"
        static let actionsList = "getActionList"
        static let getDepartmentAndUserList = "getDepartmentUserList"
        static let updateStatus = "updateStatus"
        static let getUserByType = "getUserByType"
        static let deleteAction = "deleteAction"
        static let getActionStatusCount = "getActionStatusCount"
        static let getThemeList = "getThemeList"
        static let getUserDashboardReport = "getUserDashboardReport"

    }
    struct Logout{
        
        static let logout = "logout"
        static let tribeLogout = "userLogout"
    }
    struct Comment{
        
        static let commentList = "listComment"
        static let Addcomment = "addComment"
    }
    
    struct COT{
        
        static let questions = "getCOTQuestions"
        static let saveAnswers = "addCOTAnswer"
        static let individualSummary = "getCOTindividualSummary"
        static let isCOTanswerDone = "isCOTanswerDone"
        static let teamSummary = "getCOTteamSummary"
        static let officenDepartments = "getAllOfficenDepartments"
        static let getCOTmaperkey = "getCOTmaperkey"
        static let getCOTMapperSummary = "getCOTMapperSummary"
        static let getCOTteamRoleCompletedAnswers = "getCOTteamRoleCompletedAnswers"
        static let updateCOTteamRoleMapAnswers = "updateCOTteamRoleMapAnswers"
        static let getCOTteamRoleMapValues = "getCOTteamRoleMapValues"
    }
    
    struct SOT {
        static let getSOTdetail = "getSOTdetail"
        static let getSotQuestionList = "getSotQuestionList"
        static let addSOTAnswers = "addSOTanswers"
        static let getSOTQuestionAnswers = "getSOTquestionAnswers"
        static let updateSOTQuestionAnswer = "updateSOTquestionAnswer"
    }
    
    struct MotivationStructure {
        
        static let requestSOTMotivationScoreMail = "requestSOTmotivationScoreMail"
        static let isSOTMotivationAnswersDone = "isSOTmotivationAnswersDone"
        static let getSOTMotivationUserList = "getSOTmotivationUserList"
        static let getSOTmotivationQuestions = "getSOTmotivationQuestions"
        static let addSOTmotivationAnswer = "addSOTmotivationAnswer"
          static let getSOTmotivationCompletedAnswer = "getSOTmotivationCompletedAnswer"
        static let updateSOTmotivationAnswers = "updateSOTmotivationAnswers"
    }
    
    struct FunctionalLens{
        
        static let questionsDistributedToUser = "isQuestionsDistributedToUser"
        static let requestQuestionnaireList = "requestQuestionnaireList"
        static let getCOTFunctionalLensDetail = "getCOTFunctionalLensDetail"
        static let getCOTfunctionalLensQuestionsList = "getCOTfunctionalLensQuestionsList"
        static let getCOTfuncLensCompletedAnswers = "getCOTfuncLensCompletedAnswers"
        static let addCOTfunctionalLensAnswer = "addCOTfunctionalLensAnswer"
        static let updateCOTfunLensAnswers = "updateCOTfunLensAnswers"
    }
    
    struct IOT {
        
        static let postFeedback = "postdetails/"
        static let historyList = "getuserrecord/"
        static let messageList = "getinboxitem/"
        static let resendMail = "resendemailbyid/"
        static let chatList = "getusermessages/"
        static let sendMessage = "sendchatmsg/"
        static let getquestion = "getquestion/"
        static let getuseranser = "getuseranser/"
        static let signup = "signup/"
        static let getIOTdetail1 = "getIOTdetail1/"
        static let postFeedbackNew = "postFeedback"
    }
    
    struct Improve {
        
        static let postFeedback = "postFeedback"
        static let getInboxChatList = "getInboxChatList"
        static let getChatMessages = "getChatMessages"
        static let iotSendMsg = "iotSendMsg"
        static let getFeedbackDetail = "getFeedbackDetail"

    }
  
    struct Reports {
        static let getBubbleRatingList = "getBubbleRatingList"
        static let addBubbleRatings = "addbubbleRatings"
        static let getBubbleFromUserRating = "getBubbleFromUserRating"
        static let getReportFunctionalLensGraph = "getReportFunctionalLensGraph"
        static let addDOTBubbleRatingsToMultiDepartment = "addDOTBubbleRatingsToMultiDepartment"
        static let getBubbleUnReadNotifications = "getBubbleUnReadNotifications"
        static let getOrgDashboardReportWithFilter = "getOrgDashboardReportWithFilter"

    }
    
    struct Diagnostic {

      static let getTribeometerQuestionList = "getTribeometerQuestionList"
      static let getDiagnosticQuestionList = "getDiagnosticQuestionList"
      static let addDiagnosticAnswers = "addDiagnosticAnswers"
      static let addTribeometerAnswers = "addTribeometerAnswers"
      static let isDiagnosticTribeometerAnswerDone = "isDiagnosticTribeometerAnswerDone"
      static let getDiagnosticCompletedAnswers = "getDiagnosticCompletedAnswers"
      static let getTribeometerCompletedAnswers = "getTribeometerCompletedAnswers"
      static let updateDiagnosticAnswers = "updateDiagnosticAnswers"
      static let updateTribeometerAnswers = "updateTribeometerAnswers"
      static let getDiagnosticReport = "getDiagnosticReport"
      static let getTribeometerReport = "getTribeometerReport"
        static let getPersonalityTypeQuestionList = "getPersonalityTypeQuestionList"
        static let getPersonalityTypeCompletedAnswers = "getPersonalityTypeCompletedAnswers"
        static let updatePersonalityTypeAnswers = "updatePersonalityTypeAnswers"
        static let addPersonalityTypeAnswers = "addPersonalityTypeAnswers"

    }
    
    struct SupportScreensType {
        
        static let motivation = "motivation"
        static let tribeSupport = "tribeSupport"
        static let tribeometer = "tribeometer"
        static let diagnostic = "diagnostic"
        static let diagnosticHome = "diagnosticHome"
        static let report = "report"
        static let iot = "iot"
        static let culturalStructure = "culturalStructure"
        static let sot = "sot"
        static let cot = "cot"
        static let teamRoleMap = "teamRoleMap"
        static let functionalLens = "functionalLens"
        static let dot = "dot"
        static let tribe = "tribe"
        static let register = "register"

    }
}
