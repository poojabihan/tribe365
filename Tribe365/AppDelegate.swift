//
//  AppDelegate.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Photos
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SCLAlertView
import Fabric
import Crashlytics
import UserNotificationsUI
import NVActivityIndicatorView
import JKNotificationPanel
let kConstantObj = kConstant()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var _isCurrentpageChat = false
    var _isQuesFromNotify = false
    var customAlert = SCLAlertView()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Fabric.sharedSDK().debug = true
        let defaults = UserDefaults.standard
        
        if #available(iOS 10.0, *) {
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        if AuthModel.sharedInstance.user_id.isEmpty {
            if defaults.value(forKey: "token") != nil {
                
                //direct login
                setLoginData()
                
                if defaults.value(forKey: "role") as! String == "1" {
                    let storyboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    let rootController = storyboard.instantiateViewController(withIdentifier: "ChoiceDesignationViewNav")
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = rootController
                    self.window?.makeKeyAndVisible()
                }
                else if defaults.value(forKey: "role") as! String == "3" {
//                    let storyboard = UIStoryboard(name: "AuthModule", bundle: nil)
//                    let rootController = storyboard.instantiateViewController(withIdentifier: "CategoryDefinedViewNav")
//                    self.window = UIWindow(frame: UIScreen.main.bounds)
//                    self.window?.rootViewController = rootController
//                    self.window?.makeKeyAndVisible()
                    
                    let mainVcIntial =
                        kConstantObj.SetIntialMainViewController("CategoryDefinedViewForUser")
                    self.window?.rootViewController = mainVcIntial

                }
            }
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
//        //            NotificationCenter.default.post(name: NSNotification.Name("showPopUp"), object: self, userInfo: nil)
//                    if AuthModel.sharedInstance.role != kSuperAdmin {
//                        let now = Date()
//                        let four_today = now.dateAt(hours: 16, minutes: 1)
//                        let eleven_fiftyNine_today = now.dateAt(hours: 23, minutes: 59)
//
//                        if now >= four_today && now <=  eleven_fiftyNine_today
//                        {
//                            let isHappyIndex = UserDefaults.standard.value(forKey: "isHappyIndex") as? Bool
//                            let isHappyIndexDate = UserDefaults.standard.value(forKey: "isHappyIndexDate") as? Date
//                            
//                            if isHappyIndex != nil{
//                                let order = Calendar.current.compare(Date(), to: isHappyIndexDate!, toGranularity: .day)
//
//                                if order == .orderedSame {
//                                    if isHappyIndex == false{
//                                        self.showHappyIndexPopUp()
//                                        return
//                                    }
//                                }
//                                else {
//                                    self.showHappyIndexPopUp()
//                                    return
//                                }
//                            }
//                        }
//
//                    }
                    
        
//        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
//        let myNewVC = mainStoryboardIpad.instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
//        let navController = UINavigationController(rootViewController: myNewVC)
//        navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        appDelegate?.window?.rootViewController?.present(navController, animated: true, completion: nil)

//        let defaults = UserDefaults.standard
//        if  defaults.value(forKey: "pendingCountForChecklist") != nil{
//
//            let strBadgeCount: Int = defaults.value(forKey: "pendingCountForChecklist") as! Int
//            
//            UIApplication.shared.applicationIconBadgeNumber =  strBadgeCount
//        }
//        else{
//            UIApplication.shared.applicationIconBadgeNumber = 0
//            
//        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
//        let defaults = UserDefaults.standard
//        if  defaults.value(forKey: "pendingCountForChecklist") != nil{
//
//            let strBadgeCount: Int = defaults.value(forKey: "pendingCountForChecklist") as! Int
//
//            UIApplication.shared.applicationIconBadgeNumber =  strBadgeCount
//        }
//        else{
//            UIApplication.shared.applicationIconBadgeNumber = 0
//
//        }
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - Firebase
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    //MARK: - Push Notifications
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Message Id: ",userInfo["gcm.message_id"] ?? "")
        print("Message ",userInfo)
        print("Notification Delivered")
        
        // iOS 10 will handle notifications through other methods
        if #available(iOS 9.0, *) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
        } else {
            // or use some work around
            return
        }
        
        print("HANDLE PUSH, didReceiveRemoteNotification: ",userInfo)
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["title"] as? NSString {
                    //Do stuff
                    print(message)
                    if message.contains("Feedback") {
//                        NotificationCenter.default.post(name: NSNotification.Name("showPopUp"), object: self, userInfo: nil)
                        if AuthModel.sharedInstance.role != kSuperAdmin {
                            let now = Date()
                            let four_today = now.dateAt(hours: 16, minutes: 1)
                            let eleven_fiftyNine_today = now.dateAt(hours: 23, minutes: 59)

                            if now >= four_today && now <=  eleven_fiftyNine_today
//                            {
//                                let isHappyIndex = UserDefaults.standard.value(forKey: "isHappyIndex") as? Bool
//                                if isHappyIndex == false && isHappyIndex != nil{
//                                    self.showHappyIndexPopUp()
//                                    return
//                                }
//                            }
                            {
                                let isHappyIndex = UserDefaults.standard.value(forKey: "isHappyIndex") as? Bool
                                let isHappyIndexDate = UserDefaults.standard.value(forKey: "isHappyIndexDate") as? Date
                                
                                if isHappyIndex != nil{
                                    let order = Calendar.current.compare(Date(), to: isHappyIndexDate!, toGranularity: .day)

                                    if order == .orderedSame {
                                        if isHappyIndex == false{
                                            self.showHappyIndexPopUp()
                                            return
                                        }
                                    }
                                    else {
                                        self.showHappyIndexPopUp()
                                        return
                                    }
                                }
                            }
                        }
                    }
                    else if message.contains("Kudos") {
                                                
                        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                        let myNewVC = mainStoryboardIpad.instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
                        myNewVC.strUsername = userInfo["gcm.notification.userName"] as! String
                        myNewVC.strEmail = userInfo["gcm.notification.userEmail"] as! String
                        myNewVC.strKudos = userInfo["gcm.notification.kudosCount"] as! String
                        myNewVC.strImgUser = userInfo["userImage"] as! String
                        myNewVC.isMultiple = (userInfo["gcm.notification.isMultiple"] as! String).bool!

                        let navController = UINavigationController(rootViewController: myNewVC)
                        navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.window?.rootViewController?.present(navController, animated: true, completion: nil)
                    }
                    else {
                        NotificationCenter.default.post(name: NSNotification.Name("updateNotificationCount"), object: self, userInfo: userInfo)
                    }
                }
            }
        }
        
       // let title = userInfo["alert.title"] as? String
       // if (title?.contains("Thumbs Up"))! {
//            NotificationCenter.default.post(name: NSNotification.Name("updateNotificationCount"), object: self, userInfo: userInfo)
        //}
        //else {
        //    NotificationCenter.default.post(name: NSNotification.Name("showPopUp"), object: self, userInfo: nil)
        //}
        
        return
        
        // custom code to handle notification content
        if UIApplication.shared.applicationState == .inactive {
            print("INACTIVE")
            
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            
            if (userInfo["changeit_id"] != nil) {
                //Crash2
                
                if !(self.window!.rootViewController is UINavigationController) {
                    
                    NotificationCenter.default.post(name: NSNotification.Name("GoToViewOfPushChat"), object: self, userInfo: userInfo)
                    
                    return
                }
                
                self._isCurrentpageChat = true
                let rootViewController = self.window!.rootViewController as! UINavigationController
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "IOTModule", bundle: nil)
                
                let chatVC = mainStoryboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
                chatVC.strSelectedHistoryChangeitID = userInfo["changeit_id"] as! String
                rootViewController.pushViewController(chatVC, animated: false)
                completionHandler(UIBackgroundFetchResult.newData)
                
            }
            else{
                self._isQuesFromNotify = true
                //crash 4
                if !(self.window!.rootViewController is UINavigationController)
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToViewOfPush"), object: nil)
                    return
                }
                let rootViewController = self.window!.rootViewController as! UINavigationController
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                
                let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
                rootViewController.pushViewController(homeVC, animated: false)
                completionHandler(UIBackgroundFetchResult.newData)
                
            }
            
        }
        else if UIApplication.shared.applicationState == .background
        {
            print("BACKGROUND")
            completionHandler(UIBackgroundFetchResult.newData)
        }
        else
        {
            print("FOREGROUND")
            
            if (userInfo["changeit_id"] != nil) {
                playNotificationSound()
                
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: true, showCircularIcon: true
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Show") {
                    self._isCurrentpageChat = true
                    
                    if !(self.window!.rootViewController is UINavigationController) {
                        
                        NotificationCenter.default.post(name: NSNotification.Name("GoToViewOfPushChat"), object: self, userInfo: userInfo)
                        
                        return
                    }
                    
                    let rootViewController = self.window!.rootViewController as! UINavigationController
                    
                    if (rootViewController.viewControllers.last != nil) {
                        
                        if rootViewController.viewControllers.last is ChatView {
                            
                            let chatVC = rootViewController.viewControllers.last as! ChatView
                            chatVC.strSelectedHistoryChangeitID = userInfo["changeit_id"] as! String
                            chatVC.callWebServiceLoadEailerMessages()
                        }
                            
                        else {
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "IOTModule", bundle: nil)
                            let chatVC = mainStoryboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
                            chatVC.strSelectedHistoryChangeitID = userInfo["changeit_id"] as! String
                            rootViewController.pushViewController(chatVC, animated: false)
                            completionHandler(UIBackgroundFetchResult.newData)
                        }
                    }
                    else {
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "IOTModule", bundle: nil)
                        let chatVC = mainStoryboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
                        chatVC.strSelectedHistoryChangeitID = userInfo["changeit_id"] as! String
                        rootViewController.pushViewController(chatVC, animated: false)
                        completionHandler(UIBackgroundFetchResult.newData)
                        
                    }
                    
                    
                }
                playNotificationSound()
                
                alertView.showCustom(kAppNameAlert, subTitle: "Announcement Received", color: ColorCodeConstant.themeRedColor, icon: UIImage.init(named: "alertIcon.png")!, closeButtonTitle: "Cancel", timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
                
                //                alertView.showInfo(, subTitle: "Announcement Received", circleIconImage: UIImage.init(named: "alertIcon.jpg"))
                
            }
            else{
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: true, showCircularIcon: true
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Show") {
                    print("Second button tapped")
                    self._isQuesFromNotify = true
                    
                    //Crash 3
                    if !(self.window!.rootViewController is UINavigationController)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToViewOfPush"), object: nil)
                        return
                    }
                    
                    let rootViewController = self.window!.rootViewController as! UINavigationController
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
                    rootViewController.pushViewController(homeVC, animated: true)
                    
                }
                playNotificationSound()
                //                alertView.showInfo(kAppNameAlert, subTitle: "Give us your Feedback.", circleIconImage: UIImage.init(named: "alertIcon.jpg"))
                
                alertView.showCustom(kAppNameAlert, subTitle: "Give us your Feedback.", color: ColorCodeConstant.themeRedColor, icon: UIImage.init(named: "alertIcon.png")!, closeButtonTitle: "Cancel", timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("+++++++++++++++++")
        
        if response.notification.request.content.title.contains("Feedback")
        {
//            NotificationCenter.default.post(name: NSNotification.Name("showPopUp"), object: self, userInfo: nil)
            if AuthModel.sharedInstance.role != kSuperAdmin {
                let now = Date()
                let four_today = now.dateAt(hours: 16, minutes: 1)
                let eleven_fiftyNine_today = now.dateAt(hours: 23, minutes: 59)

                if now >= four_today && now <=  eleven_fiftyNine_today
//                {
//                    let isHappyIndex = UserDefaults.standard.value(forKey: "isHappyIndex") as? Bool
//                    if isHappyIndex == false && isHappyIndex != nil{
//                        self.showHappyIndexPopUp()
//                        return
//                    }
//                }
                {
                    let isHappyIndex = UserDefaults.standard.value(forKey: "isHappyIndex") as? Bool
                    let isHappyIndexDate = UserDefaults.standard.value(forKey: "isHappyIndexDate") as? Date
                    
                    if isHappyIndex != nil{
                        let order = Calendar.current.compare(Date(), to: isHappyIndexDate!, toGranularity: .day)

                        if order == .orderedSame {
                            if isHappyIndex == false{
                                self.showHappyIndexPopUp()
                                return
                            }
                        }
                        else {
                            self.showHappyIndexPopUp()
                            return
                        }
                    }
                }
                
            }
            
        }
        else if response.notification.request.content.title.contains("Kudos") {
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
            let myNewVC = mainStoryboardIpad.instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
            myNewVC.strUsername = response.notification.request.content.userInfo["gcm.notification.userName"] as! String
            myNewVC.strEmail = response.notification.request.content.userInfo["gcm.notification.userEmail"] as! String
            myNewVC.strKudos = response.notification.request.content.userInfo["gcm.notification.kudosCount"] as! String
            myNewVC.strImgUser = response.notification.request.content.userInfo["gcm.notification.userImage"] as! String
            myNewVC.isMultiple = (response.notification.request.content.userInfo["gcm.notification.isMultiple"] as! String).bool!
            
            let navController = UINavigationController(rootViewController: myNewVC)
            navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController?.present(navController, animated: true, completion: nil)
        }
        else {
//            let rootViewController = self.window!.rootViewController as? UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
//
//            let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//            rootViewController?.pushViewController(homeVC, animated: false)

            NotificationCenter.default.post(name: NSNotification.Name("updateNotificationCount"), object: self, userInfo: nil)
        }
        
        
        return
        
        if (response.notification.request.content.userInfo["changeit_id"] != nil) {
            self._isCurrentpageChat = true
            
            //Crash1
            // if !(self.window!.rootViewController is UINavigationController) {
            
            NotificationCenter.default.post(name: NSNotification.Name("GoToViewOfPushChat"), object: self, userInfo: response.notification.request.content.userInfo)
            
            return
            // }
            
            let rootViewController = self.window!.rootViewController as! UINavigationController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "IOTModule", bundle: nil)
            
            
            let chatVC = mainStoryboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
            chatVC.strSelectedHistoryChangeitID = response.notification.request.content.userInfo["changeit_id"] as! String
            
            if !(self.window!.rootViewController is UINavigationController) {
                
            }
            rootViewController.pushViewController(chatVC, animated: false)
            completionHandler();
            
        }
        else{
            
            self._isQuesFromNotify = true
            
            if !(self.window!.rootViewController is UINavigationController) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToViewOfPush"), object: nil)
                return
            }
            
            let rootViewController = self.window!.rootViewController as! UINavigationController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
            let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
            rootViewController.pushViewController(homeVC, animated: false)
            completionHandler();
            
            
            
            
            
            /*  let rootViewController = self.window!.rootViewController as! UINavigationController
             let mainStoryboard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
             let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryDefinedView") as! CategoryDefinedView
             rootViewController.pushViewController(homeVC, animated: false)
             completionHandler();*/
            
        }
            
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print(response.notification.request.content.userInfo)
    }
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
//    {
//
//        completionHandler(.alert)
//    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("fcm token: ", fcmToken)
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "fcm_token")
    }
    
    func playNotificationSound() {
        
        if let soundURL = Bundle.main.url(forResource: "sms_alert_aurora", withExtension: "caf") {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
            // Play
            AudioServicesPlaySystemSound(mySound);
        }
    }    
    
    //MARK: - Custom Methods
    func setLoginData() {
        
        let defaults = UserDefaults.standard
        let auth = AuthModel.sharedInstance
        
        auth.name = defaults.value(forKey: "name") as! String
        auth.lastName = defaults.value(forKey: "lastName") as! String
        auth.department = defaults.value(forKey: "department") as! String
        auth.email = defaults.value(forKey: "email") as! String
        auth.user_id = defaults.value(forKey: "user_id") as! String
        auth.departmentId = defaults.value(forKey: "departmentId") as! String
        auth.office = defaults.value(forKey: "office") as! String
        auth.officeId = defaults.value(forKey: "officeId") as! String
        auth.token = defaults.value(forKey: "token") as! String
        auth.role = defaults.value(forKey: "role") as! String
        auth.organisation_logo = defaults.value(forKey: "organisation_logo") as! String
        auth.orgId = defaults.value(forKey: "orgId") as! String
        auth.profileImageUrl = defaults.value(forKey: "profileImage") as! String
        auth.orgName = defaults.value(forKey: "orgname") as! String

        if auth.role == kUser {
            
            auth.isDot = defaults.value(forKey: "isDot") as! String
        }
        
        
    }
    
    @objc func showHappyIndexPopUp() {
        
        let textViewInAlert = UITextView()
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false, showCircularIcon: true
        )
        
        customAlert = SCLAlertView(appearance: appearance)
        let question = "How\'s things at work today?"
        
        let textRect = (question.replacingOccurrences(of: "\n", with: " ") as NSString).boundingRect(with: CGSize(width: 200, height: 100),
                                                                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                                                                     attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)],
                                                                                                     context: nil)
        
        let size1 = textRect.size
        textViewInAlert.frame = CGRect.init(x: 10, y: 0, width: 200, height: size1.height + 10)
        textViewInAlert.isEditable = false
        textViewInAlert.isSelectable = false
        textViewInAlert.textAlignment = .center
        textViewInAlert.font = UIFont.systemFont(ofSize: 14.0)
        textViewInAlert.text = question
        textViewInAlert.textColor = UIColor.darkGray
        textViewInAlert.isHidden = false
        customAlert.customSubview = textViewInAlert
        
        let greenView = UIView(frame: CGRect.init(x: 10, y: 0, width: 200, height: 70))
        let button1 = UIButton.init(type: .system)
        button1.setBackgroundImage(UIImage.init(named: "smiley.png"), for: .normal)
        button1.frame = CGRect.init(x: 20, y: 10, width: 50, height: 50)
        greenView.addSubview(button1)
        
        let button2 = UIButton.init(type: .system)
        button2.setBackgroundImage(UIImage.init(named: "Neutral.png"), for: .normal)
        button2.frame = CGRect.init(x: 80, y: 10, width: 50, height: 50)
        greenView.addSubview(button2)
        
        let button3 = UIButton.init(type: .system)
        button3.setBackgroundImage(UIImage.init(named: "sad.png"), for: .normal)
        button3.frame = CGRect.init(x: 140, y: 10, width: 50, height: 50)
        greenView.addSubview(button3)
        
        customAlert.customSubview = greenView
        
        
        button1.addTarget(self, action: #selector(smileButtonAction), for: .touchUpInside)
        button2.addTarget(self, action: #selector(neutralButtonAction), for: .touchUpInside)
        button3.addTarget(self, action: #selector(sadButtonAction), for: .touchUpInside)
        
        let currentYear = Date()
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy"
        let currentYearString = formatter1.string(from: currentYear)
        print("Current year is ",currentYearString)
        
        customAlert.showCustom(question, subTitle: currentYearString, color: ColorCodeConstant.themeRedColor, icon: #imageLiteral(resourceName: "experience"), closeButtonTitle: nil, timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        
    }
    
    @objc func smileButtonAction()
    {
        callWebServiceToAddHappyIndex(moodStatus: "3")
    }
    
    @objc func neutralButtonAction()
    {
        callWebServiceToAddHappyIndex(moodStatus: "2")
    }
    
    @objc func sadButtonAction()
    {
        callWebServiceToAddHappyIndex(moodStatus: "1")
    }
    
    func callWebServiceToAddHappyIndex(moodStatus: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let panel = JKNotificationPanel()
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id,
             "moodStatus": moodStatus] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.addHappyIndex, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            self.customAlert.hideView()
            
            if response == nil {
                panel.showNotify(withStatus: .failed, inView: self.window!, title: errorMsg)
            }
            else{
                print("Succesfully uploaded")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                NotificationCenter.default.post(name: NSNotification.Name("resetHAppyindex"), object: self, userInfo: nil)
            }
        }
    }
}

