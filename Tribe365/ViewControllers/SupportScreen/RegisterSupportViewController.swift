//
//  RegisterSupportViewController.swift
//  Tribe365
//
//  Created by Apple on 16/04/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
/**
 This class is used as help screens for login screen
*/
class RegisterSupportViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblForUrl: UILabel!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let main_string = "Logins are issued after organisations have signed up via www.tribe365.co Organisations can range from 1 – 10,000 staff"
        let string_to_color = "www.tribe365.co"
        
        let range = (main_string as NSString).range(of: string_to_color)
        
        let attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue , range: range)
        
        lblForUrl.attributedText = attributedString
    }
    
    //MARK: - IBAction Methods
    /**
    * On tap this function is called and popup is dismissed
    */
    @IBAction func btnTapAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    /**
    * This function is called when user taps on url.
    * This function opens the url in safari
    */
    @IBAction func btnOpenLinkAction(_ sender: Any) {
        
        guard let url = URL(string: "https://tribe365.co/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
