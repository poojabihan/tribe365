//
//  Motivation1SwipeViewViewController.swift
//  Tribe365
//
//  Created by Apple on 08/04/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import WebKit
/**
 This class is used for help screens throughout the application
*/
class SupportScreensViewController: UIViewController, WKNavigationDelegate {
    
    // MARK: - IBOutlets
    //Used when there are more than 1 help screen
    @IBOutlet weak var pageControll: UIPageControl!
    
    //Help screens are in form of images
    @IBOutlet weak var imgSwipe: UIImageView!
    
    //loader
    @IBOutlet var Activity: UIActivityIndicatorView!
    
    //Help screens are in form of urls, to load urls we use this
    @IBOutlet var webView : WKWebView!
    
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK: - Variables
    var arrOfImgs = [#imageLiteral(resourceName: "pending"),#imageLiteral(resourceName: "ReportDOT"),#imageLiteral(resourceName: "ReportStaff")]
    var currentPage = 0
    var supportScreenType = NetworkConstant.SupportScreensType.tribeSupport
    var supportUrl = kSupportURL
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        imgSwipe.isUserInteractionEnabled = true
        
        //adde right and left swipe gestures to images
        let swipeLeftGesture=UISwipeGestureRecognizer(target: self, action: #selector(SwipeLeftImage))
        
        swipeLeftGesture.direction = UISwipeGestureRecognizerDirection.left
        imgSwipe.addGestureRecognizer(swipeLeftGesture)
        
        let swipeRightGesture=UISwipeGestureRecognizer(target: self, action: #selector(SwipeRightImage))
        swipeRightGesture.direction = UISwipeGestureRecognizerDirection.right
        imgSwipe.addGestureRecognizer(swipeRightGesture)
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: UIColor.init(red: 255.0/255.0, green: 0/255.0, blue: 25.0/255.0, alpha: 1.0))
        pageControll.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        pageControll.currentPageIndicatorTintColor = UIColor.init(red: 255.0/255.0, green: 0/255.0, blue: 25.0/255.0, alpha: 1.0)
        
        //supportScreenType stores the screen type which requires the help support
        switch supportScreenType {
            //        case NetworkConstant.SupportScreensType.tribeSupport:
            //            arrOfImgs = [#imageLiteral(resourceName: "TribeSupport")]
            //            pageControll.isHidden = true
        //            supportUrl = kSupportURL + ""
        case NetworkConstant.SupportScreensType.motivation:
            arrOfImgs = [#imageLiteral(resourceName: "Motivation1"),#imageLiteral(resourceName: "Motivation2"),#imageLiteral(resourceName: "Motivation3")]
            supportUrl = kSupportURL + "Motivation.html"
        case NetworkConstant.SupportScreensType.tribeometer:
            arrOfImgs = [#imageLiteral(resourceName: "Tribeometer1"), #imageLiteral(resourceName: "Tribeometer2"), #imageLiteral(resourceName: "Tribeometer3")]
            supportUrl = kSupportURL + "Tribeometer.html"
        case NetworkConstant.SupportScreensType.diagnostic:
            arrOfImgs = [#imageLiteral(resourceName: "Diagnostic1.jpg"), #imageLiteral(resourceName: "Diagnostic2"), #imageLiteral(resourceName: "Diagnostic3")]
            supportUrl = kSupportURL + "Diagnostic.html"
        case NetworkConstant.SupportScreensType.diagnosticHome:
            arrOfImgs = [#imageLiteral(resourceName: "DiagnosticsHome.jpg")]
            supportUrl = kSupportURL + "Diagnostics.html"
        case NetworkConstant.SupportScreensType.report:
            arrOfImgs = [#imageLiteral(resourceName: "Reportsupport.jpg")]
            supportUrl = kSupportURL + "Reports.html"
        case NetworkConstant.SupportScreensType.iot:
            arrOfImgs = [#imageLiteral(resourceName: "IOTSupport.jpg")]
            supportUrl = kSupportURL + "IOT.html"
        case NetworkConstant.SupportScreensType.culturalStructure:
            arrOfImgs = [#imageLiteral(resourceName: "Culture Structure1.jpg"), #imageLiteral(resourceName: "Culture Structure2.jpg"), #imageLiteral(resourceName: "Culture Structure3.jpg")]
            supportUrl = kSupportURL + "Culture-Structure.html"
        case NetworkConstant.SupportScreensType.sot:
            arrOfImgs = [#imageLiteral(resourceName: "SOT.jpg")]
            supportUrl = kSupportURL + "sot.html"
        case NetworkConstant.SupportScreensType.functionalLens:
            arrOfImgs = [#imageLiteral(resourceName: "Personality Types1.jpg"), #imageLiteral(resourceName: "Personality Types2.jpg"), #imageLiteral(resourceName: "Personality Types3.jpg"), #imageLiteral(resourceName: "Personality Types4.png")]
            supportUrl = kSupportURL + "Personality.html"
        case NetworkConstant.SupportScreensType.teamRoleMap:
            arrOfImgs = [#imageLiteral(resourceName: "Team Roles1.jpg"), #imageLiteral(resourceName: "Team Roles2.jpg"), #imageLiteral(resourceName: "Team Roles3.jpg")]
            supportUrl = kSupportURL + "team-roles.html"
        case NetworkConstant.SupportScreensType.cot:
            arrOfImgs = [#imageLiteral(resourceName: "COTSupport.jpg")]
            supportUrl = kSupportURL + "cot.html"
        case NetworkConstant.SupportScreensType.dot:
            arrOfImgs = [#imageLiteral(resourceName: "DOT1.jpg"), #imageLiteral(resourceName: "DOT2.jpg"), #imageLiteral(resourceName: "DOT3.jpg")]
            supportUrl = kSupportURL + "dot.html"
        case NetworkConstant.SupportScreensType.tribe:
            arrOfImgs = [#imageLiteral(resourceName: "TribeHelp.jpg")]
            supportUrl = kSupportURL + "tribe365.html"
            //        case NetworkConstant.SupportScreensType.register:
            //            arrOfImgs = [#imageLiteral(resourceName: "Registering.jpg")]
        //            supportUrl = kSupportURL + "tribe365.html"
        default:
            arrOfImgs = [#imageLiteral(resourceName: "TribeSupport")]
            pageControll.isHidden = true
            supportUrl = kSupportURL + "tribe365.html"
        }
        
        imgSwipe.image = arrOfImgs[0]
        pageControll.numberOfPages = arrOfImgs.count == 1 ? 0 : arrOfImgs.count
        
        setupWebView()
    }
    
    //MARK: - Custom Methods
    /**
    * This function is used to set up the web view
    */
    func setupWebView() {
        //        webView = WKWebView()
        webView.navigationDelegate = self
        //        view = webView
        
        //support url is set according to screen type requested in didload method
        let url = URL(string: supportUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
        self.Activity.startAnimating()
        self.webView.navigationDelegate = self
        self.Activity.hidesWhenStopped = true
    }
    
    /**
    * This function is called when image is swiped left
    */
    @objc func SwipeLeftImage(){
        
        if currentPage == arrOfImgs.count-1 {
            //if last image reached then return
            return
        }
        currentPage+=1
        imgSwipe.image = arrOfImgs[currentPage]
        pageControll.currentPage = currentPage
    }
    
    /**
       * This function is called when image is swiped right
    */
    @objc func SwipeRightImage(){
        
        if currentPage == 0 {
             //if first image reached then return
            return
        }
        currentPage-=1
        imgSwipe.image = arrOfImgs[currentPage]
        pageControll.currentPage = currentPage
    }
    
    //MARK: - IBAction Methods
    /**
       * This function is called when Tribe logo is clicked and it sends user back to previous screen
    */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
          * This function is called when back button is clicked and it sends user back to previous screen
    */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- WKNavigationDelegate
    /**
          * This is webview delegate, it is called when url load fails
    */
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        Activity.stopAnimating()
    }
    
    /**
          * This is webview delegate, it is called when url loading starts
    */
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    
    /**
          * This is webview delegate, it is called when url loading finishes
    */
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        Activity.stopAnimating()
        
    }
}


