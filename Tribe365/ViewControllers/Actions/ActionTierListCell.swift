//
//  ActionTierListCell.swift
//  Tribe365
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * Cell contains the tier name for actions
 */
class ActionTierListCell: UITableViewCell {
    
    //tier name
    @IBOutlet weak var lblTierName: UILabel!
    
    //bg white view
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
