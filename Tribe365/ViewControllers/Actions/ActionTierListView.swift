
//
//  ActionTierListView.swift
//  Tribe365
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

/**
 * Popup class used to show the tier, responsible person, risks listing for actions
 */
class ActionTierListView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: -IBOutlets
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //title
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    //done button
    @IBOutlet weak var btnDone: UIButton!
    
    // MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores tiers array
    var arrOfTierList = [TierModel]()
    
    //call back function
    var didFinishSelectTier : ((Any) -> ())?
    var arrOfSlectedTierData = [Any]()
    
    //detect which list needs to be displayed
    var strSegueIdentifier = ""
    var strWhomeCalled = ""
    
    //stores responsible person array
    var arrOfResponsiblePreson = [ResponsiblePersonModel]()
    
    //stores risks array
    var arrThemes = [ThemeModel]()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hides keyboard when user taps anywhere
        hideKeyboardWhenTappedAround()
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        self.handleDoneButton()
        
        if strSegueIdentifier == "ResponsibleClick" {
            //office listing case
            
            //set title
            lblHeaderTitle.text = strWhomeCalled
            
            if arrOfSlectedTierData.count  == 0{
                //set no data in case of no data
                lblNoData.isHidden = false
                lblNoData.text = "No data found for this Tier"
                tblView.isHidden = true
            }
        }
        else if strSegueIdentifier == "ResponsiblePerson"{
            //responsible person listing case
            
            //set title
            lblHeaderTitle.text = "RESPONSIBLE PERSON LIST"
            
            if arrOfResponsiblePreson.count  == 0{
                //set no data in case of no data
                lblNoData.isHidden = false
                lblNoData.text = "No data found"
                tblView.isHidden = true
            }
        }
        else if strSegueIdentifier == "Themes" {
            //risks listing case
            
            //set title
            lblHeaderTitle.text = "RISKS LIST"
            
            if arrThemes.count  == 0{
                //set no data in case of no data
                lblNoData.isHidden = false
                lblNoData.text = "No risks found"
                tblView.isHidden = true
            }
            
        }
        else{
            //tier listing case
            
            //set title
            lblHeaderTitle.text = "SELECT TIER"
            
            //load tiers list
            callWebServiceToGetTierList()
        }
        
    }
    
    //MARK: - IBActions
    /**
     * When user clicks cross button, risk is multiselect and other are single select
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        if strSegueIdentifier == "Themes" {
            //Risks case
            
            //declare the array which stores selected risks
            var arrSelectedThemes = [ThemeModel]()
            
            //loop through risks
            for value in arrThemes {
                
                //if value selected store it in array of selected risks
                if value.isSelected {
                    arrSelectedThemes.append(value)
                }
            }
            
            //after generating the selected array, dismiss the popup & pass the selected risk array in call back method for previous screen
            self.dismiss(animated: false) {
                self.didFinishSelectTier?(arrSelectedThemes)
            }
        }
        else {
            //dismiss the popup
            dismiss(animated: false, completion: nil)
        }
    }
    
    /**
     * When user clicks done button,  risk is multiselect and other are single select so this button is for risk selection
     */
    @IBAction func btnDoneaction() {
        
        //declare selected risk array
        var arrSelectedThemes = [ThemeModel]()
        
        //loop through risks array
        for value in arrThemes {
            
            //store the selected risk into selected risk array
            if value.isSelected {
                arrSelectedThemes.append(value)
            }
        }
        
        //after generating the selected array, dismiss the popup & pass the selected risk array in call back method for previous screen
        self.dismiss(animated: false) {
            self.didFinishSelectTier?(arrSelectedThemes)
        }
    }
    
    /**
     * This function handled the visibility of done button, if anything in the list is selected it shows otherwise it is hidden
     */
    func handleDoneButton() {
        for value in arrThemes {
            if value.isSelected {
                self.btnDone.isHidden = false
                break
            }
        }
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if strSegueIdentifier == "ResponsibleClick" {
            //return office array count
            return arrOfSlectedTierData.count
        }
        else if strSegueIdentifier == "ResponsiblePerson" {
            //return responsible array count
            return arrOfResponsiblePreson.count
        }
        else if strSegueIdentifier == "Themes" {
            //return risk array count
            return arrThemes.count
        }
        else{
            //return tier count
            return arrOfTierList.count
            
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell which return name
        let cell : ActionTierListCell = tableView.dequeueReusableCell(withIdentifier: "ActionTierListCell") as! ActionTierListCell
        
        if strSegueIdentifier == "ResponsibleClick" {
            //office, tier, dept listing case
            
            if lblHeaderTitle.text == "OFFICE"{
                //show office name
                let model = arrOfSlectedTierData[indexPath.row] as! OfficeModel
                cell.lblTierName.text = model.strOffice
            }
            else{
                if arrOfSlectedTierData[indexPath.row] is UserModel {
                    //tier name
                    let model = arrOfSlectedTierData[indexPath.row] as! UserModel
                    cell.lblTierName.text = model.strName
                }
                else {
                    //dept name
                    let model = arrOfSlectedTierData[indexPath.row] as! DepartmentModel
                    cell.lblTierName.text = model.strDepartment
                }
            }
        }
        else if strSegueIdentifier == "ResponsiblePerson" {
            //esponsible person listing case, set name of person
            cell.lblTierName.text = arrOfResponsiblePreson[indexPath.row].strName
            
        }
        else if strSegueIdentifier == "Themes" {
            //risk listing case, multi select
            
            //set risk name
            cell.lblTierName.text = arrThemes[indexPath.row].strTitle
            cell.selectionStyle = .none
            
            if arrThemes[indexPath.row].isSelected {
                //show bg color red in case if risk is selected
                cell.bgView.backgroundColor = ColorCodeConstant.themeRedColor
                cell.lblTierName.textColor = UIColor.white
            }
            else {
                //show not selected
                cell.bgView.backgroundColor = UIColor.white
                cell.lblTierName.textColor = ColorCodeConstant.darkTextcolor
            }
        }
        else{
            //tier listing case
            
            //set tier name
            let model = arrOfTierList[indexPath.row]
            cell.lblTierName.text = model.strName
        }
        return cell
    }
    
    /**
     * Tableview delegate method used when user selects a row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.strSegueIdentifier == "Themes" {
            //risk listing case
            
            //set the particular risk as selected/notSelected depending upon its selection, by setting its select bool variable true/false respectively
            self.arrThemes[indexPath.row].isSelected = !self.arrThemes[indexPath.row].isSelected
            
            //if any of the risk gets selected show done button
            if !self.arrThemes[indexPath.row].isSelected {
                self.btnDone.isHidden = true
                self.handleDoneButton()
            }
            else {
                self.btnDone.isHidden = false
            }
            
            self.tblView.reloadData()
        }
        else {
            //case other than multi-select risk listing
            self.dismiss(animated: false) {
                
                if self.strSegueIdentifier == "ResponsibleClick" {
                    //office, tier, dept listing case, pass the selected value to call back method
                    self.didFinishSelectTier?(self.arrOfSlectedTierData[indexPath.row])
                }
                    
                else if self.strSegueIdentifier == "ResponsiblePerson" {
                    //responsible person listing case, pass the selected value to call back method
                    self.didFinishSelectTier?(self.arrOfResponsiblePreson[indexPath.row])
                }
                    
                else{
                    //tier listing case, pass the selected value to call back method
                    self.didFinishSelectTier?(self.arrOfTierList[indexPath.row])
                }
            }
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: - Calling Web service
    /**
     * API - get tier listing
     */
    func callWebServiceToGetTierList() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Action.actionTierList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse tier list
                    DashboardParser.parseActionTierList(response: response!, completionHandler: { (arrTier) in
                        //store parsed array
                        self.arrOfTierList = arrTier
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
