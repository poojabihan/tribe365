//
//  ViewActionsTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * Action detail cell
 */
class ViewActionsTableViewCell: UITableViewCell {
    
    //action responsible person image
    @IBOutlet weak var imgResponsibleIcon: UIImageView!
    
    //action responsible person name
    @IBOutlet weak var lblResponsibleName: UILabel!
    
    //action description
    @IBOutlet weak var lblTaskDesciption: UILabel!
    
    //start date
    @IBOutlet weak var lblStartDate: UILabel!
    
    //due date
    @IBOutlet weak var lblDueDate: UILabel!
    
    //status image
    @IBOutlet weak var imgStatus: UIImageView!
    
    //height
    @IBOutlet weak var heightConstraintOfViewToManage: NSLayoutConstraint!
    
    //comment button
    @IBOutlet weak var btnCommentAction: UIButton!
    
    //edit button
    @IBOutlet weak var btnEditAction: UIButton!
    
    //managable view
    @IBOutlet weak var viewToManage: UIView!
    
    //segment view
    @IBOutlet weak var viewForSegment: UIView!
    
    //segment control for status
    @IBOutlet weak var segmentctrl: UISegmentedControl!
    
    //responsible person
    @IBOutlet weak var lblResponsiblePreson: UILabel!
    
    //risks
    @IBOutlet weak var lblThemes: UILabel!
    
    //cancel button
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var lblPST: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
