//
//  ViewActionsViewController.swift
//  Tribe365
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

/**
 * This class shows actions list
 */
class ViewActionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HADropDownDelegate {
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    // for HADropDown
    //select tier custom drop down
    @IBOutlet weak var dropDown: HADropDown!
    
    //image org logo
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    // MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //action list array
    var arrOfActionList = [ActionModel]()
    
    //height
    var strHeightofcellTaskLbl = CGFloat()
    
    //store id's
    var strOrgId = ""
    var strActionId = ""
    
    //array office
    var arrOfficeDetail = [OfficeModel]()
    var strOfficeId = ""
    var strOfficeName = ""
    var strStatus = "NOT STARTED"
    var selectedSegmentIndex = Int()
    var strSetNewAlphaTag = Int()
    var arrFilterData = [ActionModel]()
    var filterWorks = Bool()
    var selectedIndexOfDropdownList = Int()
    
    //For delete Action
    var strDeleteActionId = ""
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        if AuthModel.sharedInstance.role == kUser {
            //if logged in is user, set org id accordingly
            strOrgId = AuthModel.sharedInstance.orgId
        }
        
        // for HADropDown
        dropDown.delegate = self
        dropDown.items = ["ALL TIERS", "PRIMARY", "SECONDARY", "TERTIARY", "DEPARTMENT","OFFICE" , "INDIVIDUAL"]
        filterWorks = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //load actions list
        callWebServiceToGetActionList()
        
    }
    
    // MARK: - HADropDown Delegate
    /**
     * Delegate method of HADropDown, called when user selects any value from drop down
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(index)")
        
        //set the selected values
        selectedIndexOfDropdownList = index
        print(selectedIndexOfDropdownList)
        arrFilterData.removeAll()
        
        //according to the value selected from drop down, reload the list of actions
        switch index {
        case 0:
            
            print("ALL TIERS")
            filterWorks = false
            tblView.isHidden = false
            tblView.reloadData()
            
        case 1:
            filterWorks = true
            for i in (0..<self.arrOfActionList.count) {
                
                if("PRIMARY" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
        case 2:
            print("SECONDARY")
            filterWorks = true
            
            for i in (0..<self.arrOfActionList.count) {
                
                if("SECONDARY" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
            
        case 3:
            print("TERTIARY")
            filterWorks = true
            
            for i in (0..<self.arrOfActionList.count) {
                
                if("TERTIARY" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
            
        case 4:
            print("DEPARTMENT")
            filterWorks = true
            
            for i in (0..<self.arrOfActionList.count) {
                
                if("DEPARTMENT" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
            
        case 5:
            print("OFFICE")
            filterWorks = true
            
            for i in (0..<self.arrOfActionList.count) {
                
                if("OFFICE" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
            
        case 6:
            print("INDIVIDUAL")
            filterWorks = true
            
            for i in (0..<self.arrOfActionList.count) {
                
                if("INDIVIDUAL" == arrOfActionList[i].strActionTierType.uppercased()){
                    
                    arrFilterData.append(arrOfActionList[i])
                }
                print(arrFilterData)
            }
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
            
        default:
            print("ALL TIERS")
            filterWorks = false
            if arrFilterData.count == 0 {
                
                tblView.isHidden = true
                lblNoData.text = "No actions found."
                lblNoData.isHidden = false
                
            }
            else{
                tblView.isHidden = false
                tblView.reloadData()
                lblNoData.isHidden = true
                
            }
            
        }
    }
    
    // MARK: - Custom methods
    /**
     * Sets the table data
     */
    func setTableData(index : Int) {
        arrFilterData.removeAll()
        for i in (0..<self.arrOfActionList.count) {
            
            if(dropDown.items[index] == arrOfActionList[i].strActionTierType.uppercased()){
                print(arrOfActionList[i].strActionTierType)
                print(arrOfActionList[i].strOrgStatus)
                
                arrFilterData.append(arrOfActionList[i])
            }
        }
        tblView.reloadData()
    }
    
    
    
    //MARK: - IBActions
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when user clicks on Add new action
     */
    @IBAction func btnAddActionActions(_ sender: Any) {
        
        //redirects user to add new action screen
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "AddActionView") as! AddActionView
        objVC.strOrgId = strOrgId
        objVC.arrOfficeDetail = arrOfficeDetail
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterWorks == false {
            //actions list
            return arrOfActionList.count
        }
        else{
            //filtered list
            return arrFilterData.count
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom action cell
        let cell : ViewActionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ViewActionsTableViewCell") as! ViewActionsTableViewCell
        
        
        if filterWorks == false{
            //case when no filter applied
            
            //hide label
            cell.lblPST.isHidden = true
            cell.viewForSegment.alpha = 0
            
            //show status image & edit button
            cell.imgStatus.alpha = 1
            cell.btnEditAction.alpha = 1
            
            cell.lblTaskDesciption.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblTaskDesciption.numberOfLines = 0
            
            //set data according to labels
            cell.imgResponsibleIcon.image = UIImage(named:"Responsible")
            
            cell.lblResponsibleName.text = arrOfActionList[indexPath.row].strName
            
            cell.lblTaskDesciption.text = arrOfActionList[indexPath.row].strDescription
            
            cell.lblStartDate.text = arrOfActionList[indexPath.row].strStartedDate
            
            cell.lblDueDate.text = arrOfActionList[indexPath.row].strDueDate
            
            cell.lblResponsiblePreson.text = arrOfActionList[indexPath.row].strResponsibleName
            cell.lblThemes.text = arrOfActionList[indexPath.row].strThemesForDisplay
            cell.btnCommentAction.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.btnCommentAction.addTarget(self, action: #selector(commentAction(_:)), for: .touchUpInside)
            
            
            if AuthModel.sharedInstance.user_id == arrOfActionList[indexPath.row].strUserId{
                //case when action is assigned to logged in user
                //show edit button & set its target
                cell.btnEditAction.alpha = 1
                cell.btnEditAction.tag = indexPath.row
                cell.btnEditAction.addTarget(self, action: #selector(EditAction(_:)), for: .touchUpInside)
                
            }
                
            else{
                //hide edit button if action is not assigned to logged in user
                cell.btnEditAction.alpha = 0
                
            }
            
            //Image Check
            //set image according to status
            if arrOfActionList[indexPath.row].strOrgStatus == "NOT STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Not Started")
                
            }
            else if arrOfActionList[indexPath.row].strOrgStatus == "STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Orange Started")
                
            }
                
            else if arrOfActionList[indexPath.row].strOrgStatus == "COMPLETED"{
                
                cell.imgStatus.image = UIImage(named: "Green Complete")
                
            }
            cell.btnEditAction.tag = indexPath.row
            cell.lblTaskDesciption.frame.size.height = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))!
            
            //commented 5 sept
            //        strHeightofcellTaskLbl = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 150
            //
            //        cell.heightConstraintOfViewToManage.constant = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 125
            
            cell.viewToManage.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.viewToManage.layer.borderWidth = 0.5
            
            return cell
        }
            
        else{
            //case when filter is applied
            
            cell.lblPST.isHidden = true
            cell.viewForSegment.alpha = 0
            
            cell.imgStatus.alpha = 1
            cell.btnEditAction.alpha = 1
            
            cell.lblTaskDesciption.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblTaskDesciption.numberOfLines = 0
            
            cell.imgResponsibleIcon.image = UIImage(named:"Responsible")
            
            cell.lblResponsibleName.text = arrFilterData[indexPath.row].strName
            
            cell.lblTaskDesciption.text = arrFilterData[indexPath.row].strDescription
            
            cell.lblStartDate.text = arrFilterData[indexPath.row].strStartedDate
            
            cell.lblDueDate.text = arrFilterData[indexPath.row].strDueDate
            
            cell.lblResponsiblePreson.text = arrFilterData[indexPath.row].strResponsibleName
            cell.lblThemes.text = arrFilterData[indexPath.row].strThemesForDisplay
            
            cell.btnCommentAction.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.btnCommentAction.addTarget(self, action: #selector(commentAction(_:)), for: .touchUpInside)
            
            
            if AuthModel.sharedInstance.user_id == arrFilterData[indexPath.row].strUserId{
                
                cell.btnEditAction.alpha = 1
                cell.btnEditAction.tag = indexPath.row
                cell.btnEditAction.addTarget(self, action: #selector(EditAction(_:)), for: .touchUpInside)
            }
                
            else{
                cell.btnEditAction.alpha = 0
                
            }
            
            //Image Check
            
            if arrFilterData[indexPath.row].strOrgStatus == "NOT STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Not Started")
                
            }
            else if arrFilterData[indexPath.row].strOrgStatus == "STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Orange Started")
                
            }
                
            else if arrFilterData[indexPath.row].strOrgStatus == "COMPLETED"{
                
                cell.imgStatus.image = UIImage(named: "Green Complete")
                
            }
            cell.btnEditAction.tag = indexPath.row
            cell.lblTaskDesciption.frame.size.height = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))!
            
            //commented 5 sept
            //                strHeightofcellTaskLbl = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 150
            //
            //                cell.heightConstraintOfViewToManage.constant = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 125
            
            cell.viewToManage.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.viewToManage.layer.borderWidth = 0.5
            
            return cell
        }
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //commented 5 sept
        //        return strHeightofcellTaskLbl
        return UITableViewAutomaticDimension
    }
    
    /**
     * Tableview delegate method for swipe functionality
     */
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let deleteAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            
            
            // Handle delete action
        }
        
        let editAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            // Handle delete action
        }
        
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "editOrganizations"), for: .normal)
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "Delete"), for: .normal)
        return [deleteAction, editAction]
        
    }
    
    /**
     * Tableview delegate method for swipe functionality
     */
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //delete and edit buttons are declared & defined
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            self.strDeleteActionId =  self.arrOfActionList[indexPath.row].strId
            
            let alertController = UIAlertController(title: "", message: "Are you sure you want to delete this Action?", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                //run your function here
                self.callWebServiceForDeleteUser()
            }))
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            self.tblView.reloadData()
            // Reset state
            success(true)
        })
        
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "AddActionView") as! AddActionView
            
            objVC.arrOfActionToUpdate = [self.arrOfActionList[indexPath.row]]
            objVC.arrOfficeDetail = self.arrOfficeDetail
            
            self.navigationController?.pushViewController(objVC, animated: true)
            
            // Reset state
            success(true)
        })
        
        editAction.image = UIImage(named: "editOrganizations")
        editAction.backgroundColor = ColorCodeConstant.darkTextcolor
        
        
        deleteAction.image = UIImage(named: "Delete")
        deleteAction.backgroundColor = ColorCodeConstant.themeRedColor
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    /**
     * Tableview delegate method to enable swipe functionality on cell
     */
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        if AuthModel.sharedInstance.user_id == arrOfActionList[indexPath.row].strUserId{
            return true
            
        }
        else if AuthModel.sharedInstance.role == kSuperAdmin{
            return true
            
        }
        else{
            return false
        }
    }
    
    //MARK: - Cell Buttons IBAction
    
    //Edit Button Action
    @objc func EditAction(_ sender: UIButton) {
        
        if filterWorks == false{
            print(sender.tag)
            strSetNewAlphaTag = sender.tag
            let index = IndexPath(row: sender.tag, section: 0)
            let cell: ViewActionsTableViewCell = self.tblView.cellForRow(at: index) as! ViewActionsTableViewCell
            
            strActionId = arrOfActionList[sender.tag].strId
            
            cell.btnCancel.addTarget(self, action: #selector(CancelAction(_:)), for: .touchUpInside)
            cell.imgStatus.alpha = 0
            cell.btnEditAction.alpha = 0
            
            cell.viewForSegment.alpha = 1
            
            
            if arrOfActionList[sender.tag].strOrgStatus == "NOT STARTED"{
                
                cell.segmentctrl.selectedSegmentIndex = 0
                
            }
            else if arrOfActionList[sender.tag].strOrgStatus == "STARTED"{
                
                cell.segmentctrl.selectedSegmentIndex = 1
                
            }
                
            else if arrOfActionList[sender.tag].strOrgStatus == "COMPLETED"{
                
                cell.segmentctrl.selectedSegmentIndex = 2
                
            }
            
            let font = UIFont.systemFont(ofSize: 12)
            cell.segmentctrl.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                    for: .normal)
            
            selectedSegmentIndex = cell.segmentctrl.selectedSegmentIndex
            
            cell.segmentctrl.addTarget(self, action: #selector(ChangeStatus(_:)), for: .valueChanged)
        }
            
        else{
            do {
                
                print(sender.tag)
                self.strSetNewAlphaTag = sender.tag
                let index = IndexPath(row: sender.tag, section: 0)
                let cell: ViewActionsTableViewCell = self.tblView.cellForRow(at: index) as! ViewActionsTableViewCell
                
                self.strActionId = self.arrFilterData[sender.tag].strId
                
                cell.btnCancel.addTarget(self, action: #selector(self.CancelAction(_:)), for: .touchUpInside)
                cell.imgStatus.alpha = 0
                cell.btnEditAction.alpha = 0
                
                cell.viewForSegment.alpha = 1
                
                
                if self.arrFilterData[sender.tag].strOrgStatus == "NOT STARTED"{
                    
                    cell.segmentctrl.selectedSegmentIndex = 0
                    
                }
                else if self.arrFilterData[sender.tag].strOrgStatus == "STARTED"{
                    
                    cell.segmentctrl.selectedSegmentIndex = 1
                    
                }
                    
                else if self.arrFilterData[sender.tag].strOrgStatus == "COMPLETED"{
                    
                    cell.segmentctrl.selectedSegmentIndex = 2
                    
                }
                
                let font = UIFont.systemFont(ofSize: 12)
                cell.segmentctrl.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                        for: .normal)
                
                self.selectedSegmentIndex = cell.segmentctrl.selectedSegmentIndex
                
                cell.segmentctrl.addTarget(self, action: #selector(self.ChangeStatus(_:)), for: .valueChanged)
            }
        }
    }
    
    /**
     * Cance button
     */
    @objc func CancelAction(_ sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: ViewActionsTableViewCell = self.tblView.cellForRow(at: index) as! ViewActionsTableViewCell
        
        cell.imgStatus.alpha = 1
        cell.btnEditAction.alpha = 1
        
        cell.viewForSegment.alpha = 0
        
        
    }
    
    //Change Segment Controll Action [CELL WALI]
    @objc func ChangeStatus(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            strStatus = "NOT STARTED"
            callWebServiceToUpdateStatus()
            
            print(strStatus)
            
        case 1:
            strStatus = "STARTED"
            callWebServiceToUpdateStatus()
            
            print(strStatus)
            
        case 2:
            strStatus = "COMPLETED"
            callWebServiceToUpdateStatus()
            
            print(strStatus)
            
        default:
            strStatus = "NOT STARTED"
            print(strStatus)
            
        }
    }
    
    /**
     * When comment button is clicked
     */
    @objc func commentAction(_ sender: UIButton) {
        //redirect user to comments view only data passing is different
        if filterWorks == false{
            
            let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "CommentView") as! CommentView
            
            objVC.arrOfActionDetail = arrOfActionList[sender.tag]
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else{
            let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "CommentView") as! CommentView
            
            objVC.arrOfActionDetail = arrFilterData[sender.tag]
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }
        
        
    }
    
    
    
    //MARK: - Calling Web service
    /**
     * API - delete action
     */
    func callWebServiceForDeleteUser() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /*{
         "actionId":"155"
         }*/
        let param = ["actionId": strDeleteActionId ]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.deleteAction , param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.strDeleteActionId = ""
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Action deleted successfully.")
                    self.callWebServiceToGetActionList()
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - update status (started/notStarted/completed)
     */
    func callWebServiceToUpdateStatus(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "actionId":"1",
         "orgStatus":"Completed"
         }*/
        
        let param =
            [ "actionId": strActionId,
              "orgStatus": strStatus] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.updateStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Status updated successfully")
                    
                    if self.filterWorks == false {
                        
                        let index = IndexPath(row: self.strSetNewAlphaTag , section: 0)
                        let cell: ViewActionsTableViewCell = self.tblView.cellForRow(at: index) as! ViewActionsTableViewCell
                        cell.imgStatus.alpha = 1
                        cell.btnEditAction.alpha = 1
                        cell.viewForSegment.alpha = 0
                        self.callWebServiceToGetActionList()
                        self.tblView.reloadData()
                    }
                    else{
                        
                        let index = IndexPath(row: self.strSetNewAlphaTag , section: 0)
                        let cell: ViewActionsTableViewCell = self.tblView.cellForRow(at: index) as! ViewActionsTableViewCell
                        cell.imgStatus.alpha = 1
                        cell.btnEditAction.alpha = 1
                        cell.viewForSegment.alpha = 0
                        self.callWebServiceToGetActionList()
                        //                        self.tblView.reloadData()
                        //                        self.setTableData(index: self.selectedIndexOfDropdownList)
                        
                    }
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - get actions list
     */
    func callWebServiceToGetActionList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =
            ["orgId": strOrgId] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.actionsList, param: param, withHeader: true ) { (response, errorMsg) in
            
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseActionList(response: response!, completionHandler: { (arr) in
                        self.arrOfActionList.removeAll()
                        self.arrOfActionList = arr
                    })
                    if self.filterWorks == true{
                        
                        self.setTableData(index: self.selectedIndexOfDropdownList)
                        
                    }
                    if self.arrOfActionList.count == 0{
                        
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = "No actions found."
                        self.dropDown.isHidden = true
                        
                        
                    }
                    else{
                        self.lblNoData.isHidden = true
                        self.dropDown.isHidden = false
                        
                        
                    }
                    self.tblView.reloadData()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                }
                else {
                    print(errorMsg ?? "")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}

