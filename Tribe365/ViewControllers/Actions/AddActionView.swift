
//
//  AddActionView.swift
//  Tribe365
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is used to add new action
 */
class AddActionView: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    //for tier field
    @IBOutlet weak var txtSelectTier: UITextField!
    
    //for responsible person field
    @IBOutlet weak var txtResponsible: UITextField!
    
    //for start date
    @IBOutlet weak var txtStartDate: UITextField!
    
    //for due date
    @IBOutlet weak var txtDueDate: UITextField!
    
    //for status segment control
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    
    //for description field
    @IBOutlet weak var txtDescription: UITextView!
    
    //for table view
    @IBOutlet weak var tblView: UITableView!
    
    //for responsible person field
    @IBOutlet weak var txtResponsiblePerson: UITextField!
    
    //for risk field
    @IBOutlet weak var txtThemes: UITextField!
    
    //heigth for resonsible view
    @IBOutlet weak var heightContraintOfResponsibleModule: NSLayoutConstraint!
    @IBOutlet weak var viewOfResponsibleModule: UIView!
    @IBOutlet weak var lblTitleSetOfResponsibleOffOrDep: UILabel!
    
    //header
    @IBOutlet weak var lblHeader: UILabel!
    
    //org logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //textfield instance
    var textField: UITextField?
    
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //department list array
    var arrOfGetDeparmentList = [DepartmentModel]()
    
    //user list array
    var arrOfGetUserList = [UserModel]()
    
    //status
    var strStatus = ""
    
    //selected tier id
    var strSelectTierId = ""
    
    //selected office id
    var strSelectedOfficeId = ""
    
    //selected dept id
    var strDepartmentId = ""
    
    //office id
    var strOfficeId = ""
    
    //responsible person id
    var strResponsiblePersonId = ""
    
    //user id
    var strIndividualId = ""
    
    //org id
    var strOrgId = ""
    
    //office model array
    var arrOfficeDetail = [OfficeModel]()
    
    //tier type
    var strTierType = ""
    
    //type id
    var strTypeId = ""
    
    //list of responsible person
    var arrRespnsiblePreseonList = [ResponsiblePersonModel]()
    
    //risk list array
    var arrThemes = [ThemeModel]()
    
    //selected risk array
    var arrSelectedThemes = [ThemeModel]()
    
    //Update Acion
    var arrOfActionToUpdate = [ActionModel]()
    
    //Responsible Case arrays
    var arrOfOffices = [OfficeModel]()
    let picker : UIDatePicker = UIDatePicker()
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set org logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //hide keyboard when click anywhere
        self.hideKeyboardWhenTappedAround()
        
        //navigation apperance
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(AddActionView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddActionView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        //description text appearnce
        txtDescription.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        txtDescription.layer.borderWidth = 1
        txtDescription.font = UIFont(name: "verdana", size: 13.0)
        txtDescription.returnKeyType = .done
        txtDescription.delegate = self
        
        heightContraintOfResponsibleModule.constant = 0
        viewOfResponsibleModule.alpha = 0
        print(arrOfficeDetail)
        
        if AuthModel.sharedInstance.role == kUser {
            //user case
            strOrgId = AuthModel.sharedInstance.orgId
        }
        
        //set initial status text
        strStatus = "NOT STARTED"
        
        if arrOfActionToUpdate.count != 0 {
            //update case
            
            //set initial details
            lblHeader.text = "UPDATE ACTION"
            txtDueDate.text = arrOfActionToUpdate[0].strDueDate
            txtStartDate.text =  arrOfActionToUpdate[0].strStartedDate
            txtSelectTier.text =  arrOfActionToUpdate[0].strActionTierType.uppercased()
            txtDescription.text =  arrOfActionToUpdate[0].strDescription
            //txtResponsible.text =  arrOfActionToUpdate[0].
            txtResponsiblePerson.text =  arrOfActionToUpdate[0].strResponsibleName
            strOrgId =  arrOfActionToUpdate[0].strOrgId
            strStatus = arrOfActionToUpdate[0].strOrgStatus.uppercased()
            strSelectTierId = arrOfActionToUpdate[0].strTierId
            strTierType = arrOfActionToUpdate[0].strActionTierType.uppercased()
            strResponsiblePersonId = arrOfActionToUpdate[0].strResponsibleUserId
            
            //show risk name if only one risk is assigned
            if arrOfActionToUpdate[0].arrThemes.count == 1 {
                self.txtThemes.text = arrOfActionToUpdate[0].arrThemes[0].strTitle
            }
                //show count if more than one risk is selected
            else if arrOfActionToUpdate[0].arrThemes.count > 1{
                self.txtThemes.text = String(arrOfActionToUpdate[0].arrThemes.count) + " selected"
            }
            
            //set selected risks
            arrSelectedThemes = arrOfActionToUpdate[0].arrThemes
            /*  if strTierType == "OFFICE"{
             
             lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE OFFICE"
             }
             else if strTierType == "DEPARTMENT"{
             
             lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE DEPARTMENT"
             }*/
            
            //Case for action status at update action end
            if strStatus == "NOT STARTED"{
                
                segmentCtrl.selectedSegmentIndex = 0
                
            }
            else if strStatus == "STARTED"{
                
                segmentCtrl.selectedSegmentIndex = 1
                
            }
            else if strStatus == "COMPLETED"{
                
                segmentCtrl.selectedSegmentIndex = 2
                
            }
            
            //Case for office, deparment filter at update action end
            if self.txtSelectTier.text == "PRIMARY" || self.txtSelectTier.text == "SECONDARY" || self.txtSelectTier.text == "TERTIARY"  || self.txtSelectTier.text == "INDIVIDUAL" {
                
                //when tier are as mentioned above, then do not show responsible module
                self.heightContraintOfResponsibleModule.constant = 0
                self.viewOfResponsibleModule.alpha = 0
                
                //New
                self.strTierType = "organisation"
                self.strTypeId = arrOfActionToUpdate[0].strOrgId
                self.callWebServiceToGetResponsiblePersonList()
                
            }
                
            else if strTierType == "DEPARTMENT" {
                
                //when tier is department, then show responsible module for dept selection
                self.heightContraintOfResponsibleModule.constant = 75
                self.viewOfResponsibleModule.alpha = 1
                self.lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE DEPARTMENT"
                self.callWebServiceToGetDepartmentAnduserList()
                
                txtResponsible.text =  arrOfActionToUpdate[0].strOffDeptName
                strDepartmentId = arrOfActionToUpdate[0].strOffDeptId
                
                //New
                self.callWebServiceToGetResponsiblePersonList()
                self.strTierType = "department"
                print(self.strDepartmentId)
                self.strTypeId = self.strDepartmentId
            }
                
            else if strTierType == "OFFICE" {
                
                //when tier is office, then show responsible module for office selection
                self.lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE OFFICE"
                self.heightContraintOfResponsibleModule.constant = 75
                self.viewOfResponsibleModule.alpha = 1
                
                strOfficeId = arrOfActionToUpdate[0].strOffDeptId
                txtResponsible.text =  arrOfActionToUpdate[0].strOffDeptName
                
                //NEW
                self.strTierType = "office"
                print(self.strOfficeId)
                self.strTypeId = self.strOfficeId
                self.callWebServiceToGetResponsiblePersonList()
                
            }
            
            //strTypeId = arrOfActionToUpdate[0].str
            
        }
    }
    
    //MARK: - IBActions
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnSelectResponsible(_ sender: UIButton) {
        
        
    }
    
    /**
     * Loads the list according to the type selected
     */
    @IBAction func btnResponsibllePersonAction(_ sender: UIButton) {
        
        //            if AuthModel.sharedInstance.role == "1"{
        
        if self.txtSelectTier.text == "OFFICE" {
            //load office list in case of tier is office
            self.strTierType = "office"
            print(self.strOfficeId)
            self.strTypeId = self.strOfficeId
            self.callWebServiceToGetResponsiblePersonListNew(isOffice: true)
            
        }
        else if self.txtSelectTier.text == "DEPARTMENT"{
            //load dept list in case of tier is office
            self.strTierType = "department"
            print(self.strDepartmentId)
            self.strTypeId = self.strDepartmentId
            self.callWebServiceToGetResponsiblePersonListNew(isOffice: false)
        }
        else {
            //load reposible list in case of tier is office
            self.heightContraintOfResponsibleModule.constant = 0
            self.viewOfResponsibleModule.alpha = 0
            
            //New
            self.strTierType = "organisation"
            self.strTypeId = self.strOrgId
            self.callWebServiceToGetResponsiblePersonListNew(isOffice: false)
            
            
        }
        //            }
    }
    
    /**
     * Load risk list when user clicks on risks field
     */
    @IBAction func btnThemesAction(_ sender: UIButton) {
        
        if arrThemes.count == 0
        {
            //load risks
            callWebServiceForListThemes()
        }
        else {
            //redirect user to risks listing screen when themes already loaded
            self.performSegue(withIdentifier: "Themes", sender: nil)
        }
    }
    
    /**
     * When status segment is clicked
     */
    @IBAction func segmentCtrlAction(_ sender: UISegmentedControl) {
        
        switch self.segmentCtrl.selectedSegmentIndex {
        case 0:
            strStatus = "NOT STARTED"
            print(strStatus)
            
        case 1:
            strStatus = "STARTED"
            print(strStatus)
            
        case 2:
            strStatus = "COMPLETED"
            print(strStatus)
            
        default:
            strStatus = "NOT STARTED"
            print(strStatus)
            
        }
    }
    
    /**
     * submit button clicked
     */
    @IBAction func btnSubmitAction(_ sender: Any) {        
        
        let myString = txtDescription.text
        let trimmedDescString = myString?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //validate the required fields
        if  (txtSelectTier.text?.isEmpty)!{
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select one of the tier option.")
            
        }
        else if  (txtResponsiblePerson.text?.isEmpty)!{
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select responsible person for respective Action.")
            
        }
        else if (txtStartDate.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Start Data.")
        }
        else if (txtDueDate.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Due Date.")
        }
            
        else if (strStatus == "") {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Status.")
        }
            
        else if (txtDescription.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Description.")
        }
        else if(trimmedDescString?.isEmpty)!{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Description.")
        }
        else {
            //validated
            
            //update or add action according to the type
            if arrOfActionToUpdate.count != 0{
                //update action
                callWebServiceToUpdateAction()
            }
            else{
                //add action
                callWebServiceToAddAction()
            }
        }
        
    }
    
    /**
     * custom function which checks the spaces in string and returns a bool accordingly
     */
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - PrepareForSegue
    /**
     prepare for segue method called when any segue is performed
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ActionTierListView" {
            //if action tier segue navigate user to action tier list
            let vc = segue.destination as! ActionTierListView
            
            vc.didFinishSelectTier = { dict in
                self.strSelectTierId = (dict as! TierModel).strId
                self.txtSelectTier.text = (dict as! TierModel).strName
                self.txtResponsible.text = ""
                self.txtResponsiblePerson.text = ""
                self.strIndividualId = ""
                self.strDepartmentId = ""
                self.strOfficeId = ""
                
                if self.txtSelectTier.text == "PRIMARY" || self.txtSelectTier.text == "SECONDARY" || self.txtSelectTier.text == "TERTIARY"  || self.txtSelectTier.text == "INDIVIDUAL" {
                    
                    //pass responsible person
                    self.heightContraintOfResponsibleModule.constant = 0
                    self.viewOfResponsibleModule.alpha = 0
                    
                    //New
                    self.strTierType = "organisation"
                    self.strTypeId = self.strOrgId
                    self.callWebServiceToGetResponsiblePersonList()
                    
                }
                    
                else if self.txtSelectTier.text == "DEPARTMENT" {
                    
                    //pass dept list
                    self.heightContraintOfResponsibleModule.constant = 75
                    self.viewOfResponsibleModule.alpha = 1
                    self.lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE DEPARTMENT"
                    self.callWebServiceToGetDepartmentAnduserList()
                    
                    /*  //New
                     self.strTierType = "department"
                     print(self.strDepartmentId)
                     self.strTypeId = self.strDepartmentId
                     self.callWebServiceToGetResponsiblePersonList()*/
                    
                }
                    
                else if self.txtSelectTier.text == "OFFICE" {
                    
                    //pass office list
                    self.lblTitleSetOfResponsibleOffOrDep.text = "RESPONSIBLE OFFICE"
                    self.heightContraintOfResponsibleModule.constant = 75
                    self.viewOfResponsibleModule.alpha = 1
                    
                    //NEW
                    /* self.strTierType = "office"
                     print(self.strOfficeId)
                     self.strTypeId = self.strOfficeId
                     self.callWebServiceToGetResponsiblePersonList()*/
                    
                }
            }
            
        }
            
        else  if segue.identifier == "ResponsiblePerson"{
            
            if AuthModel.sharedInstance.role == kSuperAdmin{
                
                if self.txtSelectTier.text == "OFFICE" {
                    self.strTierType = "office"
                    print(self.strOfficeId)
                    self.strTypeId = self.strOfficeId
                    self.callWebServiceToGetResponsiblePersonList()
                    
                }
                else if self.txtSelectTier.text == "DEPARTMENT"{
                    
                    self.strTierType = "department"
                    print(self.strDepartmentId)
                    self.strTypeId = self.strDepartmentId
                    self.callWebServiceToGetResponsiblePersonList()
                    
                }
                
            }
            let vc = segue.destination as! ActionTierListView
            vc.strSegueIdentifier = "ResponsiblePerson"
            vc.arrOfResponsiblePreson = arrRespnsiblePreseonList
            vc.didFinishSelectTier = { dict in
                
                if dict is ResponsiblePersonModel {
                    
                    self.strResponsiblePersonId = (dict as! ResponsiblePersonModel).strId
                    
                    self.txtResponsiblePerson.text = (dict as! ResponsiblePersonModel).strName
                }
                
            }
        }
            
        else if segue.identifier == "Themes" {
            let vc = segue.destination as! ActionTierListView
            vc.strSegueIdentifier = "Themes"
            vc.arrThemes = arrThemes
            vc.didFinishSelectTier = { dict in
                
                self.arrSelectedThemes = (dict as! [ThemeModel])
                if (dict as! [ThemeModel]).count == 0 {
                    self.txtThemes.text = ""
                }
                else {
                    
                    if (dict as! [ThemeModel]).count == 1 {
                        self.txtThemes.text = String((dict as! [ThemeModel])[0].strTitle)
                    }
                    else {
                        self.txtThemes.text = String((dict as! [ThemeModel]).count) + " selected"
                    }
                }
            }
        }
            
        else  if segue.identifier == "ResponsibleClick"{
            
            let vc = segue.destination as! ActionTierListView
            
            if  txtSelectTier.text == "OFFICE"{
                
                vc.arrOfSlectedTierData = arrOfficeDetail
                
            }
            else if txtSelectTier.text == "DEPARTMENT"{
                
                vc.arrOfSlectedTierData = arrOfGetDeparmentList
                
            }
            else if txtSelectTier.text == "INDIVIDUAL"{
                
                vc.arrOfSlectedTierData = arrOfGetUserList
                
            }
            vc.strSegueIdentifier = "ResponsibleClick"
            vc.strWhomeCalled = self.txtSelectTier.text!
            vc.didFinishSelectTier = { dict in
                
                //to adjust office parameters
                if dict is OfficeModel {
                    
                    self.strOfficeId = (dict as! OfficeModel).strOffice_id
                    self.txtResponsible.text = (dict as! OfficeModel).strOffice
                }
                    
                else if dict is UserModel
                {
                    self.strIndividualId = (dict as! UserModel).strId
                    self.txtResponsible.text = (dict as! UserModel).strName
                }
                else{
                    
                    self.strDepartmentId = (dict as! DepartmentModel).strId
                    self.txtResponsible.text = (dict as! DepartmentModel).strDepartment
                    
                }
                
            }
        }
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    //MARK:- UITextViewDelegates
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    //MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Custom Methods
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    //MARK: - Calling web service
    /**
     * API - get risk list
     */
    func callWebServiceForListThemes()
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            [
                "orgId"   : strOrgId
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getThemeList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse list and assign to array
                    DashboardParser.parseThemesList(response: response!, completionHandler: { (arr) in
                        
                        self.arrThemes = arr
                        if self.arrOfActionToUpdate.count != 0 {
                            for selectedValue in self.arrOfActionToUpdate[0].arrThemes {
                                for value in self.arrThemes {
                                    if selectedValue.strId == value.strId {
                                        value.isSelected = true
                                    }
                                }
                            }
                        }
                    })
                    
                    self.tblView.reloadData()
                    self.performSegue(withIdentifier: "Themes", sender: nil)
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - get user & dept list
     */
    func callWebServiceToGetDepartmentAnduserList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["orgId": strOrgId  ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getDepartmentAndUserList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse dept & user list and assign to array
                    DashboardParser.parseDepartmentUserList(response: response!,removeSelfUser: false, completionHandler: { (arrDep, arrUser) in
                        self.arrOfGetDeparmentList = arrDep
                        self.arrOfGetUserList = arrUser
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API get responsible list
     */
    func callWebServiceToGetResponsiblePersonList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*{
         
         "type":"organisation",
         "typeId":"1"
         
         }
         */
        let param =
            [
                "type"   : strTierType,
                "typeId" : strTypeId
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getUserByType, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse responsible list and ssign to array
                    DashboardParser.parseResponsiblePresonList(response: response!, completionHandler: { (arrRespnsiblePreseonList) in
                        
                        self.arrRespnsiblePreseonList = arrRespnsiblePreseonList
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - get responsible list
     */
    func callWebServiceToGetResponsiblePersonListNew(isOffice: Bool){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        
        let param =
            [
                "type"   : strTierType,
                "typeId" : strTypeId
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getUserByType, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseResponsiblePresonListForActionPage(response: response!, completionHandler: { (arrRespnsiblePreseonList) in
                        
                        self.arrRespnsiblePreseonList = arrRespnsiblePreseonList
                    })
                    
                    self.tblView.reloadData()
                    self.performSegue(withIdentifier: "ResponsiblePerson", sender: nil)
                    
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - Update action
     */
    func callWebServiceToUpdateAction(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*
         {
         "actionId":"155",
         "orgStatus": "STARTED",
         "IndividualUserId": "",
         "orgId": "146",
         "startedDate": "20-08-2018",
         "responsibleUserId": "327",
         "description": "Description",
         "userId": "1",
         "departmentId": "",
         "tierId": "4",
         "dueDate": "20-08-2018",
         "officeId": ""
         }  */
        
        var arrThemeID = [String]()
        for value in arrSelectedThemes {
            arrThemeID.append(value.strId)
        }
        
        let param =
            [
                "actionId"         : arrOfActionToUpdate[0].strId,
                "userId"           : AuthModel.sharedInstance.user_id,
                "tierId"           : strSelectTierId,
                "departmentId"     : strDepartmentId,
                "IndividualUserId" : "",
                "officeId"         : strOfficeId,
                "orgStatus"        : strStatus,
                "orgId"            : strOrgId,
                "startedDate"      : txtStartDate.text!,
                "dueDate"          : txtDueDate.text!,
                "description"      : txtDescription.text!,
                "responsibleUserId": strResponsiblePersonId,
                "themeId" : arrThemeID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.updateAction, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Action Updated successfully")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * Add action api
     */
    func callWebServiceToAddAction() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*
         {
         "userId": "2",
         "startedDate": "2018-08-02",
         "dueDate": "2018-08-08",
         "tierId": "6",
         "departmentId": "",
         "officeId": "1",
         "IndividualUserId":"",
         "orgStatus": "Not started",
         "orgId": "1",
         "description": "This sample text",
         "responsibleUserId":"2"
         }
         */
        
        var arrThemeID = [String]()
        for value in arrSelectedThemes {
            arrThemeID.append(value.strId)
        }
        
        let param =
            [
                "userId"           : AuthModel.sharedInstance.user_id,
                "tierId"           : strSelectTierId,
                "departmentId"     : strDepartmentId,
                "IndividualUserId" : "",
                "officeId"         : strOfficeId,
                "orgStatus"        : strStatus,
                "orgId"            : strOrgId,
                "startedDate"      : txtStartDate.text!,
                "dueDate"          : txtDueDate.text!,
                "description"      : txtDescription.text!,
                "responsibleUserId": strResponsiblePersonId,
                "themeId" : arrThemeID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.addAction, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Action added successfully")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}


class DateFieldAction: UITextField {
    var date: Date?
    let datePicker = UIDatePicker()
    let formatter = DateFormatter()
    override func didMoveToSuperview() {
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        formatter.dateStyle = .short
        formatter.dateFormat = "dd-MM-yyyy"
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor.white
        datePicker.backgroundColor = UIColor.white
        datePicker.tintColor = ColorCodeConstant.darkTextcolor
        toolbar.tintColor = ColorCodeConstant.darkTextcolor
        
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
        toolbar.setItems([cancel,space,done], animated: false)
        inputAccessoryView = toolbar
        inputView = datePicker
    }
    @objc func cancelAction(_ sender: UIBarButtonItem) {
        endEditing(true)
    }
    @objc func doneAction(_ sender: UIBarButtonItem) {
        date = datePicker.date
        text = formatter.string(from: datePicker.date)
        endEditing(true)
    }
}
