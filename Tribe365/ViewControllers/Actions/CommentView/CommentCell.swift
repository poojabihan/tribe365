//
//  CommentCell.swift
//  Tribe365
//
//  Created by Apple on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell is used to show comments on actions
 */
class CommentCell: UITableViewCell {
    
    //show commenter name
    @IBOutlet weak var lblCommentatorName: UILabel!
    
    //show comment date time
    @IBOutlet weak var lblCommentDateAndTime: UILabel!
    
    //show comment
    @IBOutlet weak var lblCommentText: UILabel!
    
    //commentor bg view
    @IBOutlet weak var lblViewOfCommentator: UIView!
    
    //height
    @IBOutlet weak var heightConstraintOfCommentCell: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
