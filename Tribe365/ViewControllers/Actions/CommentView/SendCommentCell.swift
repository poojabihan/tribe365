//
//  SendCommentCell.swift
//  Tribe365
//
//  Created by Apple on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * send comment box cell
 */
class SendCommentCell: UITableViewCell {
    
    //comment text
    @IBOutlet weak var txtAddComment: UITextField!
    
    //send button
    @IBOutlet weak var btnSendAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
