//
//  CommentView.swift
//  Tribe365
//
//  Created by Apple on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class shows the comment screen of particular action
 */
class CommentView: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Variable
    //action detail model
    var arrOfActionDetail = ActionModel()
    
    //comments list aarray
    var arrOfCommentList = [CommentModel]()
    
    //height
    var strHeightofcellTaskLbl = CGFloat()
    
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //height
    var strHeightofCommentcell = CGFloat()
    
    //stores comment
    var strComment = ""
    
    //MARK: - IBOutlets
    //table
    @IBOutlet weak var tblView: UITableView!
    
    //organisation logo
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        print(arrOfActionDetail)
        tblView.reloadData()
        
        //hides keyboard when user taps anywhere on screen
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //load all comments
        callWebServiceToCommentList()
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfCommentList.count == 0{
            //actions details + send comment box
            return 2
        }
        else{
            //comments list + actions details + send comment box
            return arrOfCommentList.count + 2
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0) {
            
            //custom cell which shows the actions details
            let cell : ViewActionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ViewActionsTableViewCell") as! ViewActionsTableViewCell
            
            cell.lblPST.isHidden = true
            cell.lblTaskDesciption.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblTaskDesciption.numberOfLines = 0
            
            //show all actions details
            cell.imgResponsibleIcon.image = UIImage(named:"Responsible")
            cell.lblResponsibleName.text = arrOfActionDetail.strName
            cell.lblTaskDesciption.text = arrOfActionDetail.strDescription
            cell.lblStartDate.text = arrOfActionDetail.strStartedDate
            cell.lblDueDate.text = arrOfActionDetail.strDueDate
            cell.lblResponsiblePreson.text = arrOfActionDetail.strResponsibleName
            cell.lblThemes.text = arrOfActionDetail.strThemesForDisplay
            
            //Image Check
            //set status image according to its type
            if arrOfActionDetail.strOrgStatus == "NOT STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Not Started")
                
            }
            else if arrOfActionDetail.strOrgStatus == "STARTED"{
                
                cell.imgStatus.image = UIImage(named: "Orange Started")
                
            }
                
            else if arrOfActionDetail.strOrgStatus == "COMPLETED"{
                
                cell.imgStatus.image = UIImage(named: "Green Complete")
                
            }
            
            //set height of label according to content
            cell.lblTaskDesciption.frame.size.height = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))!
            
            
            //            strHeightofcellTaskLbl = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 150
            //
            //            cell.heightConstraintOfViewToManage.constant = (cell.lblTaskDesciption.text?.height(withConstrainedWidth: cell.lblTaskDesciption.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 125
            
            //appearnace of view
            cell.viewToManage.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.viewToManage.layer.borderWidth = 0.5
            return cell
        }
        else if indexPath.row == 1{
            
            //custom view which shows the send comment box
            let cell : SendCommentCell = tableView.dequeueReusableCell(withIdentifier: "SendCommentCell") as! SendCommentCell
            
            //set target for send button action
            cell.btnSendAction.addTarget(self, action: #selector(commentAction(_:)), for: .touchUpInside)
            return cell
        }
        else{
            
            //custom cell for comment list
            let cell : CommentCell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
            
            
            /*{
             "id": 7,
             "userId": 1,
             "comment": "test",
             "created_at": "2018-07-16 06:10:15",
             "name": "Admin"
             }*/
            
            //set al data for comments
            cell.lblCommentatorName.text = arrOfCommentList[indexPath.row - 2].strName
            //cell.lblCommentDateAndTime.text = arrOfCommentList[indexPath.row - 2]["created_at"].stringValue
            cell.lblCommentText.text = arrOfCommentList[indexPath.row - 2].strComment
            
            //Convert the Time and date into desired formate
            let myDateString = arrOfCommentList[indexPath.row - 2].strCreated_at
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "MMM dd, YYYY | HH:mm a"
            let somedateString = dateFormatter.string(from: myDate)
            print(somedateString)
            
            cell.lblCommentDateAndTime.text = somedateString
            
            
            cell.lblViewOfCommentator.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.lblViewOfCommentator.layer.borderWidth = 0.5
            
            cell.lblCommentText.frame.size.height = (cell.lblCommentText.text?.height(withConstrainedWidth: cell.lblCommentText.frame.size.width, font: UIFont.systemFont(ofSize: 12)))!
            
            strHeightofCommentcell = (cell.lblCommentText.text?.height(withConstrainedWidth: cell.lblCommentText.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 55
            
            cell.heightConstraintOfCommentCell.constant = (cell.lblCommentText.text?.height(withConstrainedWidth: cell.lblCommentText.frame.size.width, font: UIFont.systemFont(ofSize: 12)))! + 55
            
            
            return cell
        }
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
            //            return strHeightofcellTaskLbl
            return UITableViewAutomaticDimension
            
        }
        if indexPath.row == 1{
            
            return 100
        }
        else{
            
            return strHeightofCommentcell
        }
    }
    
    // MARK: - IBActions
    /**
     * function which validates the comment field and calls api to post comment for the particular action
     */
    @objc func commentAction(_ sender: UIButton) {
        
        let index = IndexPath(row: 1, section: 0)
        let cell: SendCommentCell = self.tblView.cellForRow(at: index) as! SendCommentCell
        
        strComment = cell.txtAddComment.text!
        
        //validate comment text
        if  strComment == ""{
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter comment first.")
            
        }
        else{
            //add comment
            callWebServiceToAddComment()
        }
    }
    
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - Calling WebService
    /**
     * API - add comment
     */
    func callWebServiceToAddComment() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /*
         {
         "actionId":"1",
         "comment":"test"
         
         }
         */
        
        //prepare param for api
        let param =
            [
                "actionId"   : arrOfActionDetail.strId,//action id
                "comment"    : strComment//comment to add
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Comment.Addcomment, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //show success message in case of Comment added successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Comment added successfully")
                    self.strComment = ""
                    let index = IndexPath(row: 1, section: 0)
                    let cell: SendCommentCell = self.tblView.cellForRow(at: index) as! SendCommentCell
                    
                    cell.txtAddComment.text! = ""
                    
                    //refresh comment list
                    self.callWebServiceToCommentList()
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API to get all comments
     */
    func callWebServiceToCommentList() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /* {   "actionId":"1"  }   */
        
        //prepare param for api
        let param =
            [
                "actionId"   : arrOfActionDetail.strId//action id whose comments need to be loaded
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Comment.commentList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse comment list and assign to array
                    DashboardParser.parseCommentList(response: response!, completionHandler: { (arr) in
                        self.arrOfCommentList = arr
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
