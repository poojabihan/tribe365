//
//  DiagnosticView.swift
//  Tribe365
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is no longer in use.
 This was used to show two options of Diagnostics module - Diagnostics & Tribeometer
 */
class DiagnosticView: UIViewController {
    
    //MARK: -Variable
    let panel = JKNotificationPanel()
    var isDiagnosticAnsDone = Bool()
    var isTribeometerAnsDone = Bool()
    var strOrgId = ""
    var strCompanyName = ""
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if AuthModel.sharedInstance.role == kUser {
            callWebServiceTogetTribeometerQuestionList()
        }
    }
    
    //MARK: - IBAction
    /**
     * This function is called when user taps on back button.
     * This function navigates user on previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * This function is called when user taps on Tribe logo.
     * This function navigates user on home screen
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when user taps on question mark (help) button.
     * This function navigates user on help screen with support type as "tribeometer" via function openSupportScreen
     */
    @IBAction func btnTribeometerSupportAction() {
        self.openSupportScreen(strType: NetworkConstant.SupportScreensType.tribeometer)
    }
    
    /**
     * This function is called when user taps on question mark (help) button.
     * This function navigates user on help screen with support type as "diagnostic" via function openSupportScreen
     */
    @IBAction func btnDiagnosticsSupportAction() {
        self.openSupportScreen(strType: NetworkConstant.SupportScreensType.diagnostic)
    }
    
    /**
     * This function is called when user taps on question mark (help) button.
     * This function navigates user on help screen with support type as "diagnosticHome" via function openSupportScreen
     */
    @IBAction func btnDiagnosticsHomeSupportAction() {
        self.openSupportScreen(strType: NetworkConstant.SupportScreensType.diagnosticHome)
    }
    
    /**
     * This function is called when user taps on diagnostics button.
     * This function navigates user on diagnostics view either question or result view according to the different conditions
     */
    @IBAction func btnDiagnosticAction(_ sender: Any) {
        
        //If logged in as super admin - navigate to diagnostics result
        if AuthModel.sharedInstance.role == kSuperAdmin{
            //Case - Admin
            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticResultView") as! DiagnosticResultView
            objVC.strOrgId = strOrgId
            objVC.strCompanyName = strCompanyName
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else{
            
            //If logged in as end user
            if isDiagnosticAnsDone == true{
                
                //Answer already completed case - navigate to result
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticResultView") as! DiagnosticResultView
                objVC.strOrgId = strOrgId
                objVC.strCompanyName = strCompanyName
                objVC.isDiagnosticAnsDone = isDiagnosticAnsDone
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else{
                
                //Answer not completed - navigate to questions list
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
                objVC.isDiagnosticAnsDone = isDiagnosticAnsDone
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    /**
     * This function is called when user taps on tribeometer button.
     * This function navigates user on tribeometer view either question or result view according to the different conditions
     */
    @IBAction func btnTribeometerAction(_ sender: Any) {
        
        //If logged in as super admin - navigate to tribeometer result
        if AuthModel.sharedInstance.role == kSuperAdmin{
            //Case - Admin
            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerResultView") as! TribeometerResultView
            objVC.strOrgId = strOrgId
            objVC.strCompanyName = strCompanyName
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else{
            //If logged in as end user
            if isTribeometerAnsDone == true{
                
                //Answer already completed case - navigate to result
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerResultView") as! TribeometerResultView
                objVC.strOrgId = strOrgId
                objVC.isTribeometerAnsDone = isTribeometerAnsDone
                objVC.strCompanyName = strCompanyName
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else{
                
                //Answer not completed - navigate to questions list
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
                objVC.isTribeometerAnsDone = isTribeometerAnsDone
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    //MARK: - Custom Methods
    /**
     * This function is called when user taps on question mark (help) button.
     * This function navigates user on help screen with different support types
     */
    func openSupportScreen(strType: String) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = strType
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - Calling Web Service
    /**
     * This is API calling - tells whether diagnostics & tribeometer questions are completed by user or not
     */
    func callWebServiceTogetTribeometerQuestionList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Diagnostic.isDiagnosticTribeometerAnswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //Parse the response and returns two bools which tells answer complete or not
                    DiagnosticParser.parseStatusOfDiagnosticTribeometerAnswerDone(response: response!, completionHandler: { (isDiagnosticAnsDone, isTribeometerAnsDone) in
                        
                        self.isDiagnosticAnsDone = isDiagnosticAnsDone
                        self.isTribeometerAnsDone = isTribeometerAnsDone
                        
                    })
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
}
