//
//  DiagonsticCell.swift
//  Tribe365
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
* This class is used as Diagnostics questions/options table view cell
*/
class DiagonsticCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //question title
    @IBOutlet weak var lblQuestion: UILabel!
    
    //question number
    @IBOutlet weak var lblNumber: UILabel!
    
    //option label
    @IBOutlet weak var lblNotAtAll: UILabel!
    
    //option label
    @IBOutlet weak var lblSlightly: UILabel!
    
    //option label
    @IBOutlet weak var lblModeratley: UILabel!
    
    //option label
    @IBOutlet weak var lblVery: UILabel!
    
    //option label
    @IBOutlet weak var lblExtremely: UILabel!
    
    //option button
    @IBOutlet weak var btnNotAtAll: UIButton!
    
    //option button
    @IBOutlet weak var btnSlightly: UIButton!
    
    //option button
    @IBOutlet weak var btnModeratley: UIButton!
    
    //option button
    @IBOutlet weak var btnVery: UIButton!
    
    //option button
    @IBOutlet weak var btnExtremely: UIButton!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
