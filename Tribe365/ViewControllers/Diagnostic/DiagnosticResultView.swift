//
//  DiagnosticResultView.swift
//  Tribe365
//
//  Created by Apple on 20/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is to diaply result for Disgnostics data in form of bar chart
 */
class DiagnosticResultView: UIViewController, UITableViewDataSource, UITableViewDelegate, HADropDownDelegate, UITextFieldDelegate, BasicBarChart2Delegate {
    
    //MARK: - IBOutlets
    //This is the custom BarChart modified for Diagnostics (provided clicks on graph)
    @IBOutlet weak var basicBarChart: BasicBarChart2!
    
    //Used to diplay bar graph
    @IBOutlet weak var tblView: UITableView!
    
    //"No records found" label
    @IBOutlet weak var lblNoData: UILabel!
    
    //Display organisation name
    @IBOutlet weak var lblCompanyName: UILabel!
    
    //Used to display content of graph in list view
    @IBOutlet weak var tblList: UITableView!
    
    //Redo/Review question button
    @IBOutlet weak var btnViewResult: UIButton!
    
    //This is view which contains all the filters dropdown
    @IBOutlet weak var viewForDropDown: UIView!
    
    //height constraint for viewForDropDown
    @IBOutlet weak var HeightConstraintForDropDown: NSLayoutConstraint!
    
    //top constraint for viewForDropDown
    @IBOutlet weak var topConstraintforView: NSLayoutConstraint!
    
    //Custom drop down for office
    @IBOutlet weak var dropDownOffice: HADropDown!
    
    //Custom drop down for department
    @IBOutlet weak var dropDownDept: HADropDown!
    
    //Custom drop down for from date
    @IBOutlet weak var txtFromDate: UITextField!
    
    //Custom drop down for to date
    @IBOutlet weak var txtToDate: UITextField!
    
    //MARK: - Variables
    //no longer in use
    var valueArr = [String]()
    
    //stores organisation id
    var strOrgId = ""
    
    //whether user if coming from reports module
    var comingFromReports = ""
    
    //stores array of result value
    var arrOfDiagnosticResult = [DiagnosticResultModel]()
    
    //used to show error/warning
    var panel = JKNotificationPanel()
    
    //stores organisation name
    var strCompanyName = ""
    
    //stores if answers are completed or not
    var isDiagnosticAnsDone = Bool()
    
    //For all Offices and department list Api
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    
    //Arr For All Department
    var arrForDepartmentName = [DepartmentModel]()
    var arrOfAllDeparmentList = [String]()
    var arrOfAllDepartmentIdList = [String]()
    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0
    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()
    var selectedFromDate = ""
    var selectedToDate = ""
    
    //view that stores bar graph
    @IBOutlet weak var mainViewOfBarGraph: UIView!
    
    //detect if departments loaded for the first time
    var loadForFirst = false
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        //Set organisation name
        lblCompanyName.text = strCompanyName
        
        //set delegates for bar graph
        basicBarChart.delegate = self
        
        if comingFromReports == "True"{
            //if coming from reports module
            
            //show drop down filters
            HeightConstraintForDropDown.constant = 50
            
            //do not show redo/review button
            btnViewResult.isHidden = true
            
            //adjust top constraint
            topConstraintforView.constant = 15
            
            //unhide drop down view
            viewForDropDown.alpha = 1.0
            
            //get the diagnostics result for reports
            callWebServiceForDiagonsticReport()
            
            //set dropdown delegate for depeartment
            dropDownDept.delegate = self
            
            //set dropdown delegate for office
            dropDownOffice.delegate = self
            
            //load all the office and dept (this is used when we select any particular office)
            callWebServiceToGetAllOfficesAndDepartmentList()
            
            //load all global department (this is used when no office is selected)
            callWebServiceToGetAllDepartments()
        }
        else{
            //if not coming from reports module
            
            //do not show filters
            HeightConstraintForDropDown.constant = 0
            
            //no need for top constraint
            topConstraintforView.constant = 0
            
            //hide the drop down view
            viewForDropDown.alpha = 0.0
            
            //load the result values
            callWebServiceForDiagonsticResult()
        }
        
        if AuthModel.sharedInstance.role == kSuperAdmin{
            //admin case - no need for redo/review button
            btnViewResult.isHidden = true
            
        }
        else{
            //user case
            if comingFromReports == "True"{
                //no need for redo/review button when coming from reports
                btnViewResult.isHidden = true
            }
            else{
                //show redo/review button
                btnViewResult.isHidden = false
            }
        }
        
        tblList.separatorStyle = .none
    }
    
    //MARK: - UITextField Delegates
    /**
     Textfield delegate method
     */
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            //case when fromdate field is selected
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                
                //if both the dates are selected, then call api to load diagnostics result
                callWebServiceForDiagonsticReport()
            }
        }
        else {
            //case when todate field is selected
            if selectedFromDate != "" {
                
                //format fromdate
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                //format todate
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                //validation - todate should be greater then fromdate
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        
                        //if both the dates are selected & passes the validation, then call api to load diagnostics result
                        callWebServiceForDiagonsticReport()
                    }
                }
            }
            
        }
    }
    
    //MARK: - Custom Functions
    /**
     This function generates data enteries for bar graph
     */
    func generateDataEntries() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for result array
        for j in (0..<self.arrOfDiagnosticResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfDiagnosticResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfDiagnosticResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph
     */
    func setGraphData()
    {
        var arrScores = [String]()
        
        for value in arrOfDiagnosticResult {
            //prepare array of scores
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            // tempValues[0]
            var strMaxValue = getMaxValue(arr: tempValues)
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfDiagnosticResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /***
     This function returns index(s) of a value from the array
     @strValue: This stores the vaue whose index needs to be found out
     @arrTemp: This is the temperory array which stores the value(strValue)
     @[String]: Returns the index(s) of the strValue
     */
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            //if value found store it in array
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        //return the index array
        return arrIndex
    }
    
    /**
     Returns the first value which is not a balnk entry
     */
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        //if all are blank entries, then returns blank
        return ""
    }
    
    /**
     Returns the background colour according to there position
     */
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    /**
     This function creates the array to be shown in department list
     */
    func CreateDropDownListOfAllDepartemntArray(){
        
        //loop of department array with + 1 value
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                //"All" case on first index
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                //store the values as it is in array
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
        // self.dropDownDept.items = self.arrDepartmentNames
    }
    
    /**
     This function creates array for office and dept  with "All" case
     */
    func setData() {
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
            dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    //MARK: - IBAction
    /**
     Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     Redirects user to question list (Add/update mode depends whether user has complted answers or not) for diagnostics
     */
    @IBAction func btnViewResultAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
        objVC.isDiagnosticAnsDone = isDiagnosticAnsDone
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - BasicBarChart2Delegate
    /**
     BarChart Delegate Methods
     It is called when user clicks any of the bar in chart
     */
    func didSelectGraph(index: Int) {
        print("iiiiiiiiiiiii",index)
        
        //create instance of DiagnosticsSubCategoryViewController
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticsSubCategoryViewController") as! DiagnosticsSubCategoryViewController
        
        //pass selected office id
        objVC.strOfficeId = selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : ""
        
        //pass organisation id
        objVC.strOrgId = strOrgId
        
        //pass dept id
        objVC.strDeptId = selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : ""
        
        //array that stores categories names
        var arrCatNames = [String]()
        
        //loop for diagnostics result array
        for value in arrOfDiagnosticResult {
            
            //get the titles from every result
            arrCatNames.append(value.strTitle)
        }
        
        //pass category names
        objVC.arrCategoryNames = arrCatNames
        
        //pass array of results
        objVC.arrOfDiagnosticResult = arrOfDiagnosticResult
        
        //pass clicked bar id
        objVC.strCategoryId = arrOfDiagnosticResult[index].strCategoryId
        
        //pass clicked bar title
        objVC.strCatName = arrOfDiagnosticResult[index].strTitle
        
        //pass result
        objVC.arrCategories = arrOfDiagnosticResult
        
        //push controller
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    //MARK: - UITableViewDelegate and datasource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //array which stores diagnostics result
        return arrOfDiagnosticResult.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Custom cell, it returns the cell which is displayed below the bar chart
        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
        
        //Serial number
        cell.lblMarker.text = String(indexPath.row + 1) + " -"
        
        //Result category name
        cell.lblTitle.text = arrOfDiagnosticResult[indexPath.row].strTitle.capitalized
        
        //category percentage
        cell.lblScore.text = arrOfDiagnosticResult[indexPath.row].strPercentage + "%"
        
        //category background color according to there % and position in array
        cell.lblScore.backgroundColor = arrOfDiagnosticResult[indexPath.row].bgColor
        
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    //MARK: - Call web Service
    /**
     * API to get  result for diagnostics
     */
    func callWebServiceForDiagonsticResult() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "userId":"183"
         }
         */
        var param = [String : Any]()
        if AuthModel.sharedInstance.role == kUser{
            
            //case - user logins
            
            //pass logged in user organisation id
            param = ["orgId": AuthModel.sharedInstance.orgId]
        }
        else{
            //case - super admin logins
            
            //pass the selected organisation id from the previous screen(in case of Super admin, user select the organisation from the list before coming to this screen)
            param = ["orgId": strOrgId]
        }
        print("+++++: ",param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.getDiagnosticReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse disgnostics result values
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfDiagnosticResult) in
                        
                        //stores result in array of model:DiagnosticResultModel
                        self.arrOfDiagnosticResult = arrOfDiagnosticResult
                        
                    })
                    
                    if self.arrOfDiagnosticResult.count == 0{
                        //case of no result
                        
                        //do not show table
                        self.tblView.isHidden = true
                        
                        //show no record label
                        self.lblNoData.isHidden = false
                        
                        //no record text
                        self.lblNoData.text = "Diagnostic Questionnaire are not submitted by any members of organisation yet."
                    }
                    else{
                        //hide no record label
                        self.lblNoData.isHidden = true
                        
                        //show table
                        self.tblView.isHidden = false
                        
                        //remove all values
                        self.valueArr.removeAll()
                        
                        //reload table for list
                        self.tblList.reloadData()
                        
                        //generate data enteries for bar chart
                        let dataEntries = self.generateDataEntries()
                        
                        //assign he generated data enteries to third party graph object
                        self.basicBarChart.dataEntries = dataEntries
                    }
                    
                    //set up graph data
                    self.setGraphData()
                    
                }
                    
                else {
                    //when no response
                    
                    //hide tableview
                    self.tblView.isHidden = true
                    
                    //show no reord label
                    self.lblNoData.isHidden = false
                    
                    //no record text
                    self.lblNoData.text = "Diagnostic Questionnaire are not submitted by any members of organisation yet."
                    
                    //show error message
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API to get diagnostics result for reports
     */
    func callWebServiceForDiagonsticReport(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            
            param = [ "orgId": strOrgId,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
            
        }
        else{
            param = [ "orgId": strOrgId,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
        }
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getDiagnosticReportForGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse result
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfDiagnosticResult) in
                        
                        //store it in array of model named:DiagnosticResultModel
                        self.arrOfDiagnosticResult = arrOfDiagnosticResult
                        
                    })
                    if self.arrOfDiagnosticResult.count == 0{
                        //case of no result
                        
                        //hide bar chart
                        self.mainViewOfBarGraph.isHidden = true
                        
                        //show no data label
                        self.lblNoData.isHidden = false
                        
                        //remove all values
                        self.valueArr.removeAll()
                        
                        //text as blank
                        self.lblNoData.text = ""
                        
                        //load tablelist
                        self.tblList.reloadData()
                        
                        //empty the result array
                        self.arrOfDiagnosticResult.removeAll()
                    }
                    else{
                        //hide no data label
                        self.lblNoData.isHidden = true
                        
                        //show list view
                        self.tblView.isHidden = false
                        
                        //show bar chart view
                        self.mainViewOfBarGraph.isHidden = false
                        
                        //remove values
                        self.valueArr.removeAll()
                        
                        //loaf list view
                        self.tblList.reloadData()
                        
                        //generate enteries for bar graph
                        let dataEntries = self.generateDataEntries()
                        
                        //assign enteries to the thirt party graph
                        self.basicBarChart.dataEntries = dataEntries
                    }
                    
                    //set up graph data
                    self.setGraphData()
                    
                }
                else {
                    //case of no response
                    
                    //hide bar chart view
                    self.mainViewOfBarGraph.isHidden = true
                    
                    //show no data label
                    self.lblNoData.isHidden = false
                    
                    //text
                    self.lblNoData.text = ""
                    
                    //empty values
                    self.valueArr.removeAll()
                    
                    //load list view
                    self.tblList.reloadData()
                    
                    //empty result array
                    self.arrOfDiagnosticResult.removeAll()
                    
                    //show error message
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - To get all office along with there respective departments
     */
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgId] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse office and departments
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        
                        //store office array
                        self.arrOffices = arrOffice
                        
                        //store dept array
                        self.arrDepartment = arrDept
                        
                        //store all global array's of dept independent of office
                        self.arrAllDepartment = arrDept
                        
                        //store all office names for drop down
                        self.arrOfficesName = arrOfcNames
                        
                        //store all dept names for drop down
                        self.arrDepartmentNames = arrDeptNames
                        
                        //store all global dep names for drop down
                        self.arrAllDepartmentNames = arrDeptNames
                        
                        //create array with "All" functionality
                        self.setData()
                    })
                }
                else {
                    
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - To get all depts
     */
    func callWebServiceToGetAllDepartments() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse depts
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        
                        //store depts names for drop downs
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if self.loadForFirst == false{
                        //if first time load case
                        self.loadForFirst = true
                        
                        //create array for all global depts along with "All" feature
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    
                    //show error message when no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
        
    //MARK: - HADropDown Delegate Method
    /**
     Drop Down delegate methods
     This function is called when user selects any value from dropdown
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        //selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            //case when user selects office
            
            //reset dept index to 0
            selectedIndexOfDepartment = 0
            
            //reset office index to 0
            selectedIndexOfOffice = 0
            
            //reset dept drop down title
            dropDownDept.title = "ALL DEPARTMENTS"
            
            //set office index
            selectedIndexOfOffice = index
            if index != 0 {
                //case other than "ALL"
                
                //set depts according to the selected office
                arrDepartment = arrOffices[index - 1].arrDepartment
                
                //set depts names according to selected office
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                //'All' case
                if dropDownOffice.title == "ALL OFFICES"
                {
                    //set all depts
                    dropDownDept.items = arrOfAllDeparmentList
                }
                // arrDepartment = arrAllDepartment
                //arrDepartmentNames = arrAllDepartmentNames
            }
            
            //set data
            setData()
        }
        else if dropDown == dropDownDept {
            //case when user selects any dept
            
            //set selected dept index
            selectedIndexOfDepartment = index
        }
        
        //load data for the filters applied
        callWebServiceForDiagonsticReport()
    }
    
}
