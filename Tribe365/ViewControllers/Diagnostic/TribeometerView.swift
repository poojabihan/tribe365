//
//  TribeometerView.swift
//  Tribe365
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This is used to show questions for tribeometer module
 */
class TribeometerView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //show error/warning message
    var panel = JKNotificationPanel()
    
    //store questions list
    var arrOfQuestionsList = [TriboemeterQuestionModel]()
    
    //store questions with answers list
    var arrOfUpdateList = [TriboemeterQuestionModel]()
    
    //answers array to send to api
    var arrOfAnswers = [[String : Any]]()
    
    //check if tribeometer ans done or not
    var isTribeometerAnsDone = Bool()
    
    //helps to know if user has made any changes in answers, so that we can show save popup
    var checkVariable = ""
    
    //check if answers are saved before
    var isSaved = false
    
    //stores saved answers array
    var arrOfSavedAnswers = [[String : Any]]()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        if isTribeometerAnsDone == true {
            //case - If tribeometer answers already completed
            
            //get the answers for tribeometer
            callWebServiceTogetAnswerdOptions()
            
            //update header
            lblHeader.text = "UPDATE TRIBEOMETER QUESTIONS"
            
            //update button
            btnSubmit.setTitle("UPDATE", for: .normal)
            
        }
        else if isTribeometerAnsDone == false {
            //case - If tribeometer answers not completed
            
            isSaved = (UserDefaults.standard.value(forKey: "isSavedTribeometer") != nil)
            //if answers previously saved
            if isSaved == true {
                arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForTribeometer") as! [[String : Any]]
                print(arrOfSavedAnswers)
            }
            
            //get questions list for tribeometer
            callWebServiceTogetTribeometerQuestionList()
        }
    }
    
    //MARK: - IBAction
    /**
     * This function is called when we click on tribe logo
     * This function sends user to home screen
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when we user clicks on tribe logo
     * This function sends user to home screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        //is user has changed anything so show him save popup
        if checkVariable == "filled One"{
            let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                UIAlertAction in
                //restore saved answers
                self.saveAnswers()
                
                NSLog("Save")
            }
            
            alertController.addAction(okAction)
            alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                //run your function here
                
                //go back without saving
                self.goToBackScreen()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            //go back
            goToBackScreen()
        }
    }
    
    /**
     * This function is called when we user clicks on submit button
     */
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        //update case
        if isTribeometerAnsDone == true{
            //first remove all previously saved answers
            arrOfAnswers.removeAll()
            //loop for the updated answers list
            for j in (0..<self.arrOfUpdateList.count){
                
                //check if the question is updated or not
                if arrOfUpdateList[j].selectedIndex != -1 {
                    //case when user has updated the answer for this question
                    
                    //get the answer id
                    let strAnsId =  self.arrOfUpdateList[j].strAnswerId
                    print(strAnsId)
                    
                    //answer id to pass in api
                    let strQuestionId = self.arrOfUpdateList[j].strAPIAnswerID
                    print(strQuestionId)
                    
                    let json = [  "answerId": strQuestionId,
                                  "optionId": strAnsId]
                    
                    //create json to pass in api
                    arrOfAnswers.append(json)
                }
                else{
                    //validation - all questions are manadatory
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            print(arrOfAnswers)
            //call api to update answers
            callWebServiceToUpdateAnswer()
            
        }
        else{
            arrOfAnswers.removeAll()
            //loop with questions list
            for j in (0..<self.arrOfQuestionsList.count){
                
                //check if user has selected any option
                if arrOfQuestionsList[j].selectedIndex != -1 {
                    //case when user has given answer for this question
                    
                    //get the answer id
                    let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                    print(strAnsId)
                    
                    //get question id
                    let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                    print(strQuestionId)
                    
                    let json = [  "questionId": strQuestionId,
                                  "optionId": strAnsId]
                    
                    //create a json to pass in api
                    arrOfAnswers.append(json)
                }
                else{
                    
                    //validation - all questions are mandatory
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            print(arrOfAnswers)
            
            //call api to add answers
            callWebServiceToAddAnswers()
        }
    }
    
    //MARK: -  Custom Functions
    /**
     *  This function is send the user to previous screen
     */
    func goToBackScreen(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     *  This function restores the previously saved answers
     */
    func saveAnswers(){
        
        //FIRST remove previous saved answers annd add new
        UserDefaults.standard.removeObject(forKey: "SavedDictForTribeometer")
        let keyValue = UserDefaults.standard.value(forKey: "SavedDictForTribeometer")
        print("Key Value after remove \(String(describing: keyValue))")
        
        //SECOND create new data of answer
        arrOfSavedAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
            
            //  if arrOfQuestionsList[j].selectedIndex != -1 {
            
            let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
            
            print(strAnsId)
            
            let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
            print(strQuestionId)
            
            let json = [  "questionId": strQuestionId,
                          "optionId": strAnsId]
            
            print(json)
            arrOfSavedAnswers.append(json)
            //  }
        }
        
        //Third add new answer list
        UserDefaults.standard.set(true, forKey: "isSavedTribeometer")
        UserDefaults.standard.set(arrOfSavedAnswers, forKey: "SavedDictForTribeometer")
        
        //Fourth print answer list
        arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForTribeometer") as! [[String : Any]]
        print(arrOfSavedAnswers)
        
        //go back to previous controller
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     *  This function updates the question list with the previously saved answers
     */
    func SetSelctedIndexAndAnwerIdForSavedData()  {
        if self.isSaved == true{
            
            for i in (0..<self.arrOfQuestionsList.count){
                for j in (0..<self.arrOfSavedAnswers.count){
                    
                    if self.arrOfSavedAnswers[j]["questionId"] as! String == self.arrOfQuestionsList[i].strQuestionId{
                        
                        if self.arrOfSavedAnswers[i]["optionId"] as! String != ""{
                            self.arrOfQuestionsList[j].selectedIndex = i
                        }
                        
                        if self.arrOfSavedAnswers[j]["optionId"] as! String == "1"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "1"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "2"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "2"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "3"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "3"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "4"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "4"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == ""{
                            
                            self.arrOfQuestionsList[i].selectedIndex = -1
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - TableView data source and delegate
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isTribeometerAnsDone == true {
            //update case use arrOfUpdateList - stores answers along with questions
            return arrOfUpdateList.count
        }
        else{
            //submit(first time) case use arrOfQuestionsList - stores only questions
            return arrOfQuestionsList.count
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell which displays questions & its options
        let cell : TribeometerCell = tableView.dequeueReusableCell(withIdentifier: "TribeometerCell") as! TribeometerCell
        
        if isTribeometerAnsDone == true {
            //update case
            
            //assigns question number
            cell.lblNumber.text = String(indexPath.row + 1)
            //assigns question name
            cell.lblQuestion.text = arrOfUpdateList[indexPath.row].strQuestionName
            
            //assign tags to all the option buttons so that we can detect which row buttons are clicked
            cell.btnNone.tag = indexPath.row
            cell.btnLow.tag = indexPath.row
            cell.btnMedium.tag = indexPath.row
            cell.btnHigh.tag = indexPath.row
            
            //Assign text to all option buttons
            cell.lblNone.text = arrOfUpdateList[indexPath.row].arrOfOptions[0].strOptionName.uppercased()
            cell.lblLow.text = arrOfUpdateList[indexPath.row].arrOfOptions[1].strOptionName.uppercased()
            cell.lblMedium.text = arrOfUpdateList[indexPath.row].arrOfOptions[2].strOptionName.uppercased()
            cell.lblHigh.text = arrOfUpdateList[indexPath.row].arrOfOptions[3].strOptionName.uppercased()
            
            //add target to every option button in order to perform action on there click
            cell.btnNone.addTarget(self, action: #selector(SelectedOption1(_:)), for: .touchUpInside)
            cell.btnLow.addTarget(self, action: #selector(SelectedOption2(_:)), for: .touchUpInside)
            cell.btnMedium.addTarget(self, action: #selector(SelectedOption3(_:)), for: .touchUpInside)
            cell.btnHigh.addTarget(self, action: #selector(SelectedOption4(_:)), for: .touchUpInside)
            
            //Below are the cases when which are checked according to api response
            //case when first option is selected - mark selected option as red
            if arrOfUpdateList[indexPath.row].arrOfOptions[0].isChecked == true{
                
                cell.lblNone.textColor = UIColor.white
                cell.btnNone.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                cell.btnLow.backgroundColor = UIColor.clear
                
                cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                cell.btnMedium.backgroundColor = UIColor.clear
                
                cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                cell.btnHigh.backgroundColor = UIColor.clear
                
            }
                //case when second option is selected - mark selected option as red
            else if arrOfUpdateList[indexPath.row].arrOfOptions[1].isChecked == true{
                
                cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNone.backgroundColor = UIColor.clear
                
                cell.lblLow.textColor = UIColor.white
                cell.btnLow.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                cell.btnMedium.backgroundColor = UIColor.clear
                
                cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                cell.btnHigh.backgroundColor = UIColor.clear
                
            }
                //case when third option is selected - mark selected option as red
            else if arrOfUpdateList[indexPath.row].arrOfOptions[2].isChecked == true{
                
                cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNone.backgroundColor = UIColor.clear
                
                cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                cell.btnLow.backgroundColor = UIColor.clear
                
                cell.lblMedium.textColor = UIColor.white
                cell.btnMedium.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                cell.btnHigh.backgroundColor = UIColor.clear
                
            }
                //case when fourth option is selected - mark selected option as red
            else if arrOfUpdateList[indexPath.row].arrOfOptions[3].isChecked == true{
                
                cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNone.backgroundColor = UIColor.clear
                
                cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                cell.btnLow.backgroundColor = UIColor.clear
                
                cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                cell.btnMedium.backgroundColor = UIColor.clear
                
                cell.lblHigh.textColor = UIColor.white
                cell.btnHigh.backgroundColor = ColorCodeConstant.selectedOptionColor
            }
            
            //Below are the cases which are checked when user changes any option
            if arrOfUpdateList[indexPath.row].selectedIndex  == indexPath.row{
                
                //case when user selects first option
                if arrOfUpdateList[indexPath.row].strAnswerId == "1" {
                    
                    cell.lblNone.textColor = UIColor.white
                    cell.btnNone.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnLow.backgroundColor = UIColor.clear
                    
                    cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnMedium.backgroundColor = UIColor.clear
                    
                    cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnHigh.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects second option
                else if arrOfUpdateList[indexPath.row].strAnswerId == "2"{
                    
                    cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNone.backgroundColor = UIColor.clear
                    
                    cell.lblLow.textColor = UIColor.white
                    cell.btnLow.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnMedium.backgroundColor = UIColor.clear
                    
                    cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnHigh.backgroundColor = UIColor.clear
                }
                    //case when user selects third option
                else if arrOfUpdateList[indexPath.row].strAnswerId == "3"{
                    
                    cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNone.backgroundColor = UIColor.clear
                    
                    cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnLow.backgroundColor = UIColor.clear
                    
                    cell.lblMedium.textColor = UIColor.white
                    cell.btnMedium.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnHigh.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects fourth option
                else if arrOfUpdateList[indexPath.row].strAnswerId == "4"{
                    
                    cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNone.backgroundColor = UIColor.clear
                    
                    cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnLow.backgroundColor = UIColor.clear
                    
                    cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnMedium.backgroundColor = UIColor.clear
                    
                    cell.lblHigh.textColor = UIColor.white
                    cell.btnHigh.backgroundColor = ColorCodeConstant.selectedOptionColor
                }
            }
        }
        else{
            //submit case
            
            //question number
            cell.lblNumber.text = String(indexPath.row + 1)
            //question title
            cell.lblQuestion.text = arrOfQuestionsList[indexPath.row].strQuestionName
            
            //set the options text
            cell.lblNone.text = arrOfQuestionsList[indexPath.row].arrOfOptions[0].strOptionName.uppercased()
            cell.lblLow.text = arrOfQuestionsList[indexPath.row].arrOfOptions[1].strOptionName.uppercased()
            cell.lblMedium.text = arrOfQuestionsList[indexPath.row].arrOfOptions[2].strOptionName.uppercased()
            cell.lblHigh.text = arrOfQuestionsList[indexPath.row].arrOfOptions[3].strOptionName.uppercased()
            
            //assign tags to all the option buttons so that we can detect which row buttons are clicked
            cell.btnNone.tag = indexPath.row
            cell.btnLow.tag = indexPath.row
            cell.btnMedium.tag = indexPath.row
            cell.btnHigh.tag = indexPath.row
            
            //add target to every option button in order to perform action on there click
            cell.btnNone.addTarget(self, action: #selector(SelectedOption1(_:)), for: .touchUpInside)
            cell.btnLow.addTarget(self, action: #selector(SelectedOption2(_:)), for: .touchUpInside)
            cell.btnMedium.addTarget(self, action: #selector(SelectedOption3(_:)), for: .touchUpInside)
            cell.btnHigh.addTarget(self, action: #selector(SelectedOption4(_:)), for: .touchUpInside)
            
            //Check For saved Answers
            if arrOfSavedAnswers.count != 0 {
                
                print(arrOfSavedAnswers[indexPath.row]["questionId"] as! String , arrOfQuestionsList[indexPath.row].strQuestionId )
                
                if arrOfSavedAnswers[indexPath.row]["questionId"] as! String == arrOfQuestionsList[indexPath.row].strQuestionId {
                    
                    print("Success")
                    
                    //set option as first - mark selected as red
                    if arrOfSavedAnswers[indexPath.row]["optionId"] as! String  == "1"{
                        
                        cell.lblNone.textColor = UIColor.white
                        cell.btnNone.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "2"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = UIColor.white
                        cell.btnLow.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as third - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "3"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = UIColor.white
                        cell.btnMedium.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "4"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = UIColor.white
                        cell.btnHigh.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                    }
                        //set no option selected
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "" {
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                    }
                }
                
                //for new selected
                if arrOfQuestionsList[indexPath.row].selectedIndex  == indexPath.row{
                    
                    //set option as first - mark selected as red
                    if arrOfQuestionsList[indexPath.row].strAnswerId == "1" {
                        
                        cell.lblNone.textColor = UIColor.white
                        cell.btnNone.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "2"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = UIColor.white
                        cell.btnLow.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                    }
                        //set option as third - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "3"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = UIColor.white
                        cell.btnMedium.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "4"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = UIColor.white
                        cell.btnHigh.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                    }
                }
            }
            else{
                //when no saved answers
                if arrOfQuestionsList[indexPath.row].selectedIndex  == indexPath.row{
                    
                    //set option as first - mark selected as red
                    if arrOfQuestionsList[indexPath.row].strAnswerId == "1" {
                        
                        cell.lblNone.textColor = UIColor.white
                        cell.btnNone.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "2"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = UIColor.white
                        cell.btnLow.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                    }
                        //set option as third - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "3"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = UIColor.white
                        cell.btnMedium.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnHigh.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "4"{
                        
                        cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNone.backgroundColor = UIColor.clear
                        
                        cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnLow.backgroundColor = UIColor.clear
                        
                        cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnMedium.backgroundColor = UIColor.clear
                        
                        cell.lblHigh.textColor = UIColor.white
                        cell.btnHigh.backgroundColor = ColorCodeConstant.selectedOptionColor
                    }
                }
                else{
                    
                    cell.lblNone.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNone.backgroundColor = UIColor.clear
                    
                    cell.lblLow.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnLow.backgroundColor = UIColor.clear
                    
                    cell.lblMedium.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnMedium.backgroundColor = UIColor.clear
                    
                    cell.lblHigh.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnHigh.backgroundColor = UIColor.clear
                    
                }
            }
        }
        return cell
    }
    
    /**
     * Tableview delegate method used to return estimated height of row - this is the minimum height of the row
     */
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    /**
     * Tableview delegate method used to return  height of row - here we have kept it as auto adjustable
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: - cellButtonAction
    /*
     1 --> None
     2 --> Low
     3 --> Medium
     4 --> High*/
    /**
     This function is called when first option is clicked
     */
    @objc func SelectedOption1(_ sender: UIButton) {
        if isTribeometerAnsDone == true{
            //case when user is updating answers
            arrOfUpdateList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateList[sender.tag].strAnswerId = "1"
            tblView.reloadData()
            
        }
        else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "1"
            tblView.reloadData()
            print("btnNone")
        }
    }
    
    /**
     This function is called when second option is clicked
     */
    @objc func SelectedOption2(_ sender: UIButton) {
        if isTribeometerAnsDone == true{
            //case when user is updating answers
            arrOfUpdateList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateList[sender.tag].strAnswerId = "2"
            tblView.reloadData()
            
        }
        else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "2"
            tblView.reloadData()
            print("btnLow")
        }
    }
    
    /**
     This function is called when third option is clicked
     */
    @objc func SelectedOption3(_ sender: UIButton) {
        if isTribeometerAnsDone == true{
            //case when user is updating answers
            arrOfUpdateList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateList[sender.tag].strAnswerId = "3"
            tblView.reloadData()
            
        }
        else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "3"
            tblView.reloadData()
            print("btnMedium")
        }
    }
    
    /**
     This function is called when fourth option is clicked
     */
    @objc func SelectedOption4(_ sender: UIButton) {
        if isTribeometerAnsDone == true{
            //case when user is updating answers
            arrOfUpdateList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateList[sender.tag].strAnswerId = "4"
            tblView.reloadData()
            
        }
        else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "4"
            tblView.reloadData()
            print("btnHigh")
        }
    }
    
    //MARK: - call web service
    /**
     API call - when user saves tribeometer answers
     */
    func callWebServiceToAddAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            //array of answers as json dict
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.addTribeometerAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer added succeessfully.")
                    
                    //remove saved answers keys which tells users has previously saved answers
                    UserDefaults.standard.removeObject(forKey: "SavedDictForTribeometer")
                    UserDefaults.standard.removeObject(forKey: "isSavedTribeometer")
                    
                    let keyValue = UserDefaults.standard.value(forKey: "SavedDictForTribeometer")
                    print("Key Value after remove \(String(describing: keyValue))")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - get tribeometer questions to list
     */
    func callWebServiceTogetTribeometerQuestionList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Diagnostic.getTribeometerQuestionList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse question list and return array of model named: TriboemeterQuestionModel
                    DiagnosticParser.parseTribeometerQuestionList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfQuestionsList = arrOfQuestionList
                    })
                    
                    //update the questions according to the previouly saved answers
                    self.SetSelctedIndexAndAnwerIdForSavedData()
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - get tribeometer answers list
     */
    func callWebServiceTogetAnswerdOptions(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Diagnostic.getTribeometerCompletedAnswers, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse diagnostics answers and returns answers list
                    DiagnosticParser.parseTribeometerGetAnsweredList(response: response!, completionHandler: { (arrOfAnswerdListQuestionList) in
                        
                        self.arrOfUpdateList = arrOfAnswerdListQuestionList
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - update the diagnostics answers
     */
    func callWebServiceToUpdateAnswer(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers//answers list as json array
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.updateTribeometerAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer updated succeessfully.")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
