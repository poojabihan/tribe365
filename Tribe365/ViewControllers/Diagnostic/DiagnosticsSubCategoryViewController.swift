//
//  DiagnosticsSubCategoryViewController.swift
//  Tribe365
//
//  Created by Apple on 25/07/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is to diaply result for Disgnostics subcategory data in form of bar chart (When diagnostics bar is clicked)
 */
class DiagnosticsSubCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, HADropDownDelegate, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    //This is the custom BarChart modified for diagnostics subcategopry
    @IBOutlet weak var basicBarChart: BasicBarChart2!
    
    //used to displat bar chart
    @IBOutlet weak var tblView: UITableView!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    //organisation name
    @IBOutlet weak var lblCompanyName: UILabel!
    
    //used to display list of result below bar graph
    @IBOutlet weak var tblList: UITableView!
    
    //This is view which contains all the filters dropdown
    @IBOutlet weak var viewForDropDown: UIView!
    
    //height constraint for viewForDropDown
    @IBOutlet weak var HeightConstraintForDropDown: NSLayoutConstraint!
    
    //top constraint for viewForDropDown
    @IBOutlet weak var topConstraintforView: NSLayoutConstraint!
    
    //Custom drop down for Categaories(values of bars in diagnostics)
    @IBOutlet weak var dropDownCategory: HADropDown!
    
    //view that stores bar graph
    @IBOutlet weak var mainViewOfBarGraph: UIView!
    
    //MARK: - Variables
    var arrCategories = [DiagnosticResultModel]()
    var arrCategoryNames = [String]()
    
    var strOrgId = ""
    var strOfficeId = ""
    var strDeptId = ""
    var strCategoryId = ""
    var arrOfDiagnosticResult = [DiagnosticResultModel]()
    var panel = JKNotificationPanel()
    var strCatName = ""
    
    //Arr For All Department
    var selectedIndexOfCategory = 0
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //selected category name as heading
        lblCompanyName.text = strCatName
        
        //custom drop down for category names
        dropDownCategory.items = arrCategoryNames
        
        //custom drop down title as selectec category name
        dropDownCategory.title = strCatName
        
        //set delegate of drop down
        dropDownCategory.delegate = self
        
        //load subcategories of selected diagnostic value
        callWebServiceForSubCategory()
        
        tblList.separatorStyle = .none
    }
    
    //MARK: - Custom Function
    /**
     This function generates data enteries for bar graph
     */
    func generateDataEntries() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for result array
        for j in (0..<self.arrOfDiagnosticResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfDiagnosticResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfDiagnosticResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph
     */
    func setGraphData()
    {
        var arrScores = [String]()
        
        for value in arrOfDiagnosticResult {
            //prepare array of scores
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfDiagnosticResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /***
     This function returns index(s) of a value from the array
     @strValue: This stores the vaue whose index needs to be found out
     @arrTemp: This is the temperory array which stores the value(strValue)
     @[String]: Returns the index(s) of the strValue
     */
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            //if value found store it in array
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        //return the index array
        return arrIndex
    }
    
    /**
     Returns the first value which is not a balnk entry
     */
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        //if all are blank entries, then returns blank
        return ""
    }
    
    /**
     Returns the background colour according to there position
     */
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    
    //MARK: - IBAction
    /**
     Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITableViewDelegate and datasource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOfDiagnosticResult.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Custom cell, it returns the cell which is displayed below the bar chart
        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
        
        //Serial number
        cell.lblMarker.text = String(indexPath.row + 1) + " -"
        
        //Result sub category name
        cell.lblTitle.text = arrOfDiagnosticResult[indexPath.row].strTitle.capitalized
        
        //sub category percentage
        cell.lblScore.text = arrOfDiagnosticResult[indexPath.row].strPercentage + "%"
        
        //sub category background color according to there % and position in array
        cell.lblScore.backgroundColor = arrOfDiagnosticResult[indexPath.row].bgColor
        
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    //MARK: - WebService Methods
    /**
     * API to get  result for fib categories of diagnostics
     */
    func callWebServiceForSubCategory(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = [ "orgId": strOrgId,
                      "officeId": strOfficeId,
                      "departmentId": strDeptId,
                      "categoryId": strCategoryId]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getDiagnsticReportSubGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse sub category result values
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfDiagnosticResult) in
                        
                        //stores result in array of model:DiagnosticResultModel
                        self.arrOfDiagnosticResult = arrOfDiagnosticResult
                        
                    })
                    if self.arrOfDiagnosticResult.count == 0{
                        //case of no result
                        
                        //hide the graph view
                        self.mainViewOfBarGraph.isHidden = true
                        
                        //show no data label
                        self.lblNoData.isHidden = false
                        
                        //text for no data label
                        self.lblNoData.text = ""
                        
                        //load list view
                        self.tblList.reloadData()
                        
                        //empty array of results
                        self.arrOfDiagnosticResult.removeAll()
                    }
                    else{
                        //hide no data label
                        self.lblNoData.isHidden = true
                        
                        //show tableview
                        self.tblView.isHidden = false
                        
                        //show graph view
                        self.mainViewOfBarGraph.isHidden = false
                        
                        //loaf list view
                        self.tblList.reloadData()
                        
                        //generate data enteries for bar chart
                        let dataEntries = self.generateDataEntries()
                        
                        //assign he generated data enteries to third party graph object
                        self.basicBarChart.dataEntries = dataEntries
                    }
                    self.setGraphData()
                    
                }
                    
                else {
                    //when no response
                    
                    //hide graoh view
                    self.mainViewOfBarGraph.isHidden = true
                    
                    //show no data label
                    self.lblNoData.isHidden = false
                    
                    //no data text
                    self.lblNoData.text = ""
                    
                    //load list view
                    self.tblList.reloadData()
                    
                    //empty resuly array
                    self.arrOfDiagnosticResult.removeAll()
                    
                    //show error message
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - HADropDown Delegate Method
    /**
     Drop Down delegate methods
     This function is called when user selects any value from dropdown
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        //selectedIndexOfOffice = 0
        //set selected sub category index
        selectedIndexOfCategory = index
        
        //set selected sub category id
        strCategoryId = arrCategories[index].strCategoryId
        print("Item selected at index \(index)")
        
        //set selected category name
        lblCompanyName.text = arrCategoryNames[index]
        
        //load data for the filters applied
        callWebServiceForSubCategory()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
