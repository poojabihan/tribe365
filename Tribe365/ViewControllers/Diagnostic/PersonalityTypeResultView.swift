//
//  PersonalityTypeResultView.swift
//  Tribe365
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import SwiftyJSON
import Charts

/**
 * This class is used to display result for Personality type
 */
class PersonalityTypeResultView: UIViewController, UITableViewDelegate, UITableViewDataSource, BasicBarChartPeronalityTypeDelegate {
    
    //MARK: - IBoutlets
    //used to display graph
    @IBOutlet var tblView: UITableView!
    
    // MARK: - Variables
    //used to diplay error/warning
    let panel = JKNotificationPanel()
    
    //stores result values
    var arrOfPersonalityTypeResult = [DiagnosticResultModel]()
    
    //array of Model BarEntry(third party) for personality type
    var dataEntriesPersonalityType = [BarEntry]()
    
    //checks if data needs to be shown for personality type or its sub category
    var isPersonalityTypeSubCategory = false
    
    //stores result for sub category of personality type
    var arrOfSubCategoryPersonalityTypeResult = [DiagnosticResultModel]()
    
    //array of Model BarEntry(third party) for sub category
    var dataEntriesSubCategoryPersonalityType = [BarEntry]()
    var isPersonailtyTypeAnswered = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //call web service for personality type result
        
        //load the result for personality type
        callWebServiceForPersonalityTypeResult()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    /**
     *This function is called when redo/review button for personality type questions is clicked
     */
    @objc func btnRedoReviewPersonalityTypeAction(_ sender: UIButton) {
        
        //        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
        //
        //        if sender.tag != 70 {
        //            objVC.comingFromReduReviewBtn = true
        //        }
        //        self.navigationController?.pushViewController(objVC, animated: true)
        
        //create instance of a class PersonalityTypeClickView(it displays the questions for personality type)
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
        
        //pass whether answers are already completed or not
        objVC.isPersonalityTypeAnsDone = isPersonailtyTypeAnswered
        
        //push to PersonalityTypeClickView view controller
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     *Returns the background colour according to there position
     */
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        case 6:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.4)
        case 7:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.3)
        case 8:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.2)
        case 9:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.15)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    /**
     Returns the first value which is not a balnk entry
     */
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        return ""
    }
    
    /***
     This function returns index(s) of a value from the array
     @strValue: This stores the vaue whose index needs to be found out
     @arrTemp: This is the temperory array which stores the value(strValue)
     @[String]: Returns the index(s) of the strValue
     */
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        return arrIndex
    }
    
    /**
     This function generates data enteries for bar graph
     */
    func generateDataEntriesPersonalitytype() -> [BarEntry] {
        
        var result: [BarEntry] = []
        
        for j in (0..<self.arrOfPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfPersonalityTypeResult[j].strPercentage)! / 100.0
            
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph for personality type
     */
    func setGraphDataPersonalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfPersonalityTypeResult {
            //prepare array of scores
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph for personality type sub category
     */
    func setGraphDataSubCategoryPeronalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfSubCategoryPersonalityTypeResult {
            //prepare array of scores
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfSubCategoryPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     *This function generates data enteries for bar graph of personality type sub categories
     */
    func generateDataEntriesSubCategoryPersonalityType() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for result array
        for j in (0..<self.arrOfSubCategoryPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfSubCategoryPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfSubCategoryPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data enteries for bar graph of personality type
     */
    func generateDataEntries() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for result array
        for j in (0..<self.arrOfPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph for personality type
     */
    func setGraphData()
    {
        var arrScores = [String]()
        
        for value in arrOfPersonalityTypeResult {
            //prepare array of scores
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    // MARK: - IBActions
    /**
     redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     *sets graph back to original values from sub category values
     */
    @objc func btnBackToPersonalityTypeGraphAction(_ sender: UIButton) {
        isPersonalityTypeSubCategory = false
        
        self.tblView.reloadData()
    }
    
    //MARK: - BasicBarChartPeronalityTypeDelegate
    /**
     BarChart Delegate Methods
     It is called when user clicks any of the bar in personality type chart
     */
    func didSelectGraphPerosnalityType(index: Int) {
        print("iiiiiiiiiiiii",index)
        
        isPersonalityTypeSubCategory = true
        callWebServiceForSubCategoryPersonalityType(catID: arrOfPersonalityTypeResult[index].strCategoryId)
    }
    
    //MARK: - UITableViewDelegate and dataSource
    /**
     * Tableview delegate method used to return number of sections
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isPersonalityTypeSubCategory {
            //case of sub category
            //returns array of result + redo/review button cell + graph cell
            return arrOfSubCategoryPersonalityTypeResult.count + 2
        }
        else {
            //case of personality type result
            //returns array of result + redo/review button cell + graph cell
            return arrOfPersonalityTypeResult.count + 2
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isPersonalityTypeSubCategory {
            //case of sub category
            if indexPath.row == 0 {
                //Show Custom cell for graph on first index
                let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                
                //assign data enteries for sub category
                cell.basicBarChartForPersonalityType.dataEntries = dataEntriesSubCategoryPersonalityType
                
                //set delegate
                cell.basicBarChartForPersonalityType.delegate = nil
                cell.selectionStyle = .none
                
                //show back button
                cell.btnBack.isHidden = false
                
                //assign targer to back button
                cell.btnBack.addTarget(self, action: #selector(btnBackToPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                return cell
            }
            else if indexPath.row == arrOfSubCategoryPersonalityTypeResult.count + 1 {
                //Show Custom cell for redo/review question on last index
                let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                cell?.selectionStyle = .none
                
                //assign tag to button
                let btn = cell?.viewWithTag(70) as! UIButton
                
                //assign target(action) to redo/review button
                btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                
                return cell!
            }
            else {
                //Custom cell, it returns the cell which is displayed below the bar chart
                let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                cell.selectionStyle = .none
                
                //Serial number
                cell.lblMarker.text = String(indexPath.row) + " -"
                
                //Result category name
                cell.lblTitle.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                
                //category percentage
                cell.lblScore.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                
                //category background color according to there % and position in array
                cell.lblScore.backgroundColor = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].bgColor
                
                return cell
            }
        }
        else {
            //case of personality type
            
            if indexPath.row == 0 {
                //Show Custom cell for graph on first index
                let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                
                //set delegate
                cell.basicBarChartForPersonalityType.delegate = self
                
                //set data enteries for graph
                cell.basicBarChartForPersonalityType.dataEntries = dataEntriesPersonalityType
                cell.selectionStyle = .none
                
                //hide back button
                cell.btnBack.isHidden = true
                return cell
            }
            else if indexPath.row == arrOfPersonalityTypeResult.count + 1 {
                //Show Custom cell for redo/review question on last index
                let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                cell?.selectionStyle = .none
                
                //assign tag to button
                let btn = cell?.viewWithTag(70) as! UIButton
                
                //assign target(action) to redo/review button
                btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                
                return cell!
            }
            else {
                //Custom cell, it returns the cell which is displayed below the bar chart
                let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                cell.selectionStyle = .none
                
                //Serial number
                cell.lblMarker.text = String(indexPath.row) + " -"
                
                //Result category name
                cell.lblTitle.text = arrOfPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                
                //category percentage
                cell.lblScore.text = arrOfPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                
                //category background color according to there % and position in array
                cell.lblScore.backgroundColor = arrOfPersonalityTypeResult[indexPath.row - 1].bgColor
                
                return cell
            }
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.arrOfPersonalityTypeResult.count == 0 {
            //case when no data
            return 50
        }
        else {
            if indexPath.row == 0 {
                //graph view height
                return 390
            }
            else if indexPath.row == arrOfPersonalityTypeResult.count + 1 {
                //redo/review button cell height
                return 60
            }
            else {
                //list view height
                return 40
            }
        }
    }
    
    //MARK: - Call Web service
    /**
     * API to get  result for personality type
     */
    func callWebServiceForPersonalityTypeResult() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
//        param = ["orgId": AuthModel.sharedInstance.orgId]
        param = ["userId": AuthModel.sharedInstance.user_id]

        print("+++++: ",param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getPersonalitytypeReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse personality type result values
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfDiagnosticResult) in
                        
                        //stores result in array of model:DiagnosticResultModel
                        self.arrOfPersonalityTypeResult = arrOfDiagnosticResult
                        
                    })
                    
                    if self.arrOfPersonalityTypeResult.count == 0{
                        //case of no result
                        
                        //empty result array
                        self.arrOfPersonalityTypeResult.removeAll()
                    }
                    else{
                        //assign he generated data enteries to third party graph object
                        self.dataEntriesPersonalityType = self.generateDataEntriesPersonalitytype()
                    }
                    
                    //set up graph data
                    self.setGraphDataPersonalityType()
                    
                    //load table view
                    self.tblView.reloadData()
                    
                }
                    
                else {
                    //case of no response
                    
                    //show error message
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API to get  result for personality type sub categories
     */
    func callWebServiceForSubCategoryPersonalityType(catID: String){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =
            [
                "orgId"   : AuthModel.sharedInstance.orgId,
                "userId" : AuthModel.sharedInstance.user_id,
                "categoryId" : catID
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getPersonalitytypeReportSubGraphUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse result values
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfPersonalityTypeResult) in
                        
                        //stores result in array of model:DiagnosticResultModel
                        self.arrOfSubCategoryPersonalityTypeResult = arrOfPersonalityTypeResult
                        
                    })
                    if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                        //case of no result
                        
                        //empty result array
                        self.arrOfPersonalityTypeResult.removeAll()
                    }
                    else{
                        //assign he generated data enteries to third party graph object
                        self.dataEntriesSubCategoryPersonalityType = self.generateDataEntriesSubCategoryPersonalityType()
                    }
                    
                    //set up graph data
                    self.setGraphDataSubCategoryPeronalityType()
                    
                    //load table view
                    self.tblView.reloadData()
                }
                    
                else {
                    //case of no response
                    self.arrOfSubCategoryPersonalityTypeResult.removeAll()
                    
                    //show error message
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
