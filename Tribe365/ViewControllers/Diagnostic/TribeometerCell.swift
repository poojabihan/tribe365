//
//  TribeometerCell.swift
//  Tribe365
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
* This class is used as Tribeometer questions/options table view cell
*/
class TribeometerCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //question label
    @IBOutlet weak var lblQuestion: UILabel!
    
    //question number label
    @IBOutlet weak var lblNumber: UILabel!
    
    //option button
    @IBOutlet weak var btnNone: UIButton!
    
    //option button
    @IBOutlet weak var btnLow: UIButton!
    
    //option button
    @IBOutlet weak var btnMedium: UIButton!
    
    //option button
    @IBOutlet weak var btnHigh: UIButton!
    
    //option label
    @IBOutlet weak var lblNone: UILabel!
    
    //option label
    @IBOutlet weak var lblLow: UILabel!
    
    //option label
    @IBOutlet weak var lblMedium: UILabel!
    
    //option label
    @IBOutlet weak var lblHigh: UILabel!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
