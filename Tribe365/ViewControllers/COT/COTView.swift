//
//  COTView.swift
//  Tribe365
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//   1 Aug ©2018


import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class COTView: UIViewController {

    
    //MARK: - IBOutlets
    
    @IBOutlet weak var viewForTeamRoleMap: UIView!
    @IBOutlet weak var viewForFunctionalLens: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnTeamTeamRoleMap: UIButton!
    @IBOutlet weak var btnIndividualTeamRoleMap: UIButton!
    @IBOutlet weak var btnTeamFunctionalLens: UIButton!

    // MARK: - Variables
    let panel = JKNotificationPanel()
    var strOrgID = ""

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For Navigation bar tranparancy
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        
        tblView?.tableFooterView = UIView()
        viewForTeamRoleMap.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        viewForTeamRoleMap.layer.borderWidth = 1
        viewForFunctionalLens.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        viewForFunctionalLens.layer.borderWidth = 1
        tblView?.tableFooterView = UIView()

        if AuthModel.sharedInstance.role == kUser {
            btnIndividualTeamRoleMap.isHidden = false
            btnTeamTeamRoleMap.isHidden = false
        }
        else {
            btnTeamTeamRoleMap.isHidden = false
            btnIndividualTeamRoleMap.isHidden = true
            btnTeamFunctionalLens.isHidden = false
        }

    }

    
    //MARK: - Action Methods
    @IBAction func btnBackAction(_ sender: Any) {
        
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnIndividualOfTeamRole(_ sender: UIButton) {
        
        callWebServiceForCOT()
    }
    
    @IBAction func btnTeamOfTeamRole(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "TeamViewForTeamRole") as! TeamViewForTeamRole
        
        objVC.strOrgID = strOrgID
    self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnIndividualOfFunctionalLens(_ sender: UIButton) {
      //New functionality
        
    callWebServiceCheckQuestionsForMail()
        
    }

    @IBAction func btnTeamOfFunctionalLens(_ sender: UIButton) {
        
       let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTips") as! FunctionalLensTribeTips
        objVC.orgID = self.strOrgID
        objVC.strMyType = "team"
       self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnSupportAction(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.cot)
    }
    
    @IBAction func btnTeamRoleMapSupport(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.teamRoleMap)
    }
    
    @IBAction func btnFunctionalLensAction(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.functionalLens)
    }
    
    //MARK: - Custom Methods
    func openSupportScreen(strType: String) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = strType
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - WebService Methods
    
    func callWebServiceCheckQuestionsForMail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.FunctionalLens.questionsDistributedToUser, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS abi vala ",response ?? "")
                    
                    if response!["data"]["isCOTfunclensAnswersDone"].boolValue{
                        
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensView") as! FunctionalLensView
                        
                        objVC.strOrgID = AuthModel.sharedInstance.orgId
                        objVC.strUserID = AuthModel.sharedInstance.user_id
                        objVC.strMyType = "Indi"

                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    else {
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
//                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionnaireView") as! QuestionnaireView
//
//                        objVC.strMailSent = response!["data"]["isCOTquestionsMailSent"].stringValue
//                        objVC.strDate = response!["data"]["COTFunLensStartDate"].stringValue
//                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func callWebServiceForCOT() {
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.isCOTanswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    if response!["data"]["isCOTanswered"].boolValue == true {
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "IndividualViewForTeamRoleMap") as! IndividualViewForTeamRoleMap
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
