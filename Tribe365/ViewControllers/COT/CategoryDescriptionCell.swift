//
//  CategoryDescriptionCell.swift
//  Tribe365
//
//  Created by Apple on 13/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class CategoryDescriptionCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
