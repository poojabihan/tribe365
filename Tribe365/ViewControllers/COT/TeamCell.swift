//
//  TeamCell.swift
//  Tribe365
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblShaper: UILabel!
    @IBOutlet weak var lblImplementer: UILabel!
    @IBOutlet weak var lblCompleter: UILabel!
    @IBOutlet weak var lblMonitorEvaluator: UILabel!
    @IBOutlet weak var lblTeamWorker: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    @IBOutlet weak var lblResourceInvestigator: UILabel!
    @IBOutlet weak var lblMemberName: UILabel!
    
    @IBOutlet weak var lblCoordinatorScore: UILabel!
    @IBOutlet weak var lblShaperScore: UILabel!
    @IBOutlet weak var lblImplementerScore: UILabel!
    @IBOutlet weak var lblCompleterScore: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorScore: UILabel!
    @IBOutlet weak var lblTeamWorkerScore: UILabel!
    @IBOutlet weak var lblPlantScore: UILabel!
    @IBOutlet weak var lblResourceInvestigatorScore: UILabel!

    @IBOutlet weak var lblCoordinatorValue: UILabel!
    @IBOutlet weak var lblShaperValue: UILabel!
    @IBOutlet weak var lblImplementerValue: UILabel!
    @IBOutlet weak var lblCompleterValue: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorValue: UILabel!
    @IBOutlet weak var lblTeamWorkerValue: UILabel!
    @IBOutlet weak var lblPlantValue: UILabel!
    @IBOutlet weak var lblResourceInvestigatorValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
