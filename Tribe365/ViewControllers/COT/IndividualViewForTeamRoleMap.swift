//
//  IndividualViewForTeamRoleMap.swift
//  Tribe365
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

let greenColor = "#6AA854"
let yellowColor = "#FFFD46"
let blueColor = "#006BF5"
let orangeColor = "#FF972B"

class IndividualViewForTeamRoleMap: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFourth: UILabel!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var lblShaperValue: UILabel!
    @IBOutlet weak var lblCoordinatorValue: UILabel!
    @IBOutlet weak var lblImplementerValue: UILabel!
    @IBOutlet weak var lblCompleterValue: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorValue: UILabel!
    @IBOutlet weak var lblTeamWorkValue: UILabel!
    @IBOutlet weak var lblPlantValue: UILabel!
    @IBOutlet weak var lblResourceInvestigatorValue: UILabel!

    @IBOutlet weak var lblShaperScore: UILabel!
    @IBOutlet weak var lblCoordinatorScore: UILabel!
    @IBOutlet weak var lblImplementerScore: UILabel!
    @IBOutlet weak var lblCompleterScore: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorScore: UILabel!
    @IBOutlet weak var lblTeamWorkScore: UILabel!
    @IBOutlet weak var lblPlantScore: UILabel!
    @IBOutlet weak var lblResourceInvestigatorScore: UILabel!

    @IBOutlet weak var lblShaper: UILabel!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblImplementer: UILabel!
    @IBOutlet weak var lblCompleter: UILabel!
    @IBOutlet weak var lblMonitorEvaluator: UILabel!
    @IBOutlet weak var lblTeamWork: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    @IBOutlet weak var lblResourceInvestigator: UILabel!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    let panel = JKNotificationPanel()
    var isQuestionDestributed = false

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //For Navigation bar tranparancy
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //Calling Func to write text in subscript
        
        prepareLabel(label: lblFirst, string: "   1", superScript: "st", string2: " Preference" )
        
        prepareLabel(label: lblSecond, string: "   2", superScript: "nd", string2: " Preference")
        
        prepareLabel(label: lblThird, string: "   3", superScript: "rd", string2: " Preference")
        
        prepareLabel(label: lblFourth, string: "   ", superScript: "", string2: " Reserve Role")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callWebServiceForIndividualSummary()
    }

    //MARK: - IBAction

    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnRedoReviewAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
        objVC.isQuestionDestributed = isQuestionDestributed
        objVC.isEditMode = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForIndividualSummary() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.individualSummary, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")

                    
                    self.lblShaperValue.text = response!["data"]["shaper"].stringValue
                    self.updateBgColor(label: self.lblShaperValue)

                    self.lblCoordinatorValue.text = response!["data"]["coordinator"].stringValue
                    self.updateBgColor(label: self.lblCoordinatorValue)

                    self.lblImplementerValue.text = response!["data"]["implementer"].stringValue
                    self.updateBgColor(label: self.lblImplementerValue)

                    self.lblCompleterValue.text = response!["data"]["completerFinisher"].stringValue
                    self.updateBgColor(label: self.lblCompleterValue)

                    self.lblMonitorEvaluatorValue.text = response!["data"]["monitorEvaluator"].stringValue
                    self.updateBgColor(label: self.lblMonitorEvaluatorValue)

                    self.lblTeamWorkValue.text = response!["data"]["teamworker"].stringValue
                    self.updateBgColor(label: self.lblTeamWorkValue)

                    self.lblPlantValue.text = response!["data"]["plant"].stringValue
                    self.updateBgColor(label: self.lblPlantValue)

                    self.lblResourceInvestigatorValue.text = response!["data"]["resourceInvestigator"].stringValue
                    self.updateBgColor(label: self.lblResourceInvestigatorValue)

                    self.lblShaperScore.text = "Score: " + response!["data"]["totalKeyCount"]["shaper"].stringValue
                    self.lblCoordinatorScore.text = "Score: " + response!["data"]["totalKeyCount"]["coordinator"].stringValue
                    self.lblImplementerScore.text = "Score: " + response!["data"]["totalKeyCount"]["implementer"].stringValue
                    self.lblCompleterScore.text = "Score: " + response!["data"]["totalKeyCount"]["completerFinisher"].stringValue
                    self.lblMonitorEvaluatorScore.text = "Score: " + response!["data"]["totalKeyCount"]["monitorEvaluator"].stringValue
                    self.lblTeamWorkScore.text = "Score: " + response!["data"]["totalKeyCount"]["teamworker"].stringValue
                    self.lblPlantScore.text = "Score: " + response!["data"]["totalKeyCount"]["plant"].stringValue
                    self.lblResourceInvestigatorScore.text = "Score: " + response!["data"]["totalKeyCount"]["resourceInvestigator"].stringValue
                    
                    self.lblShaper.text = response!["data"]["mapersArray"]["shaper"].stringValue
                    self.lblCoordinator.text = response!["data"]["mapersArray"]["coordinator"].stringValue
                    self.lblImplementer.text = response!["data"]["mapersArray"]["implementer"].stringValue
                    self.lblCompleter.text = response!["data"]["mapersArray"]["completerFinisher"].stringValue
                    self.lblMonitorEvaluator.text = response!["data"]["mapersArray"]["monitorEvaluator"].stringValue
                    self.lblTeamWork.text = response!["data"]["mapersArray"]["teamworker"].stringValue
                    self.lblPlant.text = response!["data"]["mapersArray"]["plant"].stringValue
                    self.lblResourceInvestigator.text = response!["data"]["mapersArray"]["resourceInvestigator"].stringValue

                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Function
    func updateBgColor(label: UILabel) {
        switch label.text {
        case "1":
            label.backgroundColor = UIColor(hexString: greenColor)
        case "2":
            label.backgroundColor = UIColor(hexString: yellowColor)
            label.textColor = UIColor.black
        case "3":
            label.backgroundColor = UIColor(hexString: blueColor)
        case "4", "5", "6", "7", "8":
            label.backgroundColor = UIColor(hexString: orangeColor)
        default:
            print("defaultcase")
        }
    }
    
    func prepareLabel(label: UILabel, string: String, superScript: String, string2: String) {
        
        let font = UIFont.systemFont(ofSize: 11, weight: .regular)
    
        let fontSuper =  UIFont.systemFont(ofSize: 7, weight: .regular)
        //let fontSuper = UIFont(name: "Helvetica", size:10)
        
        
        let attributedString = NSMutableAttributedString(string: string + superScript + string2, attributes: [kCTFontAttributeName as NSAttributedStringKey:font])
        
        attributedString.setAttributes([.font: fontSuper,.baselineOffset:5], range: NSRange(location:string.count, length:superScript.count))

        
        label.attributedText = attributedString
    }
    

}
