//
//  StaffCell.swift
//  Tribe365
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class StaffCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
