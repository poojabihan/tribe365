//
//  CategoryDescriptionView.swift
//  Tribe365
//
//  Created by Apple on 13/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
class CategoryDescriptionView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK: -Variables
    let panel = JKNotificationPanel()
    var arrOfcatDescription = [CategoryDescriptionModel]()
    
    //MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        // Do any additional setup after loading the view.
        tblView.estimatedRowHeight = 100
        tblView.separatorStyle = .none
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        callWebServiceToGetCatergoryDescription()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK: - UITableView Delegate And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOfcatDescription.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*"user_type" = Admin;
         "user_type" = User;*/
        
        let cell : CategoryDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "CategoryDescriptionCell") as! CategoryDescriptionCell
        
        //let attrs2 = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 15)]
        
        let attrs = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16.0, weight: .heavy)]
        //for Title
        let attributedTitleString = NSMutableAttributedString(string: "Title: ", attributes:attrs)
        let normalString1 = NSMutableAttributedString(string:arrOfcatDescription[indexPath.row].strTitle)
        
        attributedTitleString.append(normalString1)
        
        cell.lblTitle.attributedText = attributedTitleString
        
        //for Value
        let attributedValueString = NSMutableAttributedString(string: "Value: ", attributes:attrs)
        
        let normalString2 = NSMutableAttributedString(string:arrOfcatDescription[indexPath.row].strShortDescription)
        
        attributedValueString.append(normalString2)
        cell.lblValue.attributedText =  attributedValueString
        
        //for Description
        let attributedDescriptionString = NSMutableAttributedString(string: "Description: ", attributes:attrs)
        
        let normalString3 = NSMutableAttributedString(string:arrOfcatDescription[indexPath.row].strLongDescription)
        
        attributedDescriptionString.append(normalString3)
        cell.lblDescription.attributedText = attributedDescriptionString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func callWebServiceToGetCatergoryDescription() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.getCOTMapperSummary, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    COTParser.parseCategoryDescription(response: response!, completionHandler: { (arrOfCategoryDescription) in
                        
                        self.arrOfcatDescription = arrOfCategoryDescription
                        
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

}
