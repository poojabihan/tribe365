//
//  TeamAverageResultView.swift
//  Tribe365
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

let laalColor = "#FF0019"

class TeamAverageResultView: UIViewController, HADropDownDelegate, UITextFieldDelegate{

   // MARK: - IBOutlets
    @IBOutlet weak var lblShaper: UILabel!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblImplementer: UILabel!
    @IBOutlet weak var lblCompleter: UILabel!
    @IBOutlet weak var lblMonitorEvaluator: UILabel!
    @IBOutlet weak var lblTeamWorker: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    @IBOutlet weak var lblResourceInvestigator: UILabel!
    
    @IBOutlet weak var lblShaperValue: UILabel!
    @IBOutlet weak var lblCoordinatorValue: UILabel!
    @IBOutlet weak var lblImplementerValue: UILabel!
    @IBOutlet weak var lblCompleterValue: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorValue: UILabel!
    @IBOutlet weak var lblTeamWorkerValue: UILabel!
    @IBOutlet weak var lblPlantValue: UILabel!
    @IBOutlet weak var lblResourceInvestigatorValue: UILabel!

    @IBOutlet weak var viewForDropDown: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var HeightConstraintForDropDown: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraintforView: NSLayoutConstraint!
    
    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var dropDownDept: HADropDown!
    
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!

    //MARK: - Variables
    var strOrgID = ""
    let panel = JKNotificationPanel()
    var comingFromReports = ""
    
    //For all Offices and department list Api
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    
    //Arr For All Department
    var arrForDepartmentName = [DepartmentModel]()
    var arrOfAllDeparmentList = [String]()
    var arrOfAllDepartmentIdList = [String]()
    
    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0
    
    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()
    
    var loadForFirst = false
    var selectedFromDate = ""
    var selectedToDate = ""
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if comingFromReports == "True"{
            lblHeader.text = "REPORT - TEAM ROLE MAP REVIEW"
            HeightConstraintForDropDown.constant = 50
            topConstraintforView.constant = 15
            viewForDropDown.alpha = 1.0
            callWebServiceTeamRoleMapReport()
            dropDownDept.delegate = self
            dropDownOffice.delegate = self
            callWebServiceToGetAllOfficesAndDepartmentList()
            callWebServiceToGetAllDepartments()
        }
        else{
            
             HeightConstraintForDropDown.constant = 0
            topConstraintforView.constant = 0
            viewForDropDown.alpha = 0.0
            callWebServiceForResults()
        }
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
     
    //MARK: - Custom Methods
    func setColor(label: UILabel, compareText: String) {
        
        if (compareText == "underrepresented") {
            label.backgroundColor = UIColor(hexString: laalColor)
        }
        else if (compareText == "overrepresented") {
            label.backgroundColor = UIColor.black
        }
        else {
            label.backgroundColor = UIColor.white
        }
    }
    
    func CreateDropDownListOfAllDepartemntArray(){
        
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
        // self.dropDownDept.items = self.arrDepartmentNames
    }
    
    func setData() {
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
            dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                callWebServiceTeamRoleMapReport()
            }
        }
        else {
            
            if selectedFromDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        callWebServiceTeamRoleMapReport()
                    }
                }
            }
            
            
        }
    }

    
    //MARK: - WebService Methods
    
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        self.arrDepartment = arrDept
                        self.arrAllDepartment = arrDept
                        self.arrOfficesName = arrOfcNames
                        self.arrDepartmentNames = arrDeptNames
                        self.arrAllDepartmentNames = arrDeptNames
                        self.setData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllDepartments() {
    
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
   
    
    func callWebServiceForResults() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID
                     ] as [String : Any]
        
        print("+++++: ",param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.getCOTmaperkey, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    self.lblPlant.text = response!["data"]["plant"].stringValue + "%"
                    self.lblMonitorEvaluator.text = response!["data"]["monitorEvaluator"].stringValue + "%"
                    self.lblTeamWorker.text = response!["data"]["teamworker"].stringValue + "%"
                    self.lblCompleter.text = response!["data"]["completerFinisher"].stringValue + "%"
                    self.lblShaper.text = response!["data"]["shaper"].stringValue + "%"
                    self.lblCoordinator.text = response!["data"]["coordinator"].stringValue + "%"
                    self.lblImplementer.text = response!["data"]["implementer"].stringValue + "%"
                    self.lblResourceInvestigator.text = response!["data"]["resourceInvestigator"].stringValue + "%"
                    
                    
                    self.lblPlantValue.text = response!["data"]["mapersArray"]["plant"].stringValue
                    self.lblMonitorEvaluatorValue.text = response!["data"]["mapersArray"]["monitorEvaluator"].stringValue
                    self.lblTeamWorkerValue.text = response!["data"]["mapersArray"]["teamworker"].stringValue
                    self.lblCompleterValue.text = response!["data"]["mapersArray"]["completerFinisher"].stringValue
                    self.lblShaperValue.text = response!["data"]["mapersArray"]["shaper"].stringValue
                    self.lblCoordinatorValue.text = response!["data"]["mapersArray"]["coordinator"].stringValue
                    self.lblImplementerValue.text = response!["data"]["mapersArray"]["implementer"].stringValue
                    self.lblResourceInvestigatorValue.text = response!["data"]["mapersArray"]["resourceInvestigator"].stringValue

                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceTeamRoleMapReport() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            
            param = [ "orgId": strOrgID,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",
                      "startDate":selectedFromDate,
                      "endDate":selectedToDate]
            
        }
        else{
            param = [ "orgId": strOrgID,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                      "startDate":selectedFromDate,
                      "endDate":selectedToDate]
        }
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getCOTteamRoleMapReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    self.lblPlant.text = response!["data"]["plant"].stringValue + "%"
                    self.lblMonitorEvaluator.text = response!["data"]["monitorEvaluator"].stringValue + "%"
                    self.lblTeamWorker.text = response!["data"]["teamworker"].stringValue + "%"
                    self.lblCompleter.text = response!["data"]["completerFinisher"].stringValue + "%"
                    self.lblShaper.text = response!["data"]["shaper"].stringValue + "%"
                    self.lblCoordinator.text = response!["data"]["coordinator"].stringValue + "%"
                    self.lblImplementer.text = response!["data"]["implementer"].stringValue + "%"
                    self.lblResourceInvestigator.text = response!["data"]["resourceInvestigator"].stringValue + "%"
                    
                    self.lblPlantValue.text = response!["data"]["mapersArray"]["plant"].stringValue
                    self.lblMonitorEvaluatorValue.text = response!["data"]["mapersArray"]["monitorEvaluator"].stringValue
                    self.lblTeamWorkerValue.text = response!["data"]["mapersArray"]["teamworker"].stringValue
                    self.lblCompleterValue.text = response!["data"]["mapersArray"]["completerFinisher"].stringValue
                    self.lblShaperValue.text = response!["data"]["mapersArray"]["shaper"].stringValue
                    self.lblCoordinatorValue.text = response!["data"]["mapersArray"]["coordinator"].stringValue
                    self.lblImplementerValue.text = response!["data"]["mapersArray"]["implementer"].stringValue
                    self.lblResourceInvestigatorValue.text = response!["data"]["mapersArray"]["resourceInvestigator"].stringValue

                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*func callWebServiceForCategory() {
       
        let arrCategory = [String]()
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.getCOTteamRoleMapValues, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    for value in response!["data"].arrayValue {
                        arrCategory.append(value["maper"].stringValue)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }*/
    
    //MARK: - HADropDown Delegate Method
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
      //  selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
        // dropDownDept.item
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            selectedIndexOfDepartment = 0
            selectedIndexOfOffice = 0
            dropDownDept.title = "ALL DEPARTMENTS"
            selectedIndexOfOffice = index
            if index != 0 {
                arrDepartment = arrOffices[index - 1].arrDepartment
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                
                if dropDownOffice.title == "ALL OFFICES"
                {
                    dropDownDept.items = arrOfAllDeparmentList
                }
                // arrDepartment = arrAllDepartment
                //arrDepartmentNames = arrAllDepartmentNames
            }
            setData()
        }
        else if dropDown == dropDownDept {
            selectedIndexOfDepartment = index
        }
        callWebServiceTeamRoleMapReport()
    }
    
    
    
}
