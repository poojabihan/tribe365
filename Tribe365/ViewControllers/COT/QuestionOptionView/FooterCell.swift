//
//  FooterCell.swift
//  Tribe365
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This class us no longer in use
 */
class FooterCell: UITableViewCell {

    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var btnNextQuestion: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
