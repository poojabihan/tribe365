//
//  QuestionOptionView.swift
//  Tribe365
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class QuestionOptionView: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate {
    
    //MARK: - IbOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btnNextQuestion: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerContainerView: UIView!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrOfOptions = [OptionModel]()
    var arrOfSno = ["A","B","C","D","E","F","G","H"]
//    var questionlblHeight = CGFloat()
//    var optionLblHeight = CGFloat()
    var arrOfQuestions = [QuestionModel]()
    var currentQues = ""
    var currentIndex = 0
    var currentTotalScores = 0
    var selectedTextField: UITextField?
    var arrQuestionsAnswers = [[String : Any]]()
    var isQuestionDestributed = false
    var arrayScores = [String]()
    var selectedPickerValue = "0"
    var isEditMode = false
    var cellHeightDictionary: NSMutableDictionary = NSMutableDictionary()// To overcome the issue of iOS11.2
    var checkVariable = false
    var checkVariableShouldMatter = false
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        arrayScores = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        if isEditMode {
            arrayScores = ["0"]
        }
        self.pickerView.dataSource = self
        self.pickerView.delegate = self

        tblView.tableFooterView = UIView()
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 125
        cellHeightDictionary = NSMutableDictionary()


        // Notification Observers for UIKeyboard Hide Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(QuestionOptionView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(QuestionOptionView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        
        
        if isEditMode {
            tblView.isHidden = false
            lblNoData.isHidden = true
            callWebServiceForAnswers()
        }
        else {
            if isQuestionDestributed {
                tblView.isHidden = false
                lblNoData.isHidden = true
                callWebServiceForQuestions()
            }
            else {
                tblView.isHidden = true
                lblNoData.isHidden = false
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Questions are not distributed yet.")
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
    
//        self.navigationController?.popViewController(animated: true)
        if isEditMode == false{
            if checkVariable == true{
                let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                    self.saveAnswers()
                    self.navigationController?.popViewController(animated: true)

                    
                    NSLog("Save")
                }
                
                alertController.addAction(okAction)
                
                alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    //run your function here
                    self.navigationController?.popViewController(animated: true)
                }))
                
                
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
        }
            
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnNextQueAction(_ sender: UIButton) {
        
        if self.arrOfQuestions.count == currentIndex {
            currentIndex = currentIndex - 1
        }
        
        for (index,value) in self.arrOfQuestions[currentIndex].arrOptions.enumerated() {
            
            if value.strAnswer == "" {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please provide score for option " + arrOfSno[index])
//                self.scrollToFirstRow()
                return
            }
        }
        
        if Int(getCurrentscore()) != 10 {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please revise your scores, total scores should be 10")
            return
        }
        
        if isEditMode {
            updateData()
        }
        else {
            checkVariable = checkVariableShouldMatter
            saveData()
        }
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        selectedTextField?.text = selectedPickerValue
        selectedTextField?.resignFirstResponder()
        print("before",self.arrOfQuestions[currentIndex].arrOptions[(selectedTextField?.tag)! - 1].strOption, self.arrOfQuestions[currentIndex].arrOptions[(selectedTextField?.tag)! - 1].strAnswer)
        
        self.arrOfQuestions[currentIndex].arrOptions[(selectedTextField?.tag)! - 1].strAnswer = selectedPickerValue
       
        print("after", self.arrOfQuestions[currentIndex].arrOptions[(selectedTextField?.tag)! - 1].strOption,self.arrOfQuestions[currentIndex].arrOptions[(selectedTextField?.tag)! - 1].strAnswer)

        currentTotalScores = Int(getCurrentscore())!
        self.lblTotal.text = String(self.currentTotalScores)
        
        arrayScores.removeAll()
        
        if self.currentTotalScores == 10 {
            arrayScores.append("0")
        }
        else {
            
            for value in 0...(10 - self.currentTotalScores) {
                
                arrayScores.append(String(value))
            }
        }
    }
    
    @objc func btnMinusAction(_ sender: UIButton) {
        print("You clicked button Minus at index", sender.tag)
        
        checkVariableShouldMatter = true
        self.arrOfQuestions[currentIndex].arrOptions[(sender.tag) - 1].strAnswer = String(Int(self.arrOfQuestions[currentIndex].arrOptions[(sender.tag) - 1].strAnswer)! - 1)
        
        currentTotalScores = Int(getCurrentscore())!
        self.lblTotal.text = String(self.currentTotalScores)
        
        tblReload(sender)

    }

    @objc func btnPlusAction(_ sender: UIButton) {
        print("You clicked button Plus at index", sender.tag)
        
        checkVariableShouldMatter = true
        self.arrOfQuestions[currentIndex].arrOptions[(sender.tag) - 1].strAnswer = String(Int(self.arrOfQuestions[currentIndex].arrOptions[(sender.tag) - 1].strAnswer)! + 1)
        
        currentTotalScores = Int(getCurrentscore())!
        self.lblTotal.text = String(self.currentTotalScores)
        
        tblReload(sender)
    }
    
    @objc func txtScoreAction(_ sender: UITextField) {
        
        selectedPickerValue = "0"
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
        sender.inputView = pickerContainerView
        selectedTextField = sender
        selectedTextField?.tag = sender.tag
    }
    
    //MARK: - Custom Method
    func saveAnswers() {
        UserDefaults.standard.set(true, forKey: "isSavedTeamRoleMap")
        UserDefaults.standard.set(arrQuestionsAnswers, forKey: "SavedDictForTeamRoleMap")
    }
    
    func getCurrentscore() -> String {
        var total = 0
        
        if currentIndex > self.arrOfQuestions.count {
            return ""
        }
        
        for value in self.arrOfQuestions[currentIndex].arrOptions {
            if value.strAnswer != "" {
                total = total + Int(value.strAnswer)!
            }
        }
        
        return String(total)
    }
    
    func saveData() {
        
        var arrAnswers = [[String : String]]()
        for value in self.arrOfQuestions[currentIndex].arrOptions {
            let dict =  ["optionId" : value.strOptionId,
                         "point" : value.strAnswer,
                         "roleMapId" : value.strRoleMapId]
            
            arrAnswers.append(dict)
        }
        
        let dictQues =  ["questionId" : arrOfQuestions[currentIndex].strQuestionId,
                         "option" : arrAnswers] as [String : Any]
        
        arrQuestionsAnswers.append(dictQues)
        
        self.currentIndex = self.currentIndex + 1
        if self.currentIndex != self.arrOfQuestions.count {
            self.currentQues = self.arrOfQuestions[self.currentIndex].strQuestionName
            self.arrOfOptions = self.arrOfQuestions[self.currentIndex].arrOptions
            self.btnNextQuestion.setTitle(self.currentIndex == self.arrOfQuestions.count - 1 ? "FINISH" : "NEXT QUESTION", for: .normal)
            self.currentTotalScores = 0
//            if isEditMode {
                self.currentTotalScores = Int(self.getCurrentscore())!
//            }
            self.lblTotal.text = String(self.currentTotalScores)
            self.tblView.reloadData()
            //self.tblView.setContentOffset(.zero, animated: true)
            self.scrollToFirstRow()
            self.selectedPickerValue = "0"
            self.arrayScores = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
            
        }
        else {
            callWebServiceForSaveAnswers()
        }
    }
    
    func updateData() {
        
        var arrAnswers = [[String : String]]()
        for value in self.arrOfQuestions[currentIndex].arrOptions {
            let dict =  ["answerId" : value.strAnswerID,
                         "point" : value.strAnswer]
            
            arrAnswers.append(dict)
        }
        
        let dictQues =  ["questionId" : arrOfQuestions[currentIndex].strQuestionId,
                         "option" : arrAnswers] as [String : Any]
        
        arrQuestionsAnswers.append(dictQues)
        
        self.currentIndex = self.currentIndex + 1
        if self.currentIndex != self.arrOfQuestions.count {
            self.currentQues = self.arrOfQuestions[self.currentIndex].strQuestionName
            self.arrOfOptions = self.arrOfQuestions[self.currentIndex].arrOptions
            self.btnNextQuestion.setTitle(self.currentIndex == self.arrOfQuestions.count - 1 ? "FINISH" : "NEXT QUESTION", for: .normal)
            self.currentTotalScores = 0
            if isEditMode {
                self.currentTotalScores = Int(self.getCurrentscore())!
            }
            self.lblTotal.text = String(self.currentTotalScores)
            self.tblView.reloadData()
            //self.tblView.setContentOffset(.zero, animated: true)
            self.scrollToFirstRow()
            self.selectedPickerValue = "0"
            self.arrayScores = ["0"]
            
        }
        else {
            callWebServiceForUpdateAnswers()
        }
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tblView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func tblReload(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.15, animations: {
        
//            let indexPath = NSIndexPath(row: sender.tag, section: 0)
//            self.view.layoutIfNeeded()
//            self.tblView.beginUpdates()
//            self.tblView.reloadRows(at: [indexPath as IndexPath], with: .middle) //try other animations
//            self.tblView.endUpdates()

//        }, completion: nil)
        
        self.tblView.reloadData()
    }
    
    //MARK: - UITableView Delegate and DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return arrOfOptions.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as! QuestionCell
            cell.lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblQuestion.numberOfLines = 0
            
            cell.lblQuestion.text = currentQues

//            questionlblHeight = (cell.lblQuestion.text?.height(withConstrainedWidth: cell.lblQuestion.frame.size.width, font: UIFont.systemFont(ofSize: 19.0, weight: .regular)))!
//            cell.heightConstaintForLblQuestion.constant = questionlblHeight
            
            return cell
            
        }
            
        /*else if indexPath.row == arrOfOptions.count + 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! FooterCell
            
            cell.btnNextQuestion.setTitle(currentIndex == self.arrOfQuestions.count - 1 ? "FINISH" : "NEXT QUESTION", for: .normal)
            cell.btnNextQuestion.addTarget(self, action: #selector(btnNextQueAction(_:)), for: .touchUpInside)
            cell.lblTotal.text = String(currentTotalScores)
            return cell
            
        }*/
        else  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell") as! OptionCell
            //To remove seprator from particular cell
            
//            cell.delegate = self
            tblView.separatorStyle = .none
            
            cell.lblOptions.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblOptions.numberOfLines = 0
            
            cell.lblSNo.text = arrOfSno[indexPath.row - 1]
            cell.lblOptions.text = arrOfOptions[indexPath.row - 1].strOption
//            cell.txtPoints.text = arrOfOptions[indexPath.row - 1].strAnswer
//            cell.txtPoints.tag = indexPath.row
//            cell.txtPoints.addTarget(self, action: #selector(txtScoreAction(_:)), for: .editingDidBegin)
            
            cell.lblPoints.text = arrOfOptions[indexPath.row - 1].strAnswer
            cell.btnPlus.tag = indexPath.row
            cell.btnPlus.addTarget(self, action: #selector(btnPlusAction(_:)), for: .touchUpInside)
            
            cell.btnMinus.tag = indexPath.row
            cell.btnMinus.addTarget(self, action: #selector(btnMinusAction(_:)), for: .touchUpInside)

            if arrOfOptions[indexPath.row - 1].strAnswer == "0"{
                cell.btnMinus.isUserInteractionEnabled = false
                cell.btnPlus.isUserInteractionEnabled = true
                cell.btnMinus.alpha = 0.5
                cell.btnPlus.alpha = 1.0
            }
            else {
                cell.btnMinus.isUserInteractionEnabled = true
                cell.btnMinus.alpha = 1.0
            }
            
            if arrOfOptions[indexPath.row - 1].strAnswer == "10" {
                cell.btnPlus.isUserInteractionEnabled = false
                cell.btnMinus.isUserInteractionEnabled = true
                cell.btnMinus.alpha = 1.0
                cell.btnPlus.alpha = 0.5
            }
            else {
                cell.btnPlus.isUserInteractionEnabled = true
                cell.btnPlus.alpha = 1.0
            }
            
            if getCurrentscore() == "10" {
                cell.btnPlus.isUserInteractionEnabled = false
//                cell.btnMinus.isUserInteractionEnabled = false
//                cell.btnMinus.alpha = 0.5
                cell.btnPlus.alpha = 0.5
            }
            
//            optionLblHeight = (cell.lblOptions.text?.height(withConstrainedWidth: cell.lblOptions.frame.size.width, font: UIFont.systemFont(ofSize: 14.0, weight: .light)))!
//            if optionLblHeight <= 45{
//                optionLblHeight = 35
//            }
//            else{
//                let optionLblHeightCopy = optionLblHeight
//                optionLblHeight = optionLblHeightCopy
//            }
//            cell.HeightConstaintOfLblOption.constant = optionLblHeight
//            cell.HeightConstaintOfLblOptionView.constant = optionLblHeight + 10
//            cell.HeightConstarintForOptionCell.constant = optionLblHeight + 65
            
            return cell
        }
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableViewAutomaticDimension
////        if indexPath.row == 0{
////
////            return questionlblHeight + 20
////        }
//////        else if indexPath.row == arrOfOptions.count + 1{
//////
//////            return 220
//////        }
////        else{
////            return optionLblHeight + 90
////        }
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightDictionary.setObject(cell.frame.size.height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellHeightDictionary.object(forKey: indexPath) != nil {
            let height = cellHeightDictionary.object(forKey: indexPath) as! CGFloat
            return height
        }
        return UITableViewAutomaticDimension
    }
    
//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cellHeightDictionary.setObject(cell.frame.size.height, forKey: indexPath as NSCopying)
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if cellHeightDictionary.object(forKey: indexPath) != nil {
//            let height = cellHeightDictionary.object(forKey: indexPath) as! CGFloat
//            return height
//        }
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
//    func cellDidChangeValue(cell: OptionCell) {
//
//        guard let indexPath = self.tblView.indexPath(for: cell) else {
//            return
//        }
//
//        /// Update data source - we have cell and its indexPath
//        self.arrOfQuestions[currentIndex].arrOptions[indexPath.row - 1].strAnswer = cell.txtPoints.text!
//        currentTotalScores = Int(getCurrentscore())!
//        self.lblTotal.text = String(self.currentTotalScores)
//    }
    
    //MARK: - WebService Methods
    func callWebServiceForQuestions() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.questions, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseQuestionsList(response: response!, completionHandler: { (arrQue) in
                        self.arrOfQuestions = arrQue
                        
                        self.currentQues = self.arrOfQuestions[self.currentIndex].strQuestionName
                        self.arrOfOptions = self.arrOfQuestions[self.currentIndex].arrOptions
//                        self.lblTotal.text = String(self.currentTotalScores)
                        self.currentTotalScores = Int(self.getCurrentscore())!
                        self.lblTotal.text = String(self.currentTotalScores)
                        self.tblView.tableFooterView = self.footerView


                        self.tblView.reloadData()
                    })
                    
                }
                else {
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForAnswers() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.getCOTteamRoleCompletedAnswers, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseAnswersList(response: response!, completionHandler: { (arrQue) in
                        self.arrOfQuestions = arrQue
                        
                        self.currentQues = self.arrOfQuestions[self.currentIndex].strQuestionName
                        self.arrOfOptions = self.arrOfQuestions[self.currentIndex].arrOptions
                        self.currentTotalScores = Int(self.getCurrentscore())!
                        self.lblTotal.text = String(self.currentTotalScores)
                        self.tblView.tableFooterView = self.footerView
                        
                        self.tblView.reloadData()

                    })
                    
                }
                else {
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForSaveAnswers() {
        
        let param = ["userId":AuthModel.sharedInstance.user_id,
                     "orgId":AuthModel.sharedInstance.orgId,
                     "answer":arrQuestionsAnswers] as [String : Any]
        
        print("Param :", param)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.saveAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForTeamRoleMap")
                    UserDefaults.standard.removeObject(forKey: "isSavedTeamRoleMap")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.checkVariable = false
                    self.btnBackAction(self)
                }
                else {
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUpdateAnswers() {
        
        let param = ["answer":arrQuestionsAnswers] as [String : Any]
        
        print("Param :", param)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.updateCOTteamRoleMapAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.btnBackAction(self)
                }
                else {
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    // Decipad config for adding Done button above itself
//    @objc func doneClicked(){
//        textField?.resignFirstResponder()
//        textField?.endEditing(true)
//    }
    
    
    //MARK: - UITextField Delegates
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
//
//        // Decipad config for adding Done button above itself
//        let toolBar = UIToolbar()
//        toolBar.sizeToFit()
//        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
//        toolBar.setItems([flexiableSpace, doneButton], animated: false)
//
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        textField.resignFirstResponder()
//        return true
//    }
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayScores.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayScores[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPickerValue = arrayScores[row]
//        self.labelFruit.text = arrayScores[row]
    }
}
