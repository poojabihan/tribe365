//
//  OptionCell.swift
//  Tribe365
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

//protocol UITableViewCellUpdateDelegate {
//    func cellDidChangeValue(cell: OptionCell)
//}
class OptionCell: UITableViewCell {

//    var delegate: UITableViewCellUpdateDelegate?

    @IBOutlet weak var lblOptions: UILabel!
    @IBOutlet weak var lblSNo: UILabel!
    @IBOutlet weak var HeightConstaintOfLblOptionView: NSLayoutConstraint!
    @IBOutlet weak var HeightConstaintOfLblOption: NSLayoutConstraint!
    @IBOutlet weak var HeightConstarintForOptionCell: NSLayoutConstraint!
    @IBOutlet weak var txtPoints: UITextField!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        txtPoints.addTarget(self, action: #selector(didCHangeTextFieldValue(sender:)), for: .editingChanged)

    }
    
//    @objc func didCHangeTextFieldValue(sender: AnyObject?) {
//        self.delegate?.cellDidChangeValue(cell: self)
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
