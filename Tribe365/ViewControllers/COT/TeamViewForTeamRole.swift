//
//  TeamViewForTeamRole.swift
//  Tribe365
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class TeamViewForTeamRole: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, HADropDownDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblKeyFirst: UILabel!
    @IBOutlet weak var lblKeySecond: UILabel!
    @IBOutlet weak var lblKeyFourth: UILabel!
    @IBOutlet weak var lblKeyThird: UILabel!
    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var dropDownDept: HADropDown!
    @IBOutlet weak var dropDownCategory: HADropDown!
    @IBOutlet weak var dropDownPreference: HADropDown!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrSummary = [SummaryModel]()
    var totalPageCount = 1
    var currentPage = 1
    var isLoadMore = true
    var isSearchEnabled = false
    var strOrgID = ""
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    var arrCategory = [String]()
    var arrPreference = [String]()

    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0
    var selectedIndexOfCategory = 0
    var selectedIndexOfPreference = 0

    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //For Navigation bar tranparancy
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
//        arrCategory = ["shaper", "coordinator", "completerFinisher", "teamworker", "implementer", "monitorEvaluator", "plant", "resourceInvestigato"]
        
        arrPreference = ["primary", "secondary", "tertiary"]

        
        prepareLabel(label: lblKeyFirst, string: "   1", superScript: "st", string2: " Preference" )
        
        prepareLabel(label: lblKeySecond, string: "   2", superScript: "nd", string2: " Preference")
        
        prepareLabel(label: lblKeyThird, string: "   3", superScript: "rd", string2: " Preference")
        
        prepareLabel(label: lblKeyFourth, string: "   ", superScript: "", string2: " Reserve Role")
        
        dropDownOffice.delegate = self
        dropDownDept.delegate = self
        dropDownCategory.delegate = self
        dropDownPreference.delegate = self

        callWebServiceForTeamSummary(searchText: searchBar.text ?? "")
        callWebServiceForOfficeDeptList()
        callWebServiceForCategory()
    }
    
    /*Search bar Customisation*/
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
//        
//        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
//        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
//        searchTextField.layer.cornerRadius = 1
//        searchTextField.borderStyle = .none
//        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
//        searchTextField.textAlignment = NSTextAlignment.left
//        let image:UIImage = UIImage(named: "search")!
//        let imageView:UIImageView = UIImageView.init(image: image)
//        searchTextField.leftView = nil
//        searchTextField.placeholder = " Search"
//        searchTextField.rightView = imageView
//        searchTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    
    //MARK: - IBAction
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnOfficeFilterAction(_ sender: Any) {
        
    }
    
    @IBAction func btnDeptFilterAction(_ sender: Any) {
        
    }
    
    
    //MARK: - Custom Method
    func setData() {
        dropDownOffice.items = ["All Offices"] + arrOfficesName
        dropDownDept.items = ["All Departments"] + arrDepartmentNames
        dropDownPreference.items = ["All Preference"] + arrPreference
    }
    
    func updateBgColor(label: UILabel) {
        switch label.text {
        case "1":
            label.backgroundColor = UIColor(hexString: greenColor)
        case "2":
            label.backgroundColor = UIColor(hexString: yellowColor)
        case "3":
            label.backgroundColor = UIColor(hexString: blueColor)
        case "4", "5", "6", "7", "8":
            label.backgroundColor = UIColor(hexString: orangeColor)
        default:
            label.backgroundColor = UIColor.white
        }
    }
    
    func prepareLabel(label: UILabel, string: String, superScript: String, string2: String) {
        
        let font = UIFont.systemFont(ofSize: 11, weight: .regular)
        
        let fontSuper =  UIFont.systemFont(ofSize: 7, weight: .regular)
        //let fontSuper = UIFont(name: "Helvetica", size:10)
        
        
        let attributedString = NSMutableAttributedString(string: string + superScript + string2, attributes: [kCTFontAttributeName as NSAttributedStringKey:font])
        
        attributedString.setAttributes([.font: fontSuper,.baselineOffset:5], range: NSRange(location:string.count, length:superScript.count))
        
        
        label.attributedText = attributedString
    }
    
    //MARK: - UITableView Delegate and DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSummary.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell") as! TeamCell
            //To remove seprator from particular cell
            tblView.separatorStyle = .none
        
            cell.lblMemberName.text = arrSummary[indexPath.row].strUserName
            cell.lblShaper.text = arrSummary[indexPath.row].strShaper
            cell.lblCoordinator.text = arrSummary[indexPath.row].strCoordinator
            cell.lblImplementer.text = arrSummary[indexPath.row].strImplementer
            cell.lblMonitorEvaluator.text = arrSummary[indexPath.row].strMonitorEvaluator
            cell.lblTeamWorker.text = arrSummary[indexPath.row].strTeamworker
            cell.lblPlant.text = arrSummary[indexPath.row].strPlant
            cell.lblResourceInvestigator.text = arrSummary[indexPath.row].strResourceInvestigator
            cell.lblCompleter.text = arrSummary[indexPath.row].strCompleterFinisher
        
            cell.lblShaperScore.text = "Score: " + arrSummary[indexPath.row].strShaperScore
            cell.lblCoordinatorScore.text = "Score: " + arrSummary[indexPath.row].strCoordinatorScore
            cell.lblImplementerScore.text = "Score: " + arrSummary[indexPath.row].strImplementerScore
            cell.lblMonitorEvaluatorScore.text = "Score: " + arrSummary[indexPath.row].strMonitorEvaluatorScore
            cell.lblTeamWorkerScore.text = "Score: " + arrSummary[indexPath.row].strTeamworkerScore
            cell.lblPlantScore.text = "Score: " + arrSummary[indexPath.row].strPlantScore
            cell.lblResourceInvestigatorScore.text = "Score: " + arrSummary[indexPath.row].strResourceInvestigatorScore
            cell.lblCompleterScore.text = "Score: " + arrSummary[indexPath.row].strCompleterFinisherScore

        cell.lblShaperValue.text = arrSummary[indexPath.row].strShaperValue
        cell.lblCoordinatorValue.text = arrSummary[indexPath.row].strCoordinatorValue
        cell.lblImplementerValue.text = arrSummary[indexPath.row].strImplementerValue
        cell.lblMonitorEvaluatorValue.text = arrSummary[indexPath.row].strMonitorEvaluatorValue
        cell.lblTeamWorkerValue.text = arrSummary[indexPath.row].strTeamworkerValue
        cell.lblPlantValue.text = arrSummary[indexPath.row].strPlantValue
        cell.lblResourceInvestigatorValue.text = arrSummary[indexPath.row].strResourceInvestigatorValue
        cell.lblCompleterValue.text = arrSummary[indexPath.row].strCompleterFinisherValue

            updateBgColor(label: cell.lblShaper)
            updateBgColor(label: cell.lblCoordinator)
            updateBgColor(label: cell.lblImplementer)
            updateBgColor(label: cell.lblMonitorEvaluator)
            updateBgColor(label: cell.lblTeamWorker)
            updateBgColor(label: cell.lblPlant)
            updateBgColor(label: cell.lblResourceInvestigator)
            updateBgColor(label: cell.lblCompleter)

            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       return 364//444
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.currentPage == self.totalPageCount {
            self.isLoadMore = false
        }
        let lastElement = arrSummary.count - 1
        if indexPath.row == lastElement && isLoadMore && !isSearchEnabled {
            // handle your logic here to get more items, add it to dataSource and reload tableview
           
            self.currentPage = self.currentPage + 1
            callWebServiceForTeamSummary(searchText: searchBar.text ?? "")
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForTeamSummary(searchText: String) {
        
        if searchText == "" {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        }
        
        if searchText != "" || selectedIndexOfOffice != 0 || selectedIndexOfDepartment != 0 || selectedIndexOfCategory != 0 || selectedIndexOfPreference != 0 {
            currentPage = 1
            isSearchEnabled = true
        }
        else {
            isSearchEnabled = false
        }
        
        let param = ["orgId": strOrgID,
                     "searchKey": searchText,
                     "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                     "departmentId": selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                     "maperKey": selectedIndexOfCategory != 0 ? arrCategory[selectedIndexOfCategory - 1] : "",
                     "preferenceKey": selectedIndexOfPreference != 0 ? arrPreference[selectedIndexOfPreference - 1] : "",
                     "page":currentPage] as [String : Any]
        
        print("+++++: ",param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.teamSummary, param: param, withHeader: true ) { (response, errorMsg) in
            
            if searchText == "" {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseTeamSummaryList(response: response!, completionHandler: { (arrSummary, totalPages) in
                        
                        if self.isSearchEnabled {
                            self.arrSummary = arrSummary
                        }
                        else {
                            if self.currentPage == 1 {
                                self.arrSummary = arrSummary
                            }
                            else {
                                self.arrSummary.append(contentsOf: arrSummary)
                            }
                            
                            self.totalPageCount = Int(totalPages)!
                            
                            
                        }
                        
                        self.tblView.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForOfficeDeptList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        self.arrDepartment = arrDept
                        self.arrAllDepartment = arrDept
                        self.arrOfficesName = arrOfcNames
                        self.arrDepartmentNames = arrDeptNames
                        self.arrAllDepartmentNames = arrDeptNames
                        self.setData()
                        self.tblView.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForCategory() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.getCOTteamRoleMapValues, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    for value in response!["data"].arrayValue {
                        self.arrCategory.append(value["maper"].stringValue)
                    }
                    
                    self.dropDownCategory.items = ["All Categories"] + self.arrCategory

                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - HADropDown Delegate Method
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            selectedIndexOfOffice = index
            if index != 0 {
                arrDepartment = arrOffices[index - 1].arrDepartment
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                arrDepartment = arrAllDepartment
                arrDepartmentNames = arrAllDepartmentNames
            }
            setData()
        }
        else if dropDown == dropDownDept {
            selectedIndexOfDepartment = index
        }
        else if dropDown == dropDownCategory {
            selectedIndexOfCategory = index
        }
        else {
            selectedIndexOfPreference = index
        }
        
        currentPage = 1
        callWebServiceForTeamSummary(searchText: searchBar.text ?? "")
    }
    
    //MARK: - UISearchBar Delegate Methods
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        callWebServiceForTeamSummary(searchText: searchBar.text!)
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        callWebServiceForTeamSummary(searchText: searchBar.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toResult" {
            let vc = segue.destination as? TeamAverageResultView
            vc?.strOrgID = strOrgID
        }
    }

}
