//
//  PersonalityTypeClickView.swift
//  Tribe365
//
//  Created by Apple on 20/01/20.
//  Copyright © 2020 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This is used to show questions for personality type module
 */
class PersonalityTypeClickView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    //table view which holds the question list
    @IBOutlet weak var tblView: UITableView!
    
    //lable for heading
    @IBOutlet weak var lblHeader: UILabel!
    
    //submit button
    @IBOutlet weak var btnSubmit: UIButton!
    
    //show organisation logo
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variables
    //for diplaying the top banner of message success/error
    var panel = JKNotificationPanel()
    
    //stores the questions model
    var arrOfQuestionsList = [DiagonisticQuestionsModel]()
    
    //stores the answers list
    var arrOfUpdateAnswersList = [DiagonisticQuestionsModel]()
    
    //stores the json value of answers for api parameter
    var arrOfAnswers = [[String : Any]]()
    
    //stores the filled questions which needs to be saved for later (when pressed back button it asks to save questions)
    var arrOfSavedAnswers = [[String : Any]]()
    
    //to detect if user has saved the quetions for later
    var isSaved = false
    
    //to check whether user has completed the answers
    var isPersonalityTypeAnsDone = Bool()
    
    //to detect if user have changed anything, so that we can show the save later popup on back button click
    var checkVariable = ""
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        if isPersonalityTypeAnsDone == true{
            //Case when user has alredy completed the answers, show the update process
            lblHeader.text = "UPDATE PERSONALITY TYPE QUESTIONS"
            btnSubmit.setTitle("UPDATE", for: .normal)
            callWebServiceTogetPersonalityTypeAnsweredList()
        }
        else if isPersonalityTypeAnsDone == false {
            
            //Case when user has not completed the answers
            
            //            isSaved = (UserDefaults.standard.value(forKey: "isSaved") != nil)
            //            if isSaved == true {
            //                arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForPersonalityType") as! [[String : Any]]
            //                print(arrOfSavedAnswers)
            //            }
            callWebServiceTogetPersonalityTypeQuestionList()
        }
        
    }
    
    //MARK: - IBActions
    /**
     * This function navigates back to previous screen
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function navigates back to previous screen after saving answers in add case
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        if isPersonalityTypeAnsDone == false{
            if checkVariable == "filled One"{
                //if user has changed anything which can be saved, show save popup
                let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.saveAnswers()
                    
                    NSLog("Save")
                }
                
                alertController.addAction(okAction)
                alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    //run your function here
                    self.goToBackScreen()
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                //go back
                goToBackScreen()
            }
        }
        else{
            //go back
            goToBackScreen()
        }
    }
    
    /**
     * Submit click handle
     */
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if isPersonalityTypeAnsDone == true {
            //update case
            arrOfAnswers.removeAll()
            
            //loop throught the answers
            for j in (0..<self.arrOfUpdateAnswersList.count){
                
                if arrOfUpdateAnswersList[j].selectedIndex != -1 {
                    
                    let strAnsId =  self.arrOfUpdateAnswersList[j].strAnswerId
                    print(strAnsId)
                    
                    let strQuestionId = self.arrOfUpdateAnswersList[j].strAPIAnswerID
                    print(strQuestionId)
                    
                    let json = [  "answerId": strQuestionId,
                                  "optionId": strAnsId]
                    
                    //prepare array for answers
                    arrOfAnswers.append(json)
                }
                else{
                    //validate the required feilds
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                }
            }
            print(arrOfAnswers)
            
            //update the answers
            callWebServiceToUpdateAnswer()
        }
        else{
            //add case
            arrOfAnswers.removeAll()
            
            //loop throught the answers
            for j in (0..<self.arrOfQuestionsList.count){
                
                if arrOfQuestionsList[j].selectedIndex != -1 {
                    
                    let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                    print(strAnsId)
                    
                    let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                    print(strQuestionId)
                    
                    let json = [  "questionId": strQuestionId,
                                  "optionId": strAnsId]
                    
                    //prepare array for answers
                    arrOfAnswers.append(json)
                }
                else{
                    //validate the required feilds
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                }
            }
            print(arrOfAnswers)
            
            //add the answers
            callWebServiceToAddAnswers()
        }
    }
    
    //MARK: -  Custom Functions
    /**
     * Redirects user to previous screen
     */
    func goToBackScreen(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * This function is called when user choose to save the answer from the popup which appears on back button click.
     */
    func saveAnswers(){
        
        //FIRST remove previous saved answers annd add new
        UserDefaults.standard.removeObject(forKey: "SavedDictForPersonalityType")
        let keyValue = UserDefaults.standard.value(forKey: "SavedDictForPersonalityType")
        print("Key Value after remove \(String(describing: keyValue))")
        
        //SECOND create new data of answer
        arrOfSavedAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
            
            let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
            
            print(strAnsId)
            
            let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
            print(strQuestionId)
            
            let json = [  "questionId": strQuestionId,
                          "optionId": strAnsId]
            
            print(json)
            arrOfSavedAnswers.append(json)
        }
        
        //Third add new answer list
        UserDefaults.standard.set(true, forKey: "isSaved")
        UserDefaults.standard.set(arrOfSavedAnswers, forKey: "SavedDictForPersonalityType")
        
        //Fourth print answer list
        arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForPersonalityType") as! [[String : Any]]
        print(arrOfSavedAnswers)
        
        //go back to previous controller
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * This functions stores the previously saved answres into the newly fetched model from api
     */
    func SetSelctedIndexAndAnwerIdForSavedData()  {
        //chcek if user has saved something for later
        if self.isSaved == true{
            //freshly fetched array from api
            for i in (0..<self.arrOfQuestionsList.count){
                
                //previously saved answers array
                for j in (0..<self.arrOfSavedAnswers.count){
                    
                    //found the same qusetion which was saved so add the saved answers to question matched
                    if self.arrOfSavedAnswers[j]["questionId"] as! String == self.arrOfQuestionsList[i].strQuestionId{
                        
                        if self.arrOfSavedAnswers[i]["optionId"] as! String != ""{
                            self.arrOfQuestionsList[j].selectedIndex = i
                        }
                        
                        if self.arrOfSavedAnswers[j]["optionId"] as! String == "1"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "1"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "2"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "2"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "3"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "3"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "4"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "4"
                            
                        }
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == "5"
                        {
                            self.arrOfQuestionsList[i].strAnswerId = "5"
                            
                        }
                            
                        else if self.arrOfSavedAnswers[j]["optionId"] as! String == ""{
                            
                            self.arrOfQuestionsList[i].selectedIndex = -1
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - TableView data source and delegate
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isPersonalityTypeAnsDone == true{
            //if update case use arrOfUpdateAnswersList - stores answers along with questions
            return arrOfUpdateAnswersList.count
        }
        else{
            //if submit(first time) case use arrOfQuestionsList - stores only questions
            return arrOfQuestionsList.count
        }
    }
    
    /**
     * Tableview delegate method used to return estimated height of row
     */
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell
        let cell : DiagonsticCell = tableView.dequeueReusableCell(withIdentifier: "DiagonsticCell") as! DiagonsticCell
        
        if isPersonalityTypeAnsDone == true {
            //update case
            
            //Question number
            cell.lblNumber.text = String(indexPath.row + 1)
            
            //Question title
            cell.lblQuestion.text = arrOfUpdateAnswersList[indexPath.row].strQuestionName
            
            //assign tags to all the option buttons so that we can detect which row buttons are clicked
            cell.btnNotAtAll.tag = indexPath.row
            cell.btnSlightly.tag = indexPath.row
            cell.btnModeratley.tag = indexPath.row
            cell.btnVery.tag = indexPath.row
            cell.btnExtremely.tag = indexPath.row
            
            //add target to every option button in order to perform action on there click
            cell.btnNotAtAll.addTarget(self, action: #selector(SelectedOption1(_:)), for: .touchUpInside)
            cell.btnSlightly.addTarget(self, action: #selector(SelectedOption2(_:)), for: .touchUpInside)
            cell.btnModeratley.addTarget(self, action: #selector(SelectedOption3(_:)), for: .touchUpInside)
            cell.btnVery.addTarget(self, action: #selector(SelectedOption4(_:)), for: .touchUpInside)
            cell.btnExtremely.addTarget(self, action: #selector(SelectedOption5(_:)), for: .touchUpInside)
            
            //            cell.btnNotAtAll.setTitle(arrOfUpdateAnswersList[indexPath.row].arrOfOptions[0].strOptionName.uppercased(), for: .normal)
            //            cell.btnSlightly.setTitle(arrOfUpdateAnswersList[indexPath.section].arrOfOptions[1].strOptionName.uppercased(), for: .normal)
            //            cell.btnModeratley.setTitle(arrOfUpdateAnswersList[indexPath.section].arrOfOptions[2].strOptionName.uppercased(), for: .normal)
            //            cell.btnVery.setTitle(arrOfUpdateAnswersList[indexPath.section].arrOfOptions[3].strOptionName.uppercased(), for: .normal)
            //            cell.btnExtremely.setTitle(arrOfUpdateAnswersList[indexPath.section].arrOfOptions[4].strOptionName.uppercased(), for: .normal)
            
            //Assign text to all option buttons
            cell.lblNotAtAll.text = arrOfUpdateAnswersList[indexPath.row].arrOfOptions[0].strOptionName.uppercased()
            cell.lblSlightly.text = arrOfUpdateAnswersList[indexPath.section].arrOfOptions[1].strOptionName.uppercased()
            cell.lblModeratley.text = arrOfUpdateAnswersList[indexPath.section].arrOfOptions[2].strOptionName.uppercased()
            cell.lblVery.text = arrOfUpdateAnswersList[indexPath.section].arrOfOptions[3].strOptionName.uppercased()
            cell.lblExtremely.text = arrOfUpdateAnswersList[indexPath.section].arrOfOptions[4].strOptionName.uppercased()
            
            //Below are the cases when which are checked according to api response
            //case when first option is selected - mark selected option as red
            if arrOfUpdateAnswersList[indexPath.row].arrOfOptions[0].isChecked == true{
                
                cell.lblNotAtAll.textColor = UIColor.white
                cell.btnNotAtAll.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                cell.btnSlightly.backgroundColor = UIColor.clear
                
                cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                cell.btnModeratley.backgroundColor = UIColor.clear
                
                cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                cell.btnVery.backgroundColor = UIColor.clear
                
                cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                cell.btnExtremely.backgroundColor = UIColor.clear
                
            }
                //case when second option is selected - mark selected option as red
            else if arrOfUpdateAnswersList[indexPath.row].arrOfOptions[1].isChecked == true{
                
                cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNotAtAll.backgroundColor = UIColor.clear
                
                cell.lblSlightly.textColor = UIColor.white
                cell.btnSlightly.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                cell.btnModeratley.backgroundColor = UIColor.clear
                
                cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                cell.btnVery.backgroundColor = UIColor.clear
                
                cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                cell.btnExtremely.backgroundColor = UIColor.clear
                
            }
                //case when third option is selected - mark selected option as red
            else if arrOfUpdateAnswersList[indexPath.row].arrOfOptions[2].isChecked == true{
                cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNotAtAll.backgroundColor = UIColor.clear
                
                cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                cell.btnSlightly.backgroundColor = UIColor.clear
                
                cell.lblModeratley.textColor = UIColor.white
                cell.btnModeratley.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                cell.btnVery.backgroundColor = UIColor.clear
                
                cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                cell.btnExtremely.backgroundColor = UIColor.clear
                
            }
                //case when fourth option is selected - mark selected option as red
            else if arrOfUpdateAnswersList[indexPath.row].arrOfOptions[3].isChecked == true{
                
                cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNotAtAll.backgroundColor = UIColor.clear
                
                cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                cell.btnSlightly.backgroundColor = UIColor.clear
                
                cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                cell.btnModeratley.backgroundColor = UIColor.clear
                
                cell.lblVery.textColor = UIColor.white
                cell.btnVery.backgroundColor = ColorCodeConstant.selectedOptionColor
                
                cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                cell.btnExtremely.backgroundColor = UIColor.clear
                
            }
                //case when fifth option is selected - mark selected option as red
            else if arrOfUpdateAnswersList[indexPath.row].arrOfOptions[4].isChecked == true{
                
                cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                cell.btnNotAtAll.backgroundColor = UIColor.clear
                
                cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                cell.btnSlightly.backgroundColor = UIColor.clear
                
                cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                cell.btnModeratley.backgroundColor = UIColor.clear
                
                cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                cell.btnVery.backgroundColor = UIColor.clear
                
                cell.lblExtremely.textColor = UIColor.white
                cell.btnExtremely.backgroundColor = ColorCodeConstant.selectedOptionColor
                
            }
            
            //Below are the cases which are checked when user changes any option
            if arrOfUpdateAnswersList[indexPath.row].selectedIndex  == indexPath.row{
                
                //case when user selects first option
                if arrOfUpdateAnswersList[indexPath.row].strAnswerId == "1" {
                    
                    cell.lblNotAtAll.textColor = UIColor.white
                    cell.btnNotAtAll.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnSlightly.backgroundColor = UIColor.clear
                    
                    cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnModeratley.backgroundColor = UIColor.clear
                    
                    cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnVery.backgroundColor = UIColor.clear
                    
                    cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnExtremely.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects second option
                else if arrOfUpdateAnswersList[indexPath.row].strAnswerId == "2"{
                    
                    cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNotAtAll.backgroundColor = UIColor.clear
                    
                    cell.lblSlightly.textColor = UIColor.white
                    cell.btnSlightly.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnModeratley.backgroundColor = UIColor.clear
                    
                    cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnVery.backgroundColor = UIColor.clear
                    
                    cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnExtremely.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects third option
                else if arrOfUpdateAnswersList[indexPath.row].strAnswerId == "3"{
                    
                    cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNotAtAll.backgroundColor = UIColor.clear
                    
                    cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnSlightly.backgroundColor = UIColor.clear
                    
                    cell.lblModeratley.textColor = UIColor.white
                    cell.btnModeratley.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnVery.backgroundColor = UIColor.clear
                    
                    cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnExtremely.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects fourth option
                else if arrOfUpdateAnswersList[indexPath.row].strAnswerId == "4"{
                    
                    cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNotAtAll.backgroundColor = UIColor.clear
                    
                    cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnSlightly.backgroundColor = UIColor.clear
                    
                    cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnModeratley.backgroundColor = UIColor.clear
                    
                    cell.lblVery.textColor = UIColor.white
                    cell.btnVery.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                    cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnExtremely.backgroundColor = UIColor.clear
                    
                }
                    //case when user selects fifth option
                else if arrOfUpdateAnswersList[indexPath.row].strAnswerId == "5"{
                    
                    cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNotAtAll.backgroundColor = UIColor.clear
                    
                    cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnSlightly.backgroundColor = UIColor.clear
                    
                    cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnModeratley.backgroundColor = UIColor.clear
                    
                    cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnVery.backgroundColor = UIColor.clear
                    
                    cell.lblExtremely.textColor = UIColor.white
                    cell.btnExtremely.backgroundColor = ColorCodeConstant.selectedOptionColor
                    
                }
            }
        }
        else{
            //submit case
            
            //question number
            cell.lblNumber.text = String(indexPath.row + 1)
            //question name
            cell.lblQuestion.text = arrOfQuestionsList[indexPath.row].strQuestionName
            
            //            cell.btnNotAtAll.setTitle(arrOfQuestionsList[indexPath.section].arrOfOptions[0].strOptionName.uppercased(), for: .normal)
            //            cell.btnSlightly.setTitle(arrOfQuestionsList[indexPath.section].arrOfOptions[1].strOptionName.uppercased(), for: .normal)
            //            cell.btnModeratley.setTitle(arrOfQuestionsList[indexPath.section].arrOfOptions[2].strOptionName.uppercased(), for: .normal)
            //            cell.btnVery.setTitle(arrOfQuestionsList[indexPath.section].arrOfOptions[3].strOptionName.uppercased(), for: .normal)
            //            cell.btnExtremely.setTitle(arrOfQuestionsList[indexPath.section].arrOfOptions[4].strOptionName.uppercased(), for: .normal)
            
            //set the options text
            cell.lblNotAtAll.text = arrOfQuestionsList[indexPath.section].arrOfOptions[0].strOptionName.uppercased()
            cell.lblSlightly.text = arrOfQuestionsList[indexPath.section].arrOfOptions[1].strOptionName.uppercased()
            cell.lblModeratley.text = arrOfQuestionsList[indexPath.section].arrOfOptions[2].strOptionName.uppercased()
            cell.lblVery.text = arrOfQuestionsList[indexPath.section].arrOfOptions[3].strOptionName.uppercased()
            cell.lblExtremely.text = arrOfQuestionsList[indexPath.section].arrOfOptions[4].strOptionName.uppercased()
            
            //assign tags to all the option buttons so that we can detect which row buttons are clicked
            cell.btnNotAtAll.tag = indexPath.row
            cell.btnSlightly.tag = indexPath.row
            cell.btnModeratley.tag = indexPath.row
            cell.btnVery.tag = indexPath.row
            cell.btnExtremely.tag = indexPath.row
            
            //add target to every option button in order to perform action on there click
            cell.btnNotAtAll.addTarget(self, action: #selector(SelectedOption1(_:)), for: .touchUpInside)
            cell.btnSlightly.addTarget(self, action: #selector(SelectedOption2(_:)), for: .touchUpInside)
            cell.btnModeratley.addTarget(self, action: #selector(SelectedOption3(_:)), for: .touchUpInside)
            cell.btnVery.addTarget(self, action: #selector(SelectedOption4(_:)), for: .touchUpInside)
            cell.btnExtremely.addTarget(self, action: #selector(SelectedOption5(_:)), for: .touchUpInside)
            
            //Check For saved Answers
            if arrOfSavedAnswers.count != 0 {
                
                print(arrOfSavedAnswers[indexPath.row]["questionId"] as! String , arrOfQuestionsList[indexPath.row].strQuestionId)
                
                if arrOfSavedAnswers[indexPath.row]["questionId"] as! String == arrOfQuestionsList[indexPath.row].strQuestionId {
                    
                    print("Success")
                    
                    //set option as first - mark selected as red
                    if arrOfSavedAnswers[indexPath.row]["optionId"] as! String  == "1"{
                        
                        cell.lblNotAtAll.textColor = UIColor.white
                        cell.btnNotAtAll.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "2"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = UIColor.white
                        cell.btnSlightly.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as third - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "3"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = UIColor.white
                        cell.btnModeratley.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "4"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = UIColor.white
                        cell.btnVery.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fifth - mark selected as red
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "5" {
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = UIColor.white
                        cell.btnExtremely.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                    }
                        //set no option selected
                    else if arrOfSavedAnswers[indexPath.row]["optionId"] as! String == "" {
                        
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                }
                //for new selected
                if arrOfQuestionsList[indexPath.row].selectedIndex == indexPath.row{
                    
                    //set option as first - mark selected as red
                    if arrOfQuestionsList[indexPath.row].strAnswerId == "1" {
                        
                        cell.lblNotAtAll.textColor = UIColor.white
                        cell.btnNotAtAll.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "2"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = UIColor.white
                        cell.btnSlightly.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as third - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "3"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = UIColor.white
                        cell.btnModeratley.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "4"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = UIColor.white
                        cell.btnVery.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fifth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "5"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = UIColor.white
                        cell.btnExtremely.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                    }
                }
            }
            else{
                //when no saved answers
                if arrOfQuestionsList[indexPath.row].selectedIndex == indexPath.row{
                    
                    //set option as first - mark selected as red
                    if arrOfQuestionsList[indexPath.row].strAnswerId == "1" {
                        
                        cell.lblNotAtAll.textColor = UIColor.white
                        cell.btnNotAtAll.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as second - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "2"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = UIColor.white
                        cell.btnSlightly.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as third - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "3"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = UIColor.white
                        cell.btnModeratley.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                        
                    }
                        //set option as fourth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "4"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = UIColor.white
                        cell.btnVery.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                        cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnExtremely.backgroundColor = UIColor.clear
                        
                    }
                        //set option as fifth - mark selected as red
                    else if arrOfQuestionsList[indexPath.row].strAnswerId == "5"{
                        
                        cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnNotAtAll.backgroundColor = UIColor.clear
                        
                        cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnSlightly.backgroundColor = UIColor.clear
                        
                        cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnModeratley.backgroundColor = UIColor.clear
                        
                        cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                        cell.btnVery.backgroundColor = UIColor.clear
                        
                        cell.lblExtremely.textColor = UIColor.white
                        cell.btnExtremely.backgroundColor = ColorCodeConstant.selectedOptionColor
                        
                    }
                }
                    
                else{
                    
                    cell.lblNotAtAll.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnNotAtAll.backgroundColor = UIColor.clear
                    
                    cell.lblSlightly.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnSlightly.backgroundColor = UIColor.clear
                    
                    cell.lblModeratley.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnModeratley.backgroundColor = UIColor.clear
                    
                    cell.lblVery.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnVery.backgroundColor = UIColor.clear
                    
                    cell.lblExtremely.textColor = ColorCodeConstant.darkTextcolor
                    cell.btnExtremely.backgroundColor = UIColor.clear
                    
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: - cellButtonAction
    /**
     This function is called when first option is clicked
     */
    @objc func SelectedOption1(_ sender: UIButton) {
        
        if isPersonalityTypeAnsDone == true{
            //case when user is updating answers
            arrOfUpdateAnswersList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].strAnswerId = "1"
            tblView.reloadData()
            
        }else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "1"
            tblView.reloadData()
            print("btnNotAtAll")
        }
    }
    
    /**
     This function is called when second option is clicked
     */
    @objc func SelectedOption2(_ sender: UIButton) {
        
        if isPersonalityTypeAnsDone == true{
            //case when user is updating answers
            arrOfUpdateAnswersList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].strAnswerId = "2"
            tblView.reloadData()
            
        }else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "2"
            tblView.reloadData()
            print("btnSlightly")
        }
    }
    
    /**
     This function is called when third option is clicked
     */
    @objc func SelectedOption3(_ sender: UIButton) {
        
        if isPersonalityTypeAnsDone == true{
            //case when user is updating answers
            arrOfUpdateAnswersList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].strAnswerId = "3"
            tblView.reloadData()
            
        }else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "3"
            tblView.reloadData()
            print("btnModeratley")
        }
    }
    
    /**
     This function is called when fourth option is clicked
     */
    @objc func SelectedOption4(_ sender: UIButton) {
        
        if isPersonalityTypeAnsDone == true{
            //case when user is updating answers
            arrOfUpdateAnswersList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].strAnswerId = "4"
            tblView.reloadData()
            
        }else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "4"
            tblView.reloadData()
            print("btnVery")
        }
    }
    
    /**
     This function is called when fifth option is clicked
     */
    @objc func SelectedOption5(_ sender: UIButton) {
        
        if isPersonalityTypeAnsDone == true{
            //case when user is updating answers
            arrOfUpdateAnswersList[sender.tag].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].strAnswerId = "5"
            tblView.reloadData()
            
        }else{
            //this variables tells that user has changed something and we need to show him save popup on back click
            checkVariable = "filled One"
            arrOfQuestionsList[sender.tag].selectedIndex = sender.tag
            arrOfQuestionsList[sender.tag].strAnswerId = "5"
            tblView.reloadData()
            print("btnExtremely")
        }
    }
    
    //MARK: - call web service
    /**
     API call - when user saves personality type answers
     */
    func callWebServiceToAddAnswers() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            //array of answers as json dict
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.addPersonalityTypeAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer added succeessfully.")
                    
                    //remove saved answers keys which tells users has previously saved answers
                    UserDefaults.standard.removeObject(forKey: "SavedDictForPersonalityType")
                    UserDefaults.standard.removeObject(forKey: "isSaved")
                    
                    let keyValue = UserDefaults.standard.value(forKey: "SavedDictForPersonalityType")
                    print("Key Value after remove \(String(describing: keyValue))")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - get personality type questions to list
     */
    func callWebServiceTogetPersonalityTypeQuestionList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Diagnostic.getPersonalityTypeQuestionList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse question list and return array of model named: DiagonisticQuestionsModel
                    DiagnosticParser.parseDiagonisticQuestionList(response: response!, completionHandler: { (arrOfQuestionList) in
                        //save array
                        self.arrOfQuestionsList = arrOfQuestionList
                    })
                    
                    //update the questions according to the previouly saved answers
                    self.SetSelctedIndexAndAnwerIdForSavedData()
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - get personality type answers list
     */
    func callWebServiceTogetPersonalityTypeAnsweredList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            :] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.getPersonalityTypeCompletedAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            //        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.getPersonalityTypeCompletedAnswers, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse personality type answers and returns answers list
                    DiagnosticParser.parseDiagonisticGetAnsweredList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfUpdateAnswersList = arrOfQuestionList
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API call - update the personality type answers
     */
    func callWebServiceToUpdateAnswer(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            //answers list as json array
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Diagnostic.updatePersonalityTypeAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer updated succeessfully.")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
