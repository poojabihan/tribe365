//
//  summaryTblCell.swift
//  Tribe365
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
* No longer in use
*/
class summaryTblCell: UITableViewCell {

    @IBOutlet weak var lblSummaryTitle: UILabel!
    @IBOutlet weak var lblSummaryDescription: UILabel!
    @IBOutlet weak var heightconstraint: NSLayoutConstraint!
    @IBOutlet weak var btnReview: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
