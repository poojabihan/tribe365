//
//  QuestionnaireView.swift
//  Tribe365
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
* No longer in use
*/
class QuestionnaireView: UIViewController {

    //MARK: - Variable
    var panel = JKNotificationPanel()
    var strMailSent = ""
    var strDate = ""
    
    @IBOutlet weak var btnRequestLink: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if strMailSent == "1"{
            
          //Mail ja chuka hai but questions k answer nahi feed kiye hai
            //" Link to questinair has been sent"
            
            btnRequestLink.setTitle("LINK TO QUESTIONNAIRE HAS BEEN SENT", for: .normal)
            btnRequestLink.alpha = 0.6
            btnRequestLink.isEnabled = false
        }
        
        if strDate != ""{
        lblDate.text = strDate
        }
        // Do any additional setup after loading the view.
    }

    //MARK: - Action Methods
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnRequestQuestionAction(_ sender: Any) {
        
        callWebServiceRequestForQuestion()
    }
    //MARK: - CallWebService
    
    func callWebServiceRequestForQuestion() {
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.FunctionalLens.requestQuestionnaireList, param: nil, withHeader: true ) { (response, errorMsg) in
            
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                     self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue )
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
