///Users/apple/Desktop/Tribe365Bitbucket/Tribe365/ViewControllers/COT
//  FunctionalLensTribeTips.swift
//  Tribe365
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import  NVActivityIndicatorView
import  JKNotificationPanel

/**
 * This class is no longer in use
 */
class FunctionalLensTribeTipsDOT: UIViewController , UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnContinue: UIButton!

    //MARK: - Variable
    var arrOfUserList = [ResponsiblePersonModel]()
    var filteredData = [ResponsiblePersonModel]()
    var selectedData = [ResponsiblePersonModel]()

    var panel = JKNotificationPanel()
    var orgID = ""
    var strMyType = ""
    var SOTObj = ""
    //for buble page varia
    var strDotId = ""
    var strValueId = ""
    var strId =  ""
    var strBeliefId = ""
    var strValueName = ""
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        callWebServiceToGetResponsiblePersonList()
    }
    
    /*Search bar Customisation*/
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
//        
//        searchBar.layer.cornerRadius = 20
//        searchBar.clipsToBounds = true
//        searchBar.backgroundColor = UIColor.white
//        searchTextField.backgroundColor = UIColor.white
//        searchTextField.layer.cornerRadius = 18.5
//        searchTextField.borderStyle = .none
//        searchTextField.font = UIFont.systemFont(ofSize: 14.0)
//        searchTextField.textAlignment = NSTextAlignment.left
//        let image:UIImage = UIImage(named: "search")!
//        let imageView:UIImageView = UIImageView.init(image: image)
//        searchTextField.rightView = nil
//        searchTextField.placeholder = "  Search"
//        searchTextField.leftView = imageView
//        searchTextField.rightViewMode = UITextFieldViewMode.always
        
    }
    
    //MARK: - Action Methods
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        callWebServiceForLikeUnlike()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell") as! UserInfoCell
        
        cell.lblUserName.text = filteredData[indexPath.row].strName
        cell.lblEmail.text = filteredData[indexPath.row].strEmail
        
        if selectedData.contains(filteredData[indexPath.row]) {
            cell.viewForCell.backgroundColor = UIColor.init(hexString: "#71c390")
        }
        else {
            cell.viewForCell.backgroundColor = UIColor.white
        }
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedData.contains(filteredData[indexPath.row]) {
            selectedData.remove(at: selectedData.index(of: filteredData[indexPath.row])!)
        }
        else {
            selectedData.append(filteredData[indexPath.row])
        }
        
        if selectedData.count > 0 {
            self.btnContinue.isHidden = false
        }
        else {
            self.btnContinue.isHidden = true
        }
        
        tblView.reloadData()
        
       /* //For ReportsDOT
        
            let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "BubbleView") as! BubbleView
            
            objVC.modalPresentationStyle = .overCurrentContext
            
            objVC.strOrgID = filteredData[indexPath.row].strOrgId
            objVC.strUserID = filteredData[indexPath.row].strId
            objVC.strUserName = filteredData[indexPath.row].strName
            
            objVC.strDotId = strDotId
            objVC.strValueId = strValueId
            objVC.strId = strId
            objVC.strBeliefId = strBeliefId
            objVC.strValueName = strValueName
            self.navigationController?.present(objVC, animated: false, completion: nil)
            */
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? arrOfUserList : arrOfUserList.filter { (item: ResponsiblePersonModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblView.reloadData()
    }
    
    func callWebServiceToGetResponsiblePersonList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*{
         
         "type":"organisation",
         "typeId":"1"
         
         }
         */
        let param =
            [
                "type"   : "organisation",
                "typeId" : orgID
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getUserByType, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    
                    DashboardParser.parseResponsiblePresonList(response: response!, completionHandler: { (arrRespnsiblePreseonList) in
                        
                        self.filteredData = arrRespnsiblePreseonList
                        self.arrOfUserList = arrRespnsiblePreseonList
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    func callWebServiceForLikeUnlike() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var arrUserIDs = [String]()
        for value in selectedData {
            arrUserIDs.append(value.strId)
        }
        
        let param = ["toUserId": arrUserIDs,
                     "dotId": strDotId,
                     "dotBeliefId": strBeliefId,
                     "dotValueId": strId,
                     "dotValueNameId": strValueId,
                     "bubbleFlag": "1"
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.addBubbleRatings, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Kudos sent successfully.")
                    
                    self.btnBackAction(self)
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    

}
