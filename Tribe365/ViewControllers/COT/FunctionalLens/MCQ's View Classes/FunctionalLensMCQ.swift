//
//  FonctionalLensMCQ.swift
//  Tribe365
//
//  Created by Apple on 05/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * No longer in use
 */
class FunctionalLensMCQ: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOultes
    @IBOutlet weak var tblView: UITableView!

    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK: - Variables
    var selectedDict = [FunctionLenseQuestionModel]()
    var arrOfQuestionsList = [FunctionLenseQuestionModel]()
    var arrOfUpdatedAnswerList = [FunctionLenseQuestionModel]()
    var panel = JKNotificationPanel()
    var selectedQuestionIndex = -1
    var arrOfAnswers = [[String : Any]]()
    var checkVariable = ""
    var comingFromReduReviewBtn = Bool()
    var arrOfSavedAnswers = [[String : Any]]()
    var isSaved = false

    
    //MARK: - view Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        let keyValue = UserDefaults.standard.value(forKey: "isSavedFC")
        print("Key Value after remove \(String(describing: keyValue))")
        
        if comingFromReduReviewBtn == true{
            
       lblHeader.text = "UPDATE/REVIEW PERSONALITY TYPE QUESTIONS"
         btnSend.setTitle("UPDATE", for: .normal)
        callWebServiceTogetCOTFunctionalLensMCQAnswers()
        }
        else{
            isSaved = (UserDefaults.standard.value(forKey: "isSavedFC") != nil)
            if isSaved == true {
                arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForFC") as! [[String : Any]]
                print(arrOfSavedAnswers)
            }
            
        callWebServiceTogetCOTFunctionLensMCQ()
        }
    }
    
    
    //MARK: - IBActions
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if arrOfUpdatedAnswerList.count != 0{
            
            arrOfAnswers.removeAll()
            for j in (0..<self.arrOfUpdatedAnswerList.count){
                
                if arrOfUpdatedAnswerList[j].selectedIndex != -1 {
                    
                    let strAnsId =  self.arrOfUpdatedAnswerList[j].strAPIAnswerID
                    print(strAnsId)
                    
                    let strOption = self.arrOfUpdatedAnswerList[j].strAnswerId
                    
                    let json = [  "answerId": strAnsId,
                                  "optionId": strOption]
                    
                    arrOfAnswers.append(json)
                }
                else{
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            print(arrOfAnswers)
            callWebServiceToUpdateAnswers()
        }
            
        else {
            
            arrOfAnswers.removeAll()
            for j in (0..<self.arrOfQuestionsList.count){
                
                if arrOfQuestionsList[j].selectedIndex != -1 {
                    
                    let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                    print(strAnsId)
                    
                    let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                    print(strQuestionId)
                    
                    let json = [  "questionId": strQuestionId,
                                 "optionId": strAnsId]
                    
                    arrOfAnswers.append(json)
                }
                else{
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            print(arrOfAnswers)
            callWebServiceToAddAnswers()
    }
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
       /* Answers will not be saved!
        Are you sure you want to go back?
        Yes No*/
        if comingFromReduReviewBtn == true{
            goToBackScreen()
        }
        else{
        if checkVariable == "filled One"{
        let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
        
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.saveAnswers()
                
                NSLog("Save")
            }
            
            alertController.addAction(okAction)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            //run your function here
            self.goToBackScreen()
        }))
        self.present(alertController, animated: true, completion: nil)
        }
        else{
           goToBackScreen()
        }
        }
    }
    
    func goToBackScreen(){
        
        self.navigationController?.popViewController(animated: true)

    }
    
    func saveAnswers(){
        
        //FIRST remove previous saved answers annd add new
        UserDefaults.standard.removeObject(forKey: "SavedDictForFC")
        let keyValue = UserDefaults.standard.value(forKey: "SavedDictForFC")
        print("Key Value after remove \(String(describing: keyValue))")
        
        //SECOND create new data of answer
        arrOfSavedAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
            
                let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                print(strAnsId)
                
                let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                print(strQuestionId)
                
                let json = [  "questionId": strQuestionId,
                              "optionId": strAnsId]
                
                arrOfSavedAnswers.append(json)
           
        }
        print(arrOfSavedAnswers)
        
        //Third add new answer list
        UserDefaults.standard.set(true, forKey: "isSavedFC")
        UserDefaults.standard.set(arrOfSavedAnswers, forKey: "SavedDictForFC")
        
        //Fourth print answer list
        arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForFC") as! [[String : Any]]
        print(arrOfSavedAnswers)
        
        //go back to previous controller
        self.navigationController?.popViewController(animated: true)
    }
    
    func SetSelctedIndexAndAnwerIdForSavedData()  {
        if self.isSaved == true{
            
            for i in (0..<self.arrOfQuestionsList.count){
                for j in (0..<self.arrOfSavedAnswers.count){
                    
                    
                    
                    if arrOfSavedAnswers[j]["questionId"] as! String == arrOfQuestionsList[i].strQuestionId{
                        
                        let stroptnId =  arrOfSavedAnswers[j]["optionId"] as! String
                        print("Current savedOption Id is \(stroptnId)")
                        
                        if stroptnId == ""{
                            
                            arrOfQuestionsList[i].selectedIndex = -1
                            arrOfQuestionsList[i].arrOfOptions[0].isChecked = false
                            arrOfQuestionsList[i].arrOfOptions[1].isChecked = false
                            
                        }
                        else if stroptnId == arrOfQuestionsList[i].arrOfOptions[0].strOptionId{
                            
                            print("\(stroptnId) and \(arrOfQuestionsList[i].arrOfOptions[0].strOptionId) are same ")
                            
                        arrOfQuestionsList[i].selectedIndex = 0
                        arrOfQuestionsList[i].arrOfOptions[0].isChecked = true
                        arrOfQuestionsList[i].arrOfOptions[1].isChecked = false
                        arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[j]["optionId"] as! String
                        }
                            
                        else if stroptnId == arrOfQuestionsList[i].arrOfOptions[1].strOptionId {
                            print("\(stroptnId) and \(arrOfQuestionsList[i].arrOfOptions[1].strOptionId) are not same ")
                            
                            arrOfQuestionsList[i].selectedIndex = 1
                            arrOfQuestionsList[i].arrOfOptions[1].isChecked = true
                            arrOfQuestionsList[i].arrOfOptions[0].isChecked = false
                            
                            arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[j]["optionId"] as! String
                            
                        }
                        
                        
                        
                    }
                }
                
            }
            
        }
        
    }
    
    //MARK: - UITableView Delegate And DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if arrOfUpdatedAnswerList.count != 0 {
            return arrOfUpdatedAnswerList.count
            
        }
        else{
            return arrOfQuestionsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrOfUpdatedAnswerList.count != 0 {
            return arrOfUpdatedAnswerList[section].arrOfOptions.count

        }
        else{
        return arrOfQuestionsList[section].arrOfOptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 3 , y: 0, width: tableView.frame.width - 2 , height: 55))
        
        headerView.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        headerView.layer.borderWidth = 1.0
        headerView.backgroundColor = UIColor.white
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width - 15 , height: headerView.frame.height - 10)
        if arrOfUpdatedAnswerList.count != 0 {
            
            label.text =  arrOfUpdatedAnswerList[section].strQuestionId + ". " + arrOfUpdatedAnswerList[section].strQuestionName

        }
        else{
        label.text =  arrOfQuestionsList[section].strQuestionId + ". " + arrOfQuestionsList[section].strQuestionName
        }
        label.textAlignment = .natural
        label.backgroundColor = UIColor(red: 225.0, green: 225.0, blue: 225.0, alpha: 0.6)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)// my custom font
        label.textColor = ColorCodeConstant.darkTextcolor // my custom colour
        
        
        headerView.addSubview(label)
        
        return headerView
    }
    
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 10))
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
            let cell : SOTStatementsCell = tableView.dequeueReusableCell(withIdentifier: "SOTStatementsCellNew") as! SOTStatementsCell
        
        if arrOfUpdatedAnswerList.count != 0 {
        
            cell.lblStatmentFirst.text = arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[indexPath.row].strOptionName.capitalized
            
         if arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[indexPath.row].isChecked == true {

                cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
            }
            else {
                cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
            }
        
        }
        
        else{
            cell.lblStatmentFirst.text = arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row].strOptionName.capitalized
            
            
            if arrOfSavedAnswers.count != 0 {
                
                if arrOfSavedAnswers[indexPath.section]["questionId"] as! String == arrOfQuestionsList[indexPath.section].strQuestionId{
                    
                    let stroptnId =  arrOfSavedAnswers[indexPath.section]["optionId"] as! String
                    print("Current savedOption Id is \(stroptnId)")
                    print("\(indexPath.row) of Section \(indexPath.section)")
                    
                    if stroptnId == arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row].strOptionId{
                      
                        print("\(stroptnId) and \(arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row].strOptionId) are same ")
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "check")

                        
                    }
                    
                    else{
                         print("\(stroptnId) and \(arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row].strOptionId) are not same ")
                        
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")

                    }
                
                   }
                
                //For new Selection
                if arrOfQuestionsList[indexPath.section].selectedIndex == indexPath.row {
                    cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
                }
                else {
                    cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
                }
            }
            
            else{
            if arrOfQuestionsList[indexPath.section].selectedIndex == indexPath.row {
                cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
            }
            else {
                cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
            }
            }
            
            if indexPath.row - 1 == arrOfQuestionsList[indexPath.row].arrOfOptions.count - 1 {
                cell.borderViewBottom.constant = 0
            }
            else {
                cell.borderViewBottom.constant = -1
            }
            }
        
            return cell
      
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var sectionHeaderHeight = CGFloat()
        sectionHeaderHeight = 50
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y >= 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        checkVariable = "filled One"
        if arrOfUpdatedAnswerList.count != 0{
            
        arrOfUpdatedAnswerList[indexPath.section].selectedIndex = indexPath.row
            
            if indexPath.row == 0{
        arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[0].isChecked = true
        arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[1].isChecked = false
                
            }
            else if indexPath.row == 1{
               arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[0].isChecked = false
                arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[1].isChecked = true
            }
    
   
            arrOfUpdatedAnswerList[indexPath.section].strAnswerId = arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[indexPath.row].strOptionId

            //arrOfUpdatedAnswerList[indexPath.section].strAnswerId
            
    print(arrOfUpdatedAnswerList[indexPath.section].arrOfOptions[indexPath.row].strOptionId)
            
            tableView.reloadData()
        }
        else{
        arrOfQuestionsList[indexPath.section].selectedIndex = indexPath.row
        arrOfQuestionsList[indexPath.section].strAnswerId = arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row].strOptionId
       
    print(arrOfQuestionsList[indexPath.section].arrOfOptions[indexPath.row ].strOptionId)
            
            tableView.reloadData()
        }
    }


    
    //MARK: - Call Web Service
    func callWebServiceTogetCOTFunctionalLensMCQAnswers(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.FunctionalLens.getCOTfuncLensCompletedAnswers, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                COTParser.parseCOTFunctionalLenseMCQAnswerList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfUpdatedAnswerList = arrOfQuestionList
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    func callWebServiceTogetCOTFunctionLensMCQ(){
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.FunctionalLens.getCOTfunctionalLensQuestionsList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    COTParser.parseCOTFunctionalLenseMCQList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfQuestionsList = arrOfQuestionList
                    })
                    
                    self.SetSelctedIndexAndAnwerIdForSavedData()
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    func callWebServiceToAddAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.FunctionalLens.addCOTfunctionalLensAnswer, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer added succeessfully.")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForFC")
                    UserDefaults.standard.removeObject(forKey: "isSavedFC")
                    let keyValue = UserDefaults.standard.value(forKey: "isSavedFC")
                    print("Key Value after remove \(String(describing: keyValue))")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    func callWebServiceToUpdateAnswers(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.FunctionalLens.updateCOTfunLensAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer Updated succeessfully.")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
}
