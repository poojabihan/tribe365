//
//  FunctionalLensView.swift
//  Tribe365
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class FunctionalLensView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource{
    
    //MARK: - Variables
    var arrOfFuncLensKeyDetail = [FuncLensKeyDetailModel]()
    var arrOfIntialValueList = [InitialValueListModel]()
    var arrOfTribeTips = [TribeTipsArray]()
    var arrOfAllCombinationsValue = [AllCombinationModel]()
    var panel = JKNotificationPanel()
    var arrForAllSummary = [ModelForAllSummary]()
    var strOrgID = ""
    var strUserID = ""
    var strMyType = ""
    var strUserName = ""
    //for hight the border of selected cell
    var selectedIndexCollectionDichotomies = -1
    var selectedIndexCollectionScores = -1
    var isCOTfunclensAnswersDone = Bool()
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionViewOfDichotomies: UICollectionView!
    @IBOutlet weak var collectionViewOfScore: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoRecordsAvailabel: UILabel!
    @IBOutlet weak var btnIntialResult: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblSummary: UILabel!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var HeightOfButtonViewTeam: NSLayoutConstraint!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    @IBOutlet weak var HeightOfRedoReviewbtn: NSLayoutConstraint!
    //MARK: -  View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        callWebServiceForFunctionalDetails()
        tblView.separatorStyle = .none
        
        if AuthModel.sharedInstance.role == kUser{
            
            if strMyType == "team"{
                btnSend.isHidden = true
                lblUserName.text = strUserName
                HeightOfButtonViewTeam.constant = 0
                HeightOfRedoReviewbtn.constant = 0
                
            }
            else{
                btnSend.isHidden = false
                
                lblUserName.text = AuthModel.sharedInstance.name.capitalized
            }
        }
        else{
            btnSend.isHidden = true
            lblUserName.text = strUserName
            HeightOfButtonViewTeam.constant = 0
            HeightOfRedoReviewbtn.constant = 0
            
        }
    }
    //MARK: - Action Methods
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnRedoReviewAction(_ sender: Any) {
        
//        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
//
//        objVC.comingFromReduReviewBtn = true
//        self.navigationController?.pushViewController(objVC, animated: true)
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
        //pass this to detect update case or submit case
        objVC.isPersonalityTypeAnsDone = true //isPersonailtyTypeAnswered
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    @IBAction func btnViewTeamAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTips") as! FunctionalLensTribeTips
        
        objVC.orgID = AuthModel.sharedInstance.orgId
        objVC.strMyType = "team"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    @IBAction func btnIntialResultAction(_ sender: UIButton) {
        
        btnIntialResult.tag = 1
        arrForAllSummary.removeAll()
        if btnIntialResult.tag == 1{
            
            if self.strMyType == "team"{
                if AuthModel.sharedInstance.role == kSuperAdmin{
                    
                    self.CreateIntialResultData()
                    
                }
                else{
                    CreateTeamResultData()
                }
            }
            else{
                CreateIntialResultData()
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViewFullResultAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeResultView") as! PersonalityTypeResultView
        objVC.isPersonailtyTypeAnswered = true //isPersonalityTypeAns
        self.navigationController?.pushViewController(objVC, animated: true)

    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView  == collectionViewOfDichotomies
        {
            return arrOfFuncLensKeyDetail.count
            
        }
        else{
            
            return arrOfFuncLensKeyDetail.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunctionalLensCollectionViewCell", for: indexPath as IndexPath) as! FunctionalLensCollectionViewCell
        
        if collectionView  == collectionViewOfDichotomies
        {
            //condition for border color
            if indexPath.row == selectedIndexCollectionDichotomies {
                cell.lblTxt.layer.borderWidth = 1.0
                cell.lblTxt.layer.borderColor = ColorCodeConstant.borderRedColor.cgColor
            }
            else {
                cell.lblTxt.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            }
            //Printing text
            cell.lblTxt.text = arrOfFuncLensKeyDetail[indexPath.row].strValue
            return cell
            
        }
        else{
            
            let strValue = arrOfFuncLensKeyDetail[indexPath.row].strValue
            print(strValue)
            for j in (0..<self.arrOfIntialValueList.count){
                
                cell.lblTxt.text = "0"
                cell.lblTxt.textColor = ColorCodeConstant.darkTextcolor
                //condition for border color
                if indexPath.row == selectedIndexCollectionScores {
                    cell.lblTxt.layer.borderWidth = 1.0
                    cell.lblTxt.layer.borderColor = ColorCodeConstant.borderRedColor.cgColor
                }
                else {
                    cell.lblTxt.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
                }
                //get 1st word of statement
                
                let onlineString:String =  arrOfIntialValueList[j].strTitle
                
                let substring:String = onlineString.components(separatedBy: " ")[0]
                
                print(substring) // <ONLINE>
                
                //Set Color According to the statements
                
                if substring.lowercased().contains("very")
                {
                    //for very clear --> set Red (e98285)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "e98285").withAlphaComponent(0.5)
                }
                if substring.lowercased().contains("clear")
                {
                    //for clear --> set Orange (f1b48a)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "f1b48a").withAlphaComponent(0.5)
                    
                }
                if substring.lowercased().contains("moderate")
                {
                    //for moderate --> set Light Green (84d2a0)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "84d2a0").withAlphaComponent(0.5)
                    
                }
                if substring.lowercased().contains("slight")
                {
                    //for slight --> set Green (00b050)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "00b050").withAlphaComponent(0.5)
                    
                }
                if substring == ""
                {
                    //for slight --> set Green (00b050)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "999896").withAlphaComponent(0.1)
                    cell.lblTxt.textColor = ColorCodeConstant.darkTextcolor.withAlphaComponent(0.3)
                    
                }
                
                //Printing text
                if strValue == arrOfIntialValueList[j].strValue {
                    print("found Score: ", arrOfIntialValueList[j].strScore)
                    cell.lblTxt.text = arrOfIntialValueList[j].strScore
                    break
                }
            }
            return cell
        }
        
    }
    
    //MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        btnIntialResult.tag = 0
        if btnIntialResult.tag == 0{
            
            btnIntialResult.isHidden = false
        }
        
        if collectionView == collectionViewOfDichotomies
        {
            
            createFunctionalArrForSelectedIndex(indexPath: indexPath.row)
            
        }
        else if collectionView == collectionViewOfScore{
            
            collectionView.isUserInteractionEnabled = true
            if arrOfIntialValueList[indexPath.row].strScore == "0"{
                return
            }
                
            else{
                createIntialArrForSelectedIndex(indexPath: indexPath.item)
            }
        }
    }
    
    
    //MARK: - Custom Methods
    func CreateIntialResultData(){
        
        selectedIndexCollectionDichotomies = -1
        selectedIndexCollectionScores = -1
        
        self.btnIntialResult.isHidden = true
        
        if self.arrOfAllCombinationsValue.count > 0 {
            
            arrForAllSummary.removeAll()
            lblSummary.isHidden = false
            //Append Summary
            let model = ModelForAllSummary()
            
            model.strtitle = /*"Summary: "*/self.arrOfAllCombinationsValue[0].strTitle.uppercased()
            
            model.strValue = NSMutableAttributedString.init(string: self.arrOfAllCombinationsValue[0].strSummary)
            
            self.arrForAllSummary.append(model)
            
            
        }
        else{
            
            lblSummary.isHidden = true
            tblView.reloadData()
        }
        
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
        tblView.reloadData()
        
    }
    func CreateTeamResultData(){
        
        selectedIndexCollectionDichotomies = -1
        selectedIndexCollectionScores = -1
        
        self.btnIntialResult.isHidden = true
        if self.arrOfTribeTips.count > 0 {
            
            arrForAllSummary.removeAll()
            
            //Append Summary
            let model = ModelForAllSummary()
            
            model.strtitle = /*"Summary: "*/self.arrOfTribeTips[0].strTitle.uppercased()
            
            model.strValue = NSMutableAttributedString.init(string: self.arrOfTribeTips[0].strSummary.capitalized)
            
            self.arrForAllSummary.append(model)
            
            var strSeek = ""
            for i in (0..<self.arrOfTribeTips[0].arrSeekValues.count){
                
                let model = ModelForAllSummary()
                
                model.strtitle = self.arrOfTribeTips[0].arrSeekValues[i].strValueType.capitalized
                
                
                if strSeek == "" {
                    strSeek = "\u{2022} " + self.arrOfTribeTips[0].arrSeekValues[i].strValue
                }
                else {
                    strSeek = strSeek + "\n\u{2022} " +  self.arrOfTribeTips[0].arrSeekValues[i].strValue
                }
                
            }
            
            if self.arrOfTribeTips[0].arrSeekValues.count > 0 {
                let model = ModelForAllSummary()
                model.strtitle = self.arrOfTribeTips[0].arrSeekValues[0].strValueType.capitalized
                model.strValue = NSMutableAttributedString.init(string: strSeek)
                self.arrForAllSummary.append(model)
            }
            
            
            var strPersuade = ""
            
            for i in (0..<self.arrOfTribeTips[0].arrPersuadeValues.count){
                
                let model = ModelForAllSummary()
                
                model.strtitle = self.arrOfTribeTips[0].arrPersuadeValues[i].strValueType.capitalized
                
                //                        model.strValue = self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                
                if strPersuade == "" {
                    strPersuade = "\u{2022} " + self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                }
                else {
                    strPersuade = strPersuade + "\n\u{2022} " + self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                }
                
            }
            
            if self.arrOfTribeTips[0].arrPersuadeValues.count > 0 {
                let model = ModelForAllSummary()
                model.strtitle = self.arrOfTribeTips[0].arrPersuadeValues[0].strValueType.capitalized
                model.strValue = NSMutableAttributedString.init(string: strPersuade)
                self.arrForAllSummary.append(model)
            }
            
        }
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
        tblView.reloadData()
    }
    
    
    func createFunctionalArrForSelectedIndex(indexPath : Int){
        
        selectedIndexCollectionDichotomies = indexPath
        selectedIndexCollectionScores = -1
        
        arrForAllSummary.removeAll()
        let model = ModelForAllSummary()
        
        model.strtitle = self.arrOfFuncLensKeyDetail[indexPath].strTitle
        model.strValue = NSMutableAttributedString(string: self.arrOfFuncLensKeyDetail[indexPath].strdescription)
        
        self.arrForAllSummary.append(model)
        tblView.reloadData()
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
    }
    
    func createIntialArrForSelectedIndex(indexPath : Int){
        
        selectedIndexCollectionScores = indexPath
        selectedIndexCollectionDichotomies = -1
        
        arrForAllSummary.removeAll()
        
        let model = ModelForAllSummary()
        
        model.strtitle = self.arrOfIntialValueList[indexPath].strTitle
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold("Positives:")
            .normal(self.arrOfIntialValueList[indexPath].strPositives + "\n")
            .bold("Allowable Weaknesses: ")
            .normal(self.arrOfIntialValueList[indexPath].strAllowableWeaknesses)
        
        model.strValue = formattedString
        self.arrForAllSummary.append(model)
        
        tblView.reloadData()
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
    }
    
    
    func setColorAccordingToState() {
        
        
    }
    //MARK: - UITableViewDelegate and Datasources
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrForAllSummary.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : summaryTblCell = tableView.dequeueReusableCell(withIdentifier: "summaryTblCell") as! summaryTblCell
        
        if arrForAllSummary[indexPath.row].strtitle == ""{
            lblSummary.isHidden = true
            cell.lblSummaryTitle.isHidden = true
            cell.lblSummaryDescription.isHidden = true
        }
        else{
            lblSummary.isHidden = false
            cell.lblSummaryTitle.isHidden = false
            cell.lblSummaryDescription.isHidden = false
            cell.lblSummaryTitle.text = arrForAllSummary[indexPath.row].strtitle
            cell.lblSummaryDescription.attributedText = arrForAllSummary[indexPath.row].strValue
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    //MARK: - callWeb Service
    func callWebServiceForFunctionalDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID,
                     "userId": strUserID] as [String : Any]
        
        print("+++++: ",param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.FunctionalLens.getCOTFunctionalLensDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseCOTFunctionalLensDetail(response: response!, completionHandler: { (arrOfFunctionalLens,arrOfIntialValues,arrtribeTips,arrOfAllCombination, isCOTfunclensAnswersDone ) in
                        
                        self.arrOfFuncLensKeyDetail = arrOfFunctionalLens
                        self.arrOfIntialValueList = arrOfIntialValues
                        self.arrOfTribeTips = arrtribeTips
                        self.arrOfAllCombinationsValue = arrOfAllCombination
                        self.isCOTfunclensAnswersDone = isCOTfunclensAnswersDone
                    })
                    
                    if self.isCOTfunclensAnswersDone == false {
                        self.tblView.isHidden = true
                        self.lblNoRecordsAvailabel.isHidden = false
                        self.collectionViewOfScore.isHidden = true
                        self.collectionViewOfDichotomies.isHidden = true
                    }
                    else{
                        self.tblView.isHidden = false
                        self.collectionViewOfScore.isHidden = false
                        self.collectionViewOfDichotomies.isHidden = false
                        self.lblNoRecordsAvailabel.isHidden = true
                    if self.arrOfFuncLensKeyDetail.count == 0 {
                        self.lblNoRecordsAvailabel.isHidden = false
                        return
                    }
                    else {
                        self.tblView.isHidden = false
                    }
                    
                    self.collectionViewOfScore.reloadData()
                    self.collectionViewOfDichotomies.reloadData()
                    
                    //call single Array for Summary
                    print(self.strMyType)
                    if self.strMyType == "team"{
                        
                        if AuthModel.sharedInstance.role == kSuperAdmin{
                            
                            self.CreateIntialResultData()
                            
                        }
                        else{
                            for i in (0..<self.arrOfFuncLensKeyDetail.count){
                                
                                if i == 0 || i == 2{
                                    self.arrOfFuncLensKeyDetail.remove(at: i)
                                }
                                /*else if i == 2{
                                 self.arrOfFuncLensKeyDetail.remove(at: i)
                                 }*/
                            }
                            for i in (0..<self.arrOfIntialValueList.count){
                                
                                if i == 0 || i == 2{
                                    self.arrOfIntialValueList.remove(at: i)
                                }
                                /*else if i == 2{
                                 self.arrOfFuncLensKeyDetail.remove(at: i)
                                 }*/
                            }
                            print(self.arrOfFuncLensKeyDetail.count)
                            self.CreateTeamResultData()
                        }
                    }
                    else{
                        self.CreateIntialResultData()
                    }
                    }
                }
                    
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
}


