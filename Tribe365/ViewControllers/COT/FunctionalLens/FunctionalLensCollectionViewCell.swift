//
//  FunctionalLensCollectionViewCell.swift
//  Tribe365
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * Collection view cell which contains text
 */
class FunctionalLensCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTxt: UILabel!
    
    
}
