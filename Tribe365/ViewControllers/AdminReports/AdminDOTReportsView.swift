//
//  AdminDOTReportsView.swift
//  Tribe365
//
//  Created by Apple on 14/02/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class AdminDOTReportsView: UIViewController, UITableViewDelegate , UITableViewDataSource, HADropDownDelegate, UITextFieldDelegate  {

    let panel = JKNotificationPanel()
    var arrBeliefs = [BeliefModel]()
    var arrValuesDemo = [ValueModel]()
    var calacualtedMaxHeightOfCollection = Int()
    var strOrgID = ""
    var strOrgName = ""
    var selectedIndexOfDropdownList = Int()
    var strBeliefId = ""
    var strOfficeId = ""

    var arrOfBelief = [String]()
    var arrOfBeliefId = [String]()
    var loadForFirst = false
    var arrOfficesName = [String]()
    var arrOffices = [OfficeModel]()
    var selectedIndexOfOffice = 0
    var selectedFromDate = ""
    var selectedToDate = ""

    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var dropDown: HADropDown!
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!

    @IBOutlet weak var btnViewFeedback: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callWebServiceDOTReportDetail()
        callWebServiceToGetAllOfficesAndDepartmentList()
        
        tblView.delegate = self
        tblView.dataSource = self
        tblView.separatorStyle = .none
        lblHeader.text = "REPORT - Directing"
        dropDownOffice.delegate = self
//        if AuthModel.sharedInstance.role == kSuperAdmin{
//            
//            btnViewFeedback.isHidden = true
//        }
        // Do any additional setup after loading the view.
        
        dropDown.delegate = self
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName

    }

    @IBAction func btnViewFeedbackAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "DOTReportsView") as! DOTReportsView
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                callWebServiceDOTReportDetail()
            }
        }
        else {
            
            if selectedFromDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        callWebServiceDOTReportDetail()
                    }
                }
            }
            
        }
    }
    
    //MARK: UITableViewDelegate and dataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            
            let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
            
            cell.arrBeliefs = arrBeliefs
            cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
            cell.collectionView.reloadData()
            cell.strComingFromReport = "True"
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

         return  100 + CGFloat( 90 * calacualtedMaxHeightOfCollection)
        
    }
   
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        if dropDown == dropDownOffice {
            selectedIndexOfOffice = 0
            selectedIndexOfOffice = index
            
            dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
            strOfficeId = arrOffices[selectedIndexOfOffice].strOffice_id

            callWebServiceDOTReportDetail()

        }
        else {
            selectedIndexOfDropdownList = index
            strBeliefId = arrOfBeliefId[selectedIndexOfDropdownList]
            callWebServiceDOTReportDetail()
        }
    }
    
    func CreateDropDownListArray(){
        
        for i in (0..<self.arrBeliefs.count + 1) {
            if i == 0{
                self.arrOfBelief.insert("All BELIEFS", at: 0)
                self.arrOfBeliefId.insert("", at: 0)
                
            }
            else{
                self.arrOfBelief.insert(self.arrBeliefs[i - 1].strName.uppercased(), at: i)
                self.arrOfBeliefId.insert(self.arrBeliefs[i - 1].strId, at: i)
                
            }
        }
        self.dropDown.items = self.arrOfBelief
    }
    //MARK: - WebService Methods
    func callWebServiceDOTReportDetail() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId":strOrgID,
                     "beliefId" : strBeliefId,
                     "officeId": strOfficeId,
                     "startDate": selectedFromDate,
                     "endDate": selectedToDate] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getDOTreportGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    AdminReportsParser.parseDOTDetail(response: response!, completionHandler: { (arrOfDOTReports) in
                        
                        self.arrBeliefs = arrOfDOTReports
                       
                    })
                   /* if self.arrBeliefs.count == 0{
                        self.lblNoData.text = "Belief and Values are not avaliable."
                        self.lblNoData.isHidden = false
                    }*/
                    
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        self.CreateDropDownListArray()
                    }
                    self.CalculateMaxHeight()
                    self.tblView.reloadData()
                    
                }
                else {
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllOfficesAndDepartmentList() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        let model = OfficeModel()
                        model.strOffice_id = ""
                        model.strOffice = "ALL OFFICES"
                        self.arrOffices.insert(model, at: 0)

                        self.arrOfficesName = arrOfcNames
                        self.dropDownOffice.items = ["ALL OFFICES"] + self.arrOfficesName
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    
    //MARK: - Custom Functions
    func CalculateMaxHeight(){
        for i in (0..<self.arrBeliefs.count) {
            
            if i == 0 {
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
            }
            if calacualtedMaxHeightOfCollection < self.arrBeliefs[i].arrValues.count {
                
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                
                print(calacualtedMaxHeightOfCollection)
            }
            
        }
    }
    
}
