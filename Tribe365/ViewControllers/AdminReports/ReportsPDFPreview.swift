//
//  ReportsPDFPreview.swift
//  Tribe365
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
class ReportsPDFPreview: UIViewController, UIWebViewDelegate{

    @IBOutlet weak var webView: UIWebView!

    var strPDFLink = ""
    let panel = JKNotificationPanel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.delegate = self
        if strPDFLink == ""{
            self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "Link not available.")
            webView.isHidden = true
        }
        else{
        let url: URL! = URL(string: strPDFLink)
            webView.loadRequest(URLRequest(url: url!))
            webView.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func btnShowPDFAction(_ sender: Any) {
        
        showSavedPdf(url: strPDFLink, fileName: AuthModel.sharedInstance.office + "Reports")
    }
    
    @IBAction func btnDownloadPdfAction(_ sender: Any) {
        
        savePdf(urlString: strPDFLink , fileName: AuthModel.sharedInstance.office + "Reports")
    }
    
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "Tribe365-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    
    func showSavedPdf(url:String, fileName:String) {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                print(docURL)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("\(fileName).pdf") {
                        // its your file! do what you want with it!
                        
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
    
    // check to avoid saving a file multiple times
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("YourAppName-\(fileName).pdf") {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
    }
}
