//
//  functionalLensReportView.swift
//  Tribe365
//
//  Created by Apple on 20/02/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class functionalLensReportView: UIViewController, HADropDownDelegate, UITextFieldDelegate{

    let panel = JKNotificationPanel()
    var st = ""
    var sf = ""
    var nt = ""
    var nf = ""
    var strOrgID = ""
    
    //For all Offices and department list Api
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    
    //Arr For All Department
    var arrForDepartmentName = [DepartmentModel]()
    var arrOfAllDeparmentList = [String]()
    var arrOfAllDepartmentIdList = [String]()

    
    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0

    
    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()
    
    var loadForFirst = false
    var selectedFromDate = ""
    var selectedToDate = ""
    
    @IBOutlet weak var lblST: UILabel!
    @IBOutlet weak var lblSF: UILabel!
    @IBOutlet weak var lblNT: UILabel!
    @IBOutlet weak var lblNF: UILabel!

    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var dropDownDept: HADropDown!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        print(strOrgID)
        dropDownDept.delegate = self
        dropDownOffice.delegate = self
        
    callWebServiceToGetFunctionalLensData()
    callWebServiceToGetAllOfficesAndDepartmentList()
    callWebServiceToGetAllDepartments()
        // Do any additional setup after loading the view.
    }

    /*Sky Blue : #45dade
     Yellow : #ffa73f
     Red : #ff4e56
     Off Red : #ffadaf*/
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func callWebServiceToGetFunctionalLensData() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            
            param = [ "orgId": strOrgID,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
            
        }
        else{
        param = [ "orgId": strOrgID,
                  "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                  "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                  "startDate": selectedFromDate,
                  "endDate": selectedToDate]
        }
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getReportFunctionalLensGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AdminReportsParser.parseFunctionalLenseReport(response: response!, completionHandler: { (st,sf,nt,nf) in
                        
                        self.st = st
                        self.sf = sf
                        self.nt = nt
                        self.nf = nf
                        
                    })
                    self.lblST.text = self.st + "%"
                    self.lblSF.text = self.sf + "%"
                    self.lblNT.text = self.nt + "%"
                    self.lblNF.text = self.nf + "%"
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func setData() {
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
        dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                callWebServiceToGetFunctionalLensData()
            }
        }
        else {
            
            if selectedFromDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        callWebServiceToGetFunctionalLensData()
                    }
                }
            }
        }
    }
    
    //MARK: - HADropDown Delegate Method
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        //selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
       // dropDownDept.item
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            selectedIndexOfDepartment = 0
            selectedIndexOfOffice = 0
            dropDownDept.title = "ALL DEPARTMENTS"
            selectedIndexOfOffice = index
            if index != 0 {
                arrDepartment = arrOffices[index - 1].arrDepartment
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                
                if dropDownOffice.title == "ALL OFFICES"
                {
                    dropDownDept.items = arrOfAllDeparmentList
                }
               // arrDepartment = arrAllDepartment
                //arrDepartmentNames = arrAllDepartmentNames
            }
            setData()
        }
        else if dropDown == dropDownDept {
            selectedIndexOfDepartment = index
        }
        callWebServiceToGetFunctionalLensData()
    }
    
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        self.arrDepartment = arrDept
                        self.arrAllDepartment = arrDept
                        self.arrOfficesName = arrOfcNames
                        self.arrDepartmentNames = arrDeptNames
                        self.arrAllDepartmentNames = arrDeptNames
                        self.setData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllDepartments() {
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        self.arrForDepartmentName = arrDept
                    })
                   
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func CreateDropDownListOfAllDepartemntArray(){
        
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
       // self.dropDownDept.items = self.arrDepartmentNames
    }
}
