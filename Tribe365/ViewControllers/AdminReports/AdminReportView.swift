//
//  AdminReportView.swift
//  Tribe365
//
//  Created by Apple on 14/02/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class AdminReportView: UIViewController {

     //MARK: - Variables
    var strOrgId = ""
    var strOrgName = ""
    let panel = JKNotificationPanel()
    var arrOfSOTDetails = [SOTDetailModel]()
    var arrOfSOTResultSummary = [SOTDetailResultModel]()

     //MARK: - IbOutlets
    @IBOutlet weak var lblHeader: UILabel!
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnReportSupportAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.report
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectedAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "AdminDOTReportsView") as! AdminDOTReportsView
            objVC.strOrgID = strOrgId
            objVC.strOrgName =  strOrgName
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
        else if sender.tag == 2 {
            let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "TeamAverageResultView") as! TeamAverageResultView
            objVC.strOrgID = strOrgId
            objVC.comingFromReports =  "True"
            self.navigationController?.pushViewController(objVC, animated: true)
        }
            
        else if sender.tag == 3 {
            
            let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "functionalLensReportView") as! functionalLensReportView
            objVC.strOrgID = strOrgId
            self.navigationController?.pushViewController(objVC, animated: true)
        }
            
        else if sender.tag == 4 {
            
            callWebServiceToGetSOTDetail()
        }

        else if sender.tag == 5 {
            
            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationStructureGraphView") as!  MotivationStructureGraphView
            
            objVC.strOrgID = strOrgId
            objVC.strUserName = strOrgName
            objVC.comingFromReports =  "True"

            self.navigationController?.pushViewController(objVC, animated: true)
        }

        else if sender.tag == 6 {
           
            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticResultView") as! DiagnosticResultView
            objVC.strOrgId = strOrgId
            objVC.strCompanyName = strOrgName
            objVC.comingFromReports =  "True"

            self.navigationController?.pushViewController(objVC, animated: true)
        }

        else if sender.tag == 7 {
            
            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerResultView") as! TribeometerResultView
            objVC.strOrgId = strOrgId
            objVC.strCompanyName = strOrgName
            objVC.comingFromReports =  "True"

            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
        else if sender.tag == 8 {
            
            let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "DOTReportsView") as! DOTReportsView
            objVC.selectedOrgID = strOrgId
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnGenratePDFAction(_ sender: Any) {
        callWebServiceTOGetPDFLink()
    }
    
    //MARK: - Call Web Service
    func callWebServiceTOGetPDFLink(){
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        param = [ "orgId": strOrgId]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getReportPdfUrl, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    
                    let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "ReportsPDFPreview") as! ReportsPDFPreview
                    print(response!["data"].stringValue)
                    objVC.strPDFLink = response!["data"].stringValue
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    func callWebServiceToGetSOTDetail() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        param = [ "orgId": strOrgId]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.SOT.getSOTdetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTDetails(response: response!, completionHandler: { (arrOfSOTDetail , arrOfSOTResultSummmary, IsQuestionnaireAnswerFilled, IsUserFilledAnswer) in
                        
                        self.arrOfSOTDetails = arrOfSOTDetail
                        self.arrOfSOTResultSummary = arrOfSOTResultSummmary
                    })
                    
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTOrganisationResultView") as! SOTOrganisationResultView
                    objVC.strOrgId = self.strOrgId
                    objVC.arrOfSOTDetails = self.arrOfSOTDetails
                    objVC.arrOfSOTResultSummary = self.arrOfSOTResultSummary
                    objVC.comingFromReport = "True"
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

}
