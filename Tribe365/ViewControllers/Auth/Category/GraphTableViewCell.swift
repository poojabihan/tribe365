//
//  GraphTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 19/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import Charts

/**
 * This class is used to show the bar graphs
 */
class GraphTableViewCell: UITableViewCell {

    //used for bar graph for motivation cell
    @IBOutlet weak var basicBarChartMotivation: BasicBarChart!
    
    //used for bar graph for diagnostics cell
    @IBOutlet weak var basicBarChartForDiagnostics: BasicBarChart2!
    
    //used for bar graph for happy index cell
    @IBOutlet weak var BasicBarChartHappyIndex: BasicBarChartHappyIndex!
    
    //used for bar graph for tribeometer cell
    @IBOutlet weak var basicBarChartTribeometer: BasicBarChartForTribeometer!
    
    //back button(used to switch to previous graph)
    @IBOutlet weak var btnBack: UIButton!
    
    //instance of line chart
    @IBOutlet var lineChart: LineChartView!
    
    //used for bar graph for personality type cell
    @IBOutlet weak var basicBarChartForPersonalityType: BasicBarChartPeronalityType!
    
    //used to go back to domination view for personality type
    @IBOutlet weak var btnBackToDominatingView: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
