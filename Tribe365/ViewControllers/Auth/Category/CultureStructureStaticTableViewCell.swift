//
//  CultureStructureStaticTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 18/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell is used to show the culture structure result
 */
class CultureStructureStaticTableViewCell: UITableViewCell {

    //shows result text
    @IBOutlet weak var lblResultText: UILabel!
    
    //shows summary
    @IBOutlet weak var lblSOOCS: UILabel!
    
    //shows 1st title
    @IBOutlet weak var lblPersonTitle: UILabel!
    
    //shows 1st value
    @IBOutlet weak var lblPersonScore: UILabel!
    
    //shows 2nd title
    @IBOutlet weak var lblPowerTitle: UILabel!
    
    //shows 2nd value
    @IBOutlet weak var lblPowerScore: UILabel!
    
    //shows 3rd title
    @IBOutlet weak var lblRoleTitle: UILabel!
    
    //shows 3rd value
    @IBOutlet weak var lblRoleScore: UILabel!
    
    //shows 4th title
    @IBOutlet weak var lblTribeTitle: UILabel!
    
    //shows 4th value
    @IBOutlet weak var lblTribeScore: UILabel!
    
    //result image
    @IBOutlet weak var imgResult: UIImageView!
    
    //view containg 4 values
    @IBOutlet weak var viewOf4View: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
