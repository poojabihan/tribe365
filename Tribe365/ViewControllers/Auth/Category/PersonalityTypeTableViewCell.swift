//
//  PersonalityTypeTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * No longer in use
 */
class PersonalityTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblST: UILabel!
    @IBOutlet weak var lblSF: UILabel!
    @IBOutlet weak var lblNT: UILabel!
    @IBOutlet weak var lblNF: UILabel!

    @IBOutlet weak var lblSTValue: UILabel!
    @IBOutlet weak var lblSFValue: UILabel!
    @IBOutlet weak var lblNTValue: UILabel!
    @IBOutlet weak var lblNFValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(response: JSON) {
        
        self.lblST.text = response["stValue"].stringValue
        self.lblSF.text = response["sfValue"].stringValue
        self.lblNT.text = response["ntValue"].stringValue
        self.lblNF.text = response["nfValue"].stringValue
        
        self.lblSTValue.text = response["st"].stringValue + "%"
        self.lblSFValue.text = response["sf"].stringValue + "%"
        self.lblNTValue.text = response["nt"].stringValue + "%"
        self.lblNFValue.text = response["nf"].stringValue + "%"

    }

}
