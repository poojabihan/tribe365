//
//  CategoryDefinedTableCellForActions.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 31/05/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

/**
This class is no longer in use
*/
class CategoryDefinedTableCellForActions: UITableViewCell {
    
    //MARK: - Cell IBOutlets
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var btnMessage: UIButton!    
    @IBOutlet weak var btnDiagnostic: UIButton!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
