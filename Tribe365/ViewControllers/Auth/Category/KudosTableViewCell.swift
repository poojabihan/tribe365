//
//  KudosTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 24/01/20.
//  Copyright © 2020 chetaru. All rights reserved.
//

import UIKit

/**
 * Kudos table view cell
 */
class KudosTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //for user name
    @IBOutlet weak var lblUsername: UILabel!
    
    //for email
    @IBOutlet weak var lblEmail: UILabel!
    
    //for kudos count
    @IBOutlet weak var lblKudos: UILabel!
    
    //for image
    @IBOutlet weak var imgUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
