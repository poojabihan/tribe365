//
//  TeamRoleReportTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class is used to show the team role view in know organisation & know members screens
 */
class TeamRoleReportTableViewCell: UITableViewCell {

    //shows the value for shaper
    @IBOutlet weak var lblShaper: UILabel!
    
    //shows the value for coordinator
    @IBOutlet weak var lblCoordinator: UILabel!
    
    //shows the value for implementer
    @IBOutlet weak var lblImplementer: UILabel!
    
    //shows the value for completer
    @IBOutlet weak var lblCompleter: UILabel!
    
    //shows the value for monitor evaluator
    @IBOutlet weak var lblMonitorEvaluator: UILabel!
    
    //shows the value for team worker
    @IBOutlet weak var lblTeamWorker: UILabel!
    
    //shows the value for plant
    @IBOutlet weak var lblPlant: UILabel!
    
    //shows the value for resource investigator
    @IBOutlet weak var lblResourceInvestigator: UILabel!
    
    //all below for showing static labels
    @IBOutlet weak var lblShaperScore: UILabel!
    @IBOutlet weak var lblCoordinatorScore: UILabel!
    @IBOutlet weak var lblImplementerScore: UILabel!
    @IBOutlet weak var lblCompleterScore: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorScore: UILabel!
    @IBOutlet weak var lblTeamWorkerScore: UILabel!
    @IBOutlet weak var lblPlantScore: UILabel!
    @IBOutlet weak var lblResourceInvestigatorScore: UILabel!

    @IBOutlet weak var lblShaperValue: UILabel!
    @IBOutlet weak var lblCoordinatorValue: UILabel!
    @IBOutlet weak var lblImplementerValue: UILabel!
    @IBOutlet weak var lblCompleterValue: UILabel!
    @IBOutlet weak var lblMonitorEvaluatorValue: UILabel!
    @IBOutlet weak var lblTeamWorkerValue: UILabel!
    @IBOutlet weak var lblPlantValue: UILabel!
    @IBOutlet weak var lblResourceInvestigatorValue: UILabel!

    @IBOutlet weak var btnRedoReview: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Custom Function
    /**
     * This function sets value for every label
     */
    func setData(response: JSON) {
        self.lblPlant.text = response["plant"].stringValue + "%"
        self.lblMonitorEvaluator.text = response["monitorEvaluator"].stringValue + "%"
        self.lblTeamWorker.text = response["teamworker"].stringValue + "%"
        self.lblCompleter.text = response["completerFinisher"].stringValue + "%"
        self.lblShaper.text = response["shaper"].stringValue + "%"
        self.lblCoordinator.text = response["coordinator"].stringValue + "%"
        self.lblImplementer.text = response["implementer"].stringValue + "%"
        self.lblResourceInvestigator.text = response["resourceInvestigator"].stringValue + "%"
        
        self.lblPlantValue.text = response["mapersArray"]["plant"].stringValue
        self.lblMonitorEvaluatorValue.text = response["mapersArray"]["monitorEvaluator"].stringValue
        self.lblTeamWorkerValue.text = response["mapersArray"]["teamworker"].stringValue
        self.lblCompleterValue.text = response["mapersArray"]["completerFinisher"].stringValue
        self.lblShaperValue.text = response["mapersArray"]["shaper"].stringValue
        self.lblCoordinatorValue.text = response["mapersArray"]["coordinator"].stringValue
        self.lblImplementerValue.text = response["mapersArray"]["implementer"].stringValue
        self.lblResourceInvestigatorValue.text = response["mapersArray"]["resourceInvestigator"].stringValue
    }
    
    /**
     * This function sets value for every label in individual team role screen
     */
    func setDataForIndividual(response: JSON) {
        self.lblShaperValue.text = response["shaper"].stringValue
        self.updateBgColor(label: self.lblShaperValue)
        
        self.lblCoordinatorValue.text = response["coordinator"].stringValue
        self.updateBgColor(label: self.lblCoordinatorValue)
        
        self.lblImplementerValue.text = response["implementer"].stringValue
        self.updateBgColor(label: self.lblImplementerValue)
        
        self.lblCompleterValue.text = response["completerFinisher"].stringValue
        self.updateBgColor(label: self.lblCompleterValue)
        
        self.lblMonitorEvaluatorValue.text = response["monitorEvaluator"].stringValue
        self.updateBgColor(label: self.lblMonitorEvaluatorValue)
        
        self.lblTeamWorkerValue.text = response["teamworker"].stringValue
        self.updateBgColor(label: self.lblTeamWorkerValue)
        
        self.lblPlantValue.text = response["plant"].stringValue
        self.updateBgColor(label: self.lblPlantValue)
        
        self.lblResourceInvestigatorValue.text = response["resourceInvestigator"].stringValue
        self.updateBgColor(label: self.lblResourceInvestigatorValue)
        
        self.lblShaperScore.text = "Score: " + response["totalKeyCount"]["shaper"].stringValue
        self.lblCoordinatorScore.text = "Score: " + response["totalKeyCount"]["coordinator"].stringValue
        self.lblImplementerScore.text = "Score: " + response["totalKeyCount"]["implementer"].stringValue
        self.lblCompleterScore.text = "Score: " + response["totalKeyCount"]["completerFinisher"].stringValue
        self.lblMonitorEvaluatorScore.text = "Score: " + response["totalKeyCount"]["monitorEvaluator"].stringValue
        self.lblTeamWorkerScore.text = "Score: " + response["totalKeyCount"]["teamworker"].stringValue
        self.lblPlantScore.text = "Score: " + response["totalKeyCount"]["plant"].stringValue
        self.lblResourceInvestigatorScore.text = "Score: " + response["totalKeyCount"]["resourceInvestigator"].stringValue
        
        self.lblShaper.text = response["mapersArray"]["shaper"].stringValue
        self.lblCoordinator.text = response["mapersArray"]["coordinator"].stringValue
        self.lblImplementer.text = response["mapersArray"]["implementer"].stringValue
        self.lblCompleter.text = response["mapersArray"]["completerFinisher"].stringValue
        self.lblMonitorEvaluator.text = response["mapersArray"]["monitorEvaluator"].stringValue
        self.lblTeamWorker.text = response["mapersArray"]["teamworker"].stringValue
        self.lblPlant.text = response["mapersArray"]["plant"].stringValue
        self.lblResourceInvestigator.text = response["mapersArray"]["resourceInvestigator"].stringValue
    }
    
    /**
     * This function provide color to label according to there rank
     */
    func updateBgColor(label: UILabel) {
        switch label.text {
        case "1":
            label.backgroundColor = UIColor(hexString: greenColor)
            label.textColor = UIColor.white
        case "2":
            label.backgroundColor = UIColor(hexString: yellowColor)
            label.textColor = UIColor.black
        case "3":
            label.backgroundColor = UIColor(hexString: blueColor)
            label.textColor = UIColor.white
        case "4", "5", "6", "7", "8":
            label.backgroundColor = UIColor(hexString: orangeColor)
            label.textColor = UIColor.white
        default:
            label.backgroundColor = UIColor.lightGray
            label.textColor = UIColor.black
            print("defaultcase")
        }
    }

}
