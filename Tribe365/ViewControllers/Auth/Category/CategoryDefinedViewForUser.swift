//
//  CategoryDefinedViewForUser.swift
//  Tribe365
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import UserNotifications
import SwiftyJSON
import Alamofire
import SCLAlertView
import UserNotificationsUI

/**
 This class is the home screen of app when logged in user is end user
 */
class CategoryDefinedViewForUser: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextViewDelegate, UISearchBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: - IBoutlets
    //main table which contains every view
    @IBOutlet var tblView: UITableView!
    
    //value table
    @IBOutlet var tblValues: UITableView!
    
    //Department & Users table
    @IBOutlet var tblOptions: UITableView!
    
    //IOT Feedback view
    @IBOutlet weak var iotFeedbackView: UIView!
    
    //Header view
    @IBOutlet weak var tblHeaderView: UIView!
    
    //Height constraint for feedback view
    @IBOutlet weak var feedbackHeightConstraint: NSLayoutConstraint!
    
    //Height constraint for values view
    @IBOutlet weak var valuesHeightConstraint: NSLayoutConstraint!
    
    //feedback text view
    @IBOutlet weak var txtIOTFeedback: UITextView!
    
    //notification(bell icon) button
    @IBOutlet weak var btnNotification: UIButton!
    
    //Height constraint for Aim
    @IBOutlet weak var heightAim: NSLayoutConstraint!
    
    //happy index image (smiley image)
    @IBOutlet weak var imgHappyIndex: UIImageView!
    
    //happy index view(bottom right)
    @IBOutlet weak var happyIndexView: UIView!
    
    //happy index DD value label
    @IBOutlet weak var lblHappyIndexDD: UILabel!
    
    //happy index MA value label
    @IBOutlet weak var lblHappyIndexMA: UILabel!
    
    //organisation index DD value label
    @IBOutlet weak var lblOrgIndexDD: UILabel!
    
    //organisation index MA value label
    @IBOutlet weak var lblOrgIndexMA: UILabel!
    
    //department index DD value label
    @IBOutlet weak var lblDeptIndexDD: UILabel!
    
    //department index MA value label
    @IBOutlet weak var lblDeptIndexMA: UILabel!
    
    //happy index DD view
    @IBOutlet weak var happyIndexDDView: UIView!
    
    //happy index MA view
    @IBOutlet weak var happyIndexMAView: UIView!
    
    //happy index DD image view(top/bottom arrow)
    @IBOutlet weak var imgHappyIndexDD: UIImageView!
    
    //happy index MA image view(top/bottom arrow)
    @IBOutlet weak var imgHappyIndexMA: UIImageView!
    
    //organisation index DD view
    @IBOutlet weak var OrgIndexDDView: UIView!
    
    //organisation index MA view
    @IBOutlet weak var OrgIndexMAView: UIView!
    
    //organisation index DD image view(top/bottom arrow)
    @IBOutlet weak var imgOrgIndexDD: UIImageView!
    
    //organisation index MA image view(top/bottom arrow)
    @IBOutlet weak var imgOrgIndexMA: UIImageView!
    
    //department index DD view
    @IBOutlet weak var deptIndexDDView: UIView!
    
    //department index MA view
    @IBOutlet weak var deptIndexMAView: UIView!
    
    //department index DD image view(top/bottom arrow)
    @IBOutlet weak var imgdeptIndexDD: UIImageView!
    
    //department index MA image view(top/bottom arrow)
    @IBOutlet weak var imgdeptIndexMA: UIImageView!
    
    //Add Dept & Add User view IBOutlet
    // To show selected depts
    @IBOutlet weak var lblAddTeam: UILabel!
    
    // To show selected Users
    @IBOutlet weak var lblAddIndividual: UILabel!
    @IBOutlet weak var btnDoneThumbsView: UIButton!
    
    // View with buttons add team/individual
    @IBOutlet var thumsUpSelectionView: UIView!
    
    //Add Dept & Add User selection view IBOutlet
    //Blur view which open on AddTeam/AddIndividual click
    @IBOutlet weak var addOptionView: UIView!
    
    //continue button (red tick) when finidhed selecting individual/team
    @IBOutlet weak var btnContinue: UIButton!
    
    //heading for options (individual/team) view
    @IBOutlet weak var lblHeadingOptionView: UILabel!
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    //menu button(side bar)
    @IBOutlet weak var btnMenu: UIButton!
    
    //organisation name label
    @IBOutlet weak var txtCompanyName: UILabel!
    
    //organisation logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //left view of navigation bar
    @IBOutlet weak var viewOfNB: UIView!
    
    //profile button
    @IBOutlet weak var btnProfile: UIButton!
    
    //no longer in use
    @IBOutlet weak var btnChecklistCount: UIButton!
    @IBOutlet weak var iotView: UIView!
    @IBOutlet weak var reportsView: UIView!
    @IBOutlet weak var company360View: UIView!
    @IBOutlet weak var btnActionCount: UIButton!
    @IBOutlet weak var sotViewUser: UIView!
    @IBOutlet weak var sotViewAdmin: UIView!
    @IBOutlet weak var companyViewUser: UIView!
    @IBOutlet weak var companyViewAdmin: UIView!
    @IBOutlet weak var lblDirectedPercent: UILabel!
    @IBOutlet weak var lblConnectedPercent: UILabel!
    
    //feedback image
    @IBOutlet weak var imgViewIOT: UIImageView!
    
    //notification count label
    @IBOutlet weak var lblUnredCount: UILabel!
    
    //aim static text label
    @IBOutlet weak var lblAim: UILabel!
    
    //aim value text label
    @IBOutlet weak var lblAimValue: UILabel!
    
    //Be text label
    @IBOutlet weak var lblBe: UILabel!
    
    //know text label
    @IBOutlet weak var lblKnow: UILabel!
    
    //Happy index percent value label
    @IBOutlet weak var lblHappyIndexValue: UILabel!
    
    //organisation index percent label
    @IBOutlet weak var lblOrgIndex: UILabel!
    
    //department index percent label
    @IBOutlet weak var lblDeptIndex: UILabel!
    
    // MARK: - Variables
    //image picker
    var imagePicker = UIImagePickerController()
    
    //show error message
    let panel = JKNotificationPanel()
    
    //stores organisation id
    var strOrgID = ""
    
    //stores office models
    var arrOfficeDetail = [OfficeModel]()
    
    //stores organisation name
    var strCompanyName = ""
    
    //stores organisation logo
    var urlForOrgLogo = ""
    
    //tells if DOT is added or not
    var isDOTAdded = 0
    
    //no longer in use
    var arrayForCategoryTitle = ["Directing", "Connecting", "Supercharging", "Improving", "REPORTS"]
    
    //no longer in use
    var arrayForCategorySubTitle = ["Directing Our Tribe","Connecting Our Tribe","Supercharging Our Tribe", "Improving Our Tribe", ""]
    
    //shred instance object
    var  auth = AuthModel.sharedInstance
    
    //no longer in use
    var arrChecklist = [ChecklistModel]()
    var arrTodos = [ChecklistModel]()
    var arrSectionTitles = [String]()
    
    //array of models
    var arrValues = [ValueModel]()
    var arrOfGetDeparmentList = [DepartmentModel]()
    var arrOfGetUserList = [UserModel]()
    
    var arrFilteredDeparmentList = [DepartmentModel]()
    var arrFilteredUserList = [UserModel]()
    
    var arrSelectedDept = [DepartmentModel]()
    var arrSelectedUsers = [UserModel]()
    
    //Custom alert to show happy index popup
    var customAlert = SCLAlertView()
    
    // To detect whether AddTeam clicked or AddIndividual clicked
    var isDept = true
    var isHappyIndex = false
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set delegates
        searchBar.delegate = self
        imagePicker.delegate = self
        
        tblView.reloadData()
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            //case - super admin
            
            //do not show notification & unread count
            btnNotification.isHidden = true
            lblUnredCount.isHidden = true
        }
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //no longer in use
        NotificationCenter.default.addObserver(self, selector: #selector(CategoryDefinedView.MoveToIOT(_:)), name: NSNotification.Name(rawValue: "GoToViewOfPush"), object: nil)
        let userUpdateNotification = Notification.Name("GoToViewOfPushChat")
        NotificationCenter.default.addObserver(self, selector: #selector(moveToChat(not:)), name: userUpdateNotification, object: nil)
        
        //This observer is called when user submits happy index vaue
        let resetHappy = Notification.Name("resetHAppyindex")
        NotificationCenter.default.addObserver(self, selector: #selector(resetHappyIndex), name: resetHappy, object: nil)
        
        //        imgOrgLogo.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
        //        imgOrgLogo.layer.borderWidth = 1
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        if auth.role == kUser{
            //case when logged in is end user
            
            //set office name & org logo
            txtCompanyName.text = strCompanyName
            let url = URL(string: auth.organisation_logo)
            imgOrgLogo.kf.setImage(with: url)
            txtCompanyName.text = auth.office
            
            //show menu icon
            btnMenu.isHidden = false
            
            //show the navigation view
            viewOfNB.frame.size.width = 135
            //            imgViewOftribeNB.frame.origin.x = viewOfNB.frame.origin.x - 16
            
            //show profile button
            btnProfile.isHidden = false
        }
        else{
            //case when logged in is super admin
            
            //no longer in use
            arrayForCategoryTitle = ["Directing", "Connecting", "Supercharging" /*, "REPORTS"*/]
            arrayForCategorySubTitle = ["Directing Our Tribe","Connecting Our Tribe","Supercharging Our Tribe"/*, ""*/]
            
            //set org name & logo
            txtCompanyName.text = strCompanyName
            let url = URL(string: urlForOrgLogo)
            imgOrgLogo.kf.setImage(with: url)
            
            //hide side menu
            btnMenu.isHidden = true
        }
        
        //set feedback text view placeholder text and its appearance
        txtIOTFeedback.text = "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable"
        txtIOTFeedback.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        txtIOTFeedback.textColor = UIColor.lightGray
        txtIOTFeedback.layer.borderWidth = 1
        txtIOTFeedback.font = UIFont(name: "verdana", size: 13.0)
        txtIOTFeedback.returnKeyType = .done
        txtIOTFeedback.delegate = self
        
        //        self.valuesHeightConstraint.constant = 0
        //        self.feedbackHeightConstraint.constant = 0
        //        self.tblHeaderView.frame.size.height = 696
        
        //        registerNotification()
        //        scheduleNotification()
        
        //no longer in use
        NotificationCenter.default.addObserver(self, selector: #selector(CategoryDefinedViewForUser.showHappyIndexPopUp), name: NSNotification.Name(rawValue: "showPopUp"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        //This observer is called when app receives any push notificaton and we need to update the count
        NotificationCenter.default.addObserver(self, selector: #selector(callWebServiceToGetUnreadCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        if AuthModel.sharedInstance.role == kUser{
            //for user
        }
        else{
            //for admin hide every view
            iotView.isHidden = true
            reportsView.isHidden = true
            sotViewUser.isHidden = true
            sotViewAdmin.isHidden = false
            companyViewUser.isHidden = true
            companyViewAdmin.isHidden = false
        }
        
        //get all unread notification count
        callWebServiceToGetUnreadCount()
        //        callWebServiceForUnReadNotifications()
        
        //        callWebServiceForChecklist()
        //        callWebServiceForActions()
        
        //        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        //
        //        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
        //        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchBar.layer.cornerRadius = 18
        //        searchTextField.borderStyle = .none
        //        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
        //        searchTextField.textAlignment = NSTextAlignment.left
        //        let image:UIImage = UIImage(named: "search")!
        //        let imageView:UIImageView = UIImageView.init(image: image)
        //        searchTextField.leftView = nil
        //        searchTextField.placeholder = " Search"
        //        searchTextField.leftView = imageView
        //        searchTextField.rightViewMode = UITextFieldViewMode.always
        
        
        //        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 44))
        //        searchBar.searchBarStyle = .default
        //        view.addSubview(searchBar)
        
        // searchBar.placeholder = " Search"
        // searchBar.set(textColor: .brown)
        // searchBar.setTextField(color: UIColor.green.withAlphaComponent(0.3))
        // searchBar.setPlaceholder(textColor: .white)
        // searchBar.setSearchImage(color: .white)
        // searchBar.setClearButton(color: .red)
        
        //tap gestures added to happy index image in order to show happy index popup
        let tap = UITapGestureRecognizer(target: self, action: #selector(showHappyIndexPopUp))
        imgHappyIndex.addGestureRecognizer(tap)
        
        //load details for the page
        callWebServiceForDashboardDetails()
    }
    
    // MARK: - Custom Methods
    /**
     This function is no longer in use
     */
    func registerNotification() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            
            if granted {
                print("Granted")
            }
            else {
                print("Not granted")
            }
        }
    }
    
    /**
     This function is no longer in use
     */
    func scheduleNotification() {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "Feedback"
        content.subtitle = "How\'s things at work today?"
        content.categoryIdentifier = "alarm"
        content.sound = .default()
        
        var datecomponent = DateComponents()
        datecomponent.hour = 14//17
        datecomponent.minute = 20//00
        let trigger = UNCalendarNotificationTrigger(dateMatching: datecomponent, repeats: true)
        //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
        
    }
    
    /**
     This function is used to show custom happy index popup
     */
    @objc func showHappyIndexPopUp() {
        
        //custom textview
        let textViewInAlert = UITextView()
        
        //custom alert to show close button
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false, showCircularIcon: true
        )
        
        customAlert = SCLAlertView(appearance: appearance)
        //set title for the alert
        let question = "How\'s things at work today?"
        
        //set title appearance
        let textRect = (question.replacingOccurrences(of: "\n", with: " ") as NSString).boundingRect(with: CGSize(width: 200, height: 100),
                                                                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                                                                     attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)],
                                                                                                     context: nil)
        
        let size1 = textRect.size
        textViewInAlert.frame = CGRect.init(x: 10, y: 0, width: 200, height: size1.height + 10)
        textViewInAlert.isEditable = false
        textViewInAlert.isSelectable = false
        textViewInAlert.textAlignment = .center
        textViewInAlert.font = UIFont.systemFont(ofSize: 14.0)
        textViewInAlert.text = question
        textViewInAlert.textColor = UIColor.darkGray
        textViewInAlert.isHidden = false
        //add custom alert as alert's subview
        customAlert.customSubview = textViewInAlert
        
        //add smiley button to custom view
        let greenView = UIView(frame: CGRect.init(x: 10, y: 0, width: 200, height: 70))
        let button1 = UIButton.init(type: .system)
        button1.setBackgroundImage(UIImage.init(named: "smiley.png"), for: .normal)
        button1.frame = CGRect.init(x: 20, y: 10, width: 50, height: 50)
        greenView.addSubview(button1)
        
        //add neutral smiley button to custom view
        let button2 = UIButton.init(type: .system)
        button2.setBackgroundImage(UIImage.init(named: "Neutral.png"), for: .normal)
        button2.frame = CGRect.init(x: 80, y: 10, width: 50, height : 50)
        greenView.addSubview(button2)
        
        //add sad smiley button to custom view
        let button3 = UIButton.init(type: .system)
        button3.setBackgroundImage(UIImage.init(named: "sad.png"), for: .normal)
        button3.frame = CGRect.init(x: 140, y: 10, width: 50, height: 50)
        greenView.addSubview(button3)
        
        //add the custom view to custom alert
        customAlert.customSubview = greenView
        
        //set target for all 3 smiley's buttons
        button1.addTarget(self, action: #selector(smileButtonAction), for: .touchUpInside)
        button2.addTarget(self, action: #selector(neutralButtonAction), for: .touchUpInside)
        button3.addTarget(self, action: #selector(sadButtonAction), for: .touchUpInside)
        
        let currentYear = Date()
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy"
        let currentYearString = formatter1.string(from: currentYear)
        print("Current year is ",currentYearString)
        
        //show custom alert
        customAlert.showCustom(question, subTitle: currentYearString, color: ColorCodeConstant.themeRedColor, icon: #imageLiteral(resourceName: "experience"), closeButtonTitle: nil, timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        
    }
    
    /**
     This function is called when smile button of happy index popup is clicked
     */
    @objc func smileButtonAction()
    {
        //submit value for happy index
        callWebServiceToAddHappyIndex(moodStatus: "3")
    }
    
    /**
     This function is called when neutral button of happy index popup is clicked
     */
    @objc func neutralButtonAction()
    {
        //submit value for happy index
        callWebServiceToAddHappyIndex(moodStatus: "2")
    }
    
    /**
     This function is called when sad button of happy index popup is clicked
     */
    @objc func sadButtonAction()
    {
        //submit value for happy index
        callWebServiceToAddHappyIndex(moodStatus: "1")
    }
    
    /**
     This function is no longer in use
     */
    @objc func MoveToIOT(_ notification:Notification) {
        
        if AuthModel.sharedInstance.role == kUser{
            
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
            
            if self.navigationController?.viewControllers.last is IOTFeedBackView {
                objVC.getQuessionarieMethod()
                return
                
            }
            
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    /**
     This function is no longer in use
     */
    @objc func moveToChat(not: Notification) {
        
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let changeit_id = userInfo["changeit_id"] as? String {
                print(changeit_id)
                if AuthModel.sharedInstance.role == kUser{
                    let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
                    objVC.strSelectedHistoryChangeitID = changeit_id
                    
                    if self.navigationController?.viewControllers.last is ChatView {
                        var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
                        navigationArray!.remove(at: (navigationArray?.count)! - 1) // To remove previous UIViewController
                        self.navigationController?.viewControllers = navigationArray!
                        
                        //                        objVC.viewDidLoad()
                        //                        objVC.viewWillAppear(true)
                        //                        return
                        
                    }
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
    }
    
    /**
     When user clicks any department(team) from add team popup(after selecting any value for kudos) this function is called
     */
    @objc func btnDeptAction(sender: UIButton) {
        //set dept/team as selected
        arrFilteredDeparmentList[sender.tag].isSelected = !arrFilteredDeparmentList[sender.tag].isSelected
        tblOptions.reloadData()
        
        //check if any value in list is selected
        if showOptionSelectionTickView() {
            //show continue button if user has made any selection
            btnContinue.isHidden = false
        }
        else {
            //hide continue button if user have not selected any value
            btnContinue.isHidden = true
        }
    }
    
    /**
     When user clicks any individual from add team popup(after selecting any value for kudos) this function is called
     */
    @objc func btnIndiAction(sender: UIButton) {
        //set user as selected
        arrFilteredUserList[sender.tag].isSelected = !arrFilteredUserList[sender.tag].isSelected
        tblOptions.reloadData()
        
        //check if any value in list is selected
        if showOptionSelectionTickView() {
            //show continue button if user has made any selection
            btnContinue.isHidden = false
        }
        else {
            //hide continue button if user have not selected any value
            btnContinue.isHidden = true
        }
    }
    
    /**
     This function is called when user clicks on any value for kudos
     */
    @objc func btnValueAction(sender: UIButton) {
        //set value as selected
        arrValues[sender.tag].isSelected = !arrValues[sender.tag].isSelected
        tblValues.reloadData()
        
        //check if any value in list is selected
        if showThumbsUpSelectionView() {
            //show thumbs up selection view & hide feedback view
            thumsUpSelectionView.isHidden = false
            iotFeedbackView.isHidden = true
        }
        else {
            //show feedback view & hide thumbs up selection view when no value is selected
            thumsUpSelectionView.isHidden = true
            iotFeedbackView.isHidden = false
        }
    }
    
    /**
     This function checks if user has selected any value for kudos and accordingly returns whether to show rating/Thumbs view or not
     */
    func showThumbsUpSelectionView() -> Bool {
        
        for value in arrValues {
            if value.isSelected {
                return true
            }
        }
        
        return false
    }
    
    /**
     This function checks if user has selected any dept(if there is dept listing) or user/individual(if there is user listing) and accordingly returns whether to show continue button or not
     */
    func showOptionSelectionTickView() -> Bool {
        
        if isDept {
            //case of dept selection list
            for value in arrFilteredDeparmentList {
                if value.isSelected {
                    //if any one dept is selected return true
                    return true
                }
            }
            
            //case when no dept is selcted
            return false
        }
        else {
            //case of user/individual selection list
            for value in arrFilteredUserList {
                if value.isSelected {
                    //if any one individual is selected return true
                    return true
                }
            }
            
            //case when no individual is selcted
            return false
        }
    }
    
    /**
     This function sets the selected dept & users in array for web service call and also sets the final selection result on the labels
     */
    func setSelectedOptions() {
        
        if isDept {
            //case of dept selection list
            
            //empty array
            arrSelectedDept.removeAll()
            
            //loop through the dept array
            for value in arrOfGetDeparmentList {
                if value.isSelected {
                    //append the selected departments in new array arrSelectedDept
                    arrSelectedDept.append(value)
                }
            }
            
            if arrSelectedDept.count == 0 {
                //if no departtments selected, reset the values
                lblAddTeam.text = "Add Teams"
                btnDoneThumbsView.isHidden = true
            }
            else if arrSelectedDept.count == 1{
                //if single department selected, set department name and show done button
                lblAddTeam.text = arrSelectedDept[0].strDepartment
                btnDoneThumbsView.isHidden = false
            }
            else {
                //if multiple department selected, set selected depts count and show done button
                lblAddTeam.text = String(arrSelectedDept.count) + " selected"
                btnDoneThumbsView.isHidden = false
            }
        }
        else {
            //case of individual selection list
            
            //empty array
            arrSelectedUsers.removeAll()
            
            //loop through the individual/users array
            for value in arrOfGetUserList {
                if value.isSelected {
                    //append the selected user in new array arrSelectedUsers
                    arrSelectedUsers.append(value)
                }
            }
            if arrSelectedUsers.count == 0 {
                //if no individual/user selected, reset the values
                lblAddIndividual.text = "Add Individuals"
                btnDoneThumbsView.isHidden = true
            }
            else if arrSelectedUsers.count == 1{
                //if single individual/user selected, set individual/user name and show done button
                lblAddIndividual.text = arrSelectedUsers[0].strName
                btnDoneThumbsView.isHidden = false
            }
            else {
                //if multiple individual/user selected, set selected individuals/users count and show done button
                lblAddIndividual.text = String(arrSelectedUsers.count) + " selected"
                btnDoneThumbsView.isHidden = false
            }
        }
    }
    
    /**
     This function resets all the values of kudos section after successfull submission to server
     */
    func resetSelectedValues() {
        for value in arrValues {
            value.isSelected = false
        }
        
        for value in arrFilteredDeparmentList {
            value.isSelected = false
        }
        
        for value in arrFilteredUserList {
            value.isSelected = false
        }
        
        arrSelectedUsers.removeAll()
        arrSelectedDept.removeAll()
        
        lblAddTeam.text = "Add Teams"
        lblAddIndividual.text = "Add Individuals"
        btnDoneThumbsView.isHidden = true
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgViewIOT.image!, 1.0)!
        if let imageData = imgViewIOT.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    // MARK: - IBActions
    /**
     This function is called when view more button for Aim is clicked
     */
    @IBAction func btnViewMoreAction(_ sender: UIButton) {
        //load details for DOT(directing)
        callWebServiceDOTDetail()
    }
    
    /**
     This function is called when menu button is clicked
     */
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        //this call opens/closes the side bar
        sideMenuVC.toggleMenu()
    }
    
    /**
     This function is called when profile button is clicked
     */
    @IBAction func btnProfileAction(_ sender: UIButton) {
        if auth.role == kUser{
            //case when logged in is end user, send it to users profile screen
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            objVC.showMenu = false
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else{
            //case when logged in is super admin, send it to admin's profile screen
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "SuperAdminProfileView") as! SuperAdminProfileView
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    /**
     This function is called when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     This function is called when action button(center right of screen with vertical text) is clicked
     */
    @IBAction func btnActionAction(_ sender: Any) {
        
        //create instance of ViewActionsViewController
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ViewActionsViewController") as! ViewActionsViewController
        //pass organisation id
        objVC.strOrgId = strOrgID
        //pass office details
        objVC.arrOfficeDetail = arrOfficeDetail
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func btnmessageAction(_ sender: Any) {
        //Message Click
        self.panel.showNotify(withStatus: .warning , inView: self.appDelegate.window!, title: "Coming soon")
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func btnDiagnosticAction(_ sender: Any) {
        //Message Click
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticView") as! DiagnosticView
        
        if AuthModel.sharedInstance.role == kUser{//User
            objVC.strOrgId = AuthModel.sharedInstance.orgId
            objVC.strCompanyName = AuthModel.sharedInstance.office
        }
        else{
            objVC.strOrgId = strOrgID
            objVC.strCompanyName = strCompanyName
        }
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func btnSupportAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.tribe
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func btnChecklistAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ChecklistViewController") as!  ChecklistViewController
        objVC.arrChecklist = arrChecklist
        objVC.arrTodos = arrTodos
        objVC.arrSectionTitles = arrSectionTitles
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func CategoryActions(sender: UIButton) {
        
        if(sender.tag == 10){
            let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "COTView") as! COTView
            objVC.strOrgID = self.strOrgID == "" ? auth.orgId : self.strOrgID
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else {
            //Navigate to new view
            if auth.role == kUser{//user
                if(sender.tag == 100){
                    
                    callWebServiceDOTDetail()
                    
                }
                else if(sender.tag == 20){
                    //SOT
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTView") as! SOTView
                    objVC.strOrgID = AuthModel.sharedInstance.orgId
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                else if(sender.tag == 30){
                    
                    callWebServiceToGetUserDetail()
                    
                }
                else if(sender.tag == 40){
                    
                    
                    //Change 28/02
                    
                    let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "AdminReportView") as! AdminReportView
                    
                    objVC.strOrgId = AuthModel.sharedInstance.orgId
                    objVC.strOrgName = AuthModel.sharedInstance.office
                    
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                    
                }
            }
            else{//super admin
                if(sender.tag == 100){
                    
                    if isDOTAdded == 0 {
                        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "AddBeliefsView") as! AddBeliefsView
                        objVC.strOrgID = strOrgID
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else{
                        
                        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                        objVC.strOrgID = strOrgID
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    
                }
                    //SOT Click
                else if(sender.tag == 20){
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTView") as! SOTView
                    objVC.strOrgID = strOrgID
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                //IOT Ckick
                /* else if(indexPath.row == 3){
                 
                 let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "SuperAdminReportsView") as! SuperAdminReportsView
                 objVC.strOrgID = strOrgID
                 self.navigationController?.pushViewController(objVC, animated: true)
                 
                 
                 }*/
                
            }
        }
    }
    
    /**
     This function is called when user clicks on "Add Teams" button (This button appears when user select any value for sending kudos)
     */
    @IBAction func btnAddTeamAction(sender: UIButton) {
        
        //set this bool as true as we need list for depts
        isDept = true
        
        //set list heading to Select Teams
        lblHeadingOptionView.text = "Select Teams"
        
        //load table
        tblOptions.reloadData()
        
        //show the list view
        addOptionView.isHidden = false
    }
    
    /**
     This function is called when user clicks on "Add Individual" button (This button appears when user select any value for sending kudos)
     */
    @IBAction func btnAddIndividualAction(sender: UIButton) {
        
        //set this bool as false as we need list for individuals
        isDept = false
        
        //set list heading to Select Individuals
        lblHeadingOptionView.text = "Select Individuals"
        
        //load table
        tblOptions.reloadData()
        
        //show the list view
        addOptionView.isHidden = false
    }
    
    /**
     This function is called when user clicks on "Red Tick" button (This button appears when user select any one value from option(dept/individual) list)
     */
    @IBAction func btnContinueAction(sender: UIButton) {
        
        //set the selected values on label & prepare array for api
        setSelectedOptions()
        
        //reset serach bar
        searchBar.text = ""
        
        //save selected values to array
        arrFilteredUserList = arrOfGetUserList
        arrFilteredDeparmentList = arrOfGetDeparmentList
        
        //hide option view
        addOptionView.isHidden = true
        
    }
    
    /**
     This function is called when user clicks on "done" button (This button appears when user select any one value from option(dept/individual) list and is ready to send kudos)
     */
    @IBAction func btnDoneAction(sender: UIButton) {
        //add thumbs to selected users or depts
        callWebServiceToAddThumbsUp()
    }
    
    /**
     This function is called when user clicks on send button after entering the feedback
     */
    @IBAction func btnSendFeedbackAction(sender: UIButton) {
        
        //validation - feedback text should not be empty
        if (txtIOTFeedback.text?.isEmpty)! || txtIOTFeedback.text == "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable" {
            //show error in case of empty feedback
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter feedback.")
        }
        else {
            //when feedback is eneterd
            
            //prepare paramater for api
            var param = [String : AnyObject]()
            
            if imgViewIOT.image == nil
            {//no image added[[FIRInstanceID instanceID]token]
                param =  [
                    "message"      : txtIOTFeedback.text!,//feedback text
                    "userId"     : AuthModel.sharedInstance.user_id,//logged in user id
                    "orgId" : AuthModel.sharedInstance.orgId,//logged in user organisation id
                    "image"       : ""//when no image attached with the feedback
                    ] as [String : AnyObject]
                
            }
            else{ //send image
                
                
                param =  [
                    "message"      : txtIOTFeedback.text!,//feedback text
                    "userId"     : AuthModel.sharedInstance.user_id,//logged in user id
                    "orgId" : AuthModel.sharedInstance.orgId,//logged in user organisation id
                    "image"       : convertImageToBase64(image: imgViewIOT.image!)//when image attached with the feedback
                    ] as [String : AnyObject]
            }
            
            //call api to send feedback
            callWebServiceToSendFeedback(param: param)
        }
    }
    
    /**
     This function is called when attachment button is clicked for feedback
     */
    @IBAction func btnIOTCameraAction(_ sender: Any) {
        
        //create alert view for choice of Camera or Gallery
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function is no longer in user
     */
    @IBAction func btnRefreshValuesAction(_ sender: Any) {
        resetSelectedValues()
        callWebServiceDOTValues()
    }
    
    /**
     This function is called when happy index view is clicked
     */
    @IBAction func btnHappyIndexAction(_ sender: Any) {
        
        //        let objVC1 = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
        //        objVC1.isMultiple = false
        //        objVC1.modalPresentationStyle = .overCurrentContext
        //        self.navigationController?.present(objVC1, animated: false, completion: nil)
        //
        //        return
        
        //get todays date
        let now = Date()
        
        //get today's 4PM date
        let four_today = now.dateAt(hours: 16, minutes: 1)
        
        //get today's 11:59PM date
        let eleven_fiftyNine_today = now.dateAt(hours: 23, minutes: 59)
        
        //condition if time is between 4PM & 11:59 PM
        if now >= four_today && now <=  eleven_fiftyNine_today
        {
            if isHappyIndex == false {
                //show happy index popup if not filled already and return from here
                showHappyIndexPopUp()
                return
            }
        }
        
        //if happy index already filled, navigate user to Know organisation screen
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "KnowOrganisationViewController") as! KnowOrganisationViewController
        //passing this as true will redirect user directly to the happy index section
        objVC.isRedirectToHappyIndex = true
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     This function is called when user clicks on Oganisation/department index views
     */
    @IBAction func btnSendToKnowOrgAction(_ sender: Any) {
        //redirect user to know organisation screen
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "KnowOrganisationViewController") as! KnowOrganisationViewController
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     This function sets the happy index view to the mode when happy index is alredy filled
     */
    @objc func resetHappyIndex() {
        
        //set happy index filled
        self.isHappyIndex = true
        
        //remove the blink animation
        self.happyIndexView.layer.removeAllAnimations()
        
        //set happy index view background color to white
        self.happyIndexView.backgroundColor = UIColor.white
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            imgViewIOT.image = image
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK:- UITextViewDelegates
    /***
     Textview delegate method - Called when user starts writing on textview
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        //check if anything is written in textview or not
        if textView.text == "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable" {
            //if placeholder only so make textview blank and change text color
            textView.text = ""
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
    
    /***
     Textview delegate method - Called when user types in any character
     */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            //if any new line detected return the keyboard
            textView.resignFirstResponder()
        }
        return true
    }
    
    /***
     Textview delegate method - Called when user ends writing
     */
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            //if not text so set its placeholder back
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.text = "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblValues {
            //case when we are showing values list
            //return values array count/ 2 (as we are showing 2 values in a single row) + if odd no of values so add 1 extra row for the single value
            return arrValues.count / 2 + arrValues.count % 2
        }
        else {//if tableView == tblOptions
            if isDept {
                //dept listing
                return arrFilteredDeparmentList.count
            }
            else {
                //individual(user) listing
                return arrFilteredUserList.count
            }
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblValues {
            //case when we are showing values list table
            
            //custom cell for values listing
            let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCell") as! ValuesTableViewCell
            //To remove seprator from particular cell
            tableView.separatorStyle = .none
            cell.selectionStyle = .none
            
            //For displaying 1st value
            
            //select the index for first value
            let index1 = indexPath.row * 2
            
            //set value name
            cell.lblValue1.text = arrValues[index1].strName
            
            //set value tag
            cell.btnValue1.tag = index1
            
            //set value target
            cell.btnValue1.addTarget(self, action: #selector(btnValueAction(sender:)), for: .touchUpInside)
            
            //check if value is selected
            if arrValues[index1].isSelected {
                //case - value selected
                
                //set it as selected by changing background & text color
                cell.btnValue1.backgroundColor = ColorCodeConstant.themeRedColor
                cell.lblValue1.textColor = UIColor.white
            }
            else {
                //case - value is not selected
                
                //deselected by changing background & text color
                cell.btnValue1.backgroundColor = UIColor.clear
                cell.lblValue1.textColor = ColorCodeConstant.darkTextcolor
            }
            
            //For displaying 2nd value
            
            //select the index for second value (which will be next to first index)
            let index2 = index1 + 1
            if (index2) < arrValues.count {
                //case - if even number of values
                
                //set value name
                cell.lblValue2.text = arrValues[index2].strName
                
                //set value tag
                cell.btnValue2.tag = index2
                
                //set value target
                cell.btnValue2.addTarget(self, action: #selector(btnValueAction(sender:)), for: .touchUpInside)
                
                //check if value is selected
                if arrValues[index2].isSelected {
                    //case - value selected
                    
                    //set it as selected by changing background & text color
                    cell.btnValue2.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue2.textColor = UIColor.white
                }
                else {
                    //case - value is not selected
                    
                    //deselected by changing background & text color
                    cell.btnValue2.backgroundColor = UIColor.clear
                    cell.lblValue2.textColor = ColorCodeConstant.darkTextcolor
                }
                
                //show 2nd value too
                cell.viewValue2.isHidden = false
                
            }
            else {
                //case - if odd number of values & we are on the last row
                
                //hide 2nd value
                cell.viewValue2.isHidden = true
            }
            
            return cell
        }
        else {//if tableView == tblOptions
            
            if isDept {
                //case when the option table lists the departments list
                
                //custom cell to show depts
                let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCell") as! ValuesTableViewCell
                //To remove seprator from particular cell
                tableView.separatorStyle = .none
                cell.selectionStyle = .none
                
                //get the index
                let index1 = indexPath.row
                
                //set the dept name
                cell.lblValue1.text = arrFilteredDeparmentList[index1].strDepartment
                
                //set the tag
                cell.btnValue1.tag = index1
                
                //set the target
                cell.btnValue1.addTarget(self, action: #selector(btnDeptAction(sender:)), for: .touchUpInside)
                
                //check if the dept is selected
                if arrFilteredDeparmentList[index1].isSelected {
                    //case - dept selected
                    
                    //set it as selected by changing background & text color
                    cell.btnValue1.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue1.textColor = UIColor.white
                }
                else {
                    //case - dept is not selected
                    
                    //deselected by changing background & text color
                    cell.btnValue1.backgroundColor = UIColor.clear
                    cell.lblValue1.textColor = ColorCodeConstant.darkTextcolor
                }
                
                return cell
                
            }
            else {
                //case when the option table lists the individuals(users) list
                
                //custom cell to show users list
                let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCellIndi") as! ValuesTableViewCell
                //To remove seprator from particular cell
                tableView.separatorStyle = .none
                cell.selectionStyle = .none
                
                //get the index
                let index1 = indexPath.row
                
                //set the dept name
                cell.lblValue2.text = arrFilteredUserList[index1].strName
                
                //set the tag
                cell.btnValue2.tag = index1
                
                //set the target
                cell.btnValue2.addTarget(self, action: #selector(btnIndiAction(sender:)), for: .touchUpInside)
                
                //check if the user is selected
                if arrFilteredUserList[index1].isSelected {
                    //case - user selected
                    
                    //set it as selected by changing background & text color
                    cell.btnValue2.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue2.textColor = UIColor.white
                }
                else {
                    //case - user is not selected
                    
                    //deselected by changing background & text color
                    cell.btnValue2.backgroundColor = UIColor.clear
                    cell.lblValue2.textColor = ColorCodeConstant.darkTextcolor
                }
                
                return cell                
            }
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    //MARK: - UISearchBar Delegates
    /**
     Serach bar delegate method - called when serach bar cancel button cliked
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     Serach bar delegate method - called when serach bar search button cliked
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     Serach bar delegate method - called when user types in
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        if isDept {
            //when its dept list search
            arrFilteredDeparmentList = searchText.isEmpty ? arrOfGetDeparmentList : arrOfGetDeparmentList.filter { (item: DepartmentModel) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.strDepartment.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        else {
            //when its individual/users list search
            arrFilteredUserList = searchText.isEmpty ? arrOfGetUserList : arrOfGetUserList.filter { (item: UserModel) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        tblOptions.reloadData()
    }
    
    //MARK: - Calling Web service
    /***
     API - No longer in use
     */
    func callWebServiceForUnReadNotifications(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.getBubbleRatingUnReadNotificationList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse the response
                    DashboardParser.parseUnReadNotificationList(response: response!, completionHandler: { (arrUnRead, _) in
                        
                        if arrUnRead.count == 0 {
                            //if no pending notifications do not show the count on bell icon
                            self.lblUnredCount.layer.borderWidth = 0.0
                            self.lblUnredCount.text = " "
                            self.lblUnredCount.isHidden = true
                        }
                        else {
                            //if there are pending notifications show the count on bell icon
                            self.lblUnredCount.layer.borderWidth = 1.0
                            self.lblUnredCount.text = String(arrUnRead.count)
                            self.lblUnredCount.isHidden = false
                        }
                    })
                }
                else {
                    print(errorMsg ?? "")
                    //Show error message if no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API is called just to check the if the logged in user is authorised or not
     */
    @objc func callWebServiceToGetUnreadCount() {
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleUnReadNotifications, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["data"]["notificationCount"].stringValue == "0" {
                    self.lblUnredCount.layer.borderWidth = 0.0
                    self.lblUnredCount.text = " "
                    self.lblUnredCount.isHidden = true
                }
                else {
                    self.lblUnredCount.layer.borderWidth = 1.0
                    self.lblUnredCount.text = response!["data"]["notificationCount"].stringValue
                    self.lblUnredCount.isHidden = false
                }
            }
        }
    }
    
    /**
     API - Load details for the dashbaord
     */
    //(lblHistoryDescription.text?.height(withConstrainedWidth: lblHistoryDescription.frame.size.width, font: UIFont.systemFont(ofSize: 13)))!
    func callWebServiceForDashboardDetails() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["orgId": AuthModel.sharedInstance.orgId] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.getDashboardDetails, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("Succesfully uploaded")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //Set static text 'Aim + organisation name'
                self.lblAim.text = "Aim " + AuthModel.sharedInstance.orgName
                
                //Set aim of organisation
                self.lblAimValue.text = "\"" + response!["data"]["vision"].stringValue + "\""
                
                //Set static text 'Be + organisation name'
                self.lblBe.text = "Be " + AuthModel.sharedInstance.orgName
                
                //Set static text 'Know + organisation name'
                self.lblKnow.text = "Know " + AuthModel.sharedInstance.orgName
                
                //                self.lblDirectedPercent.text = response!["data"]["tribeometerDetail"].arrayValue[0]["percentage"].stringValue + "%"
                //                self.lblConnectedPercent.text = response!["data"]["tribeometerDetail"].arrayValue[1]["percentage"].stringValue + "%"
                //                self.lblHappyIndexValue.text = response!["data"]["happyIndex"].stringValue + "%"
                
                //Set happy index percentage
                self.lblHappyIndexValue.text = (String(format: "%.2f", response!["data"]["happyIndex"].doubleValue) + "%").replacingOccurrences(of: ".00", with: "")
                
                //set organisation index percentage
                self.lblOrgIndex.text = String(format: "%.2f", response!["data"]["indexOrg"].doubleValue).replacingOccurrences(of: ".00", with: "")
                
                //set dept index percentage
                self.lblDeptIndex.text = String(format: "%.2f", response!["data"]["indexDept"].doubleValue).replacingOccurrences(of: ".00", with: "")
                
                //get organisation index DD value
                let ddOI = response!["data"]["indexOrgDD"].doubleValue
                
                //get organisation index MA value
                let maOI = response!["data"]["indexOrgMA"].doubleValue
                
                if ddOI > 0 {
                    //case when DD value is positive
                    self.lblOrgIndexDD.text = (String(format: " %.2f", ddOI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.OrgIndexDDView.layer.borderWidth = 1.0
                    self.OrgIndexDDView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblOrgIndexDD.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgOrgIndexDD.image =  UIImage(named: "up")
                }
                else if ddOI < 0 {
                    //case when DD value is negative
                    
                    let pstv = ddOI * -1
                    self.lblOrgIndexDD.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.OrgIndexDDView.layer.borderWidth = 1.0
                    self.OrgIndexDDView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblOrgIndexDD.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgOrgIndexDD.image =  UIImage(named: "down")
                }
                else {
                    //case when DD value is zero
                    self.lblOrgIndexDD.text = (String(format: " %.2f", ddOI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.OrgIndexDDView.layer.borderWidth = 0.0
                    self.imgOrgIndexDD.image =  nil
                }
                
                if maOI > 0 {
                    //case when MA value is positive
                    self.lblOrgIndexMA.text = (String(format: " %.2f", maOI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.OrgIndexMAView.layer.borderWidth = 1.0
                    self.OrgIndexMAView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblOrgIndexMA.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgOrgIndexMA.image =  UIImage(named: "up")
                }
                else if maOI < 0 {
                    //case when DD value is negative
                    let pstv = maOI * -1
                    self.lblOrgIndexMA.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.OrgIndexMAView.layer.borderWidth = 1.0
                    self.OrgIndexMAView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblOrgIndexMA.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgOrgIndexMA.image =  UIImage(named: "down")
                    
                }
                else {
                    //case when DD value is zero
                    self.lblOrgIndexMA.text = (String(format: " %.2f", maOI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.OrgIndexMAView.layer.borderWidth = 0.0
                    self.imgOrgIndexMA.image =  nil
                }
                
                //get department index DD value
                let ddDI = response!["data"]["indexDepDD"].doubleValue
                
                //get department index MA value
                let maDI = response!["data"]["indexDeptMA"].doubleValue
                
                if ddDI > 0 {
                    //case when DD value is positive
                    self.lblDeptIndexDD.text = (String(format: " %.2f", ddDI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.deptIndexDDView.layer.borderWidth = 1.0
                    self.deptIndexDDView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblDeptIndexDD.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgdeptIndexDD.image =  UIImage(named: "up")
                }
                else if ddDI < 0 {
                    //case when DD value is negative
                    let pstv = ddDI * -1
                    self.lblDeptIndexDD.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.deptIndexDDView.layer.borderWidth = 1.0
                    self.deptIndexDDView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblDeptIndexDD.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgdeptIndexDD.image =  UIImage(named: "down")
                }
                else {
                    //case when DD value is zero
                    self.lblDeptIndexDD.text = (String(format: " %.2f", ddDI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.deptIndexDDView.layer.borderWidth = 0.0
                    self.imgdeptIndexDD.image =  nil
                }
                
                if maDI > 0 {
                    //case when MA value is positive
                    self.lblDeptIndexMA.text = (String(format: " %.2f", maDI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.deptIndexMAView.layer.borderWidth = 1.0
                    self.deptIndexMAView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblDeptIndexMA.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgdeptIndexMA.image =  UIImage(named: "up")
                    
                }
                else if maDI < 0 {
                    //case when MA value is negative
                    let pstv = maDI * -1
                    self.lblDeptIndexMA.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.deptIndexMAView.layer.borderWidth = 1.0
                    self.deptIndexMAView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblDeptIndexMA.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgdeptIndexMA.image =  UIImage(named: "down")
                    
                }
                else {
                    //case when MA value is zero
                    self.lblDeptIndexMA.text = (String(format: " %.2f", maDI) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.deptIndexMAView.layer.borderWidth = 0.0
                    self.imgdeptIndexMA.image =  nil
                }
                
                //get happy index DD value
                let dd = response!["data"]["happyIndexDD"].doubleValue
                
                //get happy index MA value
                let ma = response!["data"]["happyIndexMA"].doubleValue
                
                if dd > 0 {
                    //case when DD value is positive
                    self.lblHappyIndexDD.text = (String(format: " %.2f", dd) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.happyIndexDDView.layer.borderWidth = 1.0
                    self.happyIndexDDView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblHappyIndexDD.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgHappyIndexDD.image =  UIImage(named: "up")
                }
                else if dd < 0 {
                    //case when DD value is negative
                    let pstv = dd * -1
                    self.lblHappyIndexDD.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.happyIndexDDView.layer.borderWidth = 1.0
                    self.happyIndexDDView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblHappyIndexDD.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgHappyIndexDD.image =  UIImage(named: "down")
                }
                else {
                    //case when DD value is zero
                    self.lblHappyIndexDD.text = (String(format: " %.2f", dd) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.happyIndexDDView.layer.borderWidth = 0.0
                    self.imgHappyIndexDD.image =  nil
                }
                
                if ma > 0 {
                    //case when MA value is positive
                    self.lblHappyIndexMA.text = (String(format: " %.2f", ma) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as green
                    self.happyIndexMAView.layer.borderWidth = 1.0
                    self.happyIndexMAView.layer.borderColor = ColorCodeConstant.happyIndexGreenColor.cgColor
                    self.lblHappyIndexMA.textColor = ColorCodeConstant.happyIndexGreenColor
                    
                    //set an up arrow
                    self.imgHappyIndexMA.image =  UIImage(named: "up")
                }
                else if ma < 0 {
                    //case when MA value is negative
                    let pstv = ma * -1
                    self.lblHappyIndexMA.text = (String(format: " -%.2f", pstv) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //set border and text color as red
                    self.happyIndexMAView.layer.borderWidth = 1.0
                    self.happyIndexMAView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
                    self.lblHappyIndexMA.textColor = ColorCodeConstant.themeRedColor
                    
                    //set a down arrow
                    self.imgHappyIndexMA.image =  UIImage(named: "down")
                }
                else {
                    //case when MA value is zero
                    self.lblHappyIndexMA.text = (String(format: " %.2f", ma) + "% ").replacingOccurrences(of: ".00", with: "")
                    
                    //no border and image required
                    self.happyIndexMAView.layer.borderWidth = 0.0
                    self.imgHappyIndexMA.image =  nil
                    
                }
                
                //calculate height for Aim of company
                var ht = (self.lblAimValue.text?.height(withConstrainedWidth: self.lblAimValue.frame.size.width, font: UIFont.systemFont(ofSize: 20)))!
                
                //set minimum height as 30
                if ht < 30 {
                    ht = 30
                }
                
                //stores whether user has sent happy index or not in local variable + user defaults
                self.isHappyIndex = response!["data"]["userGivenfeedback"].boolValue
                let defaults = UserDefaults.standard
                defaults.set(self.isHappyIndex, forKey: "isHappyIndex")
                defaults.set(Date(),forKey:"isHappyIndexDate")
                
                //get current date
                let now = Date()
                
                //get 4:01 PM date
                let four_today = now.dateAt(hours: 16, minutes: 1)
                
                //get 11:59 PM date
                let eleven_fiftyNine_today = now.dateAt(hours: 23, minutes: 59)
                
                //check range between 4:00 - 11:59
                if now >= four_today && now <=  eleven_fiftyNine_today
                {
                    //if user has not submitted happy index for the day
                    if self.isHappyIndex == false {
                        //                        self.imgHappyIndex.blink()
                        //animate the happy index view
                        self.happyIndexView.animate()
                    }
                }
                
                //set height of aim value height constraint
                self.heightAim.constant = ht
                
                //set header size according to the height of aim
                self.tblHeaderView.frame.size.height = 797 + ht
                
                //load DOT values
                self.callWebServiceDOTValues()
                
                //load users & depts
                self.callWebServiceToGetDepartmentAnduserList()
            }
        }
    }
    
    /**
     *API - submits happy index value to server
     *moodStatus - accepts the happy index value submitted by user (happy/avg/sad as 3/2/1)
     */
    func callWebServiceToAddHappyIndex(moodStatus: String) {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //prepare param for api
        let param =
            ["userId": AuthModel.sharedInstance.user_id,//logged in user id
                "moodStatus": moodStatus//mood value
                ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.addHappyIndex, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop the loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            //hide the happy index custom view
            self.customAlert.hideView()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                
                //set happy index to true as happy index is not submitted
                self.isHappyIndex = true
            }
            else{
                print("Succesfully uploaded")
                //stop the loader
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //set the happy index values after it is being submitted
                self.resetHappyIndex()
            }
        }
    }
    
    /**
     * API - submits feedback to server
     * param: parameters to send to api
     */
    func callWebServiceToSendFeedback(param: [String : Any]) {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.postFeedback, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                print("Succesfully uploaded")
                //reset the feedback field & image after submission
                self.txtIOTFeedback.text = ""
                self.imgViewIOT.image = nil
                
                //stop the loader
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //show success message of submission
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Feedback sent successfully.")
                self.view.endEditing(true)
            }
        }
    }
    
    /**
     * This function is no longer in use
     */
    func callWebServiceToGetUserDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of error
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    /**
     * This function is no longer in use
     */
    func callWebServiceForChecklist() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.checklist, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                DashboardParser.parseChecklist(response: response!, completionHandler: { (checkList, todoList, showTodo, pendingCount) in
                    self.arrChecklist = checkList
                    self.arrTodos = todoList
                    
                    if showTodo {
                        self.arrSectionTitles = ["First Use Checklist", "To Do List"]
                        //show all sections
                        self.valuesHeightConstraint.constant = 300
                        self.feedbackHeightConstraint.constant = 220
                        self.tblHeaderView.frame.size.height = 1216
                        
                        self.callWebServiceDOTValues()
                        self.callWebServiceToGetDepartmentAnduserList()
                        
                    }
                    else {
                        self.arrSectionTitles = ["First Use Checklist"]
                        //show only menu options
                        self.valuesHeightConstraint.constant = 0
                        self.feedbackHeightConstraint.constant = 0
                        self.tblHeaderView.frame.size.height = 696
                    }
                    
                    self.tblView.reloadData()
                    
                    if pendingCount == 0 {
                        self.btnChecklistCount.setImage(UIImage(named: "checkRed"), for: .normal)
                        self.btnChecklistCount.setTitle("", for: .normal)
                    }
                    else {
                        self.btnChecklistCount.setTitle(String(pendingCount) + "!", for: .normal)
                    }
                    
                    //                    let badgeCount: Int = pendingCount
                    //                    let defaults = UserDefaults.standard
                    //                    defaults.set(pendingCount, forKey: "pendingCountForChecklist")
                    //                    let application = UIApplication.shared
                    //                    let center = UNUserNotificationCenter.current()
                    //                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    //                        // Enable or disable features based on authorization.
                    //                    }
                    //                    application.registerForRemoteNotifications()
                    //                    application.applicationIconBadgeNumber = badgeCount
                })
            }
        }
    }
    
    /**
     * This function is no longer in use
     */
    func callWebServiceForActions() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id,
             "orgId": strOrgID == "" ? AuthModel.sharedInstance.orgId : strOrgID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getActionStatusCount, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                DashboardParser.parseActions(response: response!, completionHandler: { (started, notstarted, completed) in
                    
                    if started + notstarted == 0 {
                        self.btnActionCount.setImage(UIImage(named: "checkRed"), for: .normal)
                        self.btnActionCount.setTitle("", for: .normal)
                    }
                    else {
                        self.btnActionCount.setTitle(String(started + notstarted) + "!", for: .normal)
                    }
                    
                    
                })
            }
        }
    }
    
    /**
     * API - get DOT Details
     */
    func callWebServiceDOTDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare param for api
        let param = ["orgId":AuthModel.sharedInstance.orgId] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.DOTDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //get dot view instance
                    let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                    //pass organisation id
                    objVC.strOrgID = AuthModel.sharedInstance.orgId
                    //push view to viewDOTView
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                else {
                    //show error message in case no dot is added for the organisation
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No DOT Added for this organisation.")
                    
                }
            }
        }
    }
    
    /**
     * API - load DOT values
     */
    func callWebServiceDOTValues() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare paramaters for api
        let param = ["orgId":AuthModel.sharedInstance.orgId] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.getDOTValuesList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse values
                    DashboardParser.parseDOTValues(response: response!, completionHandler: { (arr) in
                        
                        //assign values array
                        self.arrValues = arr
                        
                        //load table
                        self.tblValues.reloadData()                        
                    })
                }
                else {
                    //show error message in case no dot added
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No DOT Added for this organisation.")
                }
            }
        }
    }
    
    /**
     * API - load user and dept list
     */
    func callWebServiceToGetDepartmentAnduserList(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //prepare parameter for api
        let param =
            ["orgId": AuthModel.sharedInstance.orgId] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getDepartmentAndUserList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse depts & users list
                    DashboardParser.parseDepartmentUserList(response: response!,removeSelfUser: true, completionHandler: { (arrDep, arrUser) in
                        
                        //assign the parsed array to there respected values
                        self.arrOfGetDeparmentList = arrDep
                        self.arrOfGetUserList = arrUser
                        
                        self.arrFilteredUserList = arrUser
                        self.arrFilteredDeparmentList = arrDep
                        
                        //load options table
                        self.tblOptions.reloadData()
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    //show erroe message in case of no response
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - Submits thumbs up to selected users & depts on specific values
     */
    func callWebServiceToAddThumbsUp() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //get selected userid's array
        var arrUserIDs = [String]()
        for value in arrSelectedUsers {
            arrUserIDs.append(value.strId)
        }
        
        //get selected dept id's array
        var arrDeptIDs = [String]()
        for value in arrSelectedDept {
            arrDeptIDs.append(value.strId)
        }
        
        //get selected values array
        var arrSelectedValues = [ValueModel]()
        for value in arrValues {
            if value.isSelected {
                arrSelectedValues.append(value)
            }
        }
        
        //create options array for selected values with there beliefID, valueID & valueNameID
        var arrOptions = [[String:Any]]()
        for value in arrSelectedValues {
            var dict = [String:Any]()
            dict["dotBeliefId"] = value.strBeliefId
            dict["dotValueId"] = value.strId
            dict["dotValueNameId"] = value.strValueId
            
            arrOptions.append(dict)
        }
        
        //prepare params
        let param = ["toUserId": arrUserIDs,//selected user id's array
            "departmentIdArray": arrDeptIDs,//selected dept id's array
            "dotId": arrValues[0].strDotId,//dot id
            "options": arrOptions,//options array
            "bubbleFlag": "1"//1 for selected
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.addDOTBubbleRatingsToMultiDepartment, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //show success message
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Kudos sent successfully.")
                    
                    //hide the thumbs up / kudos view & reset to feedback view
                    self.thumsUpSelectionView.isHidden = true
                    self.iotFeedbackView.isHidden = false
                    
                    //reset values for selected options
                    self.resetSelectedValues()
                    self.tblValues.reloadData()
                    
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - No longer in use
     */
    func requestWith(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    self.txtIOTFeedback.text = ""
                    self.imgViewIOT.image = nil
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Feedback sent successfully.")
                    self.view.endEditing(true)
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}


//extension UISearchBar {
//
//    /*        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
//
//    //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
//    //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
//    searchBar.layer.cornerRadius = 18
//    searchTextField.borderStyle = .none
//    searchTextField.font = UIFont.systemFont(ofSize: 13.0)
//    searchTextField.textAlignment = NSTextAlignment.left
//    let image:UIImage = UIImage(named: "search")!
//    let imageView:UIImageView = UIImageView.init(image: image)
//    searchTextField.leftView = nil
//    searchTextField.placeholder = " Search"
//    searchTextField.leftView = imageView
//    searchTextField.rightViewMode = UITextFieldViewMode.always
//*/
//
//    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
//    func set(textColor: UIColor) { if let textField = getTextField() { textField.textColor = textColor } }
//    func setPlaceholder(textColor: UIColor) { getTextField()?.setPlaceholder(textColor: textColor) }
//    func setClearButton(color: UIColor) { getTextField()?.setClearButton(color: color) }
//
//    func setTextField(color: UIColor) {
//        guard let textField = getTextField() else { return }
//        switch searchBarStyle {
//        case .minimal:
//            textField.layer.backgroundColor = color.cgColor
//            textField.layer.cornerRadius = 18
//            textField.borderStyle = .none
//            textField.font = UIFont.systemFont(ofSize: 13.0)
//            textField.textAlignment = .left
//            let image:UIImage = UIImage(named: "search")!
//            let imageView:UIImageView = UIImageView.init(image: image)
//            textField.leftView = nil
//            textField.placeholder = " Search"
//            textField.leftView = imageView
//            textField.rightViewMode = UITextFieldViewMode.always
//
//        case .prominent, .default: textField.backgroundColor = color
//        @unknown default: break
//        }
//    }
//
//    func setSearchImage(color: UIColor) {
//        guard let imageView = getTextField()?.leftView as? UIImageView else { return }
//        imageView.tintColor = color
//        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
//    }
//}
//
//private extension UITextField {
//
//    private class Label: UILabel {
//        private var _textColor = UIColor.lightGray
//        override var textColor: UIColor! {
//            set { super.textColor = _textColor }
//            get { return _textColor }
//        }
//
//        init(label: UILabel, textColor: UIColor = .lightGray) {
//            _textColor = textColor
//            super.init(frame: label.frame)
//            self.text = label.text
//            self.font = label.font
//        }
//
//        required init?(coder: NSCoder) { super.init(coder: coder) }
//    }
//
//
//    private class ClearButtonImage {
//        static private var _image: UIImage?
//        static private var semaphore = DispatchSemaphore(value: 1)
//        static func getImage(closure: @escaping (UIImage?)->()) {
//            DispatchQueue.global(qos: .userInteractive).async {
//                semaphore.wait()
//                DispatchQueue.main.async {
//                    if let image = _image { closure(image); semaphore.signal(); return }
//                    guard let window = UIApplication.shared.windows.first else { semaphore.signal(); return }
//                    let searchBar = UISearchBar(frame: CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 44))
//                    window.rootViewController?.view.addSubview(searchBar)
//                    searchBar.text = "txt"
//                    searchBar.layoutIfNeeded()
//                    _image = searchBar.getTextField()?.getClearButton()?.image(for: .normal)
//                    closure(_image)
//                    searchBar.removeFromSuperview()
//                    semaphore.signal()
//                }
//            }
//        }
//    }
//
//    func setClearButton(color: UIColor) {
//        ClearButtonImage.getImage { [weak self] image in
//            guard   let image = image,
//                let button = self?.getClearButton() else { return }
//            button.imageView?.tintColor = color
//            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
//        }
//    }
//
//    var placeholderLabel: UILabel? { return value(forKey: "placeholderLabel") as? UILabel }
//
//    func setPlaceholder(textColor: UIColor) {
//        guard let placeholderLabel = placeholderLabel else { return }
//        let label = Label(label: placeholderLabel, textColor: textColor)
//        setValue(label, forKey: "placeholderLabel")
//    }
//
//    func getClearButton() -> UIButton? { return value(forKey: "clearButton") as? UIButton }
//}
