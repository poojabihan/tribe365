//
//  PerformanceViewController.swift
//  Tribe365
//
//  Created by Apple on 08/07/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 This class is used to show the performance of different modules
 */
class PerformanceViewController: UIViewController, HADropDownDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: - IBOutlets
    //custom drop down for organisation
    @IBOutlet weak var dropDownOrganosation: HADropDown!
    
    //custom drop down for time
    @IBOutlet weak var dropDownTime: HADropDown!
    
    //month text field
    @IBOutlet weak var txtMonth: UITextField!
    
    //thumbs percentage label
    @IBOutlet weak var lblThumbsPercent: UILabel!
    
    //dot percent label
    @IBOutlet weak var lblDOTPercent: UILabel!
    
    //dot updated percentage label
    @IBOutlet weak var lblDOTUpdatePercent: UILabel!
    
    //team role percent label
    @IBOutlet weak var lblTeamRolesPercent: UILabel!
    
    //team role updated percent label
    @IBOutlet weak var lblTeamRolesUpdatePercent: UILabel!
    
    //personality type percent label
    @IBOutlet weak var lblPersonalityTypePercent: UILabel!
    
    //personality type updated percent label
    @IBOutlet weak var lblPersonalityTypeUpdatePercent: UILabel!
    
    //organisation structure percent label
    @IBOutlet weak var lblOrgStructurePercent: UILabel!
    
    //organisation structure updated percent label
    @IBOutlet weak var lblOrgStructureUpdatePercent: UILabel!
    
    //motivation percent label
    @IBOutlet weak var lblMotivationPercent: UILabel!
    
    //motivation updated percent label
    @IBOutlet weak var lblMotivationUpdatePercent: UILabel!
    
    //feedbacks percent label
    @IBOutlet weak var lblImprovementsPercent: UILabel!
    
    //transparent background view for the month popup
    @IBOutlet weak var alphaView: UIView!
    
    //calendar view
    @IBOutlet weak var calenderView: UIView!
    
    
    //picker view
    @IBOutlet weak var pickerView: UIPickerView!
    
    //MARK: - Variables
    //to show error/warning
    let panel = JKNotificationPanel()
    
    //stores organisation names list
    var arrOrganisationName = [String]()
    
    //stores organisation model list
    var arrOrganisationModel = [OrganisationModel]()
    
    //stores selected organisation
    var selectedOrgIndex = -1
    
    //stores selected type of timeline
    var selectedType = "daily"
    
    //stores the selected year
    var selectedYear = ""
    
    //stores the selected month
    var selectedMonth = ""
    
    //stores the selected date
    var selectedDate = ""
    
    //variables For Calender View
    var monthsArray = [String]()
    var newArray: [String] = []
    var yearsArray = [String]()
    var selectedIndexForYears = 0
    var selectedIndexForMonths = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //set drop down delegate
        dropDownOrganosation.delegate = self
        dropDownTime.delegate = self
        
        //set type dropdown values
        dropDownTime.items = ["DAILY", "MONTHLY"]
        
        //load organisation list
        callWebServiceForOrganisationListing()
        
        //load performance data
        callWebServicePerformance()
        
        //set picker view delegates
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        //hide alpha & calendar view
        alphaView.alpha = 0
        calenderView.alpha = 0
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    /**
     This function sets data for organisation drop down with an "ALL"  feature
     */
    func setData() {
        dropDownOrganosation.items = ["ALL ORGANISATIONS"] + arrOrganisationName
    }
    
    /**
     This function prepares array of month for selected year
     */
    func SetArrayForMonth(){
        
        //empty all array
        monthsArray.removeAll()
        yearsArray.removeAll()
        newArray.removeAll()
        
        //get current year
        let nameOfYear = Calendar.current.component(.year, from: Date())
        
        //loop for 5
        for i in 0..<5 {
            yearsArray.append("\(nameOfYear - i)")
        }
        
        //check if selected year is current year
        if yearsArray[selectedIndexForYears] == String(nameOfYear)
        {
            //todays date
            let now = Date()
            
            //instance of date formatter
            let dateFormatter = DateFormatter()
            
            //set date formate to display only month
            dateFormatter.dateFormat = "MM"
            
            //get current month
            let nameOfMonth = dateFormatter.string(from: now)
            
            //prepare list of months
            monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            
            //get the previous month
            let preFromCurrentMonth = Int(nameOfMonth)! - 01
            //            let indexcount = monthsArray.count - preFromCurrentMonth
            
            //set the previous month as minmum range
            let minRange = preFromCurrentMonth
            //            let maxRange = indexcount + 1
            
            //loop for month array
            for i in 0..<monthsArray.count {
                
                //do not include the month if it is greater than previous month
                if i >= minRange /*&& i <= maxRange*/ {
                    /// Avoid
                    continue
                }
                newArray.append(monthsArray[i])
            }
            
            //get the new month array
            monthsArray = newArray
            
            //reset the picker values
            pickerView.selectRow(0, inComponent: 0, animated: true)
            pickerView.reloadAllComponents()
        }
        else{
            //set month array from Jan to Dec in case selected year is not the current year
            monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            pickerView.reloadAllComponents()
            
        }
    }
    
    /**
     Open calendar picker
     */
    func openPicker() {
        alphaView.alpha = 0.8
        calenderView.alpha = 1.0
        SetArrayForMonth()
        pickerView.reloadAllComponents()
    }
    
    //MARK: - HADropDown Delegate Method
    /**
     Drop down delegate method
     This is called when user selects any value for drop down
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        // dropDownDept.item
        print("Item selected at index \(index)")
        
        if dropDown == dropDownTime {
            //case when drop down is time
            
            if index == 1 {
                //txtMonth.becomeFirstResponder()
                //when type is monthly, then open picker
                openPicker()
            }
            else {
                //when type is daily, then load performance data according to selected filter
                selectedType = "daily"
                selectedDate = ""
                callWebServicePerformance()
            }
        }
        else {
            //case when drop down is organisation, load performance data according to selected filter
            
            selectedOrgIndex = index - 1
            callWebServicePerformance()
        }
    }
    
    //MARK: - UITextField Delegates
    /**
     This is textfield delegate
     */
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtMonth{
            //set title for drop down and load data for selected filter
            dropDownTime.title = txtMonth.text!
            selectedType = txtMonth.text!
            callWebServicePerformance()
        }
    }
    
    //MARK: - IBAction Actions
    /**
     This function is called when month picker done button is clicked 
     */
    @IBAction func btnCalenderSaveAction(_ sender: Any) {
        
        //hide calendar view and all its subviews
        alphaView.alpha = 0
        calenderView.alpha = 0
        print("Selected Year",yearsArray[selectedIndexForYears])
        print("Selected Month",monthsArray[selectedIndexForMonths])
        
        //save selected year and month
        selectedYear = yearsArray[selectedIndexForYears]
        selectedMonth = String(selectedIndexForMonths + 1)
        
        print(monthsArray[selectedIndexForMonths] + "-" +  selectedYear)
        //set title for drop down
        dropDownTime.title = monthsArray[selectedIndexForMonths] + "-" +  selectedYear
        
        //save selected type
        selectedType = "monthly"
        selectedDate = monthsArray[selectedIndexForMonths] + "-" +  selectedYear
        
        //load data according to selected filters
        callWebServicePerformance()
    }
    
    /**
     This function is used to close the calendar view
     */
    @IBAction func btnCalenderCancelAction(_ sender: Any) {
        //hide the calendar view
        alphaView.alpha = 0
        calenderView.alpha = 0
    }
    
    /**
     Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     This function is called when send notification button is clicked (but it is of no use)
     */
    @IBAction func btnSendNotificationAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 10:
            //Thumb
            break
        case 20:
            //Improvement
            break
        case 30:
            //DOT Complete
            break
        case 40:
            //DOT Update
            break
        case 50:
            //TeamRole complete
            break
        case 60:
            //TeamRole Update
            break
        case 70:
            //Personality Complete
            break
        case 80:
            //Personality update
            break
        case 90:
            //Org complete
            break
        case 100:
            //Org update
            break
        case 110:
            //motivation complete
            break
        case 120:
            //motivation update
            break
        default:
            print("Default case")
        }
    }
    
    //MARK: - UIpicker view Delelgate and Datasource
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        //two components one for year and one for month
        return 2
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if component == 0 {
            //month array of first component
            return monthsArray.count
        }
        else{
            //year array of second component
            return yearsArray.count
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return monthsArray[row]
        case 1:
            return "\(yearsArray[row])"
        default:
            return nil
        }
    }
    
    //This is called when a value is selected
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 1 {
            //save selected year data
            selectedIndexForYears = pickerView.selectedRow(inComponent: 1)
            selectedIndexForMonths = 0
            pickerView.selectRow(0, inComponent: 0, animated: true)
            print(selectedIndexForYears)
            SetArrayForMonth()
        }
        else if component == 0 {
            //save selected month data
            selectedIndexForMonths = pickerView.selectedRow(inComponent: 0)
            print(selectedIndexForMonths)
            if selectedIndexForMonths == 0
            {
                SetArrayForMonth()
            }
        }
    }
    
    //MARK: - WebService Methods
    /**
     API - To get organisation list
     */
    func callWebServiceForOrganisationListing() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Organisation.orgList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            print(response ?? "")
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse organisation list
                    DashboardParser.parseOrganisationList(response: response!, completionHandler: { (arrOrg) in
                        self.arrOrganisationModel = arrOrg
                    })
                    
                    //loop through organisation model and store organisation name in arrOrganisationName array
                    for value in self.arrOrganisationModel {
                        self.arrOrganisationName.append(value.strOrganisation)
                    }
                    
                    //setup data for organisation dropdown
                    self.setData()
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - To get performance data
     */
    func callWebServicePerformance() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =
            [    "orgId"    : selectedOrgIndex == -1 ? "" : arrOrganisationModel[selectedOrgIndex].strOrganisation_id,
                 "reportType"        : selectedType,
                 "date": selectedDate] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getPerformance, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //set all percentages to the relevent labels from the json result
                    self.lblThumbsPercent.text = response!["data"]["thumbCompleted"].stringValue
                    self.lblImprovementsPercent.text = response!["data"]["improvements"].stringValue
                    self.lblDOTPercent.text = response!["data"]["dotCompleted"].stringValue + "%"
                    self.lblDOTUpdatePercent.text = response!["data"]["dotUpdated"].stringValue + "%"
                    self.lblTeamRolesPercent.text = response!["data"]["tealRoleMapCompleted"].stringValue + "%"
                    self.lblTeamRolesUpdatePercent.text = response!["data"]["tealRoleMapUpdated"].stringValue + "%"
                    self.lblPersonalityTypePercent.text = response!["data"]["personalityTypeCompleted"].stringValue + "%"
                    self.lblPersonalityTypeUpdatePercent.text = response!["data"]["personalityTypeUpdated"].stringValue + "%"
                    self.lblOrgStructurePercent.text = response!["data"]["cultureStructureCompleted"].stringValue + "%"
                    self.lblOrgStructureUpdatePercent.text = response!["data"]["cultureStructureUpdated"].stringValue + "%"
                    self.lblMotivationPercent.text = response!["data"]["motivationCompleted"].stringValue + "%"
                    self.lblMotivationUpdatePercent.text = response!["data"]["motivationCountUpdated"].stringValue + "%"
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
