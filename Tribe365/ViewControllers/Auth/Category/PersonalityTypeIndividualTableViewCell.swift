//
//  PersonalityTypeIndividualTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 24/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class was used to show the result for old personality type
 * It is no longer in use now
 */
class PersonalityTypeIndividualTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionViewOfScore: UICollectionView!
    @IBOutlet weak var collectionViewOfDichotomies: UICollectionView!
    @IBOutlet weak var btnIntialResult: UIButton!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var btnFullResult: UIButton!
//    @IBOutlet weak var lblSummaryTitle: UILabel!
//    @IBOutlet weak var lblSummaryDescription: UILabel!
//    @IBOutlet weak var heightDynamicconstraint: NSLayoutConstraint!

    var selectedIndexCollectionDichotomies = -1
    var selectedIndexCollectionScores = -1
    var arrOfAllCombinationsValue = [AllCombinationModel]()
    var arrForAllSummary = [ModelForAllSummary]()
    var arrOfFuncLensKeyDetail = [FuncLensKeyDetailModel]()
    var arrOfIntialValueList = [InitialValueListModel]()
    var arrOfTribeTips = [TribeTipsArray]()
    var isCOTfunclensAnswersDone = Bool()
    var isUser = false
    var obj = KnowMembersViewController()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Custom Methods
    func setData(response: JSON) {
        
        COTParser.parseCOTFunctionalLensDetailKnow(response: response, completionHandler: { (arrOfFunctionalLens,arrOfIntialValues,arrtribeTips,arrOfAllCombination, isCOTfunclensAnswersDone ) in
            
            self.arrOfFuncLensKeyDetail = arrOfFunctionalLens
            self.arrOfIntialValueList = arrOfIntialValues
            self.arrOfTribeTips = arrtribeTips
            self.arrOfAllCombinationsValue = arrOfAllCombination
            self.isCOTfunclensAnswersDone = isCOTfunclensAnswersDone
        })
        
            self.collectionViewOfScore.reloadData()
            self.collectionViewOfDichotomies.reloadData()
            
            //call single Array for Summary
            if !self.isUser {
                
                
                    for i in (0..<self.arrOfFuncLensKeyDetail.count){
                        
                        if i == 0 || i == 2{
                            self.arrOfFuncLensKeyDetail.remove(at: i)
                        }
                        /*else if i == 2{
                         self.arrOfFuncLensKeyDetail.remove(at: i)
                         }*/
                    }
                    for i in (0..<self.arrOfIntialValueList.count){
                        
                        if i == 0 || i == 2{
                            self.arrOfIntialValueList.remove(at: i)
                        }
                        /*else if i == 2{
                         self.arrOfFuncLensKeyDetail.remove(at: i)
                         }*/
                    }
                    print(self.arrOfFuncLensKeyDetail.count)
                    self.CreateTeamResultData()
                
            }
            else{
                self.CreateIntialResultData()
            }
        

    }
    
    func CreateTeamResultData(){
        
        selectedIndexCollectionDichotomies = -1
        selectedIndexCollectionScores = -1
        
        self.btnIntialResult.isHidden = true
        if self.arrOfTribeTips.count > 0 {
            
            arrForAllSummary.removeAll()
            
            //Append Summary
            let model = ModelForAllSummary()
            
            model.strtitle = /*"Summary: "*/self.arrOfTribeTips[0].strTitle.uppercased()
            
            model.strValue = NSMutableAttributedString.init(string: self.arrOfTribeTips[0].strSummary.capitalized)
            
            self.arrForAllSummary.append(model)
            
            var strSeek = ""
            for i in (0..<self.arrOfTribeTips[0].arrSeekValues.count){
                
                let model = ModelForAllSummary()
                
                model.strtitle = self.arrOfTribeTips[0].arrSeekValues[i].strValueType.capitalized
                
                
                if strSeek == "" {
                    strSeek = "\u{2022} " + self.arrOfTribeTips[0].arrSeekValues[i].strValue
                }
                else {
                    strSeek = strSeek + "\n\u{2022} " +  self.arrOfTribeTips[0].arrSeekValues[i].strValue
                }
                
            }
            
            if self.arrOfTribeTips[0].arrSeekValues.count > 0 {
                let model = ModelForAllSummary()
                model.strtitle = self.arrOfTribeTips[0].arrSeekValues[0].strValueType.capitalized
                model.strValue = NSMutableAttributedString.init(string: strSeek)
                self.arrForAllSummary.append(model)
            }
            
            
            var strPersuade = ""
            
            for i in (0..<self.arrOfTribeTips[0].arrPersuadeValues.count){
                
                let model = ModelForAllSummary()
                
                model.strtitle = self.arrOfTribeTips[0].arrPersuadeValues[i].strValueType.capitalized
                
                //                        model.strValue = self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                
                if strPersuade == "" {
                    strPersuade = "\u{2022} " + self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                }
                else {
                    strPersuade = strPersuade + "\n\u{2022} " + self.arrOfTribeTips[0].arrPersuadeValues[i].strValue
                }
                
            }
            
            if self.arrOfTribeTips[0].arrPersuadeValues.count > 0 {
                let model = ModelForAllSummary()
                model.strtitle = self.arrOfTribeTips[0].arrPersuadeValues[0].strValueType.capitalized
                model.strValue = NSMutableAttributedString.init(string: strPersuade)
                self.arrForAllSummary.append(model)
            }
            
        }
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
//        tblView.reloadData()
        
        if arrForAllSummary[0].strtitle == ""{
            lblSummary.isHidden = true
//            self.lblSummaryTitle.isHidden = true
//            self.lblSummaryDescription.isHidden = true
        }
        else{
            lblSummary.isHidden = false
//            self.lblSummaryTitle.isHidden = false
//            self.lblSummaryDescription.isHidden = false
//            self.lblSummaryTitle.text = arrForAllSummary[0].strtitle
//            self.lblSummaryDescription.attributedText = arrForAllSummary[0].strValue
        }
        
//        obj.reloadTableData()
//        heightDynamicconstraint.constant = (lblSummaryDescription.text?.height(withConstrainedWidth: lblSummaryDescription.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
        obj.didselectCollectionView(arr: arrForAllSummary)
    }
    
    func CreateIntialResultData(){
        
        selectedIndexCollectionDichotomies = -1
        selectedIndexCollectionScores = -1
        
        self.btnIntialResult.isHidden = true
        
        if self.arrOfAllCombinationsValue.count > 0 {
            
            arrForAllSummary.removeAll()
            lblSummary.isHidden = false
            //Append Summary
            let model = ModelForAllSummary()
            
            model.strtitle = /*"Summary: "*/self.arrOfAllCombinationsValue[0].strTitle.uppercased()
            
            model.strValue = NSMutableAttributedString.init(string: self.arrOfAllCombinationsValue[0].strSummary)
            
            self.arrForAllSummary.append(model)
            
            
        }
        else{
            
            lblSummary.isHidden = true
//            tblView.reloadData()
        }
        
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
//        tblView.reloadData()
        if arrForAllSummary[0].strtitle == ""{
            lblSummary.isHidden = true
//            self.lblSummaryTitle.isHidden = true
//            self.lblSummaryDescription.isHidden = true
        }
        else{
            lblSummary.isHidden = false
//            self.lblSummaryTitle.isHidden = false
//            self.lblSummaryDescription.isHidden = false
//            self.lblSummaryTitle.text = arrForAllSummary[0].strtitle
//            self.lblSummaryDescription.attributedText = arrForAllSummary[0].strValue
        }
        
//        obj.reloadTableData()
        obj.didselectCollectionView(arr: arrForAllSummary)

    }
    
    func createFunctionalArrForSelectedIndex(indexPath : Int){
        
        selectedIndexCollectionDichotomies = indexPath
        selectedIndexCollectionScores = -1
        
        arrForAllSummary.removeAll()
        let model = ModelForAllSummary()
        
        model.strtitle = self.arrOfFuncLensKeyDetail[indexPath].strTitle
        model.strValue = NSMutableAttributedString(string: self.arrOfFuncLensKeyDetail[indexPath].strdescription)
        
        self.arrForAllSummary.append(model)
       // tblView.reloadData()
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
        if arrForAllSummary[0].strtitle == ""{
            lblSummary.isHidden = true
//            self.lblSummaryTitle.isHidden = true
//            self.lblSummaryDescription.isHidden = true
        }
        else{
            lblSummary.isHidden = false
//            self.lblSummaryTitle.isHidden = false
//            self.lblSummaryDescription.isHidden = false
//            self.lblSummaryTitle.text = arrForAllSummary[0].strtitle
//            self.lblSummaryDescription.attributedText = arrForAllSummary[0].strValue
        }
        
//        obj.reloadTableData()
//        heightDynamicconstraint.constant = (lblSummaryDescription.text?.height(withConstrainedWidth: lblSummaryDescription.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
        obj.didselectCollectionView(arr: arrForAllSummary)
    }
    
    func createIntialArrForSelectedIndex(indexPath : Int){
        
        selectedIndexCollectionScores = indexPath
        selectedIndexCollectionDichotomies = -1
        
        arrForAllSummary.removeAll()
        
        let model = ModelForAllSummary()
        
        model.strtitle = self.arrOfIntialValueList[indexPath].strTitle
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold("Positives:")
            .normal(self.arrOfIntialValueList[indexPath].strPositives + "\n")
            .bold("Allowable Weaknesses: ")
            .normal(self.arrOfIntialValueList[indexPath].strAllowableWeaknesses)
        
        model.strValue = formattedString
        self.arrForAllSummary.append(model)
        
        //tblView.reloadData()
        collectionViewOfDichotomies.reloadData()
        collectionViewOfScore.reloadData()
        
        if arrForAllSummary[0].strtitle == ""{
            lblSummary.isHidden = true
//            self.lblSummaryTitle.isHidden = true
//            self.lblSummaryDescription.isHidden = true
        }
        else{
            lblSummary.isHidden = false
//            self.lblSummaryTitle.isHidden = false
//            self.lblSummaryDescription.isHidden = false
//            self.lblSummaryTitle.text = arrForAllSummary[0].strtitle
//            self.lblSummaryDescription.attributedText = arrForAllSummary[0].strValue
        }
        
//        obj.reloadTableData()
//        heightDynamicconstraint.constant = (lblSummaryDescription.text?.height(withConstrainedWidth: lblSummaryDescription.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
        obj.didselectCollectionView(arr: arrForAllSummary)
    }
    
    @IBAction func btnIntialResultAction(_ sender: UIButton) {
        
        btnIntialResult.tag = 1
        arrForAllSummary.removeAll()
        if btnIntialResult.tag == 1{
            
            if !self.isUser {
                
                    CreateTeamResultData()
            }
            else{
                CreateIntialResultData()
            }
        }
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView  == collectionViewOfDichotomies
        {
            return arrOfFuncLensKeyDetail.count
            
        }
        else{
            
            return arrOfFuncLensKeyDetail.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunctionalLensCollectionViewCell", for: indexPath as IndexPath) as! FunctionalLensCollectionViewCell
        
        if collectionView  == collectionViewOfDichotomies
        {
            //condition for border color
            if indexPath.row == selectedIndexCollectionDichotomies {
                cell.lblTxt.layer.borderWidth = 1.0
                cell.lblTxt.layer.borderColor = ColorCodeConstant.borderRedColor.cgColor
            }
            else {
                cell.lblTxt.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            }
            //Printing text
            cell.lblTxt.text = arrOfFuncLensKeyDetail[indexPath.row].strValue
            return cell
            
        }
        else{
            
            let strValue = arrOfFuncLensKeyDetail[indexPath.row].strValue
            print(strValue)
            for j in (0..<self.arrOfIntialValueList.count){
                
                cell.lblTxt.text = "0"
                cell.lblTxt.textColor = ColorCodeConstant.darkTextcolor
                //condition for border color
                if indexPath.row == selectedIndexCollectionScores {
                    cell.lblTxt.layer.borderWidth = 1.0
                    cell.lblTxt.layer.borderColor = ColorCodeConstant.borderRedColor.cgColor
                }
                else {
                    cell.lblTxt.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
                }
                //get 1st word of statement
                
                let onlineString:String =  arrOfIntialValueList[j].strTitle
                
                let substring:String = onlineString.components(separatedBy: " ")[0]
                
                print(substring) // <ONLINE>
                
                //Set Color According to the statements
                
                if substring.lowercased().contains("very")
                {
                    //for very clear --> set Red (e98285)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "e98285").withAlphaComponent(0.5)
                }
                if substring.lowercased().contains("clear")
                {
                    //for clear --> set Orange (f1b48a)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "f1b48a").withAlphaComponent(0.5)
                    
                }
                if substring.lowercased().contains("moderate")
                {
                    //for moderate --> set Light Green (84d2a0)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "84d2a0").withAlphaComponent(0.5)
                    
                }
                if substring.lowercased().contains("slight")
                {
                    //for slight --> set Green (00b050)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "00b050").withAlphaComponent(0.5)
                    
                }
                if substring == ""
                {
                    //for slight --> set Green (00b050)
                    cell.lblTxt.backgroundColor = UIColor(hexString: "999896").withAlphaComponent(0.1)
                    cell.lblTxt.textColor = ColorCodeConstant.darkTextcolor.withAlphaComponent(0.3)
                    
                }
                
                //Printing text
                if strValue == arrOfIntialValueList[j].strValue {
                    print("found Score: ", arrOfIntialValueList[j].strScore)
                    cell.lblTxt.text = arrOfIntialValueList[j].strScore
                    break
                }
            }
            return cell
        }
        
    }
    
    //MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        btnIntialResult.tag = 0
        if btnIntialResult.tag == 0{
            
            btnIntialResult.isHidden = false
        }
        
        if collectionView == collectionViewOfDichotomies
        {
            
            createFunctionalArrForSelectedIndex(indexPath: indexPath.row)
            
        }
        else if collectionView == collectionViewOfScore{
            
            collectionView.isUserInteractionEnabled = true
            if arrOfIntialValueList[indexPath.row].strScore == "0"{
                return
            }
                
            else{
                createIntialArrForSelectedIndex(indexPath: indexPath.item)
            }
        }
    }
}
