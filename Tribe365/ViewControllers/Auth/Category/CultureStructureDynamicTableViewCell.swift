//
//  CultureStructureDynamicTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 18/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell is used to show the culture structure summary row
 */
class CultureStructureDynamicTableViewCell: UITableViewCell {

    //shows the summary text
    @IBOutlet weak var lblSummary: UILabel!
    
    //summary view
    @IBOutlet weak var ViewOfSummary: UIView!
    
    //button for redo/review
    @IBOutlet weak var btnRedoReview: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
