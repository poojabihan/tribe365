//
//  NotificationViewController.swift
//  Tribe365
//
//  Created by Apple on 23/08/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 This class is used to list the user notifications
 */
class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - IBOutlets
    //table to display notifications
    @IBOutlet weak var tblView: UITableView!
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    //image of organisation logo
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //archive all button
    @IBOutlet weak var btnArchiveAll: UIButton!
    
    //height constraint for archive all button
    @IBOutlet weak var heightArchiveConstant: NSLayoutConstraint!
    
    //MARK: - Variables
    //to show errors/warning
    let panel = JKNotificationPanel()
    //    var arrFilteredNotifications = [NotificationModel]()
    
    //detect if notification or archive selected
    var isNotifications = true
    
    //stores array of archived notifications
    var arrReadNotifications = [NotificationModel]()
    
    //stores array of notifications
    var arrUnreadNotifications = [NotificationModel]()
    
    //stores the current page, it is used for pagination
    var currentPage = 1
    
    //detects if there is more data to load in pagination
    var isLoadMore = true
    
    //stores total page count in archive list
    var totalPageCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        // Do any additional setup after loading the view.
        searchBar.delegate = self
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        //load data for notifications list
        callWebServiceForUnReadNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        //
        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.layer.cornerRadius = 1
        //        searchTextField.borderStyle = .none
        //        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
        //        searchTextField.textAlignment = NSTextAlignment.left
        //        let image:UIImage = UIImage(named: "search")!
        //        let imageView:UIImageView = UIImageView.init(image: image)
        //        searchTextField.leftView = nil
        //        searchTextField.placeholder = " Search"
        //        searchTextField.rightView = imageView
        //        searchTextField.rightViewMode = UITextFieldViewMode.always
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation controller
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if isNotifications {
            //load notification list
            callWebServiceForUnReadNotifications()
        }
        tblView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     When segment controller is clicked
     */
    @IBAction func btnArchiveAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            //If notifications tab is selected
            isNotifications = true
            
            //load data for notifications
            callWebServiceForUnReadNotifications()
            
            //show archive all button
            heightArchiveConstant.constant = 50
        }
        else {
            //If archive tab is selected
            isNotifications = false
            
            //load archived notifications
            callWebServiceForReadNotifications()
            
            //hide archive all button
            heightArchiveConstant.constant = 0
        }
    }
    
    /**
     This function is called when user clicks on archive all button
     */
    @IBAction func btnArchiveAllAction(_ sender: UISegmentedControl) {
        //archive all the notifications
        callWebServiceForArchiveAll()
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotifications {
            //return notification listing
            return arrUnreadNotifications.count
        }
        
        //return archived notifications
        return arrReadNotifications.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isNotifications {
            //case when notifications tab is selected
            let cell : NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
            
            cell.selectionStyle = .none
            
            //set title for notification
            cell.lblTitle.text = arrUnreadNotifications[indexPath.row].strTitle
            
            //set description for notification
            cell.lblDesc.text = arrUnreadNotifications[indexPath.row].strDescription
            
            //set date for notification
            cell.lblDate.text = arrUnreadNotifications[indexPath.row].strDate
            
            //set image for notification according to its type
            if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.thumbdUp {
                cell.imgType.image = #imageLiteral(resourceName: "thumbs-up")
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.chat {
                cell.imgType.image = #imageLiteral(resourceName: "chat")
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.kudos {
                cell.lblDesc.text = arrUnreadNotifications[indexPath.row].strUsername
                cell.imgType.image = #imageLiteral(resourceName: "cup")
            }
            else {
                cell.imgType.image = #imageLiteral(resourceName: "pending")
            }
            
            return cell
        }
        else {
            //case when archived tab is selected
            let cell : NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
            
            //return if archived array count is zero
            if (arrReadNotifications.count == 0) && (indexPath.row > (arrReadNotifications.count - 1)){
                return cell
            }
            
            cell.selectionStyle = .none
            
            //set title for archived notification
            cell.lblTitle.text = arrReadNotifications[indexPath.row].strTitle
            
            //set description for archived notification
            cell.lblDesc.text = arrReadNotifications[indexPath.row].strDescription
            
            //set date for archived notification
            cell.lblDate.text = arrReadNotifications[indexPath.row].strDate
            
            //set image for archived notification according to its type
            if arrReadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.thumbdUp {
                cell.imgType.image = #imageLiteral(resourceName: "thumbs-up")
            }
            else if arrReadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.chat {
                cell.imgType.image = #imageLiteral(resourceName: "chat")
            }
            else if arrReadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.kudos {
                cell.imgType.image = #imageLiteral(resourceName: "cup")
                cell.lblDesc.text = arrReadNotifications[indexPath.row].strUsername
                
            }
            
            return cell
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /**
     * Tableview delegate method used to return estimated  height of row
     */
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    /**
     * Tableview delegate method used when user selects a row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isNotifications {
            //case of notification
            
            if  arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.bubbleRatings{
                //bubble rating pending checklist is clicked
                self.btnBackAction(self)
            }
            else  if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.tribeValue {
                //dot value pending checklist is clicked
                let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                objVC.strOrgID = AuthModel.sharedInstance.orgId
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.functionalLens {
                //personality type pending checklist is clicked

                //Old code
//                let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
//                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
//                    //if from todo list(that means it has come for update) set it as updated case
//                    objVC.comingFromReduReviewBtn = true
//                }
//                self.navigationController?.pushViewController(objVC, animated: true)

                
                //New code
                //create instance of a class PersonalityTypeClickView(it displays the questions for personality type)
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.isPersonalityTypeAnsDone = true
                }
                
                //push to PersonalityTypeClickView view controller
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.teamRoleAnswers {
                //team role pending checklist is clicked
                let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                objVC.isQuestionDestributed = true
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.isEditMode = true
                }
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.cultureStructure {
                //culture structure pending checklist is clicked
                let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.strUpdate = "TRUE"
                }
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.motivation {
                //motivation pending checklist is clicked
                let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.comingFromReduReviewBtn = true
                }
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.tribeometer {
                //tribeometer pending checklist is clicked
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.isTribeometerAnsDone = true
                }
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrUnreadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.diagnostic {
                //diagnostics pending checklist is clicked
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
                if arrUnreadNotifications[indexPath.row].strTitle.contains("To Do List") {
                    //if from todo list(that means it has come for update) set it as updated case
                    objVC.isDiagnosticAnsDone = true
                }
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else {
                //If clicked to non pending notification, then mark it as read
                callWebServiceForMarkReadNotifications(index: indexPath.row)
            }
        }
        else {
            //case of archived notification
            
            if arrReadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.thumbdUp {
                //Thumbs up notification is clicked
                
                let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "DOTReportsView") as! DOTReportsView
                //                objVC.showAutomatic = true
                //pass date
                objVC.selectedNotificationDate = arrReadNotifications[indexPath.row].strDate
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if arrReadNotifications[indexPath.row].strType == NetworkConstant.NotificationType.kudos{
                //Kudos champ notification is clicked
                
                //get KudosChampPopUp instance
                let objVC1 = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
                //pass if multiple kudos champ or single
                objVC1.isMultiple = true //self.arrReadNotifications[indexPath.row].isMultiple
                
                //pass username of kudos champ
                objVC1.strUsername = self.arrReadNotifications[indexPath.row].strUsername
                
                //pass email of kudos champ
                objVC1.strEmail = self.arrReadNotifications[indexPath.row].strUserEmail
                
                //pass description of kudos champ
                objVC1.strKudos = self.arrReadNotifications[indexPath.row].strDescription
                
                //pass image of kudos champ
                objVC1.strImgUser = self.arrReadNotifications[indexPath.row].strUserImage
                
                objVC1.modalPresentationStyle = .overCurrentContext
                self.navigationController?.present(objVC1, animated: false, completion: nil)
            }
            else {
                //chat notification is clicked
                
                //get ChatView instance
                let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
                //pass message model
                let obj = MessageModel()
                obj.strID = arrReadNotifications[indexPath.row].strFeedbackId
                objVC.objInboxModel = obj
                objVC.strSelectedHistoryChangeitID = arrReadNotifications[indexPath.row].strFeedbackId
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    /**
     table view delegate called when list's last row is reached
     */
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !isNotifications {
            //case of archived notification view
            if self.currentPage == self.totalPageCount {
                //case when no more items are there to load
                self.isLoadMore = false
            }
            
            //get last element index
            let lastElement = arrReadNotifications.count - 1
            
            if indexPath.row == lastElement && isLoadMore {
                // handle your logic here to get more items, add it to dataSource and reload tableview
                
                self.currentPage = self.currentPage + 1
                callWebServiceForReadNotifications()
            }
        }
    }
    
    //MARK: - UISearchBar Delegates
    /**
     Serach bar delegate called when cancel is clicked
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     Serach bar delegate called when search is clicked
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     Serach bar delegate called when user writes anything in search bar
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrReadNotifications = searchText.isEmpty ? arrReadNotifications : arrReadNotifications.filter { (item: NotificationModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strTitle.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblView.reloadData()
    }
    
    //MARK: - WebService Methods
    /**
     API - To get all the notifications
     */
    func callWebServiceForUnReadNotifications(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.getBubbleRatingUnReadNotificationList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //Parse notification list from result json
                    DashboardParser.parseUnReadNotificationList(response: response!, completionHandler: { (arrUnRead, count) in
                        
                        //assign parsed array to arrUnreadNotifications
                        self.arrUnreadNotifications = arrUnRead
                        
                        //load table
                        self.tblView.reloadData()
                        print("arrReadNotifications",self.arrUnreadNotifications.count)
                        
                        if count != "0" && count != ""{
                            //show archive button if there are notifications other than pending ones
                            self.heightArchiveConstant.constant = 50
                        }
                        else {
                            //hide archive button if there are no notifications other than pending ones
                            self.heightArchiveConstant.constant = 0
                        }
                    })
                }
                else {
                    //show error message in case of no response
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - load all archived notifications list
     */
    func callWebServiceForReadNotifications(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id,
             "page":currentPage] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.getBubbleRatingNotificationList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //Parse notification list from result json
                    DashboardParser.parseReadNotificationList(response: response!, completionHandler: { (arrRead, totalPages) in
                        
                        if self.currentPage == 1 {
                            //if first time then set array
                            self.arrReadNotifications = arrRead
                        }
                        else {
                            //if next pages are loaded append the arrays to previous
                            self.arrReadNotifications.append(contentsOf: arrRead)
                        }
                        
                        //save total page count
                        self.totalPageCount = Int(totalPages)!
                        //                        self.arrFilteredNotifications = arr
                        
                        self.tblView.reloadData()
                        
                        print("arrReadNotifications",self.arrReadNotifications.count)
                    })
                }
                else {
                    //show error message in case of no response
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - mark notifications as red
     */
    func callWebServiceForMarkReadNotifications(index: Int) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["notificationId": arrUnreadNotifications[index].strID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.changeNotificationStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    if self.arrUnreadNotifications[index].strType == NetworkConstant.NotificationType.thumbdUp {
                        //if thubs up notification case, redirect it to reports view
                        let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "DOTReportsView") as! DOTReportsView
                        //                        objVC.showAutomatic = true
                        objVC.selectedNotificationDate = self.arrUnreadNotifications[index].strDate
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else if self.arrUnreadNotifications[index].strType == NetworkConstant.NotificationType.kudos {
                        //if kudos champ notification case, redirect it to KudosChampPopUp
                        
                        let objVC1 = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "KudosChampPopUp") as! KudosChampPopUp
                        objVC1.isMultiple = self.arrUnreadNotifications[index].isMultiple
                        objVC1.strUsername = self.arrUnreadNotifications[index].strUsername
                        objVC1.strEmail = self.arrUnreadNotifications[index].strUserEmail
                        objVC1.strKudos = self.arrUnreadNotifications[index].strDescription
                        objVC1.strImgUser = self.arrUnreadNotifications[index].strUserImage
                        objVC1.modalPresentationStyle = .overCurrentContext
                        self.navigationController?.present(objVC1, animated: false, completion: nil)
                        
                    }
                    else {
                        //if chat notification case, redirect it to ChatView
                        
                        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
                        let obj = MessageModel()
                        obj.strID = self.arrUnreadNotifications[index].strFeedbackId
                        objVC.objInboxModel = obj
                        objVC.strSelectedHistoryChangeitID = self.arrUnreadNotifications[index].strFeedbackId
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    
                }
                else {
                    //show error message in case of no response
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     API - archive all notifications
     */
    func callWebServiceForArchiveAll(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.readAllNotification, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                //load all notifications left(they are the checklist ones now)
                self.callWebServiceForUnReadNotifications()
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
