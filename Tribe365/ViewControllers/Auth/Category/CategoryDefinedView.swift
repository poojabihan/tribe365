//
//  CategoryDefinedView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 31/05/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import UserNotifications
import SwiftyJSON
import Alamofire

/**
 This class is used to in super admin case
 This is used to display the dashboard for an organisation (DOT + COT + iOT + Actions buttons )
 This is almost a copy of CategoryDefinedViewForUser controller, so please the explanation and comments there
 */
class CategoryDefinedView: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextViewDelegate, UISearchBarDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //MARK: - IBoutlets
    
    @IBOutlet var tblView: UITableView! // Main table
    @IBOutlet var tblValues: UITableView! // DOT Values table
    @IBOutlet var tblOptions: UITableView! // Department & Users table
    @IBOutlet weak var iotFeedbackView: UIView!// IOT Feedback view
    @IBOutlet weak var tblHeaderView: UIView!
    @IBOutlet weak var feedbackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var valuesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtIOTFeedback: UITextView!
    @IBOutlet weak var btnNotification: UIButton!

    //Add Dept & Add User view IBOutlet
    @IBOutlet weak var lblAddTeam: UILabel! // To show selected depts
    @IBOutlet weak var lblAddIndividual: UILabel! // To show selected Users
    @IBOutlet weak var btnDoneThumbsView: UIButton!
    @IBOutlet var thumsUpSelectionView: UIView!// View with buttons add team/individual
    
    //Add Dept & Add User selection view IBOutlet
    @IBOutlet weak var addOptionView: UIView!// Blur view which open on AddTeam/AddIndividual click
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblHeadingOptionView: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var txtCompanyName: UILabel!
    @IBOutlet weak var imgOrgLogo: UIImageView!
    @IBOutlet weak var imgViewOftribeNB: UIImageView!
    @IBOutlet weak var viewOfNB: UIView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnChecklistCount: UIButton!
    @IBOutlet weak var iotView: UIView!
    @IBOutlet weak var reportsView: UIView!
    @IBOutlet weak var company360View: UIView!
    @IBOutlet weak var btnActionCount: UIButton!
    
    @IBOutlet weak var sotViewUser: UIView!
    @IBOutlet weak var sotViewAdmin: UIView!
    @IBOutlet weak var companyViewUser: UIView!
    @IBOutlet weak var companyViewAdmin: UIView!
    @IBOutlet weak var imgViewIOT: UIImageView!
    @IBOutlet weak var lblUnredCount: UILabel!

    // MARK: - Variables
    var imagePicker = UIImagePickerController()
    let panel = JKNotificationPanel()
    var strOrgID = ""
    var arrOfficeDetail = [OfficeModel]()
    var strCompanyName = ""
    var urlForOrgLogo = ""
    var isDOTAdded = 0
    var arrayForCategoryTitle = ["Directing", "Connecting", "Supercharging", "Improving", "REPORTS"]
    var arrayForCategorySubTitle = ["Directing Our Tribe","Connecting Our Tribe","Supercharging Our Tribe", "Improving Our Tribe", ""]
    var  auth = AuthModel.sharedInstance
    var arrChecklist = [ChecklistModel]()
    var arrTodos = [ChecklistModel]()
    var arrSectionTitles = [String]()
    var arrValues = [ValueModel]()
    var arrOfGetDeparmentList = [DepartmentModel]()
    var arrOfGetUserList = [UserModel]()
    
    var arrFilteredDeparmentList = [DepartmentModel]()
    var arrFilteredUserList = [UserModel]()
    
    var arrSelectedDept = [DepartmentModel]()
    var arrSelectedUsers = [UserModel]()
    
    var isDept = true// To detect whether AddTeam clicked or AddIndividual clicked
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        imagePicker.delegate = self
        
        tblView.reloadData()
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            btnNotification.isHidden = true
            lblUnredCount.isHidden = true
        }
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(CategoryDefinedView.MoveToIOT(_:)), name: NSNotification.Name(rawValue: "GoToViewOfPush"), object: nil)
        
        let userUpdateNotification = Notification.Name("GoToViewOfPushChat")
        NotificationCenter.default.addObserver(self, selector: #selector(moveToChat(not:)), name: userUpdateNotification, object: nil)
        
        imgOrgLogo.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        imgOrgLogo.layer.borderWidth = 1
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        if auth.role == kUser{
            
            txtCompanyName.text = strCompanyName
            
            let url = URL(string: auth.organisation_logo)
            
            imgOrgLogo.kf.setImage(with: url)

            txtCompanyName.text = auth.office
            
            btnBack.isHidden = true

            viewOfNB.frame.size.width = 135
            
//            imgViewOftribeNB.frame.origin.x = viewOfNB.frame.origin.x - 16
            
            btnProfile.isHidden = false
            
        }
            
        else{
            
            arrayForCategoryTitle = ["Directing", "Connecting", "Supercharging" /*, "REPORTS"*/]
            arrayForCategorySubTitle = ["Directing Our Tribe","Connecting Our Tribe","Supercharging Our Tribe"/*, ""*/]
            
            txtCompanyName.text = strCompanyName
            
            let url = URL(string: urlForOrgLogo)
            
            imgOrgLogo.kf.setImage(with: url)

            btnBack.isHidden = false

        }
        
        txtIOTFeedback.text = "Type your feedback message here.."
        txtIOTFeedback.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        txtIOTFeedback.textColor = UIColor.lightGray
        txtIOTFeedback.layer.borderWidth = 1
        txtIOTFeedback.font = UIFont(name: "verdana", size: 13.0)
        txtIOTFeedback.returnKeyType = .done
        txtIOTFeedback.delegate = self
        
        self.valuesHeightConstraint.constant = 0
        self.feedbackHeightConstraint.constant = 0
        self.tblHeaderView.frame.size.height = 696
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        
        NotificationCenter.default.addObserver(self, selector: #selector(callWebServiceToGetUnreadCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)

        if AuthModel.sharedInstance.role == kUser{
            //for user
        }
        else{
            iotView.isHidden = true
            reportsView.isHidden = true
            sotViewUser.isHidden = true
            sotViewAdmin.isHidden = false
            companyViewUser.isHidden = true
            companyViewAdmin.isHidden = false
        }
        
        callWebServiceToGetUnreadCount()
        callWebServiceForChecklist()
        callWebServiceForActions()
        
//        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
//        
//        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
//        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
//        searchBar.layer.cornerRadius = 18
//        searchTextField.borderStyle = .none
//        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
//        searchTextField.textAlignment = NSTextAlignment.left
//        let image:UIImage = UIImage(named: "search")!
//        let imageView:UIImageView = UIImageView.init(image: image)
//        searchTextField.leftView = nil
//        searchTextField.placeholder = " Search"
//        searchTextField.leftView = imageView
//        searchTextField.rightViewMode = UITextFieldViewMode.always
        
    }
    
    // MARK: - Custom Methods
    @objc func MoveToIOT(_ notification:Notification) {
        
        if AuthModel.sharedInstance.role == kUser{
            
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
            
            if self.navigationController?.viewControllers.last is IOTFeedBackView {
                objVC.getQuessionarieMethod()
                return
                
            }
            
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    @objc func moveToChat(not: Notification) {
        
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let changeit_id = userInfo["changeit_id"] as? String {
                print(changeit_id)
                if AuthModel.sharedInstance.role == kUser{
                    let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
                    objVC.strSelectedHistoryChangeitID = changeit_id
                    
                    if self.navigationController?.viewControllers.last is ChatView {
                        var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
                        navigationArray!.remove(at: (navigationArray?.count)! - 1) // To remove previous UIViewController
                        self.navigationController?.viewControllers = navigationArray!
                        
                        //objVC.viewDidLoad()
                        //objVC.viewWillAppear(true)
                        //return
                        
                    }
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
    }
    
    //When user selects any department this function is called
    @objc func btnDeptAction(sender: UIButton) {
        arrFilteredDeparmentList[sender.tag].isSelected = !arrFilteredDeparmentList[sender.tag].isSelected
        tblOptions.reloadData()
        
        if showOptionSelectionTickView() {
            btnContinue.isHidden = false
        }
        else {
            btnContinue.isHidden = true
        }
    }
    
    //When user selects any user this function is called
    @objc func btnIndiAction(sender: UIButton) {
        arrFilteredUserList[sender.tag].isSelected = !arrFilteredUserList[sender.tag].isSelected
        tblOptions.reloadData()
        
        if showOptionSelectionTickView() {
            btnContinue.isHidden = false
        }
        else {
            btnContinue.isHidden = true
        }
    }
    
    //When user selects any value this function is called
    @objc func btnValueAction(sender: UIButton) {
        arrValues[sender.tag].isSelected = !arrValues[sender.tag].isSelected
        tblValues.reloadData()
        
        if showThumbsUpSelectionView() {
            thumsUpSelectionView.isHidden = false
            iotFeedbackView.isHidden = true
        }
        else {
            thumsUpSelectionView.isHidden = true
            iotFeedbackView.isHidden = false
        }
    }
    
    //This function checks if user has selected any value for bubble rating and accordingly returns whether to show rating/Thumbs view or not
    func showThumbsUpSelectionView() -> Bool {
        
        for value in arrValues {
            if value.isSelected {
                return true
            }
        }
        
        return false
    }
    
    //This function checks if user has selected any dept(if there is dept listing) or user(if there is user listing) and accordingly returns whether to show continue button or not
    func showOptionSelectionTickView() -> Bool {
        
        if isDept {
            for value in arrFilteredDeparmentList {
                if value.isSelected {
                    return true
                }
            }
            
            return false
        }
        else {
            for value in arrFilteredUserList {
                if value.isSelected {
                    return true
                }
            }
            
            return false
        }
    }
    
    //This function sets the selected dept & users in array for web service call and also sets the final selection result on the labels
    func setSelectedOptions() {
        
        if isDept {
            arrSelectedDept.removeAll()
            for value in arrOfGetDeparmentList {
                if value.isSelected {
                    arrSelectedDept.append(value)
                }
            }
            
            if arrSelectedDept.count == 0 {
                lblAddTeam.text = "Add Teams"
                btnDoneThumbsView.isHidden = true
            }
            else if arrSelectedDept.count == 1{
                lblAddTeam.text = arrSelectedDept[0].strDepartment
                btnDoneThumbsView.isHidden = false
            }
            else {
                lblAddTeam.text = String(arrSelectedDept.count) + " selected"
                btnDoneThumbsView.isHidden = false
            }
        }
        else {
            arrSelectedUsers.removeAll()
            for value in arrOfGetUserList {
                if value.isSelected {
                    arrSelectedUsers.append(value)
                }
            }
            if arrSelectedUsers.count == 0 {
                lblAddIndividual.text = "Add Individuals"
                btnDoneThumbsView.isHidden = true
            }
            else if arrSelectedUsers.count == 1{
                lblAddIndividual.text = arrSelectedUsers[0].strName
                btnDoneThumbsView.isHidden = false
            }
            else {
                lblAddIndividual.text = String(arrSelectedUsers.count) + " selected"
                btnDoneThumbsView.isHidden = false
            }
        }
    }
    
    //This function resets all the values of thumbs up section after successfull submission to server
    func resetSelectedValues() {
        for value in arrValues {
            value.isSelected = false
        }
        
        for value in arrFilteredDeparmentList {
            value.isSelected = false
        }
        
        for value in arrFilteredUserList {
            value.isSelected = false
        }
        
        arrSelectedUsers.removeAll()
        arrSelectedDept.removeAll()
        
        lblAddTeam.text = "Add Teams"
        lblAddIndividual.text = "Add Individuals"
        btnDoneThumbsView.isHidden = true
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // convert images into base64 and keep them into string
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgViewIOT.image!, 1.0)!
        if let imageData = imgViewIOT.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    // MARK: - IBActions
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        if auth.role == kUser{
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            objVC.showMenu = false
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else{
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "SuperAdminProfileView") as! SuperAdminProfileView
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ViewActionsViewController") as! ViewActionsViewController
        objVC.strOrgId = strOrgID
        objVC.arrOfficeDetail = arrOfficeDetail
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnmessageAction(_ sender: Any) {
        //Message Click
        self.panel.showNotify(withStatus: .warning , inView: self.appDelegate.window!, title: "Coming soon")
    }
    
    @IBAction func btnDiagnosticAction(_ sender: Any) {
        //Message Click
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticView") as! DiagnosticView
        
        if AuthModel.sharedInstance.role == kUser{//User
            objVC.strOrgId = AuthModel.sharedInstance.orgId
            objVC.strCompanyName = AuthModel.sharedInstance.office
        }
        else{
            objVC.strOrgId = strOrgID
            objVC.strCompanyName = strCompanyName
        }
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnSupportAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.tribe
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnChecklistAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ChecklistViewController") as!  ChecklistViewController
        objVC.arrChecklist = arrChecklist
        objVC.arrTodos = arrTodos
        objVC.arrSectionTitles = arrSectionTitles
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func CategoryActions(sender: UIButton) {
        
        if(sender.tag == 10){
            let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "COTView") as! COTView
            objVC.strOrgID = self.strOrgID == "" ? auth.orgId : self.strOrgID
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else {
            //Navigate to new view
            if auth.role == kUser{//user
                if(sender.tag == 100){
                    
                    callWebServiceDOTDetail()
                    
                }
                else if(sender.tag == 20){
                    //SOT
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTView") as! SOTView
                    objVC.strOrgID = AuthModel.sharedInstance.orgId
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                else if(sender.tag == 30){
                    
                    callWebServiceToGetUserDetail()
                    
                }
                else if(sender.tag == 40){
                    
                    
                    //Change 28/02
                    
                    let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "AdminReportView") as! AdminReportView
                    
                    objVC.strOrgId = AuthModel.sharedInstance.orgId
                    objVC.strOrgName = AuthModel.sharedInstance.office
                    
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                    
                }
            }
            else{//super admin
                if(sender.tag == 100){
                    
                    if isDOTAdded == 0 {
                        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "AddBeliefsView") as! AddBeliefsView
                        objVC.strOrgID = strOrgID
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else{
                        
                        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                        objVC.strOrgID = strOrgID
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    
                }
                    //SOT Click
                else if(sender.tag == 20){
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTView") as! SOTView
                    objVC.strOrgID = strOrgID
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                //IOT Ckick
                /* else if(indexPath.row == 3){
                 
                 let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "SuperAdminReportsView") as! SuperAdminReportsView
                 objVC.strOrgID = strOrgID
                 self.navigationController?.pushViewController(objVC, animated: true)
                 
                 
                 }*/
                
            }
        }
    }
    
    @IBAction func btnAddTeamAction(sender: UIButton) {
        isDept = true
        lblHeadingOptionView.text = "Select Teams"
        tblOptions.reloadData()
        addOptionView.isHidden = false
    }
    
    @IBAction func btnAddIndividualAction(sender: UIButton) {
        isDept = false
        lblHeadingOptionView.text = "Select Individuals"
        tblOptions.reloadData()
        addOptionView.isHidden = false
    }
    
    @IBAction func btnContinueAction(sender: UIButton) {
        
        setSelectedOptions()
        searchBar.text = ""
        arrFilteredUserList = arrOfGetUserList
        arrFilteredDeparmentList = arrOfGetDeparmentList
        addOptionView.isHidden = true
        
    }
    
    @IBAction func btnDoneAction(sender: UIButton) {
        
        callWebServiceToAddThumbsUp()
    }
    
    @IBAction func btnSendFeedbackAction(sender: UIButton) {
        
        if (txtIOTFeedback.text?.isEmpty)! || txtIOTFeedback.text == "Type your feedback message here.." {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter feedback.")
        }
        else {
            var param = [String : AnyObject]()
            
            if imgViewIOT.image == nil
            {//no image added[[FIRInstanceID instanceID]token]
                param =  [
                    "message"      : txtIOTFeedback.text!,
                    "userId"     : AuthModel.sharedInstance.user_id,
                    "orgId" : AuthModel.sharedInstance.orgId,
                    "image"       : ""] as [String : AnyObject]
                
            }
            else{ //send image
                
                
                param =  [
                    "message"      : txtIOTFeedback.text!,
                    "userId"     : AuthModel.sharedInstance.user_id,
                    "orgId" : AuthModel.sharedInstance.orgId,
                    "image"       : convertImageToBase64(image: imgViewIOT.image!)] as [String : AnyObject]
            }
            
            callWebServiceToSendFeedback(param: param)
        }
    }
    
    @IBAction func btnIOTCameraAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnRefreshValuesAction(_ sender: Any) {
        resetSelectedValues()
        callWebServiceDOTValues()
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imgViewIOT.image = image
            self.dismiss(animated: false, completion: nil)
        }
        else{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type your feedback message here.." {
            textView.text = ""
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.text = "Type your feedback message here.."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblValues {
            return arrValues.count / 2 + arrValues.count % 2
        }
        else {//if tableView == tblOptions
            if isDept {
                return arrFilteredDeparmentList.count
            }
            else {
                return arrFilteredUserList.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblValues {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCell") as! ValuesTableViewCell
            //To remove seprator from particular cell
            tableView.separatorStyle = .none
            cell.selectionStyle = .none
            
            //value1
            let index1 = indexPath.row * 2
            cell.lblValue1.text = arrValues[index1].strName
            cell.btnValue1.tag = index1
            cell.btnValue1.addTarget(self, action: #selector(btnValueAction(sender:)), for: .touchUpInside)
            if arrValues[index1].isSelected {
                cell.btnValue1.backgroundColor = ColorCodeConstant.themeRedColor
                cell.lblValue1.textColor = UIColor.white
            }
            else {
                cell.btnValue1.backgroundColor = UIColor.clear
                cell.lblValue1.textColor = ColorCodeConstant.darkTextcolor
            }
            
            //value2
            let index2 = index1 + 1
            if (index2) < arrValues.count {
                cell.lblValue2.text = arrValues[index2].strName
                cell.btnValue2.tag = index2
                cell.btnValue2.addTarget(self, action: #selector(btnValueAction(sender:)), for: .touchUpInside)
                if arrValues[index2].isSelected {
                    cell.btnValue2.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue2.textColor = UIColor.white
                }
                else {
                    cell.btnValue2.backgroundColor = UIColor.clear
                    cell.lblValue2.textColor = ColorCodeConstant.darkTextcolor
                }
                
                cell.viewValue2.isHidden = false
                
            }
            else {
                cell.viewValue2.isHidden = true
            }
            
            return cell
        }
        else {//if tableView == tblOptions
            
            if isDept {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCell") as! ValuesTableViewCell
                //To remove seprator from particular cell
                tableView.separatorStyle = .none
                cell.selectionStyle = .none
                
                let index1 = indexPath.row
                cell.lblValue1.text = arrFilteredDeparmentList[index1].strDepartment
                cell.btnValue1.tag = index1
                cell.btnValue1.addTarget(self, action: #selector(btnDeptAction(sender:)), for: .touchUpInside)
                if arrFilteredDeparmentList[index1].isSelected {
                    cell.btnValue1.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue1.textColor = UIColor.white
                }
                else {
                    cell.btnValue1.backgroundColor = UIColor.clear
                    cell.lblValue1.textColor = ColorCodeConstant.darkTextcolor
                }
                
                return cell
                
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCellIndi") as! ValuesTableViewCell
                //To remove seprator from particular cell
                tableView.separatorStyle = .none
                cell.selectionStyle = .none
                
                let index1 = indexPath.row
                cell.lblValue2.text = arrFilteredUserList[index1].strName
                cell.btnValue2.tag = index1
                cell.btnValue2.addTarget(self, action: #selector(btnIndiAction(sender:)), for: .touchUpInside)
                if arrFilteredUserList[index1].isSelected {
                    cell.btnValue2.backgroundColor = ColorCodeConstant.themeRedColor
                    cell.lblValue2.textColor = UIColor.white
                }
                else {
                    cell.btnValue2.backgroundColor = UIColor.clear
                    cell.lblValue2.textColor = ColorCodeConstant.darkTextcolor
                }
                
                return cell
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    //MARK: - UISearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        if isDept {
            arrFilteredDeparmentList = searchText.isEmpty ? arrOfGetDeparmentList : arrOfGetDeparmentList.filter { (item: DepartmentModel) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.strDepartment.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        else {
            arrFilteredUserList = searchText.isEmpty ? arrOfGetUserList : arrOfGetUserList.filter { (item: UserModel) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        tblOptions.reloadData()
    }
    
    //MARK: - Calling Web service
    //API is called just to check the if the logged in user is authorised or not
    @objc func callWebServiceToGetUnreadCount() {
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleUnReadNotifications, param: param, withHeader: true ) { (response, errorMsg) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["data"]["notificationCount"].stringValue == "0" {
                    self.lblUnredCount.layer.borderWidth = 0.0
                    self.lblUnredCount.text = " "
                }
                else {
                    self.lblUnredCount.layer.borderWidth = 1.0
                    self.lblUnredCount.text = response!["data"]["notificationCount"].stringValue
                }
            }
        }
    }
    
    func callWebServiceToSendFeedback(param: [String : Any]) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.postFeedback, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("Succesfully uploaded")
                self.txtIOTFeedback.text = ""
                self.imgViewIOT.image = nil
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Feedback sent successfully.")
                self.view.endEditing(true)
            }
        }
    }
    
    func callWebServiceToGetUserDetail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "IOTFeedBackView") as! IOTFeedBackView
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func callWebServiceForChecklist() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.checklist, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                DashboardParser.parseChecklist(response: response!, completionHandler: { (checkList, todoList, showTodo, pendingCount) in
                    self.arrChecklist = checkList
                    self.arrTodos = todoList
                    
                    if showTodo {
                        self.arrSectionTitles = ["First Use Checklist", "To Do List"]
                        //show all sections
                        self.valuesHeightConstraint.constant = 300
                        self.feedbackHeightConstraint.constant = 220
                        self.tblHeaderView.frame.size.height = 1216
                        
                        self.callWebServiceDOTValues()
                        self.callWebServiceToGetDepartmentAnduserList()

                    }
                    else {
                        self.arrSectionTitles = ["First Use Checklist"]
                        //show only menu options
                        self.valuesHeightConstraint.constant = 0
                        self.feedbackHeightConstraint.constant = 0
                        self.tblHeaderView.frame.size.height = 696
                    }
                    
                    self.tblView.reloadData()
                    
                    if pendingCount == 0 {
                        self.btnChecklistCount.setImage(UIImage(named: "checkRed"), for: .normal)
                        self.btnChecklistCount.setTitle("", for: .normal)
                    }
                    else {
                        self.btnChecklistCount.setTitle(String(pendingCount) + "!", for: .normal)                        
                    }
                    
//                    let badgeCount: Int = pendingCount
//                    let defaults = UserDefaults.standard
//                    defaults.set(pendingCount, forKey: "pendingCountForChecklist")
//                    let application = UIApplication.shared
//                    let center = UNUserNotificationCenter.current()
//                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
//                        // Enable or disable features based on authorization.
//                    }
//                    application.registerForRemoteNotifications()
//                    application.applicationIconBadgeNumber = badgeCount
                })
            }
        }
    }
    
    func callWebServiceForActions() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id,
             "orgId": strOrgID == "" ? AuthModel.sharedInstance.orgId : strOrgID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getActionStatusCount, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                DashboardParser.parseActions(response: response!, completionHandler: { (started, notstarted, completed) in
                    
                    if started + notstarted == 0 {
                        self.btnActionCount.setImage(UIImage(named: "checkRed"), for: .normal)
                        self.btnActionCount.setTitle("", for: .normal)
                    }
                    else {
                        self.btnActionCount.setTitle(String(started + notstarted) + "!", for: .normal)
                    }
                    
                    
                })
            }
        }
    }
    
    func callWebServiceDOTDetail() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId":AuthModel.sharedInstance.orgId] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.DOTDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                    objVC.strOrgID = AuthModel.sharedInstance.orgId
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }
                else {                    
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No DOT Added for this organisation.")
                    
                }
            }
        }
    }
    
    func callWebServiceDOTValues() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId":AuthModel.sharedInstance.orgId] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.getDOTValuesList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    DashboardParser.parseDOTValues(response: response!, completionHandler: { (arr) in
                        
                        self.arrValues = arr
                        self.tblValues.reloadData()
                        
                    })
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No DOT Added for this organisation.")
                    
                }
            }
        }
    }
    
    func callWebServiceToGetDepartmentAnduserList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["orgId": AuthModel.sharedInstance.orgId] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getDepartmentAndUserList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseDepartmentUserList(response: response!,removeSelfUser: true, completionHandler: { (arrDep, arrUser) in
                        self.arrOfGetDeparmentList = arrDep
                        self.arrOfGetUserList = arrUser
                        
                        self.arrFilteredUserList = arrUser
                        self.arrFilteredDeparmentList = arrDep
                        
                        self.tblOptions.reloadData()
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToAddThumbsUp() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var arrUserIDs = [String]()
        for value in arrSelectedUsers {
            arrUserIDs.append(value.strId)
        }
        
        var arrDeptIDs = [String]()
        for value in arrSelectedDept {
            arrDeptIDs.append(value.strId)
        }
        
        var arrSelectedValues = [ValueModel]()
        for value in arrValues {
            if value.isSelected {
                arrSelectedValues.append(value)
            }
        }
        
        var arrOptions = [[String:Any]]()
        for value in arrSelectedValues {
            var dict = [String:Any]()
            dict["dotBeliefId"] = value.strBeliefId
            dict["dotValueId"] = value.strId
            dict["dotValueNameId"] = value.strValueId
            
            arrOptions.append(dict)
        }
        
        let param = ["toUserId": arrUserIDs,
                     "departmentIdArray": arrDeptIDs,
                     "dotId": arrValues[0].strDotId,
                     "options": arrOptions,
                     "bubbleFlag": "1"
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.addDOTBubbleRatingsToMultiDepartment, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Kudos sent successfully.")
                    
                    self.thumsUpSelectionView.isHidden = true
                    self.iotFeedbackView.isHidden = false
                    
                    //reset values for selected options
                    self.resetSelectedValues()
                    self.tblValues.reloadData()
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func requestWith(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    self.txtIOTFeedback.text = ""
                    self.imgViewIOT.image = nil
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Feedback sent successfully.")
                    self.view.endEditing(true)
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}
