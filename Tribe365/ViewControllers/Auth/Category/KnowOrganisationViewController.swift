//
//  KnowOrganisationViewController.swift
//  Tribe365
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import SwiftyJSON
import Charts

/**
 * This class is used to show the know organisation screen
 */
class KnowOrganisationViewController: UIViewController, HADropDownDelegate, UITableViewDataSource, UITableViewDelegate, BasicBarChart2Delegate, BasicBarChartPeronalityTypeDelegate, BasicBarChartHappyIndexDelegate, ChartViewDelegate {
    
    //MARK: - IBoutlets
    //Shows organisation image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //Shows unread notification count
    @IBOutlet weak var lblUnredCount: UILabel!
    
    //custom drop down for office
    @IBOutlet weak var dropDownOffice: HADropDown!
    
    //custom drop down for department
    @IBOutlet weak var dropDownDept: HADropDown!
    
    //table view
    @IBOutlet var tblView: UITableView!
    
    //header view for section Beliefs & values
    @IBOutlet var DOTView: UIView!
    
    //header view for section Team Role
    @IBOutlet var teamRoleView: UIView!
    
    //header view for section Personality Type
    @IBOutlet var personalityTypeView: UIView!
    
    //header view for section Culture Structure
    @IBOutlet var cultureStructureView: UIView!
    
    //header view for section Motivation
    @IBOutlet var motivationView: UIView!
    
    //header view for section Diagnostics
    @IBOutlet var diagnosticView: UIView!
    
    //header view for section Tribeometer
    @IBOutlet var tribeometerView: UIView!
    
    //header view for section Happy Index
    @IBOutlet var happyIndexView: UIView!
    
    //header view for section Culture Index
    @IBOutlet var cultureIndexView: UIView!
    
    //header view for section Engagement Index
    @IBOutlet var engagementIndexView: UIView!
    
    // MARK: - Variables
    //to show error/warning
    let panel = JKNotificationPanel()
    
    //stores office model list
    var arrOffices = [OfficeModel]()
    
    //stores dept model list office dependent
    var arrDepartment = [DepartmentModel]()
    
    //stores all global dept list
    var arrAllDepartment = [DepartmentModel]()
    
    //stores depts names for dropdown
    var arrForDepartmentName = [DepartmentModel]()
    
    //stores happy index year stats
    var arrHappyYearIndex = [HappyIndexModel]()
    
    //stores happy index montly stats
    var arrHappyMonthIndex = [HappyIndexModel]()
    
    //stores happy index week stats
    var arrHappyWeekIndex = [HappyIndexModel]()
    
    //stores happy index day stats
    var arrHappyDayIndex = [HappyIndexModel]()
    
    //stores months name for culture index
    var arrMonthsCultureIndex = [String]()
    
    //stores month name for engagement index
    var arrMonthsEngagement = [String]()
    
    //stores beliefs names
    var arrBeliefNames = [String]()
    
    //stores values names
    var arrValuesNames = [String]()
    
    //stores the happy index year selected from bar graph
    var selectedHappyIndexYear = ""
    
    //stores the happy index month selected from bar graph
    var selectedHappyIndexMonth = ""
    
    //stores the happy index week selected from bar graph
    var selectedHappyIndexWeek = ""
    
    //stores all global depts names list
    var arrOfAllDeparmentList = [String]()
    
    //stores all global depts ids list
    var arrOfAllDepartmentIdList = [String]()
    
    //stores selected office index from drop down
    var selectedIndexOfOffice = 0
    
    //stores selected dept index from drop down
    var selectedIndexOfDepartment = 0
    
    //stores office names
    var arrOfficesName = [String]()
    
    //stores dept names
    var arrDepartmentNames = [String]()
    
    //stores global depts names
    var arrAllDepartmentNames = [String]()
    
    //check if already loaded before for depts list
    var loadForFirst = false
    
    //stores beliefs model
    var arrBeliefs = [BeliefModel]()
    
    //stores calculated height of collection view for DOT values
    var calacualtedMaxHeightOfCollection = Int()
    
    //stores if diagnostics answer filled or not
    var isDiagnosticAnsDone = false
    
    //stores if tribeometer answer filled or not
    var isTribeometerAnsDone = false
    
    //stores happy index max count(but not used now)
    var happyIndexMaxValue = 0
    
    //checks if beliefs section is expanded or collapse mode
    var isBeliefsExpanded = false
    
    //checks if team role section is expanded or collapse mode
    var isTeamRoleExpanded = false
    
    //checks if personality type section is expanded or collapse mode
    var isPersonalityTypeExpanded = false
    
    //checks if culture structure section is expanded or collapse mode
    var isCultureStructureExpanded = false
    
    //checks if motivation section is expanded or collapse mode
    var isMotivationExpanded = false
    
    //checks if diagnostics section is expanded or collapse mode
    var isDiagnosticExpanded = false
    
    //checks if tribeometer section is expanded or collapse mode
    var isTribeometerExpanded = false
    
    //checks if diagnostics sub category is displayed or not
    var isDiagnosticsSubCategory = false
    
    //checks if culture index section is expanded or collapse mode
    var isCultureIndexExpanded = false
    
    //checks if engagement index section is expanded or collapse mode
    var isEngagementIndexExpanded = false
    
    //no longer in use
    var isBeliefClicked = false
    var selectedBelief = BeliefModel()
    
    //if true we show happy index section expanded by default
    var isRedirectToHappyIndex = false
    
    //checks if personality typ[e subcategory is displayed
    var isPersonalityTypeSubCategory = false
    
    //checks if happy index section is expanded or collapse mode
    var isHappyIndexExpanded = false
    //    var isHappyIndexMonthCategory = false
    //    var isHappyIndexWeekCategory = false
    //    var isHappyIndexDayCategory = false
    
    //stores the level of happy index bar graph (year/month/week)
    var selectedHappyIndexBarValue = 0
    
    //stores json for team role result
    var cotJson = JSON()
    
    //stores json for personality type result
    var personalityJson = JSON()
    
    //stores result of culture structure
    var arrOfSOTDetails = [SOTDetailModel]()
    
    //stores summary array for culture structure
    var arrOfSOTResultSummary = [SOTDetailResultModel]()
    
    //stores if culture strycture questions are filled or not
    var IsQuestionnaireAnswerFilled = Bool()
    var IsUserFilledAnswer = Bool()
    
    //stores the structured summary from the resultant summary(arrOfSOTResultSummary) of culture structure
    var arrForAllSummary = [ModelForAllSummary]()
    
    //stores result for motivation
    var arrOfMotivationData = [MotivationStructureGraphModel]()
    
    //stores Model of BarEntry for Motivation(third party for bar graph)
    var dataEntriesMotivation = [BarEntry]()
    
    //stores Model of BarEntry for Diagnostics(third party for bar graph)
    var dataEntriesDiagnostics = [BarEntry]()
    
    //stores Model of BarEntry for Diagnostics sub category(third party for bar graph)
    var dataEntriesSubCategoryDiagnostics = [BarEntry]()
    
    //stores Model of BarEntry for tribeometer(third party for bar graph)
    var dataEntriesTribeometer = [BarEntry]()
    
    //instance of line chart culture index
    let lineChartData = LineChartData()
    
    //instance of line chart for engagement
    let engagementLineChartData = LineChartData()
    
    //instance of line chart for beliefs(not in use)
    let beliefLineChartData = LineChartData()
    
    //stores Model of BarEntry for personality type sub category(third party for bar graph)
    var dataEntriesSubCategoryPersonalityType = [BarEntry]()
    
    //stores Model of BarEntry for personality type(third party for bar graph)
    var dataEntriesPersonalityType = [BarEntry]()
    
    //stores Model of BarEntry for happy index year(third party for bar graph)
    var dataEntriesHappyIndexYear = [BarEntry]()
    
    //stores Model of BarEntry for happy index month(third party for bar graph)
    var dataEntriesHappyIndexMonth = [BarEntry]()
    
    //stores Model of BarEntry for happy index week(third party for bar graph)
    var dataEntriesHappyIndexWeek = [BarEntry]()
    
    //stores Model of BarEntry for happy index day(third party for bar graph)
    var dataEntriesHappyIndexDay = [BarEntry]()
    
    //stores diagnostics result
    var arrOfDiagnosticResult = [DiagnosticResultModel]()
    
    //stores tribeometer result
    var arrOfTribeometerResult = [DiagnosticResultModel]()
    
    //stores diagnostics sub category result
    var arrOfSubCategoryDiagnosticResult = [DiagnosticResultModel]()
    
    //stores culture index result
    var arrOfCultureIndexResult = [DiagnosticResultModel]()
    
    //stores engagement result
    var arrOfEngagementIndexResult = [DiagnosticResultModel]()
    
    //stores personality type result
    var arrOfPersonalityTypeResult = [DiagnosticResultModel]()
    
    //stores persoality type sub category result
    var arrOfSubCategoryPersonalityTypeResult = [DiagnosticResultModel]()
    
    //stores the staus of personality type, its questions are completed or not
    var isPersonailtyTypeAnswered = false

    //enum for sections
    enum Reports {
        case CultureIndex
        case EngagementIndex
        case Beliefs
        case TeamRoles
        case PersonalityType
        case CultureStructure
        case Motivation
        case Diagnostics
        case Tribeometer
        case HappyIndex
    }
    
    //stores the enum array
    var arrReportsList = [Reports]()
    
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //stores the sections value to be displayed
        arrReportsList = [.CultureIndex, .EngagementIndex, .Beliefs, .TeamRoles, .PersonalityType, .CultureStructure, .Motivation, .Diagnostics, .Tribeometer, .HappyIndex]
        
        //set organisation image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //set drop down delegates
        dropDownDept.delegate = self
        dropDownOffice.delegate = self
        
        //set this to avoid extra cells at bottom of table
        tblView.tableFooterView = UIView()
        
        //load all depts & offices
        callWebServiceToGetAllDepartments()
        callWebServiceToGetAllOfficesAndDepartmentList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        //set observer for updating notification count
        NotificationCenter.default.addObserver(self, selector: #selector(callWebServiceToGetUnreadCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        //get organisation report
        callWebServiceToGetOrgReport()
        //        callWebServiceTogetDiagnosticsTribeometerAnswerDone()
        
        //get unread notification count
        callWebServiceToGetUnreadCount()
        
        //set line graph for culture index
        showLineGraph()
        
        //set as selected happy index value to 0(years data displayed by default)
        selectedHappyIndexBarValue = 0
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    /**
     * set up data for drop downs
     */
    func setData() {
        //set "All" type in all dropdowns
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
            dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    /**
     * Sets "All" value in dept array and dept id array
     */
    func CreateDropDownListOfAllDepartemntArray(){
        
        //loop through dept name
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                //insert All in first position
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                //set other next to ALL
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
        // self.dropDownDept.items = self.arrDepartmentNames
    }
    
    /**
     * Calculate max height according to values, in beliefs sections
     */
    func CalculateMaxHeight(){
        //loop throught beliefs to get the values
        for i in (0..<self.arrBeliefs.count) {
            
            if i == 0 {
                //set count for values as height for collection
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
            }
            if calacualtedMaxHeightOfCollection < self.arrBeliefs[i].arrValues.count {
                //case when height is less than values count, that means we have another belief which has values more than the previous belief
                
                //so set its this values count as height
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                
                print(calacualtedMaxHeightOfCollection)
            }
            
        }
    }
    
    /**
     * Prepares the bullet point data for summary in culture structure section
     */
    func SetOrganisationCultureReultImage(){
        
        //loop through summary result
        if self.arrOfSOTResultSummary.count > 0 {
            
            //remove all data
            arrForAllSummary.removeAll()
            
            var strSummary = ""
            let model = ModelForAllSummary()
            
            //loop through summary at 1st index
            for i in (0..<self.arrOfSOTResultSummary[0].arrOfSummary.count){
                
                if strSummary == "" {
                    //when no summary(that means initial case) add a bullet point with summary text
                    strSummary = "  \u{2022}  " + self.arrOfSOTResultSummary[0].arrOfSummary[i].strSummary
                }
                else {
                    //append the new summary points to existing one
                    strSummary = strSummary + "\n\n  \u{2022}  " +  self.arrOfSOTResultSummary[0].arrOfSummary[i].strSummary
                }
                
                //append the final result to array
                self.arrForAllSummary.append(model)
                print(arrForAllSummary[0].strValue ?? "")
            }
            
            
            if self.arrOfSOTResultSummary[0].arrOfSummary.count > 0 {
                //case when there is data
                model.strValue = NSMutableAttributedString.init(string: strSummary)
                
                self.arrForAllSummary.append(model)
            }
            
            tblView.reloadData()
            
        }
        
    }
    
    /**
     * Prepares the summary array
     */
    func SetScoreForModule()  {
        
        //if no summary data empty the array
        if arrOfSOTResultSummary.count == 0{
            
            self.arrForAllSummary.removeAll()
            
        }
        else{
            //else prepare the summary bullet points
            self.SetOrganisationCultureReultImage()
            
        }
        
    }
    
    /**
     * This function generates data enteries for motivation bar graph
     */
    func generateDataEntriesMotivation() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for motivation array
        for j in (0..<self.arrOfMotivationData.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfMotivationData[j].strScore)! / 60.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\(arrOfMotivationData[j].strScore /*valueArr[j]*/)", title: String(j + 1)/*titleArr[j]*/))
            
        }
        return result
    }
    
    /**
     * This function generates data enteries for happy index bar graph
     * @param arr: accepts happy index model
     */
    func generateDataEntriesHappyIndex(arr: [HappyIndexModel]) -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for passed array
        for j in (0..<arr.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arr[j].strHappyIndex)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arr[j].strHappyIndex /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     * This function generates data enteries for diagnostics bar graph
     */
    func generateDataEntriesDiagnostics() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for diagnostics array
        for j in (0..<self.arrOfDiagnosticResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfDiagnosticResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfDiagnosticResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     * This function generates data enteries for personality type bar graph
     */
    func generateDataEntriesPersonalitytype() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for personality type array
        for j in (0..<self.arrOfPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     * This function generates data enteries for diagnostics sub category bar graph
     */
    func generateDataEntriesSubCategoryDiagnostics() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for diagnostics sub category array
        for j in (0..<self.arrOfSubCategoryDiagnosticResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfSubCategoryDiagnosticResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfSubCategoryDiagnosticResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     * This function generates data enteries for personality type sub category bar graph
     */
    func generateDataEntriesSubCategoryPersonalityType() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for personality type sub category array
        for j in (0..<self.arrOfSubCategoryPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfSubCategoryPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfSubCategoryPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     * This function generates data enteries for tribeometer bar graph
     */
    func generateDataEntriesTribeometer() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for tribeometer array
        for j in (0..<self.arrOfTribeometerResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfTribeometerResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\(arrOfTribeometerResult[j].strPercentage /*valueArr[j]*/)", title: String( j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of motivation
     */
    func setDataInColoumns(){
        
        if arrOfMotivationData.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfMotivationData {
                //prepare array of scores
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfMotivationData[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfMotivationData.removeAll()
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of culture index
     */
    func setDataInColoumnsCultureIndex(){
        
        if arrOfCultureIndexResult.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfCultureIndexResult {
                //prepare array of scores
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfCultureIndexResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfCultureIndexResult.removeAll()
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of engagement index
     */
    func setDataInColoumnsEngagementIndex(){
        
        if arrOfEngagementIndexResult.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfEngagementIndexResult {
                //prepare array of scores
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfEngagementIndexResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfEngagementIndexResult.removeAll()
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Beliefs
     */
    func setDataInColoumnsBeliefs(){
        
        if arrBeliefs.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrBeliefs {
                //prepare array of scores
                arrScores.append(value.strRating)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrBeliefs[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrBeliefs.removeAll()
        }
    }
    
    /**
     Returns the first value which is not a balnk entry
     */
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        //if all are blank entries, then returns blank
        return ""
    }
    
    /***
     This function returns index(s) of a value from the array
     @strValue: This stores the vaue whose index needs to be found out
     @arrTemp: This is the temperory array which stores the value(strValue)
     @[String]: Returns the index(s) of the strValue
     */
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        return arrIndex
    }
    
    /**
     Returns the background colour according to there position
     */
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        case 6:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.4)
        case 7:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.3)
        case 8:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.2)
        case 9:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.15)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Happy Index
     */
    func setGraphDataHapyIndex(arr: [HappyIndexModel]) -> [HappyIndexModel]
    {
        var arrScores = [String]()
        
        for value in arr {
            //prepare array of scores
            arrScores.append(value.strHappyIndex)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                arr[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
        
        return arr
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Personality type
     */
    func setGraphDataPersonalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfPersonalityTypeResult {
            //prepare array of score
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Personality type
     */
    func setGraphData()
    {
        var arrScores = [String]()
        //prepare array of score
        for value in arrOfDiagnosticResult {
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfDiagnosticResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Diagnostics sub category
     */
    func setGraphDataSubCategory()
    {
        var arrScores = [String]()
        
        for value in arrOfSubCategoryDiagnosticResult {
            //prepare array of score
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfSubCategoryDiagnosticResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of personality type sub category
     */
    func setGraphDataSubCategoryPeronalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfSubCategoryPersonalityTypeResult {
            //prepare array of score
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfSubCategoryPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of tribeometer
     */
    func setDataInColoumnsTribeometer(){
        
        if arrOfTribeometerResult.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfTribeometerResult {
                //prepare array of score
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                    }
                }
                if strMaxValue == "" {
                    break
                }
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    tempValues[Int(value)!] = ""
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfTribeometerResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfTribeometerResult.removeAll()
        }
    }
    
    /**
     * This function is called when redo review button for tribeometer is clicked
     */
    @objc func btnRedoReviewTribeometerAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
        //pass this to detect update case or submit case
        objVC.isTribeometerAnsDone = isTribeometerAnsDone
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * This function is called when redo review button for diagnostics is clicked
     */
    @objc func btnRedoReviewDiagnosticsAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
        //pass this to detect update case or submit case
        objVC.isDiagnosticAnsDone = isDiagnosticAnsDone
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * This function is called when redo review button for culture structure is clicked
     */
    @objc func btnRedoReviewCulturestructureAction(_ sender: UIButton) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
        if self.IsUserFilledAnswer {
            //pass this to detect update case
            objVC.strUpdate = "TRUE"
        }
        
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * This function is no longer in use
     */
    @objc func btnBackDotAction(_ sender: UIButton) {
        self.isBeliefClicked = false
        self.showLineGraphBelief()
        self.tblView.reloadData()
    }
    
    /**
     * Prepares data for culture index line graph
     */
    func showLineGraph() {
        
        //array chart data(third party)
        var lineChartEntry = [ChartDataEntry]()
        
        var numArray = [Double]()
        
        for value in self.arrOfCultureIndexResult {
            //prepare score array
            numArray.append(Double(value.strScore == "" ? "0" : value.strScore)!)
        }
        
        //loop through score array
        for i in 0..<numArray.count {
            //prepare data for line chart enteries
            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
            lineChartEntry.append(value)
        }
        
        //set line appearance
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Index")
        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //        line1.drawValuesEnabled = false
        line1.circleColors = [ColorCodeConstant.themeRedColor]
        line1.drawVerticalHighlightIndicatorEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        
        //set data set
        lineChartData.dataSets = [line1]
        //lineChartData.addDataSet(line1)
    }
    
    /**
     * Prepares data for engagement index line graph
     */
    func showLineGraphEngagement() {
        
        //array chart data(third party)
        var lineChartEntry = [ChartDataEntry]()
        
        var numArray = [Double]()
        
        for value in self.arrOfEngagementIndexResult {
            //prepare score array
            numArray.append(Double(value.strScore == "" ? "0" : value.strScore)!)
        }
        
        //loop through score array
        for i in 0..<numArray.count {
            //prepare data for line chart enteries
            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
            lineChartEntry.append(value)
        }
        
        //set line appearance
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Engagement Index")
        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //        line1.drawValuesEnabled = false
        line1.circleColors = [ColorCodeConstant.themeRedColor]
        line1.drawVerticalHighlightIndicatorEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        
        //set data set
        engagementLineChartData.dataSets = [line1]
        //lineChartData.addDataSet(line1)
    }
    
    /**
     * This function is no longer in use
     */
    func showLineGraphBelief() {
        var lineChartEntry = [ChartDataEntry]()
        
        var numArray = [Double]()
        
        for value in self.arrBeliefs {
            numArray.append(Double(value.strRating == "" ? "0" : value.strRating)!)
        }
        
        for i in 0..<numArray.count {
            
            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Beliefs Average Rating")
        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //        line1.drawValuesEnabled = false
        line1.circleColors = [ColorCodeConstant.themeRedColor]
        line1.drawVerticalHighlightIndicatorEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        
        beliefLineChartData.dataSets = [line1]
        //lineChartData.addDataSet(line1)
    }
    
    /**
     * This function is no longer in use
     */
    func showLineGraphValues() {
        var lineChartEntry = [ChartDataEntry]()
        
        var numArray = [Double]()
        
        for value in selectedBelief.arrValues {
            numArray.append(Double(value.strRating == "" ? "0" : value.strRating)!)
        }
        
        for i in 0..<numArray.count {
            
            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(entries: lineChartEntry, label: selectedBelief.strName + "Values Rating")
        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //        line1.drawValuesEnabled = false
        line1.circleColors = [ColorCodeConstant.themeRedColor]
        line1.drawVerticalHighlightIndicatorEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        
        beliefLineChartData.dataSets = [line1]
        //lineChartData.addDataSet(line1)
    }
    
    // MARK: - IBActions
    /**
     * This function is called when we click side menu button.
     * It open ups the side menu
     */
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    /**
     * This function is called when we click profile button from nav.
     * It is no longer in use
     */
    @IBAction func btnProfileAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
        objVC.showMenu = false
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * This function is called when sections expand arrow is clicked 
     */
    @IBAction func btnExpand(_ sender: UIButton) {
        
        //Tag 10 is for belief
        if sender.tag == 10 {
            isBeliefsExpanded = !isBeliefsExpanded
        }
            //Tag 20 is for team role
        else if sender.tag == 20 {
            isTeamRoleExpanded = !isTeamRoleExpanded
        }
            //Tag 30 is for personality type
        else if sender.tag == 30 {
            isPersonalityTypeExpanded = !isPersonalityTypeExpanded
        }
            //Tag 40 is for culture structure
        else if sender.tag == 40 {
            isCultureStructureExpanded = !isCultureStructureExpanded
        }
            //Tag 50 is for motivation
        else if sender.tag == 50 {
            isMotivationExpanded = !isMotivationExpanded
        }
            //Tag 60 is for diagnostics
        else if sender.tag == 60 {
            isDiagnosticExpanded = !isDiagnosticExpanded
        }
            //Tag 70 is for tribeometer
        else if sender.tag == 70 {
            isTribeometerExpanded = !isTribeometerExpanded
        }
            //Tag 80 is for happy index
        else if sender.tag == 80 {
            isHappyIndexExpanded = !isHappyIndexExpanded
        }
            //Tag 90 is for culture index
        else if sender.tag == 90 {
            isCultureIndexExpanded = !isCultureIndexExpanded
        }
            //Tag 100 is for engagement index
        else if sender.tag == 100 {
            isEngagementIndexExpanded = !isEngagementIndexExpanded
        }
        
        tblView.reloadData()
    }
    
    /**
     * It resets the diagnostics sub category graph to initial diagnostics graph
     */
    @objc func btnBackTodiagnosticsGraphAction(_ sender: UIButton) {
        isDiagnosticsSubCategory = false
        
        self.tblView.reloadData()
    }
    
    /**
     * It resets the personality type sub category graph to initial personality type graph
     */
    @objc func btnBackToPersonalityTypeGraphAction(_ sender: UIButton) {
        isPersonalityTypeSubCategory = false
        
        self.tblView.reloadData()
    }
    
    /**
     * It sets the happy index sub category graph to previous happy index(week/month/year) graph
     */
    @objc func btnBackToHappyIndexGraphAction(_ sender: UIButton) {
        
        if selectedHappyIndexBarValue == 0 {
            return
        }
        else {
            selectedHappyIndexBarValue = selectedHappyIndexBarValue - 1
        }
        
        self.tblView.reloadData()
        
    }
    
    /**
     * This function is called when actions button (center right corner, vertical button) is clicked
     */
    @IBAction func btnActionClicked() {
        
        //create instance of ViewActionsViewController
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ViewActionsViewController") as! ViewActionsViewController
        //pass org id
        objVC.strOrgId = AuthModel.sharedInstance.orgId
        //        objVC.arrOfficeDetail = arrOfficeDetail
        
        //navigate to actions listing page
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - HADropDown Delegate Method
    /**
     * Drop Down delegate methods
     * This function is called when user selects any value from dropdown
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        //  selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
        // dropDownDept.item
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            //case when user selects office
            
            //reset dept index to 0
            selectedIndexOfDepartment = 0
            
            //reset office index to 0
            selectedIndexOfOffice = 0
            
            //reset dept drop down title
            dropDownDept.title = "ALL DEPARTMENTS"
            
            //set office index
            selectedIndexOfOffice = index
            if index != 0 {
                //case other than "ALL"
                
                //set depts according to the selected office
                arrDepartment = arrOffices[index - 1].arrDepartment
                
                //set depts names according to selected office
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                //'All' case
                if dropDownOffice.title == "ALL OFFICES"
                {
                    //set all depts
                    dropDownDept.items = arrOfAllDeparmentList
                }
                // arrDepartment = arrAllDepartment
                //arrDepartmentNames = arrAllDepartmentNames
            }
            
            //set data
            setData()
        }
        else if dropDown == dropDownDept {
            //case when user selects any dept
            
            //set selected dept index
            selectedIndexOfDepartment = index
        }
        
        //load data for the filters applied
        callWebServiceToGetOrgReport()
    }
    
    //MARK: - ChartView Delegate
    /**
     * This function is no longer in use
     */
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("highlight", highlight.x)
        print("chartView tag", chartView.tag)
        
        switch chartView.tag {
        case 300:
            print("DOT")
            
            if self.isBeliefClicked {
                return
            }
            
            self.isBeliefClicked = true
            self.selectedBelief = self.arrBeliefs[Int(highlight.x)]
            self.showLineGraphValues()
            for value in self.arrBeliefs[Int(highlight.x)].arrValues {
                arrValuesNames.append(value.strName)
            }
            
            self.tblView.reloadData()
        default:
            print("Default")
        }
    }
    
    //MARK: - BasicBarChart2Delegate
    /**
     * BarChart Delegate Methods
     * It is called when user clicks any of diagnostics bar
     */
    func didSelectGraph(index: Int) {
        print("iiiiiiiiiiiii",index)
        
        //set this to load diagnostics sub categories
        isDiagnosticsSubCategory = true
        
        //load sub categories of diagnostics
        callWebServiceForSubCategory(catID: arrOfDiagnosticResult[index].strCategoryId)
    }
    
    //MARK: - BasicBarChartPeronalityTypeDelegate
    /**
     * BarChart Delegate Methods
     * It is called when user clicks any of personality type bar
     */
    func didSelectGraphPerosnalityType(index: Int) {
        print("iiiiiiiiiiiii",index)
        
        //set this to load personality type sub categories
        isPersonalityTypeSubCategory = true
        
        //load sub categories of personality type
        callWebServiceForSubCategoryPersonalityType(catID: arrOfPersonalityTypeResult[index].strCategoryId)
    }
    
    //MARK: - BasicBarChartHappyIndexDelegate
    /**
     * BarChart Delegate Methods
     * It is called when user clicks any of happy index bar
     */
    func didSelectGraphHappyIndex(index: Int) {
        
        //check if user has not clicked day wise chart of happy index
        if selectedHappyIndexBarValue != 3 {
            //if any of the bar clicked except the day wise chart then check below consitions
            switch selectedHappyIndexBarValue {
            case 0://case when year graph is clicked
                
                //save the year been clicked
                selectedHappyIndexYear = arrHappyYearIndex[index].strYear
                
                //load the month wise data of the selected year
                callWebServiceForHappyIndexMonth(year: arrHappyYearIndex[index].strYear)
            case 1://case when month graph is clicked
                
                //save the month which is been clicked
                selectedHappyIndexMonth = String(index + 1) //arrHappyMonthIndex[index].strMonth
                
                //load the week wise data of the selected month
                callWebServiceForHappyIndexWeek(year: selectedHappyIndexYear, month: selectedHappyIndexMonth)
            case 2://case when week graph is clicked
                
                //save the week which is been clicked
                selectedHappyIndexWeek = String(index + 1) //arrHappyWeekIndex[index].strWeek
                
                //load the day wise data of the selected week
                callWebServiceForHappyIndexDay(year: selectedHappyIndexYear, month: selectedHappyIndexMonth, week: selectedHappyIndexWeek)
            default:
                print("Not Allowed")
            }
            
            //update the selected happy index bar value
            selectedHappyIndexBarValue = selectedHappyIndexBarValue + 1
        }
        else {
            //return if day wise bar is clicked
            return
        }
    }
    
    //MARK: - UITableViewDelegate and dataSource
    /**
     * Tableview delegate method used to return number of sections in table
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        //return the count of sections stored in array of reports list
        return arrReportsList.count
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //check for every section
        switch arrReportsList[section] {
        case .CultureIndex: //case of culture index section
            
            //check if culture index is in expanded mode or not
            if isCultureIndexExpanded {
                //when it is in expand mode
                
                if self.arrOfCultureIndexResult.count == 0 {
                    //return single row for no data found if no result found for culture index
                    return 1
                }
                else{
                    //arrOfCultureIndexResult.count - result count to show the values below graph
                    //1 - to show the graph
                    return arrOfCultureIndexResult.count + 1
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .EngagementIndex: //case of engagement index section
            
            //check if engagement index is in expanded mode or not
            if isEngagementIndexExpanded {
                //when it is in expand mode
                
                if self.arrOfEngagementIndexResult.count == 0 {
                    //return single row for no data found if no result found for engagement index
                    return 1
                }
                else{
                    //arrOfEngagementIndexResult.count - result count to show the values below graph
                    //1 - to show the graph
                    return arrOfEngagementIndexResult.count + 1
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .Beliefs: //case of beliefs section
            
            //check if beliefs section is in expanded mode or not
            if isBeliefsExpanded{
                //return a row if expanded
                return 1
            }
            else{
                //return no rows if section is in collapse mode
                return 0
            }
            //uncomment to show DOT as line graph
            //            if isBeliefsExpanded {
            //                if self.arrBeliefs.count == 0 {
            //                    return 1
            //                }
            //                else{
            //                    if self.isBeliefClicked {
            //                        return selectedBelief.arrValues.count + 1
            //                    }
            //                    return arrBeliefs.count + 1
            //                }
            //            }
            //            else {
            //                return 0
        //            }
        case .TeamRoles: //case of team role section
            
            //check if team role section is in expanded mode or not
            if isTeamRoleExpanded{
                //return a row if expanded
                return 1
            }
            else{
                //return no rows if section is in collapse mode
                return 0
            }
        case .PersonalityType: //case of personality type section
            /* OLD
             if isPersonalityTypeExpanded{
             return 1
             }
             else{
             return 0
             }*/
            
            //check if personality type section is in expanded mode or not
            if isPersonalityTypeExpanded {
                
                //check if personality type sub category need to be listed
                if isPersonalityTypeSubCategory {
                    //personality type sub category mode
                    
                    if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                        //return single row for no data found if no result found for sub category
                        return 1
                    }
                    else{
                        //arrOfSubCategoryPersonalityTypeResult.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrOfSubCategoryPersonalityTypeResult.count + 1
                    }
                }
                else {
                    //to show personality type result
                    
                    if !self.isPersonailtyTypeAnswered {
                        //return single row for no data found if no result found for personality type
                        return 1
                    }
                    else{
                        //arrOfPersonalityTypeResult.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrOfPersonalityTypeResult.count + 1
                    }
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .CultureStructure://case of culture structure section
            
            //check if culture structure section is in expanded mode or not
            if isCultureStructureExpanded{
                
                //create a variable to store rows count
                var noOfRows = 0
                
                if arrForAllSummary.count == 0{
                    //if no summary found for culture structre set row to 0
                    noOfRows = 0
                }
                else{
                    //set rows as 1 in case of summary
                    noOfRows = 1
                }
                
                if self.arrOfSOTResultSummary.count > 0 {
                    //if result summary found, add one row to existing rows
                    noOfRows = noOfRows + 1
                }
                
                if noOfRows == 0 {
                    //case when no summary so set rows to 2, 1 for no data found and other one for redo/review question button
                    return 2
                }
                else {
                    //return rows calculated above
                    return noOfRows
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .Motivation://case of motivation section
            
            //check if motivation is in expanded mode or not
            if isMotivationExpanded {
                
                if self.arrOfMotivationData.count == 0 {
                    //return single row for no data found if no result found for motivation data
                    return 1
                }
                else{
                    //arrOfMotivationData.count - result count to show the values below graph
                    //1 - to show the graph
                    return arrOfMotivationData.count + 1
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .Diagnostics://case of diagnostics section
            
            //check if diagnostics section is in expanded mode or not
            if isDiagnosticExpanded {
                
                //check if diagnostics sub category need to be listed
                if isDiagnosticsSubCategory {
                    //diagnostics sub category mode
                    if self.arrOfSubCategoryDiagnosticResult.count == 0{
                        //return one row for no data found and other one for redo/review question button
                        return 2
                    }
                    else{
                        //arrOfSubCategoryDiagnosticResult.count - result count to show the values below graph
                        //1 - to show the graph
                        //1- to show redo/review question button
                        return arrOfSubCategoryDiagnosticResult.count + 2
                    }
                }
                else {
                    //to show diagnostics result
                    if self.arrOfDiagnosticResult.count == 0{
                        //return one row for no data found and other one for redo/review question button
                        return 2
                    }
                    else{
                        //arrOfDiagnosticResult.count - result count to show the values below graph
                        //1 - to show the graph
                        //1- to show redo/review question button
                        return arrOfDiagnosticResult.count + 2
                    }
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .Tribeometer: //case of tribeometer section
            
            //check if tribeometer section is in expanded mode or not
            if isTribeometerExpanded {
                
                if self.arrOfTribeometerResult.count == 0{
                    //return one row for no data found and other one for redo/review question button
                    return 2
                }
                else{
                    //arrOfTribeometerResult.count - result count to show the values below graph
                    //1 - to show the graph
                    //1- to show redo/review question button
                    return arrOfTribeometerResult.count + 2
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case .HappyIndex: //case of happy index section
            
            //check if happy index section is in expanded mode or not
            if isHappyIndexExpanded {
                
                if self.arrHappyYearIndex.count == 0 {
                    //return no data found row if yearly data is nil
                    return 1
                }
                else {
                    //check the mode(yearly/monthly/weekly/daily)
                    if selectedHappyIndexBarValue == 0 {
                        //case of year graph
                        
                        //arrHappyYearIndex.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrHappyYearIndex.count + 1
                    }
                    else if selectedHappyIndexBarValue == 1 {
                        //case of month graph
                        
                        //arrHappyMonthIndex.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrHappyMonthIndex.count + 1
                    }
                    else if selectedHappyIndexBarValue == 2 {
                        //case of weekly graph
                        
                        //arrHappyWeekIndex.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrHappyWeekIndex.count + 1
                    }
                    else {
                        //case of day graph
                        
                        //arrHappyDayIndex.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrHappyDayIndex.count + 1
                    }
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        default:
            //default case
            return 0
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //switch case for every section
        switch arrReportsList[indexPath.section] {
        case .CultureIndex: //case of culture index section
            
            if self.arrOfCultureIndexResult.count == 0 {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                //check for 1st index
                if indexPath.row == 0 {
                    //create custom cell for culture index graph
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellLine") as! GraphTableViewCell
                    //                    let unitsSold = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0]
                    //                    cell.lineChart.setBarChartData(xValues: arrMonthsCultureIndex, yValues: unitsSold, label: "Index")
                    
                    //this sets the x axis with month names
                    cell.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values:arrMonthsCultureIndex)
                    
                    //set the line chart data
                    cell.lineChart.data = lineChartData
                    
                    //set the x axis label(month names) postion to bottom
                    cell.lineChart.xAxis.labelPosition = .bottom
                    
                    //set right axis labels to disappear
                    cell.lineChart.rightAxis.labelTextColor = .clear
                    
                    //disable the zoom functionality for x/y axis
                    cell.lineChart.scaleXEnabled = false
                    cell.lineChart.scaleYEnabled = false
                    //                    cell.lineChart.delegate = self
                    //                    cell.lineChart.tag = 100
                    //                    cell.lineChart.xAxis.setLabelCount(arrMonthsCultureIndex.count, force: true)
                    //                    cell.lineChart.xAxis.wordWrapEnabled = true
                    
                    //                    cell.lineChart.xAxis.drawGridLinesEnabled = false
                    //                    cell.lineChart.rightAxis.drawGridLinesEnabled = false
                    //                    cell.lineChart.leftAxis.drawGridLinesEnabled = false
                    cell.selectionStyle = .none
                    
                    return cell
                }
                else {
                    //create custom cell for culture index listiing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    
                    //set the serial no
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set the title
                    cell.lblTitle.text = arrOfCultureIndexResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set the score
                    cell.lblScore.text = arrOfCultureIndexResult[indexPath.row - 1].strScore.capitalized
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfCultureIndexResult[indexPath.row - 1].bgColor
                    cell.selectionStyle = .none
                    
                    return cell
                }
            }
        case .EngagementIndex: //case of engagement index section
            
            if self.arrOfEngagementIndexResult.count == 0 {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                //check for 1st index
                if indexPath.row == 0 {
                    //create custom cell for engagement index graph
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellLineEng") as! GraphTableViewCell
                    //                    let unitsSold = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0]
                    //                    cell.lineChart.setBarChartData(xValues: arrMonthsCultureIndex, yValues: unitsSold, label: "Index")
                    
                    //this sets the x axis with month names
                    cell.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values:arrMonthsEngagement)
                    
                    //set the line chart data
                    cell.lineChart.data = engagementLineChartData
                    
                    //set the x axis label(month names) postion to bottom
                    cell.lineChart.xAxis.labelPosition = .bottom
                    
                    //set right axis labels to disappear
                    cell.lineChart.rightAxis.labelTextColor = .clear
                    
                    //disable the zoom functionality for x/y axis
                    cell.lineChart.scaleXEnabled = false
                    cell.lineChart.scaleYEnabled = false
                    //                    cell.lineChart.delegate = self
                    //                    cell.lineChart.tag = 200
                    //                    cell.lineChart.xAxis.setLabelCount(arrMonthsEngagement.count, force: true)
                    
                    //                    cell.lineChart.xAxis.drawGridLinesEnabled = false
                    //                    cell.lineChart.rightAxis.drawGridLinesEnabled = false
                    //                    cell.lineChart.leftAxis.drawGridLinesEnabled = false
                    cell.selectionStyle = .none
                    
                    return cell
                }
                else {
                    //create custom cell for engagement index listing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    
                    //set the serial no
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set the title
                    cell.lblTitle.text = arrOfEngagementIndexResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set the score
                    cell.lblScore.text = arrOfEngagementIndexResult[indexPath.row - 1].strScore.capitalized
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfEngagementIndexResult[indexPath.row - 1].bgColor
                    cell.selectionStyle = .none
                    
                    return cell
                }
            }
        case .Beliefs: //case of beliefs section
            if arrBeliefs.count == 0{
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                //create custom cell for showing the vertical listing of values
                let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
                
                cell.selectionStyle = .none
                
                //set coming from reports to true
                cell.strComingFromReport = "True"
                
                //set array of beliefs
                cell.arrBeliefs = arrBeliefs
                
                //set max height of row
                cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
                
                //load the collection view(setting up of data in row is done inside the cell: ViewDOTCell)
                cell.collectionView.reloadData()
                return cell
            }
            //uncomment to show DOT as line graph
            /*if self.arrBeliefs.count == 0 {
             let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
             cell?.selectionStyle = .none
             return cell!
             }
             else{
             if indexPath.row == 0 {
             let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellLineDot") as! GraphTableViewCell
             
             if self.isBeliefClicked {
             cell.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: arrValuesNames)
             cell.lineChart.xAxis.setLabelCount(arrValuesNames.count, force: true) //enter the number of labels here
             cell.btnBack.isHidden = false
             }
             else {
             cell.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: arrBeliefNames)
             cell.lineChart.xAxis.setLabelCount(arrBeliefNames.count, force: true) //enter the number of labels here
             cell.btnBack.isHidden = true
             }
             
             cell.lineChart.data = beliefLineChartData
             cell.lineChart.xAxis.labelPosition = .bottom
             cell.lineChart.rightAxis.labelTextColor = .clear
             cell.lineChart.scaleXEnabled = false
             cell.lineChart.scaleYEnabled = false
             cell.lineChart.xAxis.centerAxisLabelsEnabled = false
             cell.lineChart.tag = 300
             cell.lineChart.delegate = self
             cell.btnBack.addTarget(self, action: #selector(btnBackDotAction(_:)), for: .touchUpInside)
             
             
             cell.selectionStyle = .none
             
             
             
             return cell
             }
             else {
             let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
             
             if self.isBeliefClicked {
             cell.lblMarker.text = String(indexPath.row) + " -"
             cell.lblTitle.text = selectedBelief.arrValues[indexPath.row - 1].strName.capitalized
             cell.lblScore.text = selectedBelief.arrValues[indexPath.row - 1].strRating.capitalized
             cell.lblScore.backgroundColor = selectedBelief.arrValues[indexPath.row - 1].bgColor
             }
             else {
             cell.lblMarker.text = String(indexPath.row) + " -"
             cell.lblTitle.text = arrBeliefs[indexPath.row - 1].strName.capitalized
             cell.lblScore.text = arrBeliefs[indexPath.row - 1].strRating.capitalized
             cell.lblScore.backgroundColor = arrBeliefs[indexPath.row - 1].bgColor
             }
             cell.selectionStyle = .none
             
             return cell
             }
             }*/
        case .TeamRoles: //case of team role section
            
            //create custom cell for showing team role result
            let cell : TeamRoleReportTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TeamRoleReportTableViewCell") as! TeamRoleReportTableViewCell
            cell.selectionStyle = .none
            
            //set json(setting up of data in row is done inside the cell: TeamRoleReportTableViewCell)
            cell.setData(response: cotJson)
            return cell
        case .PersonalityType: //case of personality type section
            /* OLD
             let cell : PersonalityTypeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PersonalityTypeTableViewCell") as! PersonalityTypeTableViewCell
             cell.selectionStyle = .none
             cell.setData(response: personalityJson)
             return cell*/
            
            //case of sub category mode
            if isPersonalityTypeSubCategory {
                if indexPath.row == 0 {
                    //create custom cell for showing personality type sub category bar graph view
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                    
                    //set up data entries
                    cell.basicBarChartForPersonalityType.dataEntries = dataEntriesSubCategoryPersonalityType
                    
                    //remove bar delegates
                    cell.basicBarChartForPersonalityType.delegate = nil
                    
                    cell.selectionStyle = .none
                    
                    //show back button
                    cell.btnBack.isHidden = false
                    
                    //set target for back button
                    cell.btnBack.addTarget(self, action: #selector(btnBackToPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                    return cell
                }
                else {
                    //create custom cell for sub category listiing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    cell.selectionStyle = .none
                    
                    //set serial no
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set tile
                    cell.lblTitle.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set percentage
                    cell.lblScore.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].bgColor
                    
                    return cell
                }
            }
            else {
                //showing personality type result
                if !self.isPersonailtyTypeAnswered {
                    //                    if indexPath.row == 0 {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                    //                    }
                }
                else{
                    if indexPath.row == 0 {
                        //create custom cell for showing personality type bar graph view
                        let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                        
                        //set the bar delegate
                        cell.basicBarChartForPersonalityType.delegate = self
                        
                        //set the graph data entries
                        cell.basicBarChartForPersonalityType.dataEntries = dataEntriesPersonalityType
                        cell.selectionStyle = .none
                        //do not show back button
                        cell.btnBack.isHidden = true
                        return cell
                    }
                    else {
                        //create custom cell for sub category listiing view below the graph
                        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                        cell.selectionStyle = .none
                        
                        //set serial no.
                        cell.lblMarker.text = String(indexPath.row) + " -"
                        
                        //set title
                        cell.lblTitle.text = arrOfPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrOfPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                        
                        //set the background colour depending on the score
                        cell.lblScore.backgroundColor = arrOfPersonalityTypeResult[indexPath.row - 1].bgColor
                        
                        return cell
                    }
                }
            }
            
        case .CultureStructure: //case of culture structure section
            
            if self.arrOfSOTResultSummary.count > 0 && indexPath.row == 0 {
                //create custom cell to show the result value for culture structure
                let cell : CultureStructureStaticTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CultureStructureStaticTableViewCell") as! CultureStructureStaticTableViewCell
                cell.selectionStyle = .none
                
                //show summary title
                cell.lblResultText.text = arrOfSOTResultSummary[0].strTitle + " organisation culture structure"
                
                //get summary image url
                let url = URL(string:arrOfSOTResultSummary[0].strImageUrl)
                //load summary image
                cell.imgResult.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                
                //set score & title for person detail
                cell.lblPersonScore.text = arrOfSOTDetails[0].strSOTCount
                cell.lblPersonTitle.text = arrOfSOTDetails[0].strTitle.uppercased()
                
                //set score & title for power detail
                cell.lblPowerScore.text = arrOfSOTDetails[1].strSOTCount
                cell.lblPowerTitle.text = arrOfSOTDetails[1].strTitle.uppercased()
                
                //set score & title for role detail
                cell.lblRoleScore.text = arrOfSOTDetails[2].strSOTCount
                cell.lblRoleTitle.text = arrOfSOTDetails[2].strTitle.uppercased()
                
                //set score & title for tribe detail
                cell.lblTribeScore.text = arrOfSOTDetails[3].strSOTCount
                cell.lblTribeTitle.text = arrOfSOTDetails[3].strTitle.uppercased()
                
                return cell
            }
            else if arrForAllSummary.count == 0 {
                if indexPath.row == 0 {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                }
                else {
                    //return a redo/review button cell
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewCultureStructure")
                    cell?.selectionStyle = .none
                    
                    //set redo/review button tag & target
                    let btn = cell?.viewWithTag(70) as! UIButton
                    btn.addTarget(self, action: #selector(btnRedoReviewCulturestructureAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
            }
            else {
                //create custom cell to show the result value for summary text
                let cell : CultureStructureDynamicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CultureStructureDynamicTableViewCell") as! CultureStructureDynamicTableViewCell
                cell.selectionStyle = .none
                
                //set attributed text for summary value
                cell.lblSummary.attributedText = arrForAllSummary[indexPath.row].strValue
                
                //set target for redo/review button
                cell.btnRedoReview.addTarget(self, action: #selector(btnRedoReviewCulturestructureAction(_:)), for: .touchUpInside)
                return cell
            }
        case .Motivation: //case of motivation section
            if self.arrOfMotivationData.count == 0 {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                if indexPath.row == 0 {
                    //create custom cell for showing motivation bar graph view
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellMotivation") as! GraphTableViewCell
                    //set data entries
                    cell.basicBarChartMotivation.dataEntries = dataEntriesMotivation
                    cell.selectionStyle = .none
                    return cell
                }
                else {
                    //create custom cell for motivation listiing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    
                    //set the serial no.
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set the title
                    cell.lblTitle.text = arrOfMotivationData[indexPath.row - 1].strTitle.capitalized
                    
                    //set the score
                    cell.lblScore.text = arrOfMotivationData[indexPath.row - 1].strScore.capitalized + "%"
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfMotivationData[indexPath.row - 1].bgColor
                    cell.selectionStyle = .none
                    
                    return cell
                }
            }
        case .Diagnostics: //case of diagnostics section
            
            if isDiagnosticsSubCategory {
                //case of diagnostics sub category
                if indexPath.row == 0 {
                    //create custom cell for showing diagnostics sub category bar graph view
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellDiagnostics") as! GraphTableViewCell
                    
                    //set data entries
                    cell.basicBarChartForDiagnostics.dataEntries = dataEntriesSubCategoryDiagnostics
                    
                    //remove the delegates
                    cell.basicBarChartForDiagnostics.delegate = nil
                    cell.selectionStyle = .none
                    
                    //show back button
                    cell.btnBack.isHidden = false
                    
                    //set back button target
                    cell.btnBack.addTarget(self, action: #selector(btnBackTodiagnosticsGraphAction(_:)), for: .touchUpInside)
                    return cell
                }
                else if indexPath.row == arrOfSubCategoryDiagnosticResult.count + 1 {
                    
                    //create custom cell for showing redo/review button
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewDiagnostics")
                    cell?.selectionStyle = .none
                    
                    //set redo/review button tag
                    let btn = cell?.viewWithTag(90) as! UIButton
                    
                    //set redo/review button target
                    btn.addTarget(self, action: #selector(btnRedoReviewDiagnosticsAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
                else {
                    //create custom cell for motivation listiing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    cell.selectionStyle = .none
                    
                    //set serial no.
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set title
                    cell.lblTitle.text = arrOfSubCategoryDiagnosticResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set score
                    cell.lblScore.text = arrOfSubCategoryDiagnosticResult[indexPath.row - 1].strPercentage + "%"
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfSubCategoryDiagnosticResult[indexPath.row - 1].bgColor
                    
                    return cell
                }
            }
            else {
                //showing diagnostics result
                
                if self.arrOfDiagnosticResult.count == 0{
                    
                    if indexPath.row == 0 {
                        //return no record cell in case of no result
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                        cell?.selectionStyle = .none
                        return cell!
                    }
                    else {
                        //create custom cell for showing redo/review button
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewDiagnostics")
                        cell?.selectionStyle = .none
                        
                        //set redo/review button tag
                        let btn = cell?.viewWithTag(90) as! UIButton
                        
                        //set redo/review button target
                        btn.addTarget(self, action: #selector(btnRedoReviewDiagnosticsAction(_:)), for: .touchUpInside)
                        
                        return cell!
                    }
                }
                else{ 
                    if indexPath.row == 0 {
                        //create custom cell for showing diagnostics bar graph view
                        let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellDiagnostics") as! GraphTableViewCell
                        
                        //set bar graph delegates
                        cell.basicBarChartForDiagnostics.delegate = self
                        
                        //set data entries
                        cell.basicBarChartForDiagnostics.dataEntries = dataEntriesDiagnostics
                        cell.selectionStyle = .none
                        
                        //hide back button
                        cell.btnBack.isHidden = true
                        return cell
                    }
                    else if indexPath.row == arrOfDiagnosticResult.count + 1 {
                        //create custom cell for showing redo/review button
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewDiagnostics")
                        cell?.selectionStyle = .none
                        
                        //set redo/review button tag
                        let btn = cell?.viewWithTag(90) as! UIButton
                        
                        //set target for redo/review button
                        btn.addTarget(self, action: #selector(btnRedoReviewDiagnosticsAction(_:)), for: .touchUpInside)
                        
                        return cell!
                    }
                    else {
                        //create custom cell for motivation listing view below the graph
                        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                        cell.selectionStyle = .none
                        
                        //set serial no.
                        cell.lblMarker.text = String(indexPath.row) + " -"
                        
                        //set title
                        cell.lblTitle.text = arrOfDiagnosticResult[indexPath.row - 1].strTitle.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrOfDiagnosticResult[indexPath.row - 1].strPercentage + "%"
                        
                        //set the background colour depending on the percentage
                        cell.lblScore.backgroundColor = arrOfDiagnosticResult[indexPath.row - 1].bgColor
                        
                        return cell
                    }
                }
            }
        case .Tribeometer: //case of tribeometer section
            if self.arrOfTribeometerResult.count == 0{
                if indexPath.row == 0 {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                }
                else {
                    //create custom cell for showing redo/review button
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewTribeometer")
                    cell?.selectionStyle = .none
                    
                    //set the button tag
                    let btn = cell?.viewWithTag(80) as! UIButton
                    
                    //set target for button
                    btn.addTarget(self, action: #selector(btnRedoReviewTribeometerAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
            }
            else{
                //case when there is result for tribeometer
                if indexPath.row == 0 {
                    //create custom cell for showing tribeometer bar graph view
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellTribeometer") as! GraphTableViewCell
                    
                    //set data entries for graph
                    cell.basicBarChartTribeometer.dataEntries = dataEntriesTribeometer
                    cell.selectionStyle = .none
                    
                    return cell
                }
                else if indexPath.row == arrOfTribeometerResult.count + 1 {
                    //create custom cell for showing redo/review button
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewTribeometer")
                    cell?.selectionStyle = .none
                    
                    //set button tag
                    let btn = cell?.viewWithTag(80) as! UIButton
                    
                    //set target for button
                    btn.addTarget(self, action: #selector(btnRedoReviewTribeometerAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
                else {
                    //create custom cell for motivation listing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    cell.selectionStyle = .none
                    
                    //set serial no.
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set title
                    cell.lblTitle.text = arrOfTribeometerResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set percentage
                    cell.lblScore.text = arrOfTribeometerResult[indexPath.row - 1].strPercentage.capitalized + "%"
                    
                    //set the background colour depending on the percentage
                    cell.lblScore.backgroundColor = arrOfTribeometerResult[indexPath.row - 1].bgColor
                    
                    return cell
                }
            }
        case .HappyIndex: //case of happy index section
            if self.arrHappyYearIndex.count == 0 {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else {
                
                if indexPath.row == 0 {
                    //create custom cell for showing tribeometer bar graph view
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellHappyIndex") as! GraphTableViewCell
                    
                    //set bar delegates
                    cell.BasicBarChartHappyIndex.delegate = self
                    
                    if selectedHappyIndexBarValue == 0 {
                        //if year graph is been displayed, set data entries for year graph
                        cell.BasicBarChartHappyIndex.dataEntries = dataEntriesHappyIndexYear
                    }
                    else if selectedHappyIndexBarValue == 1 {
                        //if month graph is been displayed, set data entries for month graph
                        cell.BasicBarChartHappyIndex.dataEntries = dataEntriesHappyIndexMonth
                    }
                    else if selectedHappyIndexBarValue == 2 {
                        //if week graph is been displayed, set data entries for week graph
                        cell.BasicBarChartHappyIndex.dataEntries = dataEntriesHappyIndexWeek
                    }
                    else {
                        //if day graph is been displayed, set data entries for day graph
                        cell.BasicBarChartHappyIndex.dataEntries = dataEntriesHappyIndexDay
                    }
                    
                    if selectedHappyIndexBarValue > 0 &&  selectedHappyIndexBarValue < 4 {
                        //show back button for month/week/day wise graph display
                        cell.btnBack.isHidden = false
                        
                        //set target for back button for month/week/day wise graph display
                        cell.btnBack.addTarget(self, action: #selector(btnBackToHappyIndexGraphAction(_:)), for: .touchUpInside)
                    }
                    else {
                        //hide back button when year data id been displayed
                        cell.btnBack.isHidden = true
                    }
                    
                    cell.selectionStyle = .none
                    return cell
                }
                else {
                    //create custom cell for happy index listing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    cell.selectionStyle = .none
                    
                    //set serial count
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    if selectedHappyIndexBarValue == 0 {
                        //year graph case
                        
                        //set the title
                        cell.lblTitle.text = arrHappyYearIndex[indexPath.row - 1].strYear.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrHappyYearIndex[indexPath.row - 1].strHappyIndex + "%"
                        
                        //set the background colour depending on the percentage
                        cell.lblScore.backgroundColor = arrHappyYearIndex[indexPath.row - 1].bgColor
                    }
                    else if selectedHappyIndexBarValue == 1 {
                        //month graph case
                        
                        //set the title
                        cell.lblTitle.text = arrHappyMonthIndex[indexPath.row - 1].strMonth.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrHappyMonthIndex[indexPath.row - 1].strHappyIndex + "%"
                        
                        //set the background colour depending on the percentage
                        cell.lblScore.backgroundColor = arrHappyMonthIndex[indexPath.row - 1].bgColor
                    }
                    else if selectedHappyIndexBarValue == 2 {
                        //week graph case
                        
                        //set the title
                        cell.lblTitle.text = arrHappyWeekIndex[indexPath.row - 1].strWeek.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrHappyWeekIndex[indexPath.row - 1].strHappyIndex + "%"
                        
                        //set the background colour depending on the percentage
                        cell.lblScore.backgroundColor = arrHappyWeekIndex[indexPath.row - 1].bgColor
                    }
                    else {
                        //day graph case
                        
                        //set the title
                        cell.lblTitle.text = arrHappyDayIndex[indexPath.row - 1].strDay.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrHappyDayIndex[indexPath.row - 1].strHappyIndex + "%"
                        
                        //set the background colour depending on the percentage
                        cell.lblScore.backgroundColor = arrHappyDayIndex[indexPath.row - 1].bgColor
                    }
                    
                    return cell
                }
                
            }
        default:
            //return no record cell in default case
            let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
            cell?.selectionStyle = .none
            return cell!
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //switch case for every section
        switch arrReportsList[indexPath.section] {
        case .CultureIndex:
            if self.arrOfCultureIndexResult.count == 0 {
                //return no record cell height
                return  50
            }
            else{
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .EngagementIndex:
            if self.arrOfEngagementIndexResult.count == 0 {
                //return no record cell height
                return  50
            }
            else{
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .Beliefs:
            if arrBeliefs.count == 0{
                //return no record cell height
                return  50
            }
            else{
                //return the height for the vertical values listing
                return  110 + CGFloat( 90 * calacualtedMaxHeightOfCollection)
            }
            //uncomment to show DOT as line graph
            //            if self.arrBeliefs.count == 0 {
            //                return  50
            //            }
            //            else{
            //                if indexPath.row == 0 {
            //                    return 390
            //                }
            //                else {
            //                    return 40
            //
            //                }
        //            }
        case .TeamRoles:
            //return height for team role cell
            return 260//340
        case .PersonalityType:
            //OLD            return 300
            if !self.isPersonailtyTypeAnswered {
                //return no record cell height
                return 50
            }
            else {
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .CultureStructure:
            if self.arrOfSOTResultSummary.count > 0 && indexPath.row == 0 {
                //return summary cell height
                return 430
            }
            else if arrForAllSummary.count == 0 {
                //return no data cell height
                return  50
            }
            else {
                //return height which is automatic adjustable according to content of cell
                return UITableViewAutomaticDimension
            }
        case .Motivation:
            if self.arrOfMotivationData.count == 0 {
                //return no data cell height
                return  50
            }
            else{
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .Diagnostics:
            if self.arrOfDiagnosticResult.count == 0 {
                //return no data cell height
                return 50
            }
            else {
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else if indexPath.row == arrOfDiagnosticResult.count + 1 {
                    //return redo/review cell height
                    return 60
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .Tribeometer:
            if self.arrOfTribeometerResult.count == 0 {
                //return no data cell height
                return 50
            }
            else {
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else if indexPath.row == arrOfTribeometerResult.count + 1 {
                    //return redo/review cell height
                    return 60
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case .HappyIndex:
            if self.arrHappyYearIndex.count == 0 {
                //return no data cell height
                return 50
            }
            else {
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        default:
            //return 0 for default case
            return 0
        }
        
    }
    
    /**
     * Tableview delegate method used to return  section header view
     */
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //switch case for every section
        switch arrReportsList[section] {
        case .CultureIndex:
            //return culture index view
            return cultureIndexView
        case .EngagementIndex:
            //return engagement index view
            return engagementIndexView
        case .Beliefs:
            //return beliefs view
            return DOTView
        case .TeamRoles:
            //return team role view
            return teamRoleView
        case .PersonalityType:
            //return personality type view
            return personalityTypeView
        case .CultureStructure:
            //return culture structure view
            return cultureStructureView
        case .Motivation:
            //return motivation view
            return motivationView
        case .Diagnostics:
            //return diagnostics view
            return diagnosticView
        case .Tribeometer:
            //return tribeometer view
            return tribeometerView
        case .HappyIndex:
            //return happy index view
            return happyIndexView
            
        default:
            return nil
        }
    }
    
    /**
     * Tableview delegate method used to return  height for section header view
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    /**
     * Tableview delegate method used to return  height for section footer view
     */
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    //MARK: - Call Web service
    /**
     * API - get the unread notification count
     * Called but notification count is no longer used
     */
    @objc func callWebServiceToGetUnreadCount() {
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleUnReadNotifications, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["data"]["notificationCount"].stringValue == "0" {
                    self.lblUnredCount.layer.borderWidth = 0.0
                    self.lblUnredCount.text = " "
                }
                else {
                    self.lblUnredCount.layer.borderWidth = 1.0
                    self.lblUnredCount.text = response!["data"]["notificationCount"].stringValue
                }
            }
        }
    }
    
    //    func callWebServiceTogetDiagnosticsTribeometerAnswerDone(){
    //
    //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
    //
    //        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Diagnostic.isDiagnosticTribeometerAnswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
    //
    //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    //
    //            if response == nil {
    //                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
    //            }
    //            else{
    //                if response!["status"].boolValue == true {
    //                    print("SUCCESS Add")
    //
    //                    DiagnosticParser.parseStatusOfDiagnosticTribeometerAnswerDone(response: response!, completionHandler: { (isDiagnosticAnsDone, isTribeometerAnsDone) in
    //
    //                        self.isDiagnosticAnsDone = isDiagnosticAnsDone
    //                        self.isTribeometerAnsDone = isTribeometerAnsDone
    //
    //                    })
    //
    //                }
    //                else {
    //                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
    //                }
    //            }
    //        }
    //
    //    }
    
    /**
     * API - to get organisation report
     */
    func callWebServiceToGetOrgReport() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId, //set org id of logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "", //set selected office id from drop down
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "" //set selected global dept id from drop down
            ]
            
        }
        else{
            param = [ "orgId": AuthModel.sharedInstance.orgId, //set org id of logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//set selected office id from drop down
                "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : ""] //set selected dept id from drop down
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getOrgDashboardReportWithFilter, param: param, withHeader: true ) { (response, errorMsg) in
            
            
            if response == nil {
                //stop the loader
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                //Parse Beliefs & Values
                AdminReportsParser.parseDOTDetailKnowOrg(response: response!, completionHandler: { (arrOfDOTReports, beliefNames) in
                    
                    //set belief value to array
                    self.arrBeliefs = arrOfDOTReports
                    
                    //set belif names array
                    self.arrBeliefNames = beliefNames
                    
                })
                
                //calculate height for the belief cell
                self.CalculateMaxHeight()
                
                //uncomment to show DOT as line graph
                //                if self.arrBeliefs.count == 0{
                //                    self.arrBeliefs.removeAll()
                //                }
                //                else{
                //                    self.showLineGraphBelief()
                //                }
                //
                //                self.setDataInColoumnsBeliefs()
                
                //TeamRole - set json for team role
                self.cotJson = response!["data"]["getCOTteamRoleMapReport"]
                
                //PersonalityType
                //OLD                self.personalityJson = response!["data"]["getCOTpersonalityType"]
                //parse personality type reports
                DiagnosticParser.parseDiagnosticReportKnow(response: response!["data"]["getCOTpersonalityType"], completionHandler: { (arrOfDiagnosticResult) in
                    //set personality type result
                    self.arrOfPersonalityTypeResult = arrOfDiagnosticResult
                })
                
                if self.arrOfPersonalityTypeResult.count == 0{
                    //empty personality type result values in case of no data
                    self.arrOfPersonalityTypeResult.removeAll()
                }
                else{
                    //set data entries for personality type
                    self.dataEntriesPersonalityType = self.generateDataEntriesPersonalitytype()
                }
                
                //set if user has answered personality type questions or not
                self.isPersonailtyTypeAnswered = response!["data"]["orgStatus"]["personalityTypeStatus"].boolValue
                
                //set up graph for personality type
                self.setGraphDataPersonalityType()
                
                //Culture Structure - parse culture structure data
                SOTParser.parseSOTDetailsKnow(response: response!["data"]["getSOTcultureStructureReport"], completionHandler: { (arrOfSOTDetail , arrOfSOTResultSummmary, IsQuestionnaireAnswerFilled, IsUserFilledAnswer) in
                    
                    //store the culture structure details
                    self.arrOfSOTDetails = arrOfSOTDetail
                    
                    //store the summary details
                    self.arrOfSOTResultSummary = arrOfSOTResultSummmary
                    
                    //set user flags
                    self.IsQuestionnaireAnswerFilled = IsQuestionnaireAnswerFilled
                    self.IsUserFilledAnswer = IsUserFilledAnswer
                    
                })
                
                //set culture structure result image
                self.SetOrganisationCultureReultImage()
                
                //set the result view for scores
                self.SetScoreForModule()
                
                //Motivation - parse result
                SOTParser.parseSOTMotivationStructureDetailKnow(response: response!["data"]["getSOTmotivationReport"], completionHandler: { (arrOfMotivationStructure) in
                    
                    //set motivation result data
                    self.arrOfMotivationData = arrOfMotivationStructure
                })
                
                if self.arrOfMotivationData.count > 0 {
                    //set data entries for motivation graph
                    self.dataEntriesMotivation = self.generateDataEntriesMotivation()
                }
                
                //set up motivation graoh
                self.setDataInColoumns()
                
                //Diagnostics - parse result
                DiagnosticParser.parseDiagnosticReportKnow(response: response!["data"]["getDiagnosticReportForGraph"], completionHandler: { (arrOfDiagnosticResult) in
                    
                    //set diagnostics result
                    self.arrOfDiagnosticResult = arrOfDiagnosticResult
                })
                
                if !response!["data"]["orgStatus"]["diagnosticStatus"].boolValue {
                    self.arrOfDiagnosticResult.removeAll()
                }
                
                if self.arrOfDiagnosticResult.count == 0{
                    self.arrOfDiagnosticResult.removeAll()
                }
                else{
                    //set data entries for diagnostics graph
                    self.dataEntriesDiagnostics = self.generateDataEntriesDiagnostics()
                }
                
                //set up diagnostics graph
                self.setGraphData()
                
                //Tribeometer - parse result
                DiagnosticParser.parseDiagnosticReportKnow(response: response!["data"]["getTribeometerReportForGraph"], completionHandler: { (arrOfDiagnosticResult) in
                    
                    //set tribeometer result
                    self.arrOfTribeometerResult = arrOfDiagnosticResult
                    
                })
                
                if !response!["data"]["orgStatus"]["tribeometerStatus"].boolValue {
                    self.arrOfTribeometerResult.removeAll()
                }
                
                if self.arrOfTribeometerResult.count == 0{
                    
                    self.arrOfTribeometerResult.removeAll()
                }
                else{
                    //set data entries for tribeometer
                    self.dataEntriesTribeometer = self.generateDataEntriesTribeometer()
                }
                
                //set up tribeometer graph
                self.setDataInColoumnsTribeometer()
                
                //CultureIndex - parse result
                DiagnosticParser.parseCultureIndex(response: response!["data"]["cultureIndex"], completionHandler: { (arrOfDiagnosticResult, arrMonths) in
                    
                    //set culture index result value
                    self.arrOfCultureIndexResult = arrOfDiagnosticResult
                    
                    //set months value
                    self.arrMonthsCultureIndex = arrMonths
                })
                
                if self.arrOfCultureIndexResult.count == 0{
                    self.arrOfCultureIndexResult.removeAll()
                }
                else{
                    //preapre data for culture index line graph
                    self.showLineGraph()
                }
                
                //set up line graph
                self.setDataInColoumnsCultureIndex()
                
                //Engagement - parse engagement index data
                DiagnosticParser.parseCultureIndex(response: response!["data"]["engagementIndex"], completionHandler: { (arrOfDiagnosticResult, arrMonths) in
                    
                    //set up engagement index data
                    self.arrOfEngagementIndexResult = arrOfDiagnosticResult
                    
                    //set up months data
                    self.arrMonthsEngagement = arrMonths
                })
                
                if self.arrOfEngagementIndexResult.count == 0{
                    self.arrOfEngagementIndexResult.removeAll()
                }
                else{
                    //preapre data for engagement index line graph
                    self.showLineGraphEngagement()
                }
                
                //set up data for engagement index
                self.setDataInColoumnsEngagementIndex()
                
                //HappyIndex - parse result
                DashboardParser.parseHappyIndex(response: response!["data"]["getHappyIndexYearGraphCount"]) { (arr) in
                    //set data for happy index year
                    self.arrHappyYearIndex = arr
                }
                
                if self.arrHappyYearIndex.count == 0{
                    self.arrHappyYearIndex.removeAll()
                }
                else{
                    //set data entries for happy index year graph
                    self.dataEntriesHappyIndexYear = self.generateDataEntriesHappyIndex(arr: self.arrHappyYearIndex)
                }
                
                //set data
                self.arrHappyYearIndex = self.setGraphDataHapyIndex(arr: self.arrHappyYearIndex)
                
                //get the max value count for happy index
                self.happyIndexMaxValue = response!["data"]["happyMaxCount"].intValue
                
                if self.loadForFirst == false{
                    //generate dept list if its the first time case
                    self.loadForFirst = true
                    self.CreateDropDownListOfAllDepartemntArray()
                }
                
                //set diagnostics & tribeometer status of whether its answers are completed or not by logged in user
                self.isDiagnosticAnsDone = response!["data"]["orgStatus"]["isDiagnosticAnsDone"].boolValue
                self.isTribeometerAnsDone = response!["data"]["orgStatus"]["isTribeometerAnsDone"].boolValue
                
                //                self.isBeliefsExpanded = true
                //                self.isTeamRoleExpanded = true
                //                self.isPersonalityTypeExpanded = true
                //                self.isCultureStructureExpanded = true
                //                self.isMotivationExpanded = true
                //                self.isDiagnosticExpanded = true
                //                self.isTribeometerExpanded = true
                //                self.isCultureIndexExpanded = true
                //                self.isEngagementIndexExpanded = true
                //                self.isHappyIndexExpanded = true
                
                self.isPersonalityTypeSubCategory = false
                
                //case when we want to directly redirect to happy index section
                if self.isRedirectToHappyIndex {
                    
                    //set the expanded mode true
                    self.isHappyIndexExpanded = true
                    
                    //get the number of rows in order to scroll the table view to last row
                    var row = 0
                    if self.arrHappyYearIndex.count == 0 {
                        row = 1
                    }
                    else {
                        if self.selectedHappyIndexBarValue == 0 {
                            row =  self.arrHappyYearIndex.count + 1
                        }
                        else if self.selectedHappyIndexBarValue == 1 {
                            row = self.arrHappyMonthIndex.count + 1
                        }
                        else if self.selectedHappyIndexBarValue == 2 {
                            row = self.arrHappyWeekIndex.count + 1
                        }
                        else {
                            row = self.arrHappyDayIndex.count + 1
                        }
                    }
                    
                    self.tblView.reloadData()
                    
                    //scroll to happy index section
                    self.tblView.scrollToRow(at: IndexPath.init(row: row - 1, section: self.arrReportsList.count - 1), at: .bottom, animated: false)
                }
                else {
                    //expand the culture index section by default
                    self.isCultureIndexExpanded = true
                    self.tblView.reloadData()
                }
                
                //stop the loader
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
        }
    }
    
    /**
     * API - To get all office along with there respective departments
     */
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": AuthModel.sharedInstance.orgId] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse office and departments
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        
                        //store office array
                        self.arrOffices = arrOffice
                        
                        //store dept array
                        self.arrDepartment = arrDept
                        
                        //store all global array's of dept independent of office
                        self.arrAllDepartment = arrDept
                        
                        //store all office names for drop down
                        self.arrOfficesName = arrOfcNames
                        
                        //store all dept names for drop down
                        self.arrDepartmentNames = arrDeptNames
                        
                        //store all global dep names for drop down
                        self.arrAllDepartmentNames = arrDeptNames
                        
                        //create array with "All" functionality
                        self.setData()
                    })
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     API - To get all depts
     */
    func callWebServiceToGetAllDepartments() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse depts
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        
                        //store depts names for drop downs
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if self.loadForFirst == false{
                        //if first time load case
                        self.loadForFirst = true
                        
                        //create array for all global depts along with "All" feature
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    
                    //show error message when no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - to get the value for personality type sub category
     * @param catID: personality type selected category  id
     */
    func callWebServiceForSubCategoryPersonalityType(catID: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office id
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",//selected global dept id
                "categoryId": catID //personality type category value, whose sub category need to be loaded
            ]
            
        }
        else{
            //case when office & dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office id
                "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",//selected global dept id
                "categoryId": catID //personality type category value, whose sub category need to be loaded
            ]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getPersonalitytypeReportSubGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse personality type sub category result
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfPersonalityTypeResult) in
                        
                        //set array for sub category
                        self.arrOfSubCategoryPersonalityTypeResult = arrOfPersonalityTypeResult
                        
                    })
                    if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                        
                        self.arrOfPersonalityTypeResult.removeAll()
                    }
                    else{
                        //set data entries for graph
                        self.dataEntriesSubCategoryPersonalityType = self.generateDataEntriesSubCategoryPersonalityType()
                    }
                    
                    //set up graph for sub category
                    self.setGraphDataSubCategoryPeronalityType()
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrOfSubCategoryPersonalityTypeResult.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - to get the value for diagnostics sub category
     * @param catID: diagnostics selected category  id
     */
    func callWebServiceForSubCategory(catID: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office id
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",//selected global dept id
                "categoryId": catID //diagnostic category value, whose sub category need to be loaded
            ]
            
        }
        else{
            //case when office & dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office id
                "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",//selected global dept id
                "categoryId": catID //personality type category value, whose sub category need to be loaded
            ]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getDiagnsticReportSubGraph, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse result
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfDiagnosticResult) in
                        //set sub category array for diagnostics
                        self.arrOfSubCategoryDiagnosticResult = arrOfDiagnosticResult
                        
                    })
                    if self.arrOfSubCategoryDiagnosticResult.count == 0{
                        
                        self.arrOfDiagnosticResult.removeAll()
                    }
                    else{
                        //set data entries for diagnostics sub category graph
                        self.dataEntriesSubCategoryDiagnostics = self.generateDataEntriesSubCategoryDiagnostics()
                    }
                    
                    //set up graph
                    self.setGraphDataSubCategory()
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrOfSubCategoryDiagnosticResult.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - Get happy index month wise data
     * @param year: selected year whose month data needs to be loaded
     */
    func callWebServiceForHappyIndexMonth(year: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",//selected global dept value
                "year": year //selected year whose month data needs to be loaded
            ]
            
        }
        else{
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office
                "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",//selected dept value
                "year": year]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getHappyIndexMonthCount, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                //set the value to "Month"
                self.selectedHappyIndexBarValue = self.selectedHappyIndexBarValue - 1
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse month data
                    DashboardParser.parseHappyIndex(response: response!["data"]) { (arr) in
                        //set data
                        self.arrHappyMonthIndex = arr
                    }
                    
                    if self.arrHappyMonthIndex.count == 0{
                        //in case of no data found reset the array and data entries
                        self.arrHappyMonthIndex.removeAll()
                        self.dataEntriesHappyIndexMonth.removeAll()
                        
                    }
                    else{
                        //set data entries for happy index
                        self.dataEntriesHappyIndexMonth = self.generateDataEntriesHappyIndex(arr: self.arrHappyMonthIndex)
                        self.arrHappyMonthIndex = self.setGraphDataHapyIndex(arr: self.arrHappyMonthIndex)
                    }
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrHappyMonthIndex.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - Get happy index week wise data
     * @param year: selected year whose month data needs to be loaded
     * @param month: selected month whose week data needs to be loaded
     */
    func callWebServiceForHappyIndexWeek(year: String, month: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",//selected global dept value
                "year": year,//selected year whose month data needs to be loaded
                "month": month//selected month whose week data needs to be loaded
            ]
            
        }
        else{
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office
                "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",//selected dept value
                "year": year,//selected year whose month data needs to be loaded
                "month": month//selected month whose week data needs to be loaded
            ]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getHappyIndexWeeksCount, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                //set the value to "Week"
                self.selectedHappyIndexBarValue = self.selectedHappyIndexBarValue - 1
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse result
                    DashboardParser.parseHappyIndex(response: response!["data"]) { (arr) in
                        //set data
                        self.arrHappyWeekIndex = arr
                    }
                    
                    if self.arrHappyWeekIndex.count == 0{
                        //reset all values in case of no week data
                        self.arrHappyWeekIndex.removeAll()
                        self.dataEntriesHappyIndexWeek.removeAll()
                        
                    }
                    else{
                        //set data entries for week
                        self.dataEntriesHappyIndexWeek = self.generateDataEntriesHappyIndex(arr: self.arrHappyWeekIndex)
                        self.arrHappyWeekIndex = self.setGraphDataHapyIndex(arr: self.arrHappyWeekIndex)
                    }
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrHappyWeekIndex.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - Get happy index day wise data
     * @param year: selected year whose month data needs to be loaded
     * @param month: selected month whose week data needs to be loaded
     * @param week: selected week whose days data needs to be loaded
     */
    func callWebServiceForHappyIndexDay(year: String, month: String, week: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            //case when only dept is selected
            param = [ "orgId": AuthModel.sharedInstance.orgId,//org id for logged in user
                "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",//selected office id
                "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",//selected global dept id
                "year": year,//selected year whose month data needs to be loaded
                "month": month,//selected month whose week data needs to be loaded
                "week": week//selected week whose days data needs to be loaded
            ]
            
        }
        else{
            param = [ "orgId": AuthModel.sharedInstance.orgId,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                      "year": year,//selected year whose month data needs to be loaded
                "month": month,//selected month whose week data needs to be loaded
                "week": week//selected week whose days data needs to be loaded
            ]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getHappyIndexDaysCount, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                //set the value to "Days"
                self.selectedHappyIndexBarValue = self.selectedHappyIndexBarValue - 1
                
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse result
                    DashboardParser.parseHappyIndex(response: response!["data"]) { (arr) in
                        //set data
                        self.arrHappyDayIndex = arr
                    }
                    
                    if self.arrHappyDayIndex.count == 0{
                        //reset all values in case of no days data
                        self.arrHappyDayIndex.removeAll()
                        self.dataEntriesHappyIndexDay.removeAll()
                    }
                    else{
                        //set data entries for days
                        self.dataEntriesHappyIndexDay = self.generateDataEntriesHappyIndex(arr: self.arrHappyDayIndex)
                        self.arrHappyDayIndex = self.setGraphDataHapyIndex(arr: self.arrHappyDayIndex)
                    }
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrHappyDayIndex.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

/**
 * line chart extension
 */
extension LineChartView {
    
    private class BarChartFormatter: NSObject, IAxisValueFormatter {
        
        var labels: [String] = []
        
        func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return labels[Int(value)]
        }
        
        init(labels: [String]) {
            super.init()
            self.labels = labels
        }
    }
    
    func setBarChartData(xValues: [String], yValues: [Double], label: String) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<yValues.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: yValues[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: "Index")
        let chartData = ChartData(dataSet: chartDataSet)
        
        let chartFormatter = BarChartFormatter(labels: xValues)
        let xAxis = XAxis()
        xAxis.valueFormatter = chartFormatter
        self.xAxis.valueFormatter = xAxis.valueFormatter
        //        self.xAxis.labelPosition = .bottom
        
        self.data = chartData
        
        
        
        //        var lineChartEntry = [ChartDataEntry]()
        //
        //        var numArray = [Double]()
        //
        //        for value in self.arrOfCultureIndexResult {
        //            numArray.append(Double(   value.strScore)!)
        //        }
        //
        //        for i in 0..<numArray.count {
        //
        //            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
        //            lineChartEntry.append(value)
        //        }
        //
        //        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Index")
        //        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //
        //        lineChartData.addDataSet(line1)
    }
}
