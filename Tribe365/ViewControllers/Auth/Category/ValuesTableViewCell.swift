//
//  ValuesTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
This class is used to show the belief values
Its used in different tables and depending on screen requirement this cell is modified
*/
class ValuesTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //Two value labels are used in case we show 2 values in a single row
    @IBOutlet weak var lblValue1: UILabel!
    @IBOutlet weak var lblValue2: UILabel!
    
    //Two value buttons are used in case we show 2 values in a single row
    @IBOutlet weak var btnValue1: UIButton!
    @IBOutlet weak var btnValue2: UIButton!
    
    //created value 2 view in case we need to show only 1 value so we can hide this 2nd value view
    @IBOutlet weak var viewValue2: UIView!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
