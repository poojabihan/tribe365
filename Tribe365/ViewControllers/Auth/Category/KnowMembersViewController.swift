//
//  KnowMembersViewController.swift
//  Tribe365
//
//  Created by Apple on 20/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import SwiftyJSON
import Charts

/**
 * This class is used to show the know members screen
 */
class KnowMembersViewController: UIViewController, HADropDownDelegate, UITableViewDelegate, UITableViewDataSource, BasicBarChartPeronalityTypeDelegate {
    
    //MARK: - IBoutlets
    //custom drop down for users
    @IBOutlet weak var dropDownUser: HADropDown!
    
    //table view
    @IBOutlet var tblView: UITableView!
    
    //Shows organisation image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //Shows unread notification count
    @IBOutlet weak var lblUnredCount: UILabel!
    
    //header view for section Engagement Index
    @IBOutlet var engagementIndexView: UIView!
    
    //header view for section beliefs & values
    @IBOutlet var DOTView: UIView!
    
    //header view for section team role
    @IBOutlet var teamRoleView: UIView!
    
    //header view for section personality type
    @IBOutlet var personalityTypeView: UIView!
    
    //header view for section kudos
    @IBOutlet var bubbleView: UIView!
    
    //header view for section motivation
    @IBOutlet var motivationView: UIView!
    
    //header view for section diagnostics
    @IBOutlet var diagnosticView: UIView!
    
    //header view for section tribeometer
    @IBOutlet var tribeometerView: UIView!
    
    // MARK: - Variables
    //to show error/warning
    let panel = JKNotificationPanel()
    
    //stores users model list
    var arrOfUserList = [ResponsiblePersonModel]()
    
    //check if already loaded before for depts list
    var loadForFirst = false
    
    //stores users names list
    var arrOfUsersNameList = [String]()
    
    //stores selected user index value from drop down
    var selectedIndexOfUser = -1
    
    //stores beliefs model list
    var arrBeliefs = [BeliefModel]()
    
    //stores the calculated height for collectoion view used to show values listing
    var calacualtedMaxHeightOfCollection = Int()
    
    //stores the structured summary from the resultant summary(arrOfSOTResultSummary) of culture structure
    var arrForAllSummary = [ModelForAllSummary]()
    
    //stores result for motivation
    var arrOfMotivationData = [MotivationStructureGraphModel]()
    
    //stores Model of BarEntry for Motivation(third party for bar graph)
    var dataEntriesMotivation = [BarEntry]()
    
    //stores result for kudos
    var arrBubbles = [ReportsBeliefModel]()
    
    //stores cell height for kudos
    var heightForCell = CGFloat()
    
    //instance of line chart for engagement
    let engagementLineChartData = LineChartData()
    
    //stores engagement result
    var arrOfEngagementIndexResult = [DiagnosticResultModel]()
    
    //stores month name for engagement index
    var arrMonthsEngagement = [String]()
    
    //checks if beliefs section is expanded or collapse mode
    var isBeliefsExpanded = false
    
    //checks if team role section is expanded or collapse mode
    var isTeamRoleExpanded = false
    
    //checks if personality type section is expanded or collapse mode
    var isPersonailtyTypeExpanded = false
    
    //checks if motivation section is expanded or collapse mode
    var isMotivationExpanded = false
    
    //checks if kudos section is expanded or collapse mode
    var isBubbleExpanded = false
    
    //checks if engagement index section is expanded or collapse mode
    var isEngagementIndexExpanded = false
    
    //stores the staus of dot, its values are rated or not
    var isBeliefsAnswered = false
    
    //stores the staus of team role, its questions are completed or not
    var isTeamRoleAnswered = false
    
    //stores the staus of personality type, its questions are completed or not
    var isPersonailtyTypeAnswered = false
    
    //stores the staus of motivation, its questions are completed or not
    var isMotivationAnswered = false
    
    //no longer in use
    var isInitialResultPersonalityType = false
    var loadPersonalityResultText = false
    
    //stores team role json value
    var teamRoleJson = JSON()
    
    //stores personality type json value
    var personalityTypeJson = JSON()
    
    //stores personality type result
    var arrOfPersonalityTypeResult = [DiagnosticResultModel]()
    
    //stores Model of BarEntry for personality type(third party for bar graph)
    var dataEntriesPersonalityType = [BarEntry]()
    
    //checks if personality typ[e subcategory is displayed
    var isPersonalityTypeSubCategory = false
    
    //checks if personality typ[e subcategory is displayed
    var isPersonalityTypeFullResultView = false
    
    //stores persoality type sub category result
    var arrOfSubCategoryPersonalityTypeResult = [DiagnosticResultModel]()
    
    //stores Model of BarEntry for personality type sub category(third party for bar graph)
    var dataEntriesSubCategoryPersonalityType = [BarEntry]()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set drop down delegates
        dropDownUser.delegate = self
        
        //set organisation image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //load user list for drop down
        callWebServiceToGetUserList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        //set observer for updating notification count
        NotificationCenter.default.addObserver(self, selector: #selector(callWebServiceToGetUnreadCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        //load the members report
        callWebServiceToGetUsersReport()
        
        //get unread notification count
        callWebServiceToGetUnreadCount()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    /**
     * Generates users list for drop down
     */
    func CreateDropDownListUsers(){
        
        //        let value = ResponsiblePersonModel()
        //        self.arrOfUserList.insert(value, at: 0)
        
        //loop through the user list & get the logged in user id
        for (index,value) in arrOfUserList.enumerated() {
            if value.strId == AuthModel.sharedInstance.user_id {
                //if logged in user id
                //set it on first index and show it selected in drop down view
                arrOfUserList.swapAt(index, 0)
                selectedIndexOfUser = 0
                dropDownUser.title = value.strName
                break
            }
        }
        
        //loop through the user list
        for i in (0..<self.arrOfUserList.count) {
            //            if i == 0{
            //                self.arrOfUsersNameList.insert("ALL USERS", at: 0)
            //            }
            //            else{
            //prepare the user name array for drop down
            self.arrOfUsersNameList.insert(self.arrOfUserList[i].strName.capitalized, at: i)
            
            //            }
        }
        
        //set the array to items
        dropDownUser.items = arrOfUsersNameList
        
        //load user report according to filters applied
        callWebServiceToGetUsersReport()
    }
    
    /**
     * Calculate max height according to values, in beliefs sections
     */
    func CalculateMaxHeight(){
        //loop throught beliefs to get the values
        for i in (0..<self.arrBeliefs.count) {
            
            if i == 0 {
                //set count for values as height for collection
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
            }
            if calacualtedMaxHeightOfCollection < self.arrBeliefs[i].arrValues.count {
                //case when height is less than values count, that means we have another belief which has values more than the previous belief
                
                //so set its this values count as height
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                
                print(calacualtedMaxHeightOfCollection)
            }
            
        }
    }
    
    /**
     * This function is called when redo review button for teamrole is clicked
     */
    @objc func btnRedoTeamRoleAction(sender: UIButton) {
        
        if sender.tag == 80 {
            //Get the team role answers completed status
            callWebServiceForCOT()
        }
        else {
            //create instance for QuestionOptionView
            let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
            //        objVC.isQuestionDestributed = isQuestionDestributed
            //set it as edit mode
            objVC.isEditMode = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    /**
     * This function is called when redo review button for personality type is clicked
     */
    @objc func btnRedoReviewPersonalityTypeAction(_ sender: UIButton) {
        
        //        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
        //
        //        if sender.tag != 70 {
        //            objVC.comingFromReduReviewBtn = true
        //        }
        //        self.navigationController?.pushViewController(objVC, animated: true)
        
        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
        //pass this to detect update case or submit case
        objVC.isPersonalityTypeAnsDone = isPersonailtyTypeAnswered
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * This function is called when redo review button for motivation is clicked
     */
    @objc func btnRedoReviewMotivationAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
        //pass this to detect update case or submit case
        if self.isMotivationAnswered {
            objVC.comingFromReduReviewBtn = true
        }
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * This function is called when any of the personality type box is clicked
     * @param arr: sends the resultant array for selcted box
     */
    func didselectCollectionView(arr: [ModelForAllSummary]) {
        //store the array
        arrForAllSummary = arr
        self.tblView.reloadData()
        
        //no longer in use
        loadPersonalityResultText = true
    }
    
    /**
     * This function generates data enteries for motivation bar graph
     */
    func generateDataEntriesMotivation() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for motivation array
        for j in (0..<self.arrOfMotivationData.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfMotivationData[j].strScore)! / 60.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\(arrOfMotivationData[j].strScore /*valueArr[j]*/)", title: String(j + 1)/*titleArr[j]*/))
            
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of motivation
     */
    func setDataInColoumns(){
        
        if arrOfMotivationData.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfMotivationData {
                //prepare array of scores
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfMotivationData[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfMotivationData.removeAll()
        }
    }
    
    /**
     Returns the background colour according to there position
     */
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        case 6:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.4)
        case 7:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.3)
        case 8:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.2)
        case 9:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.15)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    /**
     Returns the first value which is not a balnk entry
     */
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        //if all are blank entries, then returns blank
        return ""
    }
    
    /***
     This function returns index(s) of a value from the array
     @strValue: This stores the vaue whose index needs to be found out
     @arrTemp: This is the temperory array which stores the value(strValue)
     @[String]: Returns the index(s) of the strValue
     */
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        return arrIndex
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of engagement index
     */
    func setDataInColoumnsEngagementIndex(){
        
        if arrOfEngagementIndexResult.count > 0 {
            
            var arrScores = [String]()
            
            for value in arrOfEngagementIndexResult {
                //prepare array of scores
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
                //                var strMaxIndex = 0
                
                //                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
                        //                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }
                
                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
                
                for value in arrAllIndexWithSameValue {
                    
                    tempValues[Int(value)!] = ""
                    
                    //provide colour to all the entries according to there max to min value i,e dark to light
                    self.arrOfEngagementIndexResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
        }
        else{
            self.arrOfEngagementIndexResult.removeAll()
        }
    }
    
    /**
     * Prepares data for engagement index line graph
     */
    func showLineGraphEngagement() {
        
        //array chart data(third party)
        var lineChartEntry = [ChartDataEntry]()
        
        var numArray = [Double]()
        
        for value in self.arrOfEngagementIndexResult {
            //prepare score array
            numArray.append(Double(   value.strScore == "" ? "0" : value.strScore)!)
        }
        
        //loop through score array
        for i in 0..<numArray.count {
            //prepare data for line chart enteries
            let value = ChartDataEntry(x: Double(i), y: Double(numArray[i]))
            lineChartEntry.append(value)
        }
        
        //set line appearance
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Engagement Index")
        line1.colors = [ColorCodeConstant.themeRedColor]
        //        line1.drawCirclesEnabled = false
        //        line1.drawValuesEnabled = false
        line1.circleColors = [ColorCodeConstant.themeRedColor]
        line1.drawVerticalHighlightIndicatorEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        
        //set data set
        engagementLineChartData.dataSets = [line1]
        //lineChartData.addDataSet(line1)
    }
    
    /**
     * This function generates data enteries for personality type bar graph
     */
    func generateDataEntriesPersonalitytype() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for personality type array
        for j in (0..<self.arrOfPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of Personality type
     */
    func setGraphDataPersonalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfPersonalityTypeResult {
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     This function generates data for the list view(table) which is displayed below bar graph of personality type sub category
     */
    func setGraphDataSubCategoryPeronalityType()
    {
        var arrScores = [String]()
        
        for value in arrOfSubCategoryPersonalityTypeResult {
            //prepare array of score
            arrScores.append(value.strScore)
        }
        
        var tempValues = arrScores
        //Max to min color handling
        for (mainIndex,_) in arrScores.enumerated()
        {
            var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
            //            var strMaxIndex = 0
            
            //            for (index,value) in tempValues.enumerated()
            for (_,value) in tempValues.enumerated()
            {
                if value == ""
                {
                    continue
                }
                if Float(value)! >= Float(strMaxValue)! {
                    strMaxValue = value
                    //                    strMaxIndex = index
                }
            }
            if strMaxValue == "" {
                break
            }
            
            let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)
            
            for value in arrAllIndexWithSameValue {
                
                tempValues[Int(value)!] = ""
                //provide colour to all the entries according to there max to min value i,e dark to light
                self.arrOfSubCategoryPersonalityTypeResult[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                
            }
            
        }
    }
    
    /**
     * This function generates data enteries for personality type sub category bar graph
     */
    func generateDataEntriesSubCategoryPersonalityType() -> [BarEntry] {
        
        //array of Model BarEntry(third party)
        var result: [BarEntry] = []
        
        //loop for personality type sub category array
        for j in (0..<self.arrOfSubCategoryPersonalityTypeResult.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfSubCategoryPersonalityTypeResult[j].strPercentage)! / 100.0
            
            //predefined function of bar graph to diplay data
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\( arrOfSubCategoryPersonalityTypeResult[j].strPercentage /*valueArr[j]*/)", title:  String(j + 1)/*titleArr[j]*/))
        }
        return result
    }
    
    // MARK: - IBActions
    /**
     * This function is called when we click side menu button.
     * It open ups the side menu
     */
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    /**
     * This function is called when we click profile button from nav.
     * It is no longer in use
     */
    @IBAction func btnProfileAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
        objVC.showMenu = false
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * This function is called when sections expand arrow is clicked
     */
    @IBAction func btnExpand(_ sender: UIButton) {
        
        //Tag 10 is for belief
        if sender.tag == 10 {
            isBeliefsExpanded = !isBeliefsExpanded
        }
            //Tag 20 is for team role
        else if sender.tag == 20 {
            isTeamRoleExpanded = !isTeamRoleExpanded
        }
            //Tag 30 is for personality type
        else if sender.tag == 30 {
            isPersonailtyTypeExpanded = !isPersonailtyTypeExpanded
        }
            //Tag 50 is for motivation
        else if sender.tag == 50 {
            isMotivationExpanded = !isMotivationExpanded
        }
            //Tag 60 is for kudos
        else if sender.tag == 60 {
            isBubbleExpanded = !isBubbleExpanded
        }
            //Tag 100 is for engagement index
        else if sender.tag == 100 {
            isEngagementIndexExpanded = !isEngagementIndexExpanded
        }
        
        tblView.reloadData()
    }
    
    /**
     * This function is called when actions button (center right corner, vertical button) is clicked
     */
    @IBAction func btnActionClicked() {
        
        //create instance of ViewActionsViewController
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ViewActionsViewController") as! ViewActionsViewController
        //pass org id
        objVC.strOrgId = AuthModel.sharedInstance.orgId
        //        objVC.arrOfficeDetail = arrOfficeDetail
        //navigate to actions listing page
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * It resets the personality type sub category graph to initial personality type graph
     */
    @objc func btnBackToPersonalityTypeGraphAction(_ sender: UIButton) {
        isPersonalityTypeSubCategory = false
        
        self.tblView.reloadData()
    }
    
    /**
    * It allows personality type graph view to come over
    */
    @objc func btnFullResultPersonalityTypeAction(_ sender: UIButton) {
        isPersonalityTypeFullResultView = true
        tblView.reloadData()
    }
    
    /**
    * It allows personality type dominating view to come over
    */
    @objc func btnBackToDominatingPersonalityTypeGraphAction(_ sender: UIButton) {
        isPersonalityTypeFullResultView = false
        tblView.reloadData()
    }
    
    //MARK: - HADropDown Delegate Method
    /**
     * Drop Down delegate methods
     * This function is called when user selects any value from dropdown
     */
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        print("Item selected at index \(index)")
        
        //set selected user index
        selectedIndexOfUser = index
        
        //load data according to filters applied
        callWebServiceToGetUsersReport()
    }
    
    //MARK: - BasicBarChartPeronalityTypeDelegate
    /**
     * BarChart Delegate Methods
     * It is called when user clicks any of personality type bar
     */
    func didSelectGraphPerosnalityType(index: Int) {
        print("iiiiiiiiiiiii",index)
        
        //set this to load personality type sub categories
        isPersonalityTypeSubCategory = true
        
        //load sub categories of personality type
        callWebServiceForSubCategoryPersonalityType(catID: arrOfPersonalityTypeResult[index].strCategoryId)
    }
    
    //MARK: - UITableViewDelegate and dataSource
    /**
     * Tableview delegate method used to return number of sections in table
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        if isBubbleExpanded {
            //case if kudos is expanded
            if arrBubbles.count > 0 {
                //when there are kudos persent
                //5 - for the sections other than kudos
                //arrBubbles.count - count of kudos
                return 5 + arrBubbles.count
            }
            else {
                //all sections
                return 6
            }
        }
        return 6
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //check for every section
        switch section {
        case 0: //case of engagement index section
            
            //check if engagement index is in expanded mode or not
            if isEngagementIndexExpanded {
                //when it is in expand mode
                
                if self.arrOfEngagementIndexResult.count == 0 {
                    //return single row for no data found if no result found for engagement index
                    return 1
                }
                else{
                    //arrOfEngagementIndexResult.count - result count to show the values below graph
                    //1 - to show the graph
                    return arrOfEngagementIndexResult.count + 1
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case 1: //case of beliefs section
            
            //check if beliefs section is in expanded mode or not
            if isBeliefsExpanded{
                //return a row if expanded
                return 1
            }
            else{
                //return no rows if section is in collapse mode
                return 0
            }
        case 2: //case of team role section
            
            //check if team role section is in expanded mode or not
            if isTeamRoleExpanded{
                
                //check if team role qusetions are answered by selected user
                if !isTeamRoleAnswered {
                    //check if the selected user is logged in user
                    if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user, then on row no record found & other for redo/review button
                        return 2
                    }
                    else {
                        //if not logged in user, show only the no record found
                        return 1
                    }
                }
                else{
                    //show result data
                    return 1
                    
                }
            }
            else{
                //return no rows if section is in collapse mode
                return 0
            }
        case 3: //case of personality type section
            /* OLD
             if isPersonailtyTypeExpanded {
             if isPersonailtyTypeAnswered {
             return 1 + arrForAllSummary.count
             }
             else {
             if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
             return 2
             }
             else {
             return 1
             }
             }
             }
             else {
             return 0
             }*/
            
            //check if personality type section is in expanded mode or not
            if isPersonailtyTypeExpanded {
                
                if isPersonalityTypeFullResultView {
                    //check if personality type sub category need to be listed
                    if isPersonalityTypeSubCategory {
                        //personality type sub category mode
                        
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user
                            if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                                return 2
                            }
                            else{
                                return arrOfSubCategoryPersonalityTypeResult.count + 2
                            }
                        }
                        else {
                            if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                                return 1
                            }
                            else{
                                return arrOfSubCategoryPersonalityTypeResult.count + 1
                            }
                        }
                    }
                    else {
                        //to show personality type result
                        if isPersonailtyTypeAnswered {
                            
                            if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                                //if logged in user as selected selected user
                                //arrOfPersonalityTypeResult.count - result count to show the values below graph
                                //1 - to show the graph
                                //1 - to show redo/review button
                                return arrOfPersonalityTypeResult.count + 2
                                
                            }
                            else {
                                //if not logged in user,
                                //arrOfPersonalityTypeResult.count - result count to show the values below graph
                                //1 - to show the graph
                                return arrOfPersonalityTypeResult.count + 1
                            }
                            
                            
                        }
                        else {
                            if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                                //if logged in user as selected selected user
                                //return a row for no data found if no result found for personality type & other one for redo/review question
                                return 2
                                
                            }
                            else {
                                //if not logged in user,
                                //return a row for no data found if no result found for personality type & other one for redo/review question
                                return 1
                                
                            }
                        }
                    }
                }
                else {
                    if isPersonailtyTypeAnswered {
                        return 1 + arrForAllSummary.count
                    }
                    else {
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                            return 2
                        }
                        else {
                            return 1
                        }
                    }
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        case 4: //case of motivation section
            
            //check if motivation is in expanded mode or not
            if isMotivationExpanded {
                
                //check if motivation qusetions are answered by selected user
                if !isMotivationAnswered {
                    //check if the selected user is logged in user
                    if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user, then on row no record found & other for redo/review button
                        return 2
                    }
                    else {
                        //if not logged in user, show only the no record found
                        return 1
                    }
                }
                else{
                    //check if the selected user is logged in user
                    if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user
                        //arrOfMotivationData.count - result count to show the values below graph
                        //1 - to show the graph
                        //1 - to show redo/review button
                        return arrOfMotivationData.count + 2
                    }
                    else {
                        //if selected user not the logged in one
                        //arrOfMotivationData.count - result count to show the values below graph
                        //1 - to show the graph
                        return arrOfMotivationData.count + 1
                    }
                }
            }
            else {
                //return no rows if section is in collapse mode
                return 0
            }
        default: //case of kudos section
            
            //check if kudos is in expanded mode or not
            if isBubbleExpanded {
                if arrBubbles.count > 0 {
                    //if there ae kudos return the count values
                    return  arrBubbles[section - 5].arrValues.count
                }
                else {
                    //return no record row
                    return 1
                }
            }
            //return no rows if section is in collapse mode
            return  0
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //switch case for every section
        switch indexPath.section {
        case 0: //case of engagement index section
            if self.arrOfEngagementIndexResult.count == 0 {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                //check for 1st index
                if indexPath.row == 0 {
                    //create custom cell for engagement index graph
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellLineEng") as! GraphTableViewCell
                    
                    //this sets the x axis with month names
                    cell.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values:arrMonthsEngagement)
                    
                    //set the line chart data
                    cell.lineChart.data = engagementLineChartData
                    
                    //set the x axis label(month names) postion to bottom
                    cell.lineChart.xAxis.labelPosition = .bottom
                    
                    //set right axis labels to disappear
                    cell.lineChart.rightAxis.labelTextColor = .clear
                    
                    //disable the zoom functionality for x/y axis
                    cell.lineChart.scaleXEnabled = false
                    cell.lineChart.scaleYEnabled = false
                    cell.selectionStyle = .none
                    
                    return cell
                }
                else {
                    //create custom cell for engagement index listing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    
                    //set the serial no
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set the title
                    cell.lblTitle.text = arrOfEngagementIndexResult[indexPath.row - 1].strTitle.capitalized
                    
                    //set the score
                    cell.lblScore.text = arrOfEngagementIndexResult[indexPath.row - 1].strScore.capitalized
                    
                    //set the background colour depending on the score
                    cell.lblScore.backgroundColor = arrOfEngagementIndexResult[indexPath.row - 1].bgColor
                    cell.selectionStyle = .none
                    
                    return cell
                }
            }
        case 1: //case of beliefs section
            if arrBeliefs.count == 0{
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
            else{
                //create custom cell for showing the vertical listing of values
                let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
                
                cell.selectionStyle = .none
                //set coming from reports to true
                cell.strComingFromReport = "True"
                
                //set array of beliefs
                cell.arrBeliefs = arrBeliefs
                
                //set max height of row
                cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
                
                //load the collection view(setting up of data in row is done inside the cell: ViewDOTCell)
                cell.collectionView.reloadData()
                return cell
            }
        case 2: //case of team role section
            
            //check if team role qusetions are answered by selected user
            if !isTeamRoleAnswered {
                
                //check for 1st index
                if indexPath.row == 0 {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                }
                else {
                    //return a redo/review button cell
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewTeamRole")
                    cell?.selectionStyle = .none
                    
                    //set redo/review button tag & target
                    let btn = cell?.viewWithTag(80) as! UIButton
                    btn.addTarget(self, action: #selector(btnRedoTeamRoleAction(sender:)), for: .touchUpInside)
                    
                    return cell!
                }
            }
            else{
                //create custom cell for showing team role result
                let cell : TeamRoleReportTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TeamRoleReportTableViewCell") as! TeamRoleReportTableViewCell
                
                cell.selectionStyle = .none
                //set json(setting up of data in row is done inside the cell: TeamRoleReportTableViewCell)
                cell.setDataForIndividual(response: teamRoleJson)
                
                if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                    //if logged in user as selected selected user, then show redo/review button & add its target
                    cell.btnRedoReview.addTarget(self, action: #selector(btnRedoTeamRoleAction(sender:)), for: .touchUpInside)
                    cell.btnRedoReview.isHidden = false
                }
                else {
                    //if selected user is not logged in user, then hide redo/review button & remove its target
                    cell.btnRedoReview.removeTarget(self, action: #selector(btnRedoTeamRoleAction(sender:)), for: .touchUpInside)
                    cell.btnRedoReview.isHidden = true
                }
                return cell
            }
        case 3: //case of personality type section
            /* OLD
             if !isPersonailtyTypeAnswered {
             if indexPath.row == 0 {
             let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
             cell?.selectionStyle = .none
             return cell!
             }
             else {
             let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
             cell?.selectionStyle = .none
             
             let btn = cell?.viewWithTag(70) as! UIButton
             btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
             
             return cell!
             }
             }
             else {
             if indexPath.row == 0 {
             let cell : PersonalityTypeIndividualTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PersonalityTypeIndividualTableViewCell") as! PersonalityTypeIndividualTableViewCell
             cell.selectionStyle = .none
             
             if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
             cell.isUser = true
             }
             else {
             cell.isUser = false
             }
             cell.obj = self
             if !loadPersonalityResultText {
             cell.setData(response: personalityTypeJson)
             }
             return cell
             
             }
             else {
             let cell : summaryTblCell = tableView.dequeueReusableCell(withIdentifier: "summaryTblCell") as! summaryTblCell
             cell.selectionStyle = .none
             if arrForAllSummary[indexPath.row - 1].strtitle == ""{
             //                        lblSummary.isHidden = true
             cell.lblSummaryTitle.isHidden = true
             cell.lblSummaryDescription.isHidden = true
             }
             else{
             //                        lblSummary.isHidden = false
             cell.lblSummaryTitle.isHidden = false
             cell.lblSummaryDescription.isHidden = false
             cell.lblSummaryTitle.text = arrForAllSummary[indexPath.row - 1].strtitle
             cell.lblSummaryDescription.attributedText = arrForAllSummary[indexPath.row - 1].strValue
             }
             
             if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
             cell.btnReview.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
             cell.heightconstraint.constant = 40.0
             }
             else {
             cell.btnReview.removeTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
             cell.heightconstraint.constant = 0.0
             }
             return cell
             }
             }*/
            
            
            if isPersonalityTypeFullResultView {
                //case of sub category mode
                if isPersonalityTypeSubCategory {
                    if indexPath.row == 0 {
                        //create custom cell for showing personality type sub category bar graph view
                        let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                        
                        //set up data entries
                        cell.basicBarChartForPersonalityType.dataEntries = dataEntriesSubCategoryPersonalityType
                        
                        //remove bar delegates
                        cell.basicBarChartForPersonalityType.delegate = nil
                        cell.selectionStyle = .none
                        
                        //show back button
                        cell.btnBack.isHidden = false
                        
                        //set target for back button
                        cell.btnBack.addTarget(self, action: #selector(btnBackToPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                        
                        //show back button
                        cell.btnBackToDominatingView.isHidden = false
                        
                        //set target for back button
                        cell.btnBackToDominatingView.addTarget(self, action: #selector(btnBackToDominatingPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    else if indexPath.row == arrOfSubCategoryPersonalityTypeResult.count + 1 {
                        //return a redo/review button cell
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                        cell?.selectionStyle = .none
                        
                        //set redo/review button tag & target
                        let btn = cell?.viewWithTag(70) as! UIButton
                        btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                        
                        return cell!
                    }
                    else {
                        //create custom cell for sub category listiing view below the graph
                        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                        cell.selectionStyle = .none
                        
                        //set serial no
                        cell.lblMarker.text = String(indexPath.row) + " -"
                        
                        //set tile
                        cell.lblTitle.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                        
                        //set percentage
                        cell.lblScore.text = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                        
                        //set the background colour depending on the score
                        cell.lblScore.backgroundColor = arrOfSubCategoryPersonalityTypeResult[indexPath.row - 1].bgColor
                        
                        return cell
                    }
                }
                else {
                    //to show personality type result
                    if isPersonailtyTypeAnswered {
                        
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                            //if logged in user as selected selected user
                            
                            if indexPath.row == 0 {
                                //create custom cell for showing personality type bar graph view
                                let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                                
                                //set the bar delegate
                                cell.basicBarChartForPersonalityType.delegate = self
                                
                                //set the graph data entries
                                cell.basicBarChartForPersonalityType.dataEntries = dataEntriesPersonalityType
                                cell.selectionStyle = .none
                                
                                //do not show back button
                                cell.btnBack.isHidden = true
                                
                                //show back button
                                cell.btnBackToDominatingView.isHidden = false
                                
                                //set target for back button
                                cell.btnBackToDominatingView.addTarget(self, action: #selector(btnBackToDominatingPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                                
                                return cell
                            }
                            else if indexPath.row == arrOfPersonalityTypeResult.count + 1 {
                                //return a redo/review button cell
                                let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                                cell?.selectionStyle = .none
                                
                                //set redo/review button tag & target
                                let btn = cell?.viewWithTag(70) as! UIButton
                                btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                                
                                return cell!
                            }
                            else {
                                //create custom cell for sub category listiing view below the graph
                                let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                                cell.selectionStyle = .none
                                
                                //set serial no.
                                cell.lblMarker.text = String(indexPath.row) + " -"
                                
                                //set title
                                cell.lblTitle.text = arrOfPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                                
                                //set percentage
                                cell.lblScore.text = arrOfPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                                
                                //set the background colour depending on the score
                                cell.lblScore.backgroundColor = arrOfPersonalityTypeResult[indexPath.row - 1].bgColor
                                
                                return cell
                            }
                            
                        }
                        else {
                            //if not logged in user,
                            if indexPath.row == 0 {
                                //create custom cell for showing personality type bar graph view
                                let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellPersonalityType") as! GraphTableViewCell
                                
                                //set the bar delegate
                                cell.basicBarChartForPersonalityType.delegate = self
                                
                                //set the graph data entries
                                cell.basicBarChartForPersonalityType.dataEntries = dataEntriesPersonalityType
                                cell.selectionStyle = .none
                                
                                //do not show back button
                                cell.btnBack.isHidden = true
                                
                                //show back button
                                cell.btnBackToDominatingView.isHidden = false
                                
                                //set target for back button
                                cell.btnBackToDominatingView.addTarget(self, action: #selector(btnBackToDominatingPersonalityTypeGraphAction(_:)), for: .touchUpInside)
                                
                                return cell
                            }
                            else {
                                //create custom cell for sub category listiing view below the graph
                                let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                                cell.selectionStyle = .none
                                
                                //set serial no.
                                cell.lblMarker.text = String(indexPath.row) + " -"
                                
                                //set title
                                cell.lblTitle.text = arrOfPersonalityTypeResult[indexPath.row - 1].strTitle.capitalized
                                
                                //set percentage
                                cell.lblScore.text = arrOfPersonalityTypeResult[indexPath.row - 1].strPercentage + "%"
                                
                                //set the background colour depending on the score
                                cell.lblScore.backgroundColor = arrOfPersonalityTypeResult[indexPath.row - 1].bgColor
                                
                                return cell
                            }
                        }
                    }
                    else {
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                            //if logged in user as selected selected user
                            if indexPath.row == 0 {
                                //return no record cell in case of no result
                                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                                cell?.selectionStyle = .none
                                return cell!
                                
                            }
                            else {
                                //return a redo/review button cell
                                let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                                cell?.selectionStyle = .none
                                
                                //set redo/review button tag & target
                                let btn = cell?.viewWithTag(70) as! UIButton
                                btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                                
                                return cell!
                            }
                        }
                        else {
                            //if not logged in user,
                            //return no record cell in case of no result
                            let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                            cell?.selectionStyle = .none
                            return cell!
                        }
                    }
                }
            }
            else {
                if !isPersonailtyTypeAnswered {
                    if indexPath.row == 0 {
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                        cell?.selectionStyle = .none
                        return cell!
                    }
                    else {
                        let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReviewPT")
                        cell?.selectionStyle = .none
                        
                        let btn = cell?.viewWithTag(70) as! UIButton
                        btn.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                        
                        return cell!
                    }
                }
                else {
                    if indexPath.row == 0 {
                        let cell : PersonalityTypeIndividualTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PersonalityTypeIndividualTableViewCell") as! PersonalityTypeIndividualTableViewCell
                        cell.selectionStyle = .none
                        
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                            cell.isUser = true
                            cell.btnFullResult.isHidden = false
                            cell.btnFullResult.addTarget(self, action: #selector(btnFullResultPersonalityTypeAction(_:)), for: .touchUpInside)

                        }
                        else {
                            cell.btnFullResult.isHidden = true
                            cell.isUser = false
                        }
                        cell.obj = self
                        if !loadPersonalityResultText {
                            cell.setData(response: personalityTypeJson)
                        }
                        
                        return cell
                        
                    }
                    else {
                        let cell : summaryTblCell = tableView.dequeueReusableCell(withIdentifier: "summaryTblCell") as! summaryTblCell
                        cell.selectionStyle = .none
                        if arrForAllSummary[indexPath.row - 1].strtitle == ""{
                            //                        lblSummary.isHidden = true
                            cell.lblSummaryTitle.isHidden = true
                            cell.lblSummaryDescription.isHidden = true
                        }
                        else{
                            //                        lblSummary.isHidden = false
                            cell.lblSummaryTitle.isHidden = false
                            cell.lblSummaryDescription.isHidden = false
                            cell.lblSummaryTitle.text = arrForAllSummary[indexPath.row - 1].strtitle
                            cell.lblSummaryDescription.attributedText = arrForAllSummary[indexPath.row - 1].strValue
                        }
                        
                        if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                            cell.btnReview.addTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                            cell.heightconstraint.constant = 40.0
                        }
                        else {
                            cell.btnReview.removeTarget(self, action: #selector(btnRedoReviewPersonalityTypeAction(_:)), for: .touchUpInside)
                            cell.heightconstraint.constant = 0.0
                        }
                        return cell
                    }
                }
            }
        case 4: //case of motivation section
            
            //check if motivation qusetions are answered by selected user
            if !self.isMotivationAnswered {
                
                //check for 1st index
                if indexPath.row == 0 {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                }
                else {
                    //return a redo/review button cell
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReview")
                    cell?.selectionStyle = .none
                    
                    //set redo/review button tag & target
                    let btn = cell?.viewWithTag(90) as! UIButton
                    btn.addTarget(self, action: #selector(btnRedoReviewMotivationAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
                
            }
            else{
                if indexPath.row == 0 {
                    //create custom cell for showing motivation result graph
                    let cell: GraphTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCellMotivation") as! GraphTableViewCell
                    
                    //set the graph data entries
                    cell.basicBarChartMotivation.dataEntries = dataEntriesMotivation
                    cell.selectionStyle = .none
                    return cell
                }
                else if indexPath.row == arrOfMotivationData.count + 1 {
                    //return a redo/review button cell
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "RedoReview")
                    cell?.selectionStyle = .none
                    
                    //set redo/review button tag & target
                    let btn = cell?.viewWithTag(90) as! UIButton
                    btn.addTarget(self, action: #selector(btnRedoReviewMotivationAction(_:)), for: .touchUpInside)
                    
                    return cell!
                }
                else {
                    //create custom cell for sub category listiing view below the graph
                    let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
                    
                    //set serial no.
                    cell.lblMarker.text = String(indexPath.row) + " -"
                    
                    //set title
                    cell.lblTitle.text = arrOfMotivationData[indexPath.row - 1].strTitle.capitalized
                    
                    //set percentage
                    cell.lblScore.text = arrOfMotivationData[indexPath.row - 1].strScore.capitalized + "%"
                    
                    //set the background colour depending on the percentage
                    cell.lblScore.backgroundColor = arrOfMotivationData[indexPath.row - 1].bgColor
                    cell.selectionStyle = .none
                    
                    return cell
                }
            }
        default: //case of kudos section
            
            //check if kudos is exapanded
            if isBubbleExpanded {
                
                //check if kudos array is not empty
                if arrBubbles.count > 0 {
                    //custom cell to return kudos row
                    let Cell : DOTReportsInnerTblViewCell = tableView.dequeueReusableCell(withIdentifier: "DOTReportsInnerTblViewCell") as! DOTReportsInnerTblViewCell
                    Cell.selectionStyle = .none
                    tblView.separatorStyle = .none
                    
                    if arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strRating == ""
                    {
                        //case when rating is empty, show only value name
                        Cell.lblValueName.text = arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strName.uppercased()
                    }
                    else{
                        //case when rating is not empty, show value name with its rating in circle brackets
                        Cell.lblValueName.text = arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strName.uppercased()  + "(" + arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strRating + ")"
                    }
                    
                    //set kudos count
                    Cell.lblLikes.text = arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strUpVotes
                    
                    //no longer in use
                    Cell.lblDislikes.text = arrBubbles[indexPath.section - 5].arrValues[indexPath.row].strDownVotes
                    
                    if indexPath.row == arrBubbles[indexPath.section - 5].arrValues.count - 1  {
                        //show custom seperator line when last row
                        Cell.lblBottomLine.isHidden = false
                        heightForCell = 55
                        
                    }
                    else{
                        //hide custom seperator line when last row
                        Cell.lblBottomLine.isHidden = true
                        heightForCell = 45
                        
                    }
                    return Cell
                }
                else {
                    //return no record cell in case of no result
                    let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                    cell?.selectionStyle = .none
                    return cell!
                }
                
            }
            else {
                //return no record cell in case of no result
                let cell  = tableView.dequeueReusableCell(withIdentifier: "NoRecord")
                cell?.selectionStyle = .none
                return cell!
            }
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0: //case of engagement index section
            if self.arrOfEngagementIndexResult.count == 0 {
                //return no record cell height
                return  50
            }
            else{
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        case 1:
            if arrBeliefs.count == 0{
                //return no record cell height
                return  50
            }
            else{
                //return the height for the vertical values listing
                return  110 + CGFloat( 90 * calacualtedMaxHeightOfCollection)
            }
        case 2:
            if isTeamRoleAnswered {
                if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                    //return height for team role cell in case when selected user is logged in one
                    return 530//610
                }
                else {
                    //return height for team role cell in case when selected user is not logged in one
                    return 470//550
                }
            }
            else {
                //return no record cell height
                return 50
            }
        case 3:
            /* OLD
             if isPersonailtyTypeAnswered {
             if indexPath.row == 0 {
             return 270
             }
             else {
             return UITableViewAutomaticDimension
             }
             }
             else {
             return 50
             }*/
            
            if isPersonalityTypeFullResultView {
                //to show personality type result
                if isPersonailtyTypeAnswered {
                    
                    if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user
                        if indexPath.row == 0 {
                            //return graph cell height
                            return 450
                        }
                        else if indexPath.row == arrOfPersonalityTypeResult.count + 1 {
                            //return redo/review button cell height
                            return 60
                        }
                        else {
                            //return list cell height
                            return 40
                        }
                    }
                    else {
                        //if not logged in user,
                        if indexPath.row == 0 {
                            //return graph cell height
                            return 450
                        }
                        else {
                            //return list cell height
                            return 40
                        }
                    }
                }
                else {
                    if selectedIndexOfUser != -1 && arrOfUserList[selectedIndexOfUser].strId == AuthModel.sharedInstance.user_id {
                        //if logged in user as selected selected user
                        //return a row for no data found if no result found for personality type & other one for redo/review question
                        if indexPath.row == 0 {
                            //return no record cell height
                            return 50
                        }
                        else {
                            //return redo/review button cell height
                            return 60
                        }
                        
                    }
                    else {
                        //if not logged in user,
                        //return no record cell height
                        return 50
                    }
                }
            }
            else {
                if isPersonailtyTypeAnswered {
                    if indexPath.row == 0 {
                        return 270
                    }
                    else {
                        return UITableViewAutomaticDimension
                    }
                }
                else {
                    return 50
                }
            }
            
        case 4:
            if !self.isMotivationAnswered {
                //return no record cell height
                return  50
            }
            else{
                if indexPath.row == 0 {
                    //return graph cell height
                    return 390
                }
                else if indexPath.row == arrOfMotivationData.count + 1 {
                    //return redo/review button cell height
                    return 60
                }
                else {
                    //return list cell height
                    return 40
                    
                }
            }
        default:
            if arrBubbles.count > 0 {
                //height of kudos row
                return heightForCell
            }
            else {
                //return no record cell height
                return 50
            }
        }
        
    }
    
    /**
     * Tableview delegate method used to return  section header view
     */
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //switch case for every section
        switch section {
        case 0:
            //return engagement index view
            return engagementIndexView
        case 1:
            //return beliefs view
            return DOTView
        case 2:
            //return team role view
            return teamRoleView
        case 3:
            //return personality type view
            return personalityTypeView
        case 4:
            //return motivation view
            return motivationView
        default: //return kudos view
            
            //create header view
            var headerView = UIView()//.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 55))
            
            //create label
            let label = UILabel()
            
            if !isBubbleExpanded || arrBubbles.count == 0 {
                //case when kudos in collapse mode
                headerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 55))
            }
            else if (section - 5) == 0 {
                //when 1st section od kudos
                headerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 105))
                
                label.frame = CGRect.init(x: 1, y: 60, width: headerView.frame.width - 2 , height: 45)
            }
            else {
                //other sections of kudos
                headerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 55))
                
                label.frame = CGRect.init(x: 1, y: 5, width: headerView.frame.width - 2 , height: headerView.frame.height-10)
            }
            
            if isBubbleExpanded && arrBubbles.count != 0 {
                //set custom data for custom sections, it has the kudos name
                label.text = "   " + arrBubbles[section - 5].strName.uppercased()
                label.textAlignment = .left
                label.backgroundColor = UIColor(hexString:"E9E9E8")
                label.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
                label.layer.borderWidth = 1.0
                label.numberOfLines = 0
                label.lineBreakMode = NSLineBreakMode.byWordWrapping
                label.font = UIFont.systemFont(ofSize: 17, weight: .bold)// my custom font
                label.textColor = ColorCodeConstant.darkTextcolor // my custom colour
            }
            
            if !isBubbleExpanded || arrBubbles.count == 0{
                bubbleView.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width , height: 50)
                headerView.addSubview(bubbleView)
            }
            else if (section - 5) == 0 {
                bubbleView.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width , height: 50)
                headerView.addSubview(bubbleView)
                headerView.addSubview(label)
            }
            else {
                headerView.addSubview(label)
            }
            
            //return the custom heafer view
            return headerView
        }
    }
    
    /**
     * Tableview delegate method used to return  height for section header view
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if !isBubbleExpanded{
            //for kudos section height
            return 50
        }
        if section >= 4 {
            //when kudos inner section
            if ((section - 5) == 0) && arrBubbles.count != 0{
                return 100
            }
        }
        
        //for all section height
        return 50
    }
    
    /**
     * Tableview delegate method used to return  height for section footer view
     */
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    //MARK: - Call Web service
    /**
     * API - get the unread notification count
     * Called but notification count is no longer used
     */
    @objc func callWebServiceToGetUnreadCount() {
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleUnReadNotifications, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["data"]["notificationCount"].stringValue == "0" {
                    self.lblUnredCount.layer.borderWidth = 0.0
                    self.lblUnredCount.text = " "
                }
                else {
                    self.lblUnredCount.layer.borderWidth = 1.0
                    self.lblUnredCount.text = response!["data"]["notificationCount"].stringValue
                }
            }
        }
    }
    
    /**
     * API - to get user report
     */
    func callWebServiceToGetUsersReport() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            [
                "orgId"   : AuthModel.sharedInstance.orgId, //set org id of logged in user
                "userId" : selectedIndexOfUser == -1 ? AuthModel.sharedInstance.user_id : arrOfUserList[selectedIndexOfUser].strId //selected user from dropdown
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getUserDashboardReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            if response == nil {
                //stop the loader
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                
                //show error message in case of no response
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    
                    //Beliefs & Values
                    AdminReportsParser.parseDOTDetailKnowOrg(response: response!, completionHandler: { (arrOfDOTReports, arrBeliefNames) in
                        
                        //set belief value to array
                        self.arrBeliefs = arrOfDOTReports
                        
                    })
                    
                    //chech if values are rated
                    self.isBeliefsAnswered = response!["data"]["userStatus"]["dotStatus"].boolValue
                    
                    //calculate height for the belief cell
                    self.CalculateMaxHeight()
                    
                    //TeamRole - set json for team role
                    self.teamRoleJson = response!["data"]["getCOTindividualSummary"]
                    
                    //set if user has answered team role questions or not
                    self.isTeamRoleAnswered = response!["data"]["userStatus"]["cotTeamRoleStatus"].boolValue
                    
                    //Personality Type
                    // OLD
//                     self.personalityTypeJson = response!["data"]["getCOTpersonalityType"]
//                     self.isPersonailtyTypeAnswered = response!["data"]["userStatus"]["cotPersonalityStatus"].boolValue
  
                    self.personalityTypeJson = response!["data"]["getCOTpersonalityTypeInitial"]
                    self.loadPersonalityResultText = false
                    
                    //set if user has answered personality type questions or not
                    self.isPersonailtyTypeAnswered = response!["data"]["userStatus"]["cotPersonalityStatus"].boolValue
                    
                    //parse personality type reports
                    DiagnosticParser.parseDiagnosticReportKnow(response: response!["data"]["getCOTpersonalityType"], completionHandler: { (arrOfDiagnosticResult) in
                        //set personality type result
                        self.arrOfPersonalityTypeResult = arrOfDiagnosticResult
                    })
                    
                    if self.arrOfPersonalityTypeResult.count == 0{
                        //empty personality type result values in case of no data
                        self.arrOfPersonalityTypeResult.removeAll()
                    }
                    else{
                        //set data entries for personality type
                        self.dataEntriesPersonalityType = self.generateDataEntriesPersonalitytype()
                    }
                    
                    //set up graph for personality type
                    self.setGraphDataPersonalityType()
                    
                    //reset subgraph
                    self.isPersonalityTypeSubCategory = false
                    self.isPersonalityTypeFullResultView = false
                    
                    //Motivation - parse result
                    SOTParser.parseSOTMotivationStructureDetailKnow(response: response!["data"]["getSOTmotivationUserList"], completionHandler: { (arrOfMotivationStructure) in
                        //set motivation result data
                        self.arrOfMotivationData = arrOfMotivationStructure
                    })
                    
                    if self.arrOfMotivationData.count > 0 {
                        //set data entries for motivation graph
                        self.dataEntriesMotivation = self.generateDataEntriesMotivation()
                    }
                    
                    //set if user has answered motivation questions or not
                    self.isMotivationAnswered = response!["data"]["userStatus"]["sotMotivationStatus"].boolValue
                    //set up motivation graoh
                    self.setDataInColoumns()
                    
                    //Bubble Report - parse kudos
                    ReportsParser.parseDOTReportsKnow(response: response!["data"]["getBubbleRatingList"], completionHandler: { (model) in
                        //set kudos result
                        self.arrBubbles = model.arrBelief
                        
                    })
                    
                    //Engagement - parse engagement index result
                    DiagnosticParser.parseCultureIndex(response: response!["data"]["userEngagementReport"], completionHandler: { (arrOfDiagnosticResult, arrMonths) in
                        
                        //set up engagement index data
                        self.arrOfEngagementIndexResult = arrOfDiagnosticResult
                        
                        //set up months data
                        self.arrMonthsEngagement = arrMonths
                    })
                    
                    if self.arrOfEngagementIndexResult.count == 0{
                        self.arrOfEngagementIndexResult.removeAll()
                    }
                    else{
                        //preapre data for engagement index line graph
                        self.showLineGraphEngagement()
                    }
                    
                    //set up data for engagement index
                    self.setDataInColoumnsEngagementIndex()
                    
                    self.isBeliefsExpanded = false
                    self.isTeamRoleExpanded = false
                    self.isPersonailtyTypeExpanded = false
                    self.isMotivationExpanded = false
                    self.isBubbleExpanded = false
                    //by default keep the engagement index open
                    self.isEngagementIndexExpanded = true
                    
                    self.tblView.reloadData()
                    
                    //stop the loader
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    
                    //stop loader
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                }
            }
        }
    }
    
    /**
     * API - get user list for drop down
     */
    func callWebServiceToGetUserList(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            [
                "type"   : "organisation",//type as organisation
                "typeId" : AuthModel.sharedInstance.orgId//pass logged in user org id
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getUserByType, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse result
                    DashboardParser.parseResponsiblePresonListForActionPage(response: response!, completionHandler: { (arrRespnsiblePreseonList) in
                        //set user list to array
                        self.arrOfUserList = arrRespnsiblePreseonList
                    })
                    
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        //Generates users list for drop down
                        self.CreateDropDownListUsers()
                    }
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    
    /**
     * Get team role answers completed by user result when redo/review button for team role is clicked
     */
    func callWebServiceForCOT() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.isCOTanswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    if response!["data"]["isCOTanswered"].boolValue == true {
                        //case when user is has already completed answers, show result view
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "IndividualViewForTeamRoleMap") as! IndividualViewForTeamRoleMap
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        //case when user has not completed answers so redirect to questions listisng screen
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - to get the value for personality type sub category
     * @param catID: personality type selected category  id
     */
    func callWebServiceForSubCategoryPersonalityType(catID: String){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =
            [
                "orgId"   : AuthModel.sharedInstance.orgId, //org id for logged in user
                "userId" : selectedIndexOfUser == -1 ? AuthModel.sharedInstance.user_id : arrOfUserList[selectedIndexOfUser].strId, //selected user id
                "categoryId" : catID //personality type category value, whose sub category need to be loaded
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getPersonalitytypeReportSubGraphUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse personality type sub category result
                    DiagnosticParser.parseDiagnosticReport(response: response!, completionHandler: { (arrOfPersonalityTypeResult) in
                        //set array for sub category
                        self.arrOfSubCategoryPersonalityTypeResult = arrOfPersonalityTypeResult
                        
                    })
                    if self.arrOfSubCategoryPersonalityTypeResult.count == 0{
                        
                        self.arrOfPersonalityTypeResult.removeAll()
                    }
                    else{
                        //set data entries for graph
                        self.dataEntriesSubCategoryPersonalityType = self.generateDataEntriesSubCategoryPersonalityType()
                    }
                    
                    //set up graph for sub category
                    self.setGraphDataSubCategoryPeronalityType()
                    
                    self.tblView.reloadData()
                }
                    
                else {
                    self.arrOfSubCategoryPersonalityTypeResult.removeAll()
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
