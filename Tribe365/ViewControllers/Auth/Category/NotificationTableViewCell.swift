//
//  NotificationTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 23/08/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
This class is used as notification cell
*/
class NotificationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    //title label
    @IBOutlet weak var lblTitle: UILabel!
    
    //description label
    @IBOutlet weak var lblDesc: UILabel!
    
    //date label
    @IBOutlet weak var lblDate: UILabel!
    
    //image type label
    @IBOutlet weak var imgType: UIImageView!

    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
