//
//  KudosChampPopUp.swift
//  Tribe365
//
//  Created by Apple on 22/01/20.
//  Copyright © 2020 chetaru. All rights reserved.
//

import UIKit

class KudosChampPopUp: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    //Used to show the user name
    @IBOutlet weak var lblUsername: UILabel!
    
    //Used to show the user email
    @IBOutlet weak var lblEmail: UILabel!
    
    //Used to show the user kudos count
    @IBOutlet weak var lblKudos: UILabel!
    
    //Used to show the user image
    @IBOutlet weak var imgUser: UIImageView!
    
    //view when there is single kudo champ
    @IBOutlet weak var singleView: UIView!
    
    //view when there is multiple kudos champ
    @IBOutlet weak var multiView: UIView!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    // MARK: - Variables
    //detect if there are single or multiple kudos champ
    var isMultiple = false
    
    //stores user name
    var strUsername = ""
    
    //stores user email
    var strEmail = ""
    
    //stores user kudos count
    var strKudos = ""
    
    //stores image of user
    var strImgUser = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set user name in upper case
        lblUsername.text = strUsername.uppercased()
        
        //set user email
        lblEmail.text = strEmail
        
        //set user image
        let url = URL(string: strImgUser)
        imgUser.kf.setImage(with: url, placeholder: UIImage(named: "user_default"))
        
        //set user kudos count
        lblKudos.text = strKudos
        
        if isMultiple {
            //show multiview when multiple kudos champ
            multiView.isHidden = false
        }
        else {
            //show singleview when there is one kudos champ
            singleView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //show navigation bar, when view disappears
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: - IBActions
    @IBAction func btnCrossAction(_ sender: Any) {
        //dismiss the popup
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - UITableView Delegate And DataSource
    /**
     * Tableview delegate method used to return number of sections in table
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //executed in multi user case, return the rows from multi user comma seperated names
        return strUsername.components(separatedBy: ",").count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom kudos cell
        let cell: KudosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "KudosTableViewCell")! as! KudosTableViewCell
        
        //set username from multiple comma seperated names
        cell.lblUsername.text = strUsername.components(separatedBy: ",")[indexPath.row].uppercased()
        
        //set email from multiple comma seperated emails
        cell.lblEmail.text = strEmail.components(separatedBy: ",")[indexPath.row]
        
        //set kudos count from multiple comma seperated kudos count
        cell.lblKudos.text = strKudos
        
        //set image from multiple comma seperated images
        let url = URL(string: strImgUser.components(separatedBy: ",")[indexPath.row])
        cell.imgUser.kf.setImage(with: url, placeholder: UIImage(named: "user_default"))
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
