//
//  ChoiceDesignationTableCell.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 31/05/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

/**
 * This cell returns when admin logs in and it is on the dashboard on admin
 */
class ChoiceDesignationTableCell: UITableViewCell {
    
    // MARK: - IBOutlets
    //for title
    @IBOutlet var lblDesignationTitle: UILabel!
    
    //for image
    @IBOutlet var imgDesignationTitle: UIImageView!
    
    //add button
    @IBOutlet var btnAdd: UIButton!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
