//
//  ChoiceDesignationView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 31/05/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import JKNotificationPanel

/**
 * This class is used as the admin dashboard class
 */
class ChoiceDesignationView: UIViewController, UITableViewDelegate,UITableViewDataSource{
    
    // MARK: - IBOutlets
    //table view
    @IBOutlet var tblView: UITableView!
    
    //sets title
    @IBOutlet weak var lblTitleName: UILabel!
    
    // MARK: - Variables
    //list titles to show
    var arrayForDesignationTitle = ["ORGANISATION", "STAFF", "REPORT", "PERFORMANCE"]
    
    //list images to show
    var arrayForDesignationImage = ["OrganisationtStaff","UserStaff","ReportStaff", "performance"]
    
    //to show error/warning
    var panel = JKNotificationPanel()
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.reloadData()
        
        //For Navigation bar tranparancy
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
    }
    
    // MARK: - IBActions
    
    override func viewWillAppear(_ animated: Bool) {
        
        //get title as name of admin from userdefaults
        let strTitleName = UserDefaults.standard.string(forKey: "name")
        print(strTitleName ?? "")
        if strTitleName == ""{
            //if userdefaults is empty get from shared instance and set welcome & name
            lblTitleName.text = "Welcome " + AuthModel.sharedInstance.name
            
        }
        else{
            //if userdefaults is not empty set welcome & name
            lblTitleName.text = "Welcome " + strTitleName!
            
        }
    }
    /**
     * This function is called when profile button is cliked
     */
    @IBAction func btnProfileAction(_ sender: UIButton) {
        
        if AuthModel.sharedInstance.role == kUser{
            //when user logged in redirect to user profile view
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }
        else{
            //when admin logged in redirect to admin profile view
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "SuperAdminProfileView") as! SuperAdminProfileView
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    /**
     * This function is called when plus(add) button is cliked for any row
     * @param _sender: it stores the row button clicked with its tag to detect which plus is being clicked
     */
    @objc func btnAddAction(_ sender: UIButton){
        print("\(sender)")
        
        switch sender.tag {
        case 0: //case when organisation add is clicked
            
            //redirect to add organisation screen
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddOrganisationView") as! AddOrganisationView
            
            self.navigationController?.pushViewController(objVC, animated: true)
        case 1: //case when staff add is clicked
            //redirect o organisation listing screen, as first need to select the organisation of staff to add
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CompanyView") as! CompanyView
            
            objVC.comingFromUser = "FromCellAddUser"
            self.navigationController?.pushViewController(objVC, animated: true)
            
        default:
            print("")
        }
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayForDesignationTitle.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell for ChoiceDesignationTableCell
        let cell : ChoiceDesignationTableCell = tableView.dequeueReusableCell(withIdentifier: "ChoiceDesignationTableCell") as! ChoiceDesignationTableCell
        
        //set title
        cell.lblDesignationTitle.text = arrayForDesignationTitle[indexPath.row]
        
        //set image
        cell.imgDesignationTitle.image = UIImage(named: arrayForDesignationImage[indexPath.row])
        
        //add tag to add button
        cell.btnAdd.tag = indexPath.row
        
        //add target to add button
        cell.btnAdd.addTarget(self, action: #selector(self.btnAddAction(_:)), for: .touchUpInside) //<- use `#selector(...)`
        if indexPath.row == 3 {
            
            //To remove seprator from particular cell
            tblView.separatorStyle = .none
            
            //hide add button for performance cell
            cell.btnAdd.isHidden = true
        }
        
        if indexPath.row == 2 {
            //hide add button for reports
            cell.btnAdd.isHidden = true
        }
        
        return cell
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    /**
     * Tableview delegate method called when user taps on any row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0: //case of organiation
            print(arrayForDesignationTitle[indexPath.row])
            
            //redirect to organisation listing
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CompanyView") as! CompanyView
            objVC.comingFromUser = "FALSE"
            self.navigationController?.pushViewController(objVC, animated: true)
            
        case 1: //case of staff
            print(arrayForDesignationTitle[indexPath.row])
            
            //redirect to organisation listing, so that we can show list of staff of specific organisation
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CompanyView") as! CompanyView
            
            objVC.comingFromUser = arrayForDesignationTitle[indexPath.row]
            self.navigationController?.pushViewController(objVC, animated: true)
            
        case 2: //case of reports
            print(arrayForDesignationTitle[indexPath.row])
            
            //redirect to organisation listing, so that we can show reports of specific organisation
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CompanyView") as! CompanyView
            
            objVC.comingFromUser = arrayForDesignationTitle[indexPath.row]
            self.navigationController?.pushViewController(objVC, animated: true)
            
            // self.panel.showNotify(withStatus: .warning , inView: self.appDelegate.window!, title: "Coming soon")
            
        case 3: //case of performance
            
            //redirect to performance screen
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "PerformanceViewController") as! PerformanceViewController
            self.navigationController?.pushViewController(objVC, animated: true)
            
        default:
            
            print(arrayForDesignationTitle[indexPath.row])
        }
        
    }
}
