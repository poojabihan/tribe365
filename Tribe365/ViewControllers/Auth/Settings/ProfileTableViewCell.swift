//
//  ProfileTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 11/09/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell is used in side menu profle row
 */
class ProfileTableViewCell: UITableViewCell {
    
    //for profile image
    @IBOutlet weak var imgProfile: UIImageView!
    
    //for user name
    @IBOutlet weak var lblName: UILabel!
    
    //for user email
    @IBOutlet weak var lblEmail: UILabel!
    
    //for option image
    @IBOutlet weak var imgOption: UIImageView!
    
    //for title
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
