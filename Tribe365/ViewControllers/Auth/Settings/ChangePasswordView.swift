//
//  ChangePasswordView.swift
//  Tribe365
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This is change passwrd view
 */
class ChangePasswordView: UIViewController,  UITextFieldDelegate {

    //MARK: - IBOutlets
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //current password text field
    @IBOutlet weak var txtCurrentPassword: UITextField!
    
    //new password text field
    @IBOutlet weak var txtNewPassword: UITextField!
    
    //confirm password text field
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    //MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //textfield instance
    var textField: UITextField?

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }

    
    //MARK: - IBActions
    /**
     * This function is called when user clicks on save button
     */
    @IBAction func btnSaveAction(_ sender: Any) {
        
        //validations
        if (txtCurrentPassword.text?.isEmpty)! {
            //mandatory current password
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter your current password.")
        }
        else if(checkForSpace(strCheckString: txtCurrentPassword.text!) ==  true){
            //no spaces allowed in current password
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if (txtNewPassword.text?.isEmpty)! {
            //manadatory new password
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password should not blank.")
        }
        else if(checkForSpace(strCheckString: txtNewPassword.text!) ==  true){
            //no spaces allowed in new password
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if txtConfirmPassword.text! != txtNewPassword.text! {
            //new & confirm password should match
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
        }
        
        
        else{
            //change the password called
            CallWebServiceToChangePassword();
            
        }
    }
    
    /**
     * custom function which checks the spaces in string and returns a bool accordingly
     */
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    /**
    * dismiss the view when user clicks cross button
    */
    @IBAction func btnCrossAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    //MARK: - Calling web service
    /**
    * API - change password
    */
    func CallWebServiceToChangePassword(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
       
        /*{
         "currentPassword":"123",
         "newPassword":"123"
         }
         */
        let param =
            [ "currentPassword":txtCurrentPassword.text!,//current passsword
              "newPassword": txtNewPassword.text!//new password
                ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.changePassword , param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message in case of update success
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Password update successfully.")
                    
                    //dismiss view
                    self.dismiss(animated: false, completion: nil)
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
    Notification which is called when keyboard opens
    */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
