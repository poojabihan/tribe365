//
//  ForgotPasswordView.swift
//  Tribe365
//
//  Created by Apple on 27/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import SwiftyJSON
import NVActivityIndicatorView

/**
 * This is forgot password screen
 */
class ForgotPasswordView: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    //email text
    @IBOutlet weak var txtEmail: UITextField!
    
    //MARK: - Variables
    //to show error message
    let panel = JKNotificationPanel()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hides keyboard when user taps anywhere on screen
        hideKeyboardWhenTappedAround()
        
        //set email textfield delegate
        txtEmail.delegate = self
    }
    
    //MARK: -IBAction
    /**
     * this function is called when usr clicks on submit button
     */
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        //validation - if user has entered valid email
        if (txtEmail.text?.isValidEmail() == false) {
            //show error message in case of invalid email id
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Valid Email.")
        }
        else {
            //if all vaidatin passes - call api for forgot pasword
            callWebServiceForForgotPassword()
        }
    }
    
    /**
     * This sends user back to previous screen
     */
    @IBAction func btnCancelAction(_ sender: Any) {
        //        dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITextfiled Delegete
    /**
     * This is textfield delegate which returns the keyboard
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - WebService Methods
    /**
     * API - For forgot password
     */
    func callWebServiceForForgotPassword() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)        
        
        //set param
        let param = ["email": txtEmail.text!  ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.forgotPassword, param: param, withHeader: false ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    //show success message when email is sent successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Mail has been sent successfully to you.")
                    
                    //dismiss the view
                    self.dismiss(animated: false, completion: nil)
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
