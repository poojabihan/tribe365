//
//  ProfileView.swift
//  Tribe365
//
//  Created by Apple on 27/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON
import Alamofire
import UserNotifications

/**
 * This class is user profile view class
 */
class ProfileView: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate{
    
    //MARK: - IBOutlets
    //update user image button
    @IBOutlet weak var btnEditProfile: UIButton!
    
    //first name field
    @IBOutlet var txtName: UITextField!
    
    //last name field
    @IBOutlet var txtLastName: UITextField!
    
    //email field
    @IBOutlet var txtEmail: UITextField!
    
    //contact no field
    @IBOutlet var txtContact: UITextField!
    
    //organisation name field
    @IBOutlet var txtOrganisationName: UITextField!
    
    //office name field
    @IBOutlet var txtOfficeName: UITextField!
    
    //dept name field
    @IBOutlet var txtDepartmentName: UITextField!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //image view
    @IBOutlet var imageView: UIImageView!
    
    //edit account info button
    @IBOutlet weak var editBtn: UIButton!
    
    //view with save & cancel button
    @IBOutlet weak var viewForSAveButtons: UIView!
    
    //view with logout & change password buttons
    @IBOutlet weak var viewOfOtherButtons: UIView!
    
    //team role value label
    @IBOutlet weak var lblTeamRole: UILabel!
    
    //personality type value label
    @IBOutlet weak var lblPersonalityType: UILabel!
    
    //motivation value label
    @IBOutlet weak var lblMotivation: UILabel!
    
    //office name button
    @IBOutlet var btnOfficeName: UIButton!
    
    //dept name button
    @IBOutlet var btnDepartmentName: UIButton!
    
    //menu button
    @IBOutlet var btnMenu: UIButton!
    
    //back button
    @IBOutlet var btnBack: UIButton!
    
    //org logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //image picker controller
    var imagePicker = UIImagePickerController()
    
    //to show error/warning message
    let panel = JKNotificationPanel()
    
    //textfield instance
    var textField: UITextField?
    
    //to keep whether to send image in api or not
    var doNotSendImage = Bool()
    
    //profile model instance
    var profileModel = UserProfileModel()
    
    //array of office
    var arrOffices = [OfficeModel]()
    
    //array of dept
    var arrDepartment = [DepartmentModel]()
    
    //to show mwnu or not
    var showMenu = true
    
    //stores if personality type questions are answered or not
    var isPersonalityTypeAns = false
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //set org image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //user image set circular
        imageView.layer.cornerRadius = 50
        
        //set its border red
        imageView.layer.borderColor = UIColor.red.cgColor
        
        //set border width
        imageView.layer.borderWidth = 1
        imageView.clipsToBounds = true
        
        //edit  image button to be disabled initially
        btnEditProfile.isUserInteractionEnabled = false
        
        btnEditProfile.isUserInteractionEnabled = false
        //Add Delegate for UIImagePicker
        imagePicker.delegate = self
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        //hides keyboard when user tap on screen
        self.hideKeyboardWhenTappedAround()
        
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //set all feilds to be disabled initially
        txtName.isUserInteractionEnabled = false
        txtLastName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = false
        btnOfficeName.isUserInteractionEnabled = false
        btnDepartmentName.isUserInteractionEnabled = false
        txtOfficeName.isUserInteractionEnabled = false
        txtDepartmentName.isUserInteractionEnabled = false
        
        doNotSendImage = true
        
        //load user details
        callWebServiceToGetUserDetail()
        
        if showMenu {
            //if show menu image then hide back button
            btnMenu.isHidden = false
            btnBack.isHidden = true
        }
        else {
            //if show back button image then hide menu button
            btnMenu.isHidden = true
            btnBack.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    //MARK: - IBActions
    /**
     * This function is called when we click side menu button.
     * It open ups the side menu
     */
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This is called when account information edit button is clicked
     */
    @IBAction func btnEditAction(_ sender: Any) {
        
        //enable all the fields
        btnEditProfile.isUserInteractionEnabled = true
        
        btnEditProfile.isUserInteractionEnabled = true
        viewForSAveButtons.isHidden = false
        viewOfOtherButtons.isHidden = true
        
        editBtn.isHidden = true
        
        txtName.isUserInteractionEnabled = true
        txtLastName.isUserInteractionEnabled = true
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = true
        btnOfficeName.isUserInteractionEnabled = true
        btnDepartmentName.isUserInteractionEnabled = true
        
    }
    
    /**
     * this function is called when save button is cliked
     */
    @IBAction func btnSaveAction(_ sender: Any) {
        
        //validation - name & email should not be empty
        if (txtName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter first name.")
        }
        else if (txtLastName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter last name.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
        else {
            //if all validation passes - update user profile
            callWebServiceToUpdateProfile()
        }
    }
    
    /**
     * this function is called when cancel button is clicked
     */
    @IBAction func btnCancelAction(_ sender: Any) {
        
        //disable all fields if user quits the edit mode
        btnEditProfile.isUserInteractionEnabled = false
        btnEditProfile.isUserInteractionEnabled = false
        viewOfOtherButtons.isHidden = false
        
        viewForSAveButtons.isHidden = true
        
        editBtn.isHidden = false
        
        txtName.isUserInteractionEnabled = false
        txtLastName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = false
        btnOfficeName.isUserInteractionEnabled = false
        btnDepartmentName.isUserInteractionEnabled = false
        
    }
    
    /**
     * this function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * this function is called when selects to logout
     */
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        
        //alert asking the confirmation for logout
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure Want to logout!", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            //if yes to logout, redirect to login screen
            self.goToLoginScreen()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     * This function is called when user wants to update his image
     */
    @IBAction func btnAddAction(_ sender: Any) {
        
        //alert to ask user whether he wants to update image from camera or gallery
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.doNotSendImage = false
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.doNotSendImage = false
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { _ in
            self.doNotSendImage = true
            // self.imageView.image = UIImage(named: "user_default")
        }))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /**
     * This function is called when user clicks on office name
     */
    @IBAction func btnOfficeAction(_ sender: Any) {
        
        //show popup for office listing, so that user can choose any one office
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ActionTierListView") as! ActionTierListView
        
        //pass office array
        objVC.arrOfSlectedTierData = self.arrOffices
        
        //pass segue identifier
        objVC.strSegueIdentifier = "ResponsibleClick"
        
        //pass the type
        objVC.strWhomeCalled = "OFFICE"
        objVC.modalPresentationStyle = .overCurrentContext
        
        objVC.didFinishSelectTier = { dict in
            //callback method
            //to adjust office parameters
            if dict is OfficeModel {
                //set selected office details
                self.txtOfficeName.text = (dict as! OfficeModel).strOffice
                self.profileModel.strOfficeId = (dict as! OfficeModel).strOffice_id
                self.profileModel.strOfficeName = (dict as! OfficeModel).strOffice
            }
            
        }
        self.navigationController?.present(objVC, animated: false, completion: nil)
        
    }
    
    /**
     * This function is called when user clicks on dept name
     */
    @IBAction func btnDeptAction(_ sender: Any) {
        
        //show popup for dept listing, so that user can choose any one dept
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ActionTierListView") as! ActionTierListView
        
        //pass dept array
        objVC.arrOfSlectedTierData = self.arrDepartment
        
        //pass segue identifier
        objVC.strSegueIdentifier = "ResponsibleClick"
        
        //pass the type
        objVC.strWhomeCalled = "DEPARTMENT"
        objVC.modalPresentationStyle = .overCurrentContext
        
        objVC.didFinishSelectTier = { dict in
            
            //to adjust office parameters
            if dict is DepartmentModel {
                //callback method
                
                //set selected dept details
                self.txtDepartmentName.text = (dict as! DepartmentModel).strDepartment
                self.profileModel.strDepartmentId = (dict as! DepartmentModel).strId
                self.profileModel.strDepartmentName = (dict as! DepartmentModel).strDepartment
            }
            
        }
        self.navigationController?.present(objVC, animated: false, completion: nil)
        
    }
    
    /**
     * This function is called when user clicks on team role red arrow
     */
    @IBAction func btnTeamRoleAction(_ sender: Any) {
        //load team role details
        callWebServiceForTeamRole()
    }
    
    /**
     * This function is called when user clicks on personality type red arrow
     */
    @IBAction func btnPersonalityTypeAction(_ sender: Any) {
//        callWebServiceCheckQuestionsForPersonalityType()
        
//        if isPersonalityTypeAns {
//            //case when user has completed the answers, show result view
//            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeResultView") as! PersonalityTypeResultView
//            objVC.isPersonailtyTypeAnswered = isPersonalityTypeAns
//            self.navigationController?.pushViewController(objVC, animated: true)
//        }
//        else {
//            //case when user has not completed the answers, show questions view
//            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
//            objVC.isPersonalityTypeAnsDone = isPersonalityTypeAns
//            self.navigationController?.pushViewController(objVC, animated: true)
//        }
        
        
        if isPersonalityTypeAns {
            //if personality type questions answered already - move to result view
            let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensView") as! FunctionalLensView
            
            //pass org id
            objVC.strOrgID = AuthModel.sharedInstance.orgId
            
            //pass userid
            objVC.strUserID = AuthModel.sharedInstance.user_id
            
            //pass type
            objVC.strMyType = "Indi"
            
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }
        else {
            let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
            objVC.isPersonalityTypeAnsDone = false //isPersonalityTypeAns
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    /**
     * This function is called when user clicks on motivation red arrow
     */
    @IBAction func btnMotivationAction(_ sender: Any) {
        //load motivation details
        callWebServiceCheckQuestionsForMotivation()
    }
    
    //MARK: - Custom Functions
    /**
     *This function opens camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //when type is camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *This function opens gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    // convert images into base64 and keep them into string
    /**
     *This function is used to convert image to base64 format
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imageView.image!, 1.0)!
        if let imageData = imageView.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     * ImagePicker delegate method called when user selects any image from Camera/Gallery
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        //get original image selected
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            //assign image to image view
            imageView.image = image
            
            //dismiss the picker
            self.dismiss(animated: false, completion: nil)
        }
            
        else{
            //show error if image not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
        
        
    }
    
    /**
     * ImagePicker delegate method called when user cancels  image selection view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    /**
     * This function logouts user
     */
    func goToLoginScreen(){
        
        //14Nov
        //logout
        self.callWebServiceToLogoutFromTribe()
        //        let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
        //
        //        let url = kBaseURLForIOT + NetworkConstant.Logout.logout// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
        //
        //        requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)
        
    }
    
    //logout from tribe 1st after this logout from tellsid
    func callWebServiceToLogoutFromTribe() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Logout.tribeLogout, param: [:], withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
                
            else{
                if response!["status"].boolValue == true {
                    
                    /*{
                     "code": 200,
                     "status": true,
                     "service_name": "user-logout",
                     "message": "successfully logout"
                     }*/
                    print("SUCCESS Add")
                    
                    //Call tellsid logout web service
                    //                    let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
                    //
                    //                    let url = kBaseURLForIOT + NetworkConstant.Logout.logout// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
                    //
                    //                    self.requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)
                    
                    //store stoken
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    //remove everything from shared instance
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    //                    let badgeCount: Int = 0
                    //                    let application = UIApplication.shared
                    //                    let center = UNUserNotificationCenter.current()
                    //                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    //                        // Enable or disable features based on authorization.
                    //                    }
                    //                    application.registerForRemoteNotifications()
                    //                    application.applicationIconBadgeNumber = badgeCount
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    //redirect to login screen
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login")// as! ViewController
                    
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
                    //remove all usr defaults
                    UserDefaults.standard.removeObject(forKey: "SavedDictForDiagnostic")
                    UserDefaults.standard.removeObject(forKey: "isSaved")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForTribeometer")
                    UserDefaults.standard.removeObject(forKey: "isSavedTribeometer")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForFC")
                    UserDefaults.standard.removeObject(forKey:
                        "isSavedFC")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForCuturalStructure")
                    UserDefaults.standard.removeObject(forKey: "isSavedForCuturalStructure")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForMotivation")
                    UserDefaults.standard.removeObject(forKey: "isSavedMotivation")
                    
                    //how success logout messsage
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * No longer in use
     */
    func requestToLogoutFromtellSid(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    //                    let badgeCount: Int = 0
                    //                    let application = UIApplication.shared
                    //                    let center = UNUserNotificationCenter.current()
                    //                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    //                        // Enable or disable features based on authorization.
                    //                    }
                    //                    application.registerForRemoteNotifications()
                    //                    application.applicationIconBadgeNumber = badgeCount
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
                    
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForDiagnostic")
                    UserDefaults.standard.removeObject(forKey: "isSaved")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForTribeometer")
                    UserDefaults.standard.removeObject(forKey: "isSavedTribeometer")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForFC")
                    UserDefaults.standard.removeObject(forKey:
                        "isSavedFC")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForCuturalStructure")
                    UserDefaults.standard.removeObject(forKey: "isSavedForCuturalStructure")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForMotivation")
                    UserDefaults.standard.removeObject(forKey: "isSavedMotivation")
                    
                    
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                    
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    //MARK: - Calling Web service
    /**
     * API - Loads user details
     */
    func callWebServiceToGetUserDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //store whether user has completed personality type questions or not
                    self.isPersonalityTypeAns = response!["data"]["perTypeStatus"].boolValue
                    
                    //parse result
                    DashboardParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        
                        //set org name
                        self.txtOrganisationName.text = model.strOrganisationName
                        
                        //set office name & update shared instance
                        self.txtOfficeName.text = model.strOfficeName
                        AuthModel.sharedInstance.office = model.strOfficeName
                        
                        //set dept name & update shared instance
                        self.txtDepartmentName.text = model.strDepartmentName
                        AuthModel.sharedInstance.department = model.strDepartmentName
                        
                        //set model
                        self.profileModel = model
                        
                        //set team role result value
                        self.lblTeamRole.text = "  " + model.strTeamRole.replacingOccurrences(of: ",", with: "\n ")
                        
                        //set personality type result value
//                        if self.isPersonalityTypeAns {
                        self.lblPersonalityType.text = " " + model.strPersonalityType.replacingOccurrences(of: ",", with: "\n ")
//                        }
//                        else {
//                            self.lblPersonalityType.text = "You have not submitted your answers yet."
//                        }
                        
                        //set motivation result value
                        self.lblMotivation.text = "  " + model.strMotivation.replacingOccurrences(of: ",", with: "\n ")
                        
                        //set user image & update shared instance
                        let url = URL(string:model.strProfileImage)
                        self.imageView.kf.setImage(with: url, placeholder: UIImage(named :"user_default"), progressBlock:nil, completionHandler: nil)
                        //  self.imageView.kf.setImage(with: url)
                        AuthModel.sharedInstance.profileImageUrl = model.strProfileImage
                        
                        //set first name & update shared instance
                        self.txtName.text = model.strName
                        AuthModel.sharedInstance.name = model.strName
                        
                        //set last name & update shared instance
                        self.txtLastName.text = model.strLastName
                        AuthModel.sharedInstance.lastName = model.strLastName
                        
                        //set email & update shared instance
                        self.txtEmail.text = model.strEmail
                        AuthModel.sharedInstance.email = model.strEmail
                        
                        //set contact & update shared instance
                        self.txtContact.text = model.strUserContact
                        
                    })
                    
                    //load all office & depts
                    self.callWebServiceToGetDepartmentAndOfficeList()
                    
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - update profile
     */
    func callWebServiceToUpdateProfile(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //set parms
        var param = [String : Any]()
        if doNotSendImage == true{
            param =
                [  "name":txtName.text!,//set name
                    "lastName":txtLastName.text!,//set name
                    "email":txtEmail.text!,//set email
                    "contact":txtContact.text ?? "",//contact no
                    "profileImage": "",//case when no image selected
                    "officeId": profileModel.strOfficeId,//selected office id
                    "departmentId": profileModel.strDepartmentId//selected dept id
            ]
            
        }
        else{
            
            param =
                [  "name":txtName.text!,//set name
                    "lastName":txtLastName.text!,//set name
                    "email":txtEmail.text!,//set email
                    "contact":txtContact.text ?? "",//set contact
                    "profileImage": convertImageToBase64(image: imageView.image!),//set profile image as base64
                    "officeId": profileModel.strOfficeId,//selected office id
                    "departmentId": profileModel.strDepartmentId//selected dept id
            ]
        }
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateUserProfile , param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse user profile
                    DashboardParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        
                        //set updated org name
//                        self.txtOrganisationName.text = model.strOrganisationName
                        
                        //set updated office name & update shared instance too
                        self.txtOfficeName.text = model.strOfficeName
                        AuthModel.sharedInstance.office = model.strOfficeName
                        
                        //set updated dept name & update shared instance too
                        self.txtDepartmentName.text = model.strDepartmentName
                        AuthModel.sharedInstance.department = model.strDepartmentName
                        
                        //set model
                        self.profileModel = model
                        
//                        //set updated team role result value
//                        self.lblTeamRole.text = "  " + model.strTeamRole.replacingOccurrences(of: ",", with: "\n ")
//
//                        //set updated personality type result value
//                        self.lblPersonalityType.text = "  " + model.strPersonalityType.replacingOccurrences(of: ",", with: "\n ")
//
//                        //set updated motivation result value
//                        self.lblMotivation.text = "  " + model.strMotivation.replacingOccurrences(of: ",", with: "\n ")
                        
                        //set updated first name
                        self.txtName.text = model.strName
                        AuthModel.sharedInstance.name = model.strName
                        
                        //set updated last name
                        self.txtLastName.text = model.strLastName
                        AuthModel.sharedInstance.lastName = model.strLastName
                        
                        //set updated email
//                        self.txtEmail.text = model.strEmail
//                        AuthModel.sharedInstance.email = model.strEmail
                        
                        //set updated contact no
                        self.txtContact.text = model.strUserContact
                        
                        //set updated image
                        let url = URL(string:model.strProfileImage)
                        self.imageView.kf.setImage(with: url, placeholder: UIImage(named :"user_default"), progressBlock:nil, completionHandler: nil)
                        AuthModel.sharedInstance.profileImageUrl = model.strProfileImage
                        
                    })
                    
                    //disable the state for every field
                    self.btnEditProfile.isUserInteractionEnabled = false
                    self.editBtn.isHidden = false
                    self.viewOfOtherButtons.isHidden = false
                    self.viewForSAveButtons.isHidden = true
                    self.txtName.isUserInteractionEnabled = false
                    self.txtLastName.isUserInteractionEnabled = false
                    self.txtEmail.isUserInteractionEnabled = false
                    self.txtContact.isUserInteractionEnabled = false
                    
                    //show success message
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Your profile update successfully")
                    
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - To get all office along with there respective departments
     */
    func callWebServiceToGetDepartmentAndOfficeList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["orgId": profileModel.strOrganisationId  ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse office and departments
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        
                        //store office array
                        self.arrOffices = arrOffice
                        
                        //store dept array
                        self.arrDepartment = arrDept
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * Get team role answers completed by user result
     */
    func callWebServiceForTeamRole() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.isCOTanswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    if response!["data"]["isCOTanswered"].boolValue == true {
                        //case when user is has already completed answers, show result view
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "IndividualViewForTeamRoleMap") as! IndividualViewForTeamRoleMap
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        //case when user has not completed answers so redirect to questions listisng screen
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * Get personality type answers completed by user
     */
    func callWebServiceCheckQuestionsForPersonalityType() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.FunctionalLens.questionsDistributedToUser, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS abi vala ",response ?? "")
                    
                    if response!["data"]["isCOTfunclensAnswersDone"].boolValue{
                        //if personality type questions answered already - move to result view
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensView") as! FunctionalLensView
                        
                        //pass org id
                        objVC.strOrgID = AuthModel.sharedInstance.orgId
                        
                        //pass userid
                        objVC.strUserID = AuthModel.sharedInstance.user_id
                        
                        //pass type
                        objVC.strMyType = "Indi"
                        
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    else {
                        //if personality type questions not answered - move to question view
//                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
//                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                        //case when user has not completed the answers, show questions view
                        let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "PersonalityTypeClickView") as! PersonalityTypeClickView
                        objVC.isPersonalityTypeAnsDone = false //isPersonalityTypeAns
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * Get motivation answers completed by user
     */
    func callWebServiceCheckQuestionsForMotivation() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.MotivationStructure.isSOTMotivationAnswersDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS abi vala ",response ?? "")
                    
                    if response!["data"]["isUserAnswersFilled"].boolValue{
                        
                        /*"SOTmotivationStartDate": "2018-11-05 00:00:00",
                         "isSOTmotivationMailSent": "1",
                         "isUserAnswersFilled": "0"
                         */
                        
                        //if answered already, redirect to result view
                        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationStructureGraphView") as!  MotivationStructureGraphView
                        //pass org id
                        objVC.strOrgID = AuthModel.sharedInstance.orgId
                        
                        //pass user id
                        objVC.strUserID = AuthModel.sharedInstance.user_id
                        
                        //pass type
                        objVC.strMyType = "Indi"
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    else {
                        if AuthModel.sharedInstance.role == kUser{
                            //if not answered redirect to question answer view
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                            
                            self.navigationController?.pushViewController(objVC, animated: true)
                            
                        }
                        
                    }
                    
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
