//
//  SuperAdminProfileView.swift
//  Tribe365
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import NVActivityIndicatorView
import JKNotificationPanel
import Alamofire
import SwiftyJSON

/**
 * This class is admin profile view class
 */
class SuperAdminProfileView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    //for name
    @IBOutlet var txtName: UITextField!
    
    //for email
    @IBOutlet var txtEmail: UITextField!
    
    //for contact
    @IBOutlet var txtContact: UITextField!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //image view
    @IBOutlet var imageView: UIImageView!
    
    //account info edit button
    @IBOutlet weak var editBtn: UIButton!
    
    //view with save and cancel buttons
    @IBOutlet weak var viewForSAveButtons: UIView!
    
    //view with logout and change password buttons
    @IBOutlet weak var viewOfOtherButtons: UIView!
    
    //image edit button
    @IBOutlet weak var btnEditProfile: UIButton!
    
    //MARK: - Variable
    //image picker controller
    var imagePicker = UIImagePickerController()
    
    //to show error/warning message
    let panel = JKNotificationPanel()
    
    //textfield instance
    var textField: UITextField?
    
    //to keep whether to send image in api or not
    var doNotSendImage = Bool()
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //user image set circular
        imageView.layer.cornerRadius = 50
        
        //set its border red
        imageView.layer.borderColor = UIColor.red.cgColor
        
        //set its border width
        imageView.layer.borderWidth = 1
        imageView.clipsToBounds = true
        btnEditProfile.isUserInteractionEnabled = false
        
        btnEditProfile.isUserInteractionEnabled = false
        //Add Delegate for UIImagePicker
        imagePicker.delegate = self
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(SuperAdminProfileView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SuperAdminProfileView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        //hides keyboard when user tap on screen
        self.hideKeyboardWhenTappedAround()
        
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //set all feilds to be disabled initially
        txtName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = false
        
        doNotSendImage = true
        
        //load user details
        callWebServiceToGetUserDetail()
    }
    
    //MARK: - IBAction
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This is called when account information edit button is clicked
     */
    @IBAction func btnEditAction(_ sender: Any) {
        
        //enable all the fields
        btnEditProfile.isUserInteractionEnabled = true
        
        btnEditProfile.isUserInteractionEnabled = true
        viewForSAveButtons.isHidden = false
        viewOfOtherButtons.isHidden = true
        
        editBtn.isHidden = true
        
        txtName.isUserInteractionEnabled = true
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = true
        
        
    }
    
    /**
     * this function is called when save button is cliked
     */
    @IBAction func btnSaveAction(_ sender: Any) {
        
        //validation - name & email should not be empty
        if (txtName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter name.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
            
            /* else if (txtContact.text?.isEmpty)! {
             self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter contact number.")
             }*/
            
        else {
            //if all validation passes - update user profile
            callWebServiceToUpdateProfile()
        }
    }
    
    /**
     * this function is called when cancel button is clicked
     */
    @IBAction func btnCancelAction(_ sender: Any) {
        
        //disable all fields if user quits the edit mode
        btnEditProfile.isUserInteractionEnabled = false
        btnEditProfile.isUserInteractionEnabled = false
        viewOfOtherButtons.isHidden = false
        
        viewForSAveButtons.isHidden = true
        
        editBtn.isHidden = false
        
        txtName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtContact.isUserInteractionEnabled = false
    }
    
    /**
     * this function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * this function is called when selects to logout
     */
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        
        //alert asking the confirmation for logout
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure Want to logout!", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            //if yes to logout, redirect to login screen
            self.goToLoginScreen()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     * This function is called when user wants to update his image
     */
    @IBAction func btnAddAction(_ sender: Any) {
        
        //alert to ask user whether he wants to update image from camera or gallery
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.doNotSendImage = false
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.doNotSendImage = false
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { _ in
            self.doNotSendImage = true
            //            self.imageView.image = UIImage(named: "user_default")
        }))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    //MARK: - Custom Functions
    
    /**
     *This function opens camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //when type is camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *This function opens gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    // convert images into base64 and keep them into string
    /**
     *This function is used to convert image to base64 format
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imageView.image!, 1.0)!
        if let imageData = imageView.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     * ImagePicker delegate method called when user selects any image from Camera/Gallery
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        //get original image selected
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            //assign image to image view
            imageView.image = image
            
            //dismiss the picker
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //show error if image not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     * ImagePicker delegate method called when user cancels  image selection view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    /**
     * No longer in use
     */
    func requestToLogoutFromtellSid(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login")// as! ViewController
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                    
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    /**
     * Clears all user data
     */
    func goToLoginScreen(){
        
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
        
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
        print(strFcmToken)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()
        
        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
        
        /*let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
         
         let url = kBaseURLForIOT + NetworkConstant.Logout.logout
         
         // "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
         
         requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)
         */
        
        /* let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
         let defaults = UserDefaults.standard
         defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
         
         let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
         let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
         
         let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
         print(strFcmToken)
         UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
         appDel.window!.rootViewController = centerVC
         appDel.window!.makeKeyAndVisible()*/
        
    }
    //MARK: - CallWEbservice for tellsid
    //MARK: - Calling Web service
    /**
     * API - Loads user details
     */
    func callWebServiceToGetUserDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse user profile
                    DashboardParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        //set name
                        self.txtName.text = model.strName
                        
                        //set email
                        self.txtEmail.text = model.strEmail
                        
                        //set constact no.
                        self.txtContact.text = model.strUserContact
                        
                        //set user image
                        let url = URL(string:model.strProfileImage)
                        self.imageView.kf.setImage(with: url, placeholder: UIImage(named :"user_default"), progressBlock:nil, completionHandler: nil)
                        
                        UserDefaults.standard.set(model.strName, forKey: "name") //setObject
                    })
                    
                    
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - update profile
     */
    func callWebServiceToUpdateProfile(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //set parms
        var param = [String : Any]()
        if doNotSendImage == true{
            param =
                [  "name":txtName.text!,//set name
                    "email":txtEmail.text!,//set email
                    "contact":txtContact.text ?? "",//set contact
                    "profileImage": ""//case when no image selected
            ]
            
        }
        else{
            
            param =
                [  "name":txtName.text!,//set name
                    "email":txtEmail.text!,//set email
                    "contact":txtContact.text ?? "",//set contact no.
                    "profileImage": convertImageToBase64(image: imageView.image!)//set profile image as base64
            ]
        }
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateUserProfile , param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse user profile
                    DashboardParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        
                        //set updated name
                        self.txtName.text = model.strName
                        
                        //set updated email
                        self.txtEmail.text = model.strEmail
                        
                        //set updated constact
                        self.txtContact.text = model.strUserContact
                        
                        //set updated image
                        let url = URL(string:model.strProfileImage)
                        self.imageView.kf.setImage(with: url, placeholder: UIImage(named :"user_default"), progressBlock:nil, completionHandler: nil)
                        
                        UserDefaults.standard.set(model.strName, forKey: "name") //setObject
                        
                        
                    })
                    
                    //disable the state for every field
                    self.btnEditProfile.isUserInteractionEnabled = false
                    self.editBtn.isHidden = false
                    self.viewOfOtherButtons.isHidden = false
                    self.viewForSAveButtons.isHidden = true
                    self.txtName.isUserInteractionEnabled = false
                    self.txtEmail.isUserInteractionEnabled = false
                    self.txtContact.isUserInteractionEnabled = false
                    
                    //show success message
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Your profile update successfully")
                    
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
