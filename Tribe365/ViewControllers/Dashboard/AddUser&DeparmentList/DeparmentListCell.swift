//
//  DeparmentListCell.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 15/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

/**
* This is cell class used to show the department  list rows .
*/
class DeparmentListCell: UITableViewCell {

    //department name
    @IBOutlet var lblDepatmentTitle: UILabel!
    
    //checbox
    @IBOutlet var btnCheckedUnChecked: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
