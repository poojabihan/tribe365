//
//  DeparmentsList.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 15/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//
import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class used to show the department  list .
 */
class DeparmentsList: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    //@IBOutlet var searchBar: UISearchBar!
    
    //table view
    @IBOutlet var tblView: UITableView!
    
    //for title name
    @IBOutlet weak var lblTitleName: UILabel!
    
    //MARK: - Variable
    //show error/warning messages
    let panel = JKNotificationPanel()
    
    //stores department models
    var arrForDepartmentName = [DepartmentModel]()
    
    //stores office models
    var arrPassedForOffices = [OfficeModel]()
    
    //stores organisation models array
    var arrPassedForOrganisation = [OrganisationModel]()
    
    //stores dictionary of departments
    var arrDepartments = [[String:Any]]()
    
    //call back functions
    var didFinishAddingDepartments : ((DepartmentModel) -> ())?
    var didFinishAddingOffices : ((OfficeModel) -> ())?
    var didFinishAddingOrg : ((OrganisationModel) -> ())?
    
    var clickedByWhome = ""
    
    //selection are saved in below variables
    var selectedDeptIndex = -1
    var selectedDict = DepartmentModel()
    
    var selectedOfficeIndex = -1
    var selectedOfficeDict = OfficeModel()
    
    var selectedOrgIndex = -1
    var selectedOrgDict = OrganisationModel()
    
    var selectedOrgId = ""
    //var selectedDeptId = ""
    //var selectedOfcId = ""
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.reloadData()
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        if clickedByWhome == "byDept"
        {
            //for departments
            
            //load all departments
            callWebServiceToGetAllDepartments()
            lblTitleName.text = "DEPARTMENTS"
        }
        else if clickedByWhome == "byOrg"
        {
            //for organisation
            lblTitleName.text = "ORGANISATION"
            
            if (self.selectedOrgId != "") {
                
                for (index, value) in self.arrPassedForOrganisation.enumerated() {
                    if value.strOrganisation_id == self.selectedOrgId {
                        self.selectedOrgIndex = index
                        self.selectedOrgDict = self.arrPassedForOrganisation[index]
                        break;
                    }
                }
            }
            tblView.reloadData()
            
        }
        else{
            //for office
            lblTitleName.text = "OFFICES"
            if (self.selectedOfficeDict.strOffice_id != "") {
                
                for (index, value) in self.arrPassedForOffices.enumerated() {
                    if value.strOffice_id == self.selectedOfficeDict.strOffice_id {
                        self.selectedOfficeIndex = index
                        self.selectedOfficeDict = self.arrPassedForOffices[index]
                        break;
                    }
                }
            }
            tblView.reloadData()
            
        }
        
    }
    
    //MARK: - IBActions
    /**
     * Called when user clicks on done button to submit its selections
     */
    @IBAction func btnDoneAction(_ sender: UIButton) {
        if clickedByWhome == "byDept"
        {
            //for departments
            if (selectedDict.strId == "" ) {
                //validate for atleast one selection
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one Department.")
            }
            else{
                print(selectedDict)
                self.dismiss(animated: false) {
                    
                    //call back
                    self.didFinishAddingDepartments?(self.selectedDict)
                    
                    //Notification to dismiss
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed"), object: nil)
                }
            }
        }
        else if clickedByWhome == "byOrg"
        {
            //for organisation
            if (selectedOrgDict.strOrganisation_id == "" ) {
                //validate for atleast one selection
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one Organisation.")
            }
            else{
                print(selectedOrgDict)
                self.dismiss(animated: false) {
                    
                    //call back
                    self.didFinishAddingOrg?(self.selectedOrgDict)
                    
                    //Notification to dismiss
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed"), object: nil)
                }
            }
        }
        else{
            //for office
            if (selectedOfficeDict.strOffice_id == "" ) {
                //validate for atleast one selection
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one Office.")
            }
            else{
                print(selectedOfficeDict)
                self.dismiss(animated: false) {
                    
                    //call back
                    self.didFinishAddingOffices?(self.selectedOfficeDict)
                    
                    //Notification to dismiss
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed"), object: nil)
                }
            }
        }
    }
    
    /**
     * This function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /**
     * This function is used in case of multiselect
     */
    @objc func btnMutliSelectionAction(_ sender: UIButton) {
        if clickedByWhome == "byDept"
        {
            //fordepartment
            selectedDeptIndex = sender.tag
            selectedDict = self.arrForDepartmentName[sender.tag]
            self.tblView.reloadData()
        }
        else if clickedByWhome == "byOrg"
        {
            //for organisation
            selectedOrgIndex = sender.tag
            selectedOrgDict = self.arrPassedForOrganisation[sender.tag]
            self.tblView.reloadData()
        }
        else
        {
            //for office
            selectedOfficeIndex = sender.tag
            selectedOfficeDict = self.arrPassedForOffices[sender.tag]
            self.tblView.reloadData()
        }
        
    }
    
    //MARK: - UITableViewDelegate Methods
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if clickedByWhome == "byDept"
        {
            //department case
            return arrForDepartmentName.count
            
        }
        else if clickedByWhome == "byOrg" {
            //organisation case
            return arrPassedForOrganisation.count
        }
        else{
            //office case
            return arrPassedForOffices.count
            
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell for department
        let cell : DeparmentListCell = tableView.dequeueReusableCell(withIdentifier: "DeparmentListCell") as! DeparmentListCell
        
        if clickedByWhome == "byDept"{
            //for dept
            cell.lblDepatmentTitle.text = arrForDepartmentName[indexPath.row].strDepartment
        }
        else if clickedByWhome == "byOrg" {
            //for org
            cell.lblDepatmentTitle.text = arrPassedForOrganisation[indexPath.row].strOrganisation
        }
        else {
            //for office
            cell.lblDepatmentTitle.text = arrPassedForOffices[indexPath.row].strOffice
        }
        
        //set tag & target for the checkbox button
        cell.btnCheckedUnChecked.tag = indexPath.row
        cell.btnCheckedUnChecked.addTarget(self, action: #selector(btnMutliSelectionAction(_:)), for: .touchUpInside)
        
        if clickedByWhome == "byDept"{
            //for dept
            
            if selectedDeptIndex == indexPath.row {
                //keep the checkbox checked if the displayed row is selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }
            else{
                //keep the checkbox unchecked if the displayed row is not selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
        else if clickedByWhome == "byOrg" {
            //for organisation
            
            if self.selectedOrgIndex == indexPath.row {
                //keep the checkbox checked if the displayed row is selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }
            else{
                //keep the checkbox unchecked if the displayed row is not selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
            
        }
        else {
            //for office
            
            if self.selectedOfficeIndex == indexPath.row {
                //keep the checkbox checked if the displayed row is selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }
            else{
                //keep the checkbox unchecked if the displayed row is not selected
                cell.btnCheckedUnChecked.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
            
        }
        return cell
    }
    
    /**
     * Tableview delegate method used to return heght of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    //MARK: - Calling Web service
    /**
     * API - get department list
     */
    func callWebServiceToGetAllDepartments() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse department list
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        //store the parsed departments to array
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if (self.selectedDict.strId != "") {
                        
                        for (index, value) in self.arrForDepartmentName.enumerated() {
                            if value.strId == self.selectedDict.strId {
                                self.selectedDeptIndex = index
                                self.selectedDict = self.arrForDepartmentName[index]
                                break;
                            }
                        }
                    }
                    
                    self.tblView.reloadData()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
