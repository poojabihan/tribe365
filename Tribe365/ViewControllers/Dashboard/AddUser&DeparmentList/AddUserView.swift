//
//  AddUserView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 11/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is used to add staff in organisation.
 * This comes under Super Admin login module
 */
class AddUserView: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    //show name
    @IBOutlet var txtName: UITextField!
    
    //for email
    @IBOutlet var txtEmail: UITextField!
    //    @IBOutlet var txtPassword: UITextField!
    //    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    //for department
    @IBOutlet var txtDeparment: UITextField!
    
    //for organisation
    @IBOutlet var txtOrganisationName: UITextField!
    
    //for office name
    @IBOutlet var txtOfficeName: UITextField!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //no longer in use
    @IBOutlet weak var btnDept: UIButton!
    @IBOutlet weak var btnOffice: UIButton!
    @IBOutlet weak var btnOrgName: UIButton!
    @IBOutlet weak var lblOrgName: UILabel!
    
    //NSLayoutConstraint
    @IBOutlet weak var heightLblOrg: NSLayoutConstraint!
    @IBOutlet weak var heightTxtOrg: NSLayoutConstraint!
    
    //for title
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    //MARK: - Variable
    //textfield instance
    var textField: UITextField?
    
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores department model array
    var arrDept = [DepartmentModel]()
    
    //stores office model array
    var arrOfficeDetail = [OfficeModel]()
    
    //stores selected office model
    var Selectedarrofc = [OfficeModel]()
    
    //stores selected dept id
    var strDeptId = ""
    
    //stores selected office id
    var strOfficeId = ""
    
    //stores organisation id
    var strOrgID = ""
    
    //stores value which helps manipulate the screen
    var addUserViewOfficeid = ""
    var addUserViewOfficeName = ""
    var comingFromUserInfoView = ""
    
    //stores array of user model
    var arrofselectedUser = [UserModel]()
    
    //stores array of organisation
    var arrOrganisation = [OrganisationModel]()
    
    //stores selected organisation model
    var SelectedarrOrg = [OrganisationModel]()
    
    //stores id's
    var strOrgId = ""
    var selectedOrgId = ""
    var selectedDeptId = ""
    var selectedOfcId = ""
    
    //MARK: - ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(AddUserView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddUserView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        txtDeparment.inputView = UIView()
        txtOfficeName.inputView = UIView()
        
        if comingFromUserInfoView == "TRUE"{
            //case when coming from user info screen
            txtOfficeName.text = addUserViewOfficeName
            strOfficeId = addUserViewOfficeid
            btnOffice.isUserInteractionEnabled = false
            txtOfficeName.isUserInteractionEnabled = false
        }
        
        self.hideKeyboardWhenTappedAround()
        
        if arrofselectedUser.count != 0 {
            //update case
            
            //set staff details initially
            lblTitleHeader.text = "UPDATE STAFF INFO"
            txtName.text = arrofselectedUser[0].strName
            txtEmail.text = arrofselectedUser[0].strEmail
            //            txtPassword.text = arrofselectedUser[0].strPassword
            //            txtConfirmPassword.text = arrofselectedUser[0].strPassword
            txtDeparment.text = arrofselectedUser[0].strDepartment
            txtOfficeName.text = arrofselectedUser[0].strOfficeId
            txtOfficeName.text = arrofselectedUser[0].strOfficeName
            txtOrganisationName.text = arrofselectedUser[0].strOrgName
            
            // selectedOfcId = arrofselectedUser[0].strOfficeId
            selectedOrgId = arrofselectedUser[0].strOrgId
            // selectedDeptId = arrofselectedUser[0].strDepartmentPreDefineId
            
            txtEmail.isUserInteractionEnabled = false
            txtEmail.textColor = ColorCodeConstant.borderLightGrayColor
            
            strOfficeId = arrofselectedUser[0].strOfficeId
            strDeptId = arrofselectedUser[0].strDepartmentPreDefineId
            strOrgId = arrofselectedUser[0].strOrgId
        }
        else {
            txtEmail.isUserInteractionEnabled = true
            txtEmail.textColor = ColorCodeConstant.darkTextcolor
            heightLblOrg.constant = 0
            heightTxtOrg.constant = 0
        }
    }
    
    //MARK: - IBActions
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when user submits staff details.
     */
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        //validate all the fields
        if (txtName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Name.")
        }
        else if (txtEmail.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Email.")
        }
        else if (txtEmail.text!.isValidEmail() == false){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid Email.")
            
        }
            //        else if (txtPassword.text?.isEmpty)! {
            //            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Password.")
            //        }
            //        else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
            //
            //            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
            //        }
            //
            //        else if (txtPassword.text != txtConfirmPassword.text) {
            //            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
            //        }
        else if (txtDeparment.text?.isEmpty)!  {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one Department.")
        }
        else if (txtOfficeName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Office Name.")
        }
        else {
            //all field passes the validation
            
            if arrofselectedUser.count != 0 {
                //update the user details
                callWebServiceToUpdateUser()
            }
            else {
                //add new user
                callWebServiceToAddUser()
            }
        }
    }
    
    /**
     * Custom function which checks the spaces in string and returns a bool accordingly
     */
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    /**
     * Tthis function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Calling web service
    /**
     * API - add user
     */
    func callWebServiceToAddUser() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameter for  api
        let param =
            ["orgId":strOrgID, //user organisation id
                "officeId":strOfficeId, //user office id
                "name":txtName.text ?? "", //user name
                "email":txtEmail.text ?? "", //user email
                //             "password":txtPassword.text ?? "",
                "departmentId":strDeptId //user department is
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.User.addUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message in case of User added successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "User added successfully")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - update user
     */
    func callWebServiceToUpdateUser() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameter for  api
        let param =
            ["userId":arrofselectedUser[0].strId,
             "orgId":strOrgId,
             "officeId":strOfficeId,
             "departmentId":strDeptId,
             "name":txtName.text ?? ""
                //             "password":txtPassword.text ?? ""
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.User.updateUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message in case of User updated successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "User updated successfully")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    //MARK: - Segue Method
    /**
     * Prepare segue method
     * This method is called automatically when any segue is called
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddDept" {
            //add department segue
            
            //get destination view controller
            let vc = segue.destination as! DeparmentsList
            vc.clickedByWhome = "byDept"
            vc.selectedDict.strId = self.strDeptId//Change
            
            //call back method, pass the required data
            vc.didFinishAddingDepartments = { dict in
                self.arrDept.append(dict)
                self.txtDeparment.text = dict.strDepartment
                self.strDeptId = dict.strId
                print(self.arrDept)
            }
        }
        if addUserViewOfficeid == "" {
            if segue.identifier == "AddOfficesArr" {
                //add office segue
                
                //get destination view controller
                let vc = segue.destination as! DeparmentsList
                vc.clickedByWhome = "byOffices"
                vc.selectedOfficeDict.strOffice_id = self.strOfficeId//Change
                vc.arrPassedForOffices = arrOfficeDetail
                
                //call back method, pass the required data
                vc.didFinishAddingOffices = { dict in
                    self.Selectedarrofc.append(dict)
                    self.txtOfficeName.text = dict.strOffice
                    self.strOfficeId = dict.strOffice_id
                    print(self.Selectedarrofc)
                }
            }
        }
        if arrofselectedUser.count != 0 {
            if segue.identifier == "AddOrg" {
                //add org segue
                
                //get destination view controller
                let vc = segue.destination as! DeparmentsList
                vc.clickedByWhome = "byOrg"
                self.txtDeparment.text = ""
                self.txtOfficeName.text = ""
                self.strOfficeId = ""
                self.strDeptId = ""
                vc.selectedOrgId = self.strOrgId
                vc.arrPassedForOrganisation = arrOrganisation
                
                //call back method, pass the required data
                vc.didFinishAddingOrg = { dict in
                    self.SelectedarrOrg.append(dict)
                    self.txtOrganisationName.text = dict.strOrganisation
                    self.strOrgId = dict.strOrganisation_id
                    self.arrOfficeDetail = dict.arrOffices
                    print(self.SelectedarrOrg)
                }
            }
        }
        
    }
    
    
}



