//
//  UserInfoView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 12/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

/**
 * This class is used to show the user listing
 */
class UserInfoView: UIViewController , UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate{
    
    // MARK: - IBOutlets
    //table view
    @IBOutlet var tblView: UITableView!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Variables
    //selected office id
    var strOfficeId = ""
    
    //selected office name
    var strOfficeName = ""
    
    //stores array of user
    var arrUserInfo = [UserModel]()
    
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores selected organisation id
    var strOrgID = ""
    
    //store user id for user which we want to delete
    var strDeleteUserId = ""
    
    //stores array of organisation
    var arrOrganisation = [OrganisationModel]()
    
    //stores array of office
    var arrOffices = [OfficeModel]()
    
    //stores serached array
    var filteredData = [UserModel]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set search bar delegate
        searchBar.delegate = self
        
        tblView.reloadData()
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        //
        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.layer.cornerRadius = 1
        //        searchTextField.borderStyle = .none
        //        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
        //        searchTextField.textAlignment = NSTextAlignment.left
        //        let image:UIImage = UIImage(named: "search")!
        //        let imageView:UIImageView = UIImageView.init(image: image)
        //        searchTextField.leftView = nil
        //        searchTextField.placeholder = " Search"
        //        searchTextField.rightView = imageView
        //        searchTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //load user details
        callWebServiceForUserDetail()
        tblView.reloadData()
    }
    //MARK: - IBAtions
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * This function is used to add new user
     */
    @IBAction func btnAddUserAction(_ sender: UIButton) {
        
        //redirect to add user screen
        let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddUserView") as! AddUserView
        objVC.addUserViewOfficeid = strOfficeId
        objVC.addUserViewOfficeName = strOfficeName
        objVC.strOrgID = strOrgID
        objVC.comingFromUserInfoView = "TRUE"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell for user list
        let cell : UserInfoCell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell") as! UserInfoCell
        
        //set username
        cell.lblUserName.text = filteredData[indexPath.row].strName
        
        //set department name
        cell.lblDeparment.text = filteredData[indexPath.row].strDepartment
        
        //set email
        cell.lblEmail.text = filteredData[indexPath.row].strEmail
        
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    /**
     * Tableview delegate method called when user selects any row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    /**
     * Tableview delegate method used to set swipe cell action buttons in row
     */
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //delete button
        let deleteAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            
            
            // Handle delete action
        }
        
        //edit button
        let editAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            // Handle delete action
        }
        
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "editOrganizations"), for: .normal)
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "Delete"), for: .normal)
        return [deleteAction, editAction]
        
    }
    
    /**
     * Tableview delegate method used to set swipe cell action buttons in row
     */
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        //delete user button
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            self.strDeleteUserId =  self.filteredData[indexPath.row].strId
            
            //show alert confirming the delete
            let alertController = UIAlertController(title: "", message: "Are you sure you want to delete this user?", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                //run your function here
                self.callWebServiceForDeleteUser()
            }))
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            self.tblView.reloadData()
            // Reset state
            success(true)
        })
        
        //edit user button
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddUserView") as! AddUserView
            
            objVC.arrofselectedUser = [self.filteredData[indexPath.row]]
            objVC.arrOrganisation = self.arrOrganisation
            objVC.arrOfficeDetail = self.arrOffices
            
            self.navigationController?.pushViewController(objVC, animated: true)
            
            // Reset state
            success(true)
        })
        
        editAction.image = UIImage(named: "editOrganizations")
        editAction.backgroundColor = ColorCodeConstant.darkTextcolor
        
        
        deleteAction.image = UIImage(named: "Delete")
        deleteAction.backgroundColor = ColorCodeConstant.themeRedColor
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    /**
     * Tableview delegate method which allows the swipe feature in row
     */
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        
        if AuthModel.sharedInstance.role == kSuperAdmin { //Admin
            //allow swipe only to admin
            return true
            
        }
        else{
            return false
        }
    }
    
    /**
     * UISearchBar delegate method called when user cliks on cancel button
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method called when user cliks on search button
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method called when user types something in search bar
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? arrUserInfo : arrUserInfo.filter { (item: UserModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblView.reloadData()
    }
    
    //MARK: - WebService Methods
    /**
     * API - used to delete user
     */
    func callWebServiceForDeleteUser() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameter for api
        let param = [
            "userId":  strDeleteUserId //user id to delete
        ]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.User.deleteStaff , param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //refresh user details
                    self.callWebServiceForUserDetail()
                    self.tblView.reloadData()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - get user details
     */
    func callWebServiceForUserDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameter for api
        let param = [
            "officeId": strOfficeId //office id
        ]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.User.userDetail , param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.arrUserInfo.removeAll()
                    DashboardParser.parseUserList(response: response!, completionHandler: { (arruser) in
                        //store the user list
                        self.filteredData = arruser
                        self.arrUserInfo = arruser
                    })
                    
                    if self.arrUserInfo.count == 0{
                        //show no data message in case of no data
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = "No staff data found"
                        self.tblView.isHidden = true
                    }
                    else{
                        //hide no data label if there is data available
                        self.lblNoData.isHidden = true
                        self.tblView.isHidden = false
                        
                    }
                    
                    //empty search bar
                    self.searchBar.text = ""
                    
                    self.tblView.reloadData()
                }
                else {
                    //show no data message in case of no data
                    self.lblNoData.isHidden = false
                    self.lblNoData.text = "No staff data found"
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
