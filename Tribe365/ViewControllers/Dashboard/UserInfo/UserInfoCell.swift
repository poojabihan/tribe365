//
//  UserInfoCell.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 12/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

/**
* This cell class is used to show user list row
*/
class UserInfoCell: UITableViewCell {

    //rectangle view white background
    @IBOutlet weak var viewForCell: UIView!
    
    //show user name
    @IBOutlet var lblUserName: UILabel!
    
    //show user image
    @IBOutlet var imgProfile: UIImageView!
    
    //show user email
    @IBOutlet var lblEmail: UILabel!
    
    //show phone
    @IBOutlet var lblPhone: UILabel!
    
    //show dept
    @IBOutlet var lblDeparment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewForCell.layer.borderWidth = 0.5
        viewForCell.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
