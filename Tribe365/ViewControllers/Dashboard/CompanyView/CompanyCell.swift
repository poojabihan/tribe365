//
//  CompanyCell.swift
//  Tribe365
//
//  Created by kdstudio on 01/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell class is used to show organisation list row
 */
class CompanyCell: UITableViewCell {
    
    //organisation logo
    @IBOutlet weak var imgCompanyLogo: UIImageView!
    
    //organisation name
    @IBOutlet weak var lblOrganizationName: UILabel!
    
    //department name
    @IBOutlet weak var lblDepartments: UILabel!
    
    //office name
    @IBOutlet weak var lblOffice: UILabel!
    
    //industry type
    @IBOutlet weak var lblIndustryType: UILabel!
    
    //height constraint
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    //bg view for cell to give square effect
    @IBOutlet weak var viewForCell: UIView!
    
    //add office button
    @IBOutlet weak var btnAddOffice: UIButton!
    
    //height constraint
    @IBOutlet weak var HeightConstraintForView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewForCell.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        viewForCell.layer.borderWidth = 0.5
        
        //set logo image appearance
        imgCompanyLogo
            .layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        imgCompanyLogo
            .layer.borderWidth = 0.5        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
