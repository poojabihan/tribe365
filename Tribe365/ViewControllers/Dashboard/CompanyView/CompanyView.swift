//
//  CompanyView.swift
//  Tribe365
//
//  Created by kdstudio on 01/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

/**
 * This class is used to show the organisation listing
 */
class CompanyView: UIViewController, UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - IBOutlets
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //add organisation button
    @IBOutlet weak var btnAddOrg: UIButton!
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    //    @IBOutlet weak var tblViewBottomConstarint: NSLayoutConstraint!
    
    // MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores array of organisations
    var arrOrganisation = [OrganisationModel]()
    var comingFromUser = ""
    
    //stores search result array
    var filteredData = [OrganisationModel]()
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set search bar delegate
        searchBar.delegate = self
        
        if(comingFromUser == "FromCellAddUser"){
            //when coming from staff listing, do not show the add organisation button
            btnAddOrg.isHidden = true
        }
        if(comingFromUser == "STAFF"){
            //when coming from staff listing, do not show the add organisation button
            btnAddOrg.isHidden = true
        }
        if(comingFromUser == "REPORT"){
            //when coming from report, do not show the add organisation button
            btnAddOrg.isHidden = true
        }
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        //load organisation list
        callWebServiceForOrganisationListing()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        //
        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchTextField.layer.cornerRadius = 1
        //        searchTextField.borderStyle = .none
        //        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
        //        searchTextField.textAlignment = NSTextAlignment.left
        //        let image:UIImage = UIImage(named: "search")!
        //        let imageView:UIImageView = UIImageView.init(image: image)
        //        searchTextField.leftView = nil
        //        searchTextField.placeholder = " Search"
        //        searchTextField.rightView = imageView
        //        searchTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //load organisation list
        callWebServiceForOrganisationListing()
        tblView.reloadData()
    }
    
    // MARK: - IBActions
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when clicks on add organisation button.
     * It edirects user to add organisation screen
     */
    @IBAction func btnAddOrg(_ sender: UIButton) {
        let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddOrganisationView") as! AddOrganisationView
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * this function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell for organisation listing
        let cell : CompanyCell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell") as! CompanyCell
        
        //set organisation details via below laels
        let strOrgname = filteredData[indexPath.row].strOrganisation
        cell.lblOrganizationName.text = strOrgname
        cell.lblIndustryType.text = filteredData[indexPath.row].strIndustry
        cell.lblOffice.text = filteredData[indexPath.row].strNumberOfOffices
        cell.lblDepartments.text = filteredData[indexPath.row].strNumberOfDepartments
        
        let url = URL(string:filteredData[indexPath.row].strOrganisation_logo)
        
        cell.imgCompanyLogo.kf.setImage(with: url)
        
        if(comingFromUser == "STAFF")  {
            //when coming from staff listing, do not show the add organisation button
            cell.HeightConstraintForView.constant = 115
            cell.btnAddOffice.isHidden = true
        }
            
        else if(comingFromUser == "REPORT")  {
            //when coming from reposrts, do not show the add organisation button
            cell.HeightConstraintForView.constant = 115
            cell.btnAddOffice.isHidden = true
        }
            
        else if(comingFromUser == "FromCellAddUser")  {
            //when coming from staff listing, do not show the add organisation button
            cell.HeightConstraintForView.constant = 115
            cell.btnAddOffice.isHidden = true
        }
            
        else{
            //when coming from organisation listing, show the add office button and set its tag & target
            cell.btnAddOffice.tag = indexPath.row
            cell.btnAddOffice.isHidden = false
            cell.btnAddOffice.addTarget(self, action: #selector(AddOffice(_:)), for: .touchUpInside)
        }
        
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(comingFromUser == "STAFF")  {
            
            return 132
            
        }
        else if(comingFromUser == "REPORT")  {
            
            return 132
            
        }
            
        else if(comingFromUser == "FromCellAddUser")  {
            
            return 132
        }
        else{
            
            return 145
        }
        
    }
    
    /**
     * Tableview delegate method is called when any organisation is clicked
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(comingFromUser)
        //Navigate to new view
        if comingFromUser == "STAFF" {
            //If this screen is displayed for satff listing, redirect it to OfficeShowView
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "OfficeShowView") as! OfficeShowView
            objVC.arrOfficeDetail = filteredData[indexPath.row].arrOffices
            objVC.strOrgID = filteredData[indexPath.row].strOrganisation_id
            objVC.arrOrganisation = arrOrganisation
            self.navigationController?.pushViewController(objVC, animated: true)
        }
            
        else if(comingFromUser == "FromCellAddUser"){
            //If this screen is displayed for satff listing, redirect it to AddUserView
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddUserView") as! AddUserView
            objVC.arrOfficeDetail = filteredData[indexPath.row].arrOffices
            objVC.strOrgID = filteredData[indexPath.row].strOrganisation_id
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }
        else if(comingFromUser == "REPORT"){
            //If this screen is displayed for reposrt listing, redirect it to report view
            let objVC = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "AdminReportView") as! AdminReportView
            //objVC.arrOfficeDetail = filteredData[indexPath.row].arrOffices
            objVC.strOrgId = filteredData[indexPath.row].strOrganisation_id
            objVC.strOrgName = filteredData[indexPath.row].strOrganisation
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }
        else{
            //otherwise redirect to dashboard(Home) view
            let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "CategoryDefinedView") as! CategoryDefinedView
            objVC.strOrgID = filteredData[indexPath.row].strOrganisation_id
            objVC.strCompanyName = filteredData[indexPath.row].strOrganisation
            objVC.urlForOrgLogo = filteredData[indexPath.row].strOrganisation_logo
            objVC.isDOTAdded = filteredData[indexPath.row].isDot == true ? 1 : 0
            objVC.arrOfficeDetail = filteredData[indexPath.row].arrOffices
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    /**
     * Tableview delegate method used to define the swipe cell buttons
     */
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //add a delete button to swipe view
        let deleteAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            // Handle delete action
        }
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "editOrganizations"), for: .normal)
        return [deleteAction]
        
    }
    
    /**
     * Tableview delegate method used to define actions for the swipe cell buttons
     */
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AddOrganisationView") as! AddOrganisationView
            
            objVC.strUpdateOrganisation = "TRUE"
            print(self.filteredData[indexPath.row])
            objVC.orgModel = self.filteredData[indexPath.row]
            self.navigationController?.pushViewController(objVC, animated: true)
            
            // Reset state
            success(true)
        })
        editAction.image = UIImage(named: "editOrganizations")
        editAction.backgroundColor = UIColor.gray //UIColor(hexString: "eb1c24")
        
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            //delete action
            let alertController = UIAlertController(title: "", message: "Are you sure you want to delete this organisation?", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                //run your function here
                self.callWebServiceForDeleteOrganisation(orgID: self.filteredData[indexPath.row].strOrganisation_id, index: indexPath.row)
            }))
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            self.tblView.reloadData()
            
            success(true)
        })
        deleteAction.image = UIImage(named: "Delete")
        deleteAction.backgroundColor = ColorCodeConstant.themeRedColor
        
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    /**
     * Tableview delegate method used to enable the swipe functionality
     */
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        
        if comingFromUser == "FALSE" {
            //enable when organisation listing
            return true
            
        }
        else{
            //otherwise false
            return false
        }
    }
    
    /**
     * UISearchBar delegate method called when cancel button is cliked for search bar
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method called when search button is cliked for search bar
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method called when user types in the search bar
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? arrOrganisation : arrOrganisation.filter { (item: OrganisationModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strOrganisation.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblView.reloadData()
    }
    
    //MARK: - AddOFFICE
    /**
     * Target method used to redirect user to add office screen
     */
    @objc func AddOffice(_ sender: UIButton) {
        
        //redierct user to OfficeDetailsView to add office in orgabisation
        let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "OfficeDetailsView") as! OfficeDetailsView
        objVC.modalPresentationStyle = .overCurrentContext
        objVC.strComingFromAddOffice = "TRUE"
        objVC.strOrgId = filteredData[sender.tag].strOrganisation_id
        objVC.didFinishAddingOffice = {
            //call back method, tells to refresh the organisation listing
            self.callWebServiceForOrganisationListing()
        }
        self.navigationController?.present(objVC, animated: false, completion: nil)
    }
    
    //MARK: - WebService Methods
    /**
     * API - load organisation listing
     */
    func callWebServiceForOrganisationListing() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Organisation.orgList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            print(response ?? "")
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse organisation listing
                    DashboardParser.parseOrganisationList(response: response!, completionHandler: { (arrOrg) in
                        //store the organisation data to array
                        self.arrOrganisation = arrOrg
                        self.filteredData = arrOrg
                        
                    })
                    
                    self.searchBar.text = ""
                    self.tblView.reloadData()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - delete organisation
     */
    func callWebServiceForDeleteOrganisation(orgID: String, index: Int) {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        param = ["orgId": orgID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.deleteOrganisation, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            print(response ?? "")
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //remove the deleted organisation from array
                    self.filteredData.remove(at: index)
                    self.arrOrganisation.remove(at: index)
                    self.tblView.reloadData()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}

class TableViewRowAction: UITableViewRowAction
{
    var image: UIImage?
    
    func _setButton(button: UIButton)  {
        
        if let image = image, let titleLabel = button.titleLabel {
            
            let labelString = NSString(string: titleLabel.text!)
            let titleSize = labelString.size(withAttributes: [kCTFontAttributeName as NSAttributedStringKey: titleLabel.font])
            button.tintColor = UIColor.white
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            button.imageEdgeInsets.right = -titleSize.width
        }
    }
}

