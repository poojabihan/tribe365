//
//  officeShowDetailCell.swift
//  Tribe365
//
//  Created by Apple on 19/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
* This cell class is used to show office list row
*/
class officeShowDetailCell: UITableViewCell {

    //show office name
    @IBOutlet weak var lblOfficeName: UILabel!
    
    //white rectangle background view
    @IBOutlet weak var ViewForCell: UIView!
    
    //office address
    @IBOutlet weak var lblOfficeAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //rectangle white view appearance
        ViewForCell.layer.borderWidth = 0.5
        ViewForCell.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
