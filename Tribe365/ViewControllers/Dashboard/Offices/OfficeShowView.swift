//
//  OfficeShowView.swift
//  Tribe365
//
//  Created by Apple on 19/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

/**
 * This class is used to show the office listing
 */
class OfficeShowView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - IBOutlets
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //no data label
    @IBOutlet weak var lblNOData: UILabel!
    
    // MARK: - Variables
    //stores array of office
    var arrOfficeDetail = [OfficeModel]()
    
    //stores organisation id
    var strOrgID = ""
    
    //stores selected organisation model
    var arrOrganisation = [OrganisationModel]()
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For Navigation bar tranparancy
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        if arrOfficeDetail.count == 0 {
            //show no data in case of no office
            lblNOData.isHidden = false
            lblNOData.text = "No office data found"
        }
    }
    
    
    // MARK: - IBActions
    /**
     * This function navigates to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - UITableView Data Souce and delegate
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfficeDetail.count
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell to show office list
        let cell : officeShowDetailCell = tableView.dequeueReusableCell(withIdentifier: "officeShowDetailCell") as! officeShowDetailCell
        
        //set office name
        cell.lblOfficeName.text = arrOfficeDetail[indexPath.row].strOffice
        
        //set office address
        cell.lblOfficeAddress.text = arrOfficeDetail[indexPath.row].strAddress
        
        return cell
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    /**
     * Tableview delegate method called when user clicks on any office
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Navigate to new view
        
        //redirect to the userinfo screen
        let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "UserInfoView") as! UserInfoView
        objVC.strOfficeId = arrOfficeDetail[indexPath.row].strOffice_id
        objVC.strOfficeName = arrOfficeDetail[indexPath.row].strOffice
        objVC.arrOffices = arrOfficeDetail
        objVC.strOrgID = strOrgID
        objVC.arrOrganisation = arrOrganisation
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
}
