//
//  OfficeDetailsView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 14/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

/**
 * This is a popup screen, which shows the office details.
 */

//call back method used to send the upadated office details back to the previous screen
protocol sendBack {
    func sendNameToPreviousVC(SendUpdatedName: String, SendUpdatedAdd: String, SendUpdatedCity: String ,SendUpdatedCountry: String, SendUpdatedPhone: String,SendUpdatedNumberOfEmployees: String, SendUpdatedArrayIndex: Int )
}

class OfficeDetailsView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: - IBOutlets
    //for  phone number
    @IBOutlet var txtPhone: UITextField!
    
    //for  country
    @IBOutlet var txtCountry: UITextField!
    
    //for  city
    @IBOutlet var txtCity: UITextField!
    
    //for  address
    @IBOutlet var txtViewAddress: UITextView!
    
    //for  office name
    @IBOutlet var txtOfficeName: UITextField!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //for number of employess count
    @IBOutlet weak var txtNumberOfEmployee: UITextField!
    
    //MARK: - Variable
    //to show error/warning message
    let panel = JKNotificationPanel()
    
    //stores dictionary of office detail array
    var arrOfficeDetail = [[String:Any]]()
    
    //call back method which sends back the newly added office details
    var didFinishAddingOffices : (([String:Any]) -> ())?
    
    //call back method which sends back the updated office details, but it is no longer in use
    var didFininshUpdatingOffices : (([String:Any]) -> ())?
    
    //textfield instance
    var textField: UITextField?
    
    //detect if from add office
    var strComingFromAddOffice = ""
    
    //stores org id
    var strOrgId = ""
    
    //stores updated office array
    var updateArrayOffices = OfficeModel()
    
    //if came to update
    var strUpdateOffice = ""
    
    //stores index of office from previous screen
    var intIndexPath = Int()
    
    //protocol method
    var mDelegate : sendBack? = nil
    
    //no longer in use
    var strAdd = ""
    
    //stores city
    var strCity = ""
    
    //stores country
    var strCountry = ""
    
    //stores phone
    var strPhone = ""
    
    //no longer in use
    var strName = ""
    
    //call back method which sends back the office details
    var didFinishAddingOffice : (() -> ())?
    
    //stores countries array
    var arrCountry = [CountryModel]()
    
    //stores selcted country id
    var selectedCountryId = ""
    
    //MARK: - ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hides the keyboard when user taps anywhere on screen
        hideKeyboardWhenTappedAround()
        
        //textview styling
        txtViewAddress.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        txtViewAddress.layer.borderWidth = 1
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(OfficeDetailsView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OfficeDetailsView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        if strUpdateOffice == "TRUE"{
            //update case
            
            /*"phone" : "0123456987",
             "office_id" : 50,
             "country" : "India",
             "city" : "Indore",
             "office" : "of1",
             "address" : "of1 of1"*/
            
            /*txtCity.text = strCity
             txtPhone.text = strPhone
             txtCountry.text = strCountry
             txtOfficeName.text = strName
             txtViewAddress.text = strAdd*/
            
            //set initial office details
            txtCity.text = updateArrayOffices.strCity
            txtPhone.text = updateArrayOffices.strPhone
            txtCountry.text = updateArrayOffices.strCountry
            txtOfficeName.text = updateArrayOffices.strOffice
            txtViewAddress.text = updateArrayOffices.strAddress
            txtNumberOfEmployee.text = updateArrayOffices.strNoOfEmployee
            
        }
        
        //initialise a picker for country list
        let thePicker = UIPickerView()
        thePicker.delegate = self
        txtCountry.inputView = thePicker
        
        //load country list
        callWebServiceToGetCountryList()
        
    }
    //MARK: - IBActions
    /**
     * This function is called when Done button is clicked
     */
    @IBAction func btnDoneAction(_ sender: Any) {
        
        //valiate the required fields
        if (txtOfficeName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Office Name.")
        }
        else if (txtNumberOfEmployee.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Number of Employees.")
        }
        else  if (txtViewAddress.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Address.")
        }
        else  if (txtCity.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter City.")
        }
        else  if (txtCountry.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Country.")
        }
        else  if (txtPhone.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Phone Number.")
        }
            
        else{
            //validation passes then start further process
            
            if strComingFromAddOffice == "TRUE"{
                //add office case
                
                //add office api call
                callWebServiceToAddOffice()
            }
            else if strUpdateOffice == "TRUE"{
                //update office case
                
                //pass the updated data back to previous screen via delegate
                mDelegate?.sendNameToPreviousVC(SendUpdatedName: txtOfficeName.text!, SendUpdatedAdd: txtViewAddress.text!, SendUpdatedCity: txtCity.text!, SendUpdatedCountry: txtCountry.text!, SendUpdatedPhone: txtPhone.text!, SendUpdatedNumberOfEmployees: txtNumberOfEmployee.text!, SendUpdatedArrayIndex: intIndexPath)
                
                //dismiss the controller after passing details
                self.dismiss(animated: false, completion: nil)
                
            }
            else{
                
                var dictOffice = [String:Any]()
                dictOffice =
                    ["office":txtOfficeName.text ?? "",
                     "address":txtViewAddress.text ?? "",
                     "city":txtCity.text ?? "",
                     "country":txtCountry.text ?? "",
                     "phone":txtPhone.text ?? "",
                     "noOfEmployee": txtNumberOfEmployee.text ?? ""
                ]
                print(dictOffice)
                
                //send office details back to previous page
                didFinishAddingOffices?(dictOffice)
                
                //dismiss the controller after passing details
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    /**
     * This function is called when Cross button is clicked
     */
    @IBAction func btnCrossAction(_ sender: Any) {
        
        //dismiss the controller
        self.dismiss(animated: false, completion: nil)
        
    }
    
    //MARK: - Custom Methods
    /**
     * API call - when user add office
     */
    func callWebServiceToAddOffice(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        /*{
         {
         "country": "DASdasd",
         "city": "asdad",
         "phone": "asdadas",
         "office": "adaDS",
         "address": "ADSAsd",
         "orgId":"28"
         }
         }*/
        
        //prepare parameters for api
        let param =
            [ "office"  :txtOfficeName.text ?? "", //office name
                "address" :txtViewAddress.text ?? "", //office address
                "city"    :txtCity.text ?? "", //office city
                "country" :selectedCountryId, //office country
                "phone"   :txtPhone.text ?? "", //office contact
                "orgId"   :strOrgId, //organisation id in which we need to add office
                "noOfEmployee": txtNumberOfEmployee.text ?? "" //employees count
                ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Office.addOffice, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //show success message for Office added successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Office added successfully")
                    
                    //call delegate & dismiss
                    self.didFinishAddingOffice!()
                    self.dismiss(animated: false, completion: nil)
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API call - get country list
     */
    func callWebServiceToGetCountryList(){
        
        //show loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Office.getCountryList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse country list details
                    DashboardParser.parseCountryList(response: response!, completionHandler: { arr in
                        //store parsed array to country list
                        self.arrCountry = arr
                    })
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    // MARK: - UIPickerView Delegation
    /**
     * UIPickerView delegate method used to return number of components
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /**
     * UIPickerView delegate method used to return number of rows
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountry.count
    }
    
    /**
     * UIPickerView delegate method used to return title for each row
     */
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCountry[row].strCountryName
    }
    
    /**
     * UIPickerView delegate method is called when we select any row in picker view
     */
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //set selected country name
        txtCountry.text = arrCountry[row].strCountryName
        
        //save selected country id
        selectedCountryId = arrCountry[row].strId
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

/**
 *UIColor Extension for providing hex colour
 */
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
