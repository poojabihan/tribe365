//
//  AddOrganisationView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 11/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This is add organisation screen. Only super admin can add organisation.
 */
class AddOrganisationView: UIViewController,UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate, sendBack {
    
    //MARK: - IBOutlets
    //Title of the screen
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //organisation image
    @IBOutlet var imageView: UIImageView!
    
    //password field
    @IBOutlet var txtPassword: UITextField!
    
    //industry field
    @IBOutlet var txtIndustry: UITextField!
    
    //organisation name field
    @IBOutlet var txtCompanyName: UITextField!
    
    //org address field 1
    @IBOutlet var txtAddress: UITextField!
    
    //lead contact person name field
    @IBOutlet var txtName: UITextField!
    
    //table view which shows added offices
    @IBOutlet var tblOfficeView: UITableView!
    
    //add org image button
    @IBOutlet weak var btnShowHideOfEdit: UIButton!
    
    //add office button
    @IBOutlet weak var btnAddoffices: UIButton!
    
    //no longer in use
    @IBOutlet weak var viewForpassword: UIView!
    @IBOutlet weak var heightConstriantOfpasswordView: NSLayoutConstraint!
    
    //no record found office label
    @IBOutlet weak var lblNoData: UILabel!
    
    //office listing table height
    @IBOutlet weak var HeightForOfficesTable: NSLayoutConstraint!
    
    //org address field 2
    @IBOutlet weak var txtAddress2: UITextField!
    
    //org phone no. field
    @IBOutlet weak var txtPhone: UITextField!
    
    //confirm password field
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    //turn over field
    @IBOutlet weak var txtTurnOver: UITextField!
    
    
    //MARK: - Variable
    //image picker instance
    var imagePicker = UIImagePickerController()
    
    //to display error message
    let panel = JKNotificationPanel()
    
    //stores array of dictionary added office
    var arrOffices = [[String:Any]]()
    
    //textfield instance
    var textField: UITextField?
    
    //Detect add or update case
    var strUpdateOrganisation = ""
    
    //organisation model instance
    var orgModel = OrganisationModel()
    
    //stores org id
    var strOrgId = ""
    
    //stores office array in case of update
    var updateArrayOffices = [OfficeModel]()
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add Delegate for UIImagePicker
        imagePicker.delegate = self
        
        // Notification Observers for UIKeyboard Hide Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(AddOrganisationView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddOrganisationView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        tblOfficeView.reloadData()
        tblOfficeView.layer.borderWidth = 1
        tblOfficeView.layer.borderColor =  ColorCodeConstant.borderLightGrayColor.cgColor
        
        self.hideKeyboardWhenTappedAround()
        
        //remove extra separator lines
        tblOfficeView?.tableFooterView = UIView()
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //check add or update case
        if strUpdateOrganisation == "TRUE" {
            //update case
            
            //set labels accordingly
            lblHeaderTitle.text = "UPDATE ORGANISATION"
            
            //show the edit buttin for image update
            btnShowHideOfEdit.isHidden = false
            
            //set border for image
            imageView.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
            imageView.layer.borderWidth = 1
            
            //set org image
            let url = URL(string:orgModel.strOrganisation_logo)
            imageView.kf.setImage(with: url)
            
            //set data for the organisation which needs to be updated
            txtCompanyName.text = orgModel.strOrganisation
            txtIndustry.text = orgModel.strIndustry
            txtTurnOver.text = orgModel.strTurnover
            txtAddress.text = orgModel.strAddress1
            txtAddress2.text = orgModel.strAddress2
            txtPhone.text = orgModel.strPhone
            txtName.text = orgModel.strLeadName
            txtPassword.text = orgModel.strLeadEmail
            txtConfirmPassword.text = orgModel.strLeadPhone
            updateArrayOffices = orgModel.arrOffices
            
            if updateArrayOffices.count == 0
            {
                //case when no office
                tblOfficeView.isHidden = true
                HeightForOfficesTable.constant = 0
                lblNoData.isHidden = false
                lblNoData.text = "No office found for this organisation"
            }
            tblOfficeView.isHidden = false
            
            strOrgId = orgModel.strOrganisation_id
            //heightConstriantOfpasswordView.constant = 0
            //viewForpassword.alpha = 0
            
            // txtEmail.isUserInteractionEnabled = false
            // txtName.isUserInteractionEnabled = false
            // txtEmail.textColor = UIColor.gray
            // txtName.textColor = UIColor.gray
            
            btnAddoffices.isHidden = true
        }
    }
    
    //MARK: - IBActions
    /**
     * This function is called when user taps on Tribe logo.
     * This function navigates user on home screen
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when user taps on submit button.
     */
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if strUpdateOrganisation == "TRUE"{
            //update case
            
            //Validate the required fields
            if (txtCompanyName.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Company Name.")
            }
            else if (txtTurnOver.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Company Turn Over.")
            }
            else if (txtIndustry.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Industry.")
            }
            else if (txtAddress.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Address.")
            }
            else if (txtName.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Lead Name.")
            }
            else if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Email.")
            }
            else if(txtPassword.text!.isValidEmail() == false){
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid Lead Email.")
            }
            else if (txtConfirmPassword.text?.isEmpty)! {
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter lead Phone Number.")
            }
            else {
                //All validation passed, so call api to update organisation details
                callWebServiceToUpdateOrganization()
            }
            
        }
        else{
            //add case
            
            //check all required validations
            if imageView.image == #imageLiteral(resourceName: "UPLOADLOGO") {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter organisation logo.")
            }
            else if (txtCompanyName.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Company Name.")
            }
            else if (txtIndustry.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Industry.")
            }
            else if (txtTurnOver.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Company Turn Over.")
            }
            else if (txtAddress.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Address.")
            }
            else if (txtPhone.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Phone Number.")
            }
                
            else if (txtName.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Lead Name.")
            }
            else if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Email.")
            }
            else if(txtPassword.text!.isValidEmail() == false){
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid Lead Email.")
            }
            else if (txtConfirmPassword.text?.isEmpty)! {
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter lead Phone Number.")
            }
                
            else if arrOffices.count == 0 {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter at least one Office.")
            }
            else {
                //if all validation passes, call api to add organisation
                callWebServiceToAddOrganization()
            }
        }
    }
    
    /**
     * custom function which checks the spaces in string and returns a bool accordingly
     */
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    /**
     * This function redirects user to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddOfficeAction(_ sender: Any) {
        
        
    }
    
    /**
     * This function is called when user chooses to update organisation logo
     */
    @IBAction func btnAddAction(_ sender: Any) {
        
        //show popup to ask user whether he wants to upload image via camera or choose it from gallery
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            //when user wants to upload from camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            //when user wants to choose image from gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    //MARK: - Custom Functions
    
    /*Action Sheet Options Function for Uploading File*/
    /**
     *This function opens camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //when type is camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *This function opens gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     * ImagePicker delegate method called when user selects any image from Camera/Gallery
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if strUpdateOrganisation == "TRUE"{
            //update case
            
            //get original image selected
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                //assign image to image view
                imageView.image = image
                imageView.contentMode = .scaleAspectFit //3
                
                //dismiss the picker
                self.dismiss(animated: false, completion: { self.callingWebServiceForUploadLogo()})
            }
                
            else{
                //show error if image not supported
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
                picker.popViewController(animated: true)
                
                print("Something went wrong in  image")
            }
            
        }
        else{
            //add case
            
            //get original image selected
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                //assign image to image view
                imageView.image = image
                
                //dismiss the picker
                self.dismiss(animated: false, completion: nil)
            }
                
            else{
                //show error if image not supported
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
                picker.popViewController(animated: true)
                print("Something went wrong in  image")
            }
            
        }
    }
    
    /**
     * ImagePicker delegate method called when user cancels  image selection view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - Segue Method
    /**
     * Prepare for segue method is called when any segue is called
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddOffices" {
            //case when user redirects to add office screen
            
            //get the destination view controller instance
            let vc = segue.destination as! OfficeDetailsView
            vc.didFinishAddingOffices = { dict in
                //it is a call back method which returns the selected office dict which we append to our array
                self.arrOffices.append(dict)
                print(self.arrOffices)
                self.tblOfficeView.reloadData()
            }
        }
    }
    
    // convert images into base64 and keep them into string
    /**
     *This function is used to convert image to base64 format
     */
    func convertImageToBase64(image: UIImage) -> String {
        //get image data
        var imgData = UIImageJPEGRepresentation(imageView.image!, 1.0)!
        
        //compress it
        if let imageData = imageView.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        
        //returns base64 of image
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //MARK: - Callnig web service
    /**
     *This function is used to upload organisation logo  to server
     */
    func callingWebServiceForUploadLogo(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "organisation_logo":"iVBORwP65GIAAAAASUVORK5CYII=",
         "id":"28"
         }*/
        
        //prepare parameters for api
        let param =
            ["organisation_logo" : convertImageToBase64(image: imageView.image!), //base 64 converted logo image
                "id" : strOrgId //organisation id
                ] as [String : Any]
        
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.updateOrganisationLogo, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message once logo updated
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Organisation logo uploaded successfully")
                    
                }
                else {
                    //show error message in case of fail response
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     *This function is used to add organisation details  to server
     */
    func callWebServiceToAddOrganization() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare param
        let param =
            [    "organisation"    : txtCompanyName.text!, //organisation name
                "industry"        : txtIndustry.text!, //industry
                "turnover"        : txtTurnOver.text!, //turn over
                "address"         : txtAddress.text!, //address
                "address2"        : txtAddress2.text!, //address 2
                "email"           : "",
                "password"        : "",
                "name"            : "",
                "phone"           : txtPhone.text!, //contact no
                "lead_name"       : txtName.text!, //lead person name
                "lead_email"      : txtPassword.text!, //password
                "lead_phone"       : txtConfirmPassword.text!, //confirm password
                "organisation_logo": convertImageToBase64(image: imageView.image!), //logo image in base 64 format
                "offices"         : arrOffices //added office details
                ] as [String : Any]
        
        //    print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.addOrganisation, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message for add organisation
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Organisation added successfully")
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     *This function is used to update organisation details  to server
     */
    func callWebServiceToUpdateOrganization() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare array of updated office
        var arrConverted = [[String:Any]]()
        for arr in updateArrayOffices {
            
            let json = [
                "phone" : arr.strPhone,
                "office_id":arr.strOffice_id,
                "country":arr.strCountry,
                "city":arr.strCity,
                "office":arr.strOffice,
                "address": arr.strAddress,
                "noOfEmployee": arr.strNoOfEmployee,
            ]
            arrConverted.append(json)
        }
        
        //prepare params
        let param =
            [
                "id" : strOrgId, //org id
                "organisation":txtCompanyName.text ?? "", //company name
                "industry":txtIndustry.text ?? "", //industry
                "address":txtAddress.text ?? "", //address
                "turnover":txtTurnOver.text ?? "", //turn over
                "phone": txtPhone.text ?? "", //contact no
                "address2": txtAddress2.text ?? "", //address 2
                "lead_name" : txtName.text!, //lead name
                "lead_email" : txtPassword.text!, //password
                "lead_phone"  : txtConfirmPassword.text!, //confirm password
                "offices": arrConverted //array of office
                ] as [String : Any]
        
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Organisation.updateOrganisation, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    //show success message when organisation updated successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Organisation updated Successfully.")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: Calling Delegate
    /**
     * method which is construct at office detail page and and there will be no reqiremment of calling delegate in cellforRowatindexPath
     */
    func sendNameToPreviousVC(SendUpdatedName: String, SendUpdatedAdd: String, SendUpdatedCity: String, SendUpdatedCountry: String, SendUpdatedPhone: String, SendUpdatedNumberOfEmployees: String , SendUpdatedArrayIndex: Int) {
        
        updateArrayOffices[SendUpdatedArrayIndex].strOffice = SendUpdatedName
        updateArrayOffices[SendUpdatedArrayIndex].strAddress = SendUpdatedAdd
        updateArrayOffices[SendUpdatedArrayIndex].strCity  = SendUpdatedCity
        updateArrayOffices[SendUpdatedArrayIndex].strCountry = SendUpdatedCountry
        updateArrayOffices[SendUpdatedArrayIndex].strPhone = SendUpdatedPhone
        updateArrayOffices[SendUpdatedArrayIndex].strNoOfEmployee = SendUpdatedNumberOfEmployees
        
        tblOfficeView.reloadData()
        print(updateArrayOffices)
        
    }
    // MARK: - UITableView Delegates Methods
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if strUpdateOrganisation == "TRUE"{
            //update case
            return  updateArrayOffices.count
        }
        else{
            //add case
            return arrOffices.count
        }
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //create custom cell for office
        let cell : AddOrganizationOfficeCell = tableView.dequeueReusableCell(withIdentifier: "AddOrganizationOfficeCell") as! AddOrganizationOfficeCell
        
        if strUpdateOrganisation == "TRUE"{
            //update case
            
            //set office name
            cell.lblOfficeName.text = updateArrayOffices[indexPath.row].strOffice
            
            //set address
            let strAddress = updateArrayOffices[indexPath.row].strAddress
            
            //set city
            let strCity = updateArrayOffices[indexPath.row].strCity
            
            //set country
            let strCountry = updateArrayOffices[indexPath.row].strCountry
            
            //set phone
            let strPhone = updateArrayOffices[indexPath.row].strPhone
            
            //set full address
            cell.lblOfficeLocation.text = strAddress + " " + strCity + " " + "(" +  strCountry + ")" + " " + "-" + strPhone
            
            return cell
        }
        else{
            //new office add case
            
            //set office name
            cell.lblOfficeName.text = arrOffices[indexPath.row]["office"] as? String
            
            //get full address details
            let strAddress = arrOffices[indexPath.row]["address"] as? String
            let strCity = arrOffices[indexPath.row]["city"] as? String
            let strCountry = arrOffices[indexPath.row]["country"] as? String
            let strPhone = arrOffices[indexPath.row]["phone"] as? String
            
            //set adress to label
            cell.lblOfficeLocation.text = strAddress! + " " + strCity! + " " + "(" +  strCountry! + ")" + " " + "-" + strPhone!
            
            return cell
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    /**
     * Tableview delegate method called when user selects any row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //custom office cell
        let cell : AddOrganizationOfficeCell = tableView.dequeueReusableCell(withIdentifier: "AddOrganizationOfficeCell") as! AddOrganizationOfficeCell
        
        if strUpdateOrganisation == "TRUE"{
            //update case
            
            cell.selectionStyle = .gray
            cell.isUserInteractionEnabled = true
            
            
            //get office detail instance
            let objVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "OfficeDetailsView") as! OfficeDetailsView
            
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.intIndexPath = indexPath.row
            objVC.mDelegate = self //Include this line
            objVC.updateArrayOffices = updateArrayOffices[indexPath.row]
            objVC.strUpdateOffice = "TRUE"
            
            //redirect to office detail screen in order to update the details for the selected office
            self.navigationController?.present(objVC, animated: false, completion: nil)
        }
            
        else{
            
            cell.isUserInteractionEnabled = false
            cell.selectionStyle = .none
        }
        
    }
    
    
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    /* //MARK:- PHOTO LIBRARY ACCESS CHECK
     func photoLibraryAvailabilityCheck()
     {
     if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
     {
     
     openGallary()
     }
     else
     {
     PHPhotoLibrary.requestAuthorization(requestAuthorizationHandlerForGallery)
     }
     }
     func CameraAvailabilityCheck(){
     if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
     {
     
     openCamera()
     }
     else
     {
     PHPhotoLibrary.requestAuthorization(requestAuthorizationHandlerForCamera)
     }
     
     }
     func requestAuthorizationHandlerForGallery(status: PHAuthorizationStatus)
     {
     if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
     {
     openGallary()
     }
     else
     {
     alertToEncouragePhotoLibraryAccessWhenApplicationStarts()
     }
     }
     func requestAuthorizationHandlerForCamera(status: PHAuthorizationStatus)
     {
     if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
     {
     openCamera()
     }
     else
     {
     alertToEncourageCameraAccessWhenApplicationStarts()
     }
     }
     //MARK:- CAMERA & GALLERY NOT ALLOWING ACCESS - ALERT
     func alertToEncourageCameraAccessWhenApplicationStarts()
     {
     //Camera not available - Alert
     let internetUnavailableAlertController = UIAlertController (title: "Camera Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: .alert)
     
     let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
     let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
     if let url = settingsUrl {
     DispatchQueue.main.async {
     UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) //(url as URL)
     }
     
     }
     }
     let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
     internetUnavailableAlertController .addAction(settingsAction)
     internetUnavailableAlertController .addAction(cancelAction)
     
     self.present(internetUnavailableAlertController, animated: true, completion: nil)
     
     //self.window?.rootViewController!.present(internetUnavailableAlertController , animated: true, completion: nil)
     }
     func alertToEncouragePhotoLibraryAccessWhenApplicationStarts()
     {
     //Photo Library not available - Alert
     let cameraUnavailableAlertController = UIAlertController (title: "Photo Library Unavailable", message: "Please check to see if device settings doesn't allow photo library access", preferredStyle: .alert)
     
     let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
     let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
     if let url = settingsUrl {
     UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
     }
     }
     let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
     cameraUnavailableAlertController .addAction(settingsAction)
     cameraUnavailableAlertController .addAction(cancelAction)
     self.present(cameraUnavailableAlertController, animated: true, completion: nil)
     //self.window?.rootViewController!.present(cameraUnavailableAlertController , animated: true, completion: nil)
     }*/
    
}




