//
//  AddOrganizationOfficeCell.swift/Users/kdstudio/Downloads/Tribe365/Tribe365
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 14/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

/**
 * This is a cell class used to show the details of office in add/update organisation screen.
 */
class AddOrganizationOfficeCell: UITableViewCell {
    
    //show office name
    @IBOutlet var lblOfficeName: UILabel!
    
    //show location/address
    @IBOutlet var lblOfficeLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblOfficeLocation.lineBreakMode = .byWordWrapping
        
        lblOfficeName.numberOfLines = 0
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
