//
//  ChecklistTableViewCell.swift
//  Tribe365
//
//  Created by Apple on 17/04/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

/**
* This class is no longer in use.
 This was used to show the checklist cells
*/
class ChecklistTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblSectionTitle: UILabel!
    @IBOutlet weak var lblRowTitle: UILabel!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
