//
//  ChecklistViewController.swift
//  Tribe365
//
//  Created by Apple on 17/04/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
* This class is no longer in use.
 This was used to show the checklist list (which is now merged with the notifactions list ("first use checklist" & "to do list"))
*/
class ChecklistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tblChecklist: UITableView!
    
    //MARK: - Variables
    var arrChecklist = [ChecklistModel]()
    var arrTodos = [ChecklistModel]()
    var arrSectionTitles = [String]()
    let panel = JKNotificationPanel()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupData()
    }
    
    //MARK: - Custom Methods
    func setupData() {
        
        callWebServiceForChecklist()
        self.tblChecklist.tableFooterView = UIView()
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if section == 0 {
            return arrChecklist.count
        }
        else {
            return arrTodos.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellFirst") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrChecklist[indexPath.row].strTitle
                cell.lblSectionTitle.text = arrSectionTitles[indexPath.section]
                cell.imgStatus.image = arrChecklist[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                return cell
            }
            else if indexPath.row == arrChecklist.count - 1{
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellLast") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrChecklist[indexPath.row].strTitle
                cell.imgStatus.image = arrChecklist[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                return cell
            }
            else {
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellMiddle") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrChecklist[indexPath.row].strTitle
                cell.imgStatus.image = arrChecklist[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                return cell
            }
        }
        else {
            if indexPath.row == 0 {
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellFirst") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrTodos[indexPath.row].strTitle
                cell.lblSectionTitle.text = arrSectionTitles[indexPath.section]
                cell.imgStatus.image = arrTodos[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                if cell.imgStatus.image == UIImage(named: "pending") {
                    cell.imgStatus.blink()
                }
                
                return cell
            }
            else if indexPath.row == arrTodos.count - 1{
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellLast") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrTodos[indexPath.row].strTitle
                cell.imgStatus.image = arrTodos[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                return cell
            }
            else {
                let cell : ChecklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistTableViewCellMiddle") as! ChecklistTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRowTitle.text = arrTodos[indexPath.row].strTitle
                cell.imgStatus.image = arrTodos[indexPath.row].strStatus ? UIImage(named: "completed") : UIImage(named: "pending")
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if arrChecklist[indexPath.row].strStatus {
                switch indexPath.row {
                case 0:
                    let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                    objVC.strOrgID = AuthModel.sharedInstance.orgId
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 1:
                    let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
                    objVC.comingFromReduReviewBtn = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 2:
                    let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                    objVC.isQuestionDestributed = true
                    objVC.isEditMode = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 3:
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
                    objVC.strUpdate = "TRUE"
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 4:
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                    objVC.comingFromReduReviewBtn = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 5:
                    let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
                    objVC.isTribeometerAnsDone = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 6:
                    let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
                    objVC.isDiagnosticAnsDone = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                default:
                    print("Default Case")
                }
            }
            else {
                switch indexPath.row {
                case 0:
                    if AuthModel.sharedInstance.isDot == "true" {
                        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                        objVC.strOrgID = AuthModel.sharedInstance.orgId
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else{
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No DOT Added for this organisation.")
                    }
                case 1:
                    let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                case 2:
                    callWebServiceForCOT()
                case 3:
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
                    
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 4:
                    let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                    
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 5:
                    let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
                    objVC.isTribeometerAnsDone = false
                    self.navigationController?.pushViewController(objVC, animated: true)
                case 6:
                    let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
                    objVC.isDiagnosticAnsDone = false
                    self.navigationController?.pushViewController(objVC, animated: true)
                default:
                    print("Default Case")
                }
            }
        }
        else {
            //            if arrTodos[indexPath.row].strStatus {
            //                return
            //            }
            //            else {
            switch indexPath.row {
            case 0:
                let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                objVC.strOrgID = AuthModel.sharedInstance.orgId
                self.navigationController?.pushViewController(objVC, animated: true)
            case 1:
                let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "viewDOTView") as! viewDOTView
                objVC.strOrgID = AuthModel.sharedInstance.orgId
                self.navigationController?.pushViewController(objVC, animated: true)
            case 2:
                let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensMCQ") as! FunctionalLensMCQ
                objVC.comingFromReduReviewBtn = true
                self.navigationController?.pushViewController(objVC, animated: true)
            case 3:
                let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                objVC.isQuestionDestributed = true
                objVC.isEditMode = true
                self.navigationController?.pushViewController(objVC, animated: true)
            case 4:
                let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
                objVC.strUpdate = "TRUE"
                self.navigationController?.pushViewController(objVC, animated: true)
            case 5:
                let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                objVC.comingFromReduReviewBtn = true
                self.navigationController?.pushViewController(objVC, animated: true)
            case 6:
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "TribeometerView") as! TribeometerView
                objVC.isTribeometerAnsDone = true
                self.navigationController?.pushViewController(objVC, animated: true)
            case 7:
                let objVC = UIStoryboard(name: "Diagnostic", bundle: nil).instantiateViewController(withIdentifier: "DiagnosticClcikView") as! DiagnosticClcikView
                objVC.isDiagnosticAnsDone = true
                self.navigationController?.pushViewController(objVC, animated: true)
            default:
                print("Default Case")
            }
            //            }
        }
    }
    
    //MARK: - Webservice Methods
    func callWebServiceForChecklist() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["userId": AuthModel.sharedInstance.user_id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.checklist, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                DashboardParser.parseChecklist(response: response!, completionHandler: { (checkList, todoList, showTodo, pendingCount) in
                    self.arrChecklist = checkList
                    self.arrTodos = todoList
                    
                    if showTodo {
                        self.arrSectionTitles = ["First Use Checklist", "To Do List"]
                    }
                    else {
                        self.arrSectionTitles = ["First Use Checklist"]
                    }
                    
                    self.tblChecklist.reloadData()
                })
            }
        }
    }
    
    func callWebServiceForCOT() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.COT.isCOTanswerDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS not called",response ?? "")
                    
                    if response!["data"]["isCOTanswered"].boolValue == true {
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "IndividualViewForTeamRoleMap") as! IndividualViewForTeamRoleMap
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "QuestionOptionView") as! QuestionOptionView
                        objVC.isQuestionDestributed = response!["data"]["isQuestionDestributed"].boolValue
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
