//
//  UpdateRatingView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 13/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This is popup class used to show the view for update rating
 */
class UpdateRatingView: UIViewController {
    
    
    //MARK: - IBOutlets
    //Slider for rating
    @IBOutlet var sliderForRating: UISlider!
    
    //rating value
    @IBOutlet var lblRatingValue: UILabel!
    
    //heading
    @IBOutlet var lblRatingName: UILabel!
    
    //description
    @IBOutlet weak var lblPointsData: UILabel!
    
    //height constraint for description
    @IBOutlet weak var heightForlblPointsData: NSLayoutConstraint!
    
    //MARK: -  Varialbles
    
    // These number values represent each slider position
    var numbers = [0 ,1, 2, 3, 4, 5] //Add your values here
    
    //stores old rating index
    var oldIndex = 0
    
    //stores value name
    var strValueName = ""
    
    //stores value id
    var strValueId = ""
    
    //stores belief id
    var strValueBeliefId = ""
    
    //stores dot id
    var strValuesDOTId = ""
    
    //stores previous value rating
    var strRatedValue = ""
    
    //show error/warning messages
    let panel = JKNotificationPanel()
    
    //shared instance
    var  auth = AuthModel.sharedInstance
    
    //stores height of description
    var flotHeightForPointsData = CGFloat()
    var objVC = viewDOTView()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if strRatedValue == ""{
            //if no rating provided earlier, show 0 for that rating and update the description accordingly
            strRatedValue = "0"
            CallUpdatePoints(strIndex: Int(strRatedValue)!)
        }
        else{
            //update the description to the previous rating
            CallUpdatePoints(strIndex: Int(strRatedValue)!)
        }
        
        //set rating text and  name according to previous rating
        lblRatingValue.text = strRatedValue == "" ? "0" : strRatedValue
        lblRatingName.text = strValueName
        strRatedValue == "" ? sliderForRating.setValue(0.0, animated: false) : sliderForRating.setValue(Float(strRatedValue)!, animated: false)
        print(strValueName)
        // slider values go from 0 to the number of values in your numbers array
        let numberOfSteps = Float(numbers.count - 1)
        sliderForRating!.maximumValue = numberOfSteps;
        sliderForRating!.minimumValue = 0;
        
        // As the slider moves it will continously call the -valueChanged:
        sliderForRating!.isContinuous = true; // false makes it call only once you let go
        
        lblPointsData.lineBreakMode = .byWordWrapping
        lblPointsData.numberOfLines = 0
    }
    
    //MARK: - IBActions
    /**
     Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     This function is called when user changes the slider value
     */
    @IBAction func btnSliderAction(_ sender: UISlider) {
        
        // round the slider position to the nearest index of the numbers array
        let index = (Int)(sliderForRating!.value + 0.5);
        sliderForRating?.setValue(Float(index), animated: false)
        let number = numbers[index]; // <-- This numeric value you want
        if oldIndex != index{
            //if previous rating do not match the current rating thehn update the rating value and its description
            print("sliderIndex:\(index)")
            print("number: \(number)")
            oldIndex = index
            lblRatingValue.text = String(number)
            self.CallUpdatePoints(strIndex: number)
        }
        
    }
    
    /**
     This function updates the description of rating according to the selected rating value
     */
    func CallUpdatePoints(strIndex : Int)  {
        
        switch strIndex {
        case 0:
            
            lblPointsData.text =  "0- I do not understand what it means to be " + strValueName
            
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
            
            
        case 1:
            lblPointsData.text =  "1- I understand what it means to be " + strValueName + ", I am never/rarely " + strValueName
            
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
            
        case 2:
            lblPointsData.text =  "2- I understand what it means to be " + strValueName + ", I am " + strValueName + " when I have to be"
            
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
        case 3:
            
            lblPointsData.text =  "3- I understand what it means to be " + strValueName + ", I am " + strValueName + " every day at work"
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
            
        case 4:
            lblPointsData.text =  "4- I understand what it means to be " + strValueName + ", I am " + strValueName + " every day at work, colleagues and clients would describe me as " + strValueName
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
            
        case 5:
            lblPointsData.text =  "5- I understand what it means to be " + strValueName + ", I am " + strValueName + " every day at work, colleagues and clients would describe me as " + strValueName + ", I help others become " + strValueName
            
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
            
            
        default:
            
            lblPointsData.text =  "0- I do not understand what it means to be " + strValueName
            
            flotHeightForPointsData = (lblPointsData.text?.height(withConstrainedWidth: lblPointsData.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
            
            heightForlblPointsData.constant = flotHeightForPointsData
        }
    }
    
    /**
     When user finishes giving rating this function is called which updates the rating to server via api
     */
    @IBAction func btnDoneAction(_ sender: UIButton) {
        //update to server
        callWebServiceForUpdateRating()
    }
    
    /**
     * redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        //        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: - Calling Web service for Updating Ratings
    /**
     * API - update the rating
     */
    func callWebServiceForUpdateRating() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //prepare parameters for api
        let param = ["userId":  auth.user_id  , //user id
            "dotId":  strValuesDOTId  , //dot id corresponding to the value
            "beliefId": strValueBeliefId, //belief id
            "valueId":  strValueId, //value id
            "ratings": lblRatingValue.text ?? "" //updated rating value
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.DOTValuesRatings, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //show success message in case of Ratings updated successfully, dismiss the view & refresh the previous view
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Ratings updated successfully.")
                    //                    self.navigationController?.popViewController(animated: false)
                    self.objVC.callWebServiceDOTDetail()
                    self.dismiss(animated: true, completion: nil)
                    
                }
                else {
                    print(errorMsg ?? "")
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
