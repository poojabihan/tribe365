   //
   //  AddBeliefsFirstCell.swift
   //  Tribe365
   //
   //  Created by kdstudio on 01/06/18.
   //  Copyright © 2018 chetaru. All rights reserved.
   //
   
   import UIKit
   /**
    * This class is no longer in use
    */
   class AddBeliefsFirstCell: UITableViewCell {
    
    @IBOutlet weak var lblBeliefsTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var borderView: UIView!
    var objParent: AddBeliefsView!
    
    var arrValues = [[String: Any]]()
    var updateArrValues = [[String: Any]]()
    
    @IBOutlet weak var btnClickOnDidSelectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.reloadData()
        borderView.layer.borderColor =  ColorCodeConstant.borderLightGrayColor.cgColor
        
        borderView.layer.borderWidth = 1
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 100,height: 30)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
   }
   
   extension AddBeliefsFirstCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if updateArrValues.count != 0{
            
            return updateArrValues.count
            
        }else{
            return arrValues.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextCell", for: indexPath) as! Cell
        
        if updateArrValues.count != 0{
            
            cell.layer.borderColor =  ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.headerLabel.text = updateArrValues[indexPath.row]["name"] as? String
            
            return cell
        }
            
        else{
            
            cell.layer.borderColor =  ColorCodeConstant.borderLightGrayColor.cgColor
            
            cell.headerLabel.text = arrValues[indexPath.row]["name"] as? String
            
            return cell
        }
    }
   }
   
   extension AddBeliefsFirstCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
   }
