//
//  AddValuesView.swift
//  Tribe365
//
//  Created by kdstudio on 01/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

/**
 * This class lists all the values, user can select(add/update) values from this list for their added belief
 * This is super admin module screen
 */
class AddValuesView: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    // MARK: - IBOutlets
    //table view for values list
    @IBOutlet var tblValuesView: UITableView!
    
    //white background rectangle
    @IBOutlet weak var addValuesView: UIView!
    
    //for belief text
    @IBOutlet weak var txtBeliefs: UITextField!
    
    // MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores values list
    var arrOfValuesList = [ValueModel]()
    
    //update value list
    var UpdateArrOfValuesList = [ValueModel]() //[[String:Any]]()
    
    //no longer in use
    var CopyArray = [[String:Any]]()
    var intIndexPath = Int()
    
    //updated beliefs list
    var updateArrayBeliefs = BeliefModel() //[[String:Any]]()
    
    //call back function, returns selected values with there belief
    var didFinishAddingBelief : ((BeliefModel) -> ())?
    
    //value array
    var SendingArraysofValues = [ValueModel]()
    
    //all below fields are used in code, as you go through you will understand better
    var arrSelectedRows:[Int] = []
    var UpdateArrSelectedRows:[Int] = []
    var idValuesDict = [String: Any]()
    var idValueArray = [[String: Any]]()
    var beliefId = ""
    var strDeleteValueId = ""
    var editDOT = ""
    var index_at_Which_value_Unique_id_Which_is_Deleted = Int()
    var addBeliefFromEditDOT = ""
    var strDOTId = ""
    
    // MARK: - ViewLifeCycle    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblValuesView.allowsSelection = false
        
        hideKeyboardWhenTappedAround()
        //remove extra separator lines
        tblValuesView?.tableFooterView = UIView()
        callWebServiceToGetAllValues()
        
        if editDOT == "TRUE" {
            //update case, set all initial details
            txtBeliefs.text = updateArrayBeliefs.strName
            beliefId = updateArrayBeliefs.strId
            UpdateArrOfValuesList = updateArrayBeliefs.arrValues
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        SendingArraysofValues.removeAll()
        
    }
    
    
    // MARK: - IBActions
    /**
     * this function is called when user clicks on done button
     */
    @IBAction func btnDoneAction(_ sender: Any) {
        
        //check if belief name is given
        if (txtBeliefs.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Belief.")
        }
            
            
        else if editDOT == "False"{
            //add case
            if addBeliefFromEditDOT == "TRUE"{
                //when we came to edit the already added belief before
                for (index,value) in self.arrSelectedRows.enumerated() {
                    
                    if value == 1 {
                        //store the value to different array which we need to send back
                        self.SendingArraysofValues.append(self.arrOfValuesList[index])
                        
                    }
                }
                
                print(SendingArraysofValues)
                if SendingArraysofValues.count == 0{
                    //if nothing in arrau ask user to choose atleast one value
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one value.")
                    
                }
                else{
                    //add belief
                    callWebServiceToAddBelief()
                }
                
            }
            
            
        }
            
        else if editDOT == "TRUE"{
            //update case
            
            //update belief
            self.callWebServiceToUpdateBelief()
            
        }
        else{
            
            for (index,value) in self.arrSelectedRows.enumerated() {
                
                if value == 1 {
                    //store the value to different array which we need to send back
                    self.SendingArraysofValues.append(self.arrOfValuesList[index])
                }
            }
            
            if SendingArraysofValues.count == 0{
                //if nothing in arrau ask user to choose atleast one value
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select at least one value.")
                
            }
                
            else{
                self.dismiss(animated: false) {
                    
                    //once belief & values added store it in belief model and send to previous controller
                    
                    let model = BeliefModel()
                    model.strName = self.txtBeliefs.text!
                    model.arrValues = self.SendingArraysofValues
                    
                    //                    var dictBelief = [String:Any]()
                    //                    dictBelief = ["name":self.txtBeliefs.text ?? "","belief_value":self.SendingArraysofValues]
                    
                    //call back method which returns back the added belief & its values
                    self.didFinishAddingBelief?(model)
                }
            }
        }
    }
    
    /**
     * this function is called when user clicks on delete belief button
     */
    @IBAction func btnDeleteAction(_ sender: Any) {
        //confirmation message to delete
        let alertController = UIAlertController(title: "", message: "Are you sure Want to Delete " + updateArrayBeliefs.strName + "?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            //delete the added belief
            self.GoToDeleteBelief()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     * call api to delete belief
     */
    func GoToDeleteBelief(){
        callWebServiceToDeleteBelief()
    }
    
    /**
     * This function sreturns user back to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //value list
        return arrOfValuesList.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell to return value
        let cell : ValuesAddingCell = tableView.dequeueReusableCell(withIdentifier: "ValuesAddingCell") as! ValuesAddingCell
        
        //value name
        cell.lblNewValue.text = arrOfValuesList[indexPath.row].strName
        
        if arrSelectedRows[indexPath.row] == 1
        {
            //if value selected show checkmark
            cell.btnCheckedUnchecked.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            
        }
        else
        {
            //if value not selected show un checked
            cell.btnCheckedUnchecked.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
        
        //set tag & target for checkbox button
        cell.btnCheckedUnchecked.tag = indexPath.row
        cell.btnCheckedUnchecked.addTarget(self, action: #selector(checkBoxSelection(_:)), for: .touchUpInside)
        //  }
        
        return cell
    }
    
    /**
     * This function is called when check box is clicked
     */
    @objc func checkBoxSelection(_ sender:UIButton)
    {
        if editDOT == "TRUE" {
            //update case
            if arrSelectedRows[sender.tag] == 0 {
                //case when checkbox is checked
                
                
                //store one at the selected value position
                arrSelectedRows[sender.tag] = 1
                
                //gett value id
                let strId = String.init(describing: self.arrOfValuesList[sender.tag].strId)
                idValuesDict.updateValue(strId, forKey: "valueId")
                
                //store it in array
                idValueArray.append(idValuesDict)
                if idValueArray.count != 0{
                    //add value
                    callWebServiceToAddValues()
                }
                print("calling API to Add values")
            }
                
                
            else{
                //case when checkbox is unchecked
                print("calling API to Delete Values")
                
                let CheckWhetherTheValueIdIsInJsonArrayValueId = self.arrOfValuesList[sender.tag].strId
                
                //loop through the value list
                for i in (0..<self.UpdateArrOfValuesList.count) {
                    
                    let strValueIdTest = UpdateArrOfValuesList[i].strValueId
                    if(strValueIdTest.contains(CheckWhetherTheValueIdIsInJsonArrayValueId)){
                        strDeleteValueId = UpdateArrOfValuesList[i].strId
                        
                        index_at_Which_value_Unique_id_Which_is_Deleted = i
                        print(strDeleteValueId)
                        
                        //show alert in case user tries to remove any value
                        let alertController = UIAlertController(title: "", message: "Removing this value will remove all its corresponding ratings too. \n Do you want to continue ?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alertController.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
                            //run your function here
                            self.arrSelectedRows[sender.tag] = 0
                            //delete value
                            self.performDeleteAction()
                        }))
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                            //run your function here
                            
                            self.arrSelectedRows[sender.tag] = 1
                            
                        })
                        
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        break
                    }
                }
            }
            self.tblValuesView.reloadData()
            
        }
        else{
            //add case
            
            if arrSelectedRows[sender.tag] == 0 {
                
                arrSelectedRows[sender.tag] = 1
            }
            else{
                arrSelectedRows[sender.tag] = 0
                
            }
            self.tblValuesView.reloadData()
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
    
    
    //New work of date 30 July
    
    func performDeleteAction(){
        
        // call delete api for values
        
        callWebServiceToDeleteValue()
    }
    
    //MARK: - Calling Web service
    /**
     * API - update belief
     */
    func callWebServiceToUpdateBelief() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /*{
         "id":"1",
         "name": "belief 2"
         }*/
        
        //prepare parameter for api
        let param = ["id": beliefId, //belief id of belief which needs to be updated
            "name": self.txtBeliefs.text! ,// belief name
            ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.updateBelief, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("Belief Update Successfully")
                    
                    //dismiss screen when Belief Update Successfully
                    self.dismiss(animated: true, completion:{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationID"), object: nil)
                    })
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - add belief
     */
    func callWebServiceToAddBelief() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /*{
         "name": "belief new testing",
         "dotId": "1",
         "belief_value": [
         {
         "id": 8,
         "name": "Brockwell"
         },
         {
         "id": 7,
         "name": "Honesty"
         }
         ]
         }
         */
        var arrJsonValues = [[String : Any]]()
        
        for value in self.SendingArraysofValues {
            
            let json = [
                "id":value.strId,
                "name":value.strName
            ]
            
            arrJsonValues.append(json)
        }
        
        //prepare parameters for api
        let param = ["name": txtBeliefs.text!, //belief name
            "dotId": strDOTId, //dot id for belief
            "belief_value": arrJsonValues //belief & value json form
            ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.addBelief, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //dismiss screen when Belief added Successfully
                    self.dismiss(animated: true, completion:{
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationID"), object: nil)
                    })
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - delete the added value
     */
    func callWebServiceToDeleteValue() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        
        /*   {
         "id":"1"
         }*/
        
        //prepare param for api and send the id of value to delete
        let param = [ "id": strDeleteValueId ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.deleteDotValue, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //remove the delete value from array list
                    self.UpdateArrOfValuesList.remove(at: self.index_at_Which_value_Unique_id_Which_is_Deleted)
                    
                    print(self.UpdateArrOfValuesList)
                    
                    self.tblValuesView.reloadData()
                    //self.dismiss(animated: false) {
                    
                    //                        var dictBelief = [String:Any]()
                    //                        dictBelief = ["name":self.txtBeliefs.text ?? "","belief_value":self.SendingArraysofValues]
                    //
                    //                        self.didFinishAddingBelief?(dictBelief)
                    //                    }
                    
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - add value
     */
    func callWebServiceToAddValues() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["beliefId": beliefId, //belief id for which value need to be added
            "values": idValueArray, //value ids array
            "beliefName" : "" ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.addValue, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    /*
                     code = 200;
                     data =     (
                     {
                     beliefId = 135;
                     beliefName = "<null>";
                     id = 415;
                     name = Consistency;
                     valueId = 6;
                     }
                     );
                     message = "Belief added successfully";
                     "service_name" = "add-dot-belief";
                     status = 1;
                     */
                    
                    self.idValueArray.removeAll()
                    
                    //append the new added belief to the array list
                    for json in response!["data"].arrayValue{
                        
                        let model = ValueModel()
                        model.strId = json["id"].stringValue
                        model.strValueId = json["valueId"].stringValue
                        model.strName = json["name"].stringValue
                        model.strBeliefId = json["beliefId"].stringValue
                        model.strBeliefName = json["beliefName"].stringValue
                        
                        self.UpdateArrOfValuesList.append(model)
                    }
                    
                    
                    //                    let dictOfNewValue =
                    //                    var arrConverted = [[String:Any]]()
                    //                    for arr in dictOfNewValue {
                    //
                    //                        arrConverted.append(arr.dictionaryObject!)
                    //
                    //                    }
                    //                    self.UpdateArrOfValuesList.append(arrConverted[0])
                    
                    
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    /**
     * API - Load values list
     */
    func callWebServiceToGetAllValues() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.DOT.getBeliefValuesList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse value list
                    DashboardParser.parseValuesList(response: response!, completionHandler: { (arr) in
                        //store it in value array
                        self.arrOfValuesList = arr
                    })
                    
                    
                    if self.editDOT == "TRUE"{
                        //update case
                        
                        //set the already selected values to selected array
                        for i in (0..<self.arrOfValuesList.count){
                            self.arrSelectedRows.append(0)
                            // self.UpdateArrSelectedRows.append(0)
                            let strNewValue = self.arrOfValuesList[i].strId
                            for j in (0..<self.UpdateArrOfValuesList.count)
                            {
                                
                                let strOldValue = self.UpdateArrOfValuesList[j].strValueId
                                
                                if strNewValue == strOldValue
                                {
                                    print("found")
                                    self.idValueArray.removeAll()
                                    self.arrSelectedRows.insert( 1, at: i)
                                    self.tblValuesView.reloadData()
                                    
                                    
                                }
                            }
                            
                        }
                        self.tblValuesView.reloadData()
                        
                    }
                    else{
                        for _ in (0..<self.arrOfValuesList.count)
                        {
                            self.arrSelectedRows.append(0)
                        }
                        
                        self.tblValuesView.reloadData()
                    }
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - Delete belief
     */
    func callWebServiceToDeleteBelief(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //pass belief if to delete
        let param = ["id": updateArrayBeliefs.strId ] as [String : Any]
        /*{
         "id":"1"
         }*/
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.deleteBelief, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //show message of deleted and dismiss the screen
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Belief deleted successfully.")
                    self.dismiss(animated: true, completion:{
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationID"), object: nil)})
                    
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
