//
//  ValuesAddingCell.swift
//  Tribe365
//
//  Created by kdstudio on 01/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell class displays the values names with checkbox button
 */
class ValuesAddingCell: UITableViewCell {
    
    //value name
    @IBOutlet weak var lblNewValue: UILabel!
    
    //checkbox button
    @IBOutlet weak var btnCheckedUnchecked: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
