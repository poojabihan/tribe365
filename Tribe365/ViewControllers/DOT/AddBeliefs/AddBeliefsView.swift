//
//  AddBeliefsView.swift
//  Tribe365
//
//  Created by kdstudio on 01/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * This class is used to add/update mission, vission, focus, beliefs & values for the organisation
 */

class AddBeliefsView: UIViewController, /*, UICollectionViewDelegate, UICollectionViewDataSource,*/ UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    //MARK: - IBOutlets
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //add belief button
    @IBOutlet weak var btnAddBeliefs: UIButton!
    
    //text view for mission
    @IBOutlet weak var txtMission: UITextView!
    
    //text view for vision
    @IBOutlet weak var txtVision: UITextView!
    
    //text view for focus
    @IBOutlet weak var txtFocus: UITextView!
    
    //screen title
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    //height constraint
    @IBOutlet weak var heightConstraintofBeliefView: NSLayoutConstraint!
    
    //view with beief text
    @IBOutlet weak var viewToHideandShowOfBelieg: UIView!
    
    //no data label
    @IBOutlet weak var lblNotData: UILabel!
    
    //delete belief button
    @IBOutlet weak var btnDeleteBelief: UIButton!
    
    //MARK: - Variables
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //organisation id
    var strOrgID = ""
    
    //stores array of beliefs
    var arrBeliefs = [BeliefModel]()
    
    //stores array of values
    var arrValues = [ValueModel]()
    
    //textfield instance
    var textField: UITextField?
    
    //vision text
    var strVision = ""
    
    //mission text
    var strMission = ""
    
    //focus text
    var strFocus = ""
    
    //dot(directing) id
    var strDOTId = ""
    
    //stores array of beliefs which are updated
    var arrOfUpdateBeliefs =  [BeliefModel]()
    
    //stores the indexpath of the belief which needs to updated
    var didSelectIndexpath = 0
    
    //detect if need to update or not
    var editDOT = ""
    
    //selected belief id which needs to be performed any sction
    var selectedBeliefIndex = 0
    
    //collection view stores the vertial listing of the values & beliefs and this stores the height which is calculated according to no. of values
    var calacualtedMaxHeightOfCollection = Int()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(AddBeliefsView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddBeliefsView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //Remove extra separator lines
        tblView?.tableFooterView = UIView()
        tblView.separatorStyle = .none
        
        if editDOT == "TRUE"{
            //update case
            
            //load details
            callWebServiceDOTDetail()
            
            //set the initial details
            txtFocus.text = strFocus
            txtVision.text = strVision
            txtMission.text = strMission
            lblHeaderTitle.text = "UPDATE Directing"
            btnDeleteBelief.isHidden = false
            print(arrOfUpdateBeliefs)
        }
        else{
            //add case
            btnDeleteBelief.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddBeliefsView.callWebServiceDOTDetail), name:NSNotification.Name(rawValue: "NotificationID"), object: nil)
        
    }
    
    
    //MARK: - IBActions
    /**
     * This function is called when we click tribe logo button.
     * It pops user to root view controller that is dashboard
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * This function is called when we try to add new belief.
     */
    @IBAction func btnAddBeliefAction(_ sender: UIButton) {
        
        //send it to add belief
        self.performSegue(withIdentifier: "AddBeliefs", sender: nil)
        
    }
    
    /**
     * This function is called when we want to delete belief.
     */
    @IBAction func btnDeleteBeliefAction(_ sender: UIButton) {
        
        //alert for confirmation of delete
        let alertController = UIAlertController(title: "", message: "Are you sure Want to Delete " + arrOfUpdateBeliefs[selectedBeliefIndex].strName + "?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            self.GoToDeleteBelief()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    /**
     * This function is called user finished adding/updating DOT
     */
    @IBAction func btnFinishAction(_ sender: Any) {
        
        if editDOT == "TRUE"{
            //update case
            //validate all required fields
            if (txtVision.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Vision.")
            }
            else if (txtMission.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Mission.")
            }
            else if (txtFocus.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Focus.")
            }
            else {
                //validation success
                //update the DOT details
                callWebServiceToUpdateDOT()
            }
        }
        else{
            //add case
            //validate all required fields
            if (txtVision.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Vision.")
            }
            else if (txtMission.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Mission.")
            }
            else if (txtFocus.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Focus.")
            }
            else if arrBeliefs.count == 0 {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter at least one Belief.")
            }
            else {
                //validation success
                //add the DOT details
                callWebServiceToAddDOT()
            }
        }
    }
    
    /**
     * this function navigates to previous screen
     */
    @IBAction func btnNavigationBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableViewDelegate and dataSource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell
        let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
        if arrOfUpdateBeliefs.count > 0 {
            //update case
            
            //initialise arrBeliefs with the updated beliefs array
            cell.arrBeliefs = arrOfUpdateBeliefs
            
            //initialise the max height
            cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
            
            //initialise DOT id
            cell.strDOTId = strDOTId
            
            //initialise the edit case
            cell.comingFromEdit = "True"
            
            //load the collection view, all code is written inside the cell(ViewDOTCell) class
            cell.beliefSegueVar = self
            cell.collectionView.reloadData()
            
            return cell
        }
        else{
            //add case
            
            //initialise arrBeliefs with the updated beliefs array
            cell.arrBeliefs = arrBeliefs
            
            //initialise the max height
            cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
            
            //initialise DOT id
            cell.strDOTId = strDOTId
            
            //load the collection view, all code is written inside the cell(ViewDOTCell) class
            cell.collectionView.reloadData()
            return cell
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // return 150 + CGFloat( 50 * calacualtedMaxHeightOfCollection)
        return 200 + CGFloat( 70 * calacualtedMaxHeightOfCollection)
    }
    
    //MARK: - Custom Functions
    /**
     * Call api to delete belief
     */
    func GoToDeleteBelief(){
        callWebServiceToDeleteBelief()
    }
    
    /**
     * Calculates the max height for collection view cell according to values
     */
    func CalculateMaxHeight(){
        if arrOfUpdateBeliefs.count > 0 {
            //update case
            
            //loop through updated beliefs array and get the max count of values from all beliefs
            for i in (0..<self.arrOfUpdateBeliefs.count) {
                
                if i == 0 {
                    calacualtedMaxHeightOfCollection = self.arrOfUpdateBeliefs[i].arrValues.count
                }
                if calacualtedMaxHeightOfCollection < self.arrOfUpdateBeliefs[i].arrValues.count {
                    
                    calacualtedMaxHeightOfCollection = self.arrOfUpdateBeliefs[i].arrValues.count
                    
                    print(calacualtedMaxHeightOfCollection)
                }
            }
        }
        else{
            //add case
            
            //loop through beliefs array and get the max count of values from all beliefs
            for i in (0..<self.arrBeliefs.count) {
                
                if i == 0 {
                    calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                }
                if calacualtedMaxHeightOfCollection < self.arrBeliefs[i].arrValues.count {
                    
                    calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                    
                    print(calacualtedMaxHeightOfCollection)
                }
            }
        }
    }
    
    //MARK: - Segue Method
    /**
     * This function is called wutomatically when any segue is called
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //identify the type of segue and get the destination view controller and pass the respected data
        
        if segue.identifier == "AddBeliefs" {
            
            let vc = segue.destination as! AddValuesView
            
            if editDOT == "TRUE"{
                
                vc.editDOT = "False"
                vc.strDOTId = strDOTId
                vc.addBeliefFromEditDOT = "TRUE"
                
            }
            vc.didFinishAddingBelief = { dict in
                self.arrBeliefs.append(dict)
                self.heightConstraintofBeliefView.constant = 90
                self.viewToHideandShowOfBelieg.isHidden = false
            }
        }
        if segue.identifier == "UpdateBeliefs" {
            
            let vc = segue.destination as! AddValuesView
            vc.intIndexPath = didSelectIndexpath
            vc.editDOT = "TRUE"
            vc.updateArrayBeliefs = arrOfUpdateBeliefs[didSelectIndexpath]
            vc.didFinishAddingBelief = { dict in
                self.arrOfUpdateBeliefs.insert(dict, at: self.didSelectIndexpath)
                print(self.arrOfUpdateBeliefs[self.didSelectIndexpath])
            }
        }
    }
    
    //MARK: - WebService Methods
    /**
     * API - get Dot detail
     */
    @objc func callWebServiceDOTDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameter for api
        let param = [
            "orgId":strOrgID //organisation id
            ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.DOTDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse dot details
                    DashboardParser.parseDOTDetail(response: response!, completionHandler: { (model) in
                        //store array of beliefs
                        self.arrOfUpdateBeliefs = model.arrBelief
                    })
                    
                    if self.arrOfUpdateBeliefs.count == 0{
                        //no data text in case of no beliefs
                        self.lblNotData.text = "Belief and Values are not avaliable."
                        self.lblNotData.isHidden = false
                    }
                    else{
                        //hide no data label
                        self.lblNotData.isHidden = true
                    }
                    
                    self.tblView.reloadData()
                    self.CalculateMaxHeight()
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - delete belief
     */
    func callWebServiceToDeleteBelief(){
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameters for api
        let param = [
            "id": arrOfUpdateBeliefs[selectedBeliefIndex].strId //belief id
            ] as [String : Any]
        /*{
         "id":"1"
         }*/
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.deleteBelief, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    //show success message in case of Belief deleted successfully.
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Belief deleted successfully.")
                    
                    //refresh the details
                    self.callWebServiceDOTDetail()
                    self.selectedBeliefIndex = 0
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - update DOT details
     */
    func callWebServiceToUpdateDOT() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //prepare parameters
        let param = [   "vision": txtVision.text!, //vision text
            "mission": txtMission.text!, //mission text
            "focus": txtFocus.text!, //focus text
            "orgId": strOrgID, //org id
            "dot_id": strDOTId, //dot id to update
            "introductory_information": "", //blank
            "dot_date": "", //blank
            "belief": [] //blank
            ] as [String : Any]
        
        print("params", param)
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            print("----",decoded)
            // you can now cast it with the right type
            if decoded is [String:String] {
                // use dictFromJSON
                print("++++",decoded)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.updateDOT, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //show success message in case of DOT Update Successfully.
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "DOT Update Successfully.")
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - to add new DOT details
     */
    func callWebServiceToAddDOT() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //declare array of json to pass in parameter
        var arrJsonValues = [[String : Any]]()
        
        //loop through beliefs
        for belief in self.arrBeliefs {
            var arrValues = [[String : Any]]()
            
            for value in belief.arrValues {
                let valueJson = [
                    "id" : value.strId,
                    "name" : value.strName
                ]
                arrValues.append(valueJson)
            }
            
            let json = [
                "name" : belief.strName,
                "belief_value" : arrValues
                ] as [String : Any]
            
            arrJsonValues.append(json)
        }
        
        //prepare date
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = formatter.string(from: date)
        
        //prepare param
        let param = ["vision"             :txtVision.text ?? "", //vission text
            "mission"            :txtMission.text ?? "", //mission text
            "focus"              :txtFocus.text ?? "", //focus text
            "orgId"              :strOrgID, //org id
            "introductory_information":"", //blank
            "dot_date"           :strDate, //date
            "belief"             :arrJsonValues //beliefs array
            ] as [String : Any]
        
        print("params", param)
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            print("----",decoded)
            // you can now cast it with the right type
            if decoded is [String:String] {
                // use dictFromJSON
                print("++++",decoded)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.addDOT, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    for vc in (self.navigationController?.viewControllers)! {
                        if vc is CategoryDefinedView {
                            let categroryVC = vc as! CategoryDefinedView
                            categroryVC.isDOTAdded = 1
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                    }
                    
                }
                else {
                    //show error message in case of fail response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    /**
     *Notification which is called when keyboard opens
     */
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    /**
     *Notification which is called when keyboard goes
     */
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    //MARK:- UITextViewDelegates
    /**
     *UITextField delegate which is called when user starts typing
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = ""
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.font = UIFont.systemFont(ofSize: 14)
        }
    }
    
    /**
     *UITextField delegate which is called when types
     */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.text = ""
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.systemFont(ofSize: 14)
        }
    }
}


/*
 
 //    @IBAction func btnEditBeliefAction(_ sender: Any) {
 //        didSelectIndexpath = selectedBeliefIndex
 //        self.performSegue(withIdentifier: "UpdateBeliefs", sender: nil)
 //    }
 
 MARK: - Table view data source
 
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
 if editDOT == "TRUE"{
 
 return arrOfUpdateBeliefs.count
 }
 else{
 
 return arrBeliefs.count
 
 }
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
 let cell : AddBeliefsFirstCell = tableView.dequeueReusableCell(withIdentifier: "AddBeliefsFirstCell") as! AddBeliefsFirstCell
 
 if editDOT == "TRUE"{
 
 
 cell.lblBeliefsTitle.text = "  " + (arrOfUpdateBeliefs[indexPath.row]["name"] as! String) + "  "
 cell.updateArrValues = arrOfUpdateBeliefs[indexPath.row]["belief_value"] as! [[String : Any]]
 
 cell.btnClickOnDidSelectButton.tag = indexPath.row
 cell.btnClickOnDidSelectButton.isUserInteractionEnabled = true
 cell.btnClickOnDidSelectButton.addTarget(self, action: #selector(didSelectAction(_:)), for: .touchUpInside)
 
 cell.collectionView.reloadData()
 if indexPath.row == 2 {
 //To remove seprator from particular cell
 tblView.separatorStyle = .none
 }
 
 return cell
 
 }
 else{
 
 cell.btnClickOnDidSelectButton.isUserInteractionEnabled = false
 
 cell.lblBeliefsTitle.text = "  " + (arrBeliefs[indexPath.row]["name"] as! String) + "  "
 cell.arrValues = arrBeliefs[indexPath.row]["belief_value"] as! [[String : Any]]
 
 if indexPath.row == 2 {
 //To remove seprator from particular cell
 tblView.separatorStyle = .none
 }
 
 return cell
 }
 }
 
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 
 return 105
 
 }
 
 //MARK: - UICollectionView Delegate
 //
 //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 //
 //        if arrOfUpdateBeliefs.count > 0 {
 //            if collectionView == self.collectionViewOfBeliefs{
 //                return arrOfUpdateBeliefs.count
 //            }
 //            else{
 //
 //                return arrValues.count
 //
 //            }
 //        }
 //        else {
 //            if collectionView == self.collectionViewOfBeliefs{
 //                return arrBeliefs.count
 //            }
 //            else{
 //
 //                return arrValues.count
 //
 //            }
 //        }
 //
 //    }
 //
 //    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewDOTCollectionCell", for: indexPath as IndexPath) as! ViewDOTCollectionCell
 //
 //        if collectionView == self.collectionViewOfBeliefs{
 //
 //
 //
 //            if selectedBeliefIndex == indexPath.row {
 //                cell.lblValues.textColor = ColorCodeConstant.darkTextcolor
 //                cell.lblValues.addBottomBorder(color: ColorCodeConstant.darkTextcolor, width: 2)
 //                cell.lblValues.font = UIFont.systemFont(ofSize: 16, weight: .medium)
 //
 //                if arrOfUpdateBeliefs.count > 0 {
 //                    arrValues = arrOfUpdateBeliefs[indexPath.item].arrValues
 //                }
 //                else{
 //                    arrValues = arrBeliefs[indexPath.item].arrValues
 //                }
 //                collectionViewOfValues.reloadData()
 //
 //            }
 //            else{
 //                cell.lblValues.font = UIFont.systemFont(ofSize: 15, weight: .regular)
 //                cell.lblValues.textColor = UIColor(hexString: "9A9A9A")
 //                cell.lblValues.addBottomBorder(color: UIColor(hexString: "FFFFFF"), width: 2)
 //            }
 //
 //            cell.btnBeliefSelected.tag = indexPath.row
 //            cell.btnBeliefSelected.addTarget(self, action: #selector(btnBeliefSelectionAction(_:)), for: .touchUpInside)
 ////            strDOTId  = arrBeliefs[indexPath.item]["dotId"] as! String
 //
 //            if arrOfUpdateBeliefs.count > 0 {
 //                cell.lblValues.text = arrOfUpdateBeliefs[indexPath.item].strName
 //            }
 //            else {
 //                cell.lblValues.text = arrBeliefs[indexPath.item].strName
 //            }
 //
 //            return cell
 //        }
 //        else{
 //
 //            cell.lblValues.textColor = ColorCodeConstant.darkTextcolor
 //            cell.lblValues.text = arrValues[indexPath.item].strName
 //            cell.lblValues.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
 //            cell.lblValues.layer.borderWidth = 1
 //            return cell
 //        }
 //    }
 
 //
 //    @objc func btnBeliefSelectionAction(_ sender: UIButton) {
 //
 //        selectedBeliefIndex = sender.tag
 //
 //    }
 //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 //
 //        if collectionView == collectionViewOfValues{
 //
 //            if AuthModel.sharedInstance.role == "3"{
 //
 //                collectionViewOfValues.isUserInteractionEnabled = true
 //
 //            }
 //            else{
 //
 //            }
 //
 //
 //        }
 //
 //    }
 
 
 //
 //    @objc func didSelectAction(_ sender:UIButton){
 //
 //        didSelectIndexpath = sender.tag
 //        self.performSegue(withIdentifier: "UpdateBeliefs", sender: nil)
 //    }
 //
 */
