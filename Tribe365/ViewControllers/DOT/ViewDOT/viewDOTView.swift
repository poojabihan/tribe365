//
//  viewDOTView.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 12/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * Thisclass is the main class which shows the DOT listing
 */
class viewDOTView: UIViewController,UITableViewDelegate , UITableViewDataSource, BelifesListDelegate{
    
    //MARK: - IBOutlets
    //table view
    @IBOutlet var tblView: UITableView!
    
    //edit dot button
    @IBOutlet weak var btnEditDot: UIButton!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    //organisation logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //show error/warning message
    let panel = JKNotificationPanel()
    
    //stores organisation id
    var strOrgID = ""
    
    //shows array of beliefs
    var arrBeliefs = [BeliefModel]()
    
    //stores values array
    var arrValuesDemo = [ValueModel]()
    
    //stores evidence list, its no longer in use
    var arrEvidenceList = [EvidenceModel]()
    
    //stores vision
    var strVission = ""
    
    //stores mission
    var strMission = ""
    
    //stores dot it
    var strDotId = ""
    
    //stores focus
    var strFocus = ""
    
    //stores height
    var flotheightVar = CGFloat()
    
    //instance of shared instance
    var  auth = AuthModel.sharedInstance
    
    //stores height
    var flotheightVission = CGFloat()
    var flotheightMission = CGFloat()
    var flotheightFocus = CGFloat()
    
    //stores selected items
    var SelectedBeliefIndex = 0
    var selectedBeliefSectionId = ""
    var selectedArrOfValuesForEvidence = [ValueModel]()
    var calacualtedMaxHeightOfCollection = Int()
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if auth.role == kUser{
            //if user logged on, hide the edit button
            btnEditDot.isHidden = true
            
        }
        
        //show organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //load DOT details
        callWebServiceDOTDetail()
        
        //set delegates
        tblView.delegate = self
        tblView.dataSource = self
        tblView.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //load DOT details
        callWebServiceDOTDetail()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: - IBActions
    /**
     Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * When super admin clicks on Edit DOT button this function is called
     */
    @IBAction func btnEditDotAction(_ sender: UIButton) {
        
        //redirect user to AddBeliefsView in order to edit the details
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "AddBeliefsView") as! AddBeliefsView
        
        objVC.strOrgID = strOrgID
        objVC.strVision = strVission
        objVC.strMission = strMission
        objVC.strFocus = strFocus
        objVC.strDOTId = strDotId
        objVC.editDOT = "TRUE"
        objVC.arrOfUpdateBeliefs = arrBeliefs
        
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * redirects user to help screen
     */
    @IBAction func btnHelpAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.dot
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - UITableViewDelegate and dataSource
    /**
     * Tableview delegate method used to return number of sections
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            
            //when first index show the VMF cell which contains mission, vission, focus fields
            let cell : VMFCell = tableView.dequeueReusableCell(withIdentifier: "VMFCell") as! VMFCell
            
            //set text for vision, mission & focus
            cell.lblVision.text = strVission
            cell.lblMission.text = strMission
            cell.lblFocus.text = strFocus
            
            //set tag for vision, mission & focus
            cell.btnVisionEvidence.tag = indexPath.row
            cell.btnMissionEvidence.tag = indexPath.row
            cell.btnFocusEvidence.tag = indexPath.row
            
            //no longer required
            cell.btnVisionEvidence.addTarget(self, action: #selector(self.btnVisionEvidenceAction(_:)), for: .touchUpInside)
            cell.btnMissionEvidence.addTarget(self, action: #selector(self.btnMissionEvidenceAction(_:)), for: .touchUpInside)
            cell.btnFocusEvidence.addTarget(self, action: #selector(self.btnFocusEvidenceAction(_:)), for: .touchUpInside)
            
            //no longer required
            cell.btnAddVisionEvidence.addTarget(self, action: #selector(self.btnAddVisionEvidenceAction(_:)), for: .touchUpInside)
            cell.btnAddMissionEvidence.addTarget(self, action: #selector(self.btnAddMissionEvidenceAction(_:)), for: .touchUpInside)
            cell.btnAddFocusEvidence.addTarget(self, action: #selector(self.btnAddFocusEvidenceAction(_:)), for: .touchUpInside)
            
            //SetUp Height
            flotheightVission = (cell.lblVision.text?.height(withConstrainedWidth: cell.lblVision.frame.size.width, font: UIFont.systemFont(ofSize: 22)))!
            flotheightFocus = (cell.lblFocus.text?.height(withConstrainedWidth: cell.lblFocus.frame.size.width, font: UIFont.systemFont(ofSize: 22)))!
            flotheightMission = (cell.lblMission.text?.height(withConstrainedWidth: cell.lblMission.frame.size.width, font: UIFont.systemFont(ofSize: 22)))!
            
            flotheightVar = flotheightVission + flotheightMission + flotheightFocus + 135.0
            
            if flotheightVission <= 25{
                
                flotheightVission = 35
                
            }
            
            if flotheightFocus <= 25{
                
                flotheightFocus = 35
                
            }
            
            if flotheightMission <= 25{
                
                flotheightMission = 35
                
            }
            
            if flotheightVar <= 225{
                
                flotheightVar = 255
                
            }
            cell.heightForLblVision.constant = flotheightVission
            cell.heightForLblMission.constant = flotheightMission
            cell.heightForLblFocus.constant = flotheightFocus
            
            cell.HeightForVMFView.constant = flotheightVar
            return cell
        }
        else{
            
            //Next cell will be for belief value listing, it contains a collection view which allows horizontal scroll
            let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
            
            cell.arrBeliefs = arrBeliefs
            cell.maxHeight = CGFloat(calacualtedMaxHeightOfCollection)
            cell.strDOTId = strDotId
            cell.collectionView.reloadData()
            cell.myVC = self
            
            //no longer required
            cell.btnBeilefEvidence.tag = indexPath.row
            cell.btnBeilefEvidence.addTarget(self, action: #selector(self.btnBeilefEvidenceAction(_:)), for: .touchUpInside)
            cell.btnAddBeilefEvidence.tag = indexPath.row
            cell.btnAddBeilefEvidence.addTarget(self, action: #selector(self.btnAddBeilefEvidenceAction(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return flotheightVar
        }
        else{
            if AuthModel.sharedInstance.role == kUser{
                return 100 + CGFloat( 80 * calacualtedMaxHeightOfCollection)
            }
            else{
                return 100 + CGFloat( 70 * calacualtedMaxHeightOfCollection)
            }
        }
    }
    
    //MARK: - IBActions of Cell
    /**
     * No longer in use
     */
    @objc func btnAddVisionEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "vision"
        objVC.toAddEvidence = true
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    /**
     * No longer in use
     */
    @objc func btnAddMissionEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "mission"
        objVC.toAddEvidence = true
        self.navigationController?.pushViewController(objVC, animated: false)
        
    }
    
    /**
     * No longer in use
     */
    @objc func btnAddFocusEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "focus"
        objVC.toAddEvidence = true
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    /**
     * No longer in use
     */
    @objc func btnVisionEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "vision"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * No longer in use
     */
    @objc func btnMissionEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "mission"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * No longer in use
     */
    @objc func btnFocusEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "focus"
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     * No longer in use
     */
    @objc func btnBeilefEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "SelectValueView") as! SelectValueView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "belief"
        objVC.arrOfBeliefs = arrBeliefs
        objVC.beliefDelegate = self
        present(objVC, animated: false, completion: nil)
        
    }
    
    /**
     * No longer in use
     */
    @objc func btnAddBeilefEvidenceAction(_ sender: UIButton) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "SelectValueView") as! SelectValueView
        
        objVC.strDOTId = strDotId
        objVC.strSectionName = "belief"
        objVC.arrOfBeliefs = arrBeliefs
        objVC.beliefDelegate = self
        objVC.toAddEvidence = true
        present(objVC, animated: false, completion: nil)
        
    }
    
    //MARK:- Delegate Reciver
    /**
     * No longer in use
     */
    func didSelectBeliefName(recordDOTId: String, recordID: String, recordSectionName: String, recordSelectedValueName: String, toAddEvidence: Bool) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = recordDOTId
        objVC.strSectionName = recordSectionName
        objVC.strSectionId = recordID
        objVC.strSelectedValueName = recordSelectedValueName
        objVC.toAddEvidence =  toAddEvidence
        self.navigationController?.pushViewController(objVC, animated: false)
        
    }
    
    //MARK: - Custom Functions
    /**
     * Calculates height for collection view inside the second cell so that all values can come inside
     */
    func CalculateMaxHeight(){
        for i in (0..<self.arrBeliefs.count) {
            
            if i == 0 {
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
            }
            if calacualtedMaxHeightOfCollection < self.arrBeliefs[i].arrValues.count {
                
                calacualtedMaxHeightOfCollection = self.arrBeliefs[i].arrValues.count
                
                print(calacualtedMaxHeightOfCollection)
            }
            
        }
    }
    
    //MARK: - WebService Methods
    /**
     * API - loads DOT details
     */
    func callWebServiceDOTDetail() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        //pass organisation id for parameters
        let param = ["orgId":strOrgID] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.DOTDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse detail
                    DashboardParser.parseDOTDetail(response: response!, completionHandler: { (model) in
                        
                        //set all the details to required labels
                        self.strVission = "\n" + model.strVision + "\n"
                        self.strMission = "\n" + model.strMission + "\n"
                        self.strFocus = "\n" + model.strFocus + "\n"
                        self.strDotId = model.strId
                        self.arrBeliefs = model.arrBelief
                        
                    })
                    
                    //                    if self.arrBeliefs.count == 0{
                    //                        self.lblNoData.text = "Belief and Values are not avaliable."
                    //                        self.lblNoData.isHidden = false
                    //                    }
                    
                    self.CalculateMaxHeight()
                    self.tblView.reloadData()
                    
                }
                else {
                    //show error message in case of fail response
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
//MARK : - Extensions for String -->> Adding extensions to calculate the string counts w.r.t their font size so that resultant get the lable height after inserting data into it

extension UILabel {
    
    @discardableResult func addBottomBorder(color: UIColor, width: CGFloat) -> UILabel {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width - 5, height: width)
        self.layer.addSublayer(layer)
        return self
    }
}
extension UIButton {
    
    @discardableResult func addBottomBorderOnButton(color: UIColor, width: CGFloat) -> UIButton {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: self.frame.size.height-width - 6, width: self.frame.size.width - 5, height: width)
        self.layer.addSublayer(layer)
        return self
    }
    
}
//Second Comment
//MARK: - UICollectionView Delegate

/*   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 
 if collectionView == self.collectionViewOfBeliefs{
 return arrBeliefs.count
 }
 else{
 
 return calacualtedMaxHeightOfCollection * 90
 
 }
 }
 /* @IBAction func btnEvidenceAction(_ sender: UIButton) {
 
 if sender.tag == 1 {
 
 print("Vision")
 
 let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 
 objVC.strDOTId = strDotId
 objVC.strSectionName = "vision"
 self.navigationController?.pushViewController(objVC, animated: true)
 
 }
 if sender.tag == 2 {
 
 print("Mision")
 let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 
 objVC.strDOTId = strDotId
 objVC.strSectionName = "mission"
 self.navigationController?.pushViewController(objVC, animated: true)
 }
 if sender.tag == 3{
 
 print("Focus")
 let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 
 objVC.strDOTId = strDotId
 objVC.strSectionName = "focus"
 self.navigationController?.pushViewController(objVC, animated: true)
 
 }
 if sender.tag == 4{
 
 print("Belief")
 
 let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 
 objVC.strDOTId = strDotId
 objVC.strSectionName = "belief"
 objVC.strSectionId = selectedBeliefSectionId
 objVC.strSelectedValueName = arrBeliefs[SelectedBeliefIndex].strName
 self.navigationController?.pushViewController(objVC, animated: true)
 }
 if sender.tag == 5{
 
 print("Values")
 
 /* let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "SelectValueView") as! SelectValueView
 
 objVC.strDOTId = strDotId
 objVC.strSectionName = "value"
 objVC.selectedArrOfValuesForEvidence = selectedArrOfValuesForEvidence
 objVC.delegate = self
 present(objVC, animated: false, completion: nil)*/
 }
 
 }
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewDOTCollectionCell", for: indexPath as IndexPath) as! ViewDOTCollectionCell
 
 cell.strBeliefName = arrBeliefs[indexPath.row].strName
 cell.arrValues = arrBeliefs[indexPath.item].arrValues
 cell.maxHeight = calacualtedMaxHeightOfCollection
 cell.RatingDelegate = self
 cell.ValueNameDelegate = self
 cell.ValueEvidenceDelegate = self
 cell.myVC = self
 cell.tblView.reloadData()
 
 return cell
 //if collectionView == self.collectionViewOfBeliefs{
 
 /*if SelectedBeliefIndex == indexPath.row {
 cell.lblValues.textColor = kDarkTextColor
 cell.lblValues.addBottomBorder(color: kDarkTextColor, width: 2)
 cell.lblValues.font = UIFont.systemFont(ofSize: 16, weight: .medium)
 arrValuesDemo = arrBeliefs[indexPath.item].arrValues
 
 //for evidence index 0 selection code
 
 selectedBeliefSectionId = arrBeliefs[SelectedBeliefIndex].strId
 selectedArrOfValuesForEvidence = arrBeliefs[SelectedBeliefIndex].arrValues
 
 collectionViewOfValues.reloadData()
 
 }
 else{
 cell.lblValues.font = UIFont.systemFont(ofSize: 15, weight: .regular)
 cell.lblValues.textColor = UIColor(hexString: "9A9A9A")
 cell.lblValues.addBottomBorder(color: UIColor(hexString: "FFFFFF"), width: 2)
 }
 
 cell.btnBeliefSelected.tag = indexPath.row
 cell.btnBeliefSelected.addTarget(self, action: #selector(btnBeliefSelectionAction(_:)), for: .touchUpInside)
 strDotId  = arrBeliefs[indexPath.item].strDotId
 cell.lblValues.text = arrBeliefs[indexPath.item].strName.uppercased()
 return cell
 
 }*/
 
 //  else{
 
 
 /* cell.lblValues.textColor = kDarkTextColor
 
 if AuthModel.sharedInstance.role == "3" {
 cell.lblValues?.text = arrValuesDemo[indexPath.row].strRating == "" ?  arrValuesDemo[indexPath.row ].strName :  arrValuesDemo[indexPath.row ].strName + " " + "(" + arrValuesDemo[indexPath.row].strRating + ")"
 }
 else{
 cell.lblValues.text = arrValuesDemo[indexPath.item].strName
 }
 
 cell.lblValues.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
 cell.lblValues.layer.borderWidth = 1
 return cell*/
 // }
 
 }
 
 
 
 //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 //
 //
 //        /*    if collectionView == collectionViewOfValues{
 //
 //         if auth.role == "3"{
 //
 //         collectionViewOfValues.isUserInteractionEnabled = true
 //
 //         let detailVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "UpdateRatingView") as! UpdateRatingView
 //
 //         detailVC.strValueName = arrValuesDemo[indexPath.row].strName
 //         detailVC.strValueId = arrValuesDemo[indexPath.row].strId
 //         detailVC.strValueBeliefId = arrValuesDemo[indexPath.row].strBeliefId
 //         detailVC.strValuesDOTId =  arrValuesDemo[indexPath.row].strDotId
 //         detailVC.strRatedValue = arrValuesDemo[indexPath.row].strRating
 //         self.navigationController?.pushViewController(detailVC, animated: true)
 //         }
 //
 //         else{
 //
 //         collectionView.isUserInteractionEnabled = false
 //
 //         }
 //
 //         }*/
 //
 //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewDOTCollectionCell", for: indexPath as IndexPath) as! ViewDOTCollectionCell
 //        cell.contentView.backgroundColor = UIColor.clear
 //
 //    }
 //
 //
 //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
 //        print(flotheightVar + CGFloat( 90 * calacualtedMaxHeightOfCollection))
 //        return flotheightVar + CGFloat( 90 * calacualtedMaxHeightOfCollection)
 //    }
 //MARK: - Custom Methods
 
 //Call for the border and text color of Beliefs Collection
 */
 
 /*   @objc func btnBeliefSelectionAction(_ sender: UIButton) {
 
 SelectedBeliefIndex = sender.tag
 selectedBeliefSectionId = arrBeliefs[SelectedBeliefIndex].strId
 selectedArrOfValuesForEvidence = arrBeliefs[SelectedBeliefIndex].arrValues
 collectionViewOfBeliefs.reloadData()
 }*/
 /* func didSelectItem2(recordDOTId: String, recordID: String, recordSectionName: String, recordSelectedValueName: String) {
 
 let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 
 objVC.strDOTId = recordDOTId
 objVC.strSectionName = recordSectionName
 objVC.strSectionId = recordID
 objVC.strSelectedValueName = recordSelectedValueName
 
 self.navigationController?.pushViewController(objVC, animated: false)
 }*/
 
 //MARK: - Calling Protocols
 
 
 //    func didSelectValueItemForEvidence(recordDOTId: String, recordID: String, recordSectionName: String, recordSelectedValueName: String) {
 //
 //        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
 //
 //        objVC.strDOTId = recordDOTId
 //        objVC.strSectionName = recordSectionName
 //        objVC.strSectionId = recordID
 //        objVC.strSelectedValueName = recordSelectedValueName
 //
 //        self.navigationController?.pushViewController(objVC, animated: false)
 //    }
 //    func didSelectRatingItem(record: String, recordID : String , recordBeliefId : String, recordRatedValue : String) {
 //
 //        let detailVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "UpdateRatingView") as! UpdateRatingView
 //
 //        detailVC.strValueName = record
 //        detailVC.strValueId = recordID
 //        detailVC.strValueBeliefId = recordBeliefId
 //        detailVC.strValuesDOTId =  strDotId
 //        detailVC.strRatedValue = recordRatedValue
 //        self.navigationController?.pushViewController(detailVC, animated: true)
 //
 //    }
 //
 //    func didSelectValueNameItem(dotId: String, valueId: String, id: String, BeliefId: String, valueName: String) {
 //
 //        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTips") as! FunctionalLensTribeTips
 //
 //        objVC.orgID = AuthModel.sharedInstance.orgId
 //        objVC.SOTObj = "DOTReports"
 //        objVC.strDotId = dotId
 //        objVC.strValueId = valueId
 //        objVC.strId = id
 //        objVC.strBeliefId = BeliefId
 //        objVC.strValueName = valueName
 //
 //        self.navigationController?.pushViewController(objVC, animated: true)
 //    }
 //
 //MARK: - To Set the Vision Mission Foucus View
 
 /*  func callToSetView(){
 
 lblvision.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
 lblvision.layer.borderWidth = 0.5
 
 lblFocus.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
 lblFocus.layer.borderWidth = 0.5
 
 lblMission.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
 lblMission.layer.borderWidth = 0.5
 
 lblvision.lineBreakMode = NSLineBreakMode.byWordWrapping
 lblvision.numberOfLines = 0
 
 lblFocus.lineBreakMode = NSLineBreakMode.byWordWrapping
 lblFocus.numberOfLines = 0
 
 lblMission.lineBreakMode = NSLineBreakMode.byWordWrapping
 lblMission.numberOfLines = 0
 
 
 lblvision.text = strVission
 lblMission.text = strMission
 lblFocus.text = strFocus
 
 flotheightVission = (lblvision.text?.height(withConstrainedWidth: lblvision.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 flotheightFocus = (lblFocus.text?.height(withConstrainedWidth: lblFocus.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 flotheightMission = (lblMission.text?.height(withConstrainedWidth: lblMission.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 
 flotheightVar = flotheightVission + flotheightMission + flotheightFocus + 165.0
 
 if flotheightVission <= 25{
 
 flotheightVission = 25
 
 }
 
 if flotheightFocus <= 25{
 
 flotheightFocus = 25
 
 }
 
 if flotheightMission <= 25{
 
 flotheightMission = 25
 
 }
 
 if flotheightVar <= 220{
 
 flotheightVar = 220
 
 }
 heightForLblVision.constant = flotheightVission
 heightForLblMission.constant = flotheightMission
 heightForLblFocus.constant = flotheightFocus
 
 HeightForVMFView.constant = flotheightVar
 
 }*/
 /*
 // MARK: - Table view data source
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
 return arrBeliefs.count + 1
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
 if (indexPath.row == 0) {
 
 let cell : VMFCell = tableView.dequeueReusableCell(withIdentifier: "VMFCell") as! VMFCell
 
 
 cell.lblVision.lineBreakMode = NSLineBreakMode.byWordWrapping
 cell.lblVision.numberOfLines = 0
 cell.lblFocus.lineBreakMode = NSLineBreakMode.byWordWrapping
 cell.lblFocus.numberOfLines = 0
 cell.lblMission.lineBreakMode = NSLineBreakMode.byWordWrapping
 cell.lblMission.numberOfLines = 0
 
 
 cell.lblVision.text = strVission
 cell.lblMission.text = strMission
 cell.lblFocus.text = strFocus
 
 
 flotheightVission = (cell.lblVision.text?.height(withConstrainedWidth: cell.lblVision.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 flotheightFocus = (cell.lblFocus.text?.height(withConstrainedWidth: cell.lblFocus.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 flotheightMission = (cell.lblMission.text?.height(withConstrainedWidth: cell.lblMission.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
 flotheightVar = flotheightVission + flotheightMission + flotheightFocus + 45.0
 
 print(flotheightVar)
 return cell
 }
 
 else{
 
 
 let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
 
 cell.delegate = self
 cell.collectionView.reloadData()
 cell.viewForCell.layer.borderColor =  UIColor(hexString: "#9A9A9A").cgColor
 cell.viewForCell.layer.borderWidth = 1
 cell.lblBeliefsTitle.text = "  " + arrBeliefs[indexPath.row - 1]["name"].stringValue + "  "
 strDotId  = arrBeliefs[indexPath.row - 1]["dotId"].stringValue
 cell.arrForValues = arrBeliefs[indexPath.row - 1]["belief_value"].arrayValue
 
 return cell
 }
 
 }
 
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 
 if (indexPath.row == 0) {
 return flotheightVar
 }
 else{
 return 140
 }
 
 }
 
 func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
 let cell : ViewDOTCell = tableView.dequeueReusableCell(withIdentifier: "ViewDOTCell") as! ViewDOTCell
 
 if indexPath.row == 0 {
 
 cell.selectionStyle = UITableViewCellSelectionStyle.none
 
 }
 
 }
 */
 
 */
