//
//  DOTCellForAdmin.swift
//  Tribe365
//
//  Created by Apple on 23/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell contains values & its rating for admin module
 */
class DOTCellForAdmin: UITableViewCell {
    
    //value name
    @IBOutlet weak var lblValue: UILabel!
    
    //button for value name click
    @IBOutlet weak var btnValueName: UIButton!
    
    //no longer used
    @IBOutlet weak var btnValueEvidence: UIButton!
    @IBOutlet weak var btnValueAddEvidence: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
