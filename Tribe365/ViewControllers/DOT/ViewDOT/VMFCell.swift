//
//  VMFCell.swift
//  Tribe365
//
//  Created by kdstudio on 18/06/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell is used to show the mission, vission, focus of the DOT
 */
class VMFCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //for vision
    @IBOutlet weak var lblVision: UILabel!
    
    //fos mission
    @IBOutlet weak var lblMission: UILabel!
    
    //for focus
    @IBOutlet weak var lblFocus: UILabel!
    
    //all evidencerelated things are not required anymore
    @IBOutlet weak var btnVisionEvidence: UIButton!
    @IBOutlet weak var btnMissionEvidence: UIButton!
    @IBOutlet weak var btnFocusEvidence: UIButton!
    @IBOutlet weak var btnAddVisionEvidence: UIButton!
    @IBOutlet weak var btnAddMissionEvidence: UIButton!
    @IBOutlet weak var btnAddFocusEvidence: UIButton!
    
    //stores heigth for every feild
    @IBOutlet weak var heightForLblVision: NSLayoutConstraint!
    
    @IBOutlet weak var heightForLblMission: NSLayoutConstraint!
    
    @IBOutlet weak var heightForLblFocus: NSLayoutConstraint!
    
    @IBOutlet weak var HeightForVMFView: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //appearance of every field
        lblVision.lineBreakMode = .byWordWrapping
        lblVision.numberOfLines = 0
        lblFocus.lineBreakMode = .byWordWrapping
        lblFocus.numberOfLines = 0
        lblMission.lineBreakMode = .byWordWrapping
        lblMission.numberOfLines = 0
        
        lblVision.layer.borderColor = UIColor.black.cgColor//UIColor(hexString: "9A9A9A").cgColor
        lblVision.layer.borderWidth = 1
        lblVision.cornerRadius = 10
        
        lblFocus.layer.borderColor = UIColor.black.cgColor//UIColor(hexString: "9A9A9A").cgColor
        lblFocus.layer.borderWidth = 1
        lblFocus.cornerRadius = 10
        
        lblMission.layer.borderColor = UIColor.black.cgColor//UIColor(hexString: "9A9A9A").cgColor
        lblMission.layer.borderWidth = 1
        lblMission.cornerRadius = 10
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
