//
//  ViewDOTCell.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 12/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit

//protocol CustomDelegate: class {
//    func didSelectItem(record: String, recordID : String , recordBeliefId : String, recordRatedValue: String)
//}

/**
 * This cell is used to show the vertical listing of values
 */
class ViewDOTCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, RatingDelegate , ValueNameDelegate, ValueEvidenceDelegate{
    
    
    //MARK: - IBOutlets
    //collection view for horizontal scroll
    @IBOutlet var collectionView: UICollectionView!
    
    //finish button
    @IBOutlet weak var btnFinish: UIButton!
    
    //no longer used
    @IBOutlet weak var btnBeilefEvidence: UIButton!
    @IBOutlet weak var btnAddBeilefEvidence: UIButton!
    @IBOutlet weak var NorecordHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    //belief array
    var arrBeliefs = [BeliefModel]()
    
    //values array
    var arrValuesDemo = [ValueModel]()
    
    //belief name
    var strBeliefName = ""
    
    //values array
    var arrValues = [ValueModel]()
    
    //max height of cell
    var maxHeight = CGFloat()
    
    //viewDOTView instance to call its method
    weak var myVC : viewDOTView?
    
    //AddBeliefsView instance
    weak var beliefSegueVar : AddBeliefsView?
    
    //stores selected index
    var selectedIndexTag = Int()
    
    //dot it
    var strDOTId = ""
    
    //stores calculated collection view height
    var calacualtedMaxHeightOfCollection = Int()
    
    //update case or not
    var comingFromEdit = ""
    
    //coming from reports screens or not
    var strComingFromReport = "" 
    //Delegate Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //define delegates for collection view
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - UICollectionViewDelegate and DataSources
    /**
     * UICollectionView delegate method used to return number of rows
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //returns beliefs count
        return arrBeliefs.count
        
    }
    
    /**
     * UICollectionView delegate method used to return cell/rows
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //custom cell which shows the verical listing and for this it stores table view inside it
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewDOTCollectionCell", for: indexPath as IndexPath) as! ViewDOTCollectionCell
        
        //belief name
        cell.strBeliefName = arrBeliefs[indexPath.row].strName
        
        //pass values array to table view for vertical listing & scroll
        cell.arrValues = arrBeliefs[indexPath.item].arrValues
        
        //send tables max height
        cell.maxHeight = calacualtedMaxHeightOfCollection
        
        //set delegates
        cell.RatingDelegate = self
        cell.ValueNameDelegate = self
        cell.ValueEvidenceDelegate = self 
        cell.tblView.reloadData()
        cell.myVC = myVC
        cell.tblView.reloadData()
        
        if self.strComingFromReport == "True"{ //AdminReports
            
            //show rating
            cell.strBeliefRating = arrBeliefs[indexPath.row].strRating
            cell.strComingFromReport = strComingFromReport
        }
            
        else if comingFromEdit == "True"{
            
            if AuthModel.sharedInstance.role == kSuperAdmin{ //admin
                //admin case, can edit
                cell.btnEditBeliefFromCollectionCell.tag = indexPath.row
                cell.btnEditBeliefFromCollectionCell.isHidden = false
                cell.btnEditBeliefFromCollectionCell.isUserInteractionEnabled = true
                
                cell.btnEditBeliefFromCollectionCell.addTarget(self, action: #selector(self.btnEditBeliefAction(_:)), for: .touchUpInside)
            }
            else{
                cell.btnEditBeliefFromCollectionCell.isHidden = true
            }
        }
        return cell
    }
    
    /**
     * UICollectionView delegate method used to return height of cell/rows
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if AuthModel.sharedInstance.role == kUser{
            //user case, returns different size according to need of screen
            if strComingFromReport == "True"{
                
                return CGSize(width: 120.0, height: (90 * maxHeight) + 80)
                
            }
            else {
                return CGSize(width: 115.0, height: (80 * maxHeight) + 50)
            }
        }
            
        else{
            if strComingFromReport == "True"{
                
                return CGSize(width: 90.0, height: (90 * maxHeight) + 80)
                
            }
            else {
                return CGSize(width: 85.0, height: (70 * maxHeight) + 50)
            }
        }
        
    }
    
    /**
     * UICollectionView delegate method called when user clicks on belief
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if comingFromEdit == "True"{
            if AuthModel.sharedInstance.role == kSuperAdmin{//admin
                
                collectionView.isUserInteractionEnabled = true
            }
        }
        else{
            collectionView.isUserInteractionEnabled = false
        }
    }
    
    /**
     * edit function
     */
    @objc func btnEditBeliefAction(_ sender: UIButton) {
        beliefSegueVar?.didSelectIndexpath = sender.tag
        beliefSegueVar?.performSegue(withIdentifier: "UpdateBeliefs", sender: nil)
    }
    
    
    //MARK:- Delegate Reciver
    /**
     *  no longer in use
     */
    func didSelectValueItemForEvidence(recordDOTId: String, recordID: String, recordSectionName: String, recordSelectedValueName: String, toAddEvidence: Bool) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "EvidenceListView") as! EvidenceListView
        
        objVC.strDOTId = recordDOTId
        objVC.strSectionName = recordSectionName
        objVC.strSectionId = recordID
        objVC.strSelectedValueName = recordSelectedValueName
        objVC.toAddEvidence = toAddEvidence
        
        myVC?.navigationController?.pushViewController(objVC, animated: false)
    }
    
    /**
     *  This function is called when user clicks on rating and then it redirects to update rating screen
     */
    func didSelectRatingItem(record: String, recordID : String , recordBeliefId : String, recordRatedValue : String) {
        
        let detailVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "UpdateRatingView") as! UpdateRatingView
        
        detailVC.modalPresentationStyle = .overCurrentContext
        detailVC.strValueName = record
        detailVC.strValueId = recordID
        detailVC.strValueBeliefId = recordBeliefId
        detailVC.strValuesDOTId =  strDOTId
        detailVC.strRatedValue = recordRatedValue
        detailVC.objVC = myVC!
        //        myVC?.navigationController?.pushViewController(detailVC, animated: true)
        
        
        myVC?.navigationController?.present(detailVC, animated: false, completion: nil)
        
        
    }
    
    /**
     *  This function is called when user clicks on value name and then it redirects to user listing to assign kudos
     * no longer in use
     */
    func didSelectValueNameItem(dotId: String, valueId: String, id: String, BeliefId: String, valueName: String) {
        
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTipsDOT") as! FunctionalLensTribeTipsDOT
        
        objVC.orgID = AuthModel.sharedInstance.orgId
        objVC.SOTObj = "DOTReports"
        objVC.strDotId = dotId
        objVC.strValueId = valueId
        objVC.strId = id
        objVC.strBeliefId = BeliefId
        objVC.strValueName = valueName
        
        myVC?.navigationController?.pushViewController(objVC, animated: true)
    }
    
}




