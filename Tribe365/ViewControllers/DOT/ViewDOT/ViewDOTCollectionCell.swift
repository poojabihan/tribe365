//
//  ViewDOTCollectionCell.swift
//  ScreenDesignsForTribe365
//
//  Created by Alok Mishra on 12/06/18.
//  Copyright © 2018 Ruchika. All rights reserved.
//

import UIKit


protocol RatingDelegate: class {
    func didSelectRatingItem(record: String, recordID : String , recordBeliefId : String, recordRatedValue: String)
}

protocol ValueNameDelegate: class {
    
    
    func didSelectValueNameItem( dotId: String, valueId: String, id : String , BeliefId : String, valueName: String)
}

protocol ValueEvidenceDelegate: class {
    func didSelectValueItemForEvidence(recordDOTId: String, recordID : String , recordSectionName: String, recordSelectedValueName : String, toAddEvidence: Bool)
}

/**
 * This cell is used to show the vertical listing of values, it stores table view inside it to show the vertical listing and vertical scroll
 */
class ViewDOTCollectionCell: UICollectionViewCell, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UIActionSheetDelegate{
    
    //MARK: - UIBOutlets
    @IBOutlet weak var btnBeliefSelected: UIButton!
    @IBOutlet var lblValues: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var  btnEditBeliefFromCollectionCell : UIButton!
    
    
    //MARK: - Variables
    //stores belief name
    var strBeliefName = ""
    
    //stores belief rating
    var strBeliefRating = ""
    
    //stores array of values
    var arrValues = [ValueModel]()
    
    //stores max height
    var maxHeight = Int()
    
    //stores instance of viewDOTView
    weak var myVC : viewDOTView?
    
    //stores selected index
    var selectedIndexTag = Int()
    
    //coming from report screen or not
    var strComingFromReport = ""
    
    //CustomDelegate variables
    weak var ValueNameDelegate: ValueNameDelegate?
    weak var RatingDelegate: RatingDelegate?
    weak var ValueEvidenceDelegate: ValueEvidenceDelegate?
    
    //MARK: - UITableViewDelegate and datasources
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //values count
        return  arrValues.count
    }
    
    /**
     * Tableview delegate method used to return number of sections
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     * Tableview delegate method used to return header view
     */
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if strComingFromReport == "True"{
            //from reports
            
            //define header view
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width , height: 80))
            
            headerView.backgroundColor = UIColor.clear
            
            //InnerView
            let headerInnerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width , height: 80))
            
            //headerInnerView appearance
            headerInnerView.layer.borderWidth = 0.4
            headerInnerView.backgroundColor = UIColor(red: 225.0, green: 225.0, blue: 225.0, alpha: 0.6)
            
            //First label For Belief Name
            let label = UILabel()
            label.frame = CGRect.init(x: 1, y: 0, width: headerView.frame.width - 2 , height: 35)
            label.text = "  " + strBeliefName.uppercased() + "  "
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.3
            label.font = UIFont.systemFont(ofSize: 11, weight: .medium)
            label.textColor = ColorCodeConstant.darkTextcolor
            
            headerInnerView.addSubview(label)
            
            //2nd Label sepration
            
            let labelSepration = UILabel()
            labelSepration.frame = CGRect.init(x: label.frame.origin.x + 10, y: label.frame.size.height + 1, width: label.frame.size.width - 20 , height: 0.5)
            labelSepration.backgroundColor = ColorCodeConstant.borderLightGrayColor
            headerInnerView.addSubview(labelSepration)
            
            //3rd Label for belief Rating
            let labelRating = UILabel()
            labelRating.frame = CGRect.init(x: 1, y: label.frame.size.height + 3 , width: headerView.frame.width - 2 , height: 35)
            labelRating.text = "  " + strBeliefRating.uppercased() + "  "
            labelRating.textAlignment = .center
            labelRating.backgroundColor = UIColor.clear
            labelRating.adjustsFontSizeToFitWidth = true
            labelRating.minimumScaleFactor = 0.5
            labelRating.font = UIFont.systemFont(ofSize: 17.0, weight: .medium)
            headerInnerView.addSubview(labelRating)
            
            //4th Label colored
            
            let coloredLabel = UILabel()
            coloredLabel.frame = CGRect.init(x: headerInnerView.frame.origin.x, y: label.frame.size.height + 5 +  labelRating.frame.size.height, width: headerInnerView.frame.size.width , height: 4)
            
            headerInnerView.addSubview(coloredLabel)
            
            //Check for the color of belief Rating
            let strRating = Double(strBeliefRating)
            print(strRating ?? "")
            /*0 : fc0000
             1 : fb6564
             2 : f9bbbb
             3 : a6dbbb
             4 : 71c390
             5 : 25ae59*/
            
            if ((strRating! >= 0.0) && (strRating! <= 1.0)) {
                //set colour 1-2
                // Red        :#fc0000
                labelRating.textColor = UIColor(hexString: "b2b2b2")
                coloredLabel.backgroundColor = UIColor(hexString: "b2b2b2")
                headerInnerView.layer.borderColor = UIColor(hexString: "b2b2b2").cgColor
            }
                
            else if ((strRating! >= 1.0) && (strRating! <= 2.0)) {
                //set colour 1-2
                //Orange     : #fb6564
                labelRating.textColor = UIColor(hexString: "ffa3a6")
                coloredLabel.backgroundColor = UIColor(hexString: "ffa3a6")
                headerInnerView.layer.borderColor = UIColor(hexString: "ffa3a6").cgColor
            }
                
            else if ((strRating! >= 2.0) && (strRating! <= 3.0)) {
                //set colour 2-3
                //Yellow    :#f9bbbb
                labelRating.textColor = UIColor(hexString: "ff8085")
                coloredLabel.backgroundColor = UIColor(hexString: "ff8085")
                headerInnerView.layer.borderColor = UIColor(hexString: "ff8085").cgColor
                
            }
                
            else if ((strRating! >= 3.0) && (strRating! <= 4.0)) {
                //set colour 3-4
                //Light Green  :#a6dbbb
                labelRating.textColor = UIColor(hexString: "ff454b")
                coloredLabel.backgroundColor = UIColor(hexString: "ff454b")
                headerInnerView.layer.borderColor = UIColor(hexString: "ff454b").cgColor
                
            }
            else if ((strRating! >= 4.0) && (strRating! <= 5.0)) {
                //set colour 4-5
                //Dark Green   :#25ae59
                labelRating.textColor = ColorCodeConstant.themeRedColor
                coloredLabel.backgroundColor = ColorCodeConstant.themeRedColor
                headerInnerView.layer.borderColor = ColorCodeConstant.themeRedColor.cgColor
            }
            
            headerView.addSubview(headerInnerView)
            return headerView
        }
        else{
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width , height: 60))
            
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width - 10 , height: headerView.frame.height-10)
            label.textAlignment = .center
            label.backgroundColor = UIColor(red: 225.0, green: 225.0, blue: 225.0, alpha: 1
            )
            label.layer.borderColor = UIColor.black.cgColor//UIColor(hexString: "9a9a9a").cgColor
            label.layer.borderWidth = 1.0
            label.cornerRadius = 10
            label.font = UIFont.boldSystemFont(ofSize: 16)// UIFont.systemFont(ofSize: 18, weight: .medium)
            label.numberOfLines = 3
            label.minimumScaleFactor = 0.5
            label.adjustsFontSizeToFitWidth = true
            label.clipsToBounds = true
            
            //label.numberOfLines = 0
            //label.lineBreakMode = NSLineBreakMode.byWordWrapping
            //        label.adjustsFontSizeToFitWidth = true
            //            label.lineBreakMode = .byWordWrapping
            
            label.textColor = ColorCodeConstant.darkTextcolor
            label.text = strBeliefName
            
            headerView.addSubview(label)
            
            return headerView
        }
    }
    
    /**
     * Tableview delegate method used to return height for header
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if strComingFromReport == "True"{
            return 80
        }
        else{
            return 60
        }
    }
    
    /**
     * Tableview delegate method used to return cell/row
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if AuthModel.sharedInstance.role == kUser{//User
            //user case
            
            //custom cell, set value and its rating. Also sets the background color according to the rating
            let Cell : DOTInnerTableCell = tableView.dequeueReusableCell(withIdentifier: "DOTInnerTableCell") as! DOTInnerTableCell
            
            tblView.separatorStyle = .none
            
            if arrValues[indexPath.row ].strName.contains(" ") {
                Cell.lblValue.numberOfLines = 3
            }
            else {
                Cell.lblValue.numberOfLines = 2
            }
            Cell.lblValue.text = arrValues[indexPath.row ].strName
            
            
            if strComingFromReport == "True"{ //Roprts Admin check
                
                let Cell : DOTInnerTableCell = tableView.dequeueReusableCell(withIdentifier: "DOTInnerTableCell") as! DOTInnerTableCell
                
                tblView.separatorStyle = .none
                if arrValues[indexPath.row ].strName.contains(" ") {
                    Cell.lblValue.numberOfLines = 3
                }
                else {
                    Cell.lblValue.numberOfLines = 2
                }
                Cell.lblValue.text = arrValues[indexPath.row ].strName
                
                if arrValues[indexPath.row].strRating == ""
                {
                    //                    Cell.lblRating.text = "-"
                    Cell.lblValue.text = Cell.lblValue.text! + "\n" + "-"
                    Cell.outerView.backgroundColor = UIColor(hexString: "b7b7b7")
                }
                else{
                    /* 0 : fc0000
                     1 : fb6564
                     2 : f9bbbb
                     3 : a6dbbb
                     4 : 71c390
                     5 : 25ae59*/
                    
                    //                    Cell.lblRating.text = arrValues[indexPath.row].strRating
                    Cell.lblValue.text = Cell.lblValue.text! + "\n" + arrValues[indexPath.row].strRating
                    let strRating = Double(arrValues[indexPath.row].strRating)
                    print(strRating ?? "")
                    
                    if ((strRating! >= 0.0) && (strRating! <= 1.0))/*arrValues[indexPath.row].strRating == "0"*/
                    {
                        if strRating == 0 || strRating == 0.0 || strRating == 0.00{
                            //                            Cell.lblRating.text = "0.00"
                            Cell.lblValue.text = arrValues[indexPath.row ].strName + "\n" + "0.00"
                            
                        }
                        Cell.outerView.backgroundColor = UIColor(hexString: "b2b2b2")
                    }
                    else if ((strRating! >= 1.0) && (strRating! <= 2.0))/*arrValues[indexPath.row].strRating == "1"*/
                    {
                        Cell.outerView.backgroundColor = UIColor(hexString: "ffa3a6")
                    }
                    else if ((strRating! >= 2.0) && (strRating! <= 3.0))/*arrValues[indexPath.row].strRating == "2"*/
                    {
                        Cell.outerView.backgroundColor = UIColor(hexString: "ff8085")
                    }
                    else if ((strRating! >= 3.0) && (strRating! <= 4.0))/*arrValues[indexPath.row].strRating == "3"*/
                    {
                        Cell.outerView.backgroundColor = UIColor(hexString: "ff454b")
                    }
                    else if ((strRating! >= 4.0) && (strRating! <= 5.0))/*arrValues[indexPath.row].strRating == "4"*/
                    {
                        Cell.outerView.backgroundColor = ColorCodeConstant.themeRedColor
                    }
                }
                return Cell
                
            }
                
            else if AuthModel.sharedInstance.role == kUser{
                
                Cell.btnValueName.tag = indexPath.row
                //            Cell.btnRating.tag = indexPath.row
                //                Cell.btnThumbsUp.tag = indexPath.row
                //                Cell.btnAddEvidence.tag = indexPath.row
                //                Cell.btnEvidence.tag = indexPath.row
                
                Cell.btnValueName.addTarget(self, action: #selector(self.btnActionLikeDislike(_:)), for: .touchUpInside)
                //            Cell.btnRating.addTarget(self, action: #selector(self.btnChangeRating(_:)), for: .touchUpInside)
                
                //                Cell.btnThumbsUp.addTarget(self, action: #selector(self.btnLikeAction(_:)), for: .touchUpInside)
                //                Cell.btnAddEvidence.addTarget(self, action: #selector(self.btnAddEvidenceValueAction(_:)), for: .touchUpInside)
                //                Cell.btnEvidence.addTarget(self, action: #selector(self.btnEvidenceValueAction(_:)), for: .touchUpInside)
            }
                
            else{
                Cell.btnValueName.isUserInteractionEnabled = false
                //                Cell.btnRating.isUserInteractionEnabled = false
            }
            
            if arrValues[indexPath.row].strRating == ""
            {
                
                //                Cell.lblRating.text = "-"
                Cell.lblValue.text = Cell.lblValue.text! + "\n" + "-"
                Cell.outerView.backgroundColor = UIColor(hexString: "b7b7b7")
            }
            else{
                /* 0 : fc0000
                 1 : fb6564
                 2 : f9bbbb
                 3 : a6dbbb
                 4 : 71c390
                 5 : 25ae59*/
                //                Cell.lblRating.text = arrValues[indexPath.row].strRating
                Cell.lblValue.text = Cell.lblValue.text! + "\n" + arrValues[indexPath.row].strRating
                let strRating = Double(arrValues[indexPath.row].strRating)
                print(strRating ?? "")
                
                if ((strRating! >= 0.0) && (strRating! <= 1.0))/*arrValues[indexPath.row].strRating == "0"*/
                {
                    if strRating == 0 || strRating == 0.0 || strRating == 0.00{
                        //                        Cell.lblRating.text = "0.00"
                        Cell.lblValue.text = Cell.lblValue.text! + "\n" + "0.00"
                    }
                    //                    Cell.lblRating.backgroundColor = UIColor(hexString: "fc0000")
                    Cell.outerView.backgroundColor = UIColor(hexString: "b2b2b2")
                    
                }
                else if ((strRating! >= 1.0) && (strRating! <= 2.0))/*arrValues[indexPath.row].strRating == "1"*/
                {
                    //                    Cell.lblRating.backgroundColor = UIColor(hexString: "fb6564")
                    Cell.outerView.backgroundColor = UIColor(hexString: "ffa3a6")
                    
                }
                else if ((strRating! >= 2.0) && (strRating! <= 3.0))/*arrValues[indexPath.row].strRating == "2"*/
                {
                    //                    Cell.lblRating.backgroundColor = UIColor(hexString: "f9bbbb")
                    Cell.outerView.backgroundColor = UIColor(hexString: "ff8085")
                    
                }
                else if ((strRating! >= 3.0) && (strRating! <= 4.0))/*arrValues[indexPath.row].strRating == "3"*/
                {
                    //                    Cell.lblRating.backgroundColor = UIColor(hexString: "a6dbbb")
                    Cell.outerView.backgroundColor = UIColor(hexString: "ff454b")
                    
                }
                else if ((strRating! >= 4.0) && (strRating! <= 5.0))/*arrValues[indexPath.row].strRating == "4"*/
                {
                    //                    Cell.lblRating.backgroundColor = UIColor(hexString: "25ae59")
                    Cell.outerView.backgroundColor = ColorCodeConstant.themeRedColor
                    
                }
                /* else if arrValues[indexPath.row].strRating == "5"
                 {
                 Cell.lblRating.backgroundColor = UIColor(hexString: "25ae59")
                 }*/
            }
            return Cell
        }
            
            
        else{
            
            if strComingFromReport == "True"{ //Roprts Admin check
                
                let Cell : DOTInnerTableCell = tableView.dequeueReusableCell(withIdentifier: "DOTInnerTableCell") as! DOTInnerTableCell
                
                tblView.separatorStyle = .none
                
                if arrValues[indexPath.row ].strName.contains(" ") {
                    Cell.lblValue.numberOfLines = 3
                }
                else {
                    Cell.lblValue.numberOfLines = 2
                }
                Cell.lblValue.text = arrValues[indexPath.row ].strName
                
                if arrValues[indexPath.row].strRating == ""
                {
                    //                    Cell.lblRating.text = "-"
                    Cell.lblValue.text = Cell.lblValue.text! + "\n" + "-"
                    Cell.outerView.backgroundColor = UIColor(hexString: "b7b7b7")
                }
                else{
                    /* 0 : fc0000
                     1 : fb6564
                     2 : f9bbbb
                     3 : a6dbbb
                     4 : 71c390
                     5 : 25ae59*/
                    
                    //                    Cell.lblRating.text = arrValues[indexPath.row].strRating
                    Cell.lblValue.text = Cell.lblValue.text! + "\n" + arrValues[indexPath.row].strRating
                    
                    let strRating = Double(arrValues[indexPath.row].strRating)
                    print(strRating ?? "")
                    
                    if ((strRating! >= 0.0) && (strRating! <= 1.0))/*arrValues[indexPath.row].strRating == "0"*/
                    {
                        if strRating == 0 || strRating == 0.0 || strRating == 0.00{
                            //                            Cell.lblRating.text = "0.00"
                            Cell.lblValue.text = Cell.lblValue.text! + "\n" + "0.00"
                            
                        }
                        //                        Cell.lblRating.backgroundColor = UIColor(hexString: "fc0000")
                        Cell.outerView.backgroundColor = UIColor(hexString: "b2b2b2")
                        
                    }
                    else if ((strRating! >= 1.0) && (strRating! <= 2.0))/*arrValues[indexPath.row].strRating == "1"*/
                    {
                        //                        Cell.lblRating.backgroundColor = UIColor(hexString: "fb6564")
                        Cell.outerView.backgroundColor = UIColor(hexString: "ffa3a6")
                        
                    }
                    else if ((strRating! >= 2.0) && (strRating! <= 3.0))/*arrValues[indexPath.row].strRating == "2"*/
                    {
                        //                        Cell.lblRating.backgroundColor = UIColor(hexString: "f9bbbb")
                        Cell.outerView.backgroundColor = UIColor(hexString: "ff8085")
                    }
                    else if ((strRating! >= 3.0) && (strRating! <= 4.0))/*arrValues[indexPath.row].strRating == "3"*/
                    {
                        //                        Cell.lblRating.backgroundColor = UIColor(hexString: "a6dbbb")
                        Cell.outerView.backgroundColor = UIColor(hexString: "ff454b")
                    }
                    else if ((strRating! >= 4.0) && (strRating! <= 5.0))/*arrValues[indexPath.row].strRating == "4"*/
                    {
                        //                        Cell.lblRating.backgroundColor = UIColor(hexString: "25ae59")
                        Cell.outerView.backgroundColor = ColorCodeConstant.themeRedColor
                    }
                }
                return Cell
                
            }
            else{
                
                let Cell : DOTCellForAdmin = tableView.dequeueReusableCell(withIdentifier: "DOTCellForAdmin") as! DOTCellForAdmin
                
                tblView.separatorStyle = .none
                if arrValues[indexPath.row ].strName.contains(" ") {
                    Cell.lblValue.numberOfLines = 3
                }
                else {
                    Cell.lblValue.numberOfLines = 2
                }
                Cell.lblValue.text = arrValues[indexPath.item].strName
                //            Cell.btnValueName.tag = indexPath.row
                //            Cell.btnValueName.addTarget(self, action: #selector(self.btnValueEvidenceAction(_:)), for: .touchUpInside)
                
                
                
                Cell.lblValue.layer.borderColor = UIColor.black.cgColor //UIColor(hexString: "9A9A9A").cgColor
                Cell.lblValue.layer.borderWidth = 1
                Cell.lblValue.layer.cornerRadius = 10
                return Cell
            }
        }
    }
    
    /**
     * UIScrollView delegate method used to handle scroll
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var sectionHeaderHeight = CGFloat()
        sectionHeaderHeight = 40
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y >= 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    
    /**
     * Tableview delegate method used to return height of row/cell
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if AuthModel.sharedInstance.role == kUser
        {
            if strComingFromReport == "True"{
                return 90
            }
            else{
                return 80
            }
        }
            
        else{
            if strComingFromReport == "True"{
                return 90
            }
            else{
                return 60
            }
        }
    }
    
    //MARK: - IBActions
    /**
     * no longer in use
     */
    @objc func btnValueAddEvidenceAction(_ sender: UIButton) {
        self.selectedIndexTag = sender.tag
        self.OpenEvidenceForAdminView(toAddEvidence: true)
    }
    
    /**
     * no longer in use
     */
    @objc func btnValueEvidenceAction(_ sender: UIButton) {
        
        self.selectedIndexTag = sender.tag
        self.OpenEvidenceForAdminView(toAddEvidence: false)
        
        /*return
         
         let alert = UIAlertController(title: "", message: "PLEASE SELECT AN OPTION", preferredStyle: .actionSheet)
         
         alert.addAction(UIAlertAction(title: "EVIDENCE", style: .default , handler:{ (UIAlertAction)in
         print("User click Evidence button")
         self.selectedIndexTag = sender.tag
         self.OpenEvidenceForAdminView(toAddEvidence: false)
         }))
         
         alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler:{ (UIAlertAction)in
         //self.myVC?.dismiss(animated: false, completion: nil)
         print("User click Dismiss button")
         }))
         
         myVC?.present(alert, animated: true, completion: {
         print("completion block")
         })
         */
        
    }
    
    /**
     * no longer in use
     */
    func OpenEvidenceForAdminView(toAddEvidence: Bool){
        
        ValueEvidenceDelegate?.didSelectValueItemForEvidence(recordDOTId: arrValues[selectedIndexTag].strDotId, recordID:arrValues[selectedIndexTag].strId , recordSectionName: "value", recordSelectedValueName: arrValues[selectedIndexTag].strName, toAddEvidence: toAddEvidence)
        
    }
    
    /**
     * no longer in use
     */
    @objc func btnLikeAction(_ sender: UIButton) {
        self.selectedIndexTag = sender.tag
        self.OpenBubbleView()
    }
    
    /**
     * no longer in use
     */
    @objc func btnAddEvidenceValueAction(_ sender: UIButton) {
        self.selectedIndexTag = sender.tag
        self.OpenEvidenceView(toAddEvidence: true)
    }
    
    /**
     * no longer in use
     */
    @objc func btnEvidenceValueAction(_ sender: UIButton) {
        self.selectedIndexTag = sender.tag
        self.OpenEvidenceView(toAddEvidence: false)
    }
    
    /**
     * no longer in use
     */
    @objc func btnActionLikeDislike(_ sender: UIButton) {
        
        RatingDelegate?.didSelectRatingItem(record: arrValues[sender.tag].strName, recordID: arrValues[sender.tag].strId, recordBeliefId: arrValues[sender.tag].strBeliefId, recordRatedValue: arrValues[sender.tag].strRating)
        
        /*return
         
         let alert = UIAlertController(title: "", message: "PLEASE SELECT AN OPTION", preferredStyle: .actionSheet)
         
         alert.addAction(UIAlertAction(title: "EVIDENCE", style: .default , handler:{ (UIAlertAction)in
         print("User click Evidence button")
         self.selectedIndexTag = sender.tag
         self.OpenEvidenceView(toAddEvidence: false)
         }))
         
         alert.addAction(UIAlertAction(title: "BUBBLE RATING", style: .default , handler:{ (UIAlertAction)in
         
         print("User click Bubble Rating button")
         self.selectedIndexTag = sender.tag
         self.OpenBubbleView()
         }))
         
         
         alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler:{ (UIAlertAction)in
         // self.myVC?.dismiss(animated: false, completion: nil)
         print("User click Dismiss button")
         }))
         
         myVC?.present(alert, animated: true, completion: {
         print("completion block")
         })
         */
    }
    
    /**
     * called at the time of change rating and calls delegate
     */
    @objc func btnChangeRating(_ sender: UIButton){
        
        RatingDelegate?.didSelectRatingItem(record: arrValues[sender.tag].strName, recordID: arrValues[sender.tag].strId, recordBeliefId: arrValues[sender.tag].strBeliefId, recordRatedValue: arrValues[sender.tag].strRating)
        
    }
    
    //MARK: - Calling Delegate
    /**
     * no longer in use
     */
    func OpenBubbleView(){
        
        ValueNameDelegate?.didSelectValueNameItem(dotId: arrValues[selectedIndexTag].strDotId, valueId: arrValues[selectedIndexTag].strValueId, id: arrValues[selectedIndexTag].strId, BeliefId: arrValues[selectedIndexTag].strBeliefId, valueName: arrValues[selectedIndexTag].strName )
    }
    
    /**
     * no longer in use
     */
    func OpenEvidenceView(toAddEvidence: Bool){
        
        ValueEvidenceDelegate?.didSelectValueItemForEvidence(recordDOTId: arrValues[selectedIndexTag].strDotId, recordID:arrValues[selectedIndexTag].strId , recordSectionName: "value", recordSelectedValueName: arrValues[selectedIndexTag].strName, toAddEvidence: toAddEvidence)
    }
    
}


//extension ClosedRange {
//    func clamp(_ value : Bound) -> Bound {
//        return self.lowerBound > value ? self.lowerBound
//            : self.upperBound < value ? self.upperBound
//            : value
//    }
//}
