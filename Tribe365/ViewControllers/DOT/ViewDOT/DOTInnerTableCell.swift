
//
//  DOTInnerTableCell.swift
//  Tribe365
//
//  Created by Apple on 22/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This cell contains values & its rating
 */
class DOTInnerTableCell: UITableViewCell {
    
    //for value name
    @IBOutlet weak var lblValue: UILabel!
    
    //for value rating
    @IBOutlet weak var lblRating: UILabel!
    
    //outer square box
    @IBOutlet weak var outerView: UIView!
    
    //button for value name click
    @IBOutlet weak var btnValueName: UIButton!
    
    //button for value rating click
    @IBOutlet weak var btnRating: UIButton!
    
    //no longer used
    @IBOutlet weak var btnThumbsUp: UIButton!
    @IBOutlet weak var btnAddEvidence: UIButton!
    @IBOutlet weak var btnEvidence: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
