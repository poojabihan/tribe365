//
//  DOTReportsInnerTblViewCell.swift
//  Tribe365
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * Cell which shows the kudos count corresponding to value
 */
class DOTReportsInnerTblViewCell: UITableViewCell {

    //like image
    @IBOutlet weak var imgLike: UIImageView!
    
    //value name
    @IBOutlet weak var lblValueName: UILabel!
    
    //not used
    @IBOutlet weak var imgDislike: UIImageView!
    @IBOutlet weak var lblDislikes: UILabel!
    
    //like count
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblBottomLine: UILabel!
    
    @IBOutlet weak var MainViewBottomContraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
