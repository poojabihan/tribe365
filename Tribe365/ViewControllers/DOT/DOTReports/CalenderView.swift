//
//  CalenderView.swift
//  Tribe365
//
//  Created by Apple on 29/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import AKMonthYearPickerView

/**
 * Third party AKMonthYearPickerView, which shows year and month picker view
 */
class CalenderView: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AKMonthYearPickerView.sharedInstance.barTintColor =   UIColor.blue
        
        AKMonthYearPickerView.sharedInstance.previousYear = 4
        
    }
    
    
    
    @IBAction func btnDimissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        //
        //        datepicker.onDateSelected = { (month: Int, year: Int) in
        //            let string = String(format: "%02d/%d", month, year)
        //            NSLog(string) // should show something like 05/2015
        //        }
        
        
        
        self.dismiss(animated: false, completion: nil)
        // previewPageObj.PickerContainer.isHidden = true
        
    }
    
    
}
