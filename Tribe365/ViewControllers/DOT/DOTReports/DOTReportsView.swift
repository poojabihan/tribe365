//
//  DOTReportsView.swift
//  Tribe365
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import AKMonthYearPickerView

/**
 * This class shows the total kudos corresponding to every value, which user has received till now
 */
class DOTReportsView: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UISearchBarDelegate  {
    
    // MARK: -IBOutlets
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //the background transparent view
    @IBOutlet weak var alphaView: UIView!
    
    //date label
    @IBOutlet weak var lblDate: UILabel!
    
    //picker view for year and month
    @IBOutlet weak var pickerView: UIPickerView!
    
    //calendar view
    @IBOutlet weak var calenderView: UIView!
    //Add User selection view IBOutlet
    @IBOutlet weak var addOptionView: UIView!
    
    //continue button
    @IBOutlet weak var btnContinue: UIButton!
    
    //heading
    @IBOutlet weak var lblHeadingOptionView: UILabel!
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    //option view
    @IBOutlet var tblOptions: UITableView! //Users table
    
    //selectd user name
    @IBOutlet var lblSelectedUser: UILabel!
    
    //height constraint
    @IBOutlet var filterUserHeight: NSLayoutConstraint!
    
    //org logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    // MARK: -Variables
    //belief array
    var arrBeliefs = [ReportsBeliefModel]()
    
    //values array
    var arrValuesDemo = [ReportsValueModel]()
    
    //dot it
    var strDOTId = ""
    
    //show error/warning message
    var panel = JKNotificationPanel()
    
    //not in use
    var calacualtedMaxHeightOfCollection = Int()
    
    //cell height
    var heightForCell = CGFloat()
    
    //stores selected year picker
    var selectedYear = ""
    
    //stores selected month from picker
    var selectedMonth = ""
    
    //detects whether to show the values according to filter or not
    var showAutomatic = false
    
    //user list array
    var arrOfGetUserList = [UserModel]()
    
    //array of searched result for users
    var arrFilteredUserList = [UserModel]()
    
    //selected user index
    var selectedUserIndex = -1
    
    //selected organisation id
    var selectedOrgID = ""
    
    //variables For Calender View
    var monthsArray = [String]()
    var newArray: [String] = []
    var yearsArray = [String]()
    var selectedIndexForYears = 0
    var selectedIndexForMonths = 0
    var selectedNotificationDate = ""
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //set delegate for search bar, table view & picker view
        searchBar.delegate = self
        
        tblView.delegate = self
        tblView.dataSource = self
        
        alphaView.alpha = 0.8
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        SetArrayForMonth()
        alphaView.alpha = 0.8
        calenderView.alpha = 1.0
        pickerView.reloadAllComponents()
        
        if showAutomatic {
            //if we need to set the date which is been passed feom previous screen and load the details
            
            //create months array
            let arr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            
            alphaView.alpha = 0
            calenderView.alpha = 0
            print("Selected Year == ",yearsArray[selectedIndexForYears])
            print("Selected Month == ",monthsArray[selectedIndexForMonths])
            selectedYear = String(selectedNotificationDate.components(separatedBy: " ")[0].components(separatedBy: "-")[0])
            selectedMonth = String(selectedNotificationDate.components(separatedBy: " ")[0].components(separatedBy: "-")[1])
            lblDate.text = String(arr[Int(selectedMonth)! - 1]) + ", " +  selectedYear
            callWebServiceDOTReports()
        }
        else {
            //load initial details
            alphaView.alpha = 0
            calenderView.alpha = 0
            callWebServiceDOTReports()
        }
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            //super admin case load user list
            callWebServiceToUserList()
            filterUserHeight.constant = 40.0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        //
        //        //        searchBar.backgroundColor = UIColor(hexString: "e4e4e4")
        //        //        searchTextField.backgroundColor = UIColor(hexString: "e4e4e4")
        //        searchBar.layer.cornerRadius = 18
        //        searchTextField.borderStyle = .none
        //        searchTextField.font = UIFont.systemFont(ofSize: 13.0)
        //        searchTextField.textAlignment = NSTextAlignment.left
        //        let image:UIImage = UIImage(named: "search")!
        //        let imageView:UIImageView = UIImageView.init(image: image)
        //        searchTextField.leftView = nil
        //        searchTextField.placeholder = " Search"
        //        searchTextField.leftView = imageView
        //        searchTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    //MARK: - Custom function
    /**
     * To get array of months according to year selected, no longer in use
     */
    func SetArrayForMonth(){
        
        //clear all the array
        monthsArray.removeAll()
        yearsArray.removeAll()
        newArray.removeAll()
        
        //current year name
        let nameOfYear = Calendar.current.component(.year, from: Date())
        for i in 0..<5 {
            yearsArray.append("\(nameOfYear - i)")
        }
        if yearsArray[selectedIndexForYears] == String(nameOfYear)
        {
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            let nameOfMonth = dateFormatter.string(from: now)
            monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            
            let preFromCurrentMonth = Int(nameOfMonth)! - 01
            //            let indexcount = monthsArray.count - preFromCurrentMonth
            
            let minRange = preFromCurrentMonth
            //            let maxRange = indexcount + 1
            
            for i in 0..<monthsArray.count {
                if i >= minRange /*&& i <= maxRange*/ {
                    /// Avoid
                    continue
                }
                newArray.append(monthsArray[i])
            }
            
            monthsArray = newArray
            pickerView.selectRow(0, inComponent: 0, animated: true)
            pickerView.reloadAllComponents()
        }
        else{
            monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            pickerView.reloadAllComponents()
            
        }
    }
    
    //MARK: - IBActions
    /**
     * No longer in use
     */
    @IBAction func btnCalenderAction(_ sender: Any) {
        
        alphaView.alpha = 0.8
        calenderView.alpha = 1.0
        SetArrayForMonth()
        pickerView.reloadAllComponents()
    }
    
    /*  private func doneHandler() {
     alphaView.alpha = 0
     if selectedYear == ""{
     
     selectedYear = String(Calendar.current.component(.year, from: Date()))
     print(selectedYear)
     
     let now = Date()
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM"
     let nameOfMonth = dateFormatter.string(from: now)
     print(nameOfMonth)
     
     if nameOfMonth == "01"{
     dateFormatter.dateFormat = "yyyy"
     let nameOfYear = dateFormatter.string(from: now)
     print(nameOfYear)
     selectedYear = String(Int(nameOfYear)! - 1)
     print(selectedYear)
     }
     else{
     dateFormatter.dateFormat = "yyyy"
     let nameOfYear = dateFormatter.string(from: now)
     print(nameOfYear)
     }
     }
     if selectedMonth == ""{
     
     let now = Date()
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM"
     let nameOfMonth = dateFormatter.string(from: now)
     print(nameOfMonth)
     if nameOfMonth == "01"{
     
     selectedMonth = "01"
     // selectedMonthDisplay = "Jan"
     }
     else{
     selectedMonth = String(Int(nameOfMonth)! - 1)
     
     
     print(selectedMonth)
     }
     }
     print("Month picker Done button action")
     
     let fmt = DateFormatter()
     let monthName = fmt.monthSymbols[Int(selectedMonth)! - 1]
     
     lblDate.text = monthName + ", " +  selectedYear
     callWebServiceDOTReports()
     }*/
    
    /*private func completetionalHandler(month: Int, year: Int) {
     print( "month = ", month, " year = ", year )
     selectedYear = String(year)
     selectedMonth = String(month)
     }*/
    
    /**
     * No longer in use
     */
    @IBAction func btnCalenderSaveAction(_ sender: Any) {
        
        alphaView.alpha = 0
        calenderView.alpha = 0
        print("Selected Year",yearsArray[selectedIndexForYears])
        print("Selected Month",monthsArray[selectedIndexForMonths])
        selectedYear = yearsArray[selectedIndexForYears]
        selectedMonth = String(selectedIndexForMonths + 1)
        lblDate.text = monthsArray[selectedIndexForMonths] + ", " +  selectedYear
        callWebServiceDOTReports()
    }
    
    /**
     * No longer in use
     */
    @IBAction func btnCalenderCancelAction(_ sender: Any) {
        alphaView.alpha = 0
        calenderView.alpha = 0
        
        
    }
    
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * user list action
     */
    @IBAction func btnAddUsersAction(sender: UIButton) {
        lblHeadingOptionView.text = "Select User"
        tblOptions.reloadData()
        alphaView.alpha = 0.8
        addOptionView.isHidden = false
    }
    
    //When user selects any user this function is called
    @objc func btnUserSelectAction(sender: UIButton) {
        selectedUserIndex = sender.tag
        tblOptions.reloadData()
        lblSelectedUser.text = arrFilteredUserList[selectedUserIndex].strName
        alphaView.alpha = 0
        addOptionView.isHidden = true
        
        callWebServiceDOTReports()
    }
    
    /**
     * Close user list
     */
    @IBAction func btnCloseOptionAction(sender: UIButton) {
        
        searchBar.text = ""
        arrFilteredUserList = arrOfGetUserList
        alphaView.alpha = 0
        addOptionView.isHidden = true
    }
    
    //MARK: - UIpicker view Delelgate and Datasource
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if component == 0 {
            return monthsArray.count
        }
        else{
            return yearsArray.count
        }
    }
    
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return monthsArray[row]
        case 1:
            return "\(yearsArray[row])"
        default:
            return nil
        }
        
    }
    /**
     * No longer in use
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 1 {
            selectedIndexForYears = pickerView.selectedRow(inComponent: 1)
            selectedIndexForMonths = 0
            pickerView.selectRow(0, inComponent: 0, animated: true)
            print(selectedIndexForYears)
            SetArrayForMonth()
        }
            
        else if component == 0 {
            
            selectedIndexForMonths = pickerView.selectedRow(inComponent: 0)
            print(selectedIndexForMonths)
            if selectedIndexForMonths == 0
            {
                SetArrayForMonth()
            }
            
        }
    }
    
    //MARK: - UItableViewDelegate and DataSource
    /**
     * Tableview delegate method used to return number of sections
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblOptions {
            return 1
        }
        
        //every belief has its section which contains rows of values
        return arrBeliefs.count
    }
    
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblOptions {
            return arrFilteredUserList.count
        }
        
        //values rows for every belief section
        return  arrBeliefs[section].arrValues.count
    }
    
    /**
     * Tableview delegate method used to return view for section
     */
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == tblOptions {
            return UIView()
        }
        
        //header for section which return belief name on top, under which all values list for particular belief is listed
        let headerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 55))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 1, y: 5, width: headerView.frame.width - 2 , height: headerView.frame.height-10)
        
        label.text = "   " + arrBeliefs[section].strName.uppercased()
        label.textAlignment = .left
        label.backgroundColor = UIColor(hexString:"E9E9E8")
        label.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        label.layer.borderWidth = 1.0
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)// my custom font
        label.textColor = ColorCodeConstant.darkTextcolor // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    /**
     * Tableview delegate method used to return footer
     */
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if tableView == tblOptions {
            return UIView()
        }
        let footerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 10))
        
        return footerView
    }
    
    /**
     * Tableview delegate method used to return height of section header
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblOptions {
            return 0
        }
        
        return 50
    }
    
    /**
     * Tableview delegate method used to return height of footer
     */
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tblOptions {
            return 0
        }
        return 15
    }
    
    /**
     * Tableview delegate method used to return rows/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //option table view
        if tableView == tblOptions {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTableViewCellIndi") as! ValuesTableViewCell
            //To remove seprator from particular cell
            tableView.separatorStyle = .none
            cell.selectionStyle = .none
            
            let index1 = indexPath.row
            cell.lblValue2.text = arrFilteredUserList[index1].strName
            cell.btnValue2.tag = index1
            cell.btnValue2.addTarget(self, action: #selector(btnUserSelectAction(sender:)), for: .touchUpInside)
            if selectedUserIndex == indexPath.row {
                cell.btnValue2.backgroundColor = ColorCodeConstant.themeRedColor
                cell.lblValue2.textColor = UIColor.white
            }
            else {
                cell.btnValue2.backgroundColor = UIColor.clear
                cell.lblValue2.textColor = ColorCodeConstant.darkTextcolor
            }
            
            return cell
            
        }
        
        //custom cell for the values
        let Cell : DOTReportsInnerTblViewCell = tableView.dequeueReusableCell(withIdentifier: "DOTReportsInnerTblViewCell") as! DOTReportsInnerTblViewCell
        
        tblView.separatorStyle = .none
        
        //add value name with the rating(in brackets) which user provided himself in DOT module
        if arrBeliefs[indexPath.section].arrValues[indexPath.row].strRating == ""
        {
            Cell.lblValueName.text = arrBeliefs[indexPath.section].arrValues[indexPath.row].strName.uppercased()            
        }
        else{
            Cell.lblValueName.text = arrBeliefs[indexPath.section].arrValues[indexPath.row].strName.uppercased()  + "(" + arrBeliefs[indexPath.section].arrValues[indexPath.row].strRating + ")"
        }
        
        //add kudos received
        Cell.lblLikes.text = arrBeliefs[indexPath.section].arrValues[indexPath.row].strUpVotes
        
        //no longer in use
        Cell.lblDislikes.text = arrBeliefs[indexPath.section].arrValues[indexPath.row].strDownVotes
        
        //if its the last value show bottom line
        if indexPath.row == arrBeliefs[indexPath.section].arrValues.count - 1  {
            Cell.lblBottomLine.isHidden = false
            heightForCell = 55
            
        }
        else{
            
            Cell.lblBottomLine.isHidden = true
            heightForCell = 45
            
        }
        return Cell
    }
    
    /**
     * Tableview delegate method used to return height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblOptions {
            return UITableViewAutomaticDimension
        }
        return heightForCell
    }
    
    /**
     * UIScrollView delegate method used to handle scroll
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var sectionHeaderHeight = CGFloat()
        sectionHeaderHeight = 50
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y >= 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    
    //MARK: - UISearchBar Delegates
    /**
     * UISearchBar delegate method when cancel clicked
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method when serach clicked
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /**
     * UISearchBar delegate method called when when user types anything in search bar
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        arrFilteredUserList = searchText.isEmpty ? arrOfGetUserList : arrOfGetUserList.filter { (item: UserModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        
        tblOptions.reloadData()
        
    }
    
    //MARK: - WebService Methods
    /**
     * API - get the kudos for user
     */
    func callWebServiceDOTReports() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*
         {
         "dotId":"25",
         "year":"2018",
         "month":"11"
         }
         */
        
        var userId = ""
        var orgID = ""
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            orgID = selectedOrgID
            if selectedUserIndex == -1 {
                userId = ""
            }
            else {
                userId = arrFilteredUserList[selectedUserIndex].strId
            }
        }
        else {
            userId = AuthModel.sharedInstance.user_id
            orgID = AuthModel.sharedInstance.orgId
        }
        
        //prepare parameter for api
        let param = ["year" : selectedYear,//selected year
            "month" : selectedMonth,//selected month
            "userId": userId,//user id
            "orgId": orgID//org id
            ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleRatingList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    //parse the kudos list received
                    ReportsParser.parseDOTReports(response: response!, completionHandler: { (model) in
                        //store the parsed info in array
                        self.arrBeliefs = model.arrBelief
                        
                    })
                    
                    self.tblView.reloadData()
                    // self.CalculateMaxHeight()
                }
                else {
                    //show error message in case of fail response
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /**
     * API - get user list
     */
    func callWebServiceToUserList(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param =
            ["orgId": AuthModel.sharedInstance.role == kSuperAdmin ? selectedOrgID : AuthModel.sharedInstance.orgId] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Action.getDepartmentAndUserList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    //parse the user list
                    DashboardParser.parseDepartmentUserList(response: response!,removeSelfUser: true, completionHandler: { (arrDep, arrUser) in
                        self.arrOfGetUserList = arrUser
                        
                        self.arrFilteredUserList = arrUser
                        
                        self.tblOptions.reloadData()
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
