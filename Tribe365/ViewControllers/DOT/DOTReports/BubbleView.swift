//
//  BubbleView.swift
//  Tribe365
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 * No longer in use
 */
class BubbleView: UIViewController {

    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblValueName: UILabel!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var lblpersonName: UILabel!
    
    @IBOutlet weak var lblUpVotesCount: UILabel!
    @IBOutlet weak var lblDownVotesCount: UILabel!

    var strDotId = ""
    var strValueId = ""
    var strId =  ""
    var strBeliefId = ""
    var strValueName = ""
    var panel = JKNotificationPanel()
    var strOrgID = ""
    var strUserID = ""
    var strUserName = ""
    var strVoteThumb = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblpersonName.text = strUserName
        lblValueName.text = strValueName.uppercased()
        callWebServiceForGetLikeUnlike()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnLikeDislikeAction(_ sender: UIButton) {
        
        if sender.tag == 0{
            print("like")
            strVoteThumb = ""
            strVoteThumb = "0"
            callWebServiceForLikeUnlike()
        }
        else if sender.tag == 1 {
            print("Dislike")
            strVoteThumb = ""
            strVoteThumb = "1"
            callWebServiceForLikeUnlike()
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func callWebServiceForGetLikeUnlike() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
       
       /* {
            "toUserId":"110",
            "dotBeliefId":"172",
            "dotValueNameId":"7"
        }*/
        let param = ["toUserId": strUserID,
                     "dotBeliefId": strBeliefId,
                     "dotValueNameId": strValueId ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.getBubbleFromUserRating, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    ReportsParser.parseGetBubbleFromUserRating(response: response! , completionHandler: { (model) in
                        
                        self.lblDownVotesCount.text = model.strDownVotes
                        self.lblUpVotesCount.text = model.strUpVotes
                    })
                  
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func callWebServiceForLikeUnlike() {
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*for up votes 0, for down votes 1*/
        /*
         {
         "toUserId": "343",
         "dotId": "25",
         "dotBeliefId": "36",
         "dotValueId": "1150",
         "dotValueNameId": "2",
         "bubbleFlag": "1"
         }
         */
        let param = ["toUserId": strUserID,
                     "dotId": strDotId,
                     "dotBeliefId": strBeliefId,
                     "dotValueId": strId,
                     "dotValueNameId": strValueId,
                     "bubbleFlag": strVoteThumb
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Reports.addBubbleRatings, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Kudos sent successfully.")
                    
                    self.dismiss(animated: false, completion: nil)

                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
}
