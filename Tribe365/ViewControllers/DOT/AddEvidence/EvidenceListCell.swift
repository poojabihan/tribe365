//
//  EvidenceListCell.swift
//  Tribe365
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
* This class is no longer in use
*/
class EvidenceListCell: UITableViewCell {

    @IBOutlet weak var imgEvidence: UIImageView!

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    
    @IBOutlet weak var viewForCell: UIView!
    @IBOutlet weak var lblImageHandMade: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewForCell.layer.borderWidth = 0.5
        viewForCell.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
