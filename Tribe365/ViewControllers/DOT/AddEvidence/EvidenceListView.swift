//
//  EvidenceListView.swift
//  Tribe365
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

/**
* This class is no longer in use, this was used to list evidence
*/
class EvidenceListView: UIViewController, UITableViewDelegate, UITableViewDataSource,sendBackTaskDidWithEvidence {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var btnAddEvidence: UIButton!
    @IBOutlet weak var lblEvidenceTitle: UILabel!
    
    //MARK: - Variable
    var arrForValuesEvidenceList = [EvidenceModel]()
    let panel = JKNotificationPanel()
    var strDOTId = ""
    var strEvidanceId = ""
    var IntIndexPath = Int()
    var strSectionName = ""
    var strSectionId = ""
    var strSelectedValueName = ""
    var toAddEvidence = false

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if toAddEvidence {
            btnAddEvidence(self)
        }
        //remove extra separator lines
        tblView?.tableFooterView = UIView()
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
        lblNoData.isHidden = true
        
        if strSectionName == "vision"
        {
            lblEvidenceTitle.text = "Vision"
            
            
        }
        else if strSectionName == "mission"
        {
            lblEvidenceTitle.text = "Mission"
            
        }
            
        else if strSectionName == "focus"
        {
            lblEvidenceTitle.text = "Focus"
        }
            
        else if strSectionName == "belief"
        {
            lblEvidenceTitle.text = strSelectedValueName
        }
            
        else if strSectionName == "value"
        {
            lblEvidenceTitle.text = strSelectedValueName
            
        }
        if AuthModel.sharedInstance.role == kSuperAdmin{
            
            btnAddEvidence.isHidden = true
            
        }
        else {
            
            btnAddEvidence.isHidden = false
            
        }
        
        CallWebServiceGetEvidenceList()
        tblView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if toAddEvidence {
            self.navigationController?.popViewController(animated: false)
        }
        else {
            CallWebServiceGetEvidenceList()
            tblView.reloadData()
        }
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddEvidence(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "AddEvidenceView") as! AddEvidenceView
        objVC.strDOTId = strDOTId
        objVC.strSection = strSectionName
        objVC.strSectionId = strSectionId
        objVC.m1Delegate = self
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    
    // MARK: - UITableView Delegates Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForValuesEvidenceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : EvidenceListCell = tableView.dequeueReusableCell(withIdentifier: "EvidenceListCell") as! EvidenceListCell
        
        cell.lbldate.text = arrForValuesEvidenceList[indexPath.row].strCreatedAt
        cell.lblDescription.text = arrForValuesEvidenceList[indexPath.row].strDescription
        
        let strImageUrl = arrForValuesEvidenceList[indexPath.row].strFileURL
        let url = URL(string:strImageUrl)
        if url == nil{
            cell.imgEvidence.image = UIImage(named: "No Logo")
        }
        else if url != nil {
            do {
                switch url!.pathExtension {
                case "png":
                    
                    // cell.imgEvidence.sd_setImageWithURL(url, placeholderImage:UIImage(imageNamed:"placeholder.png"))
                    
                    cell.imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                    // cell.imgEvidence.kf.setImage(with: url)
                    cell.imgEvidence.isHidden = false
                    cell.lblImageHandMade.isHidden = true
                    
                    break
                    
                case "jpg":
                    cell.imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                    //  cell.imgEvidence.kf.setImage(with: url)
                    cell.imgEvidence.isHidden = false
                    cell.lblImageHandMade.isHidden = true
                    
                    break
                    
                case "jpeg":
                    cell.imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                    //cell.imgEvidence.kf.setImage(with: url)
                    cell.imgEvidence.isHidden = false
                    cell.lblImageHandMade.isHidden = true
                    
                    break
                    
                default:
                    cell.imgEvidence.isHidden = true
                    cell.lblImageHandMade.isHidden = false
                    cell.lblImageHandMade.text = url?.pathExtension.uppercased()
                    cell.lblImageHandMade.layer.borderColor = UIColor(hexString: "3D3D3F").cgColor
                    cell.lblImageHandMade.layer.borderWidth = 2
                    cell.lblImageHandMade.layer.cornerRadius = 10
                    cell.lblImageHandMade.clipsToBounds = true
                    
                    break
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(arrForValuesEvidenceList[indexPath.row].strFileURL)
        let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "ShowEvidenceView") as! ShowEvidenceView
        
        objVC.strEviImg = arrForValuesEvidenceList[indexPath.row].strFileURL
        
        objVC.strDescription = arrForValuesEvidenceList[indexPath.row].strDescription
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            // Handle delete action
        }
        
        let editAction = UITableViewRowAction(style: .default, title: "") { action, indexPath in
            // Handle delete action
        }
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "editOrganizations"), for: .normal)
        
        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "Delete"), for: .normal)
        return [deleteAction, editAction]        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            self.strEvidanceId =  (self.arrForValuesEvidenceList[indexPath.row].strId) as String
            self.IntIndexPath = indexPath.row
            let alertController = UIAlertController(title: "", message: "Are you sure Want to delete this Evidence!", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                //run your function here
                self.goToEvidenceDeleteFunc()
            }))
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            self.tblView.reloadData()
            // Reset state
            success(true)
        })
        
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // Call edit action
            let objVC = UIStoryboard(name: "DOTModule", bundle: nil).instantiateViewController(withIdentifier: "AddEvidenceView") as! AddEvidenceView
            
            objVC.strEviImg = self.arrForValuesEvidenceList[indexPath.row].strFileURL
            
            objVC.strDescription = self.arrForValuesEvidenceList[indexPath.row].strDescription
            
            objVC.strEviId = (self.arrForValuesEvidenceList[indexPath.row].strId) as String
            
            objVC.IntIndexPath = indexPath.row
            objVC.updateEvidence = "TRUE"
            objVC.m1Delegate = self
            self.navigationController?.pushViewController(objVC, animated: true)
            
            // Reset state
            success(true)
        })
        
        editAction.image = UIImage(named: "editOrganizations")
        editAction.backgroundColor = ColorCodeConstant.darkTextcolor
        
        
        deleteAction.image = UIImage(named: "Delete")
        deleteAction.backgroundColor = ColorCodeConstant.themeRedColor
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            return false
            
        }
        else{
            return true
        }
    }
    
    func goToEvidenceDeleteFunc(){
        
        self.CallingWebServiceForDeleteEvidence()
        
        
    }
    func sendNameToPreviousVC(description: String, file_name: String, id: String, created_Updated_date: String, sendComingFromUpdate:String , SendUpdatedArrayIndex: Int) {
        
        if sendComingFromUpdate == "TRUE"{
            
            arrForValuesEvidenceList[SendUpdatedArrayIndex].strFileURL = file_name
            
            arrForValuesEvidenceList[SendUpdatedArrayIndex].strDescription = description
            
            arrForValuesEvidenceList[SendUpdatedArrayIndex].strId = id
            
            arrForValuesEvidenceList[SendUpdatedArrayIndex].strUpdatedAt = created_Updated_date
        }
            
        else if sendComingFromUpdate == "FALSE"{
            
            //            let jsonObject =
            //                   [ "fileURL"   : file_name,
            //                     "description" : description,
            //                     "id"          : id,
            //                     "created_at"  :created_Updated_date
            //            ]
            //            print(jsonObject)
            
            let model = EvidenceModel()
            model.strFileURL = file_name
            model.strDescription = description
            model.strId = id
            model.strCreatedAt = created_Updated_date
            
            arrForValuesEvidenceList.append(model)
            //arrForValuesEvidenceList[SendUpdatedArrayIndex]["created_at"].stringValue = created_Updated_date
            
        }
        print(arrForValuesEvidenceList[SendUpdatedArrayIndex])
        self.tblView.reloadData()
        
        
    }
    
    
    func CallWebServiceGetEvidenceList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        /*
         {
         "dotId":"25",
         "sectionId":"90",
         "section":"value"
         }
         */
        let param = [  "dotId"     : strDOTId,
                       "sectionId" :strSectionId,
                       "section"   :strSectionName ] as [String : Any]
        
        print("params", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.DOT.getEvidenceList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    DashboardParser.parseEvidenceList(response: response!, completionHandler: { (arrEvidence) in
                        self.arrForValuesEvidenceList = arrEvidence
                        self.tblView.reloadData()
                    })
                    
                    if self.arrForValuesEvidenceList.count == 0 {
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = "No evidence found."
                        
                    }
                    else{
                        
                        self.lblNoData.isHidden = true
                        
                    }
                    
                }
                else {
                    if errorMsg == "Evidence not found."{
                        
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = errorMsg
                    }
                    else{
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    
                    }
                }
            }
        }
    }
    
    func CallingWebServiceForDeleteEvidence(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let firstTodoEndpoint: String = kBaseURL + "deleteDotEvidence/" + strEvidanceId
        
        let defaults = UserDefaults.standard
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        let headers = ["Authorization":token,"Accept":"application/json"]
        
        Alamofire.request(firstTodoEndpoint, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            print(response)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                if json["code"].stringValue != "401" {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Evidence deleted successfully")
                    self.arrForValuesEvidenceList.remove(at: self.IntIndexPath)
                    self.tblView.reloadData()
                }
                else {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let alertView = UIAlertController(title: "", message: "You are no longer the authorized user of this application.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
                        self.logoutFromApp()
                    }))
                    
                    var topVC = UIApplication.shared.keyWindow?.rootViewController
                    while((topVC!.presentedViewController) != nil){
                        topVC = topVC!.presentedViewController
                    }
                    topVC?.present(alertView, animated: true, completion: nil)
                    
                }
                break
            case .failure(let error):
                print(error)
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: error.localizedDescription)
            }
            
            
            
        }
        
    }
    
    //MARK: - UnAuthorized Case Handling
    func logoutFromApp() {
        /*let param =  [ "emp_email" : AuthModel.sharedInstance.email] as [String : AnyObject]
        let url = kBaseURLForIOT + NetworkConstant.Logout.logout// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
        self.requestToLogoutFromtellSid(endUrl: url, imageData : nil, parameters: param)*/
                
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
        
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
        
        print(strFcmToken)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()
        
        //                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
    }
    
    func requestToLogoutFromtellSid(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "AuthModule", bundle: nil)
                    
                    let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "Login") //as! ViewController
                    
                    print(strFcmToken)
                    UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
                    appDel.window!.rootViewController = centerVC
                    appDel.window!.makeKeyAndVisible()
                    
                    //                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully logout.")
                    
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        onError?(err)
                        //                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }

}
