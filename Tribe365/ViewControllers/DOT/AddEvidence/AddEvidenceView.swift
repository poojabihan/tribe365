 //
 //  AddEvidenceView.swift
 //  ScreenDesignsForTribe365
 //
 //  Created by Alok Mishra on 12/06/18.
 //  Copyright © 2018 Ruchika. All rights reserved.
 //
 
 import UIKit
 import Photos
 import MobileCoreServices
 import NVActivityIndicatorView
 import JKNotificationPanel
 import SwiftyJSON
 import Alamofire
 import Foundation
 
 //for both add Evidence and Update Evidence
 /**
  * This class is no longer in use, this was used to add evidence
  */
 protocol sendBackTaskDidWithEvidence {
    func sendNameToPreviousVC(description: String, file_name: String, id : String , created_Updated_date: String, sendComingFromUpdate : String ,SendUpdatedArrayIndex: Int )
 }
 
 class AddEvidenceView: UIViewController, UITextViewDelegate, UIDocumentMenuDelegate, UIImagePickerControllerDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate {
    
    //MARK: - IBOutlets
    
    @IBOutlet var txtViewMessageBox: UITextView!
    @IBOutlet var imgEvidenceImage: UIImageView!
    @IBOutlet weak var lblTitleheader: UILabel!
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var imagePicker = UIImagePickerController()
    var strDOTId = ""
    var docUrl: URL!
    var strEviImg = ""
    var strDescription = ""
    var updateEvidence = ""
    var strEviId = ""
    var m1Delegate : sendBackTaskDidWithEvidence? = nil
    var IntIndexPath = Int()
    var strSection = ""
    var strSectionId = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        txtViewMessageBox.text = "Enter message here..."
        txtViewMessageBox.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        txtViewMessageBox.layer.borderWidth = 1
        txtViewMessageBox.font = UIFont(name: "verdana", size: 12.0)
        txtViewMessageBox.returnKeyType = .done
        txtViewMessageBox.delegate = self
        
        if updateEvidence == "TRUE"{
            
            lblTitleheader.text = "UPDATE EVIDENCE"
            txtViewMessageBox.text = strDescription
            
            let url = URL(string:strEviImg)
            if url == nil{
                
                imgEvidenceImage.image = UIImage(named: "UPLOAD")
            }
            if url != nil {
                
                do {
                    switch url!.pathExtension {
                    case "png":
                        imgEvidenceImage.kf.setImage(with: url)
                        
                        
                        break
                        
                    case "jpg":
                        imgEvidenceImage.kf.setImage(with: url)
                        
                        
                        break
                        
                    case "jpeg":
                        imgEvidenceImage.kf.setImage(with: url)
                        
                        
                        break
                        
                    default:
                        
                        imgEvidenceImage.image = UIImage(named:"doc")
                        
                        break
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAttachFileAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "iCloud", style: .default, handler: { _ in
            self.openiCloud()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        
       /* else if imgEvidenceImage.image == #imageLiteral(resourceName: "UPLOAD") {
        
            let strTxtView = txtViewMessageBox!.text
            let param = [
                          "description": strTxtView ?? "",
                          "dotId":  strDOTId, ] as [String : Any]
            
            requestWith( parameters: param)
                
        
            //self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please add respective file for Evidence.")
            
        }*/
        
            if updateEvidence == "TRUE"
            {
                if txtViewMessageBox.text == "Enter message here..." {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter message.")
                }
                
                else if imgEvidenceImage.image == #imageLiteral(resourceName: "doc")  {
                    
                    let strTxtView = txtViewMessageBox!.text
                    
                    let param = [
                        "description": strTxtView ?? "",
                        "id":  strEviId ] as [String : Any]
                    requestWith( parameters: param)
                    
                }
                else if imgEvidenceImage.image == #imageLiteral(resourceName: "UPLOAD") {
                    
                    let strTxtView = txtViewMessageBox!.text
                    let param = [
                        "description": strTxtView ?? "",
                        "id":  strEviId] as [String : Any]
                    
                    requestWith( parameters: param)
                    
                }
                    
                else{
                    
                    let imgData = UIImageJPEGRepresentation(imgEvidenceImage.image!, 1.0)!
                    let strTxtView = txtViewMessageBox!.text
                    let param = [
                        "description": strTxtView ?? "",
                        "id":  strEviId] as [String : Any]
                    requestWith( imageData: imgData, parameters: param)
                    
                }
            }
            else{
                if txtViewMessageBox.text == "Enter message here..." {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter message.")
                }
                
                else if imgEvidenceImage.image == #imageLiteral(resourceName: "doc")  {
                    
                    let strTxtView = txtViewMessageBox!.text
                    
                    let param = [
                        "description" : strTxtView ?? "",
                        "dotId"       : strDOTId,
                        "section"     : strSection,
                        "sectionId"   : strSectionId ] as [String : Any]
                    requestWith( parameters: param)
                    
                }
                else if imgEvidenceImage.image == #imageLiteral(resourceName: "UPLOAD") {
                    
                    let strTxtView = txtViewMessageBox!.text
                    let param = [
                        "description": strTxtView ?? "",
                        "dotId":  strDOTId,
                        "section"     : strSection,
                        "sectionId"   : strSectionId ] as [String : Any]
                    
                    requestWith( parameters: param)
                    
                }
                    
                else{
                    
                    let imgData = UIImageJPEGRepresentation(imgEvidenceImage.image!, 1.0)!
                    let strTxtView = txtViewMessageBox!.text
                    let param = [
                        "description": strTxtView ?? "",
                        "dotId":  strDOTId,
                        "section"     : strSection,
                        "sectionId"   : strSectionId
                        ] as [String : Any]
                    requestWith( imageData: imgData, parameters: param)
                }
            }
        
    }
    
    //MARK: - Calling web service
    func requestWith(imageData: Data? = nil, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var url = ""
        
        if updateEvidence == "TRUE"
        {
             
             url = kBaseURL + NetworkConstant.DOT.updateDotEvidence /* your API url */
        }
        else{
            
            url = kBaseURL + NetworkConstant.DOT.DOTAddEvidence /* your API url */
            
        }
        
        let defaults = UserDefaults.standard
        
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        
        let headers: HTTPHeaders = [
            "Authorization": token, /*  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Accept" : "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            
            if imageData != nil{
                if let data = imageData{
                    
                    multipartFormData.append(data, withName: "file", fileName: "\(arc4random()).png", mimeType: "image/png")
                }
                
            }
            else
            {
                if self.docUrl != nil {
                    
                    do {
                        let fileData = try Data(contentsOf: self.docUrl!)
                        let data : Data = fileData
                        let fileName = self.docUrl?.lastPathComponent
                        let mimeType: String?
                        
                        switch self.docUrl!.pathExtension {
                        case "pdf":
                            mimeType = "application/pdf"
                            break
                            
                        case "txt":
                            mimeType = "text/plain"
                            break
                            
                        case "doc":
                            mimeType = "application/msword"
                            break
                            
                        case "xml":
                            mimeType = "application/xml"
                            break
                            
                        case "ppt":
                            mimeType = "application/powerpoint"
                            break
                            
                        default:
                            mimeType = "image/png"
                            break
                            
                        }
                        
                        multipartFormData.append(data, withName: "file", fileName: fileName!, mimeType: mimeType!)
                    }catch let error as NSError {
                        print(error)
                    }
                }
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    else {
                        print("Succesfully uploaded")
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Evidence Upload Successfully.")
                        
                        self.btnBackAction(self)
                    }
                    
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
  /*  func requestWith(imageData: Data? = nil, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        var url = ""
        if updateEvidence == "TRUE"{
            
            url = kBaseURL + "updateDotEvidence"
        }
            
        else{
             url = kBaseURL + "addDotEvidence" /* your API url */
        }
        let defaults = UserDefaults.standard
        
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        
        let headers: HTTPHeaders = [
            "Authorization": token, /*  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Accept" : "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            
            if imageData != nil{
                if let data = imageData{
                    
                    multipartFormData.append(data, withName: "file", fileName: "\(arc4random()).png", mimeType: "image/png")
                }
                
            }
            else
            {
                if self.docUrl != nil {
                    
                    do {
                        let fileData = try Data(contentsOf: self.docUrl!)
                        let data : Data = fileData
                        let fileName = self.docUrl?.lastPathComponent
                        let mimeType: String?
                        
                        switch self.docUrl!.pathExtension {
                        case "pdf":
                            mimeType = "application/pdf"
                            break
                            
                        case "txt":
                            mimeType = "text/plain"
                            break
                            
                        case "doc":
                            mimeType = "application/msword"
                            break
                            
                        case "xml":
                            mimeType = "application/xml"
                            break
                            
                        case "ppt":
                            mimeType = "application/powerpoint"
                            break
                            
                        default:
                            mimeType = "image/png"
                            break
                            
                        }
                        
                        multipartFormData.append(data, withName: "file", fileName: fileName!, mimeType: mimeType!)
                    }catch let error as NSError {
                        print(error)
                    }
                    
                    
                }
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    else {
                        print("Succesfully uploaded")
                       self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Evidence Update Successfully.")
                    }
                    
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }*/
    
    
    
    
    //MARK: - Custom Methods
    /*Action Sheet Options Function for Uploading File*/
    func openiCloud(){
        
        let importMenu = UIDocumentPickerViewController (documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = .savedPhotosAlbum
        // imagePicker.mediaTypes =  ["public.image", "public.movie"]
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // To handle image
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgEvidenceImage.image = image
            self.dismiss(animated: true, completion: nil)

        }
        else{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
        /*
         // To handle video
         
         if let videoUrl = info[UIImagePickerControllerMediaURL] as? NSURL{
         print("videourl: ", videoUrl)
         
         imgEvidenceImage.image = UIImage(named:"playIcon")
         //trying compression of video
         let data = NSData(contentsOf: videoUrl as URL)!
         print("File size before compression: \(Double(data.length / 1048576)) mb")
         // compressWithSessionStatusFunc(videoUrl)
         }
         else{
         print("Something went wrong in  video")
         }*/
        
    
    
    
    // MARK: - UIDocumentPickerDelegate Methods
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        
        let myURL = url as URL
        docUrl = myURL
        print(myURL)
        
        imgEvidenceImage.image = UIImage(named:"doc")
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter message here..." {
            textView.text = ""
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.font = UIFont(name: "verdana", size: 12.0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.text = "Enter message here..."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 12.0)
        }
    }
    
    //MARK:- PHOTO LIBRARY ACCESS CHECK
   /* func photoLibraryAvailabilityCheck()
    {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            openCamera()
        }
        else
        {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
    }
    func requestAuthorizationHandler(status: PHAuthorizationStatus)
    {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            openGallary()
        }
        else
        {
            alertToEncouragePhotoLibraryAccessWhenApplicationStarts()
        }
    }
    
    //MARK:- CAMERA & GALLERY NOT ALLOWING ACCESS - ALERT
    func alertToEncourageCameraAccessWhenApplicationStarts()
    {
        //Camera not available - Alert
        let internetUnavailableAlertController = UIAlertController (title: "Camera Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                DispatchQueue.main.async {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) //(url as URL)
                }
                
            }
        }
        let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        internetUnavailableAlertController .addAction(settingsAction)
        internetUnavailableAlertController .addAction(cancelAction)
        
        self.present(internetUnavailableAlertController, animated: true, completion: nil)
        
        //self.window?.rootViewController!.present(internetUnavailableAlertController , animated: true, completion: nil)
    }
    func alertToEncouragePhotoLibraryAccessWhenApplicationStarts()
    {
        //Photo Library not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: "Photo Library Unavailable", message: "Please check to see if device settings doesn't allow photo library access", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        self.present(cameraUnavailableAlertController, animated: true, completion: nil)
        //self.window?.rootViewController!.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }*/
    
 }
