//
//  SelectValueView.swift
//  Tribe365
//
//  Created by Apple on 17/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

protocol BelifesListDelegate: class {
    func didSelectBeliefName(recordDOTId: String, recordID : String , recordSectionName: String, recordSelectedValueName : String, toAddEvidence: Bool)
}

/**
 * This class is  used to select values for beliefs
 */
class SelectValueView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //dot id
    var strDOTId = ""
    
    //section name
    var strSectionName = ""
    
    //array of belief
    var arrOfBeliefs = [BeliefModel]()
    
    //add evidence or not
    var toAddEvidence = false
    
    //delegate
    weak var beliefDelegate: BelifesListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //table view appearnce
        tblView.reloadData()
        tblView?.tableFooterView = UIView()
        tblView.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        tblView.layer.borderWidth = 0.5
        tblView.separatorStyle = .none
    }
    
    /**
     * This function dismisses the screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Table view data source
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfBeliefs.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //custom cell
        let cell : ValuesAddingCell = tableView.dequeueReusableCell(withIdentifier: "ValuesAddingCell") as! ValuesAddingCell
        
        //show value name
        cell.lblNewValue.text = arrOfBeliefs[indexPath.row].strName
        
        return cell
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    /**
     * Tableview delegate method called when user selects any row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //call back method used to send the selected values to previous screen
        beliefDelegate?.didSelectBeliefName(recordDOTId: strDOTId, recordID: arrOfBeliefs[indexPath.row].strId , recordSectionName: "belief", recordSelectedValueName: arrOfBeliefs[indexPath.row].strName, toAddEvidence: toAddEvidence)
        
        dismiss(animated: false, completion: nil)
        
    }
}
