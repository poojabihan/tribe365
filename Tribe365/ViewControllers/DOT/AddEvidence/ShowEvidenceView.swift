//
//  ShowEvidenceView.swift
//  Tribe365
//
//  Created by Apple on 04/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

/**
* This class is no longer in use, this was used to show evidence details
*/
class ShowEvidenceView: UIViewController , UIWebViewDelegate{
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var imgEvidence: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var UIViewForOtherData: UIView!
    @IBOutlet weak var UIViewForImageData: UIView!
    
    @IBOutlet weak var heightConstraintForMainView: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var lblDescriptionForOtherData: UILabel!
    
    
    //MARK: - Variables
    var strEviImg = ""
    var strDescription = ""
    var TitleString = ""
    
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self

        
        txtViewDescription.isEditable = false

        lblDescriptionForOtherData
            .lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDescriptionForOtherData.numberOfLines = 0
        
        let url: URL! = URL(string: strEviImg)
        if strDescription != "" {
        txtViewDescription.text = strDescription
        }
        if url == nil{
            
            imgEvidence.image = UIImage(named: "No Logo")
        }
        if url != nil {
            
            do {
                switch url!.pathExtension {
                case "png":
                   
                   imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)

                    UIViewForImageData.alpha = 1
                    UIViewForOtherData.alpha = 0
                 
                  txtViewDescription.text = strDescription
                    break
                    
                case "jpg":
                    
                    UIViewForImageData.alpha = 1
                    UIViewForOtherData.alpha = 0
                    
                   imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                    
                    txtViewDescription.text = strDescription
                   // lblDescription.frame.size.height = (lblDescription.text?.height(withConstrainedWidth: lblDescription.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!

                    break
                    
                case "jpeg":
        
                   imgEvidence.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                    UIViewForImageData.alpha = 1
                    UIViewForOtherData.alpha = 0
                    
                    txtViewDescription.text = strDescription

                   // lblDescription.frame.size.height = (lblDescription.text?.height(withConstrainedWidth: lblDescription.frame.size.width, font: UIFont.systemFont(ofSize: 14)))!
                    break
                    
                default:
                    
                    webView.loadRequest(URLRequest(url: url!))
                    
                    UIViewForImageData.alpha = 0
                    UIViewForOtherData.alpha = 1
                    
                    heightConstraintForMainView.constant = (lblDescriptionForOtherData.text?.height(withConstrainedWidth: lblDescriptionForOtherData.frame.size.width, font: UIFont.systemFont(ofSize: 14)))! + 110 + webView.frame.size.height
                    
                    
                   lblDescriptionForOtherData.frame.size.height = (lblDescriptionForOtherData.text?.height(withConstrainedWidth: lblDescriptionForOtherData.frame.size.width, font: UIFont.systemFont(ofSize: 14)))!
                    
                    lblDescriptionForOtherData.text = strDescription

                    break
                    
                }
                
            }
        }
        
    }
    
    //MARK: - IBAction
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        dismiss(animated: false, completion: nil)
    }
    
    
    
    //MARK: - Delegates
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

    }
    
    
    
    
}
