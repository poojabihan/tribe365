//
//  MessageView.swift
//  Tribe365
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

/**
 This class is used to list the message inbox (messaging user list)
 */
class MessageView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    //table to show messaging user list
    @IBOutlet weak var tblView: UITableView!
    
    //no data label
    @IBOutlet weak var lblNoData: UILabel!
    
    //show organisation logo
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variables
    //stores message list in form of array
    var arrOfMessageList = [MessageModel]()
    
    //height for description label
    var floatHeightForDescriptionlbl = CGFloat()
    
    //height for main view
    var floatHeightForMainView = CGFloat()
    
    //height for title label
    var floatHeightForTitlelbl = CGFloat()
    
    //used to show error messagegs
    var panel = JKNotificationPanel()
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set url for organisation logo
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //        callWebServiceForGetMessageList()
        
        //load table
        tblView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
        //load message list
        callWebServiceForGetMessageList()
        
        tblView.reloadData()
    }
    //MARK: - IBActions
    /**
     * Redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    /**
     * Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - UITableView Delegate and datasource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return arrOfMessageList.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Custom cell, it returns the message cell
        let cell : MessageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        cell.selectionStyle = .none
        let model = arrOfMessageList[indexPath.row]
        
        //show message text
        cell.lblMessageTitle.text = model.strMsg_detail.capitalized
        
        //last message Date and Messagetext
        
        //        let lastMessageModel  = model.arrLastMessage[0]
        //        if lastMessageModel.strPost_type == "img"{
        
        if model.isImage {
            //case - when last message is an image
            
            //show photo view
            cell.viewForPhoto.isHidden = false
            
            //show photo text
            cell.lblPhoto.text = "Photo"
            
            //hide message description text
            cell.lblMessageDescription.isHidden = true
            
            //set camera image
            cell.imgForPhoto.image = UIImage(named: "graycamera")
            
            //set date
            cell.lblDate.text = model.strCreated_date
        }
            
        else{
            //case - when last message is a text
            
            //hide photo view
            cell.viewForPhoto.isHidden = true
            
            //show message label view
            cell.lblMessageDescription.isHidden = false
            
            //set date
            cell.lblDate.text = model.strCreated_date
            
            //set description
            cell.lblMessageDescription.text = model.strMessage
        }
        //        cell.lblMessageDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        //        cell.lblMessageDescription.numberOfLines = 0
        //
        //
        //        floatHeightForDescriptionlbl = (cell.lblMessageDescription.text?.height(withConstrainedWidth: cell.lblMessageDescription.frame.size.width, font: UIFont.systemFont(ofSize: 15)))!
        //
        //        floatHeightForTitlelbl = (cell.lblMessageTitle.text?.height(withConstrainedWidth: cell.lblMessageTitle.frame.size.width, font: UIFont.systemFont(ofSize: 17)))!
        //
        //        if floatHeightForDescriptionlbl < 40{
        //
        //            floatHeightForDescriptionlbl = 30
        //        }
        //
        //        if floatHeightForTitlelbl < 25{
        //
        //            floatHeightForTitlelbl = 25
        //        }
        //        cell.heightForLblMessageDescription.constant = floatHeightForDescriptionlbl
        
        //        cell.heightForLblMessageTitle.constant = floatHeightForTitlelbl
        
        //        floatHeightForMainView = floatHeightForDescriptionlbl + 25 + floatHeightForTitlelbl
        
        //        cell.heightForMainView.constant = floatHeightForMainView
        
        return cell
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80.0//floatHeightForMainView
    }
    
    /**
     * Tableview delegate method called when user select any messgae from list
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //create instance for class ChatView
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
        
        //pass selected message model
        objVC.ModelOfMessage = [arrOfMessageList[indexPath.row]]
        
        //pass selected message model
        objVC.objInboxModel = arrOfMessageList[indexPath.row]
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    //MARK: - WebService Methods
    /**
     * API to get  result for message list
     */
    func callWebServiceForGetMessageList() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "userId"     : AuthModel.sharedInstance.user_id] as [String : AnyObject]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.getInboxChatList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                
                //parse inbox message data
                IOTParser.parseInboxData(response: response!, completionHandler: { (arrOfMessage) in
                    //stores result in array of model:MessageModel in reverse order(to show latest at top)
                    self.arrOfMessageList = arrOfMessage.reversed()
                })
                self.tblView.reloadData()
                
                if self.arrOfMessageList.count == 0 {
                    //case when message list is blank
                    
                    //show no data label
                    self.lblNoData.isHidden = false
                    
                    //show no data text
                    self.lblNoData.text = "No Records Found."
                    
                    //hide message list table
                    self.tblView.isHidden = true
                }
                else{
                    //case when there is result
                    
                    //hide no data label
                    self.lblNoData.isHidden = true
                    
                    //show message list table
                    self.tblView.isHidden = false
                    
                    //load table
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    /**
     * No longer in use
     */
    func callWebServiceForGetMessageListOld()
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "app_name"        : kAppNameAPI
            ] as [String : AnyObject]
        
        print(param)
        
        WebServiceHandler.getWebServiceForMultiplePart(url: "kBaseURLForIOT + NetworkConstant.IOT.messageList", param: param ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                
                IOTParser.parseIOTMessageList(response: response!, completionHandler: { (arrOfMessage) in
                    self.arrOfMessageList = arrOfMessage
                })
                self.tblView.reloadData()
                if self.arrOfMessageList.count == 0 {
                    
                    self.lblNoData.isHidden = false
                    self.lblNoData.text = "No Records Found."
                    self.tblView.isHidden = true
                }
                else{
                    self.lblNoData.isHidden = true
                    self.tblView.isHidden = false
                    self.tblView.reloadData()
                }
            }
        }
    }
}
