//
//  HistoryDetailView.swift
//  Tribe365
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 * This class display the history detail
 * when user clicks on history list
 */
class HistoryDetailView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    //image uploaded for feedback
    @IBOutlet weak var imgHistory: UIImageView!
    
    //no longer in use
    @IBOutlet weak var lblLocation: UILabel!
    
    //display date
    @IBOutlet weak var lblDate: UILabel!
    
    //display feedback
    @IBOutlet weak var lblHistoryDescription: UILabel!
    
    //feedback text height
    @IBOutlet weak var heightForDescription: NSLayoutConstraint!
    
    //tableview
    @IBOutlet weak var ChatTblView: UITableView!
    
    //no longer in use
    @IBOutlet weak var lblNoChat: UILabel!
    
    //no longer in use
    @IBOutlet weak var heightLblNoData: NSLayoutConstraint!
    
    //height constrint of header
    @IBOutlet weak var HeightForHeaderView: NSLayoutConstraint!
    
    //organisation logo image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //array of history model
    var ModelOfHistory = [HistoryModel]()
    
    //height constraint for message description
    var floatHeightForLblMessageDescription = CGFloat()
    //    var arrOfChat = [HistoryItemChatModel]()
    
    //no longet in use
    var arrOfChat = [ChatModel]()
    var zoomImageView = UIImageView()
    
    var floatIncomingValue = CGFloat()
    var finalheightForheader = CGFloat()
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set organisation image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //tap gesture for image
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(btnImageAction(_:)))
        imgHistory.addGestureRecognizer(tapGesture)
        
        //        ChatTblView.delegate = self
        //        ChatTblView.dataSource = self
        
        let model = ModelOfHistory[0]
        let url = URL(string: model.strImages)
        
        if url == nil {
            imgHistory.contentMode = .scaleAspectFit
        }
        imgHistory.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)//set image
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
        let attrs = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13)]
        
        let attributedString = NSMutableAttributedString(string: "Description: ", attributes:attrs)
        
        let normalString = NSMutableAttributedString(string: model.strMsg_detail)
        
        attributedString.append(normalString)
        lblHistoryDescription.attributedText = attributedString
        lblHistoryDescription.text = model.strMsg_detail
        
        lblLocation.text = model.strLocation_detail
        lblDate.text = model.strCreated_date
        
        lblHistoryDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblHistoryDescription.numberOfLines = 0
        
        floatHeightForLblMessageDescription = (lblHistoryDescription.text?.height(withConstrainedWidth: lblHistoryDescription.frame.size.width, font: UIFont.systemFont(ofSize: 13)))!
        
        heightForDescription.constant = floatHeightForLblMessageDescription
        
        arrOfChat = model.arrItemChat
        
        ChatTblView?.tableFooterView = UIView()
        
        if arrOfChat.count == 0 {
            
            //            lblNoChat.isHidden = false
            //            lblNoChat.text = "No chat available."
            //            heightLblNoData.constant = 20
            //+ noData lblHeight added because chat not aval
            finalheightForheader = 440 + floatHeightForLblMessageDescription
            
            HeightForHeaderView.constant = finalheightForheader
            
        }
        else{
            //            lblNoChat.isHidden = true
            //            ChatTblView.reloadData()
            //            heightLblNoData.constant = 0
            //- noData lblHeight added because chat aval
            finalheightForheader = 360 + floatHeightForLblMessageDescription
            
            HeightForHeaderView.constant = finalheightForheader
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Dynamic sizing for the header view
        if let headerView = ChatTblView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                ChatTblView.tableHeaderView = headerView
            }
        }
    }
    
    
    //MARK: - IBAction Methods
    /**
     redirects user to previous screen when back button is clicked
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     Redirects user to home screen when tribe logo is clicked
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     used for the zoom functionality
     */
    @objc func btnImageAction(_ sender: UITapGestureRecognizer) {
        
        let imageView = sender.view as! UIImageView
        zoomImageView = UIImageView(image: imageView.image)
        zoomImageView.frame = UIScreen.main.bounds
        zoomImageView.backgroundColor = .black
        zoomImageView.contentMode = .scaleAspectFit
        zoomImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        
        let scrollView = UIScrollView()
        scrollView.frame = UIScreen.main.bounds
        scrollView.delegate = self
        scrollView.addGestureRecognizer(tap)
        
        scrollView.addSubview(zoomImageView)
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        self.view.addSubview(scrollView)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    /**
     this function is called when user clicks on send query button
     */
    @IBAction func btnSendQueryAction(_ sender: UIButton){
        print("\(sender)")
        
        //create instance of chat view
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
        let obj = MessageModel()
        obj.strID = ModelOfHistory[0].strChangeit_id
        objVC.objInboxModel = obj
        objVC.strSelectedHistoryChangeitID = ModelOfHistory[0].strChangeit_id
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     This function dismisses full screen of image view
     */
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - UITableView Delegate and datasource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //stores array of chat list
        return arrOfChat.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*"user_type" = Admin;
         "user_type" = User;*/
        
        ChatTblView.separatorStyle = .none
        
        if arrOfChat[indexPath.row].strUser_type == "User"{
            //case of our text
            if !arrOfChat[indexPath.row].isImage{ //Text
                //case when item is text message
                
                //custom cell for outgoing text message
                let outGoingCell : OutGoingMessageCell = tableView.dequeueReusableCell(withIdentifier: "OutGoingMessageCell") as! OutGoingMessageCell
                
                //set message
                outGoingCell.lblOutgoingTxt.text = arrOfChat[indexPath.row].strMessage
                
                //set date
                outGoingCell.lblDate.text = arrOfChat[indexPath.row].strCreated_date
                
                return outGoingCell
            }
            else{ //Image
                //case when item is image(an attachment)
                
                //custom cell for outgoing image message
                let outGoingImgCell : OutGoingImageCell = tableView.dequeueReusableCell(withIdentifier: "OutGoingImageCell") as! OutGoingImageCell
                
                //              let url = URL(string: KBaseChatImageUrl + arrOfChat[indexPath.row].strMessage)
                let url = URL(string: arrOfChat[indexPath.row].strImgUrl)//image url
                
                //set image
                outGoingImgCell.imgOutgoing.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                
                //set date
                outGoingImgCell.lblDate.text = arrOfChat[indexPath.row].strCreated_date
                
                return outGoingImgCell
            }
        }
        else {//Admin
            //case of admin text
            
            if !arrOfChat[indexPath.row].isImage{
                //case when item is text message
                
                //custom cell for incoming text message
                let incomingGoingCell : IncommingMessageCell = tableView.dequeueReusableCell(withIdentifier: "IncommingMessageCell") as! IncommingMessageCell
                
                //set message
                incomingGoingCell.lblIncomingTxt.text = arrOfChat[indexPath.row].strMessage
                
                //set date
                incomingGoingCell.lblDate.text = arrOfChat[indexPath.row].strCreated_date
                
                return incomingGoingCell
            }
            else{
                //case when item is image(an attachment)
                
                //custom cell for incoming image message
                let incomingImageCell : IncomingImageCell = tableView.dequeueReusableCell(withIdentifier: "IncomingImageCell") as! IncomingImageCell
                
                //                let url = URL(string: KBaseChatImageUrl + arrOfChat[indexPath.row].strMessage)
                let url = URL(string: arrOfChat[indexPath.row].strImgUrl)//image url
                
                //set image
                incomingImageCell.incomingImg.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
                
                //set date
                incomingImageCell.lblDate.text = arrOfChat[indexPath.row].strCreated_date
                
                return incomingImageCell
            }
        }
    }
    
    /**
     * Tableview delegate method used to return height of row/cell
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if !arrOfChat[indexPath.row].isImage{
            //case of text message
            return UITableViewAutomaticDimension
        }
        else{
            //case of image message
            return 135
        }
    }
}
