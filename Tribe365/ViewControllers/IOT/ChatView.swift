//
//  ChatView.swift
//  Tribe365
//
//  Created by Apple on 12/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON
import Alamofire
import FirebaseStorage

/**
 This class is used for chat view
 Used JSQMessagesViewController for chat view as third party
 */
class ChatView: JSQMessagesViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //MARK: - Variables
    //array of MessageModel model
    var ModelOfMessage = [MessageModel]()
    
    //show error message
    var panel = JKNotificationPanel()
    
    //array of ChatModel model
    var arrOfChat = [ChatModel]()
    
    //array of JSQMessage model
    var messages = [JSQMessage]()
    
    //image picker object
    var imagePicker = UIImagePickerController()
    
    //store the selected image
    var selectedImage: UIImage?
    
    //seleted history id from previous screen
    var strSelectedHistoryChangeitID = ""
    
    //object of message model from previous screen
    var objInboxModel = MessageModel()
    
    //Define ougoing bubble images
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        //        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor(hexString: "9a9a9a"))
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: ColorCodeConstant.themeRedColor)
    }()
    
    //Define incoming bubble images
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.white)
    }()
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set delegate for image picker
        imagePicker.delegate = self
        
        //inputToolbar.contentView.leftBarButtonItem = nil
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        if ModelOfMessage.count != 0{
            //case when id is not null
            strSelectedHistoryChangeitID = ModelOfMessage[0].strChangeit_id
        }
        
        //set sender id
        senderId = strSelectedHistoryChangeitID == "" ? ModelOfMessage[0].strChangeit_id : strSelectedHistoryChangeitID
        
        //set sender display name
        senderDisplayName = strSelectedHistoryChangeitID == "" ? ModelOfMessage[0].strCreated_date : ""
        
        //set background image
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "BGWhite.png")
        self.view.insertSubview(backgroundImage, at: 0)
        collectionView.backgroundColor = UIColor(patternImage: UIImage(named: "BGWhite.png")!)
        
        //set senderId as User
        self.senderId = "User"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        //set title of navigation controller
        self.title = "CHAT"
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:ColorCodeConstant.darkTextcolor]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        self.navigationController?.navigationBar.tintColor = ColorCodeConstant.darkTextcolor
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        //load chat messages
        callWebServiceLoadEailerMessages()
        collectionView.reloadData()
        
        self.finishReceivingMessage()
        
        
    }
    
    //MARK: - CollectionView Delegates
    /**
     Collection view delegate which returns JSQMessage
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    /**
     Collection view delegate which returns JSQMessagesCollectionViewCell
     */
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : JSQMessagesCollectionViewCell =  (super.collectionView(collectionView, cellForItemAt: indexPath) as? JSQMessagesCollectionViewCell)!
        return cell;
        
    }
    
    /**
     Collection view delegate which returns messages count
     */
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    /**
     Collection view delegate which returns bubble messages depending on chat as admin or user
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    /**
     return nil as we do not display avtar images
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return nil
    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    //    {
    //        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
    //    }
    
    /**
     returns attributed text for date
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        //        let date = JSQMessagesTimestampFormatter.shared().relativeDate(for: messages[indexPath.item].date)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "      yyyy-MM-dd HH:mm:ss      "
        
        // convert your string to date
        let yourDate = formatter.date(from: messages[indexPath.row].senderDisplayName)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "      yyyy-MM-dd HH:mm:ss      "
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        //        print(myStringafd)
        
        return NSAttributedString(string: myStringafd)
    }
    
    /**
     returns height of cell
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    //    {
    //        return messages[indexPath.item].senderId == senderId ? 0 : 15
    //    }
    
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
    //        return 50
    //    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
    //        return kJSQMessagesCollectionViewCellLabelHeightDefault
    //    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
    //
    //        let msg = self.messages[indexPath.item]
    //        var preMsg:JSQMessage?
    //        if indexPath.item != 0{
    //            preMsg = self.messages[indexPath.item - 1]
    //        }
    //
    //        if indexPath.item == 0{
    //            let date = JSQMessagesTimestampFormatter.shared().relativeDate(for: msg.date)
    //            return NSAttributedString(string: date!)
    //        }else if indexPath.item - 1 > 0 && preMsg!.date != msg.date {
    //            let date = JSQMessagesTimestampFormatter.shared().relativeDate(for: msg.date)
    //            return NSAttributedString(string: date!)
    //        }
    //        return nil
    //
    ////        let date = JSQMessagesTimestampFormatter.shared().relativeDate(for: messages[indexPath.item].date)
    ////        return NSAttributedString(string: date!)
    //    }
    
    /**
     called when user taps on message bubble
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        if let test = self.getImage(indexPath: indexPath) {
            //case of photo message
            selectedImage = test
            
            //show media is invoked
            self.performSegue(withIdentifier: "showMedia", sender: self)
        }
    }
    
    //MARK: - Segue Method
    /**
     prepare for segue method
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMedia" {
            //get the destination controller
            if let pageDeDestination = segue.destination as? ShowMediaViewController {
                //pass selected image/attachment
                pageDeDestination.image = selectedImage!
            } else {
                print("type destination not ok")
            }
        } else {
            print("segue inexistant")
        }
    }
    
    //MARK: - Custom Method
    /**
     returns image from media cell
     */
    func getImage(indexPath: IndexPath) -> UIImage? {
        
        //get message
        let message = self.messages[indexPath.row]
        
        //check if message is media(photo)
        if message.isMediaMessage == true {
            
            //get media(photo)
            let mediaItem = message.media
            if mediaItem is JSQPhotoMediaItem {
                let photoItem = mediaItem as! JSQPhotoMediaItem
                if let test: UIImage = photoItem.image {
                    let image = test
                    //return the image
                    return image
                }
            }
        }
        return nil
    }
    
    /**
     This function is called when attachment button is clicked
     */
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        //Ask user to pick attachment from gallery or click from camera
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    /*Action Sheet Options Function for Uploading File*/
    /**
     This functions opens up the camera
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case - when device has camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            //present error alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This functions opens up the gallery
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     Picker controller delegate method which is called when user finish selecting image(either from camera or gallery)
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            // imageView.image = image
            self.dismiss(animated: false, completion: {
                
                //                let param =  [
                //                    "changeit_id"      : self.strSelectedHistoryChangeitID,
                //                    "app_name"         : kAppNameAPI,
                //                    "device_id"        : UIDevice.current.identifierForVendor?.uuidString ?? "",
                //                    "message"          : self.convertImageToBase64(image: image),
                //                    "email_id": AuthModel.sharedInstance.email,
                //                    "post_type"        : "img",
                //                    ] as [String : AnyObject]
                //
                //                let url = kBaseURLForIOT + NetworkConstant.IOT.sendMessage
                //
                //                self.callWebserviceToSendMessage(endUrl: url, imageData: nil, parameters: param)
                
                //create param for api
                let param =  [
                    "sendFrom"      : AuthModel.sharedInstance.user_id,//logged in user id
                    "sendTo"         : "1",//admin id
                    "message"          : self.convertImageToBase64(image: image),//image to base 64
                    "feedbackId"         : self.objInboxModel.strID,//feedback id(from previous screen)
                    "postType"        : "img"//image type
                    ] as [String : AnyObject]
                
                //                print(param)
                //call to send message api
                self.callWebserviceToSendMessage(param: param)
                
                self.finishSendingMessage()
                
                
            })
        }
            
        else{
            
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
        
    }
    
    /**
     image picker delegate method called when picking image fails
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    /**
     This function converts image to base 64 format
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = image.jpeg(.medium) {
            //            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    /**
     This function is called when user clicks send button
     */
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        //        let param =  [
        //            "changeit_id"      : self.strSelectedHistoryChangeitID,
        //            "app_name"         : kAppNameAPI,
        //            "device_id"        : UIDevice.current.identifierForVendor?.uuidString ?? "",
        //            "message"          : text,
        //            "email_id"         : AuthModel.sharedInstance.email,
        //            "post_type"        : "msg",
        //            ] as [String : AnyObject]
        
        //param for api
        let param =  [
            "sendFrom"      : AuthModel.sharedInstance.user_id,//logged in user id
            "sendTo"         : "1",//admin id
            "message"          : text,//text message
            "feedbackId"         : objInboxModel.strID,//feedback id(received from previous screen)
            "postType"        : "msg"//type as message
            ] as [String : AnyObject]
        
        //        print(param)
        //send text message
        callWebserviceToSendMessage(param: param)
        //        let url = kBaseURLForIOT + NetworkConstant.IOT.sendMessage
        //
        //        callWebserviceToSendMessage(endUrl: url, imageData: nil, parameters: param)
        finishSendingMessage()
    }
    
    //MARK: - call web service
    /**
     API - Call webservice to send message
     */
    func callWebserviceToSendMessage(param: [String : AnyObject]) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.iotSendMsg, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{   
                print("SUCCESS Add")
                self.messages.removeAll()
                
                //Parse chat list
                IOTParser.parseChat(response: response!, completionHandler: { (arrOfChat) in
                    //store array for chats
                    self.arrOfChat = arrOfChat
                })
                
                //loop through chat array
                for i in (0..<self.arrOfChat.count) {
                    
                    //get chat model
                    let model = self.arrOfChat[i]
                    
                    if self.arrOfChat[i].isImage{
                        //case when message is an image
                        
                        //get the message url
                        if let imgUrl = NSURL.init(string: self.arrOfChat[i].strImgUrl) {
                            do {
                                
                                //get image data
                                let imageData = try Data(contentsOf: imgUrl as URL)
                                let img = JSQPhotoMediaItem(image:UIImage(data: imageData))
                                
                                //if message present append it in array
                                if let message = JSQMessage(senderId: model.strUser_type, displayName: "      " + model.strCreated_date + "      ", media: img)
                                    
                                {
                                    self.messages.append(message)
                                }
                                
                            } catch {
                                print("Unable to load data: \(error)")
                            }
                        }
                        
                        self.collectionView.reloadData()
                    }
                    else{
                        //case when message is an text
                        
                        //if message present append it in array
                        if let message = JSQMessage(senderId: model.strUser_type, displayName: "      " + model.strCreated_date + "      ", text: model.strMessage)
                        {
                            self.messages.append(message)
                        }
                    }
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    /**
     No longer in use
     */
    func callWebserviceToSendMessageOld(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    print(response)
                    print("Succesfully uploaded")
                    self.callWebServiceLoadEailerMessages()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    if let err = response.error{
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    /**
     API - to load chat list
     */
    func callWebServiceLoadEailerMessages() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "feedbackId": objInboxModel.strID/*AuthModel.sharedInstance.user_id*/] as [String : AnyObject]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.getChatMessages, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                self.messages.removeAll()
                
                //parse chat list
                IOTParser.parseChat(response: response!, completionHandler: { (arrOfChat) in
                    //store chats to array
                    self.arrOfChat = arrOfChat
                })
                
                //loop through array of chat
                for i in (0..<self.arrOfChat.count) {
                    
                    //get chat model
                    let model = self.arrOfChat[i]
                    
                    if self.arrOfChat[i].isImage{
                        //case when message is an image
                        
                        //get image url
                        if let imgUrl = NSURL.init(string: self.arrOfChat[i].strImgUrl) {
                            do {
                                
                                //get image data
                                let imageData = try Data(contentsOf: imgUrl as URL)
                                let img = JSQPhotoMediaItem(image:UIImage(data: imageData))
                                
                                //if valid message, append to array
                                if let message = JSQMessage(senderId: model.strUser_type, displayName: "      " + model.strCreated_date + "      ", media: img)
                                    
                                {
                                    self.messages.append(message)
                                }
                                
                            } catch {
                                print("Unable to load data: \(error)")
                            }
                        }
                        
                        self.collectionView.reloadData()
                        self.scrollToBottom(animated: false)
                    }
                    else{
                        //case when message is a text
                        
                        //if valid message, append to array
                        if let message = JSQMessage(senderId: model.strUser_type, displayName: "      " + model.strCreated_date + "      ", text: model.strMessage)
                        {
                            self.messages.append(message)
                        }
                    }
                }
                self.collectionView.reloadData()
                self.scrollToBottom(animated: false)
            }
        }
    }
    
    /**
     No longer in use
     */
    func callWebServiceLoadEailerMessagesOld()
    {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //changeit_id=%@&email_id=%@&app_name=%@"
        let  param =  [
            "changeit_id"     : strSelectedHistoryChangeitID == "" ? ModelOfMessage[0].strChangeit_id : strSelectedHistoryChangeitID,
            "app_name"        : kAppNameAPI,
            "email_id"        : AuthModel.sharedInstance.email
            ] as [String : AnyObject]
        
        //        print(param)
        
        WebServiceHandler.getWebServiceForMultiplePart(url: "kBaseURLForIOT + NetworkConstant.IOT.chatList", param: param ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "No records found")
            }
            else{
                //                print("SUCCESS Add")
                self.messages.removeAll()
                
                IOTParser.parseChatList(response: response!, completionHandler: { (arrOfChat) in
                    self.arrOfChat = arrOfChat
                })
                for i in (0..<self.arrOfChat.count) {
                    let model = self.arrOfChat[i]
                    if self.arrOfChat[i].strPost_type == "img"{
                        
                        
                        if let imgUrl = NSURL.init(string: "KBaseChatImageUrl + self.arrOfChat[i].strMessage") {
                            do {
                                let imageData = try Data(contentsOf: imgUrl as URL)
                                let img = JSQPhotoMediaItem(image:UIImage(data: imageData))
                                if let message = JSQMessage(senderId: model.strUser_type, displayName: model.strCreated_date, media: img)
                                    
                                {
                                    self.messages.append(message)
                                }
                                
                            } catch {
                                print("Unable to load data: \(error)")
                            }
                        }
                        
                        self.collectionView.reloadData()
                    }
                    else{
                        if let message = JSQMessage(senderId: model.strUser_type, displayName: model.strCreated_date, text: model.strMessage)
                        {
                            self.messages.append(message)
                        }
                    }
                }
                self.collectionView.reloadData()
            }
        }
    }
}
