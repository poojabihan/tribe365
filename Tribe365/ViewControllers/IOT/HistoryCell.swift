//
//  HistoryCell.swift
//  Tribe365
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    //To show feedback image
    @IBOutlet weak var imgHistory: UIImageView!
    
    //To show location
    @IBOutlet weak var lblLocation: UILabel!
    
    //To show feedback description
    @IBOutlet weak var lblHistoryDescription: UILabel!
    
    //To show date
    @IBOutlet weak var lblDate: UILabel!
    
    //Button of send query
    @IBOutlet weak var btnSendQuery: UIButton!
    
    //Message button
    @IBOutlet weak var btnMessage: UIButton!
    
    //height for feedback description
    @IBOutlet weak var heightForDescriptionLbl: NSLayoutConstraint!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
