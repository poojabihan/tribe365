//
//  IOTFeedBackView.swift
//  Tribe365
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import NVActivityIndicatorView
import JKNotificationPanel
import CoreLocation
import SwiftyJSON
import Alamofire
import Firebase
import SCLAlertView

/**
 * This class display the view for feedback module (send feedback + history list)
 * when user clicks on side menu's Feedback option t redirects to this class
 */
class IOTFeedBackView: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate, UITableViewDataSource,UITableViewDelegate {
    
    //MARK: - IBOutlets
    //feedback text view
    @IBOutlet weak var txtDescription: UITextView!
    
    //no longer in use
    @IBOutlet weak var btnLocation: UIButton!
    
    //attachment button
    @IBOutlet weak var btnCamera: UIButton!
    
    //use to show image taken from camera/gallery
    @IBOutlet weak var imgView: UIImageView!
    
    //no longer in use
    @IBOutlet weak var lblLocation: UILabel!
    
    //imgView height constraint
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    
    //table view
    @IBOutlet weak var tblView: UITableView!
    
    //use to show organisation image
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK: - Variable
    //image picker view
    var imagePicker = UIImagePickerController()
    
    //used to display error/warning message
    let panel = JKNotificationPanel()
    
    //no longer in use
    var locationManager:CLLocationManager!
    
    //no longer in use
    var strLat = ""
    
    //no longer in use
    var strLog = ""
    
    //no longer in use
    var customAlert = SCLAlertView()
    
    //no longer in use
    var textViewInAlert = UITextView()
    
    //no longer in use
    var questionDataArray = JSON()
    
    //no longer in use
    var isShowingAlert = false
    
    //stores history list
    var arrOfHistoryList = [HistoryModel]()
    
    //no data label
    var noDataFound = false
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Delegate for UIImagePicker
        imagePicker.delegate = self
        
        //set organisation image
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
        //set placeholder for textview
        txtDescription.text = "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable"
        //        txtDescription.layer.borderColor = UIColor(hexString: "9A9A9A").cgColor
        txtDescription.textColor = UIColor.lightGray//light gray color for placehlder text
        //        txtDescription.layer.borderWidth = 1
        //        txtDescription.font = UIFont(name: "verdana", size: 14.0)
        txtDescription.returnKeyType = .done
        txtDescription.delegate = self
        
        //load history list
        callWebServiceForGetHistoryList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //show navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        getQuessionarieMethod()
        
    }
    
    //MARK: - IBActions
    /**
     This function is called when we click side menu button.
     It open ups the side menu
     */
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    /**
     This function navigates user to previous screen
     */
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     This function navigates user to home screen
     */
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     This function is called when user clicks on send button
     */
    @IBAction func btnSendAction(_ sender: Any) {
        
        //Check whether user has entered feedback or not
        if (txtDescription.text?.isEmpty)! || txtDescription.text == "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable"{
            //show error if empty
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter feedback.")
        }
            //        else if (lblLocation.text?.isEmpty)! {
            //            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please add you location throught location button.")
            //        }
        else {
            //case when user enters feedback
            var param = [String : AnyObject]()
            
            //            let token = UserDefaults.standard.value(forKey: "fcm_token") == nil ? "" : UserDefaults.standard.value(forKey: "fcm_token") as! String
            
            if imgView.image == #imageLiteral(resourceName: "add_image")
            {//no image added[[FIRInstanceID instanceID]token]
                /*param =  [
                 "msg_detail"      : txtDescription.text!,
                 "email_id_to"     : AuthModel.sharedInstance.email,
                 "location_detail" : lblLocation.text!,
                 "device_id"       : UIDevice.current.identifierForVendor?.uuidString ?? "",
                 "images"          : "",
                 "latitude"        : strLat,
                 "longitude"       : strLog,
                 "app_name"        : kAppNameAPI,
                 "ssecrete"        : "tellsid@1",
                 "fcm_token"       : token,
                 "org_id"          : AuthModel.sharedInstance.orgId] as [String : AnyObject]*/
                
                //case when only feeback is written
                param =  [
                    "message"      : txtDescription.text!,
                    "userId"     : AuthModel.sharedInstance.user_id,
                    "orgId" : AuthModel.sharedInstance.orgId,
                    "image"       : ""] as [String : AnyObject]
                
            }
            else{ //send image
                
                //case when feedback & image are there
                
                /* param =  [
                 "msg_detail"      : txtDescription.text!,
                 "email_id_to"     : AuthModel.sharedInstance.email,
                 "location_detail" : lblLocation.text!,
                 "device_id"       : UIDevice.current.identifierForVendor?.uuidString ?? "",
                 "images"          : convertImageToBase64(image: imgView.image!),
                 "latitude"        : strLat,
                 "longitude"       : strLog,
                 "app_name"        : kAppNameAPI,
                 "ssecrete"        : "tellsid@1",
                 "fcm_token"       : token,
                 "org_id"          : AuthModel.sharedInstance.orgId] as [String : AnyObject]*/
                
                param =  [
                    "message"      : txtDescription.text!,
                    "userId"     : AuthModel.sharedInstance.user_id,
                    "orgId" : AuthModel.sharedInstance.orgId,
                    "image"       : convertImageToBase64(image: imgView.image!)] as [String : AnyObject]
            }
            //            let url = kBaseURLForIOT + NetworkConstant.IOT.postFeedback// "http://tellsid.softintelligence.co.uk/index.php/apitellsid/postdetails/"
            //
            //            requestWith(endUrl: url, imageData : nil, parameters: param)
            
            //api call
            callWebServiceToSendFeedback(param: param)
        }
    }
    
    /**
     This function is called when attachment button is clicked
     */
    @IBAction func btnCameraAction(_ sender: Any) {
        
        //create alert view for choice of Camera or Gallery
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function is no longer in use
     */
    @IBAction func btnCurrentLocationAction(_ sender: Any) {
        
        //location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }
    
    /**
     This function is called when message button is clicked (no longer in use)
     */
    @IBAction func btnMessageAction(_ sender: Any) {
        //Redirects user to chat inbox view named:MessageView
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "MessageView") as! MessageView
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is called when history button is clicked (no longer in use)
     */
    @IBAction func btnHistoryAction(_ sender: Any) {
        //Redirects user to history list of feedback named:HistoryView
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "HistoryView") as! HistoryView
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is called when question mark button is clicked
     */
    @IBAction func btnSupportAction(_ sender: Any) {
        //Redirects user to support screen with screen type as iot.
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.iot
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /**
     This function is called when action button is clicked (the vertical button hanging at the center right of screen)
     */
    @IBAction func btnActionClicked() {
        //Redirects user to the action list view named:ViewActionsViewController
        let objVC = UIStoryboard(name: "ActionModule", bundle: nil).instantiateViewController(withIdentifier: "ViewActionsViewController") as! ViewActionsViewController
        
        //pass organisation id
        objVC.strOrgId = AuthModel.sharedInstance.orgId
        //        objVC.arrOfficeDetail = arrOfficeDetail
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    //MARK: - UITableView Delegate and datasource
    /**
     * Tableview delegate method used to return number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if noDataFound {
            //no data case, display no records cell
            return 1
        }
        
        //returns history list
        return arrOfHistoryList.count
    }
    
    /**
     * Tableview delegate method used to return row/cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if noDataFound {
            //When no data return NoDataCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell")
            cell?.selectionStyle = .none
            return cell!
        }
        
        //Custom cell to display history list
        let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        cell.selectionStyle = .none
        
        //history list model
        let model = arrOfHistoryList[indexPath.row]
        //        cell.lblLocation.text = model.strLocation_detail
        
        //for date
        cell.lblDate.text = model.strCreated_date
        
        //for title
        cell.lblHistoryDescription.text = model.strMsg_detail
        
        //for image
        let url = URL(string: model.strImages)
        cell.imgHistory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "No Logo"), progressBlock:nil, completionHandler: nil)
        
        //image border color
        cell.imgHistory.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        //image border width
        cell.imgHistory.layer.borderWidth = 0.5
        
        //        cell.btnMessage.tag = indexPath.row
        //        cell.btnSendQuery.tag = indexPath.row
        
        //        cell.btnMessage.addTarget(self, action: #selector(self.btnMessageAction(_:)), for: .touchUpInside)
        
        //        cell.btnSendQuery.addTarget(self, action: #selector(self.btnSendQueryAction(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    /**
     * Tableview delegate method is called when selects any row
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if noDataFound {
            //do nothing when user clicks on no data cell
            return
        }
        
        //Redirect to history detail screen
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "HistoryDetailView") as! HistoryDetailView
        
        //pass the history model
        objVC.ModelOfHistory = [arrOfHistoryList[indexPath.row]]
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    /**
     * Tableview delegate method used to return  height of row
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if noDataFound {
            //for no record cell
            return 20
        }
        
        //for the history list cell
        return 80
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     * ImagePicker delegate method called when user selects any image from Camera/Gallery
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        //get original image selected
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            //give height to image view
            imgHeightConstraint.constant = 45.0
            
            //assign image to image view
            imgView.image = image
            
            //dismiss the picker
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //show error if image not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     * ImagePicker delegate method called when user cancels  image selection view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    
    //MARK: - location delegate methods
    /**
     *No longer in use
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        strLat = String(userLocation.coordinate.latitude)
        strLog = String(userLocation.coordinate.longitude)
        
        //  self.labelLat.text = "\(userLocation.coordinate.latitude)"
        //  self.labelLongi.text = "\(userLocation.coordinate.longitude)"
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            
            if placemarks == nil {
                return
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
                
                self.lblLocation.text = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
            }
        }
        
        manager.stopUpdatingLocation()
    }
    
    /**
     *No longer in use
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    //MARK: - Custom Functions
    
    /*Action Sheet Options Function for Uploading File*/
    /**
     *This function opens camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //when type is camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *This function opens gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // convert images into base64 and keep them into string
    /**
     *This function is used to convert image to base64 format
     */
    func convertImageToBase64(image: UIImage) -> String {
        //get image data
        var imgData = UIImageJPEGRepresentation(imgView.image!, 1.0)!
        
        //compress it
        if let imageData = imgView.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        
        //returns base64 of image
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    /**
     *No longer in use
     */
    @objc func smileButtonAction()
    {
        
        //[customAlert hideView];
        
        let param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "ques_id" : questionDataArray[0]["ques_id"].stringValue,
            "reply"       : "Happy",
            "app_name"        : kAppNameAPI
            ] as [String : AnyObject]
        
        requestWithSubmitSmileyMethod(endUrl: "kBaseURLForIOT + NetworkConstant.IOT.getuseranser", imageData: nil, parameters: param)
        
    }
    
    /**
     *No longer in use
     */
    @objc func neutralButtonAction()
    {
        let param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "ques_id" : questionDataArray[0]["ques_id"].stringValue,
            "reply"       : "Neutral",
            "app_name"        : kAppNameAPI
            ] as [String : AnyObject]
        
        requestWithSubmitSmileyMethod(endUrl: "kBaseURLForIOT + NetworkConstant.IOT.getuseranser", imageData: nil, parameters: param)
        
    }
    
    /**
     *No longer in use
     */
    @objc func sadButtonAction()
    {
        let param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "ques_id" : questionDataArray[0]["ques_id"].stringValue,
            "reply"       : "Sad",
            "app_name"        : kAppNameAPI
            ] as [String : AnyObject]
        
        requestWithSubmitSmileyMethod(endUrl: "kBaseURLForIOT + NetworkConstant.IOT.getuseranser", imageData: nil, parameters: param)
        
    }
    
    /**
     *No longer in use
     */
    @objc func preferNotToAnswerButtonAction()
    {
        let param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "ques_id" : questionDataArray[0]["ques_id"].stringValue,
            "reply"       : "Prefer Not To Answer",
            "app_name"        : kAppNameAPI
            ] as [String : AnyObject]
        
        requestWithSubmitSmileyMethod(endUrl: "kBaseURLForIOT + NetworkConstant.IOT.getuseranser", imageData: nil, parameters: param)
        
    }
    
    /**
     *No longer in use
     */
    func showQuestionarrieAlert(question : String) {
        
        //        customAlert = SCLAlertView()
        textViewInAlert = UITextView()
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false, showCircularIcon: true
        )
        customAlert = SCLAlertView(appearance: appearance)
        
        let textRect = (question.replacingOccurrences(of: "\n", with: " ") as NSString).boundingRect(with: CGSize(width: 200, height: 100),
                                                                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                                                                     attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)],
                                                                                                     context: nil)
        
        let size1 = textRect.size
        textViewInAlert.frame = CGRect.init(x: 10, y: 0, width: 200, height: size1.height + 10)
        textViewInAlert.isEditable = false
        textViewInAlert.isSelectable = false
        textViewInAlert.textAlignment = .center
        textViewInAlert.font = UIFont.systemFont(ofSize: 14.0)
        textViewInAlert.text = question
        textViewInAlert.textColor = UIColor.darkGray
        textViewInAlert.isHidden = false
        customAlert.customSubview = textViewInAlert
        
        let greenView = UIView(frame: CGRect.init(x: 10, y: 0, width: 200, height: 70))
        let button1 = UIButton.init(type: .system)
        button1.setBackgroundImage(UIImage.init(named: "smiley.png"), for: .normal)
        button1.frame = CGRect.init(x: 20, y: 10, width: 50, height: 50)
        greenView.addSubview(button1)
        
        let button2 = UIButton.init(type: .system)
        button2.setBackgroundImage(UIImage.init(named: "Neutral.png"), for: .normal)
        button2.frame = CGRect.init(x: 80, y: 10, width: 50, height: 50)
        greenView.addSubview(button2)
        
        let button3 = UIButton.init(type: .system)
        button3.setBackgroundImage(UIImage.init(named: "sad.png"), for: .normal)
        button3.frame = CGRect.init(x: 140, y: 10, width: 50, height: 50)
        greenView.addSubview(button3)
        
        customAlert.customSubview = greenView
        
        
        button1.addTarget(self, action: #selector(smileButtonAction), for: .touchUpInside)
        button2.addTarget(self, action: #selector(neutralButtonAction), for: .touchUpInside)
        button3.addTarget(self, action: #selector(sadButtonAction), for: .touchUpInside)
        
        customAlert.addButton("Prefer Not To Answer", backgroundColor: ColorCodeConstant.themeRedColor, textColor: UIColor.white, showTimeout: nil, target: self, selector: #selector(preferNotToAnswerButtonAction))
        
        let currentYear = Date()
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy"
        let currentYearString = formatter1.string(from: currentYear)
        print("Current year is ",currentYearString)
        
        customAlert.showCustom(question, subTitle: currentYearString, color: ColorCodeConstant.themeRedColor, icon: UIImage.init(named: "alertIcon.png")!, closeButtonTitle: nil, timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        
    }
    
    //MARK: - Call Web Service
    /**
     *API - Get history list
     */
    func callWebServiceForGetHistoryList() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "userId": AuthModel.sharedInstance.user_id] as [String : AnyObject]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.getFeedbackDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                
                //parse history list
                IOTParser.parseIOTHistory(response: response!, completionHandler: { (arrOfHistory) in
                    
                    //assign the parsed array to history model in reversed order
                    self.arrOfHistoryList = arrOfHistory.reversed()
                })
                if self.arrOfHistoryList.count == 0 {
                    //case when no data found
                    self.noDataFound = true
                }
                else{
                    //case when there is data
                    self.noDataFound = false
                }
                
                //reload data
                self.tblView.reloadData()
                
            }
        }
    }
    
    /**
     API - to send the feedback
     */
    func callWebServiceToSendFeedback(param: [String : Any]) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.postFeedback, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("Succesfully uploaded")
                
                //empty textview when feedback is sent
                self.txtDescription.text = ""
                //                self.lblLocation.text = ""
                
                //set default add image
                self.imgView.image = UIImage(named: "add_image")
                
                //stop the loader
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                self.txtDescription.resignFirstResponder()
                
                self.callWebServiceForGetHistoryList()
                
                //show success message
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully uploaded.")
            }
        }
    }
    
    /**
     No longer in use
     */
    func getQuessionarieMethod() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "email_id"     : AuthModel.sharedInstance.email,
            "app_name"       : kAppNameAPI
            ] as [String : AnyObject]
        
        print(param)
        
        WebServiceHandler.getWebServiceForMultiplePart(url: "kBaseURLForIOT + NetworkConstant.IOT.getquestion", param: param ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                self.questionDataArray = response!["response"]
                
                if response!["total"].stringValue == "1" {
                    
                    self.appDelegate._isQuesFromNotify = false
                    
                    if self.isShowingAlert == false {
                        self.isShowingAlert = true
                        self.showQuestionarrieAlert(question: self.questionDataArray[0]["question"].stringValue)
                    }
                    
                }else if (self.appDelegate._isQuesFromNotify == true && response!["total"].stringValue == "0")
                {
                    self.appDelegate._isQuesFromNotify  = false
                    
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: true, showCircularIcon: true
                    )
                    let alert = SCLAlertView(appearance: appearance)
                    
                    alert.showCustom(kAppNameAPI, subTitle: "You have already submitted your Feedback.", color: ColorCodeConstant.themeRedColor, icon: UIImage.init(named: "alertIcon.png")!, closeButtonTitle: "Done", timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
                    
                }
            }
        }
    }
    
    /**
     No longer in use
     */
    func requestWith(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    self.txtDescription.text = ""
                    //                    self.lblLocation.text = ""
                    self.imgView.image = UIImage(named: "add_image")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Successfully uploaded.")
                    
                    if let err = response.error {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        onError?(err)
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "failed to upload.")
                        
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    /**
     No longer in use
     */
    func requestWithSubmitSmileyMethod(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.customAlert.hideView()
                    
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false, showCircularIcon: true
                    )
                    let alert = SCLAlertView(appearance: appearance)
                    
                    alert.addButton("Done", action: {
                        self.isShowingAlert = false
                        self.getQuessionarieMethod()
                    })
                    
                    alert.showCustom(kAppNameAlert, subTitle: "Thanks for your Feedback.", color: ColorCodeConstant.themeRedColor, icon: UIImage.init(named: "alertIcon.png")!, closeButtonTitle: nil, timeout: nil, colorStyle: SCLAlertViewStyle.success.defaultColorInt, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
                    
                    onCompletion?(nil)
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    
    //MARK:- UITextViewDelegates
    /**
     Textview delegate used to handle the placeholder text
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable" {
            textView.text = ""
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.font = UIFont(name: "verdana", size: 18.0)
        }
    }
    
    /**
     Textview delegate used to handle the placeholder text
     */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    /**
     Textview delegate used to handle the placeholder text
     */
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = ColorCodeConstant.darkTextcolor
            textView.text = "Any good things, bad things to highlight?\n\nWe love ideas, observations, thoughts, feelings and emotions\n\nThe Tribe team keep everything anonymous and non-identifiable"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
}

/**
 Image extension for compress feature
 */
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
