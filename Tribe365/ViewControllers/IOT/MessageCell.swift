//
//  MessageCell.swift
//  Tribe365
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
/**
 This class is inbox message table cell class
 */
class MessageCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //To show message title label
    @IBOutlet weak var lblMessageTitle: UILabel!
    
    //To show message description label
    @IBOutlet weak var lblMessageDescription: UILabel!
    
    //to show message date
    @IBOutlet weak var lblDate: UILabel!
    
    //to handle automatic height of cell according to description
    @IBOutlet weak var heightForLblMessageDescription: NSLayoutConstraint!
    
    //to handle automatic height of main view according to content
    @IBOutlet weak var heightForMainView: NSLayoutConstraint!
    
    //shows text for photo if user's last message sent/received is an image
    @IBOutlet weak var lblPhoto: UILabel!
    
    //shows camera image if user's last message sent/received is an image
    @IBOutlet weak var imgForPhoto: UIImageView!
    
    //view that contain photo text & image
    @IBOutlet weak var viewForPhoto: UIView!
    
    //to handle automatic height of cell according to title
    @IBOutlet weak var heightForLblMessageTitle: NSLayoutConstraint!
    
    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
