//
//  SuperAdminReportsView.swift
//  Tribe365
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import NVActivityIndicatorView
import  JKNotificationPanel

/**
 This class is no longer in use
 */
class SuperAdminReportsView: UIViewController, UIWebViewDelegate {
    
    //MARK: - Variables
    var panel = JKNotificationPanel()
    var strOrgID = ""
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnOpenUrlAction(_ sender: Any) {
        
        guard let url = URL(string: "http://production.chetaru.co.uk/tribe365/admin") else { return }
        UIApplication.shared.open(url)
    }
    
}
