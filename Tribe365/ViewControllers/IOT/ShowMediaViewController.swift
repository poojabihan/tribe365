//
//  ShowMediaViewController.swift
//  Tribe365
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 This class is used to show media of chat message which contain image
 */
class ShowMediaViewController: UIViewController {
    
    //MARK: - Variables
    //stores image
    var image: UIImage? = nil
    
    //stores title
    var titreText: String!
    
    //store image url
    var strImg = ""
    
    //MARK: - IBOutlets
    //media image view
    @IBOutlet weak var imageView: UIImageView!
    
    //message title label
    @IBOutlet weak var titre: UILabel!
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
        print(image ?? "")
        if image != nil {
            //when image is not nil, set image
            imageView.image = image
        } else {
            print("image not found")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
}
