//
//  HistoryView.swift
//  Tribe365
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

/**
* This class is no longer in use
*/
class HistoryView: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK: - Variables
    var arrOfHistoryList = [HistoryModel]()
    var panel = JKNotificationPanel()
    var selectedChangeId = ""
    
    //MARK: - ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        tblView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callWebServiceForGetHistoryList()
        //For Navigation bar tranparancy
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = .clear
        
    }
    //MARK: - IBActions
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - UITableView Delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfHistoryList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        
        let model = arrOfHistoryList[indexPath.row]
//        cell.lblLocation.text = model.strLocation_detail
        cell.lblDate.text = model.strCreated_date
        cell.lblHistoryDescription.text = model.strMsg_detail
        
        let url = URL(string: model.strImages)
        cell.imgHistory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "No Logo"), progressBlock:nil, completionHandler: nil)
        
        cell.imgHistory.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        cell.imgHistory.layer.borderWidth = 0.5
        
//        cell.btnMessage.tag = indexPath.row
        cell.btnSendQuery.tag = indexPath.row
        
//        cell.btnMessage.addTarget(self, action: #selector(self.btnMessageAction(_:)), for: .touchUpInside)
        
        cell.btnSendQuery.addTarget(self, action: #selector(self.btnSendQueryAction(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "HistoryDetailView") as! HistoryDetailView
        
        objVC.ModelOfHistory = [arrOfHistoryList[indexPath.row]]
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
    //MARK: - call web service
    func callWebServiceForGetHistoryList() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "userId": AuthModel.sharedInstance.user_id] as [String : AnyObject]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Improve.getFeedbackDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                IOTParser.parseIOTHistory(response: response!, completionHandler: { (arrOfHistory) in
                    self.arrOfHistoryList = arrOfHistory.reversed()
                })
                if self.arrOfHistoryList.count == 0 {
                    
                    self.lblNoData.isHidden = false
                    self.tblView.isHidden = true
                    self.lblNoData.text = "No Records Found."
                }
                else{
                    self.lblNoData.isHidden = true
                    self.tblView.isHidden = false
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    func callWebServiceForGetHistoryListOld()
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "email_id_to"     : AuthModel.sharedInstance.email,
            "device_id"       : UIDevice.current.identifierForVendor?.uuidString ?? "",
            "app_name"        : kAppNameAPI,
            ] as [String : AnyObject]
        
        print(param)
        
        WebServiceHandler.getWebServiceForMultiplePart(url: "kBaseURLForIOT + NetworkConstant.IOT.historyList", param: param ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                IOTParser.parseIOTHistoryList(response: response!, completionHandler: { (arrOfHistory) in
                    self.arrOfHistoryList = arrOfHistory
                })
                if self.arrOfHistoryList.count == 0 {
                    
                    self.lblNoData.isHidden = false
                    self.tblView.isHidden = true
                    self.lblNoData.text = "No Records Found."
                }
                else{
                    self.lblNoData.isHidden = true
                    self.tblView.isHidden = false
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    //MARK: - Cell button Actions
    
    @objc func btnMessageAction(_ sender: UIButton){
        print("\(sender)")
        selectedChangeId = arrOfHistoryList[sender.tag].strChangeit_id
        let alertController = UIAlertController(title: kAppNameAPI, message: "Are you sure to Resend?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Resend", style: .default, handler: { action in
            //run your function here
            self.goToResendApi()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnSendQueryAction(_ sender: UIButton){
        print("\(sender)")
        
        let objVC = UIStoryboard(name: "IOTModule", bundle: nil).instantiateViewController(withIdentifier: "ChatView") as! ChatView
        let obj = MessageModel()
        obj.strID = arrOfHistoryList[sender.tag].strChangeit_id
        objVC.objInboxModel = obj
        objVC.strSelectedHistoryChangeitID = arrOfHistoryList[sender.tag].strChangeit_id
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    //MARK: - Custom methods
    
    func goToResendApi(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let  param =  [
            "changeit_id"  : selectedChangeId,
            "app_name"     : kAppNameAPI,
            "email_id_to"  : AuthModel.sharedInstance.email
            ] as [String : AnyObject]
        
        print(param)
        
        WebServiceHandler.getWebServiceForMultiplePart(url: "kBaseURLForIOT + NetworkConstant.IOT.resendMail", param: param ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print("SUCCESS Add")
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Email sent successfully.")
            }
        }
        
    }
    
    
}
