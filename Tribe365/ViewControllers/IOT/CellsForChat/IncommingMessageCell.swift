//
//  IncommingMessageCell.swift
//  Tribe365
//
//  Created by Apple on 02/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
 This class is used to show the incoming cell in chat list
 */
class IncommingMessageCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //message text label
    @IBOutlet weak var lblIncomingTxt: UILabel!
    
    //message date label
    @IBOutlet weak var lblDate: UILabel!

    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
