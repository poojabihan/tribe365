//
//  OutGoingImageCell.swift
//  Tribe365
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/**
This class is used to show the outgoing cell of image in chat list
*/
class OutGoingImageCell: UITableViewCell {
    
    //MARK: - IBOutlets
    //message image
    @IBOutlet weak var imgOutgoing: UIImageView!
    
    //message date label
    @IBOutlet weak var lblDate: UILabel!

    //MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
