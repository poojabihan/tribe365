//
//  BasicBarChart2.swift
//  Tribe365
//
//  Created by Apple on 20/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

protocol BasicBarChart2Delegate {
    func didSelectGraph(index: Int)
}

class BasicBarChart2: UIView {

    var delegate:BasicBarChart2Delegate?

    /// the width of each bar
    let barWidth: CGFloat = 38.0
    
    /// space between each bar
    let space: CGFloat = 20.0
    
    /// space at the bottom of the bar to show the title
    private let bottomSpace: CGFloat = 50.0
    
    /// space at the top of each bar to show the value
    private let topSpace: CGFloat = 15.0
    
    /// contain all layers of the chart
    private let mainLayer: CALayer = CALayer()
    
    /// contain mainLayer to support scrolling
    private let scrollView: UIScrollView = UIScrollView()
    
    var graphRangeXValues = [CGFloat]()
    var graphRangeYValues = [CGFloat]()

    var dataEntries: [BarEntry]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntries {
                scrollView.contentSize = CGSize(width: (barWidth + space)*CGFloat(dataEntries.count), height: self.frame.size.height)
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                drawHorizontalLines()
                
                for i in 0..<dataEntries.count {
                    showEntry(index: i, entry: dataEntries[i])
//                    drawXAxis(index: i)
                }
                
                for i in 0..<11 {
                    drawXAxis(index: i)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        scrollView.layer.addSublayer(mainLayer)
        let tapBar = UITapGestureRecognizer(target: self, action: #selector(didBarTap(sender:)))
        scrollView.addGestureRecognizer(tapBar)
        self.addSubview(scrollView)
    }
    
    override func layoutSubviews() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    private func showEntry(index: Int, entry: BarEntry) {
        /// Starting x postion of the bar
        let xPos: CGFloat = space + 5 + CGFloat(index) * (barWidth + space)
        //        let xPos: CGFloat = space + CGFloat(index) * (barWidth + space)
        
        let gValue = xPos + barWidth + space
        graphRangeXValues.append(gValue)
        /// Starting y postion of the bar
        let yPos: CGFloat = translateHeightValueToYPosition(value: entry.height)
        print("<<<<<?>>>>>",xPos)
        graphRangeYValues.append(yPos)

        drawBar(xPos: xPos, yPos: yPos, color: entry.color)
        
        /// Draw text above the bar
        //        drawTextValue(xPos: xPos - space/2, yPos: yPos - 30, textValue: entry.textValue, color: entry.color)
        
        /// Draw text below the bar
        drawTitle(xPos: xPos - space/2, yPos: mainLayer.frame.height - bottomSpace + 10, title: entry.title, color: entry.color)
    }
    
    private func drawBar(xPos: CGFloat, yPos: CGFloat, color: UIColor) {
        let barLayer = CALayer()
        barLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth, height: mainLayer.frame.height - bottomSpace - yPos)
        barLayer.backgroundColor = UIColor(hexString: "#ff454b").cgColor//color.cgColor
        print("=====",barLayer.frame)
        mainLayer.addSublayer(barLayer)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let point = touches.first?.location(in: self) // Where you pressed
        
        print("=====.....",point as Any)

        if let layer = self.layer.hitTest(point!) as? CAShapeLayer { // If you hit a layer and if its a Shapelayer
            if (layer.path?.contains(point!))! { // Optional, if you are inside its content path
                print("Hit shapeLayer") // Do something
            }
        }
    }
    
    @objc func didBarTap(sender: UITapGestureRecognizer) {
        let point = sender.location(in: scrollView)

        print("=====.....scrolllll",point as Any)
        for (index,value) in graphRangeXValues.enumerated() {
            if point.x < value && point.y >= graphRangeYValues[index] {
                delegate?.didSelectGraph(index: index)
                break
            }
        }
    }
    
    private func drawHorizontalLines() {
        self.layer.sublayers?.forEach({
            if $0 is CAShapeLayer {
                $0.removeFromSuperlayer()
            }
        })
        let horizontalLineInfos = [
            ["value": Float(0.0), "dashed": false],
            ["value": Float(0.10), "dashed": false],
            ["value": Float(0.20), "dashed": false],
            ["value": Float(0.30), "dashed": false],
            ["value": Float(0.40), "dashed": false],
            ["value": Float(0.50), "dashed": false],
            ["value": Float(0.60), "dashed": false],
            ["value": Float(0.70), "dashed": false],
            ["value": Float(0.80), "dashed": false],
            ["value": Float(0.90), "dashed": false],
            ["value": Float(1.0), "dashed": false]]
        for lineInfo in horizontalLineInfos {
            let xPos = CGFloat(0.0)
            let yPos = translateHeightValueToYPosition(value: (lineInfo["value"] as! Float))
            let path = UIBezierPath()
            path.move(to: CGPoint(x: xPos + 20, y: yPos))
            path.addLine(to: CGPoint(x: scrollView.frame.size.width - 10, y: yPos))
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.lineWidth = 0.5
            if lineInfo["dashed"] as! Bool {
                lineLayer.lineDashPattern = [4, 4]
            }
            lineLayer.strokeColor = UIColor.lightGray.cgColor
            self.layer.insertSublayer(lineLayer, at: 0)
        }
    }
    
    private func drawXAxis(index: Int) {
        
        if index > 10 {
            return
        }
        
        var yPos = CGFloat()
        
        let horizontalLineInfos = [
            ["value": Float(0.0), "dashed": false],
            ["value": Float(0.10), "dashed": false],
            ["value": Float(0.20), "dashed": false],
            ["value": Float(0.30), "dashed": false],
            ["value": Float(0.40), "dashed": false],
            ["value": Float(0.50), "dashed": false],
            ["value": Float(0.60), "dashed": false],
            ["value": Float(0.70), "dashed": false],
            ["value": Float(0.80), "dashed": false],
            ["value": Float(0.90), "dashed": false],
            ["value": Float(1.0), "dashed": false]]
        
        yPos  = translateHeightValueToYPosition(value: (horizontalLineInfos[index]["value"] as! Float))
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: -2, y: yPos, width: 20, height: 10)
        textLayer.foregroundColor = ColorCodeConstant.darkTextcolor.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 8
        textLayer.string = String(index * 10)
        mainLayer.addSublayer(textLayer)
    }
    
    private func drawTextValue(xPos: CGFloat, yPos: CGFloat, textValue: String, color: UIColor) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos - 1, y: yPos, width: barWidth+space, height: 22)
        textLayer.foregroundColor = ColorCodeConstant.darkTextcolor.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 14
        textLayer.string = textValue
        mainLayer.addSublayer(textLayer)
    }
    
    private func drawTitle(xPos: CGFloat, yPos: CGFloat, title: String, color: UIColor) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos, y: yPos , width: barWidth + space, height: 80)
        
        
        textLayer.foregroundColor = ColorCodeConstant.darkTextcolor.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.isWrapped = true
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 12
        textLayer.string = title
        mainLayer.addSublayer(textLayer)
    }
    
    private func translateHeightValueToYPosition(value: Float) -> CGFloat {
        let height: CGFloat = CGFloat(value) * (mainLayer.frame.height - bottomSpace - topSpace)
        return mainLayer.frame.height - bottomSpace - height
    }

}
