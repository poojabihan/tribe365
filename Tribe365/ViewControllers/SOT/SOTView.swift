//
//  SOTView.swift
//  Tribe365
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class SOTView: UIViewController {

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var strOrgID = ""
    //SOT Variable
    var arrOfSOTDetails = [SOTDetailModel]()
    var arrOfSOTResultSummary = [SOTDetailResultModel]()
    var IsQuestionnaireAnswerFilled = Bool()
    var IsUserFilledAnswer = Bool()
    var arrForAllSummary = [ModelForAllSummary]()
    
    //MARK: -IBOutlets
    @IBOutlet weak var btnMotivationStructure: UIButton!
    @IBOutlet weak var MSMainView: UIView!
    @IBOutlet weak var btnIndividual: UIButton!
    @IBOutlet weak var btnTeam: UIButton!
    @IBOutlet weak var horiLine: UILabel!
    @IBOutlet weak var vertiLine: UILabel!
    @IBOutlet weak var ViewWithButtons: UIView!
    
    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //Check for motivational
        
        if AuthModel.sharedInstance.role == kSuperAdmin{//Admin
            
           btnMotivationStructure.isEnabled = true
            
           MSMainView.borderColor = ColorCodeConstant.borderLightGrayColor
           MSMainView.borderWidth = 1
            
            btnIndividual.isHidden = true
            btnTeam.isHidden = true
            horiLine.isHidden = true
            vertiLine.isHidden = true
            
            ViewWithButtons.borderColor = UIColor.clear
            ViewWithButtons.borderWidth = 0
            ViewWithButtons.backgroundColor  = UIColor.clear
        }
        else{//User
            
            btnMotivationStructure.isEnabled = false
            
            ViewWithButtons.backgroundColor  = UIColor.white
            ViewWithButtons.borderColor = ColorCodeConstant.borderLightGrayColor
            ViewWithButtons.borderWidth = 1
            
            btnIndividual.isHidden = false
            btnTeam.isHidden = false
            horiLine.isHidden = false
            vertiLine.isHidden = false
            
            MSMainView.borderColor = UIColor.clear
            MSMainView.borderWidth = 0
        }
        //For Navigation bar tranparancy
       self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    //MARK: - IBAction
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnCulturalStructureAction(_ sender: Any) {
        callWebServiceToGetSOTDetail()
    }
    
    @IBAction func btnMotivationStructureAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTips") as! FunctionalLensTribeTips
        objVC.orgID = self.strOrgID
        objVC.SOTObj = "SOT"
        objVC.strMyType = "team"
        self.navigationController?.pushViewController(objVC, animated: true)
        
       
    }
    
    @IBAction func btnIndividualAction(_ sender: Any) {
       
        callWebServiceCheckQuestionsForMail()
        
      
    }
    
    @IBAction func btnTeamAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "COT", bundle: nil).instantiateViewController(withIdentifier: "FunctionalLensTribeTips") as! FunctionalLensTribeTips
        objVC.orgID = self.strOrgID
        objVC.SOTObj = "SOT"
        objVC.strMyType = "team"
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSupportAction(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.sot)
    }
    
    @IBAction func btnMotivationAction(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.motivation)
    }
    
    @IBAction func btnCulturalStructureSupportAction(_ sender: Any) {
        openSupportScreen(strType: NetworkConstant.SupportScreensType.culturalStructure)
    }
    
//    @IBAction func btnBackAction(_ sender: Any) {
//    }
    
    //MARK: - Custom Methods
    func openSupportScreen(strType: String) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = strType
        self.navigationController?.pushViewController(objVC, animated: true)
    }

    //MARK: - Webservice for SOT
    func callWebServiceCheckQuestionsForMail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.MotivationStructure.isSOTMotivationAnswersDone, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS abi vala ",response ?? "")
                    
                    if response!["data"]["isUserAnswersFilled"].boolValue{
                        
                        /*"SOTmotivationStartDate": "2018-11-05 00:00:00",
                         "isSOTmotivationMailSent": "1",
                         "isUserAnswersFilled": "0"
                         */
                        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationStructureGraphView") as!  MotivationStructureGraphView
                        
                        objVC.strOrgID = AuthModel.sharedInstance.orgId
                        objVC.strUserID = AuthModel.sharedInstance.user_id
                        objVC.strMyType = "Indi"
                      self.navigationController?.pushViewController(objVC, animated: true)
                        
                    }
                    else {
                        if AuthModel.sharedInstance.role == kUser{
                            
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
                            
                            self.navigationController?.pushViewController(objVC, animated: true)
                            
                        }
                   
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func callWebServiceToGetSOTDetail() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        
        if AuthModel.sharedInstance.role == kUser {
            
            param = [ "orgId": AuthModel.sharedInstance.orgId]
        }
        else{
            param = [ "orgId": strOrgID]
        }
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.SOT.getSOTdetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTDetails(response: response!, completionHandler: { (arrOfSOTDetail , arrOfSOTResultSummmary, IsQuestionnaireAnswerFilled, IsUserFilledAnswer) in
                        
                        self.arrOfSOTDetails = arrOfSOTDetail
                        self.arrOfSOTResultSummary = arrOfSOTResultSummmary
                        self.IsQuestionnaireAnswerFilled = IsQuestionnaireAnswerFilled
                        self.IsUserFilledAnswer = IsUserFilledAnswer
                    })
                    if AuthModel.sharedInstance.role == kSuperAdmin{ //Admin
                        
                        if self.IsQuestionnaireAnswerFilled == true{
                            
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTOrganisationResultView") as! SOTOrganisationResultView
                            objVC.arrOfSOTDetails = self.arrOfSOTDetails
                            objVC.arrOfSOTResultSummary = self.arrOfSOTResultSummary
                            objVC.strOrgId = self.strOrgID
                            self.navigationController?.pushViewController(objVC, animated: true)
                            
                        }
                        else{
                            
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTStatusView") as! SOTStatusView
                            objVC.modalPresentationStyle = .overCurrentContext
                            objVC.strAdminHai = "true"
                            self.present(objVC, animated: false, completion: nil)
                        }
                    }
                        
                    else{//user
                        
                        if self.IsUserFilledAnswer == true{
                            
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTOrganisationResultView") as! SOTOrganisationResultView
                            objVC.arrOfSOTDetails = self.arrOfSOTDetails
                            objVC.arrOfSOTResultSummary = self.arrOfSOTResultSummary
                            
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                        else{
                            
                            let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
                            
                            self.navigationController?.pushViewController(objVC, animated: true)
                            
                        }
                        
                        
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
