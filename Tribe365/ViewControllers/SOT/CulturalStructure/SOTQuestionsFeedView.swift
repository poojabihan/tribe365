//
//  SOTQuestionsFeedView.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class SOTQuestionsFeedView: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    //MARK:- IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblheaderTitle: UILabel!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var imgOrgLogo: UIImageView!
    
    //MARK:- Varialbes
    var arrOfQuestionsList = [SOTQuestionModel]()
    var selectedDict = [SOTQuestionModel]()
    var arrOfAnwsersList = [SOTQuestionModel]()
    
    var panel = JKNotificationPanel()
    var selectedQuestionIndex = -1
    var arrOfAnswers = [[String : Any]]()
    var strUpdate = ""
    var arrOfSavedAnswers = [[String : Any]]()
    var isSaved = false
    var checkVariable = false

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)
        
        if strUpdate == "TRUE"{
           
            lblheaderTitle.text = "UPDATE/REVIEW SUPERCHARGING QUESTIONS"
           btnSend.setTitle("UPDATE ANSWERS", for: .normal)
            callWebServiceTogetSOTquestionAnswers()
        }
        else{
            
            isSaved = (UserDefaults.standard.value(forKey: "isSavedForCuturalStructure") != nil)
            if isSaved == true {
                arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForCuturalStructure") as! [[String : Any]]
                print(arrOfSavedAnswers)
            }
            callWebServiceToGetSOTDetail()
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        if strUpdate == "TRUE"{
            
            goToBackScreen()

        }
        else{
            if checkVariable == true{
                let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.saveAnswers()
                    
                    NSLog("Save")
                }
                
                alertController.addAction(okAction)
                alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    //run your function here
                    self.goToBackScreen()
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                goToBackScreen()
            }
        }
        }


    @IBAction func btnSubmitAction(_ sender: Any) {
        
        /* var dictOfAnswer = [String:Any]()
         dictOfAnswer = ["answer": arrOfAnswers]*/
        if strUpdate == "TRUE"{

            arrOfAnswers.removeAll()
            for j in (0..<self.arrOfAnwsersList.count){
                
                if arrOfAnwsersList[j].selectedIndex != -1 {
                    
                    let strAnsId =  self.arrOfAnwsersList[j].strAnswerId
                    print(strAnsId)
                    
                    let json = [ "id":strAnsId]
                    
                    arrOfAnswers.append(json)
                }
                else{
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            print(arrOfAnswers)
            callWebServiceToUpdateAnswers()
            
        }
        else{
        arrOfAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
            
            if arrOfQuestionsList[j].selectedIndex != -1 {
                
                let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                print(strAnsId)
                
                let json = [ "id":strAnsId]
                
                arrOfAnswers.append(json)
            }
            else{
                let strmessage = "Please agree with a statement for Question " + String(j + 1)
                
                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                return
                
            }
        }
        print(arrOfAnswers)
        callWebServiceToAddAnswers()
        }
    }
    
    
    //MARK: - CustomMethods
    
    func goToBackScreen(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func saveAnswers(){
        
        //FIRST remove previous saved answers annd add new
        UserDefaults.standard.removeObject(forKey: "SavedDictForCuturalStructure")
        let keyValue = UserDefaults.standard.value(forKey: "SavedDictForCuturalStructure")
        print("Key Value after remove \(String(describing: keyValue))")
        
        //SECOND create new data of answer
        arrOfSavedAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
      
                let strAnsId =  self.arrOfQuestionsList[j].strAnswerId
                print(strAnsId)
                
                let json = [ "id":strAnsId] as [String : Any]
                
                arrOfSavedAnswers.append(json)
           
        }
        print(arrOfSavedAnswers)
    
        //Third add new answer list
        UserDefaults.standard.set(true, forKey: "isSavedForCuturalStructure")
        UserDefaults.standard.set(arrOfSavedAnswers, forKey: "SavedDictForCuturalStructure")
        
        //Fourth print answer list
        arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForCuturalStructure") as! [[String : Any]]
        print(arrOfSavedAnswers)
        
        //go back to previous controller
        self.navigationController?.popViewController(animated: true)
    }
    
    func SetSelctedIndexAndAnwerIdForSavedData()  {
        if self.isSaved == true{
            
            for i in (0..<self.arrOfQuestionsList.count){
                
                    let stroptnId =  arrOfSavedAnswers[i]["id"] as! String
                    
                    print("Current savedOption Id is \(stroptnId)")
                   
                    if stroptnId == ""{
                        
                        print("Blank")
                        
                    arrOfQuestionsList[i].selectedIndex = -1
                    arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[i]["id"] as! String
                    }
                    else if stroptnId == arrOfQuestionsList[i].arrOptions[0].strId{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[i].arrOptions[0].strId) are same ")
                        arrOfQuestionsList[i].selectedIndex = 1
                        arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[i]["id"] as! String
                        
                    }
                    else if stroptnId == arrOfQuestionsList[i].arrOptions[1].strId{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[i].arrOptions[1].strId) are same ")
                        
                        arrOfQuestionsList[i].selectedIndex = 2
                        arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[i]["id"] as! String
                        
                       
                    }
                    else if stroptnId == arrOfQuestionsList[i].arrOptions[2].strId{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[i].arrOptions[2].strId) are same ")
                        
                        arrOfQuestionsList[i].selectedIndex = 3                                                               
                        arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[i]["id"] as! String
                       
                    }
                    else if stroptnId == arrOfQuestionsList[i].arrOptions[3].strId{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[i].arrOptions[3].strId) are same ")
                        
                        arrOfQuestionsList[i].selectedIndex = 4
                        arrOfQuestionsList[i].strAnswerId = arrOfSavedAnswers[i]["id"] as! String
                    
                   }
               }
        }
    }
    
    //MARK: - UITableView Delegate And DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if strUpdate == "TRUE"{
            return arrOfAnwsersList.count
            
        }
        else{
            return arrOfQuestionsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if strUpdate == "TRUE"{
            return arrOfAnwsersList[section].arrOptions.count + 1
            
        }
        else{
            return arrOfQuestionsList[section].arrOptions.count + 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "SectionHeader")!
            
            let lbl: UILabel = cell.viewWithTag(100) as! UILabel
            
            if strUpdate == "TRUE"{
                
                lbl.text = arrOfAnwsersList[indexPath.section].strQuestion
            }
            else{
            lbl.text = arrOfQuestionsList[indexPath.section].strQuestion
            }
            
            cell.separatorInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell : SOTStatementsCell = tableView.dequeueReusableCell(withIdentifier: "SOTStatementsCellNew") as! SOTStatementsCell
            
            if strUpdate == "TRUE"{
                
                cell.lblStatmentFirst.text = arrOfAnwsersList[indexPath.section].arrOptions[indexPath.row - 1].strQuestion
                
                if arrOfAnwsersList[indexPath.section].selectedIndex == indexPath.row {
                    cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
                }
                else {
                    cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
                }
                
                if indexPath.row - 1 == arrOfAnwsersList[indexPath.section].arrOptions.count - 1 {
                    cell.borderViewBottom.constant = 0
                }
                else {
                    cell.borderViewBottom.constant = -1
                }
                return cell
            }
                
            else{
                cell.lblStatmentFirst.text = arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1].strQuestion
                
                if arrOfSavedAnswers.count != 0 {
                    
                    let stroptnId =  arrOfSavedAnswers[indexPath.section]["id"] as! String
                    
                    print("Current savedOption Id is \(stroptnId)")
                    print("\(indexPath.row) of Section \(indexPath.section)")
                    
                    if stroptnId == arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1 ].strId{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1].strId) are same ")
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
                    }
                    else{
                        
                        print("\(stroptnId) and \(arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1].strId) are not same ")
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
                    }
                    
                    //For new Selection
                    if arrOfQuestionsList[indexPath.section].selectedIndex == indexPath.row {
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
                    }
                    else {
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
                    }
                    
                }
                    
                else{
                    
                    if arrOfQuestionsList[indexPath.section].selectedIndex == indexPath.row {
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "check")
                    }
                    else {
                        cell.imgCheckbox.image = #imageLiteral(resourceName: "uncheck")
                    }
                }
                if indexPath.row - 1 == arrOfQuestionsList[indexPath.section].arrOptions.count - 1 {
                    cell.borderViewBottom.constant = 0
                }
                else {
                    cell.borderViewBottom.constant = -1
                }
                
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            return
        }
        
        if strUpdate == "TRUE"{
            
            arrOfAnwsersList[indexPath.section].selectedIndex = indexPath.row
            arrOfAnwsersList[indexPath.section].strAnswerId = arrOfAnwsersList[indexPath.section].arrOptions[indexPath.row - 1].strId
        print(arrOfAnwsersList[indexPath.section].arrOptions[indexPath.row - 1].strId)
            
            tableView.reloadData()
            
        }
            
        else{
        checkVariable = true

        arrOfQuestionsList[indexPath.section].selectedIndex = indexPath.row
        arrOfQuestionsList[indexPath.section].strAnswerId = arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1].strId
        
        print(arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row - 1].strId)
        
        tableView.reloadData()
        }
    }
    
    //MARK: - Call Web Service
    func callWebServiceTogetSOTquestionAnswers(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.SOT.getSOTQuestionAnswers, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTQuestionsAnwserList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfAnwsersList = arrOfQuestionList
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    func callWebServiceToGetSOTDetail() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.SOT.getSotQuestionList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTQuestionsList(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfQuestionsList = arrOfQuestionList
                    })
                    self.SetSelctedIndexAndAnwerIdForSavedData()
                    
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToUpdateAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.SOT.updateSOTQuestionAnswer, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer updated succeessfully.")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    func callWebServiceToAddAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.SOT.addSOTAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer added succeessfully.")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForCuturalStructure")
                    UserDefaults.standard.removeObject(forKey: "isSavedForCuturalStructure")
                    let keyValue = UserDefaults.standard.value(forKey: "SavedDictForCuturalStructure")
                    print("Key Value after remove \(String(describing: keyValue))")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
}
