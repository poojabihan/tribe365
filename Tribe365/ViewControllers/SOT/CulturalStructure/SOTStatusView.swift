//
//  SOTStatusView.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class SOTStatusView: UIViewController , HADropDownDelegate{

    @IBOutlet weak var lblAdminText: UILabel!

    
    var strAdminHai = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if strAdminHai == "true" {
            
            lblAdminText.text = "Culture Structure Questionnaire are not submitted by any members of organisation yet."
        }
        // Do any additional setup after loading the view.
    }

    //MARK: -IBActions
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
}
