//
//  SOTStatementsCell.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class SOTStatementsCell: UITableViewCell {

    
    @IBOutlet weak var lblStatmentFirst: UILabel!
    @IBOutlet weak var borderViewBottom: NSLayoutConstraint!

    @IBOutlet weak var imgCheckbox: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
