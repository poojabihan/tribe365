
//
//  SOTOrganisationResultView.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class SOTOrganisationResultView: UIViewController, UITableViewDelegate, UITableViewDataSource, HADropDownDelegate, UITextFieldDelegate {
    
    //MARK: - IBOutlets

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblPersonTitle: UILabel!
    @IBOutlet weak var lblPowerTitle: UILabel!
    @IBOutlet weak var lblRoleTitle: UILabel!
    @IBOutlet weak var lblTribeTitle: UILabel!
    
    @IBOutlet weak var lblPersonScore: UILabel!
    @IBOutlet weak var lblPowerScore: UILabel!
    @IBOutlet weak var lblRoleScore: UILabel!
    @IBOutlet weak var lblTribeScore: UILabel!
    @IBOutlet weak var lblResultText: UILabel!
    @IBOutlet weak var imgResult: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnRedoReview: UIButton!
    @IBOutlet weak var lblNoData: UILabel!

    @IBOutlet weak var viewOf4View: UIView!
    @IBOutlet weak var btnRedoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bynRedoReviewheight: NSLayoutConstraint!
    
    @IBOutlet weak var lblSOOCS: UILabel!
    
    @IBOutlet weak var HeightConstraintForDropDown: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraintforView: NSLayoutConstraint!
    
   
    @IBOutlet weak var viewForDropDown: UIView!
  
    @IBOutlet weak var dropDownDept: HADropDown!
    
    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!

    
    //MARK: -Variables
    
    let panel = JKNotificationPanel()
    var arrOfSOTDetails = [SOTDetailModel]()
    var arrOfSOTResultSummary = [SOTDetailResultModel]()
    var isAnswerFilled = Bool()
    var arrForAllSummary = [ModelForAllSummary]()
    var strOrgId = ""
    var comingFromReport = ""
    
    var IsQuestionnaireAnswerFilled = Bool()
    var IsUserFilledAnswer = Bool()

    
    //For all Offices and department list Api
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    
    //Arr For All Department
    var arrForDepartmentName = [DepartmentModel]()
    var arrOfAllDeparmentList = [String]()
    var arrOfAllDepartmentIdList = [String]()
    
    
    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0
    
    
    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()
    
    var loadForFirst = false
    var havingTrueValue = false
    var selectedFromDate = ""
    var selectedToDate = ""

    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if comingFromReport == "True"{
            lblHeader.text = "REPORT - CULTURAL STRUCTURE"
            HeightConstraintForDropDown.constant = 50
            topConstraintforView.constant = 15
            viewForDropDown.alpha = 1.0
            callWebServiceToGetAllOfficesAndDepartmentList()
            callWebServiceToGetAllDepartments()
            callWebServiceCulturalStuctureReport()
            dropDownDept.delegate = self
            dropDownOffice.delegate = self
            
        }
        else{
            
            HeightConstraintForDropDown.constant = 0
            topConstraintforView.constant = 0
            viewForDropDown.alpha = 0.0
            callWebServiceToGetSOTDetail()

            //NOte For Report crash
//            SetScoreForModule()
//            SetOrganisationCultureReultImage()
//            tblView.reloadData()
        }
        
        if AuthModel.sharedInstance.role == kSuperAdmin {
            
            bynRedoReviewheight.constant = 0
            btnRedoTopConstraint.constant = -20
            btnRedoReview.isHidden = true
        }
        else{
            
            if comingFromReport == "True"{
                bynRedoReviewheight.constant = 0
                btnRedoTopConstraint.constant = -20
                btnRedoReview.isHidden = true             }
            else{
                
                bynRedoReviewheight.constant = 45
                btnRedoTopConstraint.constant = 20
                btnRedoReview.isHidden = false          }
          
        }
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                callWebServiceCulturalStuctureReport()
            }
        }
        else {
            
            if selectedFromDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        callWebServiceCulturalStuctureReport()
                    }
                }
            }
            
            
        }
    }
    
    //MARK: - IBActions
    @IBAction func btnRedoReviewAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SOTQuestionsFeedView") as! SOTQuestionsFeedView
        objVC.strUpdate = "TRUE"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - CustomMethods
    
    func SetOrganisationCultureReultImage(){
        
        if self.arrOfSOTResultSummary.count > 0 {
            
        arrForAllSummary.removeAll()
            
        lblResultText.text = arrOfSOTResultSummary[0].strTitle + " organisation culture structure"
        
        let url = URL(string:arrOfSOTResultSummary[0].strImageUrl)
        imgResult.kf.setImage(with: url, placeholder: UIImage(named :"No Logo"), progressBlock:nil, completionHandler: nil)
        
        var strSummary = ""
        let model = ModelForAllSummary()

        for i in (0..<self.arrOfSOTResultSummary[0].arrOfSummary.count){
            
            
            if strSummary == "" {
                strSummary = "  \u{2022}  " + self.arrOfSOTResultSummary[0].arrOfSummary[i].strSummary
            }
            else {
                strSummary = strSummary + "\n\n  \u{2022}  " +  self.arrOfSOTResultSummary[0].arrOfSummary[i].strSummary
            }
            
            self.arrForAllSummary.append(model)
            print(arrForAllSummary[0].strValue ?? "")
        }
        
        if self.arrOfSOTResultSummary[0].arrOfSummary.count > 0 {
            
            model.strValue = NSMutableAttributedString.init(string: strSummary)
            
            self.arrForAllSummary.append(model)
        }
        
        tblView.reloadData()

        }
        
    }
    
    func SetScoreForModule()  {
        
            if arrOfSOTResultSummary.count == 0{
                
               self.tblView.isHidden = false
                self.lblSOOCS.isHidden = true
                self.viewOf4View.isHidden = true
                self.imgResult.isHidden = true
                self.lblResultText.isHidden = true
                self.lblNoData.text = ""
                self.arrForAllSummary.removeAll()
                self.tblView.reloadData()
                self.lblNoData.isHidden = true
                
             }
            else{
                
                self.tblView.isHidden = false
                self.lblSOOCS.isHidden = false
                self.viewOf4View.isHidden = false
                self.imgResult.isHidden = false
                self.lblResultText.isHidden = false
                self.lblNoData.text = " "
                self.lblNoData.isHidden = true
                self.SetOrganisationCultureReultImage()
                self.tblView.reloadData()
                
                
                lblPersonScore.text = arrOfSOTDetails[0].strSOTCount
                lblPersonTitle.text = arrOfSOTDetails[0].strTitle.uppercased()
                
                lblPowerScore.text = arrOfSOTDetails[1].strSOTCount
                lblPowerTitle.text = arrOfSOTDetails[1].strTitle.uppercased()
                
                lblRoleScore.text = arrOfSOTDetails[2].strSOTCount
                lblRoleTitle.text = arrOfSOTDetails[2].strTitle.uppercased()
                
                lblTribeScore.text = arrOfSOTDetails[3].strSOTCount
                lblTribeTitle.text = arrOfSOTDetails[3].strTitle.uppercased()
                
        }
        
    }
    
    //MARK: - UITableView Delegate And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrForAllSummary.count == 0{
        return 0
        }
        else{
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*"user_type" = Admin;
         "user_type" = User;*/
        
        let cell : SOTSummaryCell = tableView.dequeueReusableCell(withIdentifier: "SOTSummaryCell") as! SOTSummaryCell
        
        cell.lblSummary.attributedText = arrForAllSummary[indexPath.row].strValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    //MARK: - call Web Service
    
    func callWebServiceToGetSOTDetail() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        
        if AuthModel.sharedInstance.role == kUser {
            
            param = [ "orgId": AuthModel.sharedInstance.orgId]
        }
        else{
            param = [ "orgId": strOrgId]
        }
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.SOT.getSOTdetail, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTDetails(response: response!, completionHandler: { (arrOfSOTDetail , arrOfSOTResultSummmary, IsQuestionnaireAnswerFilled, IsUserFilledAnswer) in
                        
                        self.arrOfSOTDetails = arrOfSOTDetail
                        self.arrOfSOTResultSummary = arrOfSOTResultSummmary
                       
                    })
                    self.SetScoreForModule()
                    self.SetOrganisationCultureReultImage()
                    self.tblView.reloadData()
                    }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceCulturalStuctureReport() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "orgId":"42"
         }*/
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            
            param = [ "orgId": strOrgId,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
            
        }
        else{
            param = [ "orgId": strOrgId,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
        }
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getSOTcultureStructureReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTDetails(response: response!, completionHandler: { (arrOfSOTDetail , arrOfSOTResultSummmary, IsQuestionnaireAnswerFilled, IsUserFilledAnswer) in
                        
                        self.arrOfSOTDetails = arrOfSOTDetail
                        self.arrOfSOTResultSummary = arrOfSOTResultSummmary
                        self.IsQuestionnaireAnswerFilled = IsQuestionnaireAnswerFilled
                        self.IsUserFilledAnswer = IsUserFilledAnswer
                    })
                    
             /*   if self.IsQuestionnaireAnswerFilled == false{
                        
                        self.tblView.isHidden = false
                        self.lblSOOCS.isHidden = true
                        self.viewOf4View.isHidden = true
                        self.imgResult.isHidden = true
                        self.lblResultText.isHidden = true
                        self.lblNoData.text = ""
                        self.arrForAllSummary.removeAll()
                        self.tblView.reloadData()
                        self.havingTrueValue = true
                        self.lblNoData.isHidden = true
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                    }
                     else if self.IsUserFilledAnswer == true{
                        
                    self.tblView.isHidden = false
                    self.lblSOOCS.isHidden = true
                    self.viewOf4View.isHidden = true
                    self.imgResult.isHidden = true
                    self.arrForAllSummary.removeAll()
                    self.tblView.reloadData()
                    self.havingTrueValue = true
                    self.lblResultText.isHidden = true
                    self.lblNoData.text = ""
                    self.lblNoData.isHidden = true
                    
                }
                else{*/
                    
                    self.tblView.isHidden = false
                    self.lblSOOCS.isHidden = false
                    self.viewOf4View.isHidden = false
                    self.imgResult.isHidden = false
                    self.lblResultText.isHidden = false
                    self.lblNoData.text = ""
                    self.lblNoData.isHidden = true
                    self.SetOrganisationCultureReultImage()
                    self.SetScoreForModule()
                    self.tblView.reloadData()
                    }
               // }
                else {
                    //self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgId] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        self.arrDepartment = arrDept
                        self.arrAllDepartment = arrDept
                        self.arrOfficesName = arrOfcNames
                        self.arrDepartmentNames = arrDeptNames
                        self.arrAllDepartmentNames = arrDeptNames
                        self.setData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllDepartments() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - HADropDown Delegate Method
    
    func setData() {
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
            dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    func CreateDropDownListOfAllDepartemntArray(){
        
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
    }
    
    
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
       // selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            selectedIndexOfDepartment = 0
            selectedIndexOfOffice = 0
            dropDownDept.title = "ALL DEPARTMENTS"
            selectedIndexOfOffice = index
            if index != 0 {
                arrDepartment = arrOffices[index - 1].arrDepartment
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                
                if dropDownOffice.title == "ALL OFFICES"
                {
                    dropDownDept.items = arrOfAllDeparmentList
                }
                // arrDepartment = arrAllDepartment
                //arrDepartmentNames = arrAllDepartmentNames
            }
            setData()
        }
        else if dropDown == dropDownDept {
            selectedIndexOfDepartment = index
        }
        callWebServiceCulturalStuctureReport()
    }
    
    
}
