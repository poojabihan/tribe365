//
//  Motivation1SwipeViewViewController.swift
//  Tribe365
//
//  Created by Apple on 08/04/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class SupportScreensViewController: UIViewController {

    
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var imgSwipe: UIImageView!
    var arrOfImgs = ["Motivation1.png","Motivation2.png","Motivation3.png"]
    var currentPage = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        imgSwipe.isUserInteractionEnabled = true
        
        let swipeLeftGesture=UISwipeGestureRecognizer(target: self, action: #selector(SwipeLeftImage))

        swipeLeftGesture.direction = UISwipeGestureRecognizerDirection.left
        imgSwipe.addGestureRecognizer(swipeLeftGesture)
        
        let swipeRightGesture=UISwipeGestureRecognizer(target: self, action: #selector(SwipeRightImage))
        swipeRightGesture.direction = UISwipeGestureRecognizerDirection.right
        imgSwipe.addGestureRecognizer(swipeRightGesture)
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: UIColor.init(red: 255.0/255.0, green: 0/255.0, blue: 25.0/255.0, alpha: 1.0))
        pageControll.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        pageControll.currentPageIndicatorTintColor = UIColor.init(red: 255.0/255.0, green: 0/255.0, blue: 25.0/255.0, alpha: 1.0)

    }

    @IBAction func btnBackAction(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: true)
    }
    
    @objc func SwipeLeftImage(){
        
        if currentPage == arrOfImgs.count-1 {
            return
        }
        currentPage+=1
        imgSwipe.image = UIImage.init(named: arrOfImgs[currentPage])
        pageControll.currentPage = currentPage
    }
    
    @objc func SwipeRightImage(){
        
        if currentPage == 0 {
            return
        }
        currentPage-=1
        imgSwipe.image = UIImage.init(named: arrOfImgs[currentPage])
        pageControll.currentPage = currentPage
    }
}

extension UIImage {
    class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
        context.addEllipse(in: rect)
        context.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
