//
//  MOtivationQouestionnarieView.swift
//  Tribe365
//
//  Created by Apple on 10/01/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import  NVActivityIndicatorView

class MotivationQuestionnarieView: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{

    //MARK:- IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    //MARK:- Varialbes
    var arrOfQuestionsList = [SOTMotivationModel]()
    var arrOfUpdateAnswersList = [SOTMotivationModel]()
    var selectedDict = [SOTMotivationModel]()
    var arrOfAnwsersList = [SOTMotivationModel]()
    var arrOfSavedAnswers = [[String : Any]]()
    var panel = JKNotificationPanel()
    var selectedQuestionIndex = -1
    var arrOfAnswers = [[String : Any]]()
    var comingFromReduReviewBtn = Bool()
    var checkVariable = ""
    var isSaved = Bool()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

        tblView.separatorStyle = .none
        if comingFromReduReviewBtn == true{
            btnSend.setTitle("UPDATE", for: .normal)
            lblHeader.text = "UPDATE/REVIEW MOTIVATION QUESTIONS"
            callWebServiceTogetMotivationAnsweredList()
            
        }
        else{
        
            isSaved = (UserDefaults.standard.value(forKey: "isSavedMotivation") != nil)
            if isSaved == true {
                arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForMotivation") as! [[String : Any]]
                print(arrOfSavedAnswers)
            }
            
        callWebServiceToGetMotivationQuestionsList()
        }
    }

    //MARK:- IBActions
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        if comingFromReduReviewBtn == false{
            if checkVariable == "filled One"{
                let alertController = UIAlertController(title: "", message: "Do you want to save the answers?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.saveAnswers()
                    
                    NSLog("Save")
                }
                
                alertController.addAction(okAction)
                alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    //run your function here
                    self.goToBackScreen()
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                goToBackScreen()
            }
        }
            
        else{
            goToBackScreen()
        }
        
    }
    
    func goToBackScreen(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func saveAnswers(){
        
        //FIRST remove previous saved answers annd add new
        UserDefaults.standard.removeObject(forKey: "SavedDictForMotivation")
        let keyValue = UserDefaults.standard.value(forKey: "SavedDictForMotivation")
        print("Key Value after remove \(String(describing: keyValue))")
        
        //SECOND create new data of answer
        arrOfSavedAnswers.removeAll()
        for j in (0..<self.arrOfQuestionsList.count){
            
                let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                print(strQuestionId)
                var arrOfOptions = [[String: Any]]()
                
                let option1 =  [
                    
                    "optionId" : self.arrOfQuestionsList[j].arrOptions[0].strOptionId,
                    "rating":self.arrOfQuestionsList[j].arrOptions[0].strSelectedOption1
                ]
                
                arrOfOptions.append(option1)
                
                
                let option2 =  [
                    
                    "optionId" : self.arrOfQuestionsList[j].arrOptions[1].strOptionId,
                    "rating":self.arrOfQuestionsList[j].arrOptions[1].strSelectedOption2
                ]
                
                arrOfOptions.append(option2)
                let json = [  "questionId": strQuestionId,
                              "option": arrOfOptions ] as [String : Any]
                
                print(json)
                arrOfSavedAnswers.append(json)
           
        }
        
        //Third add new answer list
        UserDefaults.standard.set(true, forKey: "isSavedMotivation")
        UserDefaults.standard.set(arrOfSavedAnswers, forKey: "SavedDictForMotivation")
        
        //Fourth print answer list
        arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForMotivation") as! [[String : Any]]
        print(arrOfSavedAnswers)
        
        //go back to previous controller
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
       
        if arrOfUpdateAnswersList.count != 0 {
            
            arrOfAnswers.removeAll()
            for j in (0..<self.arrOfUpdateAnswersList.count){
                
                if arrOfUpdateAnswersList[j].arrOptions[0].selectedIndex != -1 {
                    
                    let strQuestionId = self.arrOfUpdateAnswersList[j].strQuestionId
                    print(strQuestionId)
                    var arrOfOptions = [[String: Any]]()
                    
                    let option1 =  [
                        
                    "answerId" : self.arrOfUpdateAnswersList[j].arrOptions[0].strAnswerId,
                "rating":self.arrOfUpdateAnswersList[j].arrOptions[0].strSelectedOption1
                    ]
                    
                    arrOfOptions.append(option1)
                    
                    
                    let option2 =  [
                        
                        "answerId" : self.arrOfUpdateAnswersList[j].arrOptions[1].strAnswerId,
                        "rating":self.arrOfUpdateAnswersList[j].arrOptions[1].strSelectedOption2
                    ]
                    
                    arrOfOptions.append(option2)
                    let json = [  "questionId": strQuestionId,
                                  "option": arrOfOptions ] as [String : Any]
                    
                    arrOfAnswers.append(json)
                }
                else{
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
            }
            
            print(arrOfAnswers)
            callWebServiceToUpdateAnswers()
        }
        else{
            arrOfAnswers.removeAll()
            for j in (0..<self.arrOfQuestionsList.count){
                
                if arrOfQuestionsList[j].arrOptions[0].selectedIndex != -1 {
                
                    let strQuestionId = self.arrOfQuestionsList[j].strQuestionId
                    print(strQuestionId)
                    var arrOfOptions = [[String: Any]]()
              
                    let option1 =  [
                        
            "optionId" : self.arrOfQuestionsList[j].arrOptions[0].strOptionId,
         "rating":self.arrOfQuestionsList[j].arrOptions[0].strSelectedOption1
                    ]
                   
                    arrOfOptions.append(option1)
                    
                    
                    let option2 =  [
                        
                        "optionId" : self.arrOfQuestionsList[j].arrOptions[1].strOptionId,
                        "rating":self.arrOfQuestionsList[j].arrOptions[1].strSelectedOption2
                    ]
                    
                    arrOfOptions.append(option2)
                    let json = [  "questionId": strQuestionId,
                                  "option": arrOfOptions ] as [String : Any]
                    
                    arrOfAnswers.append(json)
                }
                else{
                    let strmessage = "Please agree with a statement for Question " + String(j + 1)
                    
                    self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: strmessage)
                    return
                    
                }
        }
            
            print(arrOfAnswers)
            callWebServiceToAddAnswers()
        }
    }
    
    //MARK: - UITableView Delegate And DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if arrOfUpdateAnswersList.count != 0{
            return arrOfUpdateAnswersList.count
        }
        else{
            return arrOfQuestionsList.count

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrOfUpdateAnswersList.count != 0{
            return arrOfUpdateAnswersList[section].arrOptions.count

        }
        else{
        return arrOfQuestionsList[section].arrOptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x:tableView.frame.origin.x + 15 , y: 0, width: tableView.frame.size.width - 15 , height: 50))
        
        headerView.layer.borderColor = ColorCodeConstant.borderLightGrayColor.cgColor
        headerView.layer.borderWidth = 1.0
        headerView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width - 15 , height: headerView.frame.height - 10)
        
        if arrOfUpdateAnswersList.count != 0{
            label.text =  arrOfUpdateAnswersList[section].strQuestionId + ". " + arrOfUpdateAnswersList[section].strQuestionName

        }
        else{
            
        label.text =  arrOfQuestionsList[section].strQuestionId + ". " + arrOfQuestionsList[section].strQuestionName
        }
        label.textAlignment = .natural
        label.backgroundColor = UIColor(red: 225.0, green: 225.0, blue: 225.0, alpha: 0.6)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)// my custom font
        label.textColor = ColorCodeConstant.darkTextcolor // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let footerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width , height: 10))

        return footerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell : MotivationOptionCell = tableView.dequeueReusableCell(withIdentifier: "MotivationOptionCell") as! MotivationOptionCell
        
        cell.btnZero.tag = indexPath.section
        cell.btnOne.tag = indexPath.section
        cell.btnTwo.tag = indexPath.section
        cell.btnThree.tag = indexPath.section
        cell.btnFour.tag = indexPath.section
        cell.btnFive.tag = indexPath.section
        
        cell.btnZero.addTarget(self, action: #selector(SelectedOption0(_:)), for: .touchUpInside)
        cell.btnOne.addTarget(self, action: #selector(SelectedOption1(_:)), for: .touchUpInside)
        cell.btnTwo.addTarget(self, action: #selector(SelectedOption2(_:)), for: .touchUpInside)
        cell.btnThree.addTarget(self, action: #selector(SelectedOption3(_:)), for: .touchUpInside)
        cell.btnFour.addTarget(self, action: #selector(SelectedOption4(_:)), for: .touchUpInside)
        cell.btnFive.addTarget(self, action: #selector(SelectedOption5(_:)), for: .touchUpInside)
        
        if arrOfUpdateAnswersList.count != 0{
            cell.lblOption.text = arrOfUpdateAnswersList[indexPath.section].arrOptions[indexPath.row].strOption.capitalized
            
            
            if indexPath.row == arrOfUpdateAnswersList[indexPath.section].arrOptions.count - 1  {
                
                cell.lblhoriLine.isHidden = false
                
            }
            else{
                cell.lblhoriLine.isHidden = true
                
            }
            if indexPath.row == 0 {
                cell.changeButtonColorOfIndex(index: String(arrOfUpdateAnswersList[indexPath.section].arrOptions[indexPath.row].strSelectedOption1))
                
                cell.btnZero.isUserInteractionEnabled = true
                cell.btnOne.isUserInteractionEnabled = true
                cell.btnTwo.isUserInteractionEnabled = true
                cell.btnThree.isUserInteractionEnabled = true
                cell.btnFour.isUserInteractionEnabled = true
                cell.btnFive.isUserInteractionEnabled = true
                }
                else {
                    cell.changeButtonColorOfIndex(index: String(arrOfUpdateAnswersList[indexPath.section].arrOptions[indexPath.row].strSelectedOption2))
                    
                    cell.btnZero.isUserInteractionEnabled = false
                    cell.btnOne.isUserInteractionEnabled = false
                    cell.btnTwo.isUserInteractionEnabled = false
                    cell.btnThree.isUserInteractionEnabled = false
                    cell.btnFour.isUserInteractionEnabled = false
                    cell.btnFive.isUserInteractionEnabled = false
                    
                }
           
        }
        else{
        cell.lblOption.text = arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row].strOption.capitalized
            
            if indexPath.row == arrOfQuestionsList[indexPath.section].arrOptions.count - 1  {
                
                cell.lblhoriLine.isHidden = false
                
            }
            else{
                cell.lblhoriLine.isHidden = true
                
            }
            //Check For saved Answers
            
            if arrOfSavedAnswers.count != 0 {
                
                if arrOfSavedAnswers[indexPath.row]["questionId"] as! String == arrOfQuestionsList[indexPath.row].strQuestionId {
                    
                    var arrOfSavedOptions = [[String : Any]]()
                    arrOfSavedOptions =  arrOfSavedAnswers[indexPath.section]["option"] as! [[String : Any]]
                    print(arrOfSavedOptions)
                    print(arrOfSavedOptions[0]["rating"] as! String)
                    
                    print("Success")
                    if indexPath.row == 0 {
                        cell.changeButtonColorOfIndex(index: arrOfSavedOptions[indexPath.row]["rating"] as! String)
                        
                        cell.btnZero.isUserInteractionEnabled = true
                        cell.btnOne.isUserInteractionEnabled = true
                        cell.btnTwo.isUserInteractionEnabled = true
                        cell.btnThree.isUserInteractionEnabled = true
                        cell.btnFour.isUserInteractionEnabled = true
                        cell.btnFive.isUserInteractionEnabled = true

                    }
                    else {
                        cell.changeButtonColorOfIndex(index: arrOfSavedOptions[indexPath.row]["rating"] as! String)
                        
                        cell.btnZero.isUserInteractionEnabled = false
                        cell.btnOne.isUserInteractionEnabled = false
                        cell.btnTwo.isUserInteractionEnabled = false
                        cell.btnThree.isUserInteractionEnabled = false
                        cell.btnFour.isUserInteractionEnabled = false
                        cell.btnFive.isUserInteractionEnabled = false
                        
                    }
                }
                //for new Selected
                if indexPath.row == 0 {
                    
                    cell.changeButtonColorOfIndex(index: arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row].strSelectedOption1)
                    
                    cell.btnZero.isUserInteractionEnabled = true
                    cell.btnOne.isUserInteractionEnabled = true
                    cell.btnTwo.isUserInteractionEnabled = true
                    cell.btnThree.isUserInteractionEnabled = true
                    cell.btnFour.isUserInteractionEnabled = true
                    cell.btnFive.isUserInteractionEnabled = true
                    
                    
                }
                else {
                    cell.changeButtonColorOfIndex(index: arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row].strSelectedOption2)
                    
                    cell.btnZero.isUserInteractionEnabled = false
                    cell.btnOne.isUserInteractionEnabled = false
                    cell.btnTwo.isUserInteractionEnabled = false
                    cell.btnThree.isUserInteractionEnabled = false
                    cell.btnFour.isUserInteractionEnabled = false
                    cell.btnFive.isUserInteractionEnabled = false
                }
            }
            else{
            if indexPath.row == 0 {
                
                cell.changeButtonColorOfIndex(index: arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row].strSelectedOption1)
                
                cell.btnZero.isUserInteractionEnabled = true
                cell.btnOne.isUserInteractionEnabled = true
                cell.btnTwo.isUserInteractionEnabled = true
                cell.btnThree.isUserInteractionEnabled = true
                cell.btnFour.isUserInteractionEnabled = true
                cell.btnFive.isUserInteractionEnabled = true
                
                
            }
            else {
                cell.changeButtonColorOfIndex(index: arrOfQuestionsList[indexPath.section].arrOptions[indexPath.row].strSelectedOption2)
                
                cell.btnZero.isUserInteractionEnabled = false
                cell.btnOne.isUserInteractionEnabled = false
                cell.btnTwo.isUserInteractionEnabled = false
                cell.btnThree.isUserInteractionEnabled = false
                cell.btnFour.isUserInteractionEnabled = false
                cell.btnFive.isUserInteractionEnabled = false
                
            }
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 137.5
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var sectionHeaderHeight = CGFloat()
        sectionHeaderHeight = 50
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y >= 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    
    //MARK: - Custom functions
    
    func SetSelctedIndexAndAnwerIdForSavedData()  {
        
        if self.isSaved == true{
            
            for i in (0..<self.arrOfQuestionsList.count){
                for j in (0..<self.arrOfSavedAnswers.count){
                    
                    if self.arrOfSavedAnswers[j]["questionId"] as! String == self.arrOfQuestionsList[i].strQuestionId{
                        
                        var arrOfSavedOptions = [[String : Any]]()
                        arrOfSavedOptions =  arrOfSavedAnswers[j]["option"] as! [[String : Any]]
                        print(arrOfSavedOptions)
                        print(arrOfSavedOptions[0]["rating"] as! String)
                        
                        if arrOfSavedOptions[0]["rating"] as! String == ""{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = -1
                            
                            arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = ""
                            arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = ""
                        }
                            
                        else if arrOfSavedOptions[0]["rating"] as! String == "0"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                            
                        arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "0"
                        arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "5"
                        }
                        else if arrOfSavedOptions[0]["rating"] as! String == "1"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                        arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "1"
                        arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "4"
                        }
                        else if arrOfSavedOptions[0]["rating"] as! String == "2"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                            
                        arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "2"
                        arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "3"
                        }
                        else if arrOfSavedOptions[0]["rating"] as! String == "3"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                            arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "3"
                            arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "2"
                        }
                        else if arrOfSavedOptions[0]["rating"] as! String == "4"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                            arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "4"
                            arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "1"
                        }
                        else if arrOfSavedOptions[0]["rating"] as! String == "5"{
                            arrOfQuestionsList[i].arrOptions[0].selectedIndex = i
                            
                            arrOfQuestionsList[i].arrOptions[0].strSelectedOption1 = "5"
                            arrOfQuestionsList[i].arrOptions[1].strSelectedOption2 = "0"
                        }
                    }
                }
            }
        }
    }
    @objc func SelectedOption0(_ sender: UIButton) {
        
        if arrOfUpdateAnswersList.count != 0{
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "0"
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "5"
            
            tblView.reloadData()
            print("0")
            
        }
        else{
            
        checkVariable = "filled One"

        arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "0"
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "5"
        
        tblView.reloadData()
        print("0")
        }
    }
    
    
    @objc func SelectedOption1(_ sender: UIButton) {
    
        if arrOfUpdateAnswersList.count != 0{
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "1"
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "4"
            
            tblView.reloadData()
            print("1")

        }
        else{
            
        checkVariable = "filled One"

        arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "1"
        
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "4"

        tblView.reloadData()
        print("1")
        }
    }
    
    @objc func SelectedOption2(_ sender: UIButton) {
        
        if arrOfUpdateAnswersList.count != 0{
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "2"
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "3"
            
            tblView.reloadData()
            print("2")
        }
        else{
            
         checkVariable = "filled One"

        arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "2"
        
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "3"

        tblView.reloadData()
            print("2")
        }
    }
    
    @objc func SelectedOption3(_ sender: UIButton) {
        
        if arrOfUpdateAnswersList.count != 0{
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "3"
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "2"
            tblView.reloadData()
            print("3")
        }
        else{
            
        checkVariable = "filled One"

         arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "3"
        
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "2"
        tblView.reloadData()
        print("3")
        }
    }
    
    @objc func SelectedOption4(_ sender: UIButton) {
        
        if arrOfUpdateAnswersList.count != 0{
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "4"
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "1"
            
            tblView.reloadData()
            print("4")
        }
        else{
            
            checkVariable = "filled One"

        arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "4"
        
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "1"
        
        tblView.reloadData()
        print("4")
        }
    }
    
    @objc func SelectedOption5(_ sender: UIButton) {
        
        if arrOfUpdateAnswersList.count != 0{
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].selectedIndex = sender.tag
            arrOfUpdateAnswersList[sender.tag].arrOptions[0].strSelectedOption1 = "5"
            
            arrOfUpdateAnswersList[sender.tag].arrOptions[1].strSelectedOption2 = "0"
            
            tblView.reloadData()
            print("5")
        }
        else{
            
            checkVariable = "filled One"

            
        arrOfQuestionsList[sender.tag].arrOptions[0].selectedIndex = sender.tag
        arrOfQuestionsList[sender.tag].arrOptions[0].strSelectedOption1 = "5"
        
        arrOfQuestionsList[sender.tag].arrOptions[1].strSelectedOption2 = "0"

        tblView.reloadData()
        print("5")
        }
    }
    
    
    //MARK: - call web services

    func callWebServiceToGetMotivationQuestionsList() {
      NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
         WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.MotivationStructure.getSOTmotivationQuestions, param: nil, withHeader: true ) { (response, errorMsg) in
            
           NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    SOTParser.parseSOTMOtivationQuestionsParser(response: response!, completionHandler: { (arrOfQuestionList) in

                        self.arrOfQuestionsList = arrOfQuestionList
                    })
                    
                    self.SetSelctedIndexAndAnwerIdForSavedData()
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func callWebServiceTogetMotivationAnsweredList(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.MotivationStructure.getSOTmotivationCompletedAnswer, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                     SOTParser.parseSOTMOtivationQuestionsAnswerParser(response: response!, completionHandler: { (arrOfQuestionList) in
                        
                        self.arrOfUpdateAnswersList = arrOfQuestionList
                    })
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    func callWebServiceToUpdateAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.MotivationStructure.updateSOTmotivationAnswers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
                
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer Updated succeessfully.")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    func callWebServiceToAddAnswers(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param =  [
            "answer" : arrOfAnswers
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.MotivationStructure.addSOTmotivationAnswer, param: param, withHeader: true ) { (response, errorMsg) in
            
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
                
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Answer added succeessfully.")
                    
                    UserDefaults.standard.removeObject(forKey: "SavedDictForMotivation")
                    UserDefaults.standard.removeObject(forKey: "isSavedMotivation")
                    let keyValue = UserDefaults.standard.value(forKey: "SavedDictForMotivation")
                    print("Key Value after remove \(String(describing: keyValue))")
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    

}
