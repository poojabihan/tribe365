//
//  MotivationStructureGraphView.swift
//  Tribe365
//
//  Created by Apple on 15/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
class MotivationStructureGraphView: UIViewController, UITableViewDelegate, UITableViewDataSource, HADropDownDelegate, UITextFieldDelegate {

    //MARK: - IBOutlets
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var basicBarChart: BasicBarChart!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var imgOrgLogo: UIImageView!

    
    @IBOutlet weak var viewForDropDown: UIView!
    @IBOutlet weak var HeightConstraintForDropDown: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraintforView: NSLayoutConstraint!
    
    @IBOutlet weak var dropDownOffice: HADropDown!
    @IBOutlet weak var dropDownDept: HADropDown!

    @IBOutlet weak var btnRedoReview: UIButton!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!

    //MARK: - Varialbes
    var strOrgID = ""
    var strUserID = ""
    var strMyType = ""
    var strUserName = ""
    var panel = JKNotificationPanel()
    var comingFromReports = ""
    var arrOfMotivationData = [MotivationStructureGraphModel]()
    
    //For all Offices and department list Api
    var arrOffices = [OfficeModel]()
    var arrDepartment = [DepartmentModel]()
    var arrAllDepartment = [DepartmentModel]()
    
    //Arr For All Department
    var arrForDepartmentName = [DepartmentModel]()
    var arrOfAllDeparmentList = [String]()
    var arrOfAllDepartmentIdList = [String]()
    var selectedIndexOfOffice = 0
    var selectedIndexOfDepartment = 0
    var arrOfficesName = [String]()
    var arrDepartmentNames = [String]()
    var arrAllDepartmentNames = [String]()
    
    var loadForFirst = false
    var selectedFromDate = ""
    var selectedToDate = ""

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: AuthModel.sharedInstance.organisation_logo)
        imgOrgLogo.kf.setImage(with: url)

    }

    override func viewWillAppear(_ animated: Bool) {
        
        if AuthModel.sharedInstance.role == kUser{
            
            if strMyType == "team"{
                
                lblUsername.text = strUserName
                btnRedoReview.isHidden = true
            }
            else{
                if comingFromReports == "True" {
                    lblUsername.text = strUserName.capitalized
                    btnRedoReview.isHidden = true
                }
                else {
                    lblUsername.text = AuthModel.sharedInstance.name.capitalized
                    btnRedoReview.isHidden = false
                }
            }
        }
        else if AuthModel.sharedInstance.role == kSuperAdmin{
            
            btnRedoReview.isHidden = true
            lblUsername.text = strUserName

        }
        else{
            
            lblUsername.text = strUserName
        }
        
        if comingFromReports == "True" {
            
            HeightConstraintForDropDown.constant = 50
            btnRedoReview.isHidden = true
            topConstraintforView.constant = 15
            viewForDropDown.alpha = 1.0
            callWebServiceForMotivatioStructureReport()
            dropDownDept.delegate = self
            dropDownOffice.delegate = self
            callWebServiceToGetAllOfficesAndDepartmentList()
            callWebServiceToGetAllDepartments()
        }
        else{
            
            HeightConstraintForDropDown.constant = 0
            topConstraintforView.constant = 0
            viewForDropDown.alpha = 0.0
            callWebServiceForMotivatioStructureDetail()
        }
        tblList.separatorStyle = .none
    }
    
    func generateDataEntries() -> [BarEntry] {
        
        var result: [BarEntry] = []
        
        for j in (0..<self.arrOfMotivationData.count) {
            //let value = (arc4random() % 90) + 10
            let height: Float = Float(arrOfMotivationData[j].strScore)! / 60.0
            
            result.append(BarEntry(color: ColorCodeConstant.graphBlueColor , height: height, textValue: "\(arrOfMotivationData[j].strScore /*valueArr[j]*/)", title: String(j + 1)/*titleArr[j]*/))
            
        }
        return result
    }
    
    @IBAction func btnRedoReviewAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "MotivationQuestionnarieView") as! MotivationQuestionnarieView
        objVC.comingFromReduReviewBtn = true
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTribeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - UITableViewDelegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrOfMotivationData.count == 0{
            return 0
        }
        else{
        return arrOfMotivationData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ListOfMotivationParameterCell = tableView.dequeueReusableCell(withIdentifier: "ListOfMotivationParameterCell") as! ListOfMotivationParameterCell
        
        cell.lblMarker.text = String(indexPath.row + 1) + " -"
        cell.lblTitle.text = arrOfMotivationData[indexPath.row].strTitle.capitalized
        cell.lblScore.text = arrOfMotivationData[indexPath.row].strScore.capitalized + "%"
        cell.lblScore.backgroundColor = arrOfMotivationData[indexPath.row].bgColor
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
 func setDataInColoumns(){

        if arrOfMotivationData.count > 0 {

            self.lblNoData.isHidden = true

            var arrScores = [String]()
            
            for value in arrOfMotivationData {
                arrScores.append(value.strScore)
            }
            
            var tempValues = arrScores
            //Max to min color handling
            for (mainIndex,_) in arrScores.enumerated()
            {
                var strMaxValue = getMaxValue(arr: tempValues)// tempValues[0]
//                var strMaxIndex = 0

//                for (index,value) in tempValues.enumerated()
                for value in tempValues
                {
                    if value == ""
                    {
                        continue
                    }
                    if Float(value)! >= Float(strMaxValue)! {
                        strMaxValue = value
//                        strMaxIndex = index
                    }
                }
                if strMaxValue == "" {
                    break
                }

                let arrAllIndexWithSameValue = getallIndexWithValue(strValue: strMaxValue, arrTemp: tempValues)

                for value in arrAllIndexWithSameValue {

                    tempValues[Int(value)!] = ""

                    self.arrOfMotivationData[Int(value)!].bgColor = getBGColorWithPosition(position: mainIndex)
                }
            }
            self.tblView.isHidden = false
        }

        else{

            self.lblNoData.isHidden = false
            self.lblNoData.text = ""
            self.tblList.reloadData()
            self.arrOfMotivationData.removeAll()
            self.lblNoData.isHidden = false
            self.MainView.isHidden = true
        }
    }
 
    func getallIndexWithValue(strValue: String, arrTemp:[String]) -> [String] {
        
        var arrIndex = [String]()
        
        for (index,value) in arrTemp.enumerated() {
            if value == strValue {
                arrIndex.append(String(index))
            }
        }
        
        return arrIndex
    }
    
    func getMaxValue(arr: [String]) -> String {
        
        for value in arr {
            if value != "" {
                return value
            }
        }
        
        return ""
    }
   
    
    func getBGColorWithPosition(position: Int) -> UIColor{
        
        switch position {
        case 0:
            return ColorCodeConstant.barValueListGrayColor
        case 1:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.9)
        case 2:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.8)
        case 3:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.7)
        case 4:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.6)
        case 5:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.5)
        case 6:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.4)
        case 7:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.3)
        case 8:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.2)
        case 9:
            return ColorCodeConstant.barValueListGrayColor.withAlphaComponent(0.15)
        default:
            return ColorCodeConstant.barValueListGrayColor
        }
        
    }
    
    func callWebServiceForMotivatioStructureDetail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        /*{
         "userId":"183"
         }
         */
        let param = ["userId": strUserID,
                     "orgId": strOrgID] as [String : Any]
        
        print("+++++: ",param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.MotivationStructure.getSOTMotivationUserList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    SOTParser.parseSOTMotivationStructureDetail(response: response!, completionHandler: { (arrOfMotivationStructure) in
                        
                        self.arrOfMotivationData = arrOfMotivationStructure
                        
                    })
                    
                    if self.arrOfMotivationData.count == 0 {
                        
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = "This user has not completed Questionnaire yet."
                        self.MainView.isHidden = true
                        self.tblView.isHidden = true
                        
                    }
                    else{
                        
                        self.lblNoData.isHidden = true
                        //self.lblNoData.text = "This user has not completed Questionnaire yet."
                        self.MainView.isHidden = false
                        self.tblView.isHidden = false
                        self.tblList.reloadData()
                    let dataEntries = self.generateDataEntries()
                    self.basicBarChart.dataEntries = dataEntries
                    }
                self.setDataInColoumns()
                }
                    
                else {
                    self.lblNoData.isHidden = false
                    self.lblNoData.text = "This user has not completed Questionnaire yet."
                    self.MainView.isHidden = true
                    self.tblView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func callWebServiceForMotivatioStructureReport() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        var param = [String : Any]()
        
        if selectedIndexOfOffice == 0{
            
            param = [ "orgId": strOrgID,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId": selectedIndexOfDepartment != 0 ? arrOfAllDepartmentIdList[selectedIndexOfDepartment] : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
            
        }
        else{
            param = [ "orgId": strOrgID,
                      "officeId": selectedIndexOfOffice != 0 ? arrOffices[selectedIndexOfOffice - 1].strOffice_id : "",
                      "departmentId":selectedIndexOfDepartment != 0 ? arrDepartment[selectedIndexOfDepartment - 1].strId : "",
                      "startDate": selectedFromDate,
                      "endDate": selectedToDate]
        }
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AdminReports.getSOTmotivationReport, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    SOTParser.parseSOTMotivationStructureDetail(response: response!, completionHandler: { (arrOfMotivationStructure) in
                        
                        self.arrOfMotivationData = arrOfMotivationStructure
                        
                    })
                    
                    if self.arrOfMotivationData.count == 0 {
                        
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = ""
                        self.tblList.reloadData()
                        self.arrOfMotivationData.removeAll()
                        self.lblNoData.isHidden = false
                        self.MainView.isHidden = true
                        
                    }
                    else{
                        
                        self.lblNoData.isHidden = true
                        self.MainView.isHidden = false
                        self.tblView.isHidden = false
                        self.tblList.reloadData()
                        let dataEntries = self.generateDataEntries()
                        self.basicBarChart.dataEntries = dataEntries
                    }
                    self.setDataInColoumns()
                }
                    
                else {
                    self.lblNoData.isHidden = false
                    self.lblNoData.text = ""
                    self.tblList.reloadData()
                    self.arrOfMotivationData.removeAll()
                    self.lblNoData.isHidden = false
                    self.MainView.isHidden = true
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    func CreateDropDownListOfAllDepartemntArray(){
        
        for i in (0..<self.arrForDepartmentName.count + 1) {
            if i == 0{
                self.arrOfAllDeparmentList.insert("ALL DEPARTMENTS", at: 0)
                self.arrOfAllDepartmentIdList.insert("", at: 0)
                
            }
            else{
                self.arrOfAllDeparmentList.insert(self.arrForDepartmentName[i - 1].strDepartment.uppercased(), at: i)
                self.arrOfAllDepartmentIdList.insert(self.arrForDepartmentName[i - 1].strId, at: i)
                
            }
        }
        print(arrOfAllDeparmentList)
        print(arrOfAllDepartmentIdList)
        // self.dropDownDept.items = self.arrDepartmentNames
    }
    
    func setData() {
        dropDownOffice.items = ["ALL OFFICES"] + arrOfficesName
        
        if dropDownOffice.title == "ALL OFFICES"
        {
            dropDownDept.items = arrOfAllDeparmentList
        }
        else{
            dropDownDept.items = ["ALL DEPARTMENTS"] + arrDepartmentNames
        }
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtFromDate{
            selectedFromDate = txtFromDate.text!
            if selectedToDate != "" {
                callWebServiceForMotivatioStructureReport()
            }
        }
        else {
            
            if selectedFromDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateFrom = dateFormatter.date(from: txtFromDate.text!)
                
                let dateFormatterTo = DateFormatter()
                dateFormatterTo.dateFormat = "dd-MM-yyyy"
                let dateTo = dateFormatterTo.date(from: txtToDate.text!)
                
                if dateFrom! > dateTo! {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "To Date should be greater than From Date")
                    txtToDate.text = ""
                }
                else {
                    selectedToDate = txtToDate.text!
                    if selectedFromDate != "" {
                        callWebServiceForMotivatioStructureReport()
                    }
                }
            }
            
            
        }
    }
    
    //MARK: - WebService Methods
    
    func callWebServiceToGetAllOfficesAndDepartmentList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        let param = ["orgId": strOrgID] as [String : Any]
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.COT.officenDepartments, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS",response ?? "")
                    
                    COTParser.parseOfficeAndDept(response: response!, completionHandler: { (arrOffice, arrDept, arrOfcNames, arrDeptNames) in
                        self.arrOffices = arrOffice
                        self.arrDepartment = arrDept
                        self.arrAllDepartment = arrDept
                        self.arrOfficesName = arrOfcNames
                        self.arrDepartmentNames = arrDeptNames
                        self.arrAllDepartmentNames = arrDeptNames
                        self.setData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetAllDepartments() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Department.departmentList, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    DashboardParser.parseDepartmentList(response: response!, completionHandler: { (arrDept) in
                        self.arrForDepartmentName = arrDept
                    })
                    
                    if self.loadForFirst == false{
                        self.loadForFirst = true
                        self.CreateDropDownListOfAllDepartemntArray()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    //MARK: - HADropDown Delegate Method
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        //selectedIndexOfOffice = 0
        selectedIndexOfDepartment = 0
        print("Item selected at index \(index)")
        
        if dropDown == dropDownOffice {
            selectedIndexOfDepartment = 0
            selectedIndexOfOffice = 0
            dropDownDept.title = "ALL DEPARTMENTS"
            selectedIndexOfOffice = index
            if index != 0 {
                arrDepartment = arrOffices[index - 1].arrDepartment
                arrDepartmentNames = arrOffices[index - 1].arrDepartmentNames
            }
            else {
                
                if dropDownOffice.title == "ALL OFFICES"
                {
                    dropDownDept.items = arrOfAllDeparmentList
                }
                
            }
            setData()
        }
        else if dropDown == dropDownDept {
            selectedIndexOfDepartment = index
        }
      callWebServiceForMotivatioStructureReport()
        
    }
    
}
