//
//  MotivationOptionCell.swift
//  Tribe365
//
//  Created by Apple on 11/01/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class MotivationOptionCell: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    
    @IBOutlet weak var lblhoriLine: UILabel!
    @IBOutlet weak var btnZero: UIButton!

    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    @IBOutlet weak var btnThree: UIButton!
    @IBOutlet weak var btnFour: UIButton!
    @IBOutlet weak var btnFive: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func selectedButtonColor(selectedBtn: UIButton) {
        selectedBtn.setTitleColor(UIColor.white, for: .normal)
        selectedBtn.backgroundColor = ColorCodeConstant.selectedOptionColor
    }
    
    func deselectedButtonColor(deselectedBtn: UIButton) {
        deselectedBtn.setTitleColor(ColorCodeConstant.darkTextcolor, for: .normal)
        deselectedBtn.backgroundColor = UIColor.clear
    }
    
    func changeButtonColorOfIndex(index: String) {
        
        switch index {
        case "0":
            selectedButtonColor(selectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnFive)
        case "1":
            selectedButtonColor(selectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnFive)
        case "2":
            selectedButtonColor(selectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnFive)
        case "3":
            selectedButtonColor(selectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnFive)
        case "4":
            selectedButtonColor(selectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnFive)
        case "5":
            selectedButtonColor(selectedBtn: btnFive)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnZero)
        default:
            deselectedButtonColor(deselectedBtn: btnZero)
            deselectedButtonColor(deselectedBtn: btnOne)
            deselectedButtonColor(deselectedBtn: btnTwo)
            deselectedButtonColor(deselectedBtn: btnThree)
            deselectedButtonColor(deselectedBtn: btnFour)
            deselectedButtonColor(deselectedBtn: btnFive)
        }
    }

}
