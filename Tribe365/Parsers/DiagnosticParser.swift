//
//  DiagnosticParser.swift
//  Tribe365
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class is used to parse the api details for Diagnostics Module
 */
class DiagnosticParser: NSObject {
    
    /**
     * This function is used to parse diagnostics questions list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DiagonisticQuestionsModel
     */
    class func parseDiagonisticQuestionList(response : JSON, completionHandler: @escaping ([DiagonisticQuestionsModel]) -> Void) {
        
        
        var arrQue = [DiagonisticQuestionsModel]()
        
        for json in response["data"].arrayValue {
            
            let model = DiagonisticQuestionsModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            
            for subJson in json["options"].arrayValue {
                
                let optnModel = DiagonasticOptionsModel()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
     * This function is used to parse diagnostics answers list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DiagonisticQuestionsModel
     */
    class func parseDiagonisticGetAnsweredList(response : JSON, completionHandler: @escaping ([DiagonisticQuestionsModel]) -> Void) {
        
        
        var arrQue = [DiagonisticQuestionsModel]()
        
        for json in response["data"].arrayValue {
            
            let model = DiagonisticQuestionsModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            model.strAPIAnswerID = json["answerId"].stringValue
            
            for (index,subJson) in json["options"].arrayValue.enumerated() {
                
                let optnModel = DiagonasticOptionsModel()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                optnModel.isChecked = subJson["isChecked"].boolValue
                
                if optnModel.isChecked {
                    model.selectedIndex = index + 1
                    model.strAnswerId = subJson["optionId"].stringValue
                }
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
     * This function is used to parse tribeometer question list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: TriboemeterQuestionModel
     */
    class func parseTribeometerQuestionList(response : JSON, completionHandler: @escaping ([TriboemeterQuestionModel]) -> Void) {
        
        
        var arrQue = [TriboemeterQuestionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = TriboemeterQuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            model.strAPIAnswerID = json["answerId"].stringValue
            
            
            for subJson in json["options"].arrayValue {
                
                let optnModel = TribeometerOptionModel()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                optnModel.isChecked = subJson["isChecked"].boolValue
                
                
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
     * This function is used to parse tribeometer answer list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: TriboemeterQuestionModel
     */
    class func parseTribeometerGetAnsweredList(response : JSON, completionHandler: @escaping ([TriboemeterQuestionModel]) -> Void) {
        
        var arrQue = [TriboemeterQuestionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = TriboemeterQuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            model.strAPIAnswerID = json["answerId"].stringValue
            
            for (index,subJson) in json["options"].arrayValue.enumerated() {
                
                let optnModel = TribeometerOptionModel()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                optnModel.isChecked = subJson["isChecked"].boolValue
                
                if optnModel.isChecked {
                    model.selectedIndex = index + 1
                    model.strAnswerId = subJson["optionId"].stringValue
                }
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
     * This function is used to parse the details if diagnostics/tribeometer answers are filled by user or not
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of booleans
     */
    class func parseStatusOfDiagnosticTribeometerAnswerDone(response : JSON, completionHandler: @escaping (Bool, Bool) -> Void) {
        
        let isDiagnosticAnsDone = response["data"]["isDiagnosticAnsDone"].boolValue
        
        let isTribeometerAnsDone = response["data"]["isTribeometerAnsDone"].boolValue
        
        completionHandler(isDiagnosticAnsDone, isTribeometerAnsDone )
    }
    
    class func parseDiagnosticReport(response : JSON, completionHandler: @escaping ([DiagnosticResultModel]) -> Void) {
        
        var arrOfReports = [DiagnosticResultModel]()
        
        for json in response["data"].arrayValue {
            
            let model = DiagnosticResultModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            model.strPercentage = json["percentage"].stringValue
            model.strCategoryId = json["categoryId"].stringValue
            
            arrOfReports.append(model)
        }
        
        completionHandler(arrOfReports)
        
    }
    
    /**
     * This function is used to parse diagnostics result for know organisation screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DiagnosticResultModel
     */
    class func parseDiagnosticReportKnow(response : JSON, completionHandler: @escaping ([DiagnosticResultModel]) -> Void) {
        
        var arrOfReports = [DiagnosticResultModel]()
        
        for json in response.arrayValue {
            
            let model = DiagnosticResultModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            model.strPercentage = json["percentage"].stringValue
            model.strCategoryId = json["categoryId"].stringValue
            
            arrOfReports.append(model)
        }
        
        completionHandler(arrOfReports)
        
    }
    
    /**
     * This function is used to parse culture index result for know organisation screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DiagnosticResultModel
     */
    class func parseCultureIndex(response : JSON, completionHandler: @escaping ([DiagnosticResultModel], [String]) -> Void) {
        
        var arrOfReports = [DiagnosticResultModel]()
        var arrMonths = [String]()
        
        for json in response.arrayValue {
            
            let model = DiagnosticResultModel()
            
            model.strTitle = json["monthName"].stringValue
            model.strScore = json["data"].stringValue
            
            arrOfReports.append(model)
            arrMonths.append(model.strTitle)
        }
        
        var count = 0
        for value in arrOfReports {
            if value.strScore == "0" {
                count = count + 1
            }
        }
        
        if count == arrOfReports.count {
            arrOfReports.removeAll()
            arrMonths.removeAll()
        }
        
        completionHandler(arrOfReports, arrMonths)
        
    }
    
}
