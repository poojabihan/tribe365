//
//  SOTParser.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class is used to parse the api details for SOT Module
 */
class SOTParser: NSObject {
    
    /**
     * This function is used to parse question list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTQuestionModel
     */
    class func parseSOTQuestionsList(response : JSON, completionHandler: @escaping ([SOTQuestionModel]) -> Void) {
        
        var arrOfSotQuestions = [SOTQuestionModel]()
        
        for (index,json) in response["data"].arrayValue.enumerated() {
            
            let model = SOTQuestionModel()
            model.strQuestion = json["question"].stringValue //"QUESTION " + String(index + 1)
            // for jsonSub in json.arrayValue.count {
            for jsub in json["option"].arrayValue {
                
                let subModel = SOTOptionsModel()
                
                subModel.strId = jsub["id"].stringValue
                subModel.strType = jsub["type"].stringValue
                subModel.strSection = jsub["section"].stringValue
                subModel.strQuestion = jsub["question"].stringValue
                
                model.arrOptions.append(subModel)
            }
            
            arrOfSotQuestions.append(model)
        }
        completionHandler(arrOfSotQuestions)
    }
    
    /**
     * This function is used to parse answer list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTQuestionModel
     */
    class func parseSOTQuestionsAnwserList(response : JSON, completionHandler: @escaping ([SOTQuestionModel]) -> Void) {
        
        var arrOfSotQuestions = [SOTQuestionModel]()
        
        for (index,json) in response["data"].arrayValue.enumerated() {
            
            let model = SOTQuestionModel()
            model.strQuestion = json["question"].stringValue //"QUESTION " + String(index + 1)
            
            // for jsonSub in json.arrayValue.count {
            for (index,jsub) in json["option"].arrayValue.enumerated() {
                
                let subModel = SOTOptionsModel()
                
                subModel.strId = jsub["id"].stringValue
                subModel.strType = jsub["type"].stringValue
                subModel.strSection = jsub["section"].stringValue
                subModel.strQuestion = jsub["question"].stringValue
                subModel.strIsChecked = jsub["isChecked"].boolValue
                
                if subModel.strIsChecked {
                    model.selectedIndex = index + 1
                    model.strAnswerId = jsub["id"].stringValue
                }
                
                model.arrOptions.append(subModel)
            }
            
            arrOfSotQuestions.append(model)
        }
        completionHandler(arrOfSotQuestions)
    }
    
    /**
     * This function is used to parse sot details
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTDetailModel, SOTDetailResultModel and two booleans for is questions filled and user answered questions respectively
     */
    class func parseSOTDetails(response : JSON, completionHandler: @escaping ([SOTDetailModel],[SOTDetailResultModel],Bool, Bool) -> Void) {
        
        var arrOfSotDetails = [SOTDetailModel]()
        var arrOfSotResult = [SOTDetailResultModel]()
        
        let IsQuestionnaireAnswerFilled = response["data"]["IsQuestionnaireAnswerFilled"].boolValue
        let IsUserFilledAnswer = response["data"]["IsUserFilledAnswer"].boolValue
        
        for json in response["data"]["sotDetailArray"].arrayValue {
            
            let model = SOTDetailModel()
            
            model.strId = json["id"].stringValue
            model.strType = json["type"].stringValue
            model.strTitle = json["title"].stringValue
            model.strImageUrl = json["imgUrl"].stringValue
            model.strSOTCount = json["SOTCount"].stringValue
            
            arrOfSotDetails.append(model)
        }
        
        for json in response["data"]["sotSummaryDetailArray"].arrayValue {
            
            let model = SOTDetailResultModel()
            
            model.strId = json["id"].stringValue
            model.strType = json["type"].stringValue
            model.strTitle = json["title"].stringValue
            model.strImageUrl = json["imgUrl"].stringValue
            model.strSOTCount = json["SOTCount"].stringValue
            
            for arrOfSummary in json["summary"].arrayValue {
                
                let modelSub = SOTSummaryModel()
                
                modelSub.strSummary = arrOfSummary["summary"].stringValue
                
                model.arrOfSummary.append(modelSub)
            }
            
            arrOfSotResult.append(model)
        }
        
        completionHandler(arrOfSotDetails, arrOfSotResult, IsQuestionnaireAnswerFilled,IsUserFilledAnswer)
    }
    
    /**
     * This function is used to parse sot details for know organisation screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTDetailModel, SOTDetailResultModel and two booleans for is questions filled and user answered questions respectively
     */
    class func parseSOTDetailsKnow(response : JSON, completionHandler: @escaping ([SOTDetailModel],[SOTDetailResultModel],Bool, Bool) -> Void) {
        
        var arrOfSotDetails = [SOTDetailModel]()
        var arrOfSotResult = [SOTDetailResultModel]()
        
        let IsQuestionnaireAnswerFilled = response["IsQuestionnaireAnswerFilled"].boolValue
        let IsUserFilledAnswer = response["IsUserFilledAnswer"].boolValue
        
        for json in response["sotDetailArray"].arrayValue {
            
            let model = SOTDetailModel()
            
            model.strId = json["id"].stringValue
            model.strType = json["type"].stringValue
            model.strTitle = json["title"].stringValue
            model.strImageUrl = json["imgUrl"].stringValue
            model.strSOTCount = json["SOTCount"].stringValue
            
            arrOfSotDetails.append(model)
        }
        
        for json in response["sotSummaryDetailArray"].arrayValue {
            
            let model = SOTDetailResultModel()
            
            model.strId = json["id"].stringValue
            model.strType = json["type"].stringValue
            model.strTitle = json["title"].stringValue
            model.strImageUrl = json["imgUrl"].stringValue
            model.strSOTCount = json["SOTCount"].stringValue
            
            for arrOfSummary in json["summary"].arrayValue {
                
                let modelSub = SOTSummaryModel()
                
                modelSub.strSummary = arrOfSummary["summary"].stringValue
                
                model.arrOfSummary.append(modelSub)
            }
            
            arrOfSotResult.append(model)
        }
        
        completionHandler(arrOfSotDetails, arrOfSotResult, IsQuestionnaireAnswerFilled,IsUserFilledAnswer)
    }
    
    /**
     * This function is used to parse motivation question list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTMotivationModel
     */
    class func parseSOTMOtivationQuestionsParser(response : JSON, completionHandler: @escaping ([SOTMotivationModel]) -> Void) {
        
        var arrOfSotDetails = [SOTMotivationModel]()
        
        
        for json in response["data"].arrayValue {
            
            let model = SOTMotivationModel()
            
            model.strQuestionId =  json["questionId"].stringValue
            model.strQuestionName =  json["questionName"].stringValue
            
            for subJson in json["option"].arrayValue {
                
                let subModel = MotivationOptionsModel()
                
                subModel.strOptionId = subJson["OptionId"].stringValue
                subModel.strOption = subJson["option"].stringValue
                subModel.strCategoryId = subJson["categoryId"].stringValue
                
                model.arrOptions.append(subModel)
            }
            
            arrOfSotDetails.append(model)
        }
        
        completionHandler(arrOfSotDetails)
    }
    
    /**
     * This function is used to parse motivation answer list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SOTMotivationModel
     */
    class func parseSOTMOtivationQuestionsAnswerParser(response : JSON, completionHandler: @escaping ([SOTMotivationModel]) -> Void) {
        
        var arrOfSotDetails = [SOTMotivationModel]()
        
        
        for json in response["data"].arrayValue {
            
            let model = SOTMotivationModel()
            
            model.strQuestionId =  json["questionId"].stringValue
            model.strQuestionName =  json["questionName"].stringValue
            
            for (index,subJson) in json["option"].arrayValue.enumerated() {
                
                let subModel = MotivationOptionsModel()
                
                subModel.strOptionId = subJson["OptionId"].stringValue
                subModel.strOption = subJson["option"].stringValue
                //subModel.strAnswerId = subJson["answerId"].stringValue
                
                subModel.selectedIndex = index + 1
                if index == 0{
                    subModel.strSelectedOption1 = subJson["points"].stringValue
                    subModel.strAnswerId = subJson["answerId"].stringValue
                    
                }
                else{
                    subModel.strSelectedOption2 = subJson["points"].stringValue
                    subModel.strAnswerId = subJson["answerId"].stringValue
                }
                model.arrOptions.append(subModel)
            }
            
            arrOfSotDetails.append(model)
        }
        
        completionHandler(arrOfSotDetails)
    }
    
    /**
     * This function is used to parse motivation result details
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: MotivationStructureGraphModel
     */
    class func parseSOTMotivationStructureDetail(response : JSON, completionHandler: @escaping ([MotivationStructureGraphModel]) -> Void) {
        
        
        var arrOfMotivationData = [MotivationStructureGraphModel]()
        
        for json in response["data"].arrayValue {
            
            let model = MotivationStructureGraphModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            
            arrOfMotivationData.append(model)
        }
        
        completionHandler(arrOfMotivationData)
    }
    
    /**
     * This function is used to parse motivation result details for know members screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: MotivationStructureGraphModel
     */
    class func parseSOTMotivationStructureDetailKnow(response : JSON, completionHandler: @escaping ([MotivationStructureGraphModel]) -> Void) {
        
        
        var arrOfMotivationData = [MotivationStructureGraphModel]()
        
        for json in response.arrayValue {
            
            let model = MotivationStructureGraphModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            
            arrOfMotivationData.append(model)
        }
        
        completionHandler(arrOfMotivationData)
    }
    
}
//        if response["data"].arrayValue.count != 0 {
//            for json in response["data"].arrayValue[0]["sotMotivationValues"].arrayValue {
//
//                //for subjson  in json.arrayValue {
//                let model = MotivationStructureGraphModel()
//
//                model.strMotId = json["motId"].stringValue
//                model.strMoneyScore = json["moneyScore"].stringValue
//                model.strStressAvoidScore = json["stressAvoidScore"].stringValue
//                model.strRiskAvoidanceScore = json["riskAvoidanceScore"].stringValue
//                model.strJobStructureScore = json["jobStructureScore"].stringValue
//                model.strAvoidWorkingAloneScore = json["avoidWorkingAloneScore"].stringValue
//                model.strIdentifyWithTeamScore = json["identifyWithTeamScore"].stringValue
//                model.strRecognitionScore = json["recognitionScore"].stringValue
//                model.strPowerScore = json["powerScore"].stringValue
//                model.strAutonomyVarietyScore = json["autonomyVarietyScore"].stringValue
//                model.strPersonalGrowthScore = json["personalGrowthScore"].stringValue
//
//                preModel.arrOfSotMotivationValues.append(model)
//            }
//        }


