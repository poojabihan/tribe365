//
//  COTParser.swift
//  Tribe365
//
//  Created by kdstudio on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
* This class is used to parse the api details for Connecting(COT) Module
*/
class COTParser: NSObject {

    /**
    * This function is used to parse COT Answer list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: QuestionModel
    */
    class func parseAnswersList(response : JSON, completionHandler: @escaping ([QuestionModel]) -> Void) {
        
        var arrQue = [QuestionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = QuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            
            //Parse option details
            for subJson in json["options"].arrayValue {
                
                let optnModel = OptionModel()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOption = subJson["optionName"].stringValue
                optnModel.strRoleMapId = subJson["roleMapId"].stringValue
                optnModel.strAnswer = subJson["point"].stringValue
                optnModel.strAnswerID = subJson["answerId"].stringValue
                
                model.arrOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
    * This function is used to parse Question list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: QuestionModel
    */
    class func parseQuestionsList(response : JSON, completionHandler: @escaping ([QuestionModel]) -> Void) {
        
        var arrOfSavedAnswers = [[String : Any]]()
        
        if (UserDefaults.standard.value(forKey: "isSavedTeamRoleMap") != nil) {
            arrOfSavedAnswers = UserDefaults.standard.value(forKey: "SavedDictForTeamRoleMap") as! [[String : Any]]
        }
        
        var arrQue = [QuestionModel]()
        
        for (indexQues, json) in response["data"].arrayValue.enumerated() {
            
            let model = QuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["questionName"].stringValue
            
            //Parse option details
            for (indexOption, subJson) in json["option"].arrayValue.enumerated() {
                
                let optnModel = OptionModel()
                
                optnModel.strOptionId = subJson["OptionId"].stringValue
                optnModel.strOption = subJson["option"].stringValue
                optnModel.strRoleMapId = subJson["roleMapId"].stringValue
                
                if (UserDefaults.standard.value(forKey: "isSavedTeamRoleMap") != nil && indexQues <= arrOfSavedAnswers.count - 1) {
                    
                    let arrOption:[[String:String]] = arrOfSavedAnswers[indexQues]["option"] as! [[String : String]]
                    
                    optnModel.strAnswer = arrOption[indexOption]["point"]!
                    
                }
                
                model.arrOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
    
    /**
    * This function is used to parse Category description
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: CategoryDescriptionModel
    */
    class func parseCategoryDescription(response : JSON, completionHandler: @escaping ([CategoryDescriptionModel]) -> Void) {
        
        var arrCatDes = [CategoryDescriptionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CategoryDescriptionModel()
            
            model.strTitle = json["title"].stringValue
            model.strShortDescription = json["short_description"].stringValue
            model.strLongDescription = json["long_description"].stringValue
            
            arrCatDes.append(model)
        }
        
        completionHandler(arrCatDes)
    }
    
    /**
    * This function is used to parse Team role summary
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: SummaryModel and string which return total page count
    */
    class func parseTeamSummaryList(response : JSON, completionHandler: @escaping ([SummaryModel], String) -> Void) {
        
        var arrSummary = [SummaryModel]()
        
        for json in response["data"].arrayValue {
            
            let model = SummaryModel()
            
            model.strCoordinator = json["coordinator"].stringValue
            model.strImplementer = json["implementer"].stringValue
            model.strMonitorEvaluator = json["monitorEvaluator"].stringValue
            model.strResourceInvestigator = json["resourceInvestigator"].stringValue
            model.strShaper = json["shaper"].stringValue
            model.strCompleterFinisher = json["completerFinisher"].stringValue
            model.strTeamworker = json["teamworker"].stringValue
            model.strPlant = json["plant"].stringValue
            model.strUserName = json["userName"].stringValue
            model.strUserId = json["userId"].stringValue
            
            model.strCoordinatorScore = json["totalKeyCount"]["coordinator"].stringValue
            model.strImplementerScore = json["totalKeyCount"]["implementer"].stringValue
            model.strMonitorEvaluatorScore = json["totalKeyCount"]["monitorEvaluator"].stringValue
            model.strResourceInvestigatorScore = json["totalKeyCount"]["resourceInvestigator"].stringValue
            model.strShaperScore = json["totalKeyCount"]["shaper"].stringValue
            model.strCompleterFinisherScore = json["totalKeyCount"]["completerFinisher"].stringValue
            model.strTeamworkerScore = json["totalKeyCount"]["teamworker"].stringValue
            model.strPlantScore = json["totalKeyCount"]["plant"].stringValue

            model.strCoordinatorValue = json["mapersArray"]["coordinator"].stringValue
            model.strImplementerValue = json["mapersArray"]["implementer"].stringValue
            model.strMonitorEvaluatorValue = json["mapersArray"]["monitorEvaluator"].stringValue
            model.strResourceInvestigatorValue = json["mapersArray"]["resourceInvestigator"].stringValue
            model.strShaperValue = json["mapersArray"]["shaper"].stringValue
            model.strCompleterFinisherValue = json["mapersArray"]["completerFinisher"].stringValue
            model.strTeamworkerValue = json["mapersArray"]["teamworker"].stringValue
            model.strPlantValue = json["mapersArray"]["plant"].stringValue

            arrSummary.append(model)
        }
        
        completionHandler(arrSummary, response["totalPageCount"].stringValue)
    }
    
    /**
    * This function is used to parse office an department list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: OfficeModel, DepartmentModel and string arrays which returns officenames and department names array
    */
    class func parseOfficeAndDept(response : JSON, completionHandler: @escaping ([OfficeModel], [DepartmentModel], [String], [String]) -> Void) {
        
        var arrOffices = [OfficeModel]()
        var arrDept = [DepartmentModel]()
        var arrOfficesName = [String]()
        var arrDeptNames = [String]()

        //parse office details
        for json in response["data"]["offices"].arrayValue {
            
            let model = OfficeModel()
            
            model.strOffice_id = json["officeId"].stringValue
            model.strOffice = json["office"].stringValue.uppercased()
            
            //parse department details of specific office
            for value in json["department"].arrayValue {
                
                let modelDept = DepartmentModel()
                modelDept.strId = value["id"].stringValue
                modelDept.strDepartment = value["department"].stringValue.uppercased()
                modelDept.strOfficeId = value["officeId"].stringValue
                
                model.arrDepartment.append(modelDept)
                model.arrDepartmentNames.append(value["department"].stringValue.uppercased())
            }
            
            arrOffices.append(model)
            arrOfficesName.append(json["office"].stringValue.uppercased())
        }
        
        //parse all departments
        for json in response["data"]["department"].arrayValue {
            
            let model = DepartmentModel()
            model.strId = json["id"].stringValue
            model.strDepartment = json["department"].stringValue.uppercased()
            
            arrDept.append(model)
            arrDeptNames.append(json["department"].stringValue.uppercased())

        }
                    
        completionHandler(arrOffices, arrDept, arrOfficesName, arrDeptNames)
    }
    
    /**
    * This function is used to parse connecting: personality type
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FuncLensKeyDetailModel, InitialValueListModel, TribeTipsArray, AllCombinationModel and a boolean which tells if answers  are filled or not
    */
    class func parseCOTFunctionalLensDetail(response : JSON, completionHandler: @escaping ([FuncLensKeyDetailModel], [InitialValueListModel], [TribeTipsArray],[AllCombinationModel], Bool) -> Void) {
        
        var arrFunctionalLens = [FuncLensKeyDetailModel]()
        var arrIntialValuesList = [InitialValueListModel]()
        var arrTribeTips = [TribeTipsArray]()
        var arrAllCombination = [AllCombinationModel]()
        
        for json in response["data"]["funcLensKeyDetail"].arrayValue {
            
            let model = FuncLensKeyDetailModel()
            
            model.strValue = json["value"].stringValue
            model.strTitle = json["title"].stringValue
            model.strdescription = json["description"].stringValue
        
            arrFunctionalLens.append(model)
        }
        
        for json in response["data"]["initialValueList"].arrayValue {
            
            let model = InitialValueListModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            model.strValue = json["value"].stringValue
            model.strAllowableWeaknesses = json["allowableWeaknesses"].stringValue
            model.strPositives = json["positives"].stringValue
            
            arrIntialValuesList.append(model)
            
        }
        
        for json in response["data"]["valueCombination"].arrayValue {
            
            let model = AllCombinationModel()
            
            model.strTitle = json["title"].stringValue
            model.strSummary = json["summary"].stringValue
            model.strValue = json["value"].stringValue
           
            arrAllCombination.append(model)
            
        }
        
        for json in response["data"]["tribeTipsArray"].arrayValue {
            
            let model = TribeTipsArray()
            
            model.strTitle = json["title"].stringValue
            model.strValue = json["value"].stringValue
            model.strSummary = json["summary"].stringValue

            for Persuadevalue in json["PersuadeArray"].arrayValue {
                
                let modelSub = PersuadeValues()
                
            modelSub.strValue = Persuadevalue["value"].stringValue
            modelSub.strValueType = Persuadevalue["valueType"].stringValue
                
                model.arrPersuadeValues.append(modelSub)
            }
            
            for Seeksvalue in json["seekArray"].arrayValue {
                
                let modelSub = SeeksValues()
                
                modelSub.strValue = Seeksvalue["value"].stringValue
                modelSub.strValueType = Seeksvalue["valueType"].stringValue
                
                model.arrSeekValues.append(modelSub)
            }
            arrTribeTips.append(model)

        }
       
       let isCOTfunclensAnswersDone = response["data"]["isCOTfunclensAnswersDone"].boolValue

        completionHandler(arrFunctionalLens, arrIntialValuesList, arrTribeTips, arrAllCombination, isCOTfunclensAnswersDone)
    }
    
    /**
    * This function is used to parse connecting: personality type for Know Organisation module
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FuncLensKeyDetailModel, InitialValueListModel, TribeTipsArray, AllCombinationModel and a boolean which tells if answers  are filled or not
    */
    class func parseCOTFunctionalLensDetailKnow(response : JSON, completionHandler: @escaping ([FuncLensKeyDetailModel], [InitialValueListModel], [TribeTipsArray],[AllCombinationModel], Bool) -> Void) {
        
        var arrFunctionalLens = [FuncLensKeyDetailModel]()
        var arrIntialValuesList = [InitialValueListModel]()
        var arrTribeTips = [TribeTipsArray]()
        var arrAllCombination = [AllCombinationModel]()
        
        
        for json in response["funcLensKeyDetail"].arrayValue {
            
            let model = FuncLensKeyDetailModel()
            
            model.strValue = json["value"].stringValue
            model.strTitle = json["title"].stringValue
            model.strdescription = json["description"].stringValue
            
            arrFunctionalLens.append(model)
        }
        
        for json in response["initialValueList"].arrayValue {
            
            let model = InitialValueListModel()
            
            model.strTitle = json["title"].stringValue
            model.strScore = json["score"].stringValue
            model.strValue = json["value"].stringValue
            model.strAllowableWeaknesses = json["allowableWeaknesses"].stringValue
            model.strPositives = json["positives"].stringValue
            
            arrIntialValuesList.append(model)
            
        }
        
        for json in response["valueCombination"].arrayValue {
            
            let model = AllCombinationModel()
            
            model.strTitle = json["title"].stringValue
            model.strSummary = json["summary"].stringValue
            model.strValue = json["value"].stringValue
            
            arrAllCombination.append(model)
            
        }
        
        for json in response["tribeTipsArray"].arrayValue {
            
            let model = TribeTipsArray()
            
            model.strTitle = json["title"].stringValue
            model.strValue = json["value"].stringValue
            model.strSummary = json["summary"].stringValue
            
            for Persuadevalue in json["PersuadeArray"].arrayValue {
                
                let modelSub = PersuadeValues()
                
                modelSub.strValue = Persuadevalue["value"].stringValue
                modelSub.strValueType = Persuadevalue["valueType"].stringValue
                
                model.arrPersuadeValues.append(modelSub)
            }
            
            for Seeksvalue in json["seekArray"].arrayValue {
                
                let modelSub = SeeksValues()
                
                modelSub.strValue = Seeksvalue["value"].stringValue
                modelSub.strValueType = Seeksvalue["valueType"].stringValue
                
                model.arrSeekValues.append(modelSub)
            }
            arrTribeTips.append(model)
            
        }
        
        let isCOTfunclensAnswersDone = response["isCOTfunclensAnswersDone"].boolValue
        
        completionHandler(arrFunctionalLens, arrIntialValuesList, arrTribeTips, arrAllCombination, isCOTfunclensAnswersDone)
    }
    
    /**
    * This function is used to parse connecting: personality type  Questions
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FunctionLenseQuestionModel
    */
    class func parseCOTFunctionalLenseMCQAnswerList(response : JSON, completionHandler: @escaping ([FunctionLenseQuestionModel]) -> Void) {
        
        
        var arrQue = [FunctionLenseQuestionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = FunctionLenseQuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["question"].stringValue
            model.strAPIAnswerID = json["answerId"].stringValue

            
            for (index,subJson) in json["options"].arrayValue.enumerated() {
                
                let optnModel = FunctionalLenseMCQOptions()
                
                optnModel.strOptionId = subJson["optionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                optnModel.isChecked = subJson["isChecked"].boolValue
                
                if optnModel.isChecked {
                    model.selectedIndex = index + 1
                    model.strAnswerId = subJson["optionId"].stringValue
                }
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }

    /**
       * This function is used to parse connecting: personality type  Questions
       
       * @param JSON response:  Accepts response to parse
       * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FunctionLenseQuestionModel
       */
    class func parseCOTFunctionalLenseMCQList(response : JSON, completionHandler: @escaping ([FunctionLenseQuestionModel]) -> Void) {
        
        
        var arrQue = [FunctionLenseQuestionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = FunctionLenseQuestionModel()
            
            model.strQuestionId = json["questionId"].stringValue
            model.strQuestionName = json["questionName"].stringValue
            
            for subJson in json["options"].arrayValue {
                
                let optnModel = FunctionalLenseMCQOptions()
                
                optnModel.strOptionId = subJson["OptionId"].stringValue
                optnModel.strOptionName = subJson["optionName"].stringValue
                optnModel.strValueName = subJson["valueName"].stringValue
                optnModel.strInitialValue = subJson["initialValue"].stringValue
                
                model.arrOfOptions.append(optnModel)
            }
            
            arrQue.append(model)
        }
        
        completionHandler(arrQue)
    }
}
