//
//  IOTParser.swift
//  Tribe365
//
//  Created by kdstudio on 17/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class is used to parse the api details for IOT Module
 */
class IOTParser: NSObject {
    
    /**
     * This function is used to parse Feedback history
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: HistoryModel
     */
    
    class func parseIOTHistoryList(response : JSON, completionHandler: @escaping ([HistoryModel]) -> Void) {
        
        var arrOfHistory = [HistoryModel]()
        
        for json in response["response"].arrayValue {
            
            let model = HistoryModel()
            
            model.strChangeit_id = json["changeit_id"].stringValue
            model.strCreated_date = json["created_date"].stringValue
            model.strImages = json["images"].stringValue
            model.strLocation_detail = json["location_detail"].stringValue
            model.strMsg_detail = json["msg_detail"].stringValue
            model.strMsg_type = json["msg_type"].stringValue
            
            //parse chat list items
            for subJson in json["itemchat"].arrayValue {
                
                let itemChatModel = HistoryItemChatModel()
                
                itemChatModel.strCreated_date = subJson["created_date"].stringValue
                itemChatModel.strCr_id = subJson["cr_id"].stringValue
                itemChatModel.strMessage = subJson["message"].stringValue
                itemChatModel.strPost_type = subJson["post_type"].stringValue
                itemChatModel.strUser_type = subJson["user_type"].stringValue
                
                //                model.arrItemChat.append(itemChatModel)
            }
            
            //parse last message (this is displayed on the chat list as sub heading)
            for subJson in json["lastmsg"].arrayValue {
                
                let LastMsgModel = HistoryLastMessageModel()
                
                LastMsgModel.strCreated_date = subJson["created_date"].stringValue
                LastMsgModel.strCr_id = subJson["cr_id"].stringValue
                LastMsgModel.strMessage = subJson["message"].stringValue
                LastMsgModel.strPost_type = subJson["post_type"].stringValue
                LastMsgModel.strUser_type = subJson["user_type"].stringValue
                
                model.arrLastMessage.append(LastMsgModel)
            }
            
            arrOfHistory.append(model)
        }
        
        completionHandler(arrOfHistory)
    }
    
    /**
     * This function is used to parse messages list (users list)
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: MessageModel
     */
    class func parseIOTMessageList(response : JSON, completionHandler: @escaping ([MessageModel]) -> Void) {
        
        var arrOfMessage = [MessageModel]()
        
        for json in response["response"].arrayValue {
            
            let model = MessageModel()
            
            model.strChangeit_id = json["changeit_id"].stringValue
            model.strCreated_date = json["created_date"].stringValue
            model.strImages = json["images"].stringValue
            model.strLocation_detail = json["location_detail"].stringValue
            model.strMsg_detail = json["msg_detail"].stringValue
            model.strMsg_type = json["msg_type"].stringValue
            
            //parse the chat message details
            for subJson in json["itemchat"].arrayValue {
                
                let itemChatModel = HistoryItemChatModel()
                
                itemChatModel.strCreated_date = subJson["created_date"].stringValue
                itemChatModel.strCr_id = subJson["cr_id"].stringValue
                itemChatModel.strMessage = subJson["message"].stringValue
                itemChatModel.strPost_type = subJson["post_type"].stringValue
                itemChatModel.strUser_type = subJson["user_type"].stringValue
                
                model.arrItemChat.append(itemChatModel)
            }
            
            //parse the latest message details in order to show as sub heading
            for subJson in json["lastmsg"].arrayValue {
                
                let LastMsgModel = HistoryLastMessageModel()
                
                LastMsgModel.strCreated_date = subJson["created_date"].stringValue
                LastMsgModel.strCr_id = subJson["cr_id"].stringValue
                LastMsgModel.strMessage = subJson["message"].stringValue
                LastMsgModel.strPost_type = subJson["post_type"].stringValue
                LastMsgModel.strUser_type = subJson["user_type"].stringValue
                
                model.arrLastMessage.append(LastMsgModel)
            }
            arrOfMessage.append(model)
        }
        
        completionHandler(arrOfMessage)
    }
    
    /**
     * This function is used to parse message chat list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ChatModel
     */
    class func parseChatList(response : JSON, completionHandler: @escaping ([ChatModel]) -> Void) {
        
        var arr = [ChatModel]()
        
        for json in response["response"].arrayValue {
            
            let chatModel = ChatModel()
            
            chatModel.strCreated_date = json["created_date"].stringValue
            chatModel.strCr_id = json["cr_id"].stringValue
            chatModel.strMessage = json["message"].stringValue
            chatModel.strPost_type = json["post_type"].stringValue
            chatModel.strUser_type = json["user_type"].stringValue
            
            
            arr.append(chatModel)
        }
        
        completionHandler(arr)
    }
    
    /**
     * This function is used to parse chat list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ChatModel
     */
    class func parseChat(response : JSON, completionHandler: @escaping ([ChatModel]) -> Void) {
        
        var arr = [ChatModel]()
        
        for json in response["data"]["messages"].arrayValue {
            
            let chatModel = ChatModel()
            
            chatModel.strCr_id = json["id"].stringValue
            chatModel.strSendTo = json["sendTo"].stringValue
            chatModel.strSendFrom = json["sendFrom"].stringValue
            chatModel.strName = json["name"].stringValue
            chatModel.strMessage = json["message"].stringValue
            chatModel.strCreated_date = json["created_at"].stringValue
            
            if (json["msgImageUrl"].stringValue == "") {
                chatModel.isImage = false
            }
            else {
                chatModel.isImage = true
            }
            
            chatModel.strImgUrl = json["msgImageUrl"].stringValue
            chatModel.strUser_type = json["userType"].stringValue
            
            //            chatModel.strPost_type = json["post_type"].stringValue
            
            arr.append(chatModel)
        }
        
        completionHandler(arr)
    }
    
    /**
     * This function is used to parse inbox list
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: MessageModel
     */
    class func parseInboxData(response : JSON, completionHandler: @escaping ([MessageModel]) -> Void) {
        
        var arr = [MessageModel]()
        
        for value in response["data"]["inbox"].arrayValue {
            let model = MessageModel()
            
            model.strID = value["id"].stringValue
            model.strMsg_detail = value["feedback_msg"].stringValue
            model.isImage = value["image"].boolValue
            model.strCreated_date = value["date"].stringValue
            model.strMessage = value["message"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
     * This function is used to parse history details
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: HistoryModel
     */
    class func parseIOTHistory(response : JSON, completionHandler: @escaping ([HistoryModel]) -> Void) {
        
        var arrOfHistory = [HistoryModel]()
        
        for json in response["data"].arrayValue {
            
            let model = HistoryModel()
            
            model.strChangeit_id = json["id"].stringValue
            model.strMsg_detail = json["message"].stringValue
            model.strImages = json["image"].stringValue
            model.strCreated_date = json["createdAt"].stringValue
            //            model.strLocation_detail = json["location_detail"].stringValue
            //            model.strMsg_type = json["msg_type"].stringValue
            
            for subJson in json["messages"].arrayValue {
                
                let chatModel = ChatModel()
                
                chatModel.strCr_id = subJson["id"].stringValue
                chatModel.strSendTo = subJson["sendTo"].stringValue
                chatModel.strSendFrom = subJson["sendFrom"].stringValue
                chatModel.strName = subJson["name"].stringValue
                chatModel.strMessage = subJson["message"].stringValue
                chatModel.strCreated_date = subJson["created_at"].stringValue
                
                if (subJson["msgImageUrl"].stringValue == "") {
                    chatModel.isImage = false
                }
                else {
                    chatModel.isImage = true
                }
                
                chatModel.strImgUrl = subJson["msgImageUrl"].stringValue
                chatModel.strUser_type = subJson["userType"].stringValue
                
                model.arrItemChat.append(chatModel)
            }
            
            arrOfHistory.append(model)
        }
        
        completionHandler(arrOfHistory)
    }
    
}
