//
//  AdminReportsParser.swift
//  Tribe365
//
//  Created by Apple on 14/02/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 * This class is used to parse the api details for Admin Report
 */
class AdminReportsParser: NSObject {
    
    /** OLD
     * This function is used to parse personality type reports
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of 4 strings for every combination
     */
    class func parseFunctionalLenseReport(response : JSON, completionHandler: @escaping (String,String,String,String) -> Void) {
        
        var st = String()
        var sf = String()
        var nf = String()
        var nt = String()
        
        st = response["data"]["st"].stringValue
        sf = response["data"]["sf"].stringValue
        nt = response["data"]["nt"].stringValue
        nf = response["data"]["nf"].stringValue
        
        completionHandler(st,sf,nt,nf)
    }
    
    /** OLD
     * This function is used to parse personality type reports for know members screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of 4 strings for every combination
     */
    class func parseFunctionalLenseReportKnow(response : JSON, completionHandler: @escaping (String,String,String,String) -> Void) {
        
        var st = String()
        var sf = String()
        var nf = String()
        var nt = String()
        
        st = response["st"].stringValue
        sf = response["sf"].stringValue
        nt = response["nt"].stringValue
        nf = response["nf"].stringValue
        
        completionHandler(st,sf,nt,nf)
    }
    
    /**
     * This function is used to parse directing details
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: BeliefModel
     */
    class func parseDOTDetail(response : JSON, completionHandler: @escaping ([BeliefModel]) -> Void) {
        
        var model = [BeliefModel]()
        
        
        for subJson in response["data"].arrayValue {
            
            let bModel = BeliefModel()
            
            bModel.strId = subJson["beliefId"].stringValue
            bModel.strName = subJson["beliefName"].stringValue
            bModel.strRating = subJson["beliefRatings"].stringValue
            
            for valueJson in subJson["beliefValues"].arrayValue {
                
                let vModel = ValueModel()
                
                vModel.strId = valueJson["valueId"].stringValue
                vModel.strName = valueJson["valueName"].stringValue
                vModel.strRating = valueJson["valueRatings"].stringValue
                
                bModel.arrValues.append(vModel)
            }
            model.append(bModel)
        }
        
        completionHandler(model)
    }
    
    /**
     * This function is used to parse directing details for know organisation screen
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: BeliefModel
     */
    class func parseDOTDetailKnowOrg(response : JSON, completionHandler: @escaping ([BeliefModel], [String]) -> Void) {
        
        var model = [BeliefModel]()
        var beliefNames = [String]()
        
        for (index,subJson) in response["data"]["getDOTreportGraph"].arrayValue.enumerated() {
            
            let bModel = BeliefModel()
            
            bModel.strId = subJson["beliefId"].stringValue
            bModel.strName = subJson["beliefName"].stringValue
            bModel.strRating = subJson["beliefRatings"].stringValue
            
            for valueJson in subJson["beliefValues"].arrayValue {
                
                let vModel = ValueModel()
                
                vModel.strId = valueJson["valueId"].stringValue
                vModel.strName = valueJson["valueName"].stringValue
                vModel.strRating = valueJson["valueRatings"].stringValue
                
                bModel.arrValues.append(vModel)
            }
            model.append(bModel)
            //            beliefNames.append(String(index + 1))
            
            
            var arr = bModel.strName.components(separatedBy: " ")
            let halfCount = arr.count / 2
            let firstArr = aFunction(numbers: arr, position: halfCount)
            
            for (index,_) in firstArr.enumerated() {
                arr.remove(at: 0)
            }
            let secondArr = arr
            
            let firstString = firstArr.joined(separator: " ")
            let seconddString = secondArr.joined(separator: " ")
            
            let finalString = firstString + "\n" + seconddString
            
            //            beliefNames.append(bModel.strName.replacingOccurrences(of: " ", with: "\n"))
            beliefNames.append(finalString)
            
        }
        
        completionHandler(model, beliefNames)
    }
    
    class func aFunction(numbers: Array<String>, position: Int) -> Array<String> {
        let newNumbers = Array(numbers[0..<position])
        return newNumbers
    }
    
    //    class func getArrayForGraphName(arr: [String]) {
    //
    //        var newArr = [String]()
    //
    //        for (index,value) in arr.enumerated() {
    //
    //            if index == 0 {
    //                continue
    //            }
    //
    //            if value.count + arr[index - 1].count <= 10 {
    //                newArr.append(<#T##newElement: String##String#>)
    //                arr[index - 1] = arr[index - 1] + " " + value
    //                arr.remove(at: index)
    //            }
    //            else {
    //
    //            }
    //
    //
    //
    //        }
    //    }
    
}
