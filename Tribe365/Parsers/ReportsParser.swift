//
//  ReportsParser.swift
//  Tribe365
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON
/**
 * This class is used to parse the api details for Reports Module
 */
class ReportsParser: NSObject {
    
    /**
     * This function is used to parse directing reports
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DOTReportsModel
     */
    class func parseDOTReports(response : JSON, completionHandler: @escaping (DOTReportsModel) -> Void) {
        
        let model = DOTReportsModel()
        
        for subJson in response["data"].arrayValue {
            
            let bModel = ReportsBeliefModel()
            
            bModel.strId = subJson["id"].stringValue
            bModel.strName = subJson["name"].stringValue
            
            //parse the value of specific beliefs
            for valueJson in subJson["beliefValueArr"].arrayValue {
                
                let vModel = ReportsValueModel()
                
                vModel.strIndex = valueJson["index"].stringValue
                vModel.strId = valueJson["id"].stringValue
                vModel.strName = valueJson["name"].stringValue
                vModel.strRating = valueJson["ratings"].stringValue
                vModel.strUpVotes = valueJson["upVotes"].stringValue
                vModel.strDownVotes = valueJson["downVotes"].stringValue
                
                
                bModel.arrValues.append(vModel)
            }
            
            model.arrBelief.append(bModel)
        }
        
        completionHandler(model)
    }
    
    /**
     * This function is used to parse directing reports for know members
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DOTReportsModel
     */
    class func parseDOTReportsKnow(response : JSON, completionHandler: @escaping (DOTReportsModel) -> Void) {
        
        let model = DOTReportsModel()
        
        for subJson in response.arrayValue {
            
            let bModel = ReportsBeliefModel()
            
            bModel.strId = subJson["id"].stringValue
            bModel.strName = subJson["name"].stringValue
            
            for valueJson in subJson["beliefValueArr"].arrayValue {
                
                let vModel = ReportsValueModel()
                
                vModel.strIndex = valueJson["index"].stringValue
                vModel.strId = valueJson["id"].stringValue
                vModel.strName = valueJson["name"].stringValue
                vModel.strRating = valueJson["ratings"].stringValue
                vModel.strUpVotes = valueJson["upVotes"].stringValue
                vModel.strDownVotes = valueJson["downVotes"].stringValue
                
                
                bModel.arrValues.append(vModel)
            }
            
            model.arrBelief.append(bModel)
        }
        
        completionHandler(model)
    }
    
    /**
     * This function is used to parse directing reports for user ratings
     
     * @param JSON response:  Accepts response to parse
     * @param completionHandler:  call back method used to send the parsed data in form of Model array named: UserDataOfBubbleModel
     */
    class func parseGetBubbleFromUserRating(response : JSON, completionHandler: @escaping (UserDataOfBubbleModel) -> Void) {
        
        let model = UserDataOfBubbleModel()
        
        model.strUpVotes = response["data"]["upVotes"].stringValue
        model.strDownVotes = response["data"]["downVotes"].stringValue
        
        completionHandler(model)
    }
    
    
}
