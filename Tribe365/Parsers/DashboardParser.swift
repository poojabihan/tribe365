//
//  DashboardParser.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
* This class is used to parse the api details for Dashboard screen
*/
class DashboardParser: NSObject {
    
    /**
    * This function is used to parse organisation list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: OrganisationModel
    */
    class func parseOrganisationList(response : JSON, completionHandler: @escaping ([OrganisationModel]) -> Void) {
        
        var arrOrg = [OrganisationModel]()
        
        for json in response["data"].arrayValue {
            
            let model = OrganisationModel()
            
            model.strNumberOfDepartments = json["numberOfDepartments"].stringValue
            model.strName = json["name"].stringValue
            model.strAddress1 = json["address1"].stringValue
            model.strNumberOfEmployees = json["numberOfEmployees"].stringValue
            model.strOrganisation_logo = json["organisation_logo"].stringValue
            model.strEmail = json["email"].stringValue
            model.strOrganisation_id = json["organisation_id"].stringValue
            model.strOrganisation = json["organisation"].stringValue
            model.strStatus = json["status"].stringValue
            model.strIndustry = json["industry"].stringValue
            model.strNumberOfOffices = json["numberOfOffices"].stringValue
            model.strAddress2 = json["address2"].stringValue
            model.strTurnover = json["turnover"].stringValue
            model.strPhone = json["phone"].stringValue
            model.strLeadPhone = json["lead_phone"].stringValue
            model.strLeadName = json["lead_name"].stringValue
            model.strLeadEmail = json["lead_email"].stringValue

            model.isDot = json["isDot"].boolValue
            
            //Parse office details
            for subJson in json["offices"].arrayValue {
                
                let ofcModel = OfficeModel()
                
                ofcModel.strPhone = subJson["phone"].stringValue
                ofcModel.strOffice_id = subJson["office_id"].stringValue
                ofcModel.strCountry = subJson["country"].stringValue
                ofcModel.strCity = subJson["city"].stringValue
                ofcModel.strOffice = subJson["office"].stringValue
                ofcModel.strAddress = subJson["address"].stringValue
                ofcModel.strNoOfEmployee = subJson["numberOfEmployees"].stringValue
                
                model.arrOffices.append(ofcModel)
            }
            
            arrOrg.append(model)
        }
        
        completionHandler(arrOrg)
    }
    
    /**
    * This function is used to parse department list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: DepartmentModel
    */
    class func parseDepartmentList(response : JSON, completionHandler: @escaping ([DepartmentModel]) -> Void) {
        
        var arrDept = [DepartmentModel]()
        
        for json in response["data"].arrayValue {
            
            let model = DepartmentModel()
            
            model.strId = json["id"].stringValue
            model.strDepartment = json["department"].stringValue
            
            arrDept.append(model)
        }
        
        completionHandler(arrDept)
    }
    
    /**
    * This function is used to parse country list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: CountryModel
    */
    class func parseCountryList(response : JSON, completionHandler: @escaping ([CountryModel]) -> Void) {
        
        var arr = [CountryModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CountryModel()
            
            model.strId = json["id"].stringValue
            model.strCountryName = json["countryName"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse User Profile
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: UserProfileModel
    */
    class func parseUserProfile(response : JSON, completionHandler: @escaping (UserProfileModel) -> Void) {
        
        let model = UserProfileModel()
        
        
        model.strId = response["data"]["id"].stringValue
        model.strName = response["data"]["name"].stringValue
        model.strLastName = response["data"]["lastName"].stringValue
        model.strEmail = response["data"]["email"].stringValue
        model.strOfficeId = response["data"]["officeId"].stringValue
        model.strDepartmentId = response["data"]["departmentId"].stringValue
        model.strRole = response["data"]["role"].stringValue
        model.strStatus = response["data"]["status"].stringValue
        model.strUserContact = response["data"]["userContact"].stringValue
        model.strOrganisationId = response["data"]["orgId"].stringValue
        model.strOrganisationName = response["data"]["organisationName"].stringValue
        model.strOfficeName = response["data"]["officeName"].stringValue
        model.strDepartmentName = response["data"]["departmentName"].stringValue
        model.strProfileImage = response["data"]["profileImage"].stringValue
        model.strOrganisationLogo = response["data"]["organisation_logo"].stringValue
       
        model.strTeamRole = response["data"]["cotTeamRoleMap"].stringValue
//OLD        model.strPersonalityType = response["data"]["sotDetail"].stringValue
        model.strPersonalityType = response["data"]["personalityTypeDetails"].stringValue

        model.strMotivation = response["data"]["sotMotivationDetail"].stringValue

        completionHandler(model)
    }
    
    /**
    * This function is used to parse Departments and Users List
    
    * @param JSON response:  Accepts response to parse
    * @param Bool removeSelfUser: If passed True we remove the logged in user from the user list otherwise userlist is returned as it is
    * @param completionHandler:  call back method used to send the parsed data in form of Models array named: DepartmentModel and UserModel
    */
    class func parseDepartmentUserList(response : JSON, removeSelfUser : Bool, completionHandler: @escaping ([DepartmentModel], [UserModel]) -> Void) {
        
        var arrDept = [DepartmentModel]()
        var arrUser = [UserModel]()
        
        //Parse department details
        for json in response["data"]["departments"].arrayValue {
            
            let model = DepartmentModel()
            
            model.strId = json["department_id"].stringValue
            model.strDepartment = json["name"].stringValue
            
            arrDept.append(model)
        }
        
        //Parse user details
        for json in response["data"]["users"].arrayValue {
            
            let model = UserModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            
            if removeSelfUser && AuthModel.sharedInstance.role == kUser && model.strId == AuthModel.sharedInstance.user_id{
                continue
            }
            
            arrUser.append(model)
        }
        
        completionHandler(arrDept,arrUser)
    }
    
    /**
    * This function is used to parse Tier List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: TierModel
    */
    class func parseActionTierList(response : JSON, completionHandler: @escaping ([TierModel]) -> Void) {
        
        var arr = [TierModel]()
        
        for json in response["data"].arrayValue {
            
            let model = TierModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse User List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: UserModel
    */
    class func parseUserList(response : JSON, completionHandler: @escaping ([UserModel]) -> Void) {
        
        var arr = [UserModel]()
        
        for json in response["data"].arrayValue {
            
            let model = UserModel()
            
            model.strDepartment = json["department"].stringValue
            model.strDepartmentId = json["departmentId"].stringValue
            model.strEmail = json["email"].stringValue
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strOfficeId = json["officeId"].stringValue
            model.strOrgId = json["orgId"].stringValue
            model.strPassword = json["password"].stringValue
            model.strOrgName = json["organisationName"].stringValue
            model.strOfficeName = json["officeName"].stringValue
            model.strDepartmentPreDefineId = json["departmentPreDefineId"].stringValue

            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Comment List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: CommentModel
    */
    class func parseCommentList(response : JSON, completionHandler: @escaping ([CommentModel]) -> Void) {
        
        var arr = [CommentModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CommentModel()
            
            model.strComment = json["comment"].stringValue
            model.strCreated_at = json["created_at"].stringValue
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strUserId = json["userId"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Action List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ActionModel
    */
    class func parseActionList(response : JSON, completionHandler: @escaping ([ActionModel]) -> Void) {
        
        var arr = [ActionModel]()
        
        for json in response["data"].arrayValue {
            
            let model = ActionModel()
            
            model.strDescription = json["description"].stringValue
            model.strDueDate = json["dueDate"].stringValue
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strOrgId = json["orgId"].stringValue
            model.strOrgStatus = json["orgStatus"].stringValue
            model.strStartedDate = json["startedDate"].stringValue
            model.strUserId = json["userId"].stringValue
            model.strResponsibleName = json["responsibleName"].stringValue
            model.strActionTierType = json["tier"].stringValue
            model.strTierId = json["tierId"].stringValue
            model.strOffDeptName = json["offDeptName"].stringValue
            model.strOffDeptId = json["offDeptId"].stringValue
            model.strResponsibleUserId = json["responsibleUserId"].stringValue
            
            var arrThemeNames = [String]()
            
            //Parse themes related to the action
            for value in json["themes"].arrayValue {
                
                let themeModel = ThemeModel()
                
                themeModel.strId = value["id"].stringValue
                themeModel.strTitle = value["title"].stringValue
                
                arrThemeNames.append(themeModel.strTitle)
                
                model.arrThemes.append(themeModel)
            }
            
            //Themes array changed to comma seperated string for display purpose
            model.strThemesForDisplay = arrThemeNames.joined(separator: ", ")
            
            if model.strThemesForDisplay == "" {
                model.strThemesForDisplay = " "
            }
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Value List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ValueModel
    */
    class func parseValuesList(response : JSON, completionHandler: @escaping ([ValueModel]) -> Void) {
        
        var arr = [ValueModel]()
        
        for json in response["data"].arrayValue {
            
            let model = ValueModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Evidence List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: EvidenceModel
    */
    class func parseEvidenceList(response : JSON, completionHandler: @escaping ([EvidenceModel]) -> Void) {
        
        var arr = [EvidenceModel]()
        
        for json in response["data"].arrayValue {
            
            
            let model = EvidenceModel()
            
            model.strDescription = json["description"].stringValue
            model.strUserId = json["userId"].stringValue
            model.strCreatedAt = json["created_at"].stringValue
            model.strSection = json["section"].stringValue
            model.strFileURL = json["fileURL"].stringValue
            model.strId = json["id"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Theme List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ThemeModel
    */
    class func parseThemesList(response : JSON, completionHandler: @escaping ([ThemeModel]) -> Void) {
        
        var arr = [ThemeModel]()
        
        for json in response["data"]["themeList"].arrayValue {
            
            let model = ThemeModel()
            
            model.strId = json["id"].stringValue
            model.strTitle = json["title"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Responsible person List without including logged in user
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ResponsiblePersonModel
    */
    class func parseResponsiblePresonList(response : JSON, completionHandler: @escaping ([ResponsiblePersonModel]) -> Void) {
        
        var arr = [ResponsiblePersonModel]()
        
        for json in response["data"].arrayValue {
            
            let model = ResponsiblePersonModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strEmail = json["email"].stringValue
            model.strStatus = json["status"].stringValue
            model.strRoleId = json["roleId"].stringValue
            model.strOrgId = json["orgId"].stringValue
            model.strOfficeId = json["officeId"].stringValue
            model.strDepartmentId = json["departmentId"].stringValue
            
            //Restricted logged in user detail to append in array
            if AuthModel.sharedInstance.role == kUser && model.strId == AuthModel.sharedInstance.user_id{
                continue
            }
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse Responsible person List with logged in user
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ResponsiblePersonModel
    */
    class func parseResponsiblePresonListForActionPage(response : JSON, completionHandler: @escaping ([ResponsiblePersonModel]) -> Void) {
        
        var arr = [ResponsiblePersonModel]()
        
        for json in response["data"].arrayValue {
            
            let model = ResponsiblePersonModel()
            
            model.strId = json["id"].stringValue
            model.strName = json["name"].stringValue + " " + json["lastName"].stringValue
            model.strEmail = json["email"].stringValue
            model.strStatus = json["status"].stringValue
            model.strRoleId = json["roleId"].stringValue
            model.strOrgId = json["orgId"].stringValue
            model.strOfficeId = json["officeId"].stringValue
            model.strDepartmentId = json["departmentId"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    /**
    * This function is used to parse DOT Details
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: DOTModel
    */
    class func parseDOTDetail(response : JSON, completionHandler: @escaping (DOTModel) -> Void) {
        
        let model = DOTModel()
        
        model.strId = response["data"]["id"].stringValue
        model.strVision = response["data"]["vision"].stringValue
        model.strMission = response["data"]["mission"].stringValue
        model.strFocus = response["data"]["focus"].stringValue
        model.strOrgId = response["data"]["orgId"].stringValue
        model.strDotDate = response["data"]["dot_date"].stringValue
        model.strIntroductoryInformation = response["data"]["introductory_information"].stringValue
        
        //Parse belief for DOT
        for subJson in response["data"]["belief"].arrayValue {
            
            let bModel = BeliefModel()
            
            bModel.strId = subJson["id"].stringValue
            bModel.strName = subJson["name"].stringValue
            bModel.strDotId = subJson["dotId"].stringValue
            
            //Parse values of specific belief
            for valueJson in subJson["belief_value"].arrayValue {
                
                let vModel = ValueModel()
                
                vModel.strIndex = valueJson["index"].stringValue
                vModel.strId = valueJson["id"].stringValue
                vModel.strName = valueJson["name"].stringValue
                vModel.strValueId = valueJson["valueId"].stringValue
                vModel.strBeliefId = valueJson["beliefId"].stringValue
                vModel.strStatus = valueJson["status"].stringValue
                vModel.strCreatedAt = valueJson["created_at"].stringValue
                vModel.strUpdatedAt = valueJson["updated_at"].stringValue
                vModel.strDotId = valueJson["dotId"].stringValue
                vModel.strRating = valueJson["ratings"].stringValue
                
                bModel.arrValues.append(vModel)
            }
            
            model.arrBelief.append(bModel)
        }
        
        completionHandler(model)
    }
    
    /**
    * This function is used to parse DOT Values
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: ValueModel
    */
    class func parseDOTValues(response : JSON, completionHandler: @escaping ([ValueModel]) -> Void) {
        
        var arrValues = [ValueModel]()
        
        //Parse belief for DOT
        for subJson in response["data"]["belief"].arrayValue {
            
            let bModel = BeliefModel()
            
            bModel.strId = subJson["id"].stringValue
            bModel.strName = subJson["name"].stringValue
            bModel.strDotId = subJson["dotId"].stringValue
            
            //Parse values for specific belief
            for valueJson in subJson["belief_value"].arrayValue {
                
                let vModel = ValueModel()
                
                vModel.strIndex = valueJson["index"].stringValue
                vModel.strId = valueJson["id"].stringValue
                vModel.strName = valueJson["name"].stringValue
                vModel.strValueId = valueJson["valueId"].stringValue
                vModel.strBeliefId = valueJson["beliefId"].stringValue
                vModel.strStatus = valueJson["status"].stringValue
                vModel.strCreatedAt = valueJson["created_at"].stringValue
                vModel.strUpdatedAt = valueJson["updated_at"].stringValue
                vModel.strDotId = valueJson["dotId"].stringValue
                vModel.strRating = valueJson["ratings"].stringValue
                
                arrValues.append(vModel)
            }
            
        }
        
        completionHandler(arrValues)
    }
    
    /**
    * This function is used to parse Actions Status
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Integer as ActionStarted, ActionNotStarted, ActionCompleted
    */
    class func parseActions(response : JSON, completionHandler: @escaping (Int, Int, Int) -> Void) {
        
        var started = 0
        var notStarted = 0
        var completed = 0

        started = response["data"]["statusStarted"].intValue
        notStarted = response["data"]["statusNotStarted"].intValue
        completed = response["data"]["statusCompleted"].intValue

        completionHandler(started, notStarted, completed)

    }
    
    /**
    * This function is used to parse Happy Index Values
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: HappyIndexModel
    */
    class func parseHappyIndex(response : JSON, completionHandler: @escaping ([HappyIndexModel]) -> Void) {
        
        var arrHappyIndex = [HappyIndexModel]()
        
        for json in response.arrayValue {
            
            let model = HappyIndexModel()
            
            model.strAverageIndex = json["average"].stringValue
            model.strHappyIndex = json["happy"].stringValue
            model.strSadIndex = json["sad"].stringValue
            model.strYear = json["year"].stringValue
            model.strMonth = json["monthName"].stringValue
            model.strWeek = json["week"].stringValue
            model.strDay = json["dayName"].stringValue

            arrHappyIndex.append(model)
        }
        
        completionHandler(arrHappyIndex)
        
    }

    /**
    * This function is used to parse Notification List which uses pagination
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: NotificationModel and String which returns total Page count
    */
    class func parseReadNotificationList(response : JSON, completionHandler: @escaping ([NotificationModel], String) -> Void) {
        
        var arrRead = [NotificationModel]()
//        var arrUnread = [NotificationModel]()

        for json in response["data"].arrayValue {
            
            let model = NotificationModel()
            
            model.strID = json["id"].stringValue
            model.strTitle = json["title"].stringValue
            model.strDescription = json["description"].stringValue
            model.strDate = UTCToLocal(date: json["created_at"].stringValue)
            model.strType = json["notificationType"].stringValue
            model.strFeedbackId = json["feedbackId"].stringValue
            model.isRead = json["isRead"].boolValue
            model.strUserEmail = json["userEmail"].stringValue
            model.strUserImage = json["userImage"].stringValue
            model.strUsername = json["userName"].stringValue
            model.isMultiple = json["isMultiple"].boolValue

//            if model.isRead {
                arrRead.append(model)
//            }
//            else {
//                arrUnread.append(model)
//            }
        }
        
        completionHandler(arrRead, response["totalPageCount"].stringValue)
    }
    
    /**
    * This function is used to parse Read Notification List
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: NotificationModel and String which returns notification count
    */
    class func parseUnReadNotificationList(response : JSON, completionHandler: @escaping ([NotificationModel], String) -> Void) {
        
        //This array contains the notifications in order as firstUseChecklist, Unread_Notifications, ToDoChecklist
        var arrRead = [NotificationModel]()
            
            //If any first use checklist is pending we will add to array
            if !response["ansStatus"]["tribeValue"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.tribeValue
                model.strDescription = "Evaluate Yourself against Tribe Values"
                arrRead.append(model)
            }
            if !response["ansStatus"]["functionalLens"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.functionalLens
                model.strDescription = "Complete Personality Type Questionnaire"
                arrRead.append(model)
            }
            if !response["ansStatus"]["teamRoleAnswers"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.teamRoleAnswers
                model.strDescription = "Complete Team Role Questionnaire"
                arrRead.append(model)
            }
            if !response["ansStatus"]["cultureStructure"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.cultureStructure
                model.strDescription = "Complete Culture Structure Questionnaire"
                arrRead.append(model)
            }
            if !response["ansStatus"]["motivation"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.motivation
                model.strDescription = "Complete Motivation Questionnaire"
                arrRead.append(model)
            }
            if !response["ansStatus"]["tribeometer"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.tribeometer
                model.strDescription = "Complete Tribeometer Survey"
                arrRead.append(model)
            }
            if !response["ansStatus"]["diagnostic"].boolValue {
                let model = NotificationModel()
                model.strTitle = "First Use Checklist"
                model.strDate = ""
                model.strType = NetworkConstant.NotificationType.diagnostic
                model.strDescription = "Complete Diagnostic Survey"
                arrRead.append(model)
            }
        
        //If no first use checklist is pending we will include todolist
        var includeToDo = false
        if arrRead.count == 0 {
            includeToDo = true
        }
        
        //We will add unread notifications in array
        var count = 0
        for json in response["data"].arrayValue {
            
            let model = NotificationModel()
            
            model.strID = json["id"].stringValue
            model.strTitle = json["title"].stringValue
            model.strDescription = json["description"].stringValue
            model.strDate = UTCToLocal(date: json["created_at"].stringValue) //json["created_at"].stringValue
            model.strType = json["notificationType"].stringValue
            model.strFeedbackId = json["feedbackId"].stringValue
            model.isRead = json["isRead"].boolValue
            model.strUserEmail = json["userEmail"].stringValue
            model.strUserImage = json["userImage"].stringValue
            model.strUsername = json["userName"].stringValue
            model.isMultiple = json["isMultiple"].boolValue

            arrRead.append(model)
            count = count + 1
        }
        
        //Is no first use checklist pending, then include todo list at the end of array
        if includeToDo {
            
                if !response["toDoList"]["tribeValue"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.tribeValue
                    model.strDescription = "Review your values (Monthly)"
                    arrRead.append(model)
                }
                if !response["toDoList"]["functionalLens"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.functionalLens
                    model.strDescription = "Review Personality Type Questionnaire"
                    arrRead.append(model)
                }
                if !response["toDoList"]["teamRoleAnswers"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.teamRoleAnswers
                    model.strDescription = "Review Team Role Questionnaire"
                    arrRead.append(model)
                }
                if !response["toDoList"]["cultureStructure"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.cultureStructure
                    model.strDescription = "Review Culture Structure Questionnaire"
                    arrRead.append(model)
                }
                if !response["toDoList"]["motivation"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.motivation
                    model.strDescription = "Review Motivation Questionnaire"
                    arrRead.append(model)
                }
                if !response["toDoList"]["tribeometer"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.tribeometer
                    model.strDescription = "Review Tribeometer Survey"
                    arrRead.append(model)
                }
                if !response["toDoList"]["diagnostic"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.diagnostic
                    model.strDescription = "Review Diagnostic Survey"
                    arrRead.append(model)
                }
                if !response["toDoList"]["bubbleRatings"].boolValue {
                    let model = NotificationModel()
                    model.strTitle = "To Do List"
                    model.strDate = ""
                    model.strType = NetworkConstant.NotificationType.bubbleRatings
                    model.strDescription = "Award Kudos to your colleagues (Daily)"
                    arrRead.append(model)
                }
        }
        
        completionHandler(arrRead, String(count))
    }
    
    /**
     * This function converts Local time to UTC
     * @param: date: pass the local date string which you want to convert to utc
     */
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"

        return dateFormatter.string(from: dt!)
    }

    /**
        * This function converts UTC time to Local
        * @param: date: pass the utc date string which you want to convert to local
    */
    class func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"

        print("Converted to loacl:", dateFormatter.string(from: dt!))
        
        return dateFormatter.string(from: dt!)
    }
    
    /**
    * This function is used to parse Checklist Data
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data
    * @return [ChecklistModel] Stores FirseUserChecklist & Todo checklist, Bool stores whether to show Todo checklist or not, Int stores the pending checklist count
    */
    class func parseChecklist(response : JSON, completionHandler: @escaping ([ChecklistModel], [ChecklistModel], Bool, Int) -> Void) {
        
        var arrChecklist = [ChecklistModel]()
        var arrTodo = [ChecklistModel]()
        var pendingCount = 0

        let arrChecklistTitle = ["Evaluate Yourself against Tribe Values", "Complete Personality Type Questionnaire", "Complete Team Role Questionnaire", "Complete Culture Structure Questionnaire", "Complete Motivation Questionnaire", "Complete Tribeometer Survey", "Complete Diagnostic Survey"]
        
        let arrChecklistStatus = [response["data"]["tribeStatus"]["tribeValue"].boolValue, response["data"]["tribeStatus"]["functionalLens"].boolValue, response["data"]["tribeStatus"]["teamRoleAnswers"].boolValue, response["data"]["tribeStatus"]["cultureStructure"].boolValue, response["data"]["tribeStatus"]["motivation"].boolValue, response["data"]["tribeStatus"]["tribeometer"].boolValue, response["data"]["tribeStatus"]["diagnostic"].boolValue]

        let arrTodosTitle = ["Award Kudos to your colleagues (Daily)", "Review your values (Monthly)", "Review Personality Type Questionnaire", "Review Team Role Questionnaire", "Review Culture Structure Questionnaire", "Review Motivation Questionnaire", "Review Tribeometer Survey", "Review Diagnostic Survey"]

        let arrTodosStatus = [response["data"]["toDoList"]["bubbleRatings"].boolValue, response["data"]["toDoList"]["tribeValue"].boolValue, response["data"]["toDoList"]["functionalLens"].boolValue, response["data"]["toDoList"]["teamRoleAnswers"].boolValue, response["data"]["toDoList"]["cultureStructure"].boolValue, response["data"]["toDoList"]["motivation"].boolValue, response["data"]["toDoList"]["tribeometer"].boolValue, response["data"]["toDoList"]["diagnostic"].boolValue]

        for (index, value) in arrChecklistTitle.enumerated() {
            let model = ChecklistModel()
            
            model.strTitle = value
            model.strStatus = arrChecklistStatus[index]
            
            arrChecklist.append(model)
        }
        
        for (index, value) in arrTodosTitle.enumerated() {
            let model = ChecklistModel()
            
            model.strTitle = value
            model.strStatus = arrTodosStatus[index]
            
            arrTodo.append(model)
        }
        
//        pendingCount = (arrChecklistStatus.histogram[false] ?? 0) + (arrTodosStatus.histogram[false] ?? 0)
        
        if !arrChecklistStatus.contains(false) {
            pendingCount = (arrChecklistStatus.histogram[false] ?? 0) + (arrTodosStatus.histogram[false] ?? 0)

            completionHandler(arrChecklist, arrTodo, true, pendingCount)
        }
        else {
            pendingCount = (arrChecklistStatus.histogram[false] ?? 0)

            completionHandler(arrChecklist, arrTodo, false, pendingCount)
        }
        
    }
}

/**
* This is and array extension used to count the value which is passed in as parameter
*/
extension Array where Element: Hashable {
    var histogram: [Element: Int] {
        return self.reduce(into: [:]) { counts, elem in counts[elem, default: 0] += 1 }
    }
}
