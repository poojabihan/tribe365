//
//  DateField.swift
//  CTTracker
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class DateField: UITextField {

        var date: Date?
        let datePicker = UIDatePicker()
        let formatter = DateFormatter()
        override func didMoveToSuperview() {
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            formatter.dateStyle = .short
            formatter.dateFormat = "dd-MM-yyyy"
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            toolbar.backgroundColor = UIColor.white
            datePicker.backgroundColor = UIColor.white
            datePicker.tintColor = UIColor.darkGray
            toolbar.tintColor = UIColor.darkGray
            
            let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction))
            let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancel = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(cancelAction))
            toolbar.setItems([cancel,space,done], animated: false)
            inputAccessoryView = toolbar
            inputView = datePicker
        }
        @objc func cancelAction(_ sender: UIBarButtonItem) {
            
            text = ""
            
            endEditing(true)
        }
        @objc func doneAction(_ sender: UIBarButtonItem) {
            date = datePicker.date
            text = formatter.string(from: datePicker.date)
            
            endEditing(true)
        }

}
