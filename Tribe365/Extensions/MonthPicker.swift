//
//  MonthPicker.swift
//  Tribe365
//
//  Created by Apple on 08/07/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit
import MonthYearPicker

class MonthPicker: UITextField {
    var date: Date?
    let picker = MonthYearPickerView()
    let formatter = DateFormatter()
    override func didMoveToSuperview() {

        formatter.dateStyle = .short
        formatter.dateFormat = "MMM-yyyy"
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor.white
        toolbar.tintColor = UIColor.darkGray
        
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(cancelAction))
        toolbar.setItems([cancel,space,done], animated: false)
        inputAccessoryView = toolbar
        
        let picker = MonthYearPickerView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: toolbar.bounds.width, height: 216)))
        picker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        picker.backgroundColor = UIColor.white
        picker.tintColor = UIColor.darkGray

        inputView = picker
    }
    @objc func cancelAction(_ sender: UIBarButtonItem) {
        
        text = ""
        
        endEditing(true)
    }
    @objc func doneAction(_ sender: UIBarButtonItem) {
        
        endEditing(true)
    }
    
    @objc func dateChanged(_ picker: MonthYearPickerView) {
        print("date changed: \(picker.date)")
        date = picker.date
        text = formatter.string(from: picker.date)
    }
}
