//
//  MessageModel.swift
//  Tribe365
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class MessageModel: NSObject {

    var strChangeit_id = ""
    var strCreated_date = ""
    var strImages = ""
    var strLocation_detail = ""
    var strMsg_detail = ""
    var strMsg_type = ""
    var arrItemChat = [HistoryItemChatModel]()
    var arrLastMessage = [HistoryLastMessageModel]()
    
    var strID = ""
    var isImage = false
    var strMessage = ""

}
