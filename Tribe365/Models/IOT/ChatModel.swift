//
//  ChatModel.swift
//  Tribe365
//
//  Created by Apple on 13/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ChatModel: NSObject {

    var strCr_id = ""
    var strMessage = ""
    var strCreated_date = ""
    var strPost_type = ""
    var strUser_type = ""
    var strSendTo = ""
    var strSendFrom = ""
    var strName = ""
    var isImage = false
    var strImgUrl = ""

}
