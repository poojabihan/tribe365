//
//  HistoryLastMessageModel.swift
//  Tribe365
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class HistoryLastMessageModel: NSObject {

    var strCr_id = ""
    var strMessage = ""
    var strCreated_date = ""
    var strPost_type = ""
    var strUser_type = ""
}
