//
//  ResponsiblePersonModel.swift
//  Tribe365
//
//  Created by Apple on 20/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ResponsiblePersonModel: NSObject {

     var strId = ""
     var strName = ""
     var strEmail = ""
     var strStatus = ""
     var strRoleId = ""
     var strOrgId = ""
     var strOfficeId = ""
     var strDepartmentId = ""
      
}
