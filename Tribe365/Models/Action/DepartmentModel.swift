//
//  DepartmentModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DepartmentModel: NSObject {

    var strId = ""
    var strDepartment = ""
    var strOfficeId = ""
    var isSelected = false

}
