//
//  CommentModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class CommentModel: NSObject {

    var strComment = ""
    var strCreated_at = ""
    var strId = ""
    var strName = ""
    var strUserId = ""

}
