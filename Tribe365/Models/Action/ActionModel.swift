//
//  ActionModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ActionModel: NSObject {

    var strDescription = ""
    var strDueDate = ""
    var strId = ""
    var strName = ""
    var strOrgId = ""
    var strOrgStatus = ""
    var strStartedDate = ""
    var strUserId = ""
    var strResponsibleName = ""
    var strActionTierType = ""
    var strTierId = ""
    var strOffDeptName = ""
    var strOffDeptId = ""
    var strResponsibleUserId = ""
    var arrThemes = [ThemeModel]()
    var strThemesForDisplay = ""

}
