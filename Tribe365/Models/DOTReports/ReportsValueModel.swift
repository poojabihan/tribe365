//
//  ReportsValueModel.swift
//  Tribe365
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ReportsValueModel: NSObject {

    var strId = ""
    var strName = ""
    var strIndex = ""
    var strUpVotes = ""
    var strDownVotes = ""
    var strRating = ""
}
