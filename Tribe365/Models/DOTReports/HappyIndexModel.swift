//
//  HappyIndexModel.swift
//  Tribe365
//
//  Created by Apple on 18/10/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class HappyIndexModel: NSObject {

    var strHappyIndex = ""
    var strSadIndex = ""
    var strAverageIndex = ""
    var strYear = ""
    var strMonth = ""
    var strWeek = ""
    var strDay = ""
    var bgColor = UIColor.clear

}
