//
//  TriboemeterQuestionModel.swift
//  Tribe365
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class TriboemeterQuestionModel: NSObject {
    
    var strQuestionId = ""
    var strQuestionName = ""
    var selectedIndex = -1
    var strAnswerId = ""
    var strAPIAnswerID = ""
    var arrOfOptions = [TribeometerOptionModel]()
}
