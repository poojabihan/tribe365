//
//  FunctionalLenseMCQOptions.swift
//  Tribe365
//
//  Created by Apple on 05/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class FunctionalLenseMCQOptions: NSObject {

    var strOptionId = ""
    var strOptionName = ""
    var strValueName = ""
    var strInitialValue = ""
    var isChecked = false
}
