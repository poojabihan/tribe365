//
//  OptionModel.swift
//  Tribe365
//
//  Created by kdstudio on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class OptionModel: NSObject {

    var strOptionId = ""
    var strOption = ""
    var strAnswer = "0"
    var strRoleMapId = ""
    var strAnswerID = ""

}
