//
//  QuestionModel.swift
//  Tribe365
//
//  Created by kdstudio on 06/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class QuestionModel: NSObject {
    
    var strQuestionId = ""
    var strQuestionName = ""
    var arrOptions = [OptionModel]()

}
