//
//  SummaryModel.swift
//  Tribe365
//
//  Created by kdstudio on 11/09/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class SummaryModel: NSObject {

    var strCoordinator = ""
    var strImplementer = ""
    var strMonitorEvaluator = ""
    var strResourceInvestigator = ""
    var strShaper = ""
    var strCompleterFinisher = ""
    var strTeamworker = ""
    var strPlant = ""
    
    var strCoordinatorScore = ""
    var strImplementerScore = ""
    var strMonitorEvaluatorScore = ""
    var strResourceInvestigatorScore = ""
    var strShaperScore = ""
    var strCompleterFinisherScore = ""
    var strTeamworkerScore = ""
    var strPlantScore = ""
    
    var strCoordinatorValue = ""
    var strImplementerValue = ""
    var strMonitorEvaluatorValue = ""
    var strResourceInvestigatorValue = ""
    var strShaperValue = ""
    var strCompleterFinisherValue = ""
    var strTeamworkerValue = ""
    var strPlantValue = ""

    var strUserName = ""
    var strUserId = ""

}
