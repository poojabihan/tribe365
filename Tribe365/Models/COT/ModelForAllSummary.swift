//
//  ModelForAllSummary.swift
//  Tribe365
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ModelForAllSummary : NSObject {

    var strtitle = ""
    var strValue:NSMutableAttributedString?
    var strPositives = ""
    var strallowableWeaknesses = ""
    var strSummary = ""
}
