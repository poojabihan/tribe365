//
//  TribeTipsArray.swift
//  Tribe365
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class TribeTipsArray: NSObject {

    var strValue = ""
    var strTitle = ""
    var strSummary = ""
    var arrPersuadeValues = [PersuadeValues]()
    var arrSeekValues = [SeeksValues]()

}
/*value": "ST",
"title": "ST",
"summary": "Pragmatic\r\nTask Focussed\r\nHere and Now",
"seekPersuadeValues":[
{
"value": "Measurable Results",
"valueType": "seek"
},
{
"value": "Solving Problems",
"valueType": "seek"
},
{
"value": "Proven Methods",
"valueType": "seek"
},
{
"value": "Show Results can be measured",
"valueType": "persuade"
},
{
"value": "Answer all questions",
"valueType": "persuade"
},
{
"value": "Try before buy",
"valueType": "persuade"
}
]*/
