//
//  InitialValueListModel.swift
//  Tribe365
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class InitialValueListModel: NSObject {

    var strScore = ""
    var strValue = ""
    var strTitle = ""
    var strPositives = ""
    var strAllowableWeaknesses = ""
}
/*{
 "score": 12,
 "section": "S",
 "title": "Slight Sensing",
 "positives": "",
 "allowable_weaknesses": ""
 },*/
