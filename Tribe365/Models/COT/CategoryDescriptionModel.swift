//
//  CategoryDescriptionModel.swift
//  Tribe365
//
//  Created by Apple on 13/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class CategoryDescriptionModel: NSObject {

    var strTitle = ""
    var strShortDescription = ""
    var strLongDescription = ""
    
}

