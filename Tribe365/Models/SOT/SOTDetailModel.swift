//
//  SOTDetailModel.swift
//  Tribe365
//
//  Created by Apple on 30/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class SOTDetailModel: NSObject {

    var strId = ""
    var strType = ""
    var strTitle = ""
    var strImageUrl = ""
    var strSOTCount = ""
}
