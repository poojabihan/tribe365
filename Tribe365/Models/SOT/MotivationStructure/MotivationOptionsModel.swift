//
//  MotivationOptionsModel.swift
//  Tribe365
//
//  Created by Apple on 10/01/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class MotivationOptionsModel: NSObject {

    var strOptionId = ""
    var strOption = ""
    var strCategoryId = ""
    var strPoints = ""
    var strAnswerId = ""
    var selectedIndex = -1
    var strSelectedOption1 = ""
    var strSelectedOption2 = ""
    var strRatingForOption1 = ""
    var strRatingForOption2 = ""
}
