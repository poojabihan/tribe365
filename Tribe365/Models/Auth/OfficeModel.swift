//
//  OfficeModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class OfficeModel: NSObject {

    var strPhone = ""
    var strOffice_id = ""
    var strCountry = ""
    var strCity = ""
    var strOffice = ""
    var strAddress = ""
    var strNoOfEmployee = ""
    
    var arrDepartment = [DepartmentModel]()
    var arrDepartmentNames = [String]()

}
