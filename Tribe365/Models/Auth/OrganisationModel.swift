//
//  OrganisationModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class OrganisationModel: NSObject {
    
    var strNumberOfDepartments = ""
    var strName = ""
    var strAddress1 = ""
    var strNumberOfEmployees = ""
    var strOrganisation_logo = ""
    var strEmail = ""
    var strOrganisation_id = ""
    var strOrganisation = ""
    var arrOffices = [OfficeModel]()
    var strStatus = ""
    var strIndustry = ""
    var strNumberOfOffices = ""
    var strAddress2 = ""
    var strTurnover = ""
    var strPhone = ""
    var strLeadName = ""
    var strLeadEmail = ""
    var strLeadPhone = ""
    var isDot = false
}
