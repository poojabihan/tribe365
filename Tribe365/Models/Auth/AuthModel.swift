//
//  AuthModel.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class AuthModel {

    static let sharedInstance = AuthModel()

    var name = ""
    var lastName = ""
    var department = ""
    var email = ""
    var user_id = ""
    var departmentId = ""
    var office = ""
    var officeId = ""
    var token = ""
    var role = ""
    var organisation_logo = ""
    var orgId = ""
    var isDot = ""
    var profileImageUrl = ""
    var selectedSideBarIndex = 1
    var orgName = ""

}
