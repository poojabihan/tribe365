//
//  NotificationModel.swift
//  Tribe365
//
//  Created by Apple on 23/08/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {

    var strID = ""
    var strTitle = ""
    var strDescription = ""
    var strDate = ""
    var strType = ""
    var strFeedbackId = ""
    var isRead = false
    
    var strUserEmail = ""
    var strUserImage = ""
    var strUsername = ""
    var isMultiple = false

}
