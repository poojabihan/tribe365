//
//  UserProfileModel.swift
//  Tribe365
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class UserProfileModel: NSObject {

    var strId = ""
    var strName = ""
    var strLastName = ""
    var strEmail = ""
    var strOfficeId = ""
    var strDepartmentId = ""
    var strRole = ""
    var strStatus = ""
    var strUserContact = ""
    var strOrganisationId = ""
    var strOrganisationName = ""
    var strOfficeName = ""
    var strDepartmentName = ""
    var strProfileImage = ""
    var strOrganisationLogo = ""
    
    var strTeamRole = ""
    var strPersonalityType = ""
    var strMotivation = ""

}
