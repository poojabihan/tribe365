//
//  UserModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    var strId = ""
    var strName = ""
    var strDepartment = ""
    var strDepartmentId = ""
    var strEmail = ""
    var strOfficeId = ""
    var strOrgId = ""
    var strPassword = ""
    var strOrgName = ""
    var strOfficeName = ""
    var strDepartmentPreDefineId = ""
    var isSelected = false

}
