//
//  ValueModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ValueModel: NSObject {

    var strId = ""
    var strName = ""
    var strValueId = ""
    var strBeliefId = ""
    var strBeliefName = ""
    var strIndex = ""
    var strStatus = ""
    var strCreatedAt = ""
    var strUpdatedAt = ""
    var strDotId = ""
    var strRating = ""
    var isSelected = false
    var bgColor = UIColor.clear

}
