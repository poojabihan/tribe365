//
//  EvidenceModel.swift
//  Tribe365
//
//  Created by kdstudio on 17/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class EvidenceModel: NSObject {

    var strDescription = ""
    var strUserId = ""
    var strCreatedAt = ""
    var strSection = ""
    var strFileURL = ""
    var strId = ""
    var strUpdatedAt = ""

}
