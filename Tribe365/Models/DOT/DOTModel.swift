//
//  DOTModel.swift
//  Tribe365
//
//  Created by kdstudio on 17/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DOTModel: NSObject {

    var strId = ""
    var strVision = ""
    var strMission = ""
    var strFocus = ""
    var strOrgId = ""
    var strDotDate = ""
    var strIntroductoryInformation = ""
    var arrBelief = [BeliefModel]()

}
