//
//  BeliefModel.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class BeliefModel: NSObject {

    var arrValues = [ValueModel]()
    var strName = ""
    var strId = ""
    var strDotId = ""
    var strRating = ""
    var bgColor = UIColor.clear

}
