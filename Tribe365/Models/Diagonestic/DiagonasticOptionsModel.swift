//
//  DiagonasticOptionsModel.swift
//  Tribe365
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DiagonasticOptionsModel: NSObject {

    var strOptionId = ""
    var strOptionName = ""
    var isChecked = Bool()
}
