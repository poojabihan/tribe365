//
//  DiagnosticResultModel.swift
//  Tribe365
//
//  Created by Apple on 20/12/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DiagnosticResultModel: NSObject {

    var strTitle = ""
    var strScore = ""
    var strPercentage = ""
    var bgColor = UIColor.clear
    var strCategoryId = ""

}
