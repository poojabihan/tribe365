//
//  ViewController.swift
//  Tribe365
//
//  Created by kdstudio on 16/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var roleSegment: UISegmentedControl!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    var selectedRole = "3"
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPassword.delegate = self
        txtUsername.delegate = self
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - Action Methods
    @IBAction func btnSignInAction(_ sender: Any) {
        
        if (txtUsername.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Username.")
        }
            
        else if (txtUsername.text?.isValidEmail())! {
            if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Password.")
            }
            else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
            }
            else {
                callWebServiceForLogin()
            }
        }
        else {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid Username.")
        }
    }
    
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    @IBAction func btnForgotPwdAction(_ sender: Any) {
         let objVC = UIStoryboard(name: "AuthModule", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordView") as!  ForgotPasswordView
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func roleSegmentAction(_ sender: Any) {
        
        /*Tribe365 Roles: Superadmin - 1 admin - 2 user - 3*/
        if roleSegment.selectedSegmentIndex == 0 {
            selectedRole = "3"
        }
        if roleSegment.selectedSegmentIndex == 1{
            selectedRole = "1"
        }
    }
    
    @IBAction func btnHelpAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "SOTModule", bundle: nil).instantiateViewController(withIdentifier: "SupportScreensViewController") as!  SupportScreensViewController
        objVC.supportScreenType = NetworkConstant.SupportScreensType.register
       self.present(objVC, animated: true, completion: nil) 
    }
    
    //MARK: - UITextfiled Delegete
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLogin() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["email":txtUsername.text ?? "",
                     "password":txtPassword.text ?? "",
                     "role":selectedRole,
                     "deviceId":UIDevice.current.identifierForVendor?.uuidString ?? "",
                     "fcmToken":UserDefaults.standard.value(forKey: "fcm_token") as! String]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true { // Admin
                    if response!["data"]["role"] == "1"{
                        
                        self.setLoginData(json: response!)
                        
                        if self.roleSegment.selectedSegmentIndex == 0 {
                        self.performSegue(withIdentifier: "navigateToUser", sender: nil)

//                        if self.roleSegment.selectedSegmentIndex == 0 {

                        }
                        else{
                            self.performSegue(withIdentifier: "navigateToSuperAdmin", sender: nil)
                        }
                    }
                    else{ //User
                        
//                    self.callWebServiceToRegisterFCMTokenForIOT(loginResponse: response!)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        self.setLoginData(json: response!)
                        
                        if self.roleSegment.selectedSegmentIndex == 0 {
                           // self.performSegue(withIdentifier: "navigateToUser", sender: nil)
                            
                            let mainVcIntial = kConstantObj.SetIntialMainViewController("CategoryDefinedViewForUser")
                            self.appDelegate.window?.rootViewController = mainVcIntial

                        }
                        else{
                            self.performSegue(withIdentifier: "navigateToSuperAdmin", sender: nil)
                        }
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    self.txtPassword.text = ""
                }
            }
        }
    }
    
    func callWebServiceToRegisterFCMTokenForIOT(loginResponse: JSON) {
        
        let param =  [
            "email_id_to"      : loginResponse["data"]["email"].stringValue,
            "fcm_token"        : (UserDefaults.standard.value(forKey: "fcm_token") as! String),
            "app_name"         : kAppNameAPI,
            "device_id"        : UIDevice.current.identifierForVendor?.uuidString ?? "",
            "ssecrete"         : "tellsid@1"
            ] as [String : AnyObject]
        
//        let url = kBaseURLForIOT + NetworkConstant.IOT.signup
        
        let url = ""
        requestWith(endUrl: url, imageData : nil, parameters: param)
        requestWith(endUrl: url, imageData: nil, parameters: param, onCompletion: { (response) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            self.setLoginData(json: loginResponse)
            
            if self.roleSegment.selectedSegmentIndex == 0 {
                self.performSegue(withIdentifier: "navigateToUser", sender: nil)
            }
            else{
                self.performSegue(withIdentifier: "navigateToSuperAdmin", sender: nil)
            }
        }) { (error) in
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: error?.localizedDescription)
        }
    }
    func requestWith(endUrl: String,imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    print("Succesfully uploaded")
                    if let err = response.error{
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        onError?(err)
                        return
                    }
                    onCompletion?(JSON(response))
                }
            case .failure(let error):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    //MARK: - Custom Methods
    func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.name = json["data"]["name"].stringValue
        auth.lastName = json["data"]["lastName"].stringValue
        auth.department = json["data"]["department"].stringValue
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.departmentId = json["data"]["departmentId"].stringValue
        auth.office = json["data"]["office"].stringValue
        auth.officeId = json["data"]["officeId"].stringValue
        auth.token = json["data"]["token"].stringValue
        auth.role = json["data"]["role"].stringValue
        auth.organisation_logo = json["data"]["organisation_logo"].stringValue
        auth.orgId = json["data"]["orgId"].stringValue
        auth.profileImageUrl = json["data"]["profileImage"].stringValue
        auth.orgName = json["data"]["orgname"].stringValue
        if auth.role == kUser{
            auth.isDot = json["data"]["isDot"].stringValue
        }
        
        let defaults = UserDefaults.standard
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.lastName, forKey: "lastName")
        defaults.set(auth.department, forKey: "department")
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.user_id, forKey: "user_id")
        defaults.set(auth.department, forKey: "departmentId")
        defaults.set(auth.office, forKey: "office")
        defaults.set(auth.officeId, forKey: "officeId")
        defaults.set(auth.token, forKey: "token")
        defaults.set(auth.role, forKey: "role")
        defaults.set(auth.organisation_logo, forKey: "organisation_logo")
        defaults.set(auth.orgId, forKey: "orgId")
        defaults.set(auth.profileImageUrl, forKey: "profileImage")
        defaults.set(auth.orgName, forKey: "orgname")

        if auth.role == kUser{
            defaults.set(auth.isDot, forKey: "isDot")
            
        }        
    }
}

