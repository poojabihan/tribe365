//
//  AKMonthYearPickerView.swift
//  AKMonthYearPickerView
//
//  Created by Ali Khan on 11/Jul/18.
//  Copyright © 2018 Ali Khan. All rights reserved.
//

/*
 AKMonthYearPickerView is a lightweight, clean and easy-to-use Month picker control in iOS written in Swift language.
 https://github.com/ali-cs/AKMonthYearPickerView
 */

import Foundation
import UIKit

struct AKMonthYearPickerConstants {
    
    struct AppFrameSettings {
        
        static var screenHeight : CGFloat {
            return UIScreen.main.bounds.size.height
        }
        
        static var screenWidth : CGFloat {
            return UIScreen.main.bounds.size.width
        }
    }
}

public class AKMonthYearPickerView: UIView {
    
    //MARK:- Variables
    
    var onDateSelected: ((_ month: Int, _ year: Int) -> Void)?
    var onDoneButtonSelected: (() -> Void)?
    
    private var monthYearPickerView : MonthYearPickerView?
    public  var barTintColor        = UIColor.blue
    public  var previousYear        = 4
    
    public static var sharedInstance   = {
        return AKMonthYearPickerView(frame: CGRect(origin: CGPoint(x: (AKMonthYearPickerConstants.AppFrameSettings.screenWidth - 300) / 2 , y: (AKMonthYearPickerConstants.AppFrameSettings.screenHeight - 256) / 2), size: CGSize(width: 300 /*AKMonthYearPickerConstants.AppFrameSettings.screenWidth*/, height: 200)))
    }()
    
    var toolBar : UIToolbar?
    var headerView : UIView?
    //MARK:- Inilizers
    
    convenience init() {
        let frame = CGRect(origin: CGPoint(x: 0, y: (AKMonthYearPickerConstants.AppFrameSettings.screenHeight - 256) / 2), size: CGSize(width: AKMonthYearPickerConstants.AppFrameSettings.screenWidth, height: 200))
        
        self.init(frame: frame)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor     = UIColor.white
        
        layer.borderColor   = UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0).cgColor
        
        layer.borderWidth   = 1.0
        layer.cornerRadius  = 7.0
        layer.masksToBounds = true
        
        monthYearPickerView?.addSubview(headerView!)
        monthYearPickerView = MonthYearPickerView(frame: CGRect(x: frame.origin.x, y: frame.origin.y + 50, width: frame.size.width, height: frame.size.height))
        
        monthYearPickerView?.previousYear = previousYear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- Helper Mehtods
    
    public func show(vc: UIViewController, doneHandler: @escaping () -> (), completetionalHandler: @escaping (Int, Int) -> () ) {
        monthYearPickerView?.previousYear = previousYear
        
        monthYearPickerView?.onDateSelected = completetionalHandler
        onDoneButtonSelected = doneHandler
        
        if let doneToolBar = toolBar {
            vc.view.addSubview(doneToolBar)

        }
        else if let HeaderView = headerView {
            vc.view.addSubview(HeaderView)
            
        }
        else {
            toolBar = getToolBar()
            headerView = getHeader()
            vc.view.addSubview(headerView!)
            vc.view.addSubview(toolBar!)
        }
        
        vc.view.addSubview(monthYearPickerView!)
        
        toolBar?.isHidden             = false
        headerView?.isHidden          = false
        monthYearPickerView?.isHidden = false
        
        monthYearPickerView?.commonSetup()
    }
    
    public func hide() {
        monthYearPickerView?.hide()
        
    AKMonthYearPickerView.sharedInstance.removeFromSuperview()
        toolBar?.isHidden = true
        headerView?.isHidden = true
        self.isHidden     = true
    }
    
    private func getToolBar() -> UIToolbar {
        
        let customToolbar = UIToolbar(frame: CGRect(origin: CGPoint(x: (AKMonthYearPickerConstants.AppFrameSettings.screenWidth - 300) / 2 , y: (monthYearPickerView?.frame.origin.y)! + (monthYearPickerView?.frame.size.height)! - 15 /*(AKMonthYearPickerConstants.AppFrameSettings.screenHeight - 256) / 2*/), size: CGSize(width: 300 /*AKMonthYearPickerConstants.AppFrameSettings.screenWidth*/, height: 55)))
        
       // customToolbar.barStyle     = .blackTranslucent
        customToolbar.barTintColor = barTintColor
        customToolbar.tintColor    = UIColor.darkGray
        
        embedButtons(customToolbar)
        return customToolbar
    }
    
    private func getHeader() -> UIView{
        
        let headerView =   UIView(frame: CGRect(origin: CGPoint(x: (AKMonthYearPickerConstants.AppFrameSettings.screenWidth - 300) / 2 , y: frame.origin.y), size: CGSize(width: 300 , height: 50)))
        headerView.backgroundColor = UIColor.white

        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 10, width: headerView.frame.width , height: headerView.frame.height/2)
        label.text = "SELECT DATE"
        label.backgroundColor = UIColor.white

        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 21, weight: .medium)// my custom font
        label.textColor = UIColor(red: 235.0/255.0, green: 14.0/255.0, blue: 40.0/255.0, alpha: 1.0)// my custom colour
        
        headerView.addSubview(label)
        
        let labelBorder = UILabel()
        labelBorder.frame = CGRect.init(x: 0, y: label.frame.size.height + label.frame.origin.y + 9, width: headerView.frame.width, height: 2)
        labelBorder.backgroundColor = UIColor(red: 235.0/255.0, green: 14.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        headerView.addSubview(labelBorder)

        
        return headerView
    }
    
    private func embedButtons(_ toolbar: UIToolbar) {
        let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.donePressed(_:)))
        
        
        toolbar.setItems([ flexButton, doneButton, flexButton], animated: true)
    }
    
    @objc func donePressed(_ sender: Any) {
        onDoneButtonSelected?()
        hide()
    }
}

/// This class is responsible for handling the pickerView delegate and datasource
private class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK:- Variables
    
    var months          : [String]!
    var years           : [Int]!
    var previousYear    = 2
    var month = Calendar.current.component(.month, from: Date()) {
        
        didSet {
            selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    
    var year = Calendar.current.component(.year, from: Date()) {
        didSet {
            selectRow(years.index(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var onDateSelected: ((_ month: Int, _ year: Int) -> Void)?
    
    static var sharedInstance   = {
        return MonthYearPickerView(frame: CGRect(origin: CGPoint(x: 0, y: (AKMonthYearPickerConstants.AppFrameSettings.screenHeight - 256) / 2), size: CGSize(width: AKMonthYearPickerConstants.AppFrameSettings.screenWidth, height: 216)))
    }()
    
    //MARK:- Inilizers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor     = UIColor.white
        
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    //MARK:- Helper Methods
    
    func show(vc: UIViewController, completetionalHandler: @escaping (Int, Int) -> () ) {
        
        MonthYearPickerView.sharedInstance.onDateSelected = completetionalHandler
        commonSetup()
        
        vc.view.addSubview(MonthYearPickerView.sharedInstance)
    }
    
    internal func hide() {
        MonthYearPickerView.sharedInstance.removeFromSuperview()
        self.isHidden = true
    }
    func specialSetup(){
        // population months with localized names
        var months: [String] = []
        var month = 0
        
        //Change
        let nameOfYear = Calendar.current.component(.year, from: Date())
        print(nameOfYear)
        
        if years[0] == nameOfYear
        {
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            let nameOfMonth = dateFormatter.string(from: now)
            print(nameOfMonth)
            
            let preFromCurrentMonth = Int(nameOfMonth)! - 1
            for _ in 1...12 {
                if String(month) == String(preFromCurrentMonth){
                    break
                }
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
                
            }
        }
        else{
            for _ in 1...12 {
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
            }
        }
        self.months = months
       
    }
    func commonSetup() {
        // population years
        var years: [Int] = []
        if years.count == 0 {
            var year = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
            
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            let nameOfMonth = dateFormatter.string(from: now)
            print(nameOfMonth)
            
            if nameOfMonth == "01"{
                for _ in 1...previousYear {
                    years.append(year)
                    year -= 1
                }
                years.remove(at: 0)
            }
            
            else{
            for _ in 1...previousYear {
                years.append(year)
                year -= 1
            }
            }
        }
        self.years = years
        
        // population months with localized names
        var months: [String] = []
        var month = 0
        
        //Change
        let nameOfYear = Calendar.current.component(.year, from: Date())
        print(nameOfYear)
        
      
        if years[0] == nameOfYear
        {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let nameOfMonth = dateFormatter.string(from: now)
        print(nameOfMonth)
    
        
            let preFromCurrentMonth = Int(nameOfMonth)! - 1
            for _ in 1...12 {
                if String(month) == String(preFromCurrentMonth){
                    break
                }
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
                
            }
           
        }
        else{
            for _ in 1...12 {
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
            }
        }
        self.months = months
        
        self.delegate = self
        self.dataSource = self
        
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        
        if currentMonth == 1{
            
            print("First Month")
            self.specialSetup()
        }
        else{
        self.selectRow(currentMonth - 2, inComponent: 0, animated: false)
        }
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var month = selectedRow(inComponent: 0)+1
        let selectedMonth = selectedRow(inComponent: 0)+1

        let year  = years[selectedRow(inComponent: 1)]
        let selectedYear  = years[selectedRow(inComponent: 1)]

//        specialSetup()
        if let block = onDateSelected {
            block(month, year)
        }

        
        if component == 0 {
            return
        }
    
        var months: [String] = []
        month = 0
        let nameOfYear = year

        if years[0] == nameOfYear
        {
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            var nameOfMonth = dateFormatter.string(from: now)
            print(nameOfMonth)
            if nameOfMonth == "01"{
            nameOfMonth = "13"
            let preFromCurrentMonth = Int(nameOfMonth)! - 1
            for _ in 1...12 {
                if String(month) == String(preFromCurrentMonth){
                    break
                }
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
            }
            }
                else{
                let preFromCurrentMonth = Int(nameOfMonth)! - 1
                for _ in 1...12 {
                    if String(month) == String(preFromCurrentMonth){
                        break
                    }
                    months.append(DateFormatter().monthSymbols[month].capitalized)
                    month += 1
                }
                
                }
           
        }
        else{
            for _ in 1...12 {
                months.append(DateFormatter().monthSymbols[month].capitalized)
                month += 1
            }
        }
        
        if let block = onDateSelected {
            //block(month, year)
            block(selectedMonth, selectedYear)
        }

      //  self.month = month
        self.month = selectedMonth
        self.year = year
        
        self.months = months
        pickerView.reloadAllComponents()
    }
    
}
